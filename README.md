# Tm Server Tool
Updated on 3/13/2018: This web application tool has been moved from JQuery and simple html/css to React JS framework, with nearly one month hardwork.

This is the repository for Tm Server Tool that is built by Spring Boot in the backend and React JS, Redux framework in the frontend and utilizes web services of GL Tm Server to provide following services:

1. Concordance Search
   - By entering search strings and selecting TMs in selection option box for searching, this functionality will provide user to search content in source/target and return it as a complete table.
      - User can download search result as a csv file.
2. Create CSV
   - This functionality helps user to generate two types of CSV files for Tm Server and Project Directory.
      - By selecting organization and Tm Group from selection option box, this functionality will provide user a CSV file containing all associated Tm Connection String, and a CSV file containing Tm connection info to import TM profile in PD.
3. Create multiple TMs
   - This functionality helps user to create Tm Group and create multiple TMs instantly.
      - By selecting Tm Group, source language, target language(s), it will automatically create TMs with provided info in the form. TM Name, generic username, password, Tm Cleanup password can be auto generated when user selects one Tm Group.
4. Import multiple TMs
   - This functionality helps user to import TMX files to selected TM on the displayed table.
      - User will need to select corresponding TM(s) in the table and click upload file button to upload Tmx or zip file to the server. After that, the application will automatically unzips the zip file and upload the tmx file to corresponding TM.
5. Export multiple TMs
   - This functionality helps user to export selected TMs as TMX files into FTP.
6. Rename multiple TMs
   - This functionality helps user to rename multiple TM's name instantly.
7. Manipulate multiple TMX files
   - This functionality helps user to add/remove attributes from multiple TMX files instantly.

# Running the application locally:
## Backend Spring Boot:
1. Download [SpringSTS](https://spring.io/tools/sts/all) and install it to local computer.
2. Open SpringSTS and create a workspace.
3. Clone this [project](https://weihe2015@bitbucket.org/weihe2015/tmserver-tool.git) into the workspace and import it into SpringSTS
    - *git clone https://weihe2015@bitbucket.org/weihe2015/tmserver-tool.git*
4. Install [Maven](https://maven.apache.org/download.cgi) if you have not done so.
5. Run *mvn clean install* to add all dependencies from pom.xml into the project.
6. Install MariaDB/mySQL into your local computer.
7. Configurate the database username/password in application.properties file 
8. Create a database called tm_server and import a QA/production TM Server database into it.
9. Import *db_structure.sql* into tm_server database to create user, role and user_role tables for user login and signup:
     - mysql -u *username* -p tm_server < db_structure.sql 
10. Click *Run* to start this application. You may navigate to [http://localhost:7000/Tool/](http://localhost:7000/Tool/).
     - Note: We have two version of the tool, QA and Prod. QA: http://localhost:7000/Toolqa/ and Prod: http://localhost:7000/Tool/
     - For QA version, use "yarn build-qa" or "yarn start" to bundle js files.
     - For production version, use "yarn build" to bundle js files

## Front End React JS Framework: (with Tm Server Tool 2.x)
1. Install Node.JS  and npm: https://www.npmjs.com, node version: 6.x
2. Optional: install Yarn by running following command: "npm i yarn"
3. Navigate to folder: src/main/resources/static
4. Run "npm install" if you do not install yarn or run "yarn install" with Yarn package. Both commands will install all necessary packages listed in package.json
5. Once all dependencies libraries are installed, run "npm start" or "yarn start" to start compiling the js files.

# Running the application on server:
1. Assume you have successfully run the application locally.
2. Webpack and bundle js files with command "yarn build-qa or yarn build"
3. Switch to *deploy* branch 
4. Run "mvn clean install" to make sure that it passes all the test and is able to built the application.
5. Run "mvn package" to generate a war file.
6. Upload this war file to corresponding TM Server application server.
7. Navigate to apache-tomcat/webapps folder, create a folder called Tool and put the war file there.
8. Unzip this war file, change the application.properties file in WEB-INF/classes/ to proper settings in log4j, logs file output etc.
9. Restart tomcat.

# Edit/Change frontend:
1. Make sure npm and yarn are installed globally in the computer.
2. Change Directory to folder: src/main/resources/static/
3. Start webpack by "yarn start" or "npm start".
4. Edit js files under folder: src/main/resources/static/js/react