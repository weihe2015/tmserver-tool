package org.gs4tr.tmserver.tool.service.tmx.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.apache.commons.io.IOUtils;
import org.gs4tr.foundation.locale.Locale;

public class TmxExporterWriter extends TmxStreamExporterWriter
{
    private static final String ENCODING = "UTF-8";

    private static Writer createFileWriter(String filename)
    {
        Writer w = null;
        FileOutputStream fs = null;
        try
        {
            fs = new FileOutputStream(new File(filename));
            w = new BufferedWriter(new OutputStreamWriter(fs, ENCODING));
            return w;
        }
        catch (IOException ex)
        {
            IOUtils.closeQuietly(w);
            IOUtils.closeQuietly(fs);
            throw new RuntimeException(ex);
        }
    }

    public TmxExporterWriter(Locale srcLocale, Locale tgtLocale, String filename)
    {
        super(srcLocale, tgtLocale, createFileWriter(filename));
    }

    @Override
    protected void closeWriter(Writer w)
    {
        super.closeWriter(w);
        IOUtils.closeQuietly(w);
    }
}
