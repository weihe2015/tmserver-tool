package org.gs4tr.tmserver.tool.service.user;

import java.util.Map;

import org.gs4tr.tmserver.tool.controller.model.RemoteTmServerCredential;
import org.gs4tr.tmserver.tool.controller.model.TPAuthLoginOption;
import org.gs4tr.tmserver.tool.model.user.TmUser;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserService
{
    public boolean comparePassword(TmUser prevUser, String oldPassword);

    public Map<String, Object> decodeToken(String token);

    public TmUser findUserByUsername(String username);

    public String generateToken(RemoteTmServerCredential newTmServerInfo);

    public String generateToken(String username, String password, String sessionId);

    public String generateTokenSearchOnly(String username, String password, String sessionId);

    public String generateOAuthToken(TPAuthLoginOption tpauthLoginOption, String sessionId);

    public UserDetails getUserDetailsByUsername(String username);

    public String login(String username, String password, String sessionId);

    public String refreshToken(String oldToken);

    public void saveUser(TmUser user);
}
