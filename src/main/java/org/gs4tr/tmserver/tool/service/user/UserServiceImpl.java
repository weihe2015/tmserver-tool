package org.gs4tr.tmserver.tool.service.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.configuration.JwtTokenService;
import org.gs4tr.tmserver.tool.controller.model.RemoteTmServerCredential;
import org.gs4tr.tmserver.tool.controller.model.TPAuthLoginOption;
import org.gs4tr.tmserver.tool.model.user.Role;
import org.gs4tr.tmserver.tool.model.user.TmUser;
import org.gs4tr.tmserver.tool.model.user.TmUserRole;
import org.gs4tr.tmserver.tool.repository.RoleRepository;
import org.gs4tr.tmserver.tool.repository.TmUserRoleRepository;
import org.gs4tr.tmserver.tool.repository.UserRepository;
import org.gs4tr.tmserver.tool.repository.encoder.ShaPasswordEncoderAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("userService")
public class UserServiceImpl implements UserService, UserDetailsService
{
    static public final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    public static final String SEARCH_FEATURE = "search";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TmUserRoleRepository tmUserRoleRepository;

    @Autowired
    private ShaPasswordEncoderAdapter _passwordEncoder;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    private UserDetails buildUserForAuthentication(TmUser user, List<GrantedAuthority> authorities)
    {
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
            user.isEnabled(), user.isAccountNonExpired(), user.isCredentialsNonExpired(),
            user.isAccountNonLocked(), authorities);
    }

    @Override
    public boolean comparePassword(TmUser prevUser, String oldPassword)
    {
        String password = _passwordEncoder.encode(oldPassword);
        return StringUtils.equals(prevUser.getPassword(), password);
    }

    @Override
    public Map<String, Object> decodeToken(String token)
    {
        return jwtTokenService.getClaimsMap(token);
    }

    @Override
    public TmUser findUserByUsername(String username)
    {
        return userRepository.findByUsername(username);
    }

    @Override
    public String generateToken(RemoteTmServerCredential newTmServerInfo)
    {
        String username = newTmServerInfo.getSub();
        String password = newTmServerInfo.getPass();
        String url = newTmServerInfo.getUrl();
        String sessionId = newTmServerInfo.getSid();
        final String token = jwtTokenService.generateToken(url, username, password, sessionId);
        return token;
    }

    @Override
    public String generateToken(String username, String password, String sessionId)
    {
        final String token = jwtTokenService.generateToken(username, password, sessionId);
        return token;
    }

    @Override
    public String generateTokenSearchOnly(String username, String password, String sessionId)
    {
        return jwtTokenService.generateTokenLimitedFeature(username, password, sessionId,
            SEARCH_FEATURE);
    }

    @Override
    public String generateOAuthToken(TPAuthLoginOption tpAuthLoginOption, String sessionId)
    {
        return jwtTokenService.generateToken(tpAuthLoginOption, sessionId);
    }

    private List<GrantedAuthority> getUserAuthority(Set<Role> userRoles)
    {
        Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
        for (Role role : userRoles)
        {
            roles.add(new SimpleGrantedAuthority(role.getRoleType().name()));
        }

        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>(roles);
        return grantedAuthorities;
    }

    @Override
    public UserDetails getUserDetailsByUsername(String username)
    {
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        return userDetails;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
    {
        TmUser user = userRepository.findByUsername(username);
        // If user is not found, we switch to default user "whe"
        if (user == null)
        {
            LOG.warn(String.format(Messages.getString("UserController.10"), username));
            username = "whe";
            user = userRepository.findByUsername(username);
        }
        TmUserRole tmUserRole = tmUserRoleRepository.findByUserProfileId(user.getId());
        Role userRole = roleRepository.findByRoleId(tmUserRole.getRoleId());
        Set<Role> roles = new HashSet<Role>(Arrays.asList(userRole));
        List<GrantedAuthority> authorities = getUserAuthority(roles);
        return buildUserForAuthentication(user, authorities);
    }

    @Override
    public String login(String username, String password, String sessionId)
    {
        String encodedPassword = _passwordEncoder.encode(password);
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username,
            encodedPassword);
        // Perform the security
        final Authentication authentication = authenticationManager.authenticate(upToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtTokenService.generateToken(username, password, sessionId);
        return token;
    }

    @SuppressWarnings("unused")
    @Override
    public String refreshToken(String oldToken)
    {
        final String token = oldToken.substring(tokenHead.length());
        String username = jwtTokenService.getUsernameFromToken(token);
        // TODO: compare current token creation time and user reset password timestamp.
        UserDetails userDetail = userDetailsService.loadUserByUsername(username);
        return jwtTokenService.refreshToken(token);
    }

    @Override
    public void saveUser(TmUser user)
    {
        String encodedPassword = _passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        userRepository.save(user);
    }
}
