package org.gs4tr.tmserver.tool.service.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;

@Component
public class ZipFileUtils
{
    public void unzipFiles(File zipFile, String outputFolder) throws IOException
    {
        byte[] buffer = new byte[1024];
        // create output directory is not exists
        File folder = new File(outputFolder);
        if (!folder.exists())
        {
            folder.mkdir();
        }

        // get the zip file content
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
        // get the zipped file list entry
        ZipEntry ze = zis.getNextEntry();

        while (ze != null)
        {
            String fileName = ze.getName();
            fileName = fileName.replaceAll(Constants.SPACE, Constants.UNDERSCORE);
            File newFile = new File(outputFolder + File.separator + fileName);

            // create all non exists folders
            // else you will hit FileNotFoundException for compressed folder
            newFile.getParentFile().mkdirs();
            FileOutputStream fos = new FileOutputStream(newFile);

            int len;
            while ((len = zis.read(buffer)) > 0)
            {
                fos.write(buffer, 0, len);
            }

            fos.close();
            ze = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();
    }

    /**
     * Helper function for TmImportController
     */
    public void unzipFiles(File zipFile, String outputFolder, String tmPathInfo) throws IOException
    {
        byte[] buffer = new byte[1024];
        // create output directory is not exists
        File folder = new File(outputFolder);
        if (!folder.exists())
        {
            folder.mkdir();
        }

        // get the zip file content
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
        // get the zipped file list entry
        ZipEntry ze = zis.getNextEntry();

        while (ze != null)
        {
            String fileName = tmPathInfo + "#" + ze.getName();
            File newFile = new File(outputFolder + File.separator + fileName);

            // create all non exists folders
            // else you will hit FileNotFoundException for compressed folder
            newFile.getParentFile().mkdirs();
            FileOutputStream fos = new FileOutputStream(newFile);

            int len;
            while ((len = zis.read(buffer)) > 0)
            {
                fos.write(buffer, 0, len);
            }

            fos.close();
            ze = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();
    }

    public void zipFiles(List<String> tmxFiles, String zipFile) throws IOException
    {
        byte[] buffer = new byte[1024];

        FileOutputStream fos = new FileOutputStream(zipFile);
        ZipOutputStream zos = new ZipOutputStream(fos);

        for (String tmxFile : tmxFiles)
        {
            String fileName = FilenameUtils.getName(tmxFile);
            ZipEntry ze = new ZipEntry(fileName);
            zos.putNextEntry(ze);

            FileInputStream in = new FileInputStream(tmxFile);

            int len;
            while ((len = in.read(buffer)) > 0)
            {
                zos.write(buffer, 0, len);
            }

            in.close();
        }

        zos.closeEntry();
        // remember close it
        zos.close();
    }

}
