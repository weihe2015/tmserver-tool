package org.gs4tr.tmserver.tool.service.tmx;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.foundation3.io.utils.FileUtils;
import org.gs4tr.foundation3.xml.XmlParser;
import org.gs4tr.tm3.util.wordfast.WordFastCodec;
import org.gs4tr.tmserver.tool.model.wf.common.TU;
import org.gs4tr.tmserver.tool.model.wf.common.WordFastHeader;
import org.gs4tr.tmserver.tool.model.wf.utils.WfUtils;
import org.gs4tr.tmserver.tool.service.tmx.io.MonitoringFileInputStream;
import org.gs4tr.tmserver.tool.service.tmx.io.callback.WfReaderCallback;
import org.gs4tr.tmserver.tool.service.tmx.io.callback.imp.WfReaderCallbackImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WfTxtLocaleReader
{
    private static final Logger _logger = LoggerFactory.getLogger(WfTxtLocaleReader.class);
    private WfReaderCallback callback;
    private WordFastHeader header;

    public WfTxtLocaleReader()
    {
    }

    public String getSourceLanguage()
    {
        return callback.getSourceLanguage();
    }

    public String getTargetLanguage()
    {
        return callback.getTargetLanguage();
    }

    public void readFile(String filename) throws IOException
    {
        callback = new WfReaderCallbackImpl();
        File wf = new File(filename);

        if (!wf.exists())
        {
            throw new RuntimeException("File " + filename + " does not exist");
        }

        try
        {
            WfUtils.validateWfHeader(filename);
        }
        catch (Throwable ex)
        {
            throw new RuntimeException("File " + filename + " has invalid WF header: " + ex.getMessage());
        }

        String encoding = FileUtils.getStatus(new File(filename)).getEncoding().getEncoding();

        // Read the file.
        MonitoringFileInputStream stream = new MonitoringFileInputStream(filename);
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(stream, encoding));

        try
        {
            String line = reader.readLine();

            header = new WordFastHeader(line);
            callback.handle(header);
            int tuCount = 0;

            while ((line = reader.readLine()) != null)
            {

                // TODO: getting called during validation and actual reading -
                // why?
                // System.err.println("*** line " + reader.getLineNumber() +
                // " (Thread " + Thread.currentThread().getId() + ": " +
                // Thread.currentThread().getName() + ")");

                if (StringUtils.isEmpty(line))
                {
                    continue;
                }

                if (line.startsWith("xx"))
                {
                    continue;
                }

                TU tu = null;

                try
                {
                    // Plain parsing of TU and basic error checking.
                    tu = new TU(line);
                    // Warn if there are less than 7 or more than 11 fields.
                    WfUtils.fieldCheck(line);
                    // Thoroughly check locale syntax XX-YY.
                    WfUtils.localeCheck(tu);

                    // Convert to TMX and XML-parse to detect invalid XML chars.
                    HashMap<Integer, Integer> tags = new HashMap<Integer, Integer>();
                    String tmxSource = WordFastCodec.decodeSource(tu.getSource(), tags);
                    String tmxTarget = WordFastCodec.decodeTarget(tu.getTarget(), tags);

                    XmlParser.parseXmlFragment("<x>" + tmxSource + "</x>");
                    XmlParser.parseXmlFragment("<x>" + tmxTarget + "</x>");

                }
                catch (Throwable ex)
                {
                    _logger.warn("Ignoring bad TU in file " + filename + " line " + reader.getLineNumber() +
                        ": " + ex.getMessage());
                    callback.invalidSegment(line);
                    continue;
                }
                tuCount++;

                if (!callback.handle(tu))
                {
                    callback.invalidSegment(line);
                }
                // only read one TU to get source and target locale;
                break;
            }
        }
        finally
        {
            IOUtils.closeQuietly(reader);
        }
    }
}
