package org.gs4tr.tmserver.tool.service.excel.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.gs4tr.tmserver.tool.model.bilingual.TencentTu;
import org.gs4tr.tmserver.tool.service.excel.ExcelReader;

public class ExcelReaderImpl implements ExcelReader
{
    @Override
    public List<TencentTu> readTencentExcel(String excelFilePath) throws IOException
    {
        List<TencentTu> tencentTuList = new ArrayList<>();
        HSSFWorkbook workbook = null;
        InputStream is = new FileInputStream(excelFilePath);
        try
        {
            workbook = new HSSFWorkbook(is);
            HSSFSheet mysheet = workbook.getSheetAt(0);
            int rowCnt = mysheet.getLastRowNum();

            // Skip the Excel Header:
            for (int i = 1; i <= rowCnt; i++)
            {
                Row row = mysheet.getRow(i);
                if (isRowEmpty(row))
                {
                    continue;
                }
                TencentTu tu = new TencentTu();
                // process filename and get project, product, and type info:
                getProjectProductTypeInfo(tu, excelFilePath);
                // get source and target content from row:
                getBiligualInfo(tu, row);
                tencentTuList.add(tu);
            }
        }
        finally
        {
            if (workbook != null)
            {
                workbook.close();
            }
            IOUtils.closeQuietly(is);
        }
        return tencentTuList;
    }

    /**
     * Check if row is empty. If it is empty, or target column we don't need to get the info from this
     * row.
     */
    private boolean isRowEmpty(Row row)
    {
        if (row == null || row.getLastCellNum() <= 0)
        {
            return true;
        }

        int i = row.getFirstCellNum(), n = row.getLastCellNum();
        while (i < n)
        {
            Cell cell = row.getCell(i);
            if (!isCellEmpty(cell))
            {
                return false;
            }
            i++;
        }
        return true;
    }

    private boolean isCellEmpty(Cell cell)
    {
        return cell == null || cell.getCellType() == CellType.BLANK;
    }

    private void getProjectProductTypeInfo(TencentTu tu, String excelFilePath)
    {
        String fileName = FilenameUtils.getBaseName(excelFilePath);
        Matcher matcher = EXCEL_FILE_PATTERN.matcher(fileName);
        if (!matcher.find() || (matcher.groupCount() != 3 && matcher.groupCount() != 4))
        {
            return;
        }
        String project = matcher.group(1);
        if (!StringUtils.equals(EMPTY_CONTENT, project))
        {
            tu.setProject(project);
        }

        String product = matcher.group(2);
        if (!StringUtils.equals(EMPTY_CONTENT, product))
        {
            tu.setProduct(product);
        }

        String type = matcher.group(3);
        if (!StringUtils.equals(EMPTY_CONTENT, type))
        {
            tu.setType(type);
        }
    }

    private void getBiligualInfo(TencentTu tu, Row row)
    {
        int lastRowIdx = row.getLastCellNum();
        Cell cell = row.getCell(lastRowIdx);
        if (lastRowIdx == 2 || isCellEmpty(cell))
        {
            // get Source content
            cell = row.getCell(0);
            if (!isCellEmpty(cell))
            {
                String source = cell.getStringCellValue();
                tu.setSource(source);
            }

            // get Target content
            cell = row.getCell(1);
            if (!isCellEmpty(cell))
            {
                String target = cell.getStringCellValue();
                tu.setTarget(target);
            }
        }
        else
        {
            cell = row.getCell(0);
            if (!isCellEmpty(cell))
            {
                setMetadata(tu, cell);
            }

            // get Source content
            cell = row.getCell(1);
            if (!isCellEmpty(cell))
            {
                String source = cell.getStringCellValue();
                tu.setSource(source);
            }

            // get Target content
            cell = row.getCell(2);
            if (!isCellEmpty(cell))
            {
                String target = cell.getStringCellValue();
                tu.setTarget(target);
            }
        }
    }

    private void setMetadata(TencentTu tu, Cell cell)
    {
        String filePath = cell.getStringCellValue();
        String path = FilenameUtils.getPath(filePath);
        if (StringUtils.isNotEmpty(path))
        {
            tu.setMetadata(FilenameUtils.getExtension(filePath));
        }
        else
        {
            tu.setMetadata(FilenameUtils.getName(filePath));
        }
    }
}
