package org.gs4tr.tmserver.tool.service.file;

import java.io.File;

public class Constants
{
    public static final String COMMA = ",";
    public static final String UNDERSCORE = "_";
    public static final String SPACE = " ";
    public static final String POUND = "#";
    public static final String AETT = "@";
    public static final String PERCENTAGE = "%";
    public static final String DOLLAR = "$";
    public static final String SLASH = "/";
    public static final String DOT = ".";
    public static final String CRLF = "\r\n";
    public static final String SEPARATOR = File.separator;

    public static final String TXTEXTENSION = ".txt";
    public static final String TMXEXTENSION = ".tmx";
    public static final String ZIPEXTENSION = ".zip";
    public static final String XLS_EXTENSION = ".xls";

    public static final String TXT_UPPER_EXTENSION = ".TXT";
    public static final String TMX_UPPER_EXTENSION = ".TMX";
    public static final String ZIP_UPPER_EXTENSION = ".ZIP";

    public static final String TOOL = "Tool";
    public static final String UPLOADDIR = "uploadDir";
    public static final String SOURCEDIR = "sourceManipulateDir";
    public static final String IMPORT = "import";
    public static final String MANIPULATE = "manipulate";
    public static final String MANIPULATE_EXPORT_FILE_PREFIX = "ManipulatedTmx_";
    public static final String DATEFORMATE = "yyyy-MM-dd";

    public static final String TP_AUTH = "oauth";
    public static final String TP_AUTH_ACCESS_TOKEN = "access_token";

    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";
    public static final String POST = "POST";
    public static final String ACCEPT = "Accept";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String UNDEFINED = "undefined";

    public static final String TEMPFOLDER = System.getProperty("java.io.tmpdir");

    // Create PD CSV Header:
    public static final String[] PD_CSV_HEADERS = { "TM Profile Name", "Analysis/Cleanup URL", "Analysis/Cleanup User",
        "Analysis/Cleanup Password", "Linguist URL", "Linguist Login", "Linguist Password", "TM Name",
        "Source Locale", "Target Locale", "Update TM", "Add File Name", "PaClient Name", "Organization" };

    public static final String[] PD_CSV_NEW_HEADERS = { "TM Profile Name", "Analysis/Cleanup URL", "Analysis/Cleanup User",
        "Analysis/Cleanup Password", "Linguist URL", "Linguist Login", "Linguist Password", "TM Name",
        "Source Locale", "Target Locale", "Update TM", "TM Update Passcode", "Add File Name", "PaClient Name", "Organization" };

    // Create TM Server CSV Headers:
    public static final String[] TM_SERVER_CSV_HEADER = { "Organization", "TM", "Connection string", "Source Locale",
    "Target Locale" };
}
