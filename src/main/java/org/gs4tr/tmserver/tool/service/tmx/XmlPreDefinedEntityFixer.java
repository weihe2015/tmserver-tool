package org.gs4tr.tmserver.tool.service.tmx;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.gs4tr.foundation3.codec.XmlCodec;
import org.gs4tr.foundation3.encoding.CharsetPrinter;
import org.gs4tr.foundation3.io.model.EOL;
import org.gs4tr.foundation3.io.model.FileStatus;
import org.gs4tr.foundation3.io.utils.FileUtils;
import org.gs4tr.foundation3.xml.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XmlPreDefinedEntityFixer
{
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlPreDefinedEntityFixer.class);

    // This expression uses negative lookahead to find any free standing ampersands
    public static Pattern re_nativeAmp = Pattern.compile("&(?!(.{1,10}?;))");

    public static Pattern re_encodedEntity = Pattern.compile("&.{1,10}?;");

    private File file;
    private File destFile;

    public XmlPreDefinedEntityFixer(File file)
    {
        this.file = file;
    }

    public XmlPreDefinedEntityFixer(File srcFile, File destFile)
    {
        this.file = srcFile;
        this.destFile = destFile;
    }

    /**
     * @return True upon error. Otherwise false.
     */
    public boolean clean()
    {
        Boolean hasError = false;
        LineNumberReader lnreader = null;
        Writer writer = null;
        XmlCodec codec = new XmlCodec();

        try
        {
            FileStatus fs = FileUtils.getStatus(file);
            FileInputStream fis = new FileInputStream(file);
            String encoding = guessInputEncoding(file);

            EOL eol = fs.getEol();
            if (eol == null)
            {
                eol = EOL.WIN;
            }
            InputStreamReader isr = new InputStreamReader(fis, encoding);
            lnreader = new LineNumberReader(isr);

            writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(getOutputName())), encoding);

            String line = "";
            while ((line = lnreader.readLine()) != null)
            {
                // Fix native ampersand.
                Matcher m = re_nativeAmp.matcher(line);
                StringBuffer sb = new StringBuffer();
                while (m.find())
                {
                    m.appendReplacement(sb, "&amp;");
                }
                m.appendTail(sb);

                // Decode numerically encoded control characters.
                m = re_encodedEntity.matcher(sb.toString());
                sb.setLength(0);
                while (m.find())
                {
                    char ch = codec.decodeEntity(m.group());
                    if (ch != '\uFFFF')
                    {
                        m.appendReplacement(sb, XmlUtils.encodeXmlEntities(String.valueOf(ch)));
                    }
                }
                m.appendTail(sb);

                // This removes control characters that are invalid in XML.
                writer.write(EditUtil.fixInvalidXmlChars(sb.toString()));
                writer.write(eol.getEol());
            }
        }
        catch (Exception ex)
        {
            LOGGER.warn("Failed to fix predefined illegal XML characters", ex);
            hasError = true;
        }
        finally
        {
            IOUtils.closeQuietly(lnreader);
            IOUtils.closeQuietly(writer);
            LOGGER.warn("Predefined illegal XML characters were converted to predefined entity references.");
        }

        return hasError;
    }

    /**
     * Guesses the encoding of the input file (in Java format). This function does not work when the
     * input comes from a string.
     *
     * @return an encoding in Java format
     */
    private String guessInputEncoding(File file)
    {
        if (!file.exists())
        {
            return null;
        }

        try
        {
            CharsetPrinter guesser = new CharsetPrinter();
            String result = guesser.guessEncoding(file);
            if (result == null || result.equals("void"))
            {
                result = "Cp1252";
            }

            return result;
        }
        catch (IOException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public File getOutputFile()
    {
        return new File(getOutputName());
    }

    public String getOutputName()
    {
        if (destFile != null)
        {
            return destFile.getAbsolutePath();
        }
        else
        {
            String base = FilenameUtils.removeExtension(file.getAbsolutePath());
            String extension = FilenameUtils.getExtension(file.getAbsolutePath());
            return base + "-cleaned." + extension;
        }
    }
}
