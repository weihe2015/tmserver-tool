package org.gs4tr.tmserver.tool.service.excel;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import org.gs4tr.tmserver.tool.model.bilingual.TencentTu;

public interface ExcelReader
{
    static public final Pattern EXCEL_FILE_PATTERN = Pattern.compile("^([^-]+)-([^-]+)-([^-\\(\\)]+)(-json)?",
        Pattern.CASE_INSENSITIVE);
    static public final String EMPTY_CONTENT = "[]";

    public List<TencentTu> readTencentExcel(String excelFile) throws IOException;
}
