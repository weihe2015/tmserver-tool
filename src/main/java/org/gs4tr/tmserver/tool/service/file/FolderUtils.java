package org.gs4tr.tmserver.tool.service.file;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.controller.model.TmImportOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class FolderUtils
{
    private static final Logger LOGGER = LoggerFactory.getLogger(FolderUtils.class);

    public String generateTargetZipFile(String targetFolderName, String targetDateTime)
    {
        StringBuffer sb = new StringBuffer();
        sb.append(targetFolderName);
        sb.append(Constants.MANIPULATE_EXPORT_FILE_PREFIX);
        sb.append(targetDateTime);
        sb.append(Constants.ZIPEXTENSION);
        return sb.toString();
    }

    public void getAllTmFiles(String folderName, List<String> tmxFileList)
    {
        File folder = new File(folderName);
        if (folder.listFiles() == null || folder.listFiles().length == 0)
        {
            return;
        }

        for (File file : folder.listFiles())
        {
            // 9-5-18, accept both Wf TXT and TMX file for import
            if (file.isFile() && (
                file.getName().endsWith(Constants.TMXEXTENSION) ||
                file.getName().endsWith(Constants.TMX_UPPER_EXTENSION) ||
                file.getName().endsWith(Constants.TXTEXTENSION) ||
                file.getName().endsWith(Constants.TXT_UPPER_EXTENSION)
                ))
            {
                tmxFileList.add(file.getAbsolutePath());
            }
            else if (file.isDirectory())
            {
                getAllTmFiles(file.getAbsolutePath(), tmxFileList);
            }
        }
    }

    public void getAllTmFiles(String folderName, TmImportOption tmImportOption, List<File> uploadFileList)
    {
        File folder = new File(folderName);
        if (folder.listFiles() == null || folder.listFiles().length == 0)
        {
            LOGGER.info("Folder " + folderName + " does not have files, returning.");
            return;
        }

        for (File file : folder.listFiles())
        {
            if (file.isFile() && (
                file.getName().endsWith(Constants.TMXEXTENSION) ||
                file.getName().endsWith(Constants.TMX_UPPER_EXTENSION) ||
                file.getName().endsWith(Constants.TXTEXTENSION) ||
                file.getName().endsWith(Constants.TXT_UPPER_EXTENSION)))
            {
                String prevTmPath = file.getName().split(Constants.AETT)[0];
                prevTmPath = prevTmPath.replace(Constants.DOLLAR, Constants.SLASH);
                if (StringUtils.equals(prevTmPath, tmImportOption.getTmPath()))
                {
                    uploadFileList.add(file);
                }
            }
            else if (file.isDirectory())
            {
                getAllTmFiles(file.getAbsolutePath(), tmImportOption, uploadFileList);
            }
        }
    }

    public void getAllTmxFiles(String folderName, List<String> tmxFileList)
    {
        File folder = new File(folderName);
        if (folder.listFiles() == null || folder.listFiles().length == 0)
        {
            return;
        }

        for (File file : folder.listFiles())
        {
            if (file.isFile() && (file.getName().endsWith(Constants.TMXEXTENSION) ||
                file.getName().endsWith(Constants.TMX_UPPER_EXTENSION)))
            {
                tmxFileList.add(file.getAbsolutePath());
            }
            else if (file.isDirectory())
            {
                getAllTmxFiles(file.getAbsolutePath(), tmxFileList);
            }
        }
    }

    public void getAllXLSFiles(String folderName, List<String> excelFileList)
    {
        File folder = new File(folderName);
        if (folder.listFiles() == null || folder.listFiles().length == 0)
        {
            return;
        }

        for (File file : folder.listFiles())
        {
            if (file.isFile() && file.getName().endsWith(Constants.XLS_EXTENSION))
            {
                excelFileList.add(file.getAbsolutePath());
            }
            else if (file.isDirectory())
            {
                getAllXLSFiles(file.getAbsolutePath(), excelFileList);
            }
        }
    }

    public String getFolderWithLocales(String folderName, String sourceLocaleCode, String targetLocaleCode)
    {
        StringBuffer stringBuffer = new StringBuffer(folderName);
        stringBuffer.append(sourceLocaleCode);
        stringBuffer.append(Constants.UNDERSCORE);
        stringBuffer.append(targetLocaleCode);
        stringBuffer.append(Constants.SEPARATOR);
        return stringBuffer.toString();
    }

    public String getNewTmxFilePathWithLocales(String folderNameWithLocale, String tmPath, String fileName)
    {
        StringBuffer stringBuffer = new StringBuffer(folderNameWithLocale);
        stringBuffer.append(tmPath);
        stringBuffer.append(Constants.AETT);
        stringBuffer.append(fileName);
        return stringBuffer.toString();
    }

    public String getSourceManipulateDir(String systemDir, String date, String randomToken)
    {
        File systemDirFile = new File(systemDir);
        StringBuffer stringBuffer = new StringBuffer(systemDirFile.getParent());
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(Constants.TOOL);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(Constants.SOURCEDIR);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(date);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(randomToken);
        stringBuffer.append(Constants.SEPARATOR);
        return stringBuffer.toString();
    }

    public String getUploadFolderPath(String systemDir, String date, String randomToken)
    {
        File systemDirFile = new File(systemDir);
        StringBuffer stringBuffer = new StringBuffer(systemDirFile.getParent());
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(Constants.TOOL);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(Constants.UPLOADDIR);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(date);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(randomToken);
        stringBuffer.append(Constants.SEPARATOR);
        return stringBuffer.toString();
    }

    public String getUploadFolderPathWithLocale(String systemDir, String date, String randomToken,
        String locales)
    {
        File systemDirFile = new File(systemDir);
        StringBuffer stringBuffer = new StringBuffer(systemDirFile.getParent());
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(Constants.TOOL);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(Constants.UPLOADDIR);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(date);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(randomToken);
        stringBuffer.append(Constants.SEPARATOR);
        stringBuffer.append(locales);
        stringBuffer.append(Constants.SEPARATOR);
        return stringBuffer.toString();
    }
}
