package org.gs4tr.tmserver.tool.service.tmx;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.dom4j.DocumentException;
import org.gs4tr.tmserver.tool.model.TmLocale;
import org.xml.sax.SAXException;

public class TmxLocale extends TmLocale
{
    public TmxLocale(String filePath) throws IOException, SAXException, FileNotFoundException, DocumentException
    {
        TmxLocaleReader reader = new TmxLocaleReader();
        reader.readFile(filePath);

        setSourceLocale(reader.getSourceLanguage());
        setTargetLocale(reader.getTargetLanguage());
    }
}
