package org.gs4tr.tmserver.tool.service.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.foundation3.xml.XmlUtils;
import org.gs4tr.tmserver.tool.model.bilingual.TencentTu;
import org.gs4tr.tmserver.tool.model.tmx.common.Tu;
import org.gs4tr.tmserver.tool.model.tmx.common.Tuv;
import org.gs4tr.tmserver.tool.service.excel.impl.ExcelReaderImpl;
import org.gs4tr.tmserver.tool.service.tmx.EditUtil;
import org.gs4tr.tmserver.tool.service.tmx.io.TmxExporterWriter;

public class ConvertExcelToTmx
{
    static public final String PROJECT = "Project";
    static public final String PRODUCT = "Product";
    static public final String TYPE = "Type";
    static public final String JSON_CONTEXT = "json-context";

    private String excelFilePath;
    private String targetFilePath;
    private String sourceLocale;
    private String targetLocale;

    public ConvertExcelToTmx(String excelFilePath, String targetFilePath, String sourceLocale, String targetLocale)
    {
        this.excelFilePath = excelFilePath;
        this.targetFilePath = targetFilePath;
        this.sourceLocale = sourceLocale;
        this.targetLocale = targetLocale;
    }

    public void processTencentFile() throws IOException
    {
        ExcelReader excelReader = new ExcelReaderImpl();
        List<TencentTu> tencentTuList = excelReader.readTencentExcel(excelFilePath);
        List<Tu> tus = convertTencentTusToTus(tencentTuList);
        outputFile(tus);
    }

    private void outputFile(List<Tu> tus) throws IOException
    {
        if (tus == null || tus.isEmpty())
        {
            return;
        }

        TmxExporterWriter writer = null;
        try
        {
            Locale srcLocale = Locale.makeLocale(sourceLocale);
            Locale tgtLocale = Locale.makeLocale(targetLocale);
            writer = new TmxExporterWriter(srcLocale, tgtLocale, targetFilePath);
            for (Tu tu : tus)
            {
                writer.writeSegment(tu);
            }
        }
        finally
        {
            if (writer != null)
            {
                writer.close();
            }
        }
    }

    private List<Tu> convertTencentTusToTus(List<TencentTu> tencentTuList)
    {
        Date today = new Date();
        List<Tu> tuList = new ArrayList<>();
        for (TencentTu tencentTu : tencentTuList)
        {
            // skip empty source or target content:
            if (tencentTu.getSource() == null || tencentTu.getTarget() == null)
            {
                continue;
            }
            Tu tu = new Tu();

            // add project:
            String project = tencentTu.getProject();
            if (project != null)
            {
                tu.addProp("<prop type=\"" + XmlUtils.encodeXmlEntitiesFull(PROJECT) + "\">" +
                    XmlUtils.encodeXmlEntities(project) + "</prop>");
            }
            // add product:
            String product = tencentTu.getProduct();
            if (product != null)
            {
                tu.addProp("<prop type=\"" + XmlUtils.encodeXmlEntitiesFull(PRODUCT) + "\">" +
                    XmlUtils.encodeXmlEntities(product) + "</prop>");
            }
            // add type:
            String type = tencentTu.getType();
            if (type != null)
            {
                tu.addProp("<prop type=\"" + XmlUtils.encodeXmlEntitiesFull(TYPE) + "\">" +
                    XmlUtils.encodeXmlEntities(type) + "</prop>");
            }
            // add metadata:
            String metadata = tencentTu.getMetadata();
            if (metadata != null)
            {
                tu.addProp("<prop type=\"" + XmlUtils.encodeXmlEntitiesFull(JSON_CONTEXT) + "\">" +
                    XmlUtils.encodeXmlEntities(metadata) + "</prop>");
            }

            Tuv sourceTuv = new Tuv();
            sourceTuv.setCreationDate(today);
            sourceTuv.setChangeDate(today);
            sourceTuv.setUsageCount("0");
            sourceTuv.setLanguage(sourceLocale);
            String source = EditUtil.fixInvalidXmlChars(XmlUtils.encodeXmlEntitiesFull(tencentTu.getSource()));
            sourceTuv.setSegment("<seg>" + source + "</seg>");
            tu.addTuv(sourceTuv);

            Tuv targetTuv = new Tuv();
            targetTuv.setCreationDate(today);
            targetTuv.setChangeDate(today);
            targetTuv.setUsageCount("0");
            targetTuv.setLanguage(targetLocale);
            String target = XmlUtils.encodeXmlEntitiesFull(tencentTu.getTarget());
            targetTuv.setSegment("<seg>" + EditUtil.fixInvalidXmlChars(target) + "</seg>");
            tu.addTuv(targetTuv);

            tuList.add(tu);
        }
        return tuList;
    }

    public String getExcelFilePath()
    {
        return excelFilePath;
    }

    public void setExcelFilePath(String excelFilePath)
    {
        this.excelFilePath = excelFilePath;
    }

    public String getTargetFilePath()
    {
        return targetFilePath;
    }

    public void setTargetFilePath(String targetFilePath)
    {
        this.targetFilePath = targetFilePath;
    }

    public String getSourceLocale()
    {
        return sourceLocale;
    }

    public void setSourceLocale(String sourceLocale)
    {
        this.sourceLocale = sourceLocale;
    }

    public String getTargetLocale()
    {
        return targetLocale;
    }

    public void setTargetLocale(String targetLocale)
    {
        this.targetLocale = targetLocale;
    }

}
