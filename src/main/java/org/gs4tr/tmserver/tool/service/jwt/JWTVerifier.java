package org.gs4tr.tmserver.tool.service.jwt;

import org.apache.commons.codec.binary.Base64;
import org.gs4tr.tmserver.tool.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@Component
public class JWTVerifier
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JWTVerifier.class);

    public static final String JWT_AUDIENCE_PARAM = "aud";
    private static final String JWT_EXPIRATION_PARAM = "exp";
    public static final String JWT_SUBJECT = "sub";
    public static final String JWT_AUTHORITIES = "authorities";

    @Autowired
    private ObjectMapper mapper;

    public Map<String, Object> parsePayload(String accessToken)
    {
        final String[] pieces = accessToken.split("\\.");
        if (pieces.length != 3)
        {
            throw new IllegalStateException("Parse payload - wrong number of segments: " + pieces.length);
        }

        Map<String, Object> jwtPayload = decodeAndParseToken(pieces[1]);
        return jwtPayload;
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> decodeAndParseToken(String b64String)
    {
        try
        {
            String jsonString = new String(Base64.decodeBase64(b64String), StandardCharsets.UTF_8);
            return mapper.readValue(jsonString, Map.class);
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public boolean isTokenExpired(String accessToken)
    {
        if (StringUtils.isEmpty(accessToken))
        {
            LOGGER.error(Messages.getString("JWTVerifier.0"));
            return true;
        }
        final String[] pieces = accessToken.split("\\.");
        if (pieces.length != 3)
        {
            throw new IllegalStateException("Parse payload - wrong number of segments: " + pieces.length);
        }
        Map<String, Object> jwtPayload = decodeAndParseToken(pieces[1]);
        return isTokenExpired(jwtPayload);
    }

    private boolean isTokenExpired(Map<String, Object> jwtPayload)
    {
        final long expiration = Long.valueOf(jwtPayload.get(JWT_EXPIRATION_PARAM).toString());
        long serverTime = System.currentTimeMillis() / 1000L;
        String subject = jwtPayload.get(JWT_SUBJECT).toString();
        if (expiration != 0 && serverTime >= expiration)
        {
            String message = String.format(Messages.getString("JWTVerifier.0"), expiration, serverTime, subject);
            LOGGER.warn(message);
            return true;
        }
        return false;
    }
}
