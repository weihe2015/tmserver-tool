package org.gs4tr.tmserver.tool.service.tmx.io;

import java.io.IOException;
import java.io.Writer;
import java.util.Date;

import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.tmserver.tool.model.tmx.common.Constants;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxConstants;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxHeader;

public class TmxStreamExporterWriter extends AbstractStreamExporterWriter
{
    public TmxStreamExporterWriter(Locale srcLocale, Locale tgtLocale, Writer w)
    {
        super(srcLocale, tgtLocale, w);
    }

    protected TmxHeader createHeader()
    {
        TmxHeader header = new TmxHeader();
        header.setTmxVersion("1.4");
        header.setAdminLang("EN-US");
        header.setCreationDate(new Date());
        header.setCreationTool(Constants.AP_CREATIONTOOL);
        header.setCreationToolVersion(Constants.AP_CREATIONTOOL_VERSION);
        header.setSegmentationType(TmxConstants.SEGMENTATION_SENTENCE);
        header.setOriginalFormat("unknown");
        header.setDatatype("unknown");
        header.setSourceLang(_tmSrcLocale.normalizeTmxLocale());

        return header;
    }

    @Override
    protected void writeEnd() throws IOException
    {
        _writer.write("</" + TmxConstants.BODY + ">");
        _writer.write(EOL);
        _writer.write("</" + TmxConstants.ROOT + ">");
        _writer.write(EOL);
    }

    @Override
    protected void writeHeader() throws IOException
    {
        TmxHeader header = createHeader();

        _writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
        _writer.write(EOL);
        _writer.write(header.getTmxDeclaration());
        _writer.write(EOL);
        _writer.write(header.getTmxXml());
        _writer.write(EOL);
        _writer.write(header.getHeaderXml());
        _writer.write("<" + TmxConstants.BODY + ">");
        _writer.write(EOL);
    }
}
