package org.gs4tr.tmserver.tool.service.tmx.io.callback.imp;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxHeader;
import org.gs4tr.tmserver.tool.model.tmx.common.Tu;
import org.gs4tr.tmserver.tool.model.tmx.common.Tuv;
import org.gs4tr.tmserver.tool.service.tmx.io.callback.TmxReaderCallback;

public class TmxReaderCallbackImpl implements TmxReaderCallback
{
    private String sourceLanguage;
    private String targetLanguage;

    @Override
    public String getSourceLanguage()
    {
        return sourceLanguage;
    }

    @Override
    public String getTargetLanguage()
    {
        return targetLanguage;
    }

    @Override
    public void handle(TmxHeader header)
    {
        sourceLanguage = header.getSourceLang();
    }

    @Override
    public boolean handle(Tu tu)
    {
        if (tu.getTuvs().size() != 2)
        {
            return false;
        }

        Tuv sourceTuv = tu.getTuvs().get(0);
        Tuv targetTuv = tu.getTuvs().get(1);

        // if source Tuv is in the first:
        if (StringUtils.equalsIgnoreCase(sourceTuv.getLanguage(), sourceLanguage))
        {
            targetLanguage = targetTuv.getLanguage();
        }
        // If the source Tub is in the second
        else if (StringUtils.equalsIgnoreCase(targetTuv.getLanguage(), sourceLanguage))
        {
            targetLanguage = sourceTuv.getLanguage();
        }
        else
        {
            // source locale does not match the header, will use the one in the Tuv:
            sourceLanguage = sourceTuv.getLanguage();
            targetLanguage = targetTuv.getLanguage();
        }
        return true;
    }
}
