package org.gs4tr.tmserver.tool.service.server;

import org.gs4tr.tmserver.tool.model.server.Tm;
import org.gs4tr.tmserver.tool.model.server.TmProject;

public interface TmService
{
    public Tm findTmById(Long id);

    public TmProject findTmProjectByProjectShortCode(String projectShortCode);

    public void updateTmNameById(Long id, String newName);
}
