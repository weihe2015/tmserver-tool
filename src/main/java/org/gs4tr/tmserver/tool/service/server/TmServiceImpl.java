package org.gs4tr.tmserver.tool.service.server;

import org.gs4tr.tmserver.tool.model.server.Tm;
import org.gs4tr.tmserver.tool.model.server.TmProject;
import org.gs4tr.tmserver.tool.repository.TmProjectRepository;
import org.gs4tr.tmserver.tool.repository.TmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("tmService")
public class TmServiceImpl implements TmService
{
    @Autowired
    private TmRepository tmRepository;

    @Autowired
    private TmProjectRepository tmProjectRepository;

    @Override
    public Tm findTmById(Long id)
    {
        return tmRepository.findOne(id);
    }

    @Override
    public TmProject findTmProjectByProjectShortCode(String projectShortCode)
    {
        return tmProjectRepository.findByShortCode(projectShortCode);
    }

    @Override
    public void updateTmNameById(Long id, String newName)
    {
        Tm prevTm = tmRepository.findOne(id);
        prevTm.setName(newName);
        tmRepository.save(prevTm);
    }
}
