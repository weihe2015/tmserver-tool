package org.gs4tr.tmserver.tool.service.tmx.io.callback;

import org.gs4tr.tmserver.tool.model.tmx.common.TmxHeader;
import org.gs4tr.tmserver.tool.model.tmx.common.Tu;

public interface TmxReaderCallback
{
    String getSourceLanguage();

    String getTargetLanguage();

    void handle(TmxHeader header);

    boolean handle(Tu tu);

}
