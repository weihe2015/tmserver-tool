package org.gs4tr.tmserver.tool.service.tmx;

import java.io.IOException;

import org.gs4tr.tmserver.tool.model.TmLocale;

public class WfTxtLocale extends TmLocale
{
    public WfTxtLocale(String filePath) throws IOException
    {
        WfTxtLocaleReader reader = new WfTxtLocaleReader();
        reader.readFile(filePath);

        setSourceLocale(reader.getSourceLanguage());
        setTargetLocale(reader.getTargetLanguage());
    }

}
