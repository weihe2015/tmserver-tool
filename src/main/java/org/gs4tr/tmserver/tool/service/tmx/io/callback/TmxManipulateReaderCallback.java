package org.gs4tr.tmserver.tool.service.tmx.io.callback;

import org.gs4tr.tmserver.tool.controller.model.TmxManipulateOption;
import org.json.JSONException;

public interface TmxManipulateReaderCallback extends TmxReaderCallback
{
    void setTmxFilename(String name);

    void setTmxManipulateOption(TmxManipulateOption m_option) throws JSONException;
}
