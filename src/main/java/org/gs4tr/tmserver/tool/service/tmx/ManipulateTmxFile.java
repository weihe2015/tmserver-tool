package org.gs4tr.tmserver.tool.service.tmx;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.dom4j.DocumentException;
import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.tmserver.tool.controller.model.TmxManipulateOption;
import org.gs4tr.tmserver.tool.model.tmx.common.Tu;
import org.gs4tr.tmserver.tool.service.tmx.io.TmxExporterWriter;
import org.json.JSONException;
import org.xml.sax.SAXException;

public class ManipulateTmxFile
{
    private String tmxFile;
    private String targetFileName;
    private TmxManipulateOption tmxManipulateOption;
    private Locale sourceLocale;
    private Locale targetLocale;

    public ManipulateTmxFile(String tmxFile, String targetFileName, TmxManipulateOption tmxManipulateOption)
    {
        this.tmxFile = tmxFile;
        this.targetFileName = targetFileName;
        this.tmxManipulateOption = tmxManipulateOption;
    }

    @SuppressWarnings("unused")
    private void outputFile(List<Tu> tus) throws IOException
    {
        if (tus == null || tus.isEmpty())
        {
            return;
        }

        TmxExporterWriter writer = null;
        try
        {
            writer = new TmxExporterWriter(sourceLocale, targetLocale, targetFileName);
            for (Tu tu : tus)
            {
                writer.writeSegment(tu);
            }
        }
        finally
        {
            if (writer != null)
            {
                writer.close();
            }
        }
    }

    public void process() throws IOException, SAXException, FileNotFoundException,
        DocumentException, JSONException
    {
        TmxReader reader = new TmxReader();
        reader.setTmxManipulateOption(tmxManipulateOption);
        reader.setTargetFileName(targetFileName);
        reader.readFile(tmxFile);
    }
}
