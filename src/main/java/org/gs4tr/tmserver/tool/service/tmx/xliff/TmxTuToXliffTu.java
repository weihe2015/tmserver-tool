package org.gs4tr.tmserver.tool.service.tmx.xliff;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Attribute;
import org.dom4j.Element;
import org.gs4tr.foundation3.xml.XmlUtils;
import org.gs4tr.tm3.api.XliffConstants;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxConstants;
import org.gs4tr.tmserver.tool.model.tmx.common.Tu;
import org.gs4tr.tmserver.tool.model.tmx.common.Tuv;

/**
 * Converts a TU in TMX format to a TU in XLIFF format.
 * <p>
 * Used for internal processing during TMX imports into XLIFF TMs.
 */
public class TmxTuToXliffTu
{
    private static final Log _logger = LogFactory.getLog(TmxTuToXliffTu.class);

    public TmxTuToXliffTu()
    {
    }

    /**
     * Call this first to remove unwanted HI elements that can appear anywhere in the segments. Note
     * this destructively alters the TU.
     */
    public void removeHiElements(Tu tu)
    {
        List<Tuv> tuvs = tu.getTuvs();

        for (Tuv tuv : tuvs)
        {
            XmlUtils.removeElementsPullupContent(tuv.getSegment(), TmxConstants.HI);
        }
    }

    /**
     * Converts a TU in TMX format to a TU in XLIFF format.
     */
    public void tmxToXliff(Tu tu)
    {
        List<Tuv> tuvs = tu.getTuvs();

        for (Tuv tuv : tuvs)
        {
            tmxToXliff(tuv);
        }
    }

    /**
     * Extracts matching sub-segments as new TUs, if possible.
     * <p>
     * Note this destructively alters the TU by removing any &lt;sub> element whether processed or not,
     * so that the import of the main TU succeeds.
     * <p>
     * Note: subs are assumed to contain no internal tags.
     */
    @SuppressWarnings("unchecked")
    public List<Tu> extractSubs(Tu tu)
    {
        ArrayList<Tu> result = new ArrayList<Tu>();

        Tuv srcTuv = tu.getTuvs().get(0);
        Tuv trgTuv = tu.getTuvs().get(1);
        Element srcSeg = srcTuv.getSegment();
        Element trgSeg = trgTuv.getSegment();

        if (srcSeg.selectSingleNode(".//" + TmxConstants.SUB) == null)
        {
            return result;
        }

        List<Element> tags = srcSeg.elements();
        for (Element srcTag : tags)
        {
            Element trgTag = getMatchingTag(srcTag, trgSeg);

            if (trgTag == null)
            {
                continue;
            }

            List<Element> srcSubs = srcTag.selectNodes(".//" + TmxConstants.SUB);
            List<Element> trgSubs = trgTag.selectNodes(".//" + TmxConstants.SUB);

            // Subs in the same tag appear in the same order (tag content is not editable).
            // Check for errors nonetheless.
            if (srcSubs.size() != trgSubs.size())
            {
                continue;
            }

            for (int i = 0, max = srcSubs.size(); i < max; i++)
            {
                Element srcSub = srcSubs.get(i);
                Element trgSub = trgSubs.get(i);

                Tu newTu = new Tu();
                Tuv newSrcTuv = new Tuv();
                newSrcTuv.setSegment(srcSub);
                newSrcTuv.setLanguage(srcTuv.getLanguage());
                Tuv newTrgTuv = new Tuv();
                newTrgTuv.setSegment(trgSub);
                newTrgTuv.setLanguage(trgTuv.getLanguage());
                newTu.addTuv(newSrcTuv);
                newTu.addTuv(newTrgTuv);

                // System.err.println("New SUB TU: " + newTu.getXml());

                result.add(newTu);
            }
        }

        // TODO: figure out what happens to <sub>s. They may have to be preserved.
        // For now, remove all subs.
        List<Element> nukableSubs = srcSeg.selectNodes(".//" + TmxConstants.SUB);
        for (Element nukableSub : nukableSubs)
        {
            nukableSub.detach();
        }
        nukableSubs = trgSeg.selectNodes(".//" + TmxConstants.SUB);
        for (Element nukableSub : nukableSubs)
        {
            nukableSub.detach();
        }

        return result;
    }

    // TODO: the fixing matrix is a bit more complex than thought. Must be completed.
    public void tmxToXliff(Tuv tuv)
    {
        Element seg = tuv.getSegment();
        tmxToXliff(seg);
        tuv.setSegment(seg);
    }

    @SuppressWarnings("unchecked")
    public boolean tmxToXliff(Element seg)
    {
        if (_logger.isDebugEnabled())
        {
            // _logger.debug("TMX : " + seg.asXML());
        }

        int unassignedIdValue = getHighestIdValue(seg);

        List<Element> tags = seg.elements();
        for (Element tag : tags)
        {
            boolean isEptEx = false;

            // No UT in XLIFF, use PH.
            if (tag.getName().equals(TmxConstants.UT))
            {
                tag.setName(XliffConstants.PH);
                if (!tag.hasContent())
                {
                    tag.setName(XliffConstants.X);
                }
            }
            else if (tag.getName().equals(TmxConstants.PH) && !tag.hasContent())
            {
                tag.setName(XliffConstants.X);
            }
            else if (tag.getName().equals(TmxConstants.BPT) && !tag.hasContent())
            {
                // TODO: assuming corresponding EPT is empty too
                tag.setName(XliffConstants.BX);
            }
            else if (tag.getName().equals(TmxConstants.EPT))
            {
                if (!tag.hasContent())
                {
                    // TODO: assuming corresponding BPT is empty too
                    tag.setName(XliffConstants.EX);
                }
                isEptEx = true;
            }

            // x -> id (mandatory)
            Attribute xAttr = tag.attribute(TmxConstants.X);
            if (xAttr != null)
            {
                tag.remove(xAttr);
                tag.addAttribute(XliffConstants.ID, xAttr.getValue());
            }
            else
            {
                tag.addAttribute(XliffConstants.ID, String.valueOf(unassignedIdValue++));
            }

            // i -> rid (mandatory on bpt/ept)
            Attribute iAttr = tag.attribute(TmxConstants.I);
            if (iAttr != null)
            {
                tag.remove(iAttr);
                tag.addAttribute(XliffConstants.RID, iAttr.getValue());
            }

            // pos=begin|end -> pos=open|close
            Attribute posAttr = tag.attribute(TmxConstants.POS);
            if (posAttr != null)
            {
                if (TmxConstants.POS_BEGIN.equals(posAttr.getValue()))
                {
                    posAttr.setText(XliffConstants.POS_OPEN);
                }
                else
                {
                    posAttr.setText(XliffConstants.POS_CLOSE);
                }
            }

            // assoc=p|f|b -> assoc=preceding|following|both
            Attribute assocAttr = tag.attribute(TmxConstants.ASSOC);
            if (assocAttr != null)
            {
                if (TmxConstants.ASSOC_PRECEDING.equals(assocAttr.getValue()))
                {
                    assocAttr.setText(XliffConstants.ASSOC_PRECEDING);
                }
                else if (TmxConstants.ASSOC_FOLLOWING.equals(assocAttr.getValue()))
                {
                    assocAttr.setText(XliffConstants.ASSOC_FOLLOWING);
                }
                else
                {
                    assocAttr.setText(XliffConstants.ASSOC_BOTH);
                }
            }

            // type -> ctype (optional in standard, mandatory in TM3; except for ept/ex)
            if (isEptEx)
            {
                // ctype is inherited from bpt/bx
                tag.addAttribute(TmxConstants.TYPE, null);
            }
            else
            {
                Attribute typeAttr = tag.attribute(TmxConstants.TYPE);
                if (typeAttr != null)
                {
                    tag.remove(typeAttr);
                    tag.addAttribute(XliffConstants.CTYPE, mapTmxType(typeAttr.getValue()));
                }
                else
                {
                    tag.addAttribute(XliffConstants.CTYPE, "x-unknown");
                }
            }
        }

        if (_logger.isDebugEnabled())
        {
            // _logger.debug("XLIFF: " + seg.asXML());
        }
        // System.err.println("XLIFF: " + seg.asXML());
        return true;
    }

    private String mapTmxType(String type)
    {
        // XLIFF x and ph elements:
        // image Indicates a inline image.
        // pb Indicates a page break.
        // lb Indicates a line break.
        //
        // other elements:
        // bold Indicates a run of bolded text.
        // italic Indicates a run of text in italics.
        // underlined Indicates a run of underlined text.
        // link Indicates a run of hyper-text.

        // See http://docs.oasis-open.org/xliff/v1.2/os/xliff-core.html#ctype.
        // See http://www.lisa.org/fileadmin/standards/tmx1.4/tmx.htm#type.

        if (StringUtils.isEmpty(type))
        {
            return "x-unknown";
        }

        if (type.startsWith("x-") ||
            ArrayUtils.contains(XliffConstants.predefinedXliffCtypes, type))
        {
            return type;
        }

        if (type.equals("ulined"))
        {
            return "underlined";
        }

        return "x-" + type;
    }

    /**
     * Returns the highest ID used in a segment so that tags with no ID can be assigned an unused one.
     */
    @SuppressWarnings("unchecked")
    private int getHighestIdValue(Element seg)
    {
        int result = 0;

        List<Element> tags = seg.elements();
        for (Element tag : tags)
        {
            Attribute xAttr = tag.attribute(TmxConstants.X);
            if (xAttr != null)
            {
                result = Math.max(result, Integer.parseInt(xAttr.getValue()));
            }
        }

        return result + 1;
    }

    /** Returns the matching tag in target for a tag in source using the "x" attribute linking. */
    private Element getMatchingTag(Element srcTag, Element trgSeg)
    {
        String xAttr = srcTag.attributeValue(TmxConstants.X);

        if (StringUtils.isBlank(xAttr))
        {
            return null;
        }

        Element result = (Element) trgSeg.selectSingleNode(".//*[@" + TmxConstants.X + "='" + xAttr + "']");

        return result;
    }
}
