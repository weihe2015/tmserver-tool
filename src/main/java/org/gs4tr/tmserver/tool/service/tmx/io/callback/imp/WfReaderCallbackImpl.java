package org.gs4tr.tmserver.tool.service.tmx.io.callback.imp;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.model.wf.common.TU;
import org.gs4tr.tmserver.tool.model.wf.common.WordFastHeader;
import org.gs4tr.tmserver.tool.service.tmx.io.callback.WfReaderCallback;

public class WfReaderCallbackImpl implements WfReaderCallback
{
    private String sourceLanguage;
    private String targetLanguage;
    private int tuCount;
    private boolean ignorePrivateTus = false;

    @Override
    public String getSourceLanguage()
    {
        return sourceLanguage;
    }

    @Override
    public String getTargetLanguage()
    {
        return targetLanguage;
    }

    @Override
    public boolean handle(TU tu)
    {
        tuCount++;
        sourceLanguage = tu.getSourceLocale();
        targetLanguage = tu.getTargetLocale();

        if (StringUtils.isBlank(tu.getSource()) || StringUtils.isBlank(tu.getTarget()))
        {
            return false;
        }

        // TMA-1066 limits creation user to 255 varchars.
        if (tu.getCreationUser().length() > 255)
        {
            return false;
        }

        if (ignorePrivateTus && StringUtils.isNotBlank(tu.getWorkgroup()))
        {
            return false;
        }
        return true;
    }

    @Override
    public void handle(WordFastHeader header)
    {
        sourceLanguage = header.getSourceLocale();
        targetLanguage = header.getTargetLocale();
    }

    @Override
    public void invalidSegment(String segment)
    {
    }

}
