package org.gs4tr.tmserver.tool.service.tmx;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;
import org.dom4j.io.SAXReader;
import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.tmserver.tool.controller.model.TmxManipulateOption;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxConstants;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxHeader;
import org.gs4tr.tmserver.tool.model.tmx.common.Tu;
import org.gs4tr.tmserver.tool.service.tmx.io.MonitoringFileInputStream;
import org.gs4tr.tmserver.tool.service.tmx.io.TmxExporterWriter;
import org.gs4tr.tmserver.tool.service.tmx.io.callback.TmxManipulateReaderCallback;
import org.gs4tr.tmserver.tool.service.tmx.io.callback.imp.TmxManipulateReaderCallbackImpl;
import org.json.JSONException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class TmxReader implements TmxConstants
{
    public class TmxHeaderElementHandler implements ElementHandler
    {
        @Override
        public void onEnd(ElementPath path)
        {
            Element element = path.getCurrent();

            m_header = new TmxHeader(element);
            m_header.setTmxVersion(m_tmxVersion);

            if (m_callback != null)
            {
                m_callback.handle(m_header);
            }
            // prune the current element to reduce memory
            element.detach();
        }

        @Override
        public void onStart(ElementPath path)
        {
        }
    }

    private String m_tmxVersion;
    private String m_fileName;
    private List<Tu> m_tus;

    private TmxHeader m_header;
    private TmxManipulateReaderCallback m_callback;
    private TmxManipulateOption m_option;
    private String m_targetFileName;
    private TmxExporterWriter m_writer = null;
    private Locale m_sourceLocale = null;
    private Locale m_targetLocale = null;

    public TmxReader()
    {
        m_tus = new ArrayList<Tu>();
    }

    public String getFileName()
    {
        return m_fileName;
    }

    public TmxHeader getHeader()
    {
        return m_header;
    }

    public String getSourceLanguage()
    {
        return m_callback.getSourceLanguage();
    }

    public String getTargetLanguage()
    {
        return m_callback.getTargetLanguage();
    }

    public String getTmxVersion()
    {
        return m_tmxVersion;
    }

    public List<Tu> getTus()
    {
        return this.m_tus;
    }

    /**
     * Reads and validates a TMX XML string.
     */
    private void parseFile(SAXReader p_reader, InputSource p_input, MonitoringFileInputStream p_stream)
        throws DocumentException
    {
        SAXReader reader = p_reader;
        final MonitoringFileInputStream stream = p_stream;

        // enable element complete notifications to conserve memory
        reader.addHandler("/" + TmxConstants.ROOT, new ElementHandler()
        {
            @Override
            final public void onEnd(ElementPath path)
            {
            }

            @Override
            final public void onStart(ElementPath path)
            {
                Element element = path.getCurrent();

                m_tmxVersion = element.attributeValue("version");
            }
        });

        reader.addHandler("/" + TmxConstants.ROOT + "/" + TmxConstants.HEADER, new ElementHandler()
        {
            @Override
            final public void onEnd(ElementPath path)
            {
                Element element = path.getCurrent();

                m_header = new TmxHeader(element);
                m_header.setTmxVersion(m_tmxVersion);

                if (m_callback != null)
                {
                    m_callback.handle(m_header);
                }
                // prune the current element to reduce memory
                element.detach();
            }

            @Override
            final public void onStart(ElementPath path)
            {
            }
        });
        // enable element complete notifications to conserve memory
        reader.addHandler("/" + TmxConstants.ROOT + "/" + TmxConstants.BODY + "/" + TmxConstants.TU,
            new ElementHandler()
            {
                @SuppressWarnings("unused")
                int tuCount = 0;
                int invalidTuCount = 0;

                @Override
                final public void onEnd(ElementPath path)
                {
                    shout();
                    Element element = path.getCurrent();

                    if (m_callback != null)
                    {
                        Tu tu = new Tu(element);
                        // Skip invalid TU for now
                        if (m_callback.handle(tu))
                        {
                            if (m_sourceLocale == null)
                            {
                                m_sourceLocale = Locale.makeLocale(m_callback.getSourceLanguage());
                            }
                            if (m_targetLocale == null)
                            {
                                m_targetLocale = Locale.makeLocale(m_callback.getTargetLanguage());
                            }
                            if (m_sourceLocale != null && m_targetLocale != null && m_writer == null)
                            {
                                m_writer = new TmxExporterWriter(m_sourceLocale, m_targetLocale, m_targetFileName);
                            }
                            if (m_writer != null)
                            {
                                m_writer.writeSegment(tu);
                            }
                            // m_tus.add(tu);
                        }
                        else
                        {
                            invalidTuCount += 1;
                        }
                    }
                    // prune the current element to reduce memory
                    element.detach();
                }

                @Override
                final public void onStart(ElementPath path)
                {
                }

                @SuppressWarnings("unused")
                final private void shout()
                {
                    tuCount++;
                    // int prec = (int) (stream.getCounter() * 100 / stream.getTotal());
                    // System.out.println(prec);
                }

            });
        reader.read(p_input);
        if (m_writer != null)
        {
            m_writer.close();
        }
        // all done.
    }

    public void readFile(String p_fileName)
        throws SAXException, FileNotFoundException, DocumentException, JSONException
    {
        m_fileName = p_fileName;
        m_callback = new TmxManipulateReaderCallbackImpl();
        m_callback.setTmxManipulateOption(m_option);
        m_callback.setTmxFilename(FilenameUtils.getName(p_fileName));

        SAXReader reader = new SAXReader();
        reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
        reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        reader.setFeature("http://apache.org/xml/features/validation/dynamic", false);
        reader.setEntityResolver(new TmxDtdResolver());
        reader.setValidation(false);

        MonitoringFileInputStream stream = new MonitoringFileInputStream(p_fileName);
        InputSource src = new InputSource(stream);
        parseFile(reader, src, stream);
    }

    public void setTmxManipulateOption(TmxManipulateOption p_option)
    {
        m_option = p_option;
    }

    public void setTargetFileName(String p_targetFileName)
    {
        m_targetFileName = p_targetFileName;
    }
}
