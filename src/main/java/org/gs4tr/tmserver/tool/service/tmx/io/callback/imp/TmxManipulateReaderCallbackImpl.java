package org.gs4tr.tmserver.tool.service.tmx.io.callback.imp;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.Text;
import org.gs4tr.filters3.extraction.ExtractionSupport;
import org.gs4tr.filters3.extraction.ExtractionSupportFactory;
import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.foundation3.crypt.Md5Checksum;
import org.gs4tr.foundation3.placeables.PlaceableCreator;
import org.gs4tr.foundation3.placeables.PlaceableManager;
import org.gs4tr.foundation3.placeables.TextHolder;
import org.gs4tr.foundation3.xliff.XliffUtils;
import org.gs4tr.foundation3.xliff.placeables.XliffPlaceableCreator;
import org.gs4tr.foundation3.xml.XmlParser;
import org.gs4tr.foundation3.xml.XmlUtils;
import org.gs4tr.tm3.api.XliffConstants;
import org.gs4tr.tmserver.tool.controller.model.TmxManipulateOption;
import org.gs4tr.tmserver.tool.model.tmx.NewAttribute;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxConstants;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxHeader;
import org.gs4tr.tmserver.tool.model.tmx.common.Tu;
import org.gs4tr.tmserver.tool.model.tmx.common.Tuv;
import org.gs4tr.tmserver.tool.service.tmx.io.callback.TmxManipulateReaderCallback;
import org.gs4tr.tmserver.tool.service.tmx.xliff.TmxTuToXliffTu;
import org.json.JSONException;

public class TmxManipulateReaderCallbackImpl implements TmxManipulateReaderCallback
{
    static private final Pattern MD5_CHECKSUM_NORMALIZER = Pattern.compile("[\\s\u00a0]+");
    static private final String CONTEXT_PRE_TYPE1 = "x-context-pre";
    static private final String CONTEXT_POST_TYPE1 = "x-context-post";

    static private final String CONTEXT_PRE_TYPE2 = "context_prev";
    static private final String CONTEXT_POST_TYPE2 = "context_next";

    static private final String TYPE = "type";

    static private final String PREV_MD5_CHECKSUM = "previousMd5Checksum";
    static private final String NEXT_MD5_CHECKSUM = "nextMd5Checksum";
    static private final String PREV_MD5_SEGMENT = "previousMd5Segment";
    static private final String NEXT_MD5_SEGMENT = "nextMd5Segment";

    // set of attribute names that we want to keep from TM migration
    static private Set<String> AWS_TMX_ATTRIBUTES = new HashSet<>();

    private String sourceLanguage;
    private String targetLanguage;
    private Locale srcLocale;
    private String m_tmxFilename;
    private TmxManipulateOption m_option;
    private Map<String, String> newAttributesMap;
    private TmxTuToXliffTu converter;
    private Configuration config;
    private ExtractionSupport extractionSupport;

    static
    {
        AWS_TMX_ATTRIBUTES.add("filename");
        AWS_TMX_ATTRIBUTES.add("aligned");
        AWS_TMX_ATTRIBUTES.add("mdata");
        AWS_TMX_ATTRIBUTES.add("context_prev");
        AWS_TMX_ATTRIBUTES.add("context_next");
    }

    @Override
    public String getSourceLanguage()
    {
        return sourceLanguage;
    }

    @Override
    public String getTargetLanguage()
    {
        return targetLanguage;
    }

    @Override
    public void handle(TmxHeader header)
    {
        sourceLanguage = header.getSourceLang();
    }

    @Override
    public boolean handle(Tu tu)
    {
        if (tu.getTuvs().size() != 2)
        {
            return false;
        }

        Tuv sourceTuv = tu.getTuvs().get(0);
        Tuv targetTuv = tu.getTuvs().get(1);

        // If source Tuv is in the first:
        if (StringUtils.equalsIgnoreCase(sourceTuv.getLanguage(), sourceLanguage))
        {
            targetLanguage = targetTuv.getLanguage();
        }
        // If the source Tub is in the second
        else if (StringUtils.equalsIgnoreCase(targetTuv.getLanguage(), sourceLanguage))
        {
            targetLanguage = sourceTuv.getLanguage();
        }
        else
        {
            // source locale does not match the header, will use the one in the Tuv:
            sourceLanguage = sourceTuv.getLanguage();
            targetLanguage = targetTuv.getLanguage();
        }

        srcLocale = Locale.makeLocale(sourceLanguage);
        if (srcLocale == null)
        {
            srcLocale = Locale.US;
        }

        config = new BaseConfiguration();
        extractionSupport = new ExtractionSupportFactory().getExtractionSupport(srcLocale, null, config);
        converter = new TmxTuToXliffTu();

        // Validate source and target segment to see if it is valid XLIFF.
        boolean isValidSeg = validateSegElements(sourceTuv.getSegment(), targetTuv.getSegment());
        if (!isValidSeg)
        {
            return false;
        }

        if (m_option.isRemovePreviousAttribute())
        {
            tu.clearProps();
        }

        // TMSTool-76, insert computed prev and next checksum to tu tag.
        if (m_option.isInsertChecksum())
        {
            converter = new TmxTuToXliffTu();
            insertChecksum(sourceTuv);
        }

        // TMSTool-85: move <props> under <tuv> to <tu>
        if (m_option.isMoveAttributesToTu())
        {
            movePropsToTu(tu, sourceTuv, targetTuv);
        }

        if (m_option.isAddFilenameAsAttribute())
        {
            newAttributesMap.put("filename", m_tmxFilename);
        }

        for (String attributeName : newAttributesMap.keySet())
        {
            if (StringUtils.isNotBlank(attributeName))
            {
                String attributeValue = newAttributesMap.get(attributeName);
                addPropToTu(tu, attributeName, attributeValue);
            }
        }

        if (m_option.isRemoveTrailingSpaces())
        {
            Element sourceSegment = sourceTuv.getSegment();
            processSegElement(sourceSegment);
            sourceTuv.setSegment(sourceSegment);

            Element targetSegment = targetTuv.getSegment();
            processSegElement(targetSegment);
            targetTuv.setSegment(targetSegment);
        }

        clearChecksumContextOnAttributeMap();
        sourceTuv.clearProps();
        targetTuv.clearProps();
        return true;
    }

    private boolean validateSegElements(Element srcSegment, Element tgtSegment)
    {
        if (!validateSegElement(srcSegment))
        {
            return false;
        }
        return validateSegElement(tgtSegment);
    }

    @SuppressWarnings("unchecked")
    private boolean validateSegElement(Element seg)
    {
        Element cloneSeg = (Element) seg.clone();
        converter.tmxToXliff(cloneSeg);

        if (!cloneSeg.hasContent())
        {
            return true;
        }
        Stack<Element> stack = new Stack<>();
        List<Element> tags = cloneSeg.elements();
        for (Element tag : tags)
        {
            if (tag.getName().equals(XliffConstants.X) ||
                tag.getName().equals(XliffConstants.PH))
            {
                continue;
            }
            else if (tag.getName().equals(TmxConstants.BPT) ||
                tag.getName().equals(XliffConstants.BX))
            {
                // Check if it has id and rid:
                Attribute iAttr = tag.attribute(XliffConstants.ID);
                Attribute ridAttr = tag.attribute(XliffConstants.RID);
                if (iAttr == null || ridAttr == null)
                {
                    return false;
                }
                stack.push(tag);
            }
            else if (tag.getName().equals(TmxConstants.EPT) ||
                tag.getName().equals(XliffConstants.EX))
            {
                Attribute iAttr = tag.attribute(XliffConstants.ID);
                Attribute ridAttr = tag.attribute(XliffConstants.RID);
                if (iAttr == null || ridAttr == null)
                {
                    return false;
                }
                // check if it matches the opening tag.
                if (stack.isEmpty())
                {
                    return false;
                }
                Element openTag = stack.peek();
                if (!openTag.getName().equals(TmxConstants.BPT) &&
                    !openTag.getName().equals(XliffConstants.BX))
                {
                    return false;
                }
                String rid = openTag.attribute(XliffConstants.RID).getText();
                if (!ridAttr.getText().equals(rid))
                {
                    return false;
                }
                stack.pop();
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    private void processSegElement(Element element)
    {
        List<Node> content = element.content();
        if (content.isEmpty())
        {
            return;
        }
        Node lastNode = content.get(content.size() - 1);

        if (lastNode.getNodeType() == Node.TEXT_NODE)
        {
            Text oldNode = (Text) lastNode;
            oldNode.setText(trimTrailingSpaces(oldNode.getText()));
        }
    }

    @Override
    public void setTmxFilename(String p_TmxFilename)
    {
        m_tmxFilename = p_TmxFilename;
    }

    @Override
    public void setTmxManipulateOption(TmxManipulateOption p_option) throws JSONException
    {
        m_option = p_option;
        newAttributesMap = new LinkedHashMap<String, String>();
        if (p_option.getNewAttributes() != null && p_option.getNewAttributes().size() > 0)
        {
            List<NewAttribute> newAttributes = p_option.getNewAttributes();
            for (NewAttribute newAttribute : newAttributes)
            {
                if (!newAttribute.isHide())
                {
                    String key = newAttribute.getKey();
                    if (StringUtils.isNotBlank(key) && StringUtils.isNotEmpty(key))
                    {
                        String value = newAttribute.getValue();
                        newAttributesMap.put(key, value);
                    }
                }
            }
        }
    }

    private String trimTrailingSpaces(String str)
    {
        if (str == null)
        {
            return null;
        }
        int len = str.length();
        char[] cList = str.toCharArray();
        for (; len > 0; len--)
        {
            char c = cList[len - 1];
            // TMS-TOOL-23: add more whitespace characters to remove from segment
            // List: http://jkorpela.fi/chars/spaces.html
            if (Character.isWhitespace(c) || c == '\u00A0' || c == '\u1680' || c == '\u180E' ||
                c == '\u2000' || c == '\u2001' || c == '\u2002' || c == '\u2003' || c == '\u2004' ||
                c == '\u2005' || c == '\u2006' || c == '\u2007' || c == '\u2008' || c == '\u2009' ||
                c == '\u200A' || c == '\u200B' || c == '\u202F' || c == '\u205F' || c == '\u3000' ||
                c == '\uFEFF')
            {
                continue;
            }
            else
            {
                break;
            }
        }
        return str.substring(0, len);
    }

    private void movePropsToTu(Tu tu, Tuv sourceTuv, Tuv targetTuv)
    {
        Map<String, String> attributes = new LinkedHashMap<String, String>();
        // get all props from source tuv:
        for (Element prop : sourceTuv.getProps())
        {
            Attribute contextAttribute = prop.attribute(TYPE);
            String key = contextAttribute.getValue();
            if (!AWS_TMX_ATTRIBUTES.contains(key))
            {
                continue;
            }
            String val = prop.getText();
            attributes.put(key, val);
        }
        // get all props from target tuv:
        for (Element prop : targetTuv.getProps())
        {
            Attribute contextAttribute = prop.attribute(TYPE);
            String key = contextAttribute.getValue();
            if (!AWS_TMX_ATTRIBUTES.contains(key))
            {
                continue;
            }
            String val = prop.getText();
            attributes.put(key, val);
        }

        for (String attributeName : attributes.keySet())
        {
            String attributeValue = attributes.get(attributeName);
            addPropToTu(tu, attributeName, attributeValue);
        }
    }

    private void addPropToTu(Tu tu, String attributeName, String attributeValue)
    {
        tu.addProp("<prop type=\"" + XmlUtils.encodeXmlEntitiesFull(attributeName) + "\">" +
            XmlUtils.encodeXmlEntities(attributeValue) + "</prop>");
    }

    private void insertChecksum(Tuv sourceTuv)
    {
        // Source TUV in first or last TU should only contains one props,
        // which is x-context-post or x-context-pre
        if (m_option.isInsertChecksumInAllTus() && sourceTuv.getProps().size() == 1)
        {
            handleFirstOrLastTuTuWithChecksum(sourceTuv);
        }
        else
        {
            handleTuWithChecksum(sourceTuv);
        }
    }

    private void handleTuWithChecksum(Tuv sourceTuv)
    {
        if (sourceTuv.getProps().isEmpty())
        {
            return;
        }
        Element preProp = null;
        Element nextProp = null;
        String prevMD5Checksum = "";
        String nextMD5Checksum = "";

        for (Element prop : sourceTuv.getProps())
        {
            Attribute contextAttribute = prop.attribute(TYPE);
            String attributeVal = contextAttribute.getValue();
            if (StringUtils.equals(CONTEXT_PRE_TYPE1, attributeVal) ||
                StringUtils.equals(CONTEXT_PRE_TYPE2, attributeVal))
            {
                preProp = prop;
            }
            if (StringUtils.equals(CONTEXT_POST_TYPE1, attributeVal) ||
                StringUtils.equals(CONTEXT_POST_TYPE2, attributeVal))
            {
                nextProp = prop;
            }
        }

        if (preProp != null)
        {
            prevMD5Checksum = getMd5Checksum(preProp);
            // prev Content may not be translatable content.
            // If it is symbols or numbers only, we don't store checksum of it.
            if (StringUtils.isNotEmpty(prevMD5Checksum))
            {
                newAttributesMap.put(PREV_MD5_CHECKSUM, prevMD5Checksum);
                newAttributesMap.put(PREV_MD5_SEGMENT, preProp.getText());
            }
        }

        if (nextProp != null)
        {
            nextMD5Checksum = getMd5Checksum(nextProp);
            // next content may not be translatable content.
            // If it is symbols or numbers only, we don't store checksum of it.
            if (StringUtils.isNotEmpty(nextMD5Checksum))
            {
                newAttributesMap.put(NEXT_MD5_CHECKSUM, nextMD5Checksum);
                newAttributesMap.put(NEXT_MD5_SEGMENT, nextProp.getText());
            }
        }
        // TODO: do we want to keep context_prev and context_next?
        sourceTuv.clearProps();
    }

    private void handleFirstOrLastTuTuWithChecksum(Tuv sourceTuv)
    {
        Element preProp = null;
        Element postProp = null;
        for (Element ele : sourceTuv.getProps())
        {
            Attribute contextAttribute = ele.attribute(TYPE);
            String attributeVal = contextAttribute.getValue();
            if (StringUtils.equals(CONTEXT_PRE_TYPE1, attributeVal) ||
                StringUtils.equals(CONTEXT_PRE_TYPE2, attributeVal))
            {
                preProp = ele;
                break;
            }
            if (StringUtils.equals(CONTEXT_POST_TYPE1, attributeVal) ||
                StringUtils.equals(CONTEXT_POST_TYPE2, attributeVal))
            {
                postProp = ele;
                break;
            }
        }
        // No <prop> with type = x-context-post or x-context-pre is found.
        if (preProp == null && postProp == null)
        {
            return;
        }

        String prevMD5Checksum = "";
        String nextMD5Checksum = "";

        // First TU:
        if (postProp != null)
        {
            prevMD5Checksum = "0";
            nextMD5Checksum = getMd5Checksum(postProp);
            newAttributesMap.put(NEXT_MD5_SEGMENT, postProp.getText());
        }

        // Last Tu:
        if (preProp != null)
        {
            prevMD5Checksum = getMd5Checksum(preProp);
            nextMD5Checksum = "0";
            newAttributesMap.put(PREV_MD5_SEGMENT, preProp.getText());
        }

        if (StringUtils.isNotEmpty(prevMD5Checksum) && StringUtils.isNotEmpty(nextMD5Checksum))
        {
            newAttributesMap.put(PREV_MD5_CHECKSUM, prevMD5Checksum);
            newAttributesMap.put(NEXT_MD5_CHECKSUM, nextMD5Checksum);
            sourceTuv.clearProps();
        }
    }

    private String getMd5Checksum(Element prop)
    {
        String md5Checksum = "";
        String text = prop.getText().trim();
        String input = "";

        if (text.startsWith("<seg>"))
        {
            // It contains <seg>, get its children elements:
            Element sourceElement = XmlParser.parseXml(text).getRootElement();
            converter.tmxToXliff(sourceElement);

            input = getMd5ChecksumInput(sourceElement);
        }
        else
        {
            if (!extractionSupport.isExtractable(text))
            {
                return md5Checksum;
            }
            input = text;
        }

        try
        {
            md5Checksum = Md5Checksum.createChecksum(
                new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));
        }
        catch (Exception ex)
        {

        }
        return md5Checksum;
    }

    @SuppressWarnings("unchecked")
    private String getMd5ChecksumInput(Element elem)
    {
        // Mark agrees we should:
        // 1) Replace tags with equiv-text if set, else nothing.
        // 2) Lowercase.
        // 3) Normalize whitespace.
        PlaceableManager placeableManager = XliffUtils.createDefaultPlaceableManager();
        PlaceableCreator placeableCreator = new XliffPlaceableCreator(placeableManager);

        StringBuffer sb = new StringBuffer();
        List<Node> nodes = elem.content();
        for (Node node : nodes)
        {
            if (node.getNodeType() == Node.TEXT_NODE)
            {
                Text text = (Text) node;
                sb.append(text.getText());
            }
            else if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                TextHolder textHolder = placeableCreator.createPlaceable((Element) node);
                sb.append(textHolder.getText());
            }
        }
        String result = sb.toString();
        result = result.toLowerCase(srcLocale.getJdkLocale());
        return MD5_CHECKSUM_NORMALIZER.matcher(result).replaceAll(" "); //$NON-NLS-1$
    }

    private void clearChecksumContextOnAttributeMap()
    {
        newAttributesMap.remove(PREV_MD5_CHECKSUM);
        newAttributesMap.remove(NEXT_MD5_CHECKSUM);
        newAttributesMap.remove(PREV_MD5_SEGMENT);
        newAttributesMap.remove(NEXT_MD5_SEGMENT);
    }

}
