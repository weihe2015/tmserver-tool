package org.gs4tr.tmserver.tool.service.tmx;

import java.io.FileNotFoundException;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;
import org.dom4j.io.SAXReader;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxConstants;
import org.gs4tr.tmserver.tool.model.tmx.common.TmxHeader;
import org.gs4tr.tmserver.tool.model.tmx.common.Tu;
import org.gs4tr.tmserver.tool.service.tmx.io.MonitoringFileInputStream;
import org.gs4tr.tmserver.tool.service.tmx.io.callback.TmxReaderCallback;
import org.gs4tr.tmserver.tool.service.tmx.io.callback.imp.TmxReaderCallbackImpl;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class TmxLocaleReader implements TmxConstants
{
    public class TmxHeaderElementHandler implements ElementHandler
    {
        @Override
        public void onEnd(ElementPath path)
        {
            Element element = path.getCurrent();

            m_header = new TmxHeader(element);
            m_header.setTmxVersion(m_tmxVersion);

            if (m_callback != null)
            {
                m_callback.handle(m_header);
            }
            // prune the current element to reduce memory
            element.detach();
        }

        @Override
        public void onStart(ElementPath path)
        {
        }
    }

    private String m_tmxVersion;
    private String m_fileName;
    private Tu m_tu;
    private TmxHeader m_header;

    private TmxReaderCallback m_callback;

    public String getFileName()
    {
        return m_fileName;
    }

    public TmxHeader getHeader()
    {
        return m_header;
    }

    public String getSourceLanguage()
    {
        return m_callback.getSourceLanguage();
    }

    public String getTargetLanguage()
    {
        return m_callback.getTargetLanguage();
    }

    public String getTmxVersion()
    {
        return m_tmxVersion;
    }

    @SuppressWarnings("unused")
    private void parseFile(SAXReader p_reader, InputSource p_input, MonitoringFileInputStream p_stream)
        throws DocumentException
    {
        SAXReader reader = p_reader;
        final MonitoringFileInputStream stream = p_stream;

        // enable element complete notifications to conserve memory
        reader.addHandler("/" + TmxConstants.ROOT + "/" + TmxConstants.HEADER, new TmxHeaderElementHandler());
        // enable element complete notifications to conserve memory
        reader.addHandler("/" + TmxConstants.ROOT + "/" + TmxConstants.BODY + "/" + TmxConstants.TU,
            new ElementHandler()
            {
                int tuCount = 0;

                @Override
                final public void onEnd(ElementPath path)
                {
                    shout();
                    Element element = path.getCurrent();

                    if (m_callback != null && tuCount == 1)
                    {
                        m_tu = new Tu(element);
                        m_callback.handle(m_tu);
                    }
                    // prune the current element to reduce memory
                    element.detach();
                }

                @Override
                final public void onStart(ElementPath path)
                {
                    if (tuCount > 1)
                    {
                        reader.removeHandler(path.getPath());
                        return;
                    }
                }

                final private void shout()
                {
                    tuCount++;
                    int prec = (int) (stream.getCounter() * 100 / stream.getTotal());
                }
            });
        reader.read(p_input);
    }

    public void readFile(String p_filename) throws SAXException, FileNotFoundException, DocumentException
    {
        m_fileName = p_filename;
        m_callback = new TmxReaderCallbackImpl();

        SAXReader reader = new SAXReader();
        reader.setEncoding("UTF-8");
        reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
        reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        reader.setFeature("http://apache.org/xml/features/validation/dynamic", false);
        reader.setEntityResolver(new TmxDtdResolver());
        reader.setValidation(false);

        MonitoringFileInputStream stream = new MonitoringFileInputStream(p_filename);
        InputSource src = new InputSource(stream);
        parseFile(reader, src, stream);
    }

}
