package org.gs4tr.tmserver.tool.service.tmx.io.callback;

import org.gs4tr.tmserver.tool.model.wf.common.TU;
import org.gs4tr.tmserver.tool.model.wf.common.WordFastHeader;

public interface WfReaderCallback
{
    String getSourceLanguage();

    String getTargetLanguage();

    boolean handle(TU tu);

    void handle(WordFastHeader header);

    void invalidSegment(String segment);
}
