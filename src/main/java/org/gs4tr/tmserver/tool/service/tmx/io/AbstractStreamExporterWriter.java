package org.gs4tr.tmserver.tool.service.tmx.io;

import java.io.IOException;
import java.io.Writer;

import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.tmserver.tool.model.tmx.common.Tu;

public abstract class AbstractStreamExporterWriter
{
    protected static String EOL = System.getProperty("line.separator");
    protected Locale _tmSrcLocale;
    protected Locale _tmTrgLocale;
    protected Writer _writer;

    public AbstractStreamExporterWriter(Locale srcLocale, Locale tgtLocale, Writer w)
    {
        _tmSrcLocale = srcLocale;
        _tmTrgLocale = tgtLocale;
        _writer = w;

        try
        {
            writeHeader();
        }
        catch (IOException ex)
        {
            closeWriter(_writer);
            throw new RuntimeException(ex);
        }
    }

    public void close()
    {
        if (_writer == null)
        {
            return;
        }

        try
        {
            writeEnd();
            _writer.flush();
        }
        catch (IOException ex)
        {
            throw new RuntimeException(ex);
        }
        finally
        {
            closeWriter(_writer);
        }
    }

    protected void closeWriter(Writer w)
    {
        /*
         * Since the writer could have been created externally, just "nullify" the
         * reference here. Subclasses may override and close resources, if required.
         */
        _writer = null;
    }

    protected abstract void writeEnd() throws IOException;

    protected abstract void writeHeader() throws IOException;

    public void writeSegment(Tu tu)
    {
        try
        {
            _writer.write(tu.getXml());
        }
        catch (IOException ex)
        {
            closeWriter(_writer);
            throw new RuntimeException(ex);
        }
    }
}
