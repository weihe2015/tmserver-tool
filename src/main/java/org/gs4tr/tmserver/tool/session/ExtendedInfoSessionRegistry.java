package org.gs4tr.tmserver.tool.session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class ExtendedInfoSessionRegistry
    implements SessionRegistry, ApplicationListener<SessionDestroyedEvent>
{
    protected static final Log logger = LogFactory.getLog(SessionRegistryImpl.class);
    private static final String USER_AGENT_HEADER_NAME = "User-Agent";
    /** <principal:Object,SessionIdSet> */
    private final ConcurrentMap<Object, Set<String>> principals = new ConcurrentHashMap<Object, Set<String>>();
    /** <sessionId:Object,SessionInformation> */
    private final Map<String, SessionInformation> sessionIds = new ConcurrentHashMap<String, SessionInformation>();

    @Override
    public List<Object> getAllPrincipals()
    {
        return new ArrayList<Object>(principals.keySet());
    }

    @Override
    public List<SessionInformation> getAllSessions(Object principal, boolean includeExpiredSessions)
    {
        if (sessionIds.isEmpty())
        {
            return Collections.emptyList();
        }
        List<SessionInformation> result = new ArrayList<>();
        for (SessionInformation sessionInformation : sessionIds.values())
        {
            if (sessionInformation == null)
            {
                continue;
            }
            if (includeExpiredSessions || !sessionInformation.isExpired())
            {
                result.add(sessionInformation);
            }
        }
        return result;
    }

    @Override
    public SessionInformation getSessionInformation(String sessionId)
    {
        return sessionIds.get(sessionId);
    }

    @Override
    public void onApplicationEvent(SessionDestroyedEvent event)
    {
        String sessionId = event.getId();
        removeSessionInformation(sessionId);
    }

    @Override
    public void refreshLastRequest(String sessionId)
    {
        SessionInformation info = getSessionInformation(sessionId);
        if (info != null)
        {
            info.refreshLastRequest();
        }
    }

    @Override
    public void registerNewSession(String sessionId, Object principal)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Registering session " + sessionId + ", for principal " + principal);
        }

        if (getSessionInformation(sessionId) != null)
        {
            removeSessionInformation(sessionId);
        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        String userAddress = request.getHeader("X-FORWARDED-FOR");
        if (userAddress == null)
        {
            userAddress = request.getRemoteAddr();
        }
        sessionIds.put(sessionId, new ExtendedSessionInformation("admin", sessionId, new Date(), new Date(),
            userAddress, request.getHeader(USER_AGENT_HEADER_NAME)));

        Set<String> sessionsUsedByPrincipal = principals.get(principal);

        if (sessionsUsedByPrincipal == null)
        {
            sessionsUsedByPrincipal = new CopyOnWriteArraySet<String>();
            Set<String> prevSessionsUsedByPrincipal = principals.putIfAbsent(principal,
                sessionsUsedByPrincipal);
            if (prevSessionsUsedByPrincipal != null)
            {
                sessionsUsedByPrincipal = prevSessionsUsedByPrincipal;
            }
        }

        sessionsUsedByPrincipal.add(sessionId);

        if (logger.isTraceEnabled())
        {
            logger.trace("Sessions used by '" + principal + "' : " + sessionsUsedByPrincipal);
        }
    }

    @Override
    public void removeSessionInformation(String sessionId)
    {
        SessionInformation info = getSessionInformation(sessionId);
        if (info == null)
        {
            return;
        }
        if (logger.isTraceEnabled())
        {
            logger.debug("Removing session " + sessionId + " from set of registered sessions");
        }
        sessionIds.remove(sessionId);

        Set<String> sessionsUsedByPrincipal = principals.get(info.getPrincipal());
        if (sessionsUsedByPrincipal == null)
        {
            return;
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("Removing session " + sessionId + " from principal's set of registered sessions");
        }

        sessionsUsedByPrincipal.remove(sessionId);
        if (sessionsUsedByPrincipal.isEmpty())
        {
            // No need to keep object in principals Map anymore
            if (logger.isDebugEnabled())
            {
                logger.debug("Removing principal " + info.getPrincipal() + " from registry");
            }
            principals.remove(info.getPrincipal());
        }

        if (logger.isTraceEnabled())
        {
            logger.trace("Sessions used by '" + info.getPrincipal() + "' : " + sessionsUsedByPrincipal);
        }
    }

}
