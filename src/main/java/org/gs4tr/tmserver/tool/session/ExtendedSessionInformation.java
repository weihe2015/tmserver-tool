package org.gs4tr.tmserver.tool.session;

import java.io.Serializable;
import java.util.Date;

import org.springframework.security.core.session.SessionInformation;

public class ExtendedSessionInformation extends SessionInformation implements Serializable
{
    private static final long serialVersionUID = 2767201676032080382L;

    private Date _loginTime;
    private String _userAddress;
    private String _userAgent;

    public ExtendedSessionInformation(Object principal, String sessionId, Date lastRequest)
    {
        super(principal, sessionId, lastRequest);
    }

    public ExtendedSessionInformation(Object principal, String sessionId, Date lastRequest, Date loginTime,
        String userAddress, String userAgent)
    {
        super(principal, sessionId, lastRequest);
        _loginTime = loginTime;
        _userAddress = userAddress;
        _userAgent = userAgent;
    }

    public Date getLoginTime()
    {
        return _loginTime;
    }

    public String getUserAddress()
    {
        return _userAddress;
    }

    public String getUserAgent()
    {
        return _userAgent;
    }

    public void setLoginTime(Date loginTime)
    {
        _loginTime = loginTime;
    }

    public void setUserAddress(String userAddress)
    {
        _userAddress = userAddress;
    }

    public void setUserAgent(String userAgent)
    {
        _userAgent = userAgent;
    }
}
