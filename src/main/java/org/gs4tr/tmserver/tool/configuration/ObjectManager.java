package org.gs4tr.tmserver.tool.configuration;

import org.gs4tr.tmserver.tool.controller.jobqueue.cache.ExportInfoCache;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.ImportInfoCache;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.SessionInfoCache;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.TmInfoListCache;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.UserDetailsCache;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;

@Configuration
public class ObjectManager
{
    @Value("${thread.queue.capacity:500}")
    private int cacheSize;

    @Value("${ehcache.CacheConfiguration.expireTime:43200}")
    private long expiredSeconds;

    @Value("${ehcache.CacheConfiguration.liveSeconds:43200}")
    private long liveSeconds;

    @Bean
    public ObjectMapper createObjectMapper()
    {
        return new ObjectMapper();
    }

    @Bean
    @Qualifier("serviceCacheManager")
    public CacheManager createCacheManager()
    {
        CacheManager cacheManager = CacheManager.create();
        cacheManager.removeAllCaches();

        addImportInfoCache(cacheManager);
        addExportInfoCache(cacheManager);
        addTmInfoListCache(cacheManager);
        addUserDetailsCache(cacheManager);
        addSessionInfoCache(cacheManager);
        return cacheManager;
    }

    private void addImportInfoCache(CacheManager cacheManager)
    {
        CacheConfiguration cacheConf = cacheManager.getConfiguration().getDefaultCacheConfiguration().clone();
        cacheConf.setName(ImportInfoCache.CACHE_POOL_NAME);
        cacheConf.setMaxEntriesLocalHeap(cacheSize);
        cacheConf.setTimeToIdleSeconds(expiredSeconds);
        cacheConf.setTimeToLiveSeconds(liveSeconds);
        cacheConf.setEternal(false);
        Cache cache = new Cache(cacheConf);
        cache.setName(ImportInfoCache.CACHE_ID);
        cacheManager.addCache(cache);
    }

    private void addExportInfoCache(CacheManager cacheManager)
    {
        CacheConfiguration cacheConf = cacheManager.getConfiguration().getDefaultCacheConfiguration().clone();
        cacheConf.setName(ExportInfoCache.CACHE_POOL_NAME);
        cacheConf.setMaxEntriesLocalHeap(cacheSize);
        cacheConf.setTimeToIdleSeconds(expiredSeconds);
        cacheConf.setTimeToLiveSeconds(liveSeconds);
        cacheConf.setEternal(false);
        Cache cache = new Cache(cacheConf);
        cache.setName(ExportInfoCache.CACHE_ID);
        cacheManager.addCache(cache);
    }

    private void addTmInfoListCache(CacheManager cacheManager)
    {
        CacheConfiguration cacheConf = cacheManager.getConfiguration().getDefaultCacheConfiguration().clone();
        cacheConf.setName(TmInfoListCache.CACHE_POOL_NAME);
        cacheConf.setMaxEntriesLocalHeap(30);
        cacheConf.setTimeToIdleSeconds(expiredSeconds);
        cacheConf.setTimeToLiveSeconds(liveSeconds);
        cacheConf.setEternal(false);
        Cache cache = new Cache(cacheConf);
        cache.setName(TmInfoListCache.CACHE_ID);
        cacheManager.addCache(cache);
    }

    private void addUserDetailsCache(CacheManager cacheManager)
    {
        CacheConfiguration cacheConf = cacheManager.getConfiguration().getDefaultCacheConfiguration().clone();
        cacheConf.setName(UserDetailsCache.CACHE_POOL_NAME);
        cacheConf.setMaxEntriesLocalHeap(1000);
        cacheConf.setEternal(true);
        cacheConf.setTimeToIdleSeconds(0);
        cacheConf.setTimeToLiveSeconds(0);
        Cache cache = new Cache(cacheConf);
        cache.setName(UserDetailsCache.CACHE_ID);
        cacheManager.addCache(cache);
    }

    private void addSessionInfoCache(CacheManager cacheManager)
    {
        CacheConfiguration cacheConf = cacheManager.getConfiguration().getDefaultCacheConfiguration().clone();
        cacheConf.setName(SessionInfoCache.CACHE_POOL_NAME);
        cacheConf.setMaxEntriesLocalHeap(1000);
        cacheConf.setEternal(true);
        cacheConf.setTimeToIdleSeconds(0);
        cacheConf.setTimeToLiveSeconds(0);
        Cache cache = new Cache(cacheConf);
        cache.setName(SessionInfoCache.CACHE_ID);
        cacheManager.addCache(cache);
    }
}
