package org.gs4tr.tmserver.tool.configuration;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.configuration.exception.JwtTokenInvalidException;
import org.gs4tr.tmserver.tool.controller.model.TPAuthLoginOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenService implements Serializable
{
    private static final long serialVersionUID = -3301605591108950415L;
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenService.class);
    private static final String CLAIM_KEY_USERNAME = "sub";
    private static final String CLAIM_KEY_PASSWORD = "pass";
    private static final String CLAIM_KEY_SESSIONID = "sid";
    private static final String CLAIM_KEY_CREATED = "created";
    private static final String CLAIM_KEY_URL = "url";
    private static final String CLAIM_KEY_LIMITED = "limited";
    private static final String CLAIM_KEY_APP_VERSION = "version";

    private static final String CLAIM_KEY_EMAIL = "email";
    private static final String CLAIM_KEY_EXPIRED = "exp";
    private static final String CLAIM_KEY_OAUTH = "oauth";

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${tmserver.version}")
    private String applicationVersion;

    @SuppressWarnings("unused")
    @Autowired
    private ObjectMapper objectMapper;

    private Date generateExpirationDate()
    {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }

    private String generateToken(Map<String, Object> claims)
    {
        try
        {
            return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate()).signWith(
                SignatureAlgorithm.HS512, secret.getBytes("UTF-8")).compact();
        }
        catch (UnsupportedEncodingException ex)
        {
            LOGGER.error(String.format(Messages.getString("JwtTokenService.1"), ex.getMessage()));
            return null;
        }
    }

    public String generateToken(String username, String password, String sessionId)
    {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, username);
        claims.put(CLAIM_KEY_PASSWORD, password);
        claims.put(CLAIM_KEY_SESSIONID, sessionId);
        claims.put(CLAIM_KEY_CREATED, new Date());
        claims.put(CLAIM_KEY_APP_VERSION, applicationVersion);
        return generateToken(claims);
    }

    public String generateToken(String url, String username, String password, String sessionId)
    {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_URL, url);
        claims.put(CLAIM_KEY_USERNAME, username);
        claims.put(CLAIM_KEY_PASSWORD, password);
        claims.put(CLAIM_KEY_SESSIONID, sessionId);
        claims.put(CLAIM_KEY_CREATED, new Date());
        claims.put(CLAIM_KEY_APP_VERSION, applicationVersion);
        return generateToken(claims);
    }

    public String generateToken(TPAuthLoginOption tpAuthLoginOption, String sessionId)
    {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_EMAIL, tpAuthLoginOption.getEmail());
        claims.put(CLAIM_KEY_CREATED, tpAuthLoginOption.getCreation());
        claims.put(CLAIM_KEY_EXPIRED, tpAuthLoginOption.getExpiration());
        claims.put(CLAIM_KEY_OAUTH, true);
        claims.put(CLAIM_KEY_SESSIONID, sessionId);
        claims.put(CLAIM_KEY_APP_VERSION, applicationVersion);
        return generateToken(claims);
    }

    public String generateToken(UserDetails userDetail)
    {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, userDetail.getUsername());
        claims.put(CLAIM_KEY_PASSWORD, userDetail.getPassword());
        claims.put(CLAIM_KEY_CREATED, new Date());
        claims.put(CLAIM_KEY_APP_VERSION, applicationVersion);
        return generateToken(claims);
    }

    public String generateTokenLimitedFeature(String username, String password, String sessionId,
        String feature)
    {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, username);
        claims.put(CLAIM_KEY_PASSWORD, password);
        claims.put(CLAIM_KEY_SESSIONID, sessionId);
        claims.put(CLAIM_KEY_CREATED, new Date());
        claims.put(CLAIM_KEY_LIMITED, feature);
        claims.put(CLAIM_KEY_APP_VERSION, applicationVersion);
        return generateToken(claims);
    }

    public Claims getClaimsFromToken(String token)
    {
        Claims claims;
        try
        {
            claims = Jwts.parser().setSigningKey(secret.getBytes("UTF-8")).parseClaimsJws(token).getBody();
        }
        catch (Exception ex)
        {
            LOGGER.error(String.format(Messages.getString("JwtTokenService.2"), token, ex.getMessage()));
            claims = null;
        }
        return claims;
    }

    public Date getExpirationDateFromToken(String token)
    {
        Date expiration;
        try
        {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        }
        catch (Exception ex)
        {
            LOGGER.error(String.format(Messages.getString("JwtTokenService.2"), token, ex.getMessage()));
            expiration = null;
        }
        return expiration;
    }

    public Map<String, Object> getClaimsMap(String token)
    {
        Map<String, Object> claimMap = new LinkedHashMap<>();
        Claims claims = getClaimsFromToken(token);
        if (claims == null)
        {
            return claimMap;
        }
        for (String key : claims.keySet())
        {
            claimMap.put(key, claims.get(key));
        }
        return claimMap;
    }

    public String getUsernameFromToken(String token)
    {
        String username = null;
        try
        {
            final Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
        }
        catch (Exception ex)
        {
            LOGGER.error(String.format(Messages.getString("JwtTokenService.4"), token, ex.getMessage()));
            username = null;
        }
        return username;
    }

    public boolean isOAuthToken(String token) throws JwtTokenInvalidException
    {
        try
        {
            final Claims claims = getClaimsFromToken(token);
            if (!claims.containsKey(CLAIM_KEY_OAUTH))
            {
                return false;
            }
            return (boolean) claims.get(CLAIM_KEY_OAUTH);
        }
        catch (Exception ex)
        {
            String message = String.format(Messages.getString("JwtTokenService.5"), token, ex.getMessage());
            throw new JwtTokenInvalidException(message);
        }
    }

    private Boolean isTokenExpired(String token)
    {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String refreshToken(String token)
    {
        String refreshedToken;
        try
        {
            final Claims claims = getClaimsFromToken(token);
            claims.put(CLAIM_KEY_CREATED, new Date());
            refreshedToken = generateToken(claims);
        }
        catch (Exception ex)
        {
            LOGGER.error(String.format(Messages.getString("JwtTokenService.6"), token, ex.getMessage()));
            refreshedToken = null;
        }
        return refreshedToken;
    }

    public boolean validateToken(String authToken, UserDetails userDetails)
    {
        final String username = getUsernameFromToken(authToken);
        final String userDetailname = userDetails.getUsername();
        if (userDetailname.equals("whe"))
        {
            return !isTokenExpired(authToken);
        }
        return username.equals(userDetailname) && !isTokenExpired(authToken);
    }

}
