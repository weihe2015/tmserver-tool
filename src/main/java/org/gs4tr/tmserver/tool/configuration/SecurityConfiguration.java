package org.gs4tr.tmserver.tool.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception
	{
		return new JwtAuthenticationTokenFilter();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception
	{
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception
	{
		/* .permitAll()/*.hasAnyAuthority("user","admin").anyRequest() */
		httpSecurity.authorizeRequests().antMatchers(HttpMethod.POST, "/api/v1/user/login")
				.permitAll().antMatchers(HttpMethod.POST, "/api/v1/user/logout").authenticated()
				.antMatchers(HttpMethod.POST, "/api/v1/user/remote").authenticated()
				.antMatchers(HttpMethod.POST, "/api/v1/user/oauthlogin").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/list").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v2/tms/**").authenticated()
				.antMatchers(HttpMethod.POST, "/api/v2/tms").authenticated()
				.antMatchers(HttpMethod.DELETE, "/api/v2/tms/**").authenticated()
				.antMatchers(HttpMethod.POST, "/api/v1/tm/duplicate").authenticated()
				.antMatchers("/api/v1/tm/**").authenticated()
				.antMatchers("/api/v2/tm/**").authenticated()
				.antMatchers(HttpMethod.GET, "/").permitAll()
				.antMatchers(HttpMethod.GET, "/error").permitAll()
				.antMatchers(HttpMethod.GET, "/401").permitAll()
				.antMatchers(HttpMethod.GET, "/404").permitAll()
				.antMatchers(HttpMethod.GET, "/silent-refresh").permitAll()
				.antMatchers(HttpMethod.GET, "/concordance").permitAll()
				.antMatchers(HttpMethod.GET, "/createCsv").permitAll()
				.antMatchers(HttpMethod.GET, "/createTm").permitAll()
				.antMatchers(HttpMethod.GET, "/createTmWithCSV").permitAll()
				.antMatchers(HttpMethod.GET, "/importTm").permitAll()
				.antMatchers(HttpMethod.GET, "/exportTm").permitAll()
				.antMatchers(HttpMethod.GET, "/renameTm").permitAll()
				.antMatchers(HttpMethod.GET, "/SSO").permitAll()
				.antMatchers(HttpMethod.GET, "/manipulateTmx").permitAll().and().csrf().disable()
				.exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
				// Always create seesion for import Tm and manipulate Tmx services,
				// to distinguish the imported files.
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS).and()
				.rememberMe().rememberMeParameter("tool-remember-me-param")
				// expired time = 1day
				.rememberMeCookieName("tool-remember-me").tokenValiditySeconds(24 * 60 * 60)
				.tokenRepository(persistentTokenRepository()).and().logout()
				.deleteCookies("JSESSIONID")
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/login?logout=true").and().exceptionHandling()
				.accessDeniedPage("/access-denied");

		// add JWT filter
		httpSecurity.addFilterBefore(authenticationTokenFilterBean(),
				UsernamePasswordAuthenticationFilter.class);

		// disable cache
        httpSecurity.headers().frameOptions().sameOrigin().cacheControl();
	}

	@Override
	public void configure(WebSecurity web) throws Exception
	{
		web.ignoring().antMatchers(HttpMethod.GET, "/resources/**", "/static/**", "/css/**",
				"/js/**", "/images/**", "/lib/**");
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder()
	{
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}

	@Bean
	public PersistentTokenRepository persistentTokenRepository()
	{
		JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(dataSource);
		return tokenRepository;
	}

}
