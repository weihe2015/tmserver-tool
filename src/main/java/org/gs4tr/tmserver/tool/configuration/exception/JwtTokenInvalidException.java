package org.gs4tr.tmserver.tool.configuration.exception;

public class JwtTokenInvalidException extends Exception
{
    private static final long serialVersionUID = 2812973868611461808L;

    public JwtTokenInvalidException(String message)
    {
        super(message);
    }

}
