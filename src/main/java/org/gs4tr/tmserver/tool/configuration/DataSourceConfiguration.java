package org.gs4tr.tmserver.tool.configuration;

import java.util.HashMap;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(basePackages = {
    "org.gs4tr.tmserver.tool.repository" }, entityManagerFactoryRef = "tmServerToolEntityManager", transactionManagerRef = "tmServerToolTransactionManager")
public class DataSourceConfiguration
{
    @Autowired
    private Environment env;

    @Primary
    @Bean
    public DataSource tmServerToolDataSource()
    {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        String isMariaDB = env.getProperty("tmservertool.datasource.isMariaDB");

        if (StringUtils.equals("false", isMariaDB))
        {
            dataSource.setDriverClassName(env.getProperty("mysqlDB.datasource.driver-class-name"));
            dataSource.setUrl(env.getProperty("mysqlDB.datasource.url"));
            dataSource.setUsername(env.getProperty("mysqlDB.datasource.username"));
            dataSource.setPassword(env.getProperty("mysqlDB.datasource.password"));
        }
        else
        {
            dataSource.setDriverClassName(env.getProperty("mariaDB.datasource.driver-class-name"));
            dataSource.setUrl(env.getProperty("mariaDB.datasource.url"));
            dataSource.setUsername(env.getProperty("mariaDB.datasource.username"));
            dataSource.setPassword(env.getProperty("mariaDB.datasource.password"));
        }
        return dataSource;
    }

    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean tmServerToolEntityManager()
    {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(tmServerToolDataSource());
        em.setPackagesToScan(new String[] { "org.gs4tr.tmserver.tool" });
        em.setPersistenceUnitName("tmServerToolEntityManager");
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();

        String isMariaDB = env.getProperty("tmservertool.datasource.isMariaDB");

        // Database is MySQL:
        if (StringUtils.equals("false", isMariaDB))
        {
            properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
            properties.put("hibernate.show-sql", env.getProperty("jdbc.show-sql"));
            properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        }
        // Database is MariaDB:
        else
        {
            properties.put("mariaDB.jpa.hibernate.naming.physical-strategy",
                env.getProperty("mariaDB.jpa.hibernate.naming.physical-strategy"));
        }
        properties.put("spring.datasource.testWhileIdle", true);
        properties.put("spring.datasource.validationQuery", "SELECT 1");
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Primary
    @Bean
    public PlatformTransactionManager tmServerToolTransactionManager()
    {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(tmServerToolEntityManager().getObject());
        return transactionManager;
    }
}
