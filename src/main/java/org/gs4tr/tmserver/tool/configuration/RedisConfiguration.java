package org.gs4tr.tmserver.tool.configuration;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gs4tr.tmserver.tool.api.model.FetchOrganizationsResponse;
import org.gs4tr.tmserver.tool.api.model.FetchTmLanguageResponse;
import org.gs4tr.tmserver.tool.api.model.TmListResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.Jedis;

@Component
public class RedisConfiguration
{
    static private final Log LOG = LogFactory.getLog(RedisConfiguration.class);

    @Value("${redis.ipaddress}")
    private String host;

    @Value("${redis.portNumber}")
    private String port;

    @Autowired
    private ObjectMapper objectMapper;


    private Jedis createJedis()
    {
        final int portNum = Integer.parseInt(port);
        // Connects this socket to the server with a specified timeout value 3000 ms.
        final int timeout = 3000;
        return new Jedis(host, portNum, timeout);
    }

    public String get(String key)
    {
        Jedis jedis = createJedis();
        if (!jedis.exists(key))
        {
            return null;
        }
        return jedis.get(key);
    }

    public FetchOrganizationsResponse getOrganizationList(String key)
    {
        String text = get(key);
        if (StringUtils.isEmpty(text))
        {
            return null;
        }
        FetchOrganizationsResponse response = null;
        try
        {
            response = objectMapper.readValue(text, FetchOrganizationsResponse.class);
        }
        catch (IOException ex)
        {

        }
        return response;
    }

    public FetchTmLanguageResponse getTmLanguagesList(String key)
    {
        String text = get(key);
        if (StringUtils.isEmpty(text))
        {
            return null;
        }
        FetchTmLanguageResponse response = null;
        try
        {
            response = objectMapper.readValue(text, FetchTmLanguageResponse.class);
        }
        catch (IOException ex)
        {

        }

        return response;
    }

    public TmListResponse getTmList(String key)
    {
        String text = get(key);
        if (StringUtils.isEmpty(text))
        {
            return null;
        }
        TmListResponse response = null;
        try
        {
            response = objectMapper.readValue(text, TmListResponse.class);
        }
        catch (IOException ex)
        {

        }
        return response;
    }

    public boolean isReachable()
    {
        Socket socket = null;
        boolean isReachable = false;
        try
        {
            socket = new Socket();
            final int portNum = Integer.parseInt(port);
            // Connects this socket to the server with a specified timeout value 300 ms.
            final int timeout = 300;
            socket.connect(new InetSocketAddress(host, portNum), timeout);
            isReachable = true;
        }
        catch (IOException ex)
        {
            // Ignore the IOException
            if (LOG.isDebugEnabled())
            {
                LOG.debug("Redis server is not reachable", ex);
            }
            isReachable = false;
        }
        finally
        {
            try
            {
                if (socket != null)
                {
                    socket.close();
                }
            }
            catch (IOException e)
            {
                // Ignore IOException here.
            }
        }
        return isReachable;
    }

    public void removeCache(String key)
    {
        Jedis jedis = createJedis();
        if (!jedis.exists(key))
        {
            return;
        }
        jedis.del(key);
        jedis.close();
    }

    public void save(String key, String val)
    {
        Jedis jedis = createJedis();
        // key will expire in one day:
        jedis.set(key, val);
        jedis.expire(key, 60 * 60 * 24);
        jedis.close();
    }

    public void saveTmList(String key, TmListResponse tmListResponse)
    {
        String val;
        try
        {
            val = objectMapper.writeValueAsString(tmListResponse);
            save(key, val);
        }
        catch (IOException ex)
        {

        }
    }
}
