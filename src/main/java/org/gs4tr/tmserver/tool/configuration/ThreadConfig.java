package org.gs4tr.tmserver.tool.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ThreadConfig
{
    private static final String ThreadNamePrefix = "TMSToolTaskExecutorThread";

    @Value("${thread.core-pool}")
    private int corePoolSize;

    @Value("${thread.max-pool}")
    private int maxPoolSize;

    @Value("${thread.queue.capacity}")
    private int queueCapacity;

//    @Bean
//    public TaskExecutor threadPoolTaskExecutor()
//    {
//        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
//        executor.setCorePoolSize(corePoolSize);
//        executor.setMaxPoolSize(maxPoolSize);
//        executor.setThreadNamePrefix(ThreadNamePrefix);
//        executor.setQueueCapacity(queueCapacity);
//        executor.initialize();
//        return executor;
//    }

}
