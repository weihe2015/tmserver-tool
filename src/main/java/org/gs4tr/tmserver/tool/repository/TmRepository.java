package org.gs4tr.tmserver.tool.repository;

import org.gs4tr.tmserver.tool.model.server.Tm;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("tmRepository")
public interface TmRepository extends CrudRepository<Tm, Long>
{
}
