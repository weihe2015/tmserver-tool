package org.gs4tr.tmserver.tool.repository;

import org.gs4tr.tmserver.tool.model.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("roleRepository")
public interface RoleRepository extends JpaRepository<Role, Integer>
{
    Role findByRoleId(String roleId);
}
