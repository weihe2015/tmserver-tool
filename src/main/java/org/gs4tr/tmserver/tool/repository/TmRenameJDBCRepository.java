package org.gs4tr.tmserver.tool.repository;

import java.util.List;

import org.gs4tr.tmserver.tool.controller.model.MixedTmInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class TmRenameJDBCRepository
{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional(readOnly = true)
    public List<MixedTmInfo> getAllTmList()
    {
        String SQL = "select TM_ID, NAME, TM_GROUP_NAME, SOURCE_LOCALE, TARGET_LOCALE, " +
            "concat(PROJECT_SHORT_CODE,\"/\",SHORT_CODE) PATH from TM_TM m " +
            "left outer join (select SHORT_CODE sc, NAME TM_GROUP_NAME from TM_PROJECT) p " +
            "on m.PROJECT_SHORT_CODE=p.sc where TM_GROUP_NAME IS NOT NULL";

        List<MixedTmInfo> result = jdbcTemplate.query(SQL,
            (rs, rowNum) -> {
                MixedTmInfo mixedTmInfo = new MixedTmInfo();
                Long tmId = Long.valueOf(rs.getInt("TM_ID"));
                String tmName = rs.getString("NAME");
                String tmGroupName = rs.getString("TM_GROUP_NAME");
                String sourceLocale = rs.getString("SOURCE_LOCALE");
                String targetLocale = rs.getString("TARGET_LOCALE");
                String tmPath = rs.getString("PATH");
                mixedTmInfo.initRenameTmInfo(tmId, tmName, tmGroupName, sourceLocale, targetLocale, tmPath);
                return mixedTmInfo;
            });

        return result;
    }

}
