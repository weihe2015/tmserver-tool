package org.gs4tr.tmserver.tool.repository;

import org.gs4tr.tmserver.tool.model.user.TmUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("TmUserRoleRepository")
public interface TmUserRoleRepository extends JpaRepository<TmUserRole, Long>
{
    TmUserRole findByUserProfileId(Long userProfileId);
}
