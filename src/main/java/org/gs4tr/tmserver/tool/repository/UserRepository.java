package org.gs4tr.tmserver.tool.repository;

import org.gs4tr.tmserver.tool.model.user.TmUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<TmUser, Long>
{
    TmUser findByUsername(String username);
}
