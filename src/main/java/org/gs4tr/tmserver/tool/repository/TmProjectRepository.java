package org.gs4tr.tmserver.tool.repository;

import org.gs4tr.tmserver.tool.model.server.TmProject;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("tmProjectRepository")
public interface TmProjectRepository extends CrudRepository<TmProject, Long>
{
	TmProject findByShortCode(String projectShortCode);
}
