package org.gs4tr.tmserver.tool.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class GenericResponse
{
    private String global;

    public String getGlobal()
    {
        return global;
    }

    public void setGlobal(String global)
    {
        this.global = global;
    }

}
