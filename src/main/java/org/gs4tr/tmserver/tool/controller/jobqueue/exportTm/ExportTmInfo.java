package org.gs4tr.tmserver.tool.controller.jobqueue.exportTm;

import java.io.Serializable;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ExportTmInfo implements Serializable
{
    private static final long serialVersionUID = 8149906093938055823L;

    // We use TM Path as jobId
    private String jobId;
    private ExportTmPhase exportTmPhase;
    private String message;
    private boolean exportToFtp;
    private String fileType;
    private byte[] out;
    private String tmxFileName;
    private String accessToken;

    public ExportTmInfo(String jobId)
    {
        this.jobId = jobId;
        this.exportTmPhase = ExportTmPhase.WAITING;
    }

    public String getJobId()
    {
        return jobId;
    }

    public void setJobId(String jobId)
    {
        this.jobId = jobId;
    }

    public ExportTmPhase getExportTmPhase()
    {
        return exportTmPhase;
    }

    public void setExportTmPhase(ExportTmPhase exportTmPhase)
    {
        this.exportTmPhase = exportTmPhase;
    }

    public void setExportTmPhaseStart()
    {
        this.exportTmPhase = ExportTmPhase.START;
    }

    public void setExportTmPhaseFinished()
    {
        this.exportTmPhase = ExportTmPhase.FINISHED;
    }

    public void setExportTmPhaseFailed()
    {
        this.exportTmPhase = ExportTmPhase.FAILED;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public boolean isExportToFtp()
    {
        return exportToFtp;
    }

    public void setExportToFtp(boolean exportToFtp)
    {
        this.exportToFtp = exportToFtp;
    }

    public String getFileType()
    {
        return fileType;
    }

    public void setFileType(String fileType)
    {
        this.fileType = fileType;
    }

    public byte[] getOut()
    {
        return out;
    }

    public void setOut(byte[] out)
    {
        this.out = out;
    }

    public String getTmxFileName()
    {
        return tmxFileName;
    }

    public void setTmxFileName(String tmxFileName)
    {
        this.tmxFileName = tmxFileName;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

}
