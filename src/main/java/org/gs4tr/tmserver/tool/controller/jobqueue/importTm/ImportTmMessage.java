package org.gs4tr.tmserver.tool.controller.jobqueue.importTm;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImportTmMessage
{
    private ImportTmPhase importTmPhase;

    public ImportTmMessage(ImportTmPhase importTmPhase)
    {
        this.importTmPhase = importTmPhase;
    }

    public ImportTmPhase getImportTmPhase()
    {
        return importTmPhase;
    }

    public void setImportTmPhase(ImportTmPhase importTmPhase)
    {
        this.importTmPhase = importTmPhase;
    }
}
