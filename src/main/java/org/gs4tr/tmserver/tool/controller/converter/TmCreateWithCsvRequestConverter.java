package org.gs4tr.tmserver.tool.controller.converter;

import java.io.IOException;

import org.gs4tr.tmserver.tool.controller.model.TmCreateWithCsvRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class TmCreateWithCsvRequestConverter implements Converter<String, TmCreateWithCsvRequest>
{
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public TmCreateWithCsvRequest convert(String source)
    {
        TmCreateWithCsvRequest tmCreateWithCsvRequest = null;
        try
        {
            tmCreateWithCsvRequest = objectMapper.readValue(source, TmCreateWithCsvRequest.class);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        return tmCreateWithCsvRequest;
    }

}
