package org.gs4tr.tmserver.tool.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmExportCacheDeleteResponse
{
    private String jobId;
    private String tmPath;
    private String message;

    public String getJobId()
    {
        return jobId;
    }

    public void setJobId(String jobId)
    {
        this.jobId = jobId;
    }

    public String getTmPath()
    {
        return tmPath;
    }

    public void setTmPath(String tmPath)
    {
        this.tmPath = tmPath;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public void clear()
    {
        this.jobId = null;
        this.tmPath = null;
        this.message = null;
    }
}
