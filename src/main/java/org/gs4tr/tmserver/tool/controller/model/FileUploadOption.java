package org.gs4tr.tmserver.tool.controller.model;

import org.json.JSONArray;
import org.springframework.web.multipart.MultipartFile;

public class FileUploadOption
{
    private MultipartFile[] uploadedFiles;
    private String type;
    private JSONArray selectedTmRows;
    private String randomToken;

    public MultipartFile[] getUploadedFiles()
    {
        return uploadedFiles;
    }

    public void setUploadedFiles(MultipartFile[] uploadedFiles)
    {
        this.uploadedFiles = uploadedFiles;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public JSONArray getSelectedTmRows()
    {
        return selectedTmRows;
    }

    public void setSelectedTmRows(JSONArray selectedTmRows)
    {
        this.selectedTmRows = selectedTmRows;
    }

    public String getRandomToken()
    {
        return randomToken;
    }

    public void setRandomToken(String randomToken)
    {
        this.randomToken = randomToken;
    }
}
