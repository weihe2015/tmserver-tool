package org.gs4tr.tmserver.tool.controller.model;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.model.createTm.TmGenericUser;
import org.gs4tr.tmserver.tool.service.file.Constants;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmCreateOption
{
    private DropdownOption selectedOrganization;
    private DropdownOption selectedTmGroup;
    private String sourceLanguage;
    private String targetLanguage;
    private List<String> targetLanguages;
    private String tmName;
    private String tmShortCode;
    private String genericUsername;
    private String genericUserpassword;
    private String cleanupCode;
    private boolean invertedLanguage;
    private boolean appendLanguagesToTmName;
    private boolean allGenericUserpasswordSame;
    private boolean allTmCleanupPasscodeSame;
    private boolean enabled;
    private boolean contextModeEnabled;
    private TmGenericUser user;

    // Fields for PD CSV Creation:
    private boolean pdCsvCreationEnabled;
    private String cleanupUsername;
    private String paClientName;
    private boolean addFileName;
    private boolean useGenericUsernameAsProfileName;
    private boolean emptyCleanupUserInfo;

    private boolean createTmWithCSV;

    private String accessToken;

    public DropdownOption getSelectedOrganization()
    {
        return selectedOrganization;
    }

    public void setSelectedOrganization(DropdownOption selectedOrganization)
    {
        this.selectedOrganization = selectedOrganization;
    }

    public DropdownOption getSelectedTmGroup()
    {
        return selectedTmGroup;
    }

    public void setSelectedTmGroup(DropdownOption selectedTmGroup)
    {
        this.selectedTmGroup = selectedTmGroup;
    }

    public String getSourceLanguage()
    {
        return sourceLanguage;
    }

    public void setSourceLanguage(String sourceLanguage)
    {
        this.sourceLanguage = handleLocaleCode(sourceLanguage);
    }

    public String getTargetLanguage()
    {
        return targetLanguage;
    }

    public void setTargetLanguage(String targetLanguage)
    {
        this.targetLanguage = handleLocaleCode(targetLanguage);
    }

    public List<String> getTargetLanguages()
    {
        return targetLanguages;
    }

    public void setTargetLanguages(List<String> targetLanguages)
    {
        this.targetLanguages = targetLanguages;
    }

    public String getTmName()
    {
        return tmName;
    }

    public void setTmName(String tmName)
    {
        this.tmName = tmName;
    }

    public void setTmNameWithLanguagesCode()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(tmName).append(Constants.UNDERSCORE);
        sb.append(sourceLanguage).append(Constants.UNDERSCORE);
        sb.append(targetLanguage);
        tmName = sb.toString();
    }

    public String getTmShortCode()
    {
        return tmShortCode;
    }

    public void setTmShortCode(String tmShortCode)
    {
        this.tmShortCode = tmShortCode;
    }

    public String getGenericUsername()
    {
        return genericUsername;
    }

    public void setGenericUsername(String genericUsername)
    {
        this.genericUsername = genericUsername;
    }

    public String getGenericUserpassword()
    {
        return genericUserpassword;
    }

    public void setGenericUserpassword(String genericUserpassword)
    {
        this.genericUserpassword = genericUserpassword;
    }

    public String getCleanupCode()
    {
        return cleanupCode;
    }

    public void setCleanupCode(String cleanupCode)
    {
        this.cleanupCode = cleanupCode;
    }

    public boolean isInvertedLanguage()
    {
        return invertedLanguage;
    }

    public void setInvertedLanguage(boolean invertedLanguage)
    {
        this.invertedLanguage = invertedLanguage;
    }

    public boolean isAppendLanguagesToTmName()
    {
        return appendLanguagesToTmName;
    }

    public void setAppendLanguagesToTmName(boolean appendLanguagesToTmName)
    {
        this.appendLanguagesToTmName = appendLanguagesToTmName;
    }

    public boolean isAllGenericUserpasswordSame()
    {
        return allGenericUserpasswordSame;
    }

    public void setAllGenericUserpasswordSame(
        boolean allGenericUserpasswordSame)
    {
        this.allGenericUserpasswordSame = allGenericUserpasswordSame;
    }

    public boolean isAllTmCleanupPasscodeSame()
    {
        return allTmCleanupPasscodeSame;
    }

    public void setAllTmCleanupPasscodeSame(boolean allTmCleanupPasscodeSame)
    {
        this.allTmCleanupPasscodeSame = allTmCleanupPasscodeSame;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isContextModeEnabled()
    {
        return contextModeEnabled;
    }

    public void setContextModeEnabled(boolean contextModeEnabled)
    {
        this.contextModeEnabled = contextModeEnabled;
    }

    public boolean isPdCsvCreationEnabled()
    {
        return pdCsvCreationEnabled;
    }

    public void setPdCsvCreationEnabled(boolean pdCsvCreationEnabled)
    {
        this.pdCsvCreationEnabled = pdCsvCreationEnabled;
    }

    public String getCleanupUsername()
    {
        return cleanupUsername;
    }

    public void setCleanupUsername(String cleanupUsername)
    {
        this.cleanupUsername = cleanupUsername;
    }

    public String getPaClientName()
    {
        if (StringUtils.isNotEmpty(paClientName))
        {
            return paClientName;
        }
        else
        {
            return StringUtils.EMPTY;
        }
    }

    public void setPaClientName(String paClientName)
    {
        this.paClientName = paClientName;
    }

    public boolean isAddFileName()
    {
        return addFileName;
    }

    public void setAddFileName(boolean addFileName)
    {
        this.addFileName = addFileName;
    }

    public boolean isUseGenericUsernameAsProfileName()
    {
        return useGenericUsernameAsProfileName;
    }

    public void setUseGenericUsernameAsProfileName(
        boolean useGenericUsernameAsProfileName)
    {
        this.useGenericUsernameAsProfileName = useGenericUsernameAsProfileName;
    }

    public TmGenericUser getUser()
    {
        return user;
    }

    public void setUser(TmGenericUser user)
    {
        this.user = user;
    }

    public void setGenericUsernameWithLocales()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(genericUsername);
        sb.append(Constants.UNDERSCORE);
        sb.append(sourceLanguage);
        sb.append(Constants.UNDERSCORE);
        sb.append(targetLanguage);
        user.setUsername(sb.toString());
    }

    private void setGenericUsername()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(genericUsername);
        user.setUsername(sb.toString());
    }

    public void updateGenericUserPassword(String password)
    {
        // Create new TM GenericUser each time when a new password is generated.
        user = new TmGenericUser();
        // update generic username to be TM name + sourceLocale + targetLocale
        // Only update generic username to be TM name + sourceLocale + targetLocale
        // when create TM, but not createTmWithCSV:
        if (createTmWithCSV)
        {
            setGenericUsername();
        }
        else
        {
            setGenericUsernameWithLocales();
        }
        DropdownOption dropdownOption = new DropdownOption();
        dropdownOption.setLabel("read/write");
        dropdownOption.setValue("read/write");
        user.setRole(dropdownOption);
        user.setPassword(password);
        setGenericUserpassword(password);
    }

    /**
     * This function helps to fix locale provided by user. The locale must be language code in lower
     * case and country code in upper case. Ex: zh-CN, en-US Locale code: zh-cn will be fixed with
     * zh-CN
     *
     * @param String locale
     * @return locale in valid format
     */
    private String handleLocaleCode(String locale)
    {
        // Assume input locale is either two characters zh or five characters zh-CN
        // language is in lowercase and country is in upper case.
        StringBuffer sb = new StringBuffer();
        if (locale.indexOf('-') > 0)
        {
            String language = locale.substring(0, locale.indexOf('-'));
            String country = locale.substring(locale.indexOf('-'));
            sb.append(language.toLowerCase()).append(country.toUpperCase());
        }
        else
        {
            sb.append(locale.toLowerCase());
        }
        return sb.toString();
    }

    public boolean isCreateTMWithCSV()
    {
        return createTmWithCSV;
    }

    public void setCreateTMWithCSV(boolean createTMWithCSV)
    {
        this.createTmWithCSV = createTMWithCSV;
    }

    public boolean isEmptyCleanupUserInfo()
    {
        return emptyCleanupUserInfo;
    }

    public void setEmptyCleanupUserInfo(boolean emptyCleanupUserInfo)
    {
        this.emptyCleanupUserInfo = emptyCleanupUserInfo;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }
}
