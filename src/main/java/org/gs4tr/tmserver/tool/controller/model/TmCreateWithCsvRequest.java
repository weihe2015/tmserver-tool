package org.gs4tr.tmserver.tool.controller.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmCreateWithCsvRequest
{
    private DropdownOption selectedOrganization;
    private DropdownOption selectedTmGroup;
    private List<DropdownOption> selectedTmGroups;
    private boolean enabled;
    private boolean contextModeEnabled;
    private boolean createTmInMultipleTmGroup;

    // Fields for PD CSV Creation:
    private boolean pdCsvCreationEnabled;
    private String cleanupUsername;
    private String paClientName;
    private boolean addFileName;
    private boolean useGenericUsernameAsProfileName;
    private boolean emptyCleanupUserInfo;

    private String accessToken;

    public DropdownOption getSelectedOrganization()
    {
        return selectedOrganization;
    }

    public void setSelectedOrganization(DropdownOption selectedOrganization)
    {
        this.selectedOrganization = selectedOrganization;
    }

    public DropdownOption getSelectedTmGroup()
    {
        return selectedTmGroup;
    }

    public void setSelectedTmGroup(DropdownOption selectedTmGroup)
    {
        this.selectedTmGroup = selectedTmGroup;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isContextModeEnabled()
    {
        return contextModeEnabled;
    }

    public void setContextModeEnabled(boolean contextModeEnabled)
    {
        this.contextModeEnabled = contextModeEnabled;
    }

    public boolean isCreateTmInMultipleTmGroup()
    {
        return createTmInMultipleTmGroup;
    }

    public void setCreateTmInMultipleTmGroup(boolean createTmInMultipleTmGroup)
    {
        this.createTmInMultipleTmGroup = createTmInMultipleTmGroup;
    }

    public boolean isPdCsvCreationEnabled()
    {
        return pdCsvCreationEnabled;
    }

    public void setPdCsvCreationEnabled(boolean pdCsvCreationEnabled)
    {
        this.pdCsvCreationEnabled = pdCsvCreationEnabled;
    }

    public String getCleanupUsername()
    {
        return cleanupUsername;
    }

    public void setCleanupUsername(String cleanupUsername)
    {
        this.cleanupUsername = cleanupUsername;
    }

    public String getPaClientName()
    {
        return paClientName;
    }

    public void setPaClientName(String paClientName)
    {
        this.paClientName = paClientName;
    }

    public boolean isAddFileName()
    {
        return addFileName;
    }

    public void setAddFileName(boolean addFileName)
    {
        this.addFileName = addFileName;
    }

    public boolean isUseGenericUsernameAsProfileName()
    {
        return useGenericUsernameAsProfileName;
    }

    public void setUseGenericUsernameAsProfileName(boolean useGenericUsernameAsProfileName)
    {
        this.useGenericUsernameAsProfileName = useGenericUsernameAsProfileName;
    }

    public boolean isEmptyCleanupUserInfo()
    {
        return emptyCleanupUserInfo;
    }

    public void setEmptyCleanupUserInfo(boolean emptyCleanupUserInfo)
    {
        this.emptyCleanupUserInfo = emptyCleanupUserInfo;
    }

    public List<DropdownOption> getSelectedTmGroups()
    {
        return selectedTmGroups;
    }

    public void setSelectedTmGroups(List<DropdownOption> selectedTmGroups)
    {
        this.selectedTmGroups = selectedTmGroups;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }
}
