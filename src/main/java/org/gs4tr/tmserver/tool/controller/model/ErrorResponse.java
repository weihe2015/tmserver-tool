package org.gs4tr.tmserver.tool.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class ErrorResponse
{
    private String warn;
    private String global;
    private String searchString;

    public String getGlobal()
    {
        return global;
    }

    public void setGlobal(String global)
    {
        this.global = global;
    }

    public String getWarn()
    {
        return warn;
    }

    public void setWarn(String warn)
    {
        this.warn = warn;
    }

    public String getSearchString()
    {
        return searchString;
    }

    public void setSearchString(String searchString)
    {
        this.searchString = searchString;
    }
}
