package org.gs4tr.tmserver.tool.controller.jobqueue.exportTm;

public enum ExportTmPhase
{
    PENDING, WAITING, START, FINISHED, FAILED;
}
