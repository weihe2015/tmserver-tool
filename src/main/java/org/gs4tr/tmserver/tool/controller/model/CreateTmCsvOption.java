package org.gs4tr.tmserver.tool.controller.model;

import java.util.List;

public class CreateTmCsvOption
{
    private DropdownOption selectedOrganization;
    private boolean includeOnlyEnabledTm;
    private List<DropdownOption> selectedTmGroup;

    public boolean isIncludeOnlyEnabledTm()
    {
        return includeOnlyEnabledTm;
    }

    public void setIncludeOnlyEnabledTm(boolean includeOnlyEnabledTm)
    {
        this.includeOnlyEnabledTm = includeOnlyEnabledTm;
    }

    public List<DropdownOption> getSelectedTmGroup()
    {
        return selectedTmGroup;
    }

    public void setSelectedTmGroup(List<DropdownOption> selectedTmGroup)
    {
        this.selectedTmGroup = selectedTmGroup;
    }

    public DropdownOption getSelectedOrganization()
    {
        return selectedOrganization;
    }

    public void setSelectedOrganization(DropdownOption selectedOrganization)
    {
        this.selectedOrganization = selectedOrganization;
    }
}
