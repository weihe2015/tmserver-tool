package org.gs4tr.tmserver.tool.controller.mvc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.TmInfo;
import org.gs4tr.tmserver.tool.api.model.TmListResponse;
import org.gs4tr.tmserver.tool.controller.TmServerToolController;
import org.gs4tr.tmserver.tool.controller.model.ErrorResponse;
import org.gs4tr.tmserver.tool.controller.model.TmSearchOption;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.model.concordance.TmSearchResult;
import org.gs4tr.tmserver.tool.model.swagger.JsonErrorMessage;
import org.gs4tr.tmserver.tool.model.swagger.SearchResultInfo;
import org.gs4tr.tmserver.tool.model.thread.ConcordanceThread;
import org.gs4tr.tmserver.tool.service.file.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Concordance Search REST API endpoint")
public class TmConcordanceController extends TmServerToolController
{
    private static final Logger LOG = LoggerFactory.getLogger(TmConcordanceController.class);

    @Autowired
    private TmServerApi tmServerApi;

    @ApiOperation(value = "REST API of accepting values for concordance search in TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Search Results are returned to user.", response = SearchResultInfo.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/tus")
    public ResponseEntity<?> searchTuByKeywords(
        @ApiParam("Authorization Token stored in HTTP request header") @RequestHeader(value = "Authorization") final String token,
        @ApiParam(value = "JSONObject in String containing search option") @RequestBody final TmSearchOption tmSearchOption)
        throws IOException
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(token);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);

        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);

        TmListResponse response;
        if (tmsCredential.containsKey("limited"))
        {
            response = tmServerApi.fetchTmListWithTmGroupCodes(tmServerInfo, getTmGroupShortCodes());
        }
        else
        {
            response = getTmList(tmsCredential, tmServerInfo);
        }

        if (response == null || response.getTmList().isEmpty())
        {
            String errorMessage = Messages.getString("ConcordanceSearchController.0");
            LOG.warn(errorMessage);

            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }

        List<TmSearchResult> searchResult = Collections.synchronizedList(new LinkedList<TmSearchResult>());
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);

        Set<String> selectedTmSet = getSelectedTmSet(tmSearchOption.getSelectedTmList());
        for (TmInfo tmInfo : response.getTmList())
        {
            if (!selectedTmSet.contains(tmInfo.getTmPath()))
            {
                continue;
            }
            Runnable concordanceThread = new ConcordanceThread(tmInfo, tmServerApi, tmServerInfo, searchResult,
                tmSearchOption, isTPAuth);
            executor.execute(concordanceThread);
        }

        executor.shutdown();
        // Wait until all threads are finish
        while (!executor.isTerminated())
        {

        }
        List<TmSearchResult> results = new ArrayList<>();
        if (searchResult.isEmpty())
        {
            String errorMessage = String.format(Messages.getString("ConcordanceSearchController.1"),
                tmSearchOption.getQuery());
            LOG.warn(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            errorResponse.setGlobal(errorMessage);
            errorResponse.setSearchString(tmSearchOption.getQuery());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        else
        {
            for (TmSearchResult rs : searchResult)
            {
                results.add(rs);
            }
            return ResponseEntity.ok(results);
        }
    }
}
