package org.gs4tr.tmserver.tool.controller.jobqueue.exportTm;

import java.io.IOException;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.LoginResponse;
import org.gs4tr.tmserver.tool.controller.jobqueue.Task;
import org.gs4tr.tmserver.tool.controller.jobqueue.TaskPriority;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.ExportInfoCache;
import org.gs4tr.tmserver.tool.controller.model.GenericResponse;
import org.gs4tr.tmserver.tool.controller.model.TmExportOption;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.service.jwt.JWTVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExportTmTask extends Task
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportTmTask.class);

    private final TmServerInfo tmServerInfo;
    private final TmExportOption tmExportOption;
    private final boolean isTPAuth;
    private final TmServerApi tmServerApi;
    private ExportInfoCache exportInfoCache;

    private final JWTVerifier jwtVerifier;

    public ExportTmTask(String jobId, TaskPriority taskPriority, TmServerInfo tmServerInfo,
        TmExportOption tmExportOption, boolean isTPAuth, TmServerApi tmServerApi,
        ExportInfoCache exportInfoCache, JWTVerifier jwtVerifier)
    {
        super(jobId, taskPriority);
        this.tmServerInfo = tmServerInfo;
        this.tmExportOption = tmExportOption;
        this.isTPAuth = isTPAuth;
        this.tmServerApi = tmServerApi;
        this.exportInfoCache = exportInfoCache;
        this.jwtVerifier = jwtVerifier;
    }

    @Override
    public void run()
    {
        String tmPath = tmExportOption.getTmPath();
        String jobId = getJobId();
        ExportTmInfo info = exportInfoCache.get(jobId);
        if (info == null)
        {
            return;
        }
        info.setExportTmPhaseStart();
        exportInfoCache.put(jobId, info);

        try
        {
            String securityTicket = null;
            String accessToken = null;
            boolean oauth = isTPAuth;
            if (!isTPAuth)
            {
                LoginResponse response = tmServerApi.login(tmServerInfo, tmPath);
                if (response != null)
                {
                    securityTicket = response.getSecurityTicket();
                }
            }
            else
            {
                accessToken = info.getAccessToken();
                boolean tokenExpired = jwtVerifier.isTokenExpired(accessToken);
                // If token is expired, we use default power user to access TMs.
                if (tokenExpired)
                {
                    accessToken = null;
                    LoginResponse response = tmServerApi.login(tmServerInfo, tmPath);
                    if (response != null)
                    {
                        securityTicket = response.getSecurityTicket();
                    }
                    oauth = false;
                }
            }

            tmServerApi.exportTm(tmServerInfo, tmExportOption, securityTicket, accessToken, info);
            if (!oauth)
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
            String message = String.format(Messages.getString("ExportTmController.5"), tmExportOption.getTmName(), tmExportOption.getTmPath());
            LOGGER.info(message);

            // Cache this response Entity
            info.setExportTmPhaseFinished();
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("ExportTmController.0"), ex.getMessage());
            LOGGER.error(errorMessage);
            GenericResponse genericResponse = new GenericResponse();
            genericResponse.setGlobal(errorMessage);
            info.setExportTmPhaseFailed();
        }
        finally
        {
            exportInfoCache.put(jobId, info);
        }
    }
}
