package org.gs4tr.tmserver.tool.controller.model;

import java.io.Serializable;

public class TmImportOption implements Serializable
{
    private static final long serialVersionUID = -415611640790255538L;
    private String tmName;
    private String tmPath;
    private String tmImportOption;
    private String tmAttributeImportOption;
    private String randomToken;
    private String folderLocales;
    private boolean ignoreInvalidTus;
    private boolean keepNewerTu;
    private String accessToken;

    public boolean isIgnoreInvalidTus()
    {
        return ignoreInvalidTus;
    }

    public void setIgnoreInvalidTus(boolean ignoreInvalidTus)
    {
        this.ignoreInvalidTus = ignoreInvalidTus;
    }

    public boolean isKeepNewerTu()
    {
        return keepNewerTu;
    }

    public void setKeepNewerTu(boolean keepNewerTu)
    {
        this.keepNewerTu = keepNewerTu;
    }

    public String getTmName()
    {
        return tmName;
    }

    public void setTmName(String tmName)
    {
        this.tmName = tmName;
    }

    public String getTmPath()
    {
        return tmPath;
    }

    public void setTmPath(String tmPath)
    {
        this.tmPath = tmPath;
    }

    public String getTmImportOption()
    {
        return tmImportOption;
    }

    public void setTmImportOption(String tmImportOption)
    {
        this.tmImportOption = tmImportOption;
    }

    public String getTmAttributeImportOption()
    {
        return tmAttributeImportOption;
    }

    public void setTmAttributeImportOption(String tmAttributeImportOption)
    {
        this.tmAttributeImportOption = tmAttributeImportOption;
    }

    public String getRandomToken()
    {
        return randomToken;
    }

    public void setRandomToken(String randomToken)
    {
        this.randomToken = randomToken;
    }

    public String getFolderLocales()
    {
        return folderLocales;
    }

    public void setFolderLocales(String folderLocales)
    {
        this.folderLocales = folderLocales;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }
}
