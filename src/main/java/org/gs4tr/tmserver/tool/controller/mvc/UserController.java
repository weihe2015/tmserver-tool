package org.gs4tr.tmserver.tool.controller.mvc;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.TmListResponse;
import org.gs4tr.tmserver.tool.api.model.ValidateOAuthTokenResponse;
import org.gs4tr.tmserver.tool.configuration.RedisConfiguration;
import org.gs4tr.tmserver.tool.controller.TmServerToolController;
import org.gs4tr.tmserver.tool.controller.model.ErrorResponse;
import org.gs4tr.tmserver.tool.controller.model.GenericResponse;
import org.gs4tr.tmserver.tool.controller.model.LogoutRequest;
import org.gs4tr.tmserver.tool.controller.model.RemoteTmServerCredential;
import org.gs4tr.tmserver.tool.controller.model.TPAuthLoginOption;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.model.auth.JwtAuthenticationRequest;
import org.gs4tr.tmserver.tool.model.auth.JwtAuthenticationResponse;
import org.gs4tr.tmserver.tool.model.swagger.JsonErrorMessage;
import org.gs4tr.tmserver.tool.model.user.TmUser;
import org.gs4tr.tmserver.tool.service.user.UserService;
import org.gs4tr.tmserver.tool.session.ExtendedSessionInformation;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "REST Endpoint of user login/logout actions")
public class UserController extends TmServerToolController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.tmListKey}")
    private String tmListKey;

    @Value("${tmserver.url}")
    private String url;

    @Autowired
    private UserService userService;

    @Autowired
    private TmServerApi tmServerApi;

    @Autowired
    private RedisConfiguration redis;

    @ApiOperation(value = "REST API for connecting other TM Server with power user credential")
    @PostMapping("/api/v1/user/remote")
    public ResponseEntity<?> connectToTmServer(@RequestBody final RemoteTmServerCredential newTmServerInfo)
    {
        newTmServerInfo.setSid(getSessionId());
        TmServerInfo tmServerInfo = newTmServerInfo.createNewTmServerInfo();
        try
        {
            TmListResponse tmListResponse = tmServerApi.fetchTmList(tmServerInfo);
            if (tmListResponse == null || !tmListResponse.isSuccess() || tmListResponse.getTmList().isEmpty())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.16"), tmServerInfo.getURL());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            else
            {
                String url = newTmServerInfo.getUrl();
                // handle tmList with remote Tm Server
                storeTmList(url, tmListResponse);

                // Generate a token with url, username, password and session id
                // sent back to client:
                final String token = userService.generateToken(newTmServerInfo);
                return ResponseEntity.ok(token);
            }
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.17"), ex.getMessage());
            LOGGER.error(errorMessage);

            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }

    @ApiOperation(value = "REST API for user login to TM Server Tool")
    @RequestMapping(value = { "/api/v1/user/login" }, method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenicationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException
    {
        // 6-1-18: Petr asks for login with TMS Credential.
        // So we use these credential with current TM_USER_PROFILE to get the User;

        String sessionId = getSessionId();
        String powerUsername = authenticationRequest.getUsername();
        String powerUserpassword = authenticationRequest.getPassword();

        try
        {
            TmUser prevUser = userService.findUserByUsername(powerUsername);
            if (prevUser == null)
            {
                String errorMessage = String.format(Messages.getString("UserController.0"), powerUsername);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            else if (prevUser.isSSOUser())
            {
                String errorMessage = String.format(Messages.getString("UserController.14"), prevUser.getUsername());
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            else if (!userService.comparePassword(prevUser, powerUserpassword))
            {
                String errorMessage = Messages.getString("UserController.1");
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            else if (StringUtils.equals("admin", powerUsername))
            {
                final String token = userService.generateToken(_tmPowerUserName, _tmPowerUserPassword, sessionId);
                ExtendedSessionInformation session = createNewSessionInfo(new JSONObject().toString());
                addSessionInformation(sessionId, session);
                String message = String.format(Messages.getString("UserController.2"), powerUsername, session.getUserAddress(), session.getSessionId());
                LOGGER.info(message);
                // Return the token
                return ResponseEntity.ok(new JwtAuthenticationResponse(token));
            }
            else
            {
                // 8-29-18: allow non power user to login, but only to search Tm feature
                if (!prevUser.isPowerUser())
                {
                    // err.put("global", Messages.getString("UserController.3"));
                    // return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err.toString());
                    final String token = userService.generateTokenSearchOnly(_tmPowerUserName, _tmPowerUserPassword, sessionId);

                    ExtendedSessionInformation session = createNewSessionInfo(new JSONObject().toString());
                    addSessionInformation(sessionId, session);

                    String message = String.format(Messages.getString("UserController.2"),
                        prevUser.getFullName(), session.getUserAddress(), session.getSessionId());
                    LOGGER.info(message);

                    Map<String, Object> tmsCredential = new LinkedHashMap<>();
                    TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
                    TmListResponse tmListResponse = tmServerApi.fetchTmListWithTmGroupCodes(tmServerInfo, getTmGroupShortCodes());
                    // Save to cache
                    String key = url + "-TmList-limited";
                    storeTmList(key, tmListResponse);
                    // Return the token
                    return ResponseEntity.ok(new JwtAuthenticationResponse(token));
                }
                else
                {
                    final String token = userService.login(powerUsername, powerUserpassword, sessionId);
                    final UserDetails userDetails = userService.getUserDetailsByUsername(powerUsername);
                    addUserInfoCache(sessionId, userDetails);

                    ExtendedSessionInformation session = createNewSessionInfo(powerUsername);
                    addSessionInformation(sessionId, session);

                    String message = String.format(Messages.getString("UserController.2"),
                        prevUser.getFullName(), session.getUserAddress(), session.getSessionId());
                    LOGGER.info(message);
                    return ResponseEntity.ok(new JwtAuthenticationResponse(token));
                }
            }
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("UserController.5"), ex.getMessage());
            LOGGER.error(errorMessage);

            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }

    @ApiOperation(value = "REST API for user login to TM Server Tool")
    @RequestMapping(value = { "/api/v1/user/oauthlogin" }, method = RequestMethod.POST)
    public ResponseEntity<?> createOAuthAuthenicationToken(@RequestBody final TPAuthLoginOption tpAuthLoginOption)
    {
        try
        {
            TmServerInfo tmServerInfo = new TmServerInfo();
            tmServerInfo.setUrl(url);
            ValidateOAuthTokenResponse oauthValidateResponse = tmServerApi.validateOAuthToken(tmServerInfo, tpAuthLoginOption.getAccessToken());

            if (!oauthValidateResponse.isSuccess())
            {
                String errorMessage = Messages.getString("UserController.12");
                GenericResponse genericResponse = createErrorMessage(errorMessage);
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(genericResponse);
            }

            String sessionId = getSessionId();
            final String token = userService.generateOAuthToken(tpAuthLoginOption, sessionId);
            ExtendedSessionInformation session = createNewSessionInfo(new JSONObject().toString());
            addSessionInformation(sessionId, session);
            String message = String.format(Messages.getString("UserController.11"), tpAuthLoginOption.getEmail(), session.getUserAddress(), session.getSessionId());
            LOGGER.info(message);
            // Return the token
            return ResponseEntity.ok(new JwtAuthenticationResponse(token));
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("UserController.13"), ex.getMessage());
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }


    @ApiOperation(value = "REST API for user logout to TM Server Tool")
    @PostMapping("/api/v1/user/logout")
    public ResponseEntity<?> logout(@RequestBody final LogoutRequest logoutRequest)
    {
        String sessionId = logoutRequest.getSid();
        ExtendedSessionInformation session = (ExtendedSessionInformation) getSessionInformation(sessionId);
        if (session == null)
        {
            String errorMessage = String.format(Messages.getString("UserController.6"), sessionId);
            LOGGER.error(errorMessage);

            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        String message = String.format(Messages.getString("UserController.7"), session.getUserAddress(), session.getSessionId());
        LOGGER.info(message);
        removeSessionInformation(sessionId);
        SecurityContextHolder.clearContext();
        clearServerCache(sessionId);
        return ResponseEntity.ok(StringUtils.EMPTY);
    }

    @ApiOperation(value = "REST API of pre loading TM List from TM Server to TM Server Tool")
    @ApiResponses({
        @ApiResponse(code = 200, message = "TM List is successfully preloaded", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @RequestMapping(value = { "/api/v1/list" }, method = RequestMethod.GET)
    public ResponseEntity<String> preLoadTmList(HttpServletRequest request)
    {
        String authToken = request.getParameter("key");
        if (StringUtils.equals(authToken, tmListKey))
        {
            try
            {
                Map<String, Object> tmsCredential = new LinkedHashMap<>();
                TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);

                TmListResponse tmListResponse = tmServerApi.fetchTmList(tmServerInfo);

                // Save this Tm List into redis in separated thread:
                ExecutorService service = Executors.newCachedThreadPool();
                CompletableFuture.runAsync(() -> {
                    Semaphore semaphore = new Semaphore(1);
                    semaphore.acquireUninterruptibly();

                    String key = url + "-TmList";
                    if (!redis.isReachable())
                    {
                        storeTmList(key, tmListResponse);
                    }
                    else
                    {
                        redis.saveTmList(key, tmListResponse);
                    }
                    semaphore.release();
                }, service);

                return ResponseEntity.ok("Preload Success");
            }
            catch (IOException ex)
            {
                String errorMessage = String.format(Messages.getString("UserController.1"), ex.getMessage());
                LOGGER.error(errorMessage);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
            }
        }
        else
        {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Messages.getString("UserController.3"));
        }
    }

    @ApiOperation(value = "REST API of refreshing user token of TM Server Tool")
    @RequestMapping(value = { "/api/v1/user/token" }, method = RequestMethod.PUT)
    public ResponseEntity<?> refreshAuthenticationToken(HttpServletRequest request) throws AuthenticationException
    {
        String token = request.getHeader(tokenHeader);
        String refreshedToken = userService.refreshToken(token);
        if (refreshedToken == null)
        {
            return ResponseEntity.badRequest().body(null);
        }
        else
        {
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        }
    }
}
