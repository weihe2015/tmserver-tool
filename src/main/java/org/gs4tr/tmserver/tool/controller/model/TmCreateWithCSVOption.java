package org.gs4tr.tmserver.tool.controller.model;

import java.util.List;

public class TmCreateWithCSVOption
{
    private DropdownOption selectedOrganization;
    private DropdownOption selectedTmGroup;
    private List<DropdownOption> selectedTmGroups;
    private boolean enabled;
    private boolean contextModeEnabled;
    private boolean createTmInMultipleTmGroup;

    public DropdownOption getSelectedOrganization()
    {
        return selectedOrganization;
    }

    public void setSelectedOrganization(DropdownOption selectedOrganization)
    {
        this.selectedOrganization = selectedOrganization;
    }

    public DropdownOption getSelectedTmGroup()
    {
        return selectedTmGroup;
    }

    public void setSelectedTmGroup(DropdownOption selectedTmGroup)
    {
        this.selectedTmGroup = selectedTmGroup;
    }

    public List<DropdownOption> getSelectedTmGroups()
    {
        return selectedTmGroups;
    }

    public void setSelectedTmGroups(List<DropdownOption> selectedTmGroups)
    {
        this.selectedTmGroups = selectedTmGroups;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isContextModeEnabled()
    {
        return contextModeEnabled;
    }

    public void setContextModeEnabled(boolean contextModeEnabled)
    {
        this.contextModeEnabled = contextModeEnabled;
    }

    public boolean isCreateTmInMultipleTmGroup()
    {
        return createTmInMultipleTmGroup;
    }

    public void setCreateTmInMultipleTmGroup(boolean createTmInMultipleTmGroup)
    {
        this.createTmInMultipleTmGroup = createTmInMultipleTmGroup;
    }
}
