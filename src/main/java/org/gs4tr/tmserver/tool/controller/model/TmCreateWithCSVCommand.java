package org.gs4tr.tmserver.tool.controller.model;

import org.springframework.web.multipart.MultipartFile;

public class TmCreateWithCSVCommand
{
    private MultipartFile[] uploadedFiles;
    private TmCreateWithCSVOption tmCreateWithCSVOption;

    public MultipartFile[] getUploadedFiles()
    {
        return uploadedFiles;
    }

    public void setUploadedFiles(MultipartFile[] uploadedFiles)
    {
        this.uploadedFiles = uploadedFiles;
    }

    public TmCreateWithCSVOption getTmCreateWithCSVOption()
    {
        return tmCreateWithCSVOption;
    }

    public void setTmCreateWithCSVOption(TmCreateWithCSVOption tmCreateWithCSVOption)
    {
        this.tmCreateWithCSVOption = tmCreateWithCSVOption;
    }
}
