package org.gs4tr.tmserver.tool.controller.jobqueue.cache;

import org.gs4tr.tmserver.tool.api.model.TmListResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

@Component
public class TmInfoListCache
{
    static public final String CACHE_ID = "TmInfoListCache";
    static public final String CACHE_MANAGER_ID = "serviceCacheManager";
    static public final String CACHE_POOL_NAME = "TmInfoListCachePool";

    private final CacheManager _cacheManager;

    @Autowired
    public TmInfoListCache(@Qualifier(CACHE_MANAGER_ID) CacheManager cacheManager)
    {
        _cacheManager = cacheManager;
    }

    public TmListResponse get(String key)
    {
        Element element = getCache().get(key);
        if (element == null || element.isExpired())
        {
            return null;
        }
        return (TmListResponse) element.getObjectValue();
    }

    public void put(String key, TmListResponse info)
    {
        getCache().put(new Element(key, info));
    }

    private Cache getCache()
    {
        return getCacheManager().getCache(CACHE_ID);
    }

    private CacheManager getCacheManager()
    {
        return _cacheManager;
    }

}
