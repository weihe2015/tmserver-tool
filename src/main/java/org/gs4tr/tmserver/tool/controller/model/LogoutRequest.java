package org.gs4tr.tmserver.tool.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class LogoutRequest
{
    private String endSessionURL;
    private String sid;

    public String getSid()
    {
        return sid;
    }

    public void setSid(String sid)
    {
        this.sid = sid;
    }

    public String getEndSessionURL()
    {
        return endSessionURL;
    }

    public void setEndSessionURL(String endSessionURL)
    {
        this.endSessionURL = endSessionURL;
    }
}
