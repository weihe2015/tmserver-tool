package org.gs4tr.tmserver.tool.controller.jobqueue.cache;

import org.gs4tr.tmserver.tool.controller.jobqueue.importTm.ImportTmInfos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

@Component
public class ImportInfoCache
{
    static public final String CACHE_ID = "importTmInfoCache";
    static public final String CACHE_MANAGER_ID = "serviceCacheManager";
    static public final String CACHE_POOL_NAME = "importTmInfoCachePool";

    private final CacheManager _cacheManager;

    @Autowired
    public ImportInfoCache(@Qualifier(CACHE_MANAGER_ID) CacheManager cacheManager)
    {
        _cacheManager = cacheManager;
    }

    public ImportTmInfos get(String key)
    {
        Element element = getCache().get(key);
        if (element == null || element.isExpired())
        {
            return null;
        }
        return (ImportTmInfos) element.getObjectValue();
    }

    public void put(String key, ImportTmInfos info)
    {
        getCache().put(new Element(key, info));
    }

    public boolean has(String key)
    {
        Element element = getCache().get(key);
        return element != null && !element.isExpired();
    }

    public void remove(String key)
    {
        getCache().remove(key);
    }

    private Cache getCache()
    {
        return getCacheManager().getCache(CACHE_ID);
    }

    private CacheManager getCacheManager()
    {
        return _cacheManager;
    }
}
