package org.gs4tr.tmserver.tool.controller.mvc;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.LoginResponse;
import org.gs4tr.tmserver.tool.controller.TmServerToolController;
import org.gs4tr.tmserver.tool.controller.jobqueue.Task;
import org.gs4tr.tmserver.tool.controller.jobqueue.TaskPriority;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.ExportInfoCache;
import org.gs4tr.tmserver.tool.controller.jobqueue.exportTm.ExportTmInfo;
import org.gs4tr.tmserver.tool.controller.jobqueue.exportTm.ExportTmPhase;
import org.gs4tr.tmserver.tool.controller.jobqueue.exportTm.ExportTmTask;
import org.gs4tr.tmserver.tool.controller.jobqueue.exportTm.ExportTmTaskScheduler;
import org.gs4tr.tmserver.tool.controller.model.ErrorResponse;
import org.gs4tr.tmserver.tool.controller.model.ExportTmStatus;
import org.gs4tr.tmserver.tool.controller.model.TmExportCacheDeleteResponse;
import org.gs4tr.tmserver.tool.controller.model.TmExportOption;
import org.gs4tr.tmserver.tool.controller.model.TmExportResponse;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.model.swagger.JsonErrorMessage;
import org.gs4tr.tmserver.tool.service.file.Constants;
import org.gs4tr.tmserver.tool.service.jwt.JWTVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "REST Endpoint of export TM")
public class TmExportController extends TmServerToolController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TmExportController.class);

    @Autowired
    private TmServerApi tmServerApi;

    @Autowired
    private JWTVerifier jwtVerifier;

    @Autowired
    private ExportTmTaskScheduler exportTmTaskScheduler;

    @Autowired
    private ExportInfoCache exportInfoCache;

    @Value("${tmserver.tmPowerUsername}")
    protected String _tmPowerUserName;

    @Value("${tmserver.tmPowerUserpassword}")
    protected String _tmPowerUserPassword;

    @ApiOperation(value = "REST API for exporting TM from TM Server")
    @ApiResponses({
        @ApiResponse(code = 201, message = "TM are exported from TM Server successfully", response = ResponseEntity.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping(path = "/api/v1/tms", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> exportTmPost(@RequestHeader(value = "Authorization") String authToken,
        @RequestBody final TmExportOption tmExportOption)
    {
        try
        {
            Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
            String accessToken = null;
            boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
            TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);

            String securityTicket = null;
            if (!isTPAuth)
            {
                LoginResponse response = tmServerApi.login(tmServerInfo, tmExportOption.getTmPath());
                if (response != null)
                {
                    securityTicket = response.getSecurityTicket();
                }
            }
            else
            {
                accessToken = tmExportOption.getAccessToken();
            }

            if (StringUtils.isNotEmpty(securityTicket) || isTPAuth)
            {
                ResponseEntity<?> respEntity = tmServerApi.exportTm(tmServerInfo, tmExportOption, securityTicket,
                    accessToken, null);
                if (!isTPAuth)
                {
                    tmServerApi.logout(tmServerInfo, securityTicket);
                }
                String message = String.format(Messages.getString("ExportTmController.5"), tmExportOption.getTmName(), tmExportOption.getTmPath());
                LOGGER.info(message);

                return respEntity;
            }
            else
            {
                String errorMessage = String.format(Messages.getString("ExportTmController.3"), tmExportOption.getTmPath());
                LOGGER.warn(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("ExportTmController.0"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }

    @ApiOperation(value = "REST API for exporting TM from TM Server")
    @ApiResponses(
    {
        @ApiResponse(code = 201, message = "TM are exported from TM Server successfully", response = ResponseEntity.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping(path = "/api/v2/tms", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<TmExportResponse> createExportTmRequest(@RequestHeader(value = "Authorization") String authToken,
        @RequestBody final TmExportOption tmExportOption)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        String accessToken = null;
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);

        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        if (isTPAuth)
        {
            accessToken = tmExportOption.getAccessToken();
        }

        String tmName = tmExportOption.getTmName();
        String tmPath = tmExportOption.getTmPath();
        final String jobId = UUID.randomUUID().toString();

        ExportTmInfo info = new ExportTmInfo(jobId);
        info.setAccessToken(accessToken);
        exportInfoCache.put(jobId, info);

        Task task = new ExportTmTask(jobId, TaskPriority.HIGH, tmServerInfo, tmExportOption, isTPAuth,
            tmServerApi, exportInfoCache, jwtVerifier);

        exportTmTaskScheduler.scheduleTask(task);

        String message = String.format(Messages.getString("ExportTmController.6"), tmName, tmPath);
        LOGGER.info(message);

        TmExportResponse tmExportResponse = new TmExportResponse();
        tmExportResponse.setJobId(jobId);
        tmExportResponse.setSuccess(true);
        tmExportResponse.setMessage(message);
        tmExportResponse.setStatus(ExportTmPhase.WAITING.toString());
        tmExportResponse.setExportToFtp(tmExportOption.isExportedToFtp());
        return ResponseEntity.status(HttpStatus.CREATED).body(tmExportResponse);
    }

    @ApiOperation(value = "REST API for getting export job status from serverr")
    @ApiResponses({
        @ApiResponse(code = 200, message = "TM export job status is successfully retrieved", response = ExportTmStatus.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping(path = "/api/v2/tms/job/{jobId}")
    public ResponseEntity<?> getExportTmStatus(@RequestHeader(value = "Authorization") final String authToken,
        @ApiParam(value = "Export TM Job Id", required = true) @PathVariable("jobId") final String jobId,
        @ApiParam(value = "TM Path that import file to", required = true) @RequestParam(value = "tmpath", required = true) String tmPath,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken)
    {
        // The jobId should be the UUID
        ExportTmStatus exportTmStatus = new ExportTmStatus();

        if (!exportInfoCache.has(jobId))
        {
            exportTmStatus.setStatus(ExportTmPhase.PENDING.toString());
            return ResponseEntity.status(HttpStatus.OK).body(exportTmStatus);
        }
        ExportTmInfo info = exportInfoCache.get(jobId);
        if (info.getExportTmPhase() == ExportTmPhase.FINISHED)
        {
            exportTmStatus.setStatus(ExportTmPhase.FINISHED.toString());

            if (info.isExportToFtp())
            {
                exportTmStatus.setMessage(info.getMessage());
                return ResponseEntity.status(HttpStatus.OK).body(exportTmStatus);
            }
            else
            {
                // redirect file to front end:
                String fileType = info.getFileType();
                byte[] out = info.getOut();
                String tmxFileName = info.getTmxFileName();
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.add("content-disposition", "attachment; filename=" + tmxFileName);
                responseHeaders.add(Constants.CONTENT_TYPE, fileType);
                responseHeaders.add("filename", tmxFileName);

                return new ResponseEntity<byte[]>(out, responseHeaders, HttpStatus.OK);
            }
        }
        else if (info.getExportTmPhase() == ExportTmPhase.FAILED)
        {
            // exportInfoCache.remove(jobId);
        }
        else
        {
            String oldAccessToken = info.getAccessToken();
            if (oldAccessToken != null && StringUtils.isNotEmpty(oldAccessToken))
            {
                boolean tokenExpired = jwtVerifier.isTokenExpired(oldAccessToken);
                if (tokenExpired && StringUtils.isNotEmpty(accessToken) && !StringUtils.equals(Constants.UNDEFINED, accessToken))
                {
                    // update the access token.
                    LOGGER.info("Token expired, updating token to cache");
                    info.setAccessToken(accessToken);
                    exportInfoCache.put(jobId, info);
                }
            }
        }
        exportTmStatus.setStatus(info.getExportTmPhase().toString());
        return ResponseEntity.status(HttpStatus.OK).body(exportTmStatus);
    }

    @ApiOperation(value = "REST API for deleting export TM Info cache from serverr")
    @ApiResponses({
        @ApiResponse(code = 200, message = "TM export info cache is successfully deleted", response = TmExportCacheDeleteResponse.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @DeleteMapping(path = "/api/v2/tms/job/{jobId}")
    public ResponseEntity<TmExportCacheDeleteResponse> deleteExportTmCache(
        @RequestHeader(value = "Authorization") final String authToken,
        @ApiParam(value = "Export TM Job Id", required = true) @PathVariable("jobId") final String jobId,
        @ApiParam(value = "TM Path that import file to", required = true) @RequestParam(value = "tmpath", required = true) String tmPath)
    {
        TmExportCacheDeleteResponse tmExportDelResponse = new TmExportCacheDeleteResponse();
        if (exportInfoCache.has(jobId))
        {
            String message = String.format(Messages.getString("ExportTmController.7"), tmPath);
            tmExportDelResponse.setMessage(message);
            tmExportDelResponse.setJobId(jobId);
            tmExportDelResponse.setTmPath(tmPath);
            LOGGER.info(message);
            exportInfoCache.remove(jobId);
        }
        return ResponseEntity.status(HttpStatus.OK).body(tmExportDelResponse);
    }
}
