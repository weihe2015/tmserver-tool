package org.gs4tr.tmserver.tool.controller.model;

public class DropdownOption
{
    private String label;
    private String value;
    private String shortcode;

    public String getLabel()
    {
        return label;
    }

    public String getValue()
    {
        return value;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getShortcode()
    {
        return shortcode;
    }

    public void setShortcode(String shortcode)
    {
        this.shortcode = shortcode;
    }
}
