package org.gs4tr.tmserver.tool.controller.model;

import java.util.List;

import org.json.JSONObject;

public class TmSearchOption
{
    private String query;
    private String searchLocation;
    private boolean caseSensitive;
    private boolean matchWholeWord;
    private int resultNum;
    private List<DropdownOption> selectedTmList;
    // Available in TMS 5.4:
    private boolean exactMatch;
    private boolean publicTuOnly;
    private boolean includeCDateStart;
    private boolean includeCDateEnd;
    private boolean includeMDateStart;
    private boolean includeMDateEnd;
    private Long creationDateStart;
    private Long creationDateEnd;
    private Long modificationDateStart;
    private Long modificationDateEnd;
    private String accessToken;

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public String getSearchLocation()
    {
        return searchLocation;
    }

    public void setSearchLocation(String searchLocation)
    {
        this.searchLocation = searchLocation;
    }

    public boolean isCaseSensitive()
    {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }

    public boolean isMatchWholeWord()
    {
        return matchWholeWord;
    }

    public void setMatchWholeWord(boolean matchWholeWord)
    {
        this.matchWholeWord = matchWholeWord;
    }

    public int getResultNum()
    {
        return resultNum;
    }

    public void setResultNum(int resultNum)
    {
        this.resultNum = resultNum;
    }

    public List<DropdownOption> getSelectedTmList()
    {
        return selectedTmList;
    }

    public void setSelectedTmList(List<DropdownOption> selectedTmList)
    {
        this.selectedTmList = selectedTmList;
    }

    public boolean isExactMatch()
    {
        return exactMatch;
    }

    public void setExactMatch(boolean exactMatch)
    {
        this.exactMatch = exactMatch;
    }

    public boolean isPublicTuOnly()
    {
        return publicTuOnly;
    }

    public void setPublicTuOnly(boolean publicTuOnly)
    {
        this.publicTuOnly = publicTuOnly;
    }

    public boolean isIncludeCDateStart()
    {
        return includeCDateStart;
    }

    public void setIncludeCDateStart(boolean includeCDateStart)
    {
        this.includeCDateStart = includeCDateStart;
    }

    public boolean isIncludeCDateEnd()
    {
        return includeCDateEnd;
    }

    public void setIncludeCDateEnd(boolean includeCDateEnd)
    {
        this.includeCDateEnd = includeCDateEnd;
    }

    public boolean isIncludeMDateStart()
    {
        return includeMDateStart;
    }

    public void setIncludeMDateStart(boolean includeMDateStart)
    {
        this.includeMDateStart = includeMDateStart;
    }

    public boolean isIncludeMDateEnd()
    {
        return includeMDateEnd;
    }

    public void setIncludeMDateEnd(boolean includeMDateEnd)
    {
        this.includeMDateEnd = includeMDateEnd;
    }

    public Long getCreationDateStart()
    {
        return creationDateStart;
    }

    public void setCreationDateStart(Long creationDateStart)
    {
        this.creationDateStart = creationDateStart;
    }

    public Long getCreationDateEnd()
    {
        return creationDateEnd;
    }

    public void setCreationDateEnd(Long creationDateEnd)
    {
        this.creationDateEnd = creationDateEnd;
    }

    public Long getModificationDateStart()
    {
        return modificationDateStart;
    }

    public void setModificationDateStart(Long modificationDateStart)
    {
        this.modificationDateStart = modificationDateStart;
    }

    public Long getModificationDateEnd()
    {
        return modificationDateEnd;
    }

    public void setModificationDateEnd(Long modificationDateEnd)
    {
        this.modificationDateEnd = modificationDateEnd;
    }

    @Override
    public String toString()
    {
        JSONObject obj = new JSONObject();
        obj.put("exactMatch", exactMatch);
        obj.put("publicTuOnly", publicTuOnly);
        return obj.toString();
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }
}
