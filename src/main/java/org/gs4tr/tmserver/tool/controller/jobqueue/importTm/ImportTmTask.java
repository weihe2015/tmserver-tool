package org.gs4tr.tmserver.tool.controller.jobqueue.importTm;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.ImportTmResponse;
import org.gs4tr.tmserver.tool.api.model.LoginResponse;
import org.gs4tr.tmserver.tool.controller.jobqueue.Task;
import org.gs4tr.tmserver.tool.controller.jobqueue.TaskPriority;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.ImportInfoCache;
import org.gs4tr.tmserver.tool.controller.model.TmImportOption;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.service.jwt.JWTVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImportTmTask extends Task
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ImportTmTask.class);

    private final TmServerInfo tmServerInfo;
    private final TmServerApi tmServerApi;
    private final JWTVerifier jwtVerifier;
    private final ImportInfoCache importInfoCache;

    public ImportTmTask(String jobId, TaskPriority taskPriority, TmServerInfo tmServerInfo, TmServerApi tmServerApi,
        JWTVerifier jwtVerifier, ImportInfoCache importInfoCache)
    {
        super(jobId, taskPriority);
        this.tmServerInfo = tmServerInfo;
        this.tmServerApi = tmServerApi;
        this.jwtVerifier = jwtVerifier;
        this.importInfoCache = importInfoCache;
    }

    @Override
    public void run()
    {
        String jobId = getJobId();
        ImportTmInfos infos = importInfoCache.get(jobId);
        if (infos == null)
        {
            return;
        }
        TmImportOption tmImportOption = infos.getTmImportOption();
        List<File> uploadFiles = infos.getUploadTmxFiles();

        boolean isTPAuth = infos.isTPAuth();
        String securityTicket = null;
        try
        {
            String accessToken = null;
            String tmPath = tmImportOption.getTmPath();
            if (!isTPAuth)
            {
                securityTicket = loginToTmServerWithCredential(tmPath, uploadFiles, infos);
            }
            else
            {
                accessToken = tmImportOption.getAccessToken();
                boolean tokenExpired = jwtVerifier.isTokenExpired(accessToken);
                // If token is expired, we use default power user to access TMs.
                if (tokenExpired)
                {
                    LOGGER.info(Messages.getString("ImportTmController.12"));
                    accessToken = null;
                    securityTicket = loginToTmServerWithCredential(tmPath);
                    isTPAuth = false;
                }
            }

            if (StringUtils.isNotEmpty(securityTicket) || isTPAuth)
            {
                for (File uploadFile : uploadFiles)
                {
                    // check if token expired:
                    boolean tokenExpired = false;
                    if (isTPAuth)
                    {
                        tokenExpired = jwtVerifier.isTokenExpired(accessToken);
                    }
                    // If token is expired, we use default power user to access TMs.
                    if (tokenExpired && isTPAuth)
                    {
                        LOGGER.info(Messages.getString("ImportTmController.12"));
                        accessToken = null;
                        securityTicket = loginToTmServerWithCredential(tmPath);
                        isTPAuth = false;
                    }
                    ImportTmResponse importTmResponse = tmServerApi.importTm(tmServerInfo, tmImportOption, uploadFile, securityTicket,
                        accessToken, isTPAuth);

                    updateInfoMapWithImportTmResponse(uploadFile, importTmResponse, infos);
                    cleanupFile(uploadFile);
                }
            }
        }
        catch (IOException ex)
        {
            LOGGER.error(String.format(Messages.getString("ImportTmController.6"), ex.getMessage()));
        }
        finally
        {
            importInfoCache.put(jobId, infos);
        }
    }

    private String loginToTmServerWithCredential(String tmPath) throws IOException
    {
        String securityTicket = null;
        LoginResponse response = tmServerApi.login(tmServerInfo, tmPath);
        if (response != null)
        {
            securityTicket = response.getSecurityTicket();
        }
        return securityTicket;
    }

    private String loginToTmServerWithCredential(String tmPath, List<File> uploadFiles, ImportTmInfos infos)
        throws IOException
    {
        String securityTicket = null;
        LoginResponse response = tmServerApi.login(tmServerInfo, tmPath);
        if (response != null)
        {
            securityTicket = response.getSecurityTicket();
        }
        // login failed:
        else
        {
            for (File uploadFile : uploadFiles)
            {
                String uploadFileName = uploadFile.getName();
                ImportTmInfo info = new ImportTmInfo();
                String message = String.format(Messages.getString("ImportTmController.16"), tmPath,
                    tmServerInfo.getTmUserName());
                if (response != null)
                {
                    setFailedImportTmInfo(info, response.getErrorMessage(), message, response);
                }
                infos.addInfoToMap(uploadFileName, info);
            }
        }
        return securityTicket;
    }

    private void setFailedImportTmInfo(ImportTmInfo info, String errorMessage, String fallbackMessage, Object response)
    {
        info.setImportTmPhase(ImportTmPhase.FAILED);
        info.setSuccess(false);
        if (response != null)
        {
            info.setErrorMessage(errorMessage);
        }
        else
        {
            info.setErrorMessage(fallbackMessage);
        }
    }

    private void updateInfoMapWithImportTmResponse(File uploadFile, ImportTmResponse importTmResponse,
        ImportTmInfos infos)
    {
        String uploadFileName = uploadFile.getName();
        ImportTmInfo info = new ImportTmInfo();

        info.setUploadFileName(uploadFileName);
        if (!importTmResponse.isSuccess())
        {
            setFailedImportTmInfo(info, importTmResponse.getErrorMessage(), null, importTmResponse);
        }
        else
        {
            info.setImportTmPhase(ImportTmPhase.START);
            info.setImportJobId(importTmResponse.getId());
        }
        infos.addInfoToMap(uploadFileName, info);
    }

    private void cleanupFile(File uploadFile)
    {
        // Moved files to processed folder
        // delete the upload file instead of moving it to processedDir folder.
        String uploadFilePath = uploadFile.getAbsolutePath();
        FileUtils.deleteQuietly(uploadFile);
        String message = String.format(Messages.getString("ImportTmController.10"), uploadFilePath);
        LOGGER.info(message);
    }
}
