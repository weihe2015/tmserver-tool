package org.gs4tr.tmserver.tool.controller.jobqueue.importTm;

import java.io.Serializable;

public class ImportTmInfo implements Serializable
{
    private static final long serialVersionUID = 1106878086026035407L;
    private ImportTmPhase importTmPhase;
    private String uploadFileName;
    private String importJobId;
    private String errorMessage;
    private boolean success;

    public ImportTmPhase getImportTmPhase()
    {
        return importTmPhase;
    }

    public void setImportTmPhase(ImportTmPhase importTmPhase)
    {
        this.importTmPhase = importTmPhase;
    }

    public String getUploadFileName()
    {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName)
    {
        this.uploadFileName = uploadFileName;
    }

    public String getImportJobId()
    {
        return importJobId;
    }

    public void setImportJobId(String importJobId)
    {
        this.importJobId = importJobId;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }
}
