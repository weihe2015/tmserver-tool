package org.gs4tr.tmserver.tool.controller.jobqueue;

public abstract class Task implements Runnable
{
    protected String jobId;
    private TaskPriority jobPriority;

    public Task(String jobId, TaskPriority jobPriority)
    {
        this.jobId = jobId;
        this.jobPriority = jobPriority;
    }

    public TaskPriority getJobPriority()
    {
        return jobPriority;
    }

    public void setJobPriority(TaskPriority jobPriority)
    {
        this.jobPriority = jobPriority;
    }

    public String getJobId()
    {
        return jobId;
    }

    public void setJobId(String jobId)
    {
        this.jobId = jobId;
    }

}
