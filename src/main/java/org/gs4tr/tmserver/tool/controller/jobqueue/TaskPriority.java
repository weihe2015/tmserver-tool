package org.gs4tr.tmserver.tool.controller.jobqueue;

public enum TaskPriority
{
    HIGH, MEDIUM, LOW
}
