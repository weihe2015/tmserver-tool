package org.gs4tr.tmserver.tool.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class ValidateTmShortCodeRequest
{
    private String tmShortCode;
    private String accessToken;

    public String getTmShortCode()
    {
        return tmShortCode;
    }

    public void setTmShortCode(String tmShortCode)
    {
        this.tmShortCode = tmShortCode;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }
}
