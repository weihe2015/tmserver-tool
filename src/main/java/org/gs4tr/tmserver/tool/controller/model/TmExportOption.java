package org.gs4tr.tmserver.tool.controller.model;

import org.gs4tr.tmserver.tool.api.model.TmExportFilter;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class TmExportOption
{
    private String tmName;
    private String tmPath;
    private String tmGroupName;
    private String sourceLocaleCode;
    private String targetLocaleCode;
    private String tmExportType;
    private boolean exportedToFtp;
    private boolean includeAttributes;
    private boolean includePrivateTus;
    private TmExportFilter exportFilter;
    private String accessToken;

    public String getTmName()
    {
        return tmName;
    }
    public String getTmPath()
    {
        return tmPath;
    }

    public String getTmGroupName()
    {
        return tmGroupName;
    }
    public String getSourceLocaleCode()
    {
        return sourceLocaleCode;
    }
    public String getTargetLocaleCode()
    {
        return targetLocaleCode;
    }

    public String getTmExportType()
    {
        return tmExportType;
    }

    public boolean isExportedToFtp()
    {
        return exportedToFtp;
    }

    public boolean isIncludeAttributes()
    {
        return includeAttributes;
    }

    public boolean isIncludePrivateTus()
    {
        return includePrivateTus;
    }

    public TmExportFilter getExportFilter()
    {
        return exportFilter;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }
}
