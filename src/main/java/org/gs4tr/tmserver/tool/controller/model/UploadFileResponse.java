package org.gs4tr.tmserver.tool.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class UploadFileResponse implements Cloneable
{
    private boolean status;
    private String filename;
    private String originalZipFileName;
    private String errorMessage;
    private String global;

    public boolean isStatus()
    {
        return status;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getGlobal()
    {
        return global;
    }

    public void setGlobal(String global)
    {
        this.global = global;
    }

    public String getOriginalZipFileName()
    {
        return originalZipFileName;
    }

    public void setOriginalZipFileName(String originalZipFileName)
    {
        this.originalZipFileName = originalZipFileName;
    }

    @Override
    public UploadFileResponse clone()
    {
        UploadFileResponse obj = null;
        try
        {
            obj = (UploadFileResponse) super.clone();
        }
        catch (CloneNotSupportedException ex)
        {

        }
        return obj;
    }
}
