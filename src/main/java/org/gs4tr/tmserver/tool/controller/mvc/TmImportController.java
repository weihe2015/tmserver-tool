package org.gs4tr.tmserver.tool.controller.mvc;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.ImportProcessResponse;
import org.gs4tr.tmserver.tool.api.model.LoginResponse;
import org.gs4tr.tmserver.tool.controller.TmServerToolController;
import org.gs4tr.tmserver.tool.controller.jobqueue.Task;
import org.gs4tr.tmserver.tool.controller.jobqueue.TaskPriority;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.ImportInfoCache;
import org.gs4tr.tmserver.tool.controller.jobqueue.importTm.ImportTmInfo;
import org.gs4tr.tmserver.tool.controller.jobqueue.importTm.ImportTmInfos;
import org.gs4tr.tmserver.tool.controller.jobqueue.importTm.ImportTmPhase;
import org.gs4tr.tmserver.tool.controller.jobqueue.importTm.ImportTmTask;
import org.gs4tr.tmserver.tool.controller.jobqueue.importTm.ImportTmTaskScheduler;
import org.gs4tr.tmserver.tool.controller.model.ErrorResponse;
import org.gs4tr.tmserver.tool.controller.model.TmImportErrorResponse;
import org.gs4tr.tmserver.tool.controller.model.TmImportOption;
import org.gs4tr.tmserver.tool.controller.model.ValidateRandomTokenResponse;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.model.importTm.TmImportStatus;
import org.gs4tr.tmserver.tool.model.swagger.ImportTMProcessResponse;
import org.gs4tr.tmserver.tool.model.swagger.JsonErrorMessage;
import org.gs4tr.tmserver.tool.model.swagger.TokenResponse;
import org.gs4tr.tmserver.tool.service.file.Constants;
import org.gs4tr.tmserver.tool.service.file.FolderUtils;
import org.gs4tr.tmserver.tool.service.jwt.JWTVerifier;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "REST Endpoint of import TM")
public class TmImportController extends TmServerToolController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TmImportController.class);
    private static final String SYSTEM_DIR = System.getProperty("user.dir");

    @Autowired
    private FolderUtils folderUtils;

    @Autowired
    private TmServerApi tmServerApi;

    @Autowired
    private ImportTmTaskScheduler importTmTaskScheduler;

    @Autowired
    private ImportInfoCache importInfoCache;

    @Autowired
    private JWTVerifier jwtVerifier;

    @ApiOperation(value = "REST API for getting import job status from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "TM import job status is successfully retrieved", response = ImportTMProcessResponse.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v2/tm/job/{jobId}")
    public ResponseEntity<?> getImportProcess(@RequestHeader(value = "Authorization") final String authToken,
        @ApiParam(value = "import TM Job Id", required = true) @PathVariable("jobId") final String jobId,
        @ApiParam(value = "TM Path that import file to", required = true) @RequestParam(value = "tmpath", required = true) String tmPath,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken,
        @ApiParam(value = "upload File Name", required = true) @RequestParam(value = "filename", required = true) String uploadFileName
        )
    {
        // Replace dot separator to slash.
        tmPath = tmPath.replace(Constants.DOLLAR, Constants.SLASH);
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);

        ImportTmInfos importTmInfos = importInfoCache.get(jobId);

        if (!importTmInfos.importTmInfoAvailableByUploadFileName(uploadFileName))
        {
            ImportProcessResponse response = new ImportProcessResponse();
            response.setPhase(ImportTmPhase.WAITING.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }

        ImportTmInfo importTmInfo = importTmInfos.getImportTmInfo(uploadFileName);
        String importJobId = importTmInfo.getImportJobId();

        if (importTmInfo.getImportTmPhase() == ImportTmPhase.FAILED)
        {
            ImportProcessResponse response = new ImportProcessResponse();
            response.setPhase(ImportTmPhase.FAILED.toString());
            response.setErrorMessage(importTmInfo.getErrorMessage());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
        else if (StringUtils.isEmpty(importJobId))
        {
            ImportProcessResponse response = new ImportProcessResponse();
            response.setPhase(ImportTmPhase.FAILED.toString());
            response.setErrorMessage(Messages.getString("ImportTmController.14"));
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }

        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;
        try
        {
            if (!isTPAuth)
            {
                LoginResponse response = tmServerApi.login(tmServerInfo, tmPath);
                if (response != null)
                {
                    securityTicket = response.getSecurityTicket();
                }
            }
            else
            {
                boolean tokenExpired = jwtVerifier.isTokenExpired(accessToken);
                // If token is expired, we use default power user to access TMs.
                if (tokenExpired)
                {
                    accessToken = null;
                    LoginResponse response = tmServerApi.login(tmServerInfo, tmPath);
                    if (response != null)
                    {
                        securityTicket = response.getSecurityTicket();
                    }
                    isTPAuth = false;
                }
                else
                {
                    importTmInfos.setAccessToken(accessToken);
                }
            }

            if (StringUtils.isNotEmpty(securityTicket) || isTPAuth)
            {
                ImportProcessResponse response = tmServerApi.getImportProgress(tmServerInfo, importJobId, securityTicket, accessToken);
                if (response.getPhase() == null)
                {
                    importTmInfo.setImportTmPhase(ImportTmPhase.FAILED);
                }
                else
                {
                    importTmInfo.setImportTmPhase(ImportTmPhase.valueOf(response.getPhase()));
                }
                importTmInfos.addInfoToMap(uploadFileName, importTmInfo);
                return ResponseEntity.status(HttpStatus.OK).body(response);
            }
            else
            {
                String errorMessage = String.format(Messages.getString("ImportTmController.0"), tmPath);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("ImportTmController.1"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            importInfoCache.put(jobId, importTmInfos);
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @ApiOperation(value = "REST API for import TM to TM Server")
    @ApiResponses({
        @ApiResponse(code = 201, message = "TMs are imported to TM Server successfully", response = JSONArray.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PutMapping("/api/v1/tm/tms")
    public ResponseEntity<?> importTm(@RequestHeader(value = "Authorization") final String authToken,
                                      @RequestBody final TmImportOption tmImportOption)
    {
        StringBuffer sb = new StringBuffer();
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);

        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;
        try
        {
            String tmPath = tmImportOption.getTmPath();
            String date = new SimpleDateFormat(Constants.DATEFORMATE).format(new Date());
            if (!isTPAuth)
            {
                LoginResponse response = tmServerApi.login(tmServerInfo, tmPath);
                if (response != null)
                {
                    securityTicket = response.getSecurityTicket();
                }
            }

            if (StringUtils.isNotEmpty(securityTicket) || isTPAuth)
            {
                String randomToken = tmImportOption.getRandomToken();
                // en-US_de-DE => en-us_de-de because it is what the upload folder has:
                String folderLocales = tmImportOption.getFolderLocales().toLowerCase();
                // Find imported tmx file by tm Path:
                String folderName = folderUtils.getUploadFolderPathWithLocale(SYSTEM_DIR, date, randomToken, folderLocales);
                LOGGER.info("Scanning for imported Tmx files in folder: " + folderName);
                List<File> uploadTmxFileList = new ArrayList<>();
                folderUtils.getAllTmFiles(folderName, tmImportOption, uploadTmxFileList);

                if (uploadTmxFileList.isEmpty())
                {
                    String errorMessage = Messages.getString("ImportTmController.4");
                    LOGGER.warn(errorMessage);
                    TmImportErrorResponse tmImportErrorResponse = new TmImportErrorResponse();
                    tmImportErrorResponse.setErrorMessage(errorMessage);
                    tmImportErrorResponse.setTmName(tmImportOption.getTmName());
                    return ResponseEntity.status(HttpStatus.OK).body(tmImportErrorResponse);
                }
                else
                {
                    // List<TmImportStatus> tmImportResponse = tmServerApi.importTm(tmServerInfo, tmImportOption,
                    // uploadTmxFileList, securityTicket, accessToken);
                    // Moved files to processed folder
                    for (File uploadFile : uploadTmxFileList)
                    {
                        // delete the upload file instead of moving it to processedDir folder.
                        String uploadFilePath = uploadFile.getAbsolutePath();
                        FileUtils.deleteQuietly(uploadFile);
                        String message = String.format(Messages.getString("ImportTmController.10"), uploadFilePath);

                        LOGGER.info(message);
                    }
                    //sb.append(objectMapper.writeValueAsString(tmImportResponse));
                }
            }
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("ImportTmController.6"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }

        return ResponseEntity.ok(sb.toString());
    }

    @ApiOperation(value = "REST API for import TM to TM Server")
    @ApiResponses(
    {
        @ApiResponse(code = 201, message = "TMs are imported to TM Server successfully", response = JSONArray.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PutMapping("/api/v2/tm/tms")
    public ResponseEntity<?> scheduleImportTmTask(@RequestHeader(value = "Authorization") final String authToken,
                                                  @RequestBody final TmImportOption tmImportOption)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);

        String accessToken = null;
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        if (isTPAuth)
        {
            accessToken = tmImportOption.getAccessToken();
        }

        String tmPath = tmImportOption.getTmPath();
        String date = new SimpleDateFormat(Constants.DATEFORMATE).format(new Date());
        String randomToken = tmImportOption.getRandomToken();
        // en-US_de-DE => en-us_de-de because it is what the upload folder has:
        String folderLocales = tmImportOption.getFolderLocales().toLowerCase();
        // Find imported tmx file by tm Path:
        String folderName = folderUtils.getUploadFolderPathWithLocale(SYSTEM_DIR, date, randomToken, folderLocales);

        LOGGER.info(String.format(Messages.getString("ImportTmController.13"), folderName));
        List<File> uploadTmxFiles = new ArrayList<>();
        folderUtils.getAllTmFiles(folderName, tmImportOption, uploadTmxFiles);

        if (uploadTmxFiles.isEmpty())
        {
            String errorMessage = Messages.getString("ImportTmController.4");
            LOGGER.warn(errorMessage);
            TmImportErrorResponse tmImportErrorResponse = new TmImportErrorResponse();
            tmImportErrorResponse.setErrorMessage(errorMessage);
            tmImportErrorResponse.setTmName(tmImportOption.getTmName());
            return ResponseEntity.status(HttpStatus.OK).body(tmImportErrorResponse);
        }
        else
        {
            List<TmImportStatus> responseList = new ArrayList<>();
            String jobId = UUID.randomUUID().toString();
            ImportTmInfos info = new ImportTmInfos(jobId, tmImportOption, uploadTmxFiles, isTPAuth, accessToken);
            importInfoCache.put(jobId, info);

            Task task = new ImportTmTask(jobId, TaskPriority.HIGH, tmServerInfo, tmServerApi, jwtVerifier, importInfoCache);
            importTmTaskScheduler.scheduleTask(task);

            for (File tmxFile : uploadTmxFiles)
            {
                String message = String.format(Messages.getString("ImportTmController.11"), tmPath);
                LOGGER.info(message);

                TmImportStatus status = new TmImportStatus(jobId, tmxFile.getName());
                responseList.add(status);
            }
            return ResponseEntity.status(HttpStatus.OK).body(responseList);
        }
    }

    @ApiOperation(value = "REST API of validating folder token on server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Folder token is valid", response = TokenResponse.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v1/tm/token")
    public ResponseEntity<ValidateRandomTokenResponse> validRandomToken(@ApiParam(value = "folder token", required = true)
                                                                        @RequestParam(value = "t", required = true) String randomToken)
    {
        ValidateRandomTokenResponse validateRandomTokenResponse = new ValidateRandomTokenResponse();
        String date = new SimpleDateFormat(Constants.DATEFORMATE).format(new Date());

        if (randomToken == null || StringUtils.isBlank(randomToken))
        {
            validateRandomTokenResponse.setStatus(false);
            validateRandomTokenResponse.setRandomToken(randomToken);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validateRandomTokenResponse);
        }

        String uploadFolder = folderUtils.getUploadFolderPath(SYSTEM_DIR, date, randomToken);
        File uploadDir = new File(uploadFolder);
        if (uploadDir.exists())
        {
            validateRandomTokenResponse.setStatus(false);
            validateRandomTokenResponse.setRandomToken(randomToken);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(validateRandomTokenResponse);
        }
        else
        {
            validateRandomTokenResponse.setStatus(true);
            validateRandomTokenResponse.setRandomToken(randomToken);
            return ResponseEntity.ok(validateRandomTokenResponse);
        }
    }
}
