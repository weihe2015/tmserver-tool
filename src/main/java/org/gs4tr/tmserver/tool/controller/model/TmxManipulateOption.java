package org.gs4tr.tmserver.tool.controller.model;

import java.util.List;

import org.gs4tr.tmserver.tool.model.tmx.NewAttribute;

public class TmxManipulateOption
{
    private boolean removePreviousAttribute;
    private boolean addFilenameAsAttribute;
    private boolean removeTrailingSpaces;
    private boolean insertChecksum;
    private boolean insertChecksumInAllTus;
    // Flag to indicate moving props from Tuv to Tu
    private boolean moveAttributesToTu;
    private String randomToken;
    private List<NewAttribute> newAttributes;

    public boolean isRemovePreviousAttribute()
    {
        return removePreviousAttribute;
    }

    public void setRemovePreviousAttribute(boolean removePreviousAttribute)
    {
        this.removePreviousAttribute = removePreviousAttribute;
    }

    public boolean isAddFilenameAsAttribute()
    {
        return addFilenameAsAttribute;
    }

    public void setAddFilenameAsAttribute(boolean addFilenameAsAttribute)
    {
        this.addFilenameAsAttribute = addFilenameAsAttribute;
    }

    public boolean isRemoveTrailingSpaces()
    {
        return removeTrailingSpaces;
    }

    public void setRemoveTrailingSpaces(boolean removeTrailingSpaces)
    {
        this.removeTrailingSpaces = removeTrailingSpaces;
    }

    public String getRandomToken()
    {
        return randomToken;
    }

    public void setRandomToken(String randomToken)
    {
        this.randomToken = randomToken;
    }

    public List<NewAttribute> getNewAttributes()
    {
        return newAttributes;
    }

    public void setNewAttributes(List<NewAttribute> newAttributes)
    {
        this.newAttributes = newAttributes;
    }

    public boolean isInsertChecksum()
    {
        return insertChecksum;
    }

    public void setInsertChecksum(boolean insertChecksum)
    {
        this.insertChecksum = insertChecksum;
    }

    public boolean isOutputFileToFTP()
    {
        return false;
    }

    public boolean isMoveAttributesToTu()
    {
        return moveAttributesToTu;
    }

    public void setMoveAttributesToTu(boolean moveAttributesToTu)
    {
        this.moveAttributesToTu = moveAttributesToTu;
    }

    // Insert checksum in all TUs in TMX, no matter it contains context_prev or context_post or not
    public boolean isInsertChecksumInAllTus()
    {
        return insertChecksumInAllTus;
    }

    public void setInsertChecksumInAllTus(boolean insertChecksumInAllTus)
    {
        this.insertChecksumInAllTus = insertChecksumInAllTus;
    }
}
