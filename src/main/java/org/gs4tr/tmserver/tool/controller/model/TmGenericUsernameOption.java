package org.gs4tr.tmserver.tool.controller.model;

import org.springframework.util.StringUtils;

public class TmGenericUsernameOption
{
    private String tmName;
    private String sourceLanguageCode;
    private String targetLanguageCode;
    private String accessToken;

    public String getSourceLanguageCode()
    {
        return this.sourceLanguageCode;
    }

    public String getTargetLanguageCode()
    {
        return this.targetLanguageCode;
    }

    public String getTmName()
    {
        return this.tmName;
    }

    public void setTmName(String tmName)
    {
        this.tmName = tmName;
    }

    public void setSourceLanguageCode(String sourceLanguageCode)
    {
        this.sourceLanguageCode = sourceLanguageCode;
    }

    public void setTargetLanguageCode(String targetLanguageCode)
    {
        this.targetLanguageCode = targetLanguageCode;
    }

    public boolean isLanguageEmpty()
    {
        return StringUtils.isEmpty(this.sourceLanguageCode) || StringUtils.isEmpty(this.targetLanguageCode);
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }
}
