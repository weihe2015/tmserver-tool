package org.gs4tr.tmserver.tool.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class ValidateTmCleanupCodeRequest
{
    private String cleanupCode;
    private String accessToken;

    public String getCleanupCode()
    {
        return cleanupCode;
    }

    public void setCleanupCode(String cleanupCode)
    {
        this.cleanupCode = cleanupCode;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }
}
