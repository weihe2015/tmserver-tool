package org.gs4tr.tmserver.tool.controller.jobqueue.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

@Component
public class UserDetailsCache
{
    static public final String CACHE_ID = "UserDetailsCache";
    static public final String CACHE_MANAGER_ID = "serviceCacheManager";
    static public final String CACHE_POOL_NAME = "UserDetailsCachePool";

    private final CacheManager _cacheManager;

    @Autowired
    public UserDetailsCache(@Qualifier(CACHE_MANAGER_ID) CacheManager cacheManager)
    {
        _cacheManager = cacheManager;
    }

    public UserDetails get(String key)
    {
        Element element = getCache().get(key);
        if (element == null || element.isExpired())
        {
            return null;
        }
        return (UserDetails) element.getObjectValue();
    }

    public void put(String key, UserDetails info)
    {
        getCache().put(new Element(key, info));
    }

    public void remove(String key)
    {
        getCache().remove(key);
    }

    private Cache getCache()
    {
        return getCacheManager().getCache(CACHE_ID);
    }

    private CacheManager getCacheManager()
    {
        return _cacheManager;
    }
}
