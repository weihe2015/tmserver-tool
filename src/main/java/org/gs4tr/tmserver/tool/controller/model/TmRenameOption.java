package org.gs4tr.tmserver.tool.controller.model;

import java.util.List;

public class TmRenameOption
{
    private String newTmName;
    private List<MixedTmInfo> selectedTmList;

    public String getNewTmName()
    {
        return newTmName;
    }

    public void setNewTmName(String newTmName)
    {
        this.newTmName = newTmName;
    }

    public List<MixedTmInfo> getSelectedTmList()
    {
        return selectedTmList;
    }

    public void setSelectedTmList(List<MixedTmInfo> selectedTmList)
    {
        this.selectedTmList = selectedTmList;
    }
}
