package org.gs4tr.tmserver.tool.controller.mvc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.CreateShortcodeResponse;
import org.gs4tr.tmserver.tool.api.model.CreateTmResponse;
import org.gs4tr.tmserver.tool.api.model.FetchOrganizationsResponse;
import org.gs4tr.tmserver.tool.api.model.FetchTmGroupsResponse;
import org.gs4tr.tmserver.tool.api.model.FetchTmLanguageResponse;
import org.gs4tr.tmserver.tool.api.model.GenerateUserPasswordResponse;
import org.gs4tr.tmserver.tool.api.model.GenerateUsernameResponse;
import org.gs4tr.tmserver.tool.api.model.GenericRolesResponse;
import org.gs4tr.tmserver.tool.api.model.TmCleanupPasscodeResponse;
import org.gs4tr.tmserver.tool.api.model.TmGroupCreateResponse;
import org.gs4tr.tmserver.tool.api.model.TmInfo;
import org.gs4tr.tmserver.tool.api.model.TmListResponse;
import org.gs4tr.tmserver.tool.api.model.ValidateCodeResponse;
import org.gs4tr.tmserver.tool.controller.TmServerToolController;
import org.gs4tr.tmserver.tool.controller.model.CreatePdCsvOption;
import org.gs4tr.tmserver.tool.controller.model.CreateTmCsvOption;
import org.gs4tr.tmserver.tool.controller.model.DropdownOption;
import org.gs4tr.tmserver.tool.controller.model.DuplicateTmCheckResponse;
import org.gs4tr.tmserver.tool.controller.model.ErrorResponse;
import org.gs4tr.tmserver.tool.controller.model.TmCreateOption;
import org.gs4tr.tmserver.tool.controller.model.TmCreateResponse;
import org.gs4tr.tmserver.tool.controller.model.TmCreateWithCsvRequest;
import org.gs4tr.tmserver.tool.controller.model.TmGenericUsernameOption;
import org.gs4tr.tmserver.tool.controller.model.TmGroupOption;
import org.gs4tr.tmserver.tool.controller.model.TmLanguagePair;
import org.gs4tr.tmserver.tool.controller.model.ValidateTmCleanupCodeRequest;
import org.gs4tr.tmserver.tool.controller.model.ValidateTmGroupCodeRequest;
import org.gs4tr.tmserver.tool.controller.model.ValidateTmShortCodeRequest;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.model.swagger.JsonErrorMessage;
import org.gs4tr.tmserver.tool.model.swagger.PdCsvInfo;
import org.gs4tr.tmserver.tool.service.file.Constants;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "REST API endpoint of Create TM and Create CSV files for TM Profiles and TM Strings")
public class TmCreationController extends TmServerToolController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TmCreationController.class);

    @Autowired
    private TmServerApi tmServerApi;

    private void constructCsvHeader(List<List<Object>> result, String[] headers)
    {
        List<Object> subArray = new ArrayList<>();
        for (String header : headers)
        {
            subArray.add(header);
        }
        result.add(subArray);
    }

    private List<List<Object>> constructPDCsv(TmServerInfo tmServerInfo, List<TmInfo> tmInfos, CreatePdCsvOption createPdCsvOption)
    {
        List<List<Object>> result = new ArrayList<>();
        boolean emptyCleanupUserInfo = createPdCsvOption.isEmptyCleanupUserInfo();
        if (!emptyCleanupUserInfo)
        {
            constructCsvHeader(result, Constants.PD_CSV_HEADERS);
        }
        else
        {
            constructCsvHeader(result, Constants.PD_CSV_NEW_HEADERS);
        }

        for (TmInfo tmInfo : tmInfos)
        {
            List<Object> subArray = new ArrayList<>();
            // 7-19-18, Use the correct URL if user remote login to other Tm Server
            String url = tmServerInfo.getURL();
            // To avoid empty connection strings in the TM.
            if (tmInfo.getConnectionStrings().isEmpty())
            {
                continue;
            }
            String connectionString = tmInfo.getConnectionStrings().get(0);
            Map<String, String> map = getQueryMap(connectionString);
            String tmName = tmInfo.getTmName();
            String genericUsername = map.get("usr");
            String genericPassword = map.get("pwd");
            String tmPath = tmInfo.getTmPath();

            final int updateTm = 1;
            final int addFileName = createPdCsvOption.isAddFileName() ? 1 : 0;

            // 11-7-17 use genericUsername as Tm Profile name.
            if (createPdCsvOption.isUseGenericUsernameAsProfileName())
            {
                subArray.add(genericUsername);
            }
            // 7-17-18 Not all users want generic Username as Tm Profilename.
            // So set Tm Name as Tm profile name.
            else
            {
                subArray.add(tmName);
            }
            // In PD 5.8 and TM Server 5.2, they remove Cleanup user info
            if (!emptyCleanupUserInfo)
            {
                subArray.add(url);
                subArray.add(createPdCsvOption.getCleanupUsername());
                subArray.add(createPdCsvOption.getCleanupPassword());
            }
            else
            {
                subArray.add(StringUtils.EMPTY);
                subArray.add(StringUtils.EMPTY);
                subArray.add(StringUtils.EMPTY);
            }
            subArray.add(url);
            subArray.add(genericUsername);
            subArray.add(genericPassword);

            subArray.add(tmPath);
            subArray.add(tmInfo.getSourceLanguage());
            subArray.add(tmInfo.getTargetLanguage());

            subArray.add(updateTm);
            // In PD 5.8 and TM Server 5.2, they add a new field called TM Update Passcode
            if (emptyCleanupUserInfo)
            {
                subArray.add(tmInfo.getUpdateCode());
            }
            subArray.add(addFileName);
            subArray.add(createPdCsvOption.getPaClientName());
            subArray.add(tmInfo.getOrganization());

            result.add(subArray);
        }
        return result;
    }

    private List<List<Object>> constructTmStringCsv(List<TmInfo> tmInfos, DropdownOption selectedOrganization)
    {
        List<List<Object>> jsonArray = new ArrayList<>();
        constructCsvHeader(jsonArray, Constants.TM_SERVER_CSV_HEADER);

        for (TmInfo tmInfo : tmInfos)
        {
            List<Object> subArray = new ArrayList<>();
            if (selectedOrganization != null)
            {
                subArray.add(selectedOrganization.getLabel());
            }
            else
            {
                subArray.add(tmInfo.getOrganization());
            }
            subArray.add(tmInfo.getGroupName());
            subArray.add(tmInfo.getConnectionStrings().get(0));
            subArray.add(tmInfo.getSourceLanguage());
            subArray.add(tmInfo.getTargetLanguage());

            jsonArray.add(subArray);
        }
        return jsonArray;
    }

    @ApiOperation(value = "REST API for generating generic username from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Generic username is succesfully retrieved", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/genericUsername")
    public ResponseEntity<?> createGenericUsername(@RequestHeader(value = "Authorization") final String authToken,
                                                   @RequestBody final TmGenericUsernameOption tmGenericUsernameOption)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);

        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;
        try
        {

            String accessToken = null;
            if (isTPAuth)
            {
                accessToken = tmGenericUsernameOption.getAccessToken();
            }
            else
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            GenerateUsernameResponse generateUsernameResponse = tmServerApi.generateGenericUsername(tmServerInfo,
                tmGenericUsernameOption, securityTicket, accessToken);
            if (generateUsernameResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.0"),
                    Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!generateUsernameResponse.isSuccess())
            {
                String errorMessage = generateUsernameResponse.getErrorMessage();
                errorMessage = String.format(Messages.getString("CreateTmController.0"), errorMessage);
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            return ResponseEntity.ok(generateUsernameResponse.getUsername());
        }
        catch (IOException ex)
        {
            // may be the case that no Tm is available:
            String errorMessage = String.format(Messages.getString("CreateTmController.0"), ex.getMessage());
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @ApiOperation(value = "REST API for generating generic password from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Generic Userpassword is succesfully retrieved", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v1/tm/genericPassword")
    public ResponseEntity<?> createGenericUserPassword(@RequestHeader(value = "Authorization") String authToken,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);

        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;
        try
        {
            if (!isTPAuth)
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            GenerateUserPasswordResponse generateUserPasswordResponse = tmServerApi.generateGenericUserpassword(tmServerInfo, securityTicket, accessToken);
            if (generateUserPasswordResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.1"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!generateUserPasswordResponse.isSuccess())
            {
                String errorMessage = generateUserPasswordResponse.getErrorMessage();
                errorMessage = String.format(Messages.getString("CreateTmController.1"), errorMessage);
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            return ResponseEntity.ok(generateUserPasswordResponse);
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.1"), ex.getMessage());
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @ApiOperation(value = "REST API for creating CSV files which imports into PD to create TM profiles")
    @ApiResponses({
        @ApiResponse(code = 201, message = "CSV of TM profiles in PD has created successfully", response = PdCsvInfo.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping(value = "/api/v1/tm/csv/pd", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createPdCsv(@RequestHeader(value = "Authorization") final String authToken,
        @RequestBody final CreatePdCsvOption createPdCsvOption)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        try
        {
            TmListResponse tmListResponse = getTmList(tmsCredential, tmServerInfo);
            List<TmInfo> tmInfos = new ArrayList<>();
            if (tmListResponse == null || tmListResponse.getTmList().isEmpty())
            {
                String errorMessage = Messages.getString("CreateTmController.2");
                LOGGER.warn(errorMessage);

                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            Set<String> selectedTmGroupSet = getSelectedTmGroupSet(createPdCsvOption.getSelectedTmGroup());
            for (TmInfo tmInfo : tmListResponse.getTmList())
            {
                if (selectedTmGroupSet.contains(tmInfo.getGroupShortCode()))
                {
                    tmInfos.add(tmInfo);
                }
            }
            List<List<Object>> result = constructPDCsv(tmServerInfo, tmInfos, createPdCsvOption);
            return ResponseEntity.status(HttpStatus.CREATED).body(result);
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.3"), ex.getMessage());
            LOGGER.error(errorMessage);

            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }

    @ApiOperation(value = "REST API for creating TMs in TM Server")
    @ApiResponses({
        @ApiResponse(code = 201, message = "TMs are created successfully", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/tms")
    public ResponseEntity<?> createTm(@RequestHeader(value = "Authorization") final String authToken,
                                      @RequestBody final TmCreateOption tmCreateOption)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH) &&
            StringUtils.isNotEmpty(tmCreateOption.getAccessToken());
        String securityTicket = null;
        String accessToken = null;

        try
        {
            List<List<Object>> pdCsvContent = new ArrayList<>();


            if (isTPAuth)
            {
                accessToken = tmCreateOption.getAccessToken();
            }
            else
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            if (tmCreateOption.isPdCsvCreationEnabled())
            {
                if (!tmCreateOption.isEmptyCleanupUserInfo())
                {
                    constructCsvHeader(pdCsvContent, Constants.PD_CSV_HEADERS);
                }
                else
                {
                    constructCsvHeader(pdCsvContent, Constants.PD_CSV_NEW_HEADERS);
                }
            }
            TmCreateResponse tmCreateResponse = new TmCreateResponse();
            String sourceLanguage = tmCreateOption.getSourceLanguage();
            for (String targetLanguage : tmCreateOption.getTargetLanguages())
            {
                CreateShortcodeResponse createShortCodeResponse = tmServerApi.createTmShortCode(tmServerInfo, tmCreateOption.getTmName(), securityTicket, accessToken);
                String tmShortCode = createShortCodeResponse.getShortCode();

                tmCreateOption.setTmShortCode(tmShortCode);
                if (tmCreateOption.isInvertedLanguage())
                {
                    tmCreateOption.setTargetLanguage(sourceLanguage);
                    tmCreateOption.setSourceLanguage(targetLanguage);
                }
                else
                {
                    tmCreateOption.setSourceLanguage(sourceLanguage);
                    tmCreateOption.setTargetLanguage(targetLanguage);
                }
                // Generic username will be TmName_en-US_de-DE
                tmCreateOption.setGenericUsernameWithLocales();
                CreateTmResponse createTmResponse = tmServerApi.createTm(tmServerInfo, tmCreateOption, securityTicket, accessToken);

                if (!createTmResponse.isSuccess())
                {
                    // sometimes response may contain message field instead of error message field
                    String errorMessage = "";
                    if (!StringUtils.isEmpty(createTmResponse.getErrorMessage()))
                    {
                        errorMessage = createTmResponse.getErrorMessage();
                    }
                    else if (!StringUtils.isEmpty(createTmResponse.getMessage()))
                    {
                        errorMessage = createTmResponse.getMessage();
                    }
                    errorMessage = String.format(Messages.getString("CreateTmController.4"), errorMessage);
                    LOGGER.error(errorMessage);
                    // Set TM Name, locales and TM Shortcode for user visibility.
                    createTmResponse.setFailureCreateTmInfo(tmCreateOption);
                }
                else
                {
                    // Create PD CSV File
                    if (tmCreateOption.isPdCsvCreationEnabled())
                    {
                        List<Object> subArray = new ArrayList<>();
                        constructPDCsvForTmCreation(subArray, tmCreateOption, tmServerInfo, targetLanguage);
                        pdCsvContent.add(subArray);
                    }
                    // log user info to file to keep track of who creates the TM.
                    String message = String.format(Messages.getString("CreateTmController.22"),
                        tmServerInfo.getTmUserName(), tmCreateOption.getTmName(),
                        tmCreateOption.getSourceLanguage(), tmCreateOption.getTargetLanguage(),
                        tmCreateOption.getTmShortCode());
                    LOGGER.info(message);
                }
                tmCreateResponse.addCreateTmResponse(createTmResponse);
            }

            tmCreateResponse.setPdCsvContent(pdCsvContent);
            // refresh Tm List:
            TmListResponse tmListResponse = tmServerApi.fetchTmList(tmServerInfo);
            if (!tmCreateResponse.getCreateTmResponses().isEmpty())
            {
                storeTmList(getSessionId(), tmListResponse);
            }

            return ResponseEntity.status(HttpStatus.CREATED).body(tmCreateResponse);
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.4"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    /**
     * Create TM with provided CSV and selected Tm Group. Tm ShortCode will use the one from CSV
     * file.
     */
    @ApiOperation(value = "REST API for creating TMs in TM Server with user provided CSV file")
    @ApiResponses({
        @ApiResponse(code = 201, message = "TMs are created successfully", response = JSONArray.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/tmscsv")
    public ResponseEntity<?> createTmWithCSV(@RequestHeader(value = "Authorization") final String authToken,
        @RequestParam(value = "uploadedFiles", required = true) MultipartFile[] uploadFiles,
        @RequestParam(value = "requestInfo", required = true) TmCreateWithCsvRequest tmCreateWithCsvRequest)
    {
        List<ErrorResponse> errorArr = new ArrayList<>();
        JSONArray warnJSONArray = new JSONArray();
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH) &&
            StringUtils.isNotEmpty(tmCreateWithCsvRequest.getAccessToken());

        String securityTicket = null;
        String accessToken = null;

        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        MultipartFile uploadFile = uploadFiles[0];

        try
        {
            List<List<Object>> pdCsvContent = new ArrayList<>();

            if (isTPAuth)
            {
                accessToken = tmCreateWithCsvRequest.getAccessToken();
            }
            else
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            TmCreateOption tmCreateOption = new TmCreateOption();

            if (tmCreateWithCsvRequest.isPdCsvCreationEnabled())
            {
                if (!tmCreateWithCsvRequest.isEmptyCleanupUserInfo())
                {
                    constructCsvHeader(pdCsvContent, Constants.PD_CSV_HEADERS);
                }
                else
                {
                    constructCsvHeader(pdCsvContent, Constants.PD_CSV_NEW_HEADERS);
                }
                tmCreateOption.setCleanupUsername(tmCreateWithCsvRequest.getCleanupUsername());
                tmCreateOption.setEmptyCleanupUserInfo(tmCreateWithCsvRequest.isEmptyCleanupUserInfo());
            }

            TmCreateResponse tmCreateResponse = new TmCreateResponse();

            String line = "";
            BufferedReader br = new BufferedReader(new InputStreamReader(uploadFile.getInputStream(), StandardCharsets.UTF_8));
            // Skip the csv header.
            br.readLine();
            while ((line = br.readLine()) != null)
            {
                if (StringUtils.isEmpty(line))
                {
                    continue;
                }
                // use comma as separator
                String[] info = line.split(",");
                if (info.length != 7 && info.length != 8)
                {
                    String errMessage = String.format(Messages.getString("CreateTmController.22"), line);
                    LOGGER.warn(errMessage);
                    JSONObject err = new JSONObject();
                    err.put("global", errMessage);
                    warnJSONArray.put(err);
                }

                boolean isCreateTmInMultiTmGroups = tmCreateWithCsvRequest.isCreateTmInMultipleTmGroup();
                if (info.length == 7 && !isCreateTmInMultiTmGroups)
                {
                    initializeTmCreateOption(tmCreateWithCsvRequest, tmCreateOption);
                    constructTmCreateOptionWithCSVInfo(info, tmCreateOption);
                }
                // The CSV file contains TM Group Name.
                else if (info.length == 8 && isCreateTmInMultiTmGroups)
                {
                    String tmGroupName = info[0];
                    initializeTmCreateOptionWithTmName(tmCreateWithCsvRequest, tmGroupName, tmCreateOption);
                    if (tmCreateOption.getSelectedTmGroup() == null)
                    {
                        // No matching TM Group is found
                        continue;
                    }
                    constructTmCreateOptionWithCSVInfoInMultiTmGroups(info, tmCreateOption);
                }

                CreateTmResponse createTmResponse = tmServerApi.createTm(tmServerInfo, tmCreateOption, securityTicket, accessToken);
                if (!createTmResponse.isSuccess())
                {
                    String errorMessage = String.format(Messages.getString("CreateTmController.4"), createTmResponse.getErrorMessage());
                    LOGGER.error(errorMessage);
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    errorArr.add(errorResponse);
                }
                else
                {
                    // Create PD CSV File
                    if (tmCreateWithCsvRequest.isPdCsvCreationEnabled())
                    {
                        List<Object> subArray = new ArrayList<>();
                        String targetLanguage = tmCreateOption.getTargetLanguage();
                        constructPDCsvForTmCreation(subArray, tmCreateOption, tmServerInfo, targetLanguage);
                        pdCsvContent.add(subArray);
                    }
                    // log user info to file to keep track of who creates the TM.
                    String message = String.format(Messages.getString("CreateTmController.22"),
                        tmServerInfo.getTmUserName(), tmCreateOption.getTmName(),
                        tmCreateOption.getSourceLanguage(), tmCreateOption.getTargetLanguage(),
                        tmCreateOption.getTmShortCode());
                    LOGGER.info(message);
                }
                tmCreateResponse.addCreateTmResponse(createTmResponse);
            }

            tmCreateResponse.setPdCsvContent(pdCsvContent);
            if (errorArr.isEmpty())
            {
                LOGGER.info(Messages.getString("CreateTmController.21"));
                return ResponseEntity.status(HttpStatus.CREATED).body(tmCreateResponse);
            }
            else
            {
                ErrorResponse errorResponse = new ErrorResponse();
                errorResponse.setGlobal(errorArr.toString());
                errorResponse.setWarn(warnJSONArray.toString());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
        }
        catch (IOException ex)
        {
            String errorMessage = ex.getMessage();
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }

    @ApiOperation(value = "REST API for generatng TM cleanup passcode from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Cleanup passcode is succesfully retrieved", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v1/tm/cleanupPasscode")
    public ResponseEntity<?> createTmCleanupPasscode(@RequestHeader(value = "Authorization") String authToken,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;

        try
        {
            if (!isTPAuth)
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }
            TmCleanupPasscodeResponse tmCleanupPasscodeResponse = tmServerApi.createTmCleanupPasscode(tmServerInfo, securityTicket, accessToken);

            if (tmCleanupPasscodeResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.11"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!tmCleanupPasscodeResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.11"), tmCleanupPasscodeResponse.getErrorMessage());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);

            }
            return ResponseEntity.ok(tmCleanupPasscodeResponse);
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.11"), ex.getMessage());
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    private String prepareSecurityTicket(Map<String, Object> tmsCredential, TmServerInfo tmServerInfo, boolean isTPAuthToken) throws IOException
    {
        String securityTicket = null;
        TmListResponse tmListResponse = getTmList(tmsCredential, tmServerInfo);
        if (tmListResponse == null || tmListResponse.getTmList().isEmpty())
        {
            String errorMessage = Messages.getString("CreateTmController.2");
            LOGGER.warn(errorMessage);
            return securityTicket;
        }
        if (!isTPAuthToken)
        {
            securityTicket = getSecurityTicketFromAvailableTm(tmListResponse, tmServerInfo);
        }
        return securityTicket;
    }

    @ApiOperation(value = "REST API for creating TM Group in TM Server")
    @ApiResponses({
        @ApiResponse(code = 201, message = "Tm Group is created successfully", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/groups")
    public ResponseEntity<?> createTmGroup(@RequestHeader(value = "Authorization") String authToken,
        @RequestBody TmGroupOption tmGroupOption)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;
        try
        {
            String accessToken = null;
            if (isTPAuth)
            {
                accessToken = tmGroupOption.getAccessToken();
            }
            else
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            TmGroupCreateResponse tmGroupResponse = tmServerApi.createTmGroup(tmServerInfo, tmGroupOption, securityTicket, accessToken);
            if (tmGroupResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.8"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!tmGroupResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.8"), tmGroupResponse.getErrorMessage());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            return ResponseEntity.status(HttpStatus.CREATED).body(tmGroupResponse);
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.8"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    /**
     * May not be using this time, because we generate the tmShortCode automatically.
     */
    @ApiOperation(value = "REST API of creating TM Shortcode from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "TM Shortcode is succesfully retrieved.", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v1/tm/shortCode")
    public ResponseEntity<?> createTmShortCode(@RequestHeader(value = "Authorization") String authToken,
        @ApiParam(value = "TM Name", required = false) @RequestParam(value = "name", required = true) String tmName,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;

        try
        {
            if (!isTPAuth)
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            CreateShortcodeResponse createShortCodeResponse = tmServerApi.createTmShortCode(tmServerInfo, tmName, securityTicket, accessToken);
            if (createShortCodeResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.9"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!createShortCodeResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.9"), createShortCodeResponse.getErrorMessage());
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            return ResponseEntity.ok(createShortCodeResponse.getShortCode());
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.9"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @ApiOperation(value = "REST API for creating CSV file containing TM Strings from TM Server")
    @ApiResponses({
        @ApiResponse(code = 201, message = "TM Strings CSV has created successfully", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/csv/tms")
    public ResponseEntity<?> createTmStringCsv(@RequestHeader(value = "Authorization") final String authToken,
                                               @RequestBody final CreateTmCsvOption createTmCsvOption)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        try
        {
            Set<String> tmGroupShortCodes = getSelectedTmGroupSet(createTmCsvOption.getSelectedTmGroup());
            TmListResponse tmListResponse = tmServerApi.fetchTmListWithTmGroupCodes(tmServerInfo, tmGroupShortCodes);

            if (tmListResponse == null || tmListResponse.getTmList().isEmpty())
            {
                String errorMessage = Messages.getString("CreateTmController.2");
                LOGGER.warn(errorMessage);

                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            List<List<Object>> result = constructTmStringCsv(tmListResponse.getTmList(),
                createTmCsvOption.getSelectedOrganization());
            return ResponseEntity.status(HttpStatus.CREATED).body(result);
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.10"), ex.getMessage());
            LOGGER.error(errorMessage);

            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }

    @ApiOperation(value = "REST API for getting user roles from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "User roles are succesfully retrieved", response = DropdownOption.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v1/tm/roles")
    public ResponseEntity<?> getGenericUserRole(@RequestHeader(value = "Authorization") String authToken,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;

        try
        {
            if (!isTPAuth)
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }
            GenericRolesResponse genericRolesResponse = tmServerApi.getGenericRoles(tmServerInfo, securityTicket, accessToken);
            if (genericRolesResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.12"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            if (!genericRolesResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.12"), genericRolesResponse.getErrorMessage());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            return ResponseEntity.ok(genericRolesResponse.getRoles());
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.12"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @ApiOperation(value = "REST API of getting language list from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Language List is succesfully retrieved", response = DropdownOption.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v1/tm/languages")
    public ResponseEntity<?> getLanguageList(@RequestHeader(value = "Authorization") String authToken,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;

        try
        {
            if (!isTPAuth)
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            FetchTmLanguageResponse fetchTmLanguageResponse = tmServerApi.getLanguageList(tmServerInfo, securityTicket, accessToken);
            if (fetchTmLanguageResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.13"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            if (!fetchTmLanguageResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.13"), fetchTmLanguageResponse.getErrorMessage());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            return ResponseEntity.ok(fetchTmLanguageResponse.getLanguages());
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.13"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @ApiOperation(value = "REST API of getting organization list from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Organization List is succesfully retrieved", response = DropdownOption.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v1/tm/organizations")
    public ResponseEntity<?> getOrganizationList(@RequestHeader(value = "Authorization") String authToken,
        @ApiParam(value = "isRefreshTmList", required = false) @RequestParam(value = "r", required = false) boolean refreshTmList,
        @ApiParam(value = "isRefreshOrgList", required = false) @RequestParam(value = "refreshOrgList", required = false) boolean refreshOrganizationList,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        if (refreshTmList)
        {
            tmsCredential.put("refreshTmList", refreshTmList);
        }
        if (refreshOrganizationList)
        {
            tmsCredential.put("refreshOrganizationList", refreshOrganizationList);
        }

        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;

        try
        {
            if (!isTPAuth)
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            FetchOrganizationsResponse fetchOrganizationsResponse = tmServerApi.getOrganizationList(tmServerInfo, securityTicket, accessToken);
            if (fetchOrganizationsResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.14"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!fetchOrganizationsResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.14"), fetchOrganizationsResponse.getErrorMessage());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            return ResponseEntity.ok(fetchOrganizationsResponse.getOrganizations());
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.14"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    public Map<String, String> getQueryMap(String query)
    {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params)
        {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }

    @ApiOperation(value = "REST API for getting TM Group list from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Tm Group List is succesfully retrieved", response = DropdownOption.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v1/tm/groups")
    public ResponseEntity<?> getTmGroupList(@RequestHeader(value = "Authorization") String authToken,
        @ApiParam(value = "isRefreshTmList", required = false) @RequestParam(value = "r", required = false) boolean refreshTmList,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        if (refreshTmList)
        {
            tmsCredential.put("refreshTmList", refreshTmList);
        }

        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;

        try
        {
            if (!isTPAuth)
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            FetchTmGroupsResponse fetchTmGroupResponse = tmServerApi.getGroupList(tmServerInfo, securityTicket, accessToken);
            if (fetchTmGroupResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.26"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!fetchTmGroupResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.26"), fetchTmGroupResponse.getErrorMessage());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            return ResponseEntity.ok(fetchTmGroupResponse.getProjects());
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.26"), ex.getMessage());
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @ApiOperation(value = "REST API for validating TM Cleanup passcode with TM Server")
    @ApiResponses({
        @ApiResponse(code = 201, message = "Tm cleanup passcode is valid", response = String.class),
        @ApiResponse(code = 400, message = "Bad request, Tm cleanup passcode is invalid", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/cleanupPasscode")
    public ResponseEntity<?> validateTmCleanupPassword(@RequestHeader(value = "Authorization") String authToken,
        @RequestBody ValidateTmCleanupCodeRequest validTmCleanupCodeReq)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;

        try
        {
            String accessToken = null;
            if (isTPAuth)
            {
                accessToken = validTmCleanupCodeReq.getAccessToken();
            }
            else
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }
            ValidateCodeResponse validateCodeResponse = tmServerApi.validateTmCleanupPasscode(tmServerInfo,
                validTmCleanupCodeReq.getCleanupCode(), securityTicket, accessToken);
            if (validateCodeResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.18"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!validateCodeResponse.isSuccess())
            {
                String errorMessage = Messages.getString("CreateTmController.18");
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            return ResponseEntity.status(HttpStatus.CREATED).body("valid");
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.18"), ex.getMessage());
            LOGGER.error(errorMessage);
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @ApiOperation(value = "REST API for getting TM Group Code with TM Server")
    @ApiResponses({ @ApiResponse(code = 201, message = "Tm group code is valid", response = String.class),
        @ApiResponse(code = 400, message = "Bad request, Tm group code is invalid", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping("/api/v1/tm/groupCode")
    public ResponseEntity<?> createTmGroupCode(@RequestHeader(value = "Authorization") final String authToken,
        @RequestParam(value = "name", required = true) final String tmGroupName,
        @ApiParam(value = "TPAuth Access token", required = false) @RequestParam(value = "access_token", required = false) String accessToken)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);

        String securityTicket = null;
        try
        {
            if (!isTPAuth)
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }
            CreateShortcodeResponse createShortCodeResponse = tmServerApi.createTmGroupShortCode(tmServerInfo, tmGroupName, securityTicket, accessToken);
            if (createShortCodeResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.24"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!createShortCodeResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.24"), createShortCodeResponse.getErrorMessage());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            return ResponseEntity.status(HttpStatus.OK).body(createShortCodeResponse.getShortCode());
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.24"), ex.getMessage());
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @ApiOperation(value = "REST API for validating TM Group Code with TM Server")
    @ApiResponses({ @ApiResponse(code = 201, message = "Tm group code is valid", response = String.class),
        @ApiResponse(code = 400, message = "Bad request, Tm group code is invalid", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/groupCode")
    public ResponseEntity<?> validateTmGroupCode(@RequestHeader(value = "Authorization") String authToken,
                                                 @RequestBody ValidateTmGroupCodeRequest codeRequest)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;

        try
        {
            String accessToken = null;
            if (isTPAuth)
            {
                accessToken = codeRequest.getAccessToken();
            }
            else
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }
            ValidateCodeResponse validateCodeResponse = tmServerApi.validateTmGroupCode(tmServerInfo,
                codeRequest.getGroupCode(), securityTicket, accessToken);
            if (validateCodeResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.19"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!validateCodeResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.19"), validateCodeResponse.getErrorMessage());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            return ResponseEntity.ok(validateCodeResponse);
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.19"), ex.getMessage());
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    /**
     * May not be using this function at this time, because we generate the tmShortCode
     * automatically.
     */
    @ApiOperation(value = "REST API for validating TM Short Code with TM Server")
    @ApiResponses({ @ApiResponse(code = 201, message = "Tm shortcode is valid", response = String.class),
        @ApiResponse(code = 400, message = "Bad request, Tm shortcode is invalid", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/shortCode")
    public ResponseEntity<?> validateTmShortCode(@RequestHeader(value = "Authorization") String authToken,
                                                 @RequestBody final ValidateTmShortCodeRequest validateTmShortCodeReq)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        boolean isTPAuth = tmsCredential.containsKey(Constants.TP_AUTH);
        String securityTicket = null;

        try
        {
            String accessToken = null;
            if (isTPAuth)
            {
                accessToken = validateTmShortCodeReq.getAccessToken();
            }
            else
            {
                securityTicket = prepareSecurityTicket(tmsCredential, tmServerInfo, isTPAuth);
                if (StringUtils.isEmpty(securityTicket))
                {
                    String errorMessage = Messages.getString("CreateTmController.2");
                    ErrorResponse errorResponse = createErrorResponse(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                }
            }

            ValidateCodeResponse validateCodeResponse = tmServerApi.validateTmShortCode(tmServerInfo,
                validateTmShortCodeReq.getAccessToken(), securityTicket, accessToken);
            if (validateCodeResponse == null)
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.20"), Messages.getString("CreateTmController.25"));
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            if (!validateCodeResponse.isSuccess())
            {
                String errorMessage = String.format(Messages.getString("CreateTmController.20"), validateCodeResponse.getErrorMessage());
                LOGGER.error(errorMessage);
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }

            return ResponseEntity.ok(validateCodeResponse);
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("CreateTmController.20"), ex.getMessage());
            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

    @PostMapping("/api/v1/tm/duplicate")
    public ResponseEntity<DuplicateTmCheckResponse> checkDuplicateTmInSameGroup(@RequestHeader(value = "Authorization") String authToken,
                                                                                @RequestBody final TmCreateOption tmCreateOption)
    {
        Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
        TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
        TmListResponse tmListResponse;

        DuplicateTmCheckResponse res = new DuplicateTmCheckResponse();
        res.setSuccess(true);
        try
        {
            Set<String> tmGroupShortCodes = new HashSet<>();
            tmGroupShortCodes.add(tmCreateOption.getSelectedTmGroup().getShortcode());
            tmListResponse = tmServerApi.fetchTmListWithTmGroupCodes(tmServerInfo, tmGroupShortCodes);

            List<TmInfo> tmList = tmListResponse.getTmList();
            Set<String> languagePairSet = convertToTmLanguagePairSet(tmList);

            String sourceLanguage = tmCreateOption.getSourceLanguage();
            for (String targetLanguage : tmCreateOption.getTargetLanguages())
            {
                TmLanguagePair pair = new TmLanguagePair();
                if (tmCreateOption.isInvertedLanguage())
                {
                    pair.setTargetLanguage(sourceLanguage);
                    pair.setSourceLanguage(targetLanguage);
                }
                else
                {
                    pair.setSourceLanguage(sourceLanguage);
                    pair.setTargetLanguage(targetLanguage);
                }

                String languagePair = constructLanguagePairText(pair.getSourceLanguage(), pair.getTargetLanguage());
                if (languagePairSet.contains(languagePair))
                {
                    res.addDuplicateTmLanugagePair(pair);
                    res.setSuccess(false);
                }
            }
        }
        catch (IOException ex)
        {
            res.setSuccess(false);
            String message = String.format(Messages.getString("CreateTmController.28"), ex.getMessage());
            res.setErrorMessage(message);
        }
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    private Set<String> convertToTmLanguagePairSet(List<TmInfo> tmList)
    {
        Set<String> set = new LinkedHashSet<>();
        if (tmList == null || tmList.isEmpty())
        {
            return set;
        }

        for (TmInfo tmInfo : tmList)
        {
            String sourceLanguage = tmInfo.getSourceLanguage();
            String targetLanguage = tmInfo.getTargetLanguage();
            String languagePairText = constructLanguagePairText(sourceLanguage, targetLanguage);
            set.add(languagePairText);
        }
        return set;
    }

    private String constructLanguagePairText(String sourceLanguage, String targetLanguage)
    {
        StringBuffer sb = new StringBuffer();
        sb.append(sourceLanguage);
        sb.append("->");
        sb.append(targetLanguage);
        return sb.toString();
    }

    private void initializeTmCreateOption(TmCreateWithCsvRequest tmCreateWithCsvRequest, TmCreateOption tmCreateOption)
    {
        tmCreateOption.setEnabled(tmCreateWithCsvRequest.isEnabled());
        tmCreateOption.setContextModeEnabled(tmCreateWithCsvRequest.isContextModeEnabled());
        tmCreateOption.setSelectedOrganization(tmCreateWithCsvRequest.getSelectedOrganization());
        tmCreateOption.setSelectedTmGroup(tmCreateWithCsvRequest.getSelectedTmGroup());
    }

    private void initializeTmCreateOptionWithTmName(TmCreateWithCsvRequest tmCreateWithCsvRequest, String tmGroupName,
        TmCreateOption tmCreateOption)
    {
        tmCreateOption.setEnabled(tmCreateWithCsvRequest.isEnabled());
        tmCreateOption.setContextModeEnabled(tmCreateWithCsvRequest.isContextModeEnabled());

        tmCreateOption.setSelectedOrganization(tmCreateWithCsvRequest.getSelectedOrganization());

        // Find the correct TM Group:
        List<DropdownOption> selectedTmGroups = tmCreateWithCsvRequest.getSelectedTmGroups();
        for (DropdownOption tmGroup : selectedTmGroups)
        {
            String label = tmGroup.getLabel();
            if (StringUtils.equals(tmGroupName, label))
            {
                tmCreateOption.setSelectedTmGroup(tmGroup);
                break;
            }
        }
    }

    private void constructTmCreateOptionWithCSVInfo(String[] info, TmCreateOption tmCreateOption)
    {
        tmCreateOption.setCreateTMWithCSV(true);
        tmCreateOption.setTmName(info[0]);
        tmCreateOption.setSourceLanguage(info[1]);
        tmCreateOption.setTargetLanguage(info[2]);
        tmCreateOption.setTmShortCode(info[3]);
        tmCreateOption.setCleanupCode(info[4]);
        tmCreateOption.setGenericUsername(info[5]);
        tmCreateOption.updateGenericUserPassword(info[6]);

        tmCreateOption.setAppendLanguagesToTmName(false);
        tmCreateOption.setAllGenericUserpasswordSame(true);
        tmCreateOption.setAllTmCleanupPasscodeSame(true);

    }

    private void constructTmCreateOptionWithCSVInfoInMultiTmGroups(String[] info, TmCreateOption tmCreateOption)
    {
        tmCreateOption.setCreateTMWithCSV(true);
        tmCreateOption.setTmName(info[1]);
        tmCreateOption.setSourceLanguage(info[2]);
        tmCreateOption.setTargetLanguage(info[3]);
        tmCreateOption.setTmShortCode(info[4]);
        tmCreateOption.setCleanupCode(info[5]);
        tmCreateOption.setGenericUsername(info[6]);
        tmCreateOption.updateGenericUserPassword(info[7]);

        tmCreateOption.setAppendLanguagesToTmName(false);
        tmCreateOption.setAllGenericUserpasswordSame(true);
        tmCreateOption.setAllTmCleanupPasscodeSame(true);
    }

    private void constructPDCsvForTmCreation(List<Object> subArray, TmCreateOption tmCreateOption,
        TmServerInfo tmServerInfo, String targetLanguage)
    {
        final int updateTm = 1;
        final int addFileName = 1;

        // put TM Profile Name: Assume user always want to use generic user as Tm Profile name\
        if (!tmCreateOption.isUseGenericUsernameAsProfileName())
        {
            subArray.add(tmCreateOption.getTmName());
        }
        else
        {
            // Generic username is original generic username + locales
            subArray.add(tmCreateOption.getUser().getUsername());
        }
        // In PD 5.8 and TM Server 5.2, they remove Cleanup user info
        if (!tmCreateOption.isEmptyCleanupUserInfo())
        {
            subArray.add(tmServerInfo.getURL());
            subArray.add(tmCreateOption.getCleanupUsername());
            subArray.add(tmCreateOption.getCleanupCode());
        }
        else
        {
            subArray.add(StringUtils.EMPTY);
            subArray.add(StringUtils.EMPTY);
            subArray.add(StringUtils.EMPTY);
        }

        // Linguist URL
        subArray.add(tmServerInfo.getURL());
        subArray.add(tmCreateOption.getUser().getUsername());
        subArray.add(tmCreateOption.getUser().getPassword());
        // TM Path: tmGroupCode + tmShortCode
        String tmGroupShortCode = tmCreateOption.getSelectedTmGroup().getShortcode();
        String tmPath = constructTmPath(tmGroupShortCode, tmCreateOption.getTmShortCode());
        subArray.add(tmPath);
        // TMSTool-112: handle inverted language case
        subArray.add(tmCreateOption.getSourceLanguage());
        subArray.add(tmCreateOption.getTargetLanguage());

        subArray.add(updateTm);
        // In PD 5.8 and TM Server 5.2, they add a new field called TM Update Passcode
        if (tmCreateOption.isEmptyCleanupUserInfo())
        {
            subArray.add(tmCreateOption.getCleanupCode());
        }
        subArray.add(addFileName);
        subArray.add(tmCreateOption.getPaClientName());
        DropdownOption selectedOrganization = tmCreateOption.getSelectedOrganization();
        if (selectedOrganization == null)
        {
            subArray.add(StringUtils.EMPTY);
        }
        else
        {
            String organization = selectedOrganization.getLabel();
            if (StringUtils.isEmpty(organization))
            {
                subArray.add(StringUtils.EMPTY);
            }
            else
            {
                subArray.add(organization);
            }
        }
    }

    private String constructTmPath(String tmGroupShortCode, String tmShortCode)
    {
        StringBuffer sb = new StringBuffer();
        sb.append(tmGroupShortCode);
        sb.append(Constants.SLASH);
        sb.append(tmShortCode);
        return sb.toString();
    }
}
