package org.gs4tr.tmserver.tool.controller.jobqueue.importTm;

public enum ImportTmPhase
{
    WAITING, START, VALIDATION, IMPORT, ROLLBACK, CLEANUP, FINISHED, FAILED
}
