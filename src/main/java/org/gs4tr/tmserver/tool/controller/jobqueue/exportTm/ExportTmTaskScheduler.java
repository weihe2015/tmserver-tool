package org.gs4tr.tmserver.tool.controller.jobqueue.exportTm;

import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

import org.gs4tr.tmserver.tool.controller.jobqueue.Task;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ExportTmTaskScheduler
{
    private ExecutorService exportTmTaskPoolExecutor;
    private ExecutorService exportTmTaskScheduler = Executors.newSingleThreadExecutor();
    private PriorityBlockingQueue<Task> exportTmJobQueue;

    public ExportTmTaskScheduler(@Value("${thread.max-pool:4}") int maxPoolSize,
        @Value("${thread.queue.capacity:2000}") int queueCapacity)
    {
        exportTmTaskPoolExecutor = Executors.newFixedThreadPool(maxPoolSize);
        exportTmJobQueue = new PriorityBlockingQueue<Task>(queueCapacity, Comparator.comparing(Task::getJobPriority));
        exportTmTaskScheduler.execute(() ->
        {
            while (true)
            {
                try
                {
                    Task task = exportTmJobQueue.take();
                    exportTmTaskPoolExecutor.execute(task);
                }
                catch (InterruptedException ex)
                {
                    // exception needs special handling
                    break;
                }
            }
        });
    }

    public void scheduleTask(Task task)
    {
        exportTmJobQueue.add(task);
    }
}
