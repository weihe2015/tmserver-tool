package org.gs4tr.tmserver.tool.controller.model;

import org.gs4tr.tmserver.tool.model.api.TmServerInfo;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class RemoteTmServerCredential
{
    private String url;
    private String sub;
    private String pass;
    private String sid;

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getSub()
    {
        return sub;
    }

    public void setSub(String sub)
    {
        this.sub = sub;
    }

    public String getPass()
    {
        return pass;
    }

    public void setPass(String pass)
    {
        this.pass = pass;
    }

    public TmServerInfo createNewTmServerInfo()
    {
        return new TmServerInfo(url, sub, pass, true);
    }

    public String getSid()
    {
        return sid;
    }

    public void setSid(String sid)
    {
        this.sid = sid;
    }
}
