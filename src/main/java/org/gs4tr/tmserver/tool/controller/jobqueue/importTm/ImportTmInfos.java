package org.gs4tr.tmserver.tool.controller.jobqueue.importTm;

import java.io.File;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.gs4tr.tmserver.tool.controller.model.TmImportOption;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ImportTmInfos implements Serializable
{
    private static final long serialVersionUID = 4772455071947160461L;
    private TmImportOption tmImportOption;
    private String jobId;
    private Map<String, ImportTmInfo> infoMap = new LinkedHashMap<>();
    private String accessToken;
    private boolean isTPAuth;
    private List<File> uploadTmxFiles;
    private String errorMessage;

    public ImportTmInfos(String jobId, TmImportOption tmImportOption, List<File> uploadTmxFiles, boolean isTPAuth,
        String accessToken)
    {
        this.jobId = jobId;
        this.uploadTmxFiles = uploadTmxFiles;
        this.accessToken = accessToken;
        this.tmImportOption = tmImportOption;
        this.isTPAuth = isTPAuth;
    }

    public String getJobId()
    {
        return jobId;
    }

    public void setJobId(String jobId)
    {
        this.jobId = jobId;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    public List<File> getUploadTmxFiles()
    {
        return uploadTmxFiles;
    }

    public void setUploadTmxFile(List<File> uploadTmxFiles)
    {
        this.uploadTmxFiles = uploadTmxFiles;
    }

    public TmImportOption getTmImportOption()
    {
        return tmImportOption;
    }

    public void setTmImportOption(TmImportOption tmImportOption)
    {
        this.tmImportOption = tmImportOption;
    }

    public boolean isTPAuth()
    {
        return isTPAuth;
    }

    public void setTPAuth(boolean isTPAuth)
    {
        this.isTPAuth = isTPAuth;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public Map<String, ImportTmInfo> getInfoMap()
    {
        return infoMap;
    }

    public void setInfoMap(Map<String, ImportTmInfo> infoMap)
    {
        this.infoMap = infoMap;
    }

    public void addInfoToMap(String uploadFileName, ImportTmInfo info)
    {
        infoMap.put(uploadFileName, info);
    }

    public boolean importTmInfoAvailableByUploadFileName(String uploadFileName)
    {
        return infoMap.containsKey(uploadFileName);
    }

    public ImportTmInfo getImportTmInfo(String uploadFileName)
    {
        return infoMap.get(uploadFileName);
    }

}
