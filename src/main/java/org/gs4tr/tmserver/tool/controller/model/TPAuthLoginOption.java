package org.gs4tr.tmserver.tool.controller.model;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TPAuthLoginOption
{
    // user email:
    private String email;
    // token create timestamp
    private String creation;
    // token expire timestamp
    private String expiration;
    // TM Server TPAuth access token
    private String accessToken;
    // TM Server id token:
    private String idToken;


    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getCreation()
    {
        return creation;
    }

    public void setCreation(String creation)
    {
        this.creation = creation;
    }

    public String getExpiration()
    {
        return expiration;
    }

    public void setExpiration(String expiration)
    {
        this.expiration = expiration;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getIdToken()
    {
        return idToken;
    }

    public void setIdToken(String idToken)
    {
        this.idToken = idToken;
    }
}
