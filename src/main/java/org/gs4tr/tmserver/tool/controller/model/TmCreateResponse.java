package org.gs4tr.tmserver.tool.controller.model;

import java.util.ArrayList;
import java.util.List;

import org.gs4tr.tmserver.tool.api.model.CreateTmResponse;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmCreateResponse
{
    private List<CreateTmResponse> createTmResponses;
    private List<List<Object>> pdCsvContent;

    public List<List<Object>> getPdCsvContent()
    {
        return pdCsvContent;
    }

    public void setPdCsvContent(List<List<Object>> pdCsvContent)
    {
        this.pdCsvContent = pdCsvContent;
    }

    public List<CreateTmResponse> getCreateTmResponses()
    {
        return createTmResponses;
    }

    public void setCreateTmResponses(List<CreateTmResponse> createTmResponses)
    {
        this.createTmResponses = createTmResponses;
    }

    public void addCreateTmResponse(CreateTmResponse createTmResponse)
    {
        if (createTmResponses == null)
        {
            createTmResponses = new ArrayList<>();
        }
        createTmResponses.add(createTmResponse);
    }
}
