package org.gs4tr.tmserver.tool.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmImportErrorResponse
{
    private String errorMessage;
    private String tmName;

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getTmName()
    {
        return tmName;
    }

    public void setTmName(String tmName)
    {
        this.tmName = tmName;
    }

}
