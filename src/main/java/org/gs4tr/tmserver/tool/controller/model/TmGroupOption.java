package org.gs4tr.tmserver.tool.controller.model;

public class TmGroupOption
{
    private DropdownOption selectedOrganization;
    private String tmGroupName;
    private String tmGroupShortcode;
    private String accessToken;

    public DropdownOption getSelectedOrganization()
    {
        return selectedOrganization;
    }

    public void setSelectedOrganization(DropdownOption selectedOrganization)
    {
        this.selectedOrganization = selectedOrganization;
    }

    public String getTmGroupName()
    {
        return this.tmGroupName;
    }

    public String getTmGroupShortcode()
    {
        return this.tmGroupShortcode;
    }

    public void setTmGroupName(String tmGroupName)
    {
        this.tmGroupName = tmGroupName;
    }

    public void setTmGroupShortcode(String tmGroupShortcode)
    {
        this.tmGroupShortcode = tmGroupShortcode;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }
}
