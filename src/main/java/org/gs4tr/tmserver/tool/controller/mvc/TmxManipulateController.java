package org.gs4tr.tmserver.tool.controller.mvc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.controller.TmServerToolController;
import org.gs4tr.tmserver.tool.controller.model.ErrorResponse;
import org.gs4tr.tmserver.tool.controller.model.TmxManipulateOption;
import org.gs4tr.tmserver.tool.model.export.TmFtpAdaptor;
import org.gs4tr.tmserver.tool.model.thread.ManipulateTmxThread;
import org.gs4tr.tmserver.tool.service.file.Constants;
import org.gs4tr.tmserver.tool.service.file.FolderUtils;
import org.gs4tr.tmserver.tool.service.file.ZipFileUtils;
import org.gs4tr.tmserver.tool.service.tmx.XmlPreDefinedEntityFixer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "REST Endpoint of manipulate TMX files")
public class TmxManipulateController extends TmServerToolController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TmxManipulateController.class);
    private static final String systemDir = System.getProperty("user.dir");

    @Autowired
    private ZipFileUtils zipFileUtils;

    @Autowired
    private FolderUtils folderUtils;

    @Autowired
    private TmFtpAdaptor tmFtpAdaptor;

    @PutMapping("/api/v1/tm/tmx")
    @ApiOperation(value = "REST API of getting user input for manipulate TMX files")
    public ResponseEntity<?> manipulateTmxFiles(@RequestBody final TmxManipulateOption tmxManipulateOption)
        throws JSONException
    {
        List<String> tmxFiles = new ArrayList<String>();
        String date = new SimpleDateFormat(Constants.DATEFORMATE).format(new Date());

        String randomToken = tmxManipulateOption.getRandomToken();
        String folderName = folderUtils.getSourceManipulateDir(systemDir, date, randomToken);
        String targetFolderName = folderName.replaceAll("sourceManipulateDir", "targetManipulateDir");

        try
        {
            // create Target Folder:
            if (!new File(targetFolderName).exists())
            {
                new File(targetFolderName).mkdirs();
            }

            folderUtils.getAllTmxFiles(folderName, tmxFiles);

            final int threadPoolSize = Runtime.getRuntime().availableProcessors() + 1;
            ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);

            ConcurrentMap<String, String> errorMap = new ConcurrentHashMap<String, String>();
            // Remove invalid XML chars here:
            for (String tmxFile : tmxFiles)
            {
                File tempFile = File.createTempFile("temptmx", ".tmp");
                XmlPreDefinedEntityFixer fixer = new XmlPreDefinedEntityFixer(new File(tmxFile), tempFile);
                fixer.clean();
                FileUtils.copyFile(tempFile, new File(tmxFile));
                FileUtils.deleteQuietly(tempFile);
            }

            // Process manipulate tmx files
            for (String tmxFile : tmxFiles)
            {
                String targetFileName = targetFolderName + FilenameUtils.getName(tmxFile);
                Runnable manipulateTmxThread = new ManipulateTmxThread(tmxFile, targetFileName,
                    tmxManipulateOption, errorMap);
                executor.execute(manipulateTmxThread);
            }
            executor.shutdown();
            // Wait until all threads are finish
            while (!executor.isTerminated())
            {
            }

            String targetDateTime = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss").format(new Date());
            String targetZipFileName = folderUtils.generateTargetZipFile(targetFolderName, targetDateTime);
            tmxFiles = new ArrayList<>();

            folderUtils.getAllTmxFiles(targetFolderName, tmxFiles);
            zipFileUtils.zipFiles(tmxFiles, targetZipFileName);

            // Remove source folder:
            FileUtils.deleteDirectory(new File(folderName));

            // Moved files to processed folder
            moveFilesToProcessedFolder(tmxFiles, targetFolderName);

            File zipFile = new File(targetZipFileName);
            if (zipFile.exists())
            {
                if (!tmxManipulateOption.isOutputFileToFTP())
                {
                    return outputFileToStream(targetZipFileName, zipFile, errorMap);
                }
                // Switch to FTP if necessary
                else
                {
                    return outputFileToFTP(targetZipFileName, date);
                }
            }
            else
            {
                String errorMessage = Messages.getString("TmxManipulateController.1");
                ErrorResponse errorResponse = createErrorResponse(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("TmxManipulateController.2"), ex.getMessage());
            LOGGER.error(errorMessage);

            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }

    private ResponseEntity<byte[]> outputFileToStream(String targetZipFileName, File zipFile,
        ConcurrentMap<String, String> errorMap) throws IOException, JSONException
    {
        String fileType = URLConnection.guessContentTypeFromName(targetZipFileName);
        InputStream inputStream = new FileInputStream(zipFile);

        byte[] out = IOUtils.toByteArray(inputStream);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("content-disposition", "attachment; filename=" + zipFile.getName());
        responseHeaders.add("Content-Type", fileType);
        responseHeaders.add("filename", zipFile.getName());

        if (!errorMap.isEmpty())
        {
            // Filter the duplicates:
            Map<String, String> map = new HashMap<String, String>();
            for (String errorFileName : errorMap.keySet())
            {
                map.put(FilenameUtils.getName(errorFileName), errorMap.get(errorFileName));
            }
            JSONArray errorArr = new JSONArray();
            for (String errorFileName : map.keySet())
            {
                JSONObject obj = new JSONObject();
                obj.put("fileName", errorFileName);
                obj.put("errorMessage", map.get(errorFileName));
                errorArr.put(obj);
            }
            responseHeaders.add("errormessages", errorArr.toString());
        }
        return new ResponseEntity<byte[]>(out, responseHeaders, HttpStatus.OK);
    }

    private ResponseEntity<String> outputFileToFTP(String targetZipFileName, String date)
        throws SocketException, IOException, JSONException
    {
        InputStream inputStream = new FileInputStream(targetZipFileName);
        tmFtpAdaptor.exportFileToFtp(inputStream, FilenameUtils.getName(targetZipFileName), date);

        inputStream.close();
        JSONObject objJSON = new JSONObject();
        objJSON.put("filename", FilenameUtils.getName(targetZipFileName));
        return ResponseEntity.ok(objJSON.toString());
    }

    private void moveFilesToProcessedFolder(List<String> tmxFiles, String targetFolderName) throws IOException
    {
        tmxFiles = new ArrayList<>();

        folderUtils.getAllTmxFiles(targetFolderName, tmxFiles);
        for (String tmxFile : tmxFiles)
        {
            File targetFile = new File(tmxFile.replace("targetManipulateDir", "processedDir"));
            new File(targetFile.getParent()).mkdirs();
            if (targetFile.exists())
            {
                FileUtils.deleteQuietly(targetFile);
            }
            FileUtils.moveFile(new File(tmxFile), targetFile);

            String message = String.format(Messages.getString("TmxManipulateController.0"),
                FilenameUtils.getName(tmxFile), targetFile.getParent());
            LOGGER.info(message);
        }
    }
}
