package org.gs4tr.tmserver.tool.controller.jobqueue.importTm;

import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

import org.gs4tr.tmserver.tool.controller.jobqueue.Task;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ImportTmTaskScheduler
{
    private ExecutorService importTmTaskPoolExecutor;
    private ExecutorService importTmTaskScheduler = Executors.newSingleThreadExecutor();
    private PriorityBlockingQueue<Task> importTmJobQueue;

    public ImportTmTaskScheduler(@Value("${thread.import.max-pool:8}") int maxPoolSize,
        @Value("${thread.queue.capacity:2000}") int queueCapacity)
    {
        importTmTaskPoolExecutor = Executors.newFixedThreadPool(maxPoolSize);
        importTmJobQueue = new PriorityBlockingQueue<Task>(queueCapacity, Comparator.comparing(Task::getJobPriority));

        importTmTaskScheduler.execute(() ->
        {
            while (true)
            {
                try
                {
                    Task task = importTmJobQueue.take();
                    importTmTaskPoolExecutor.execute(task);
                }
                catch (InterruptedException ex)
                {
                    // exception needs special handling
                    break;
                }
            }
        });
    }

    public void scheduleTask(Task task)
    {
        importTmJobQueue.add(task);
    }
}
