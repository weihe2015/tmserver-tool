package org.gs4tr.tmserver.tool.controller.mvc;

import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.controller.model.MixedTmInfo;
import org.gs4tr.tmserver.tool.controller.model.TmRenameOption;
import org.gs4tr.tmserver.tool.service.server.TmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "REST Endpoint of renaming TMs from TM Server")
public class TmRenameController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TmRenameController.class);

    @Autowired
    private TmService tmService;

    @ApiOperation(value = "REST API of getting TM info to rename TM names")
    @PutMapping("/api/v1/tm/name")
    public void renameTm(@RequestBody final TmRenameOption tmRenameOption)
    {
        String newTmName = tmRenameOption.getNewTmName();
        for (MixedTmInfo tmInfo : tmRenameOption.getSelectedTmList())
        {
            String oldTmName = tmInfo.getTmName();
            Long tmId = tmInfo.getTmId();
            tmService.updateTmNameById(tmId, newTmName);

            String message = String.format(Messages.getString("RenameTmController.2"), tmId, oldTmName,
                newTmName);
            // Log rename message
            LOGGER.info(message);
        }
    }
}
