package org.gs4tr.tmserver.tool.controller.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.tmserver.tool.api.model.TmInfo;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class MixedTmInfo
{
    static private final String PENDING = "Pending";

    private Long tmId;
    private String label;
    private String value;
    private String tmName;

    private String tmGroupName;
    private String tmPath;

    private String sourceLocaleCode;
    private String sourceLocale;

    private String targetLocaleCode;
    private String targetLocale;

    private Long totalTus;
    private Long numberOfPublicTus;
    private Long numberOfPrivateTus;
    private String lastTuModificationDate;

    private String status;
    private boolean uploaded;


    public void initConcordanceTmInfo(TmInfo tmInfo)
    {
        this.tmName = tmInfo.getTmName();
        this.tmGroupName = tmInfo.getGroupName();
        // 8-29-18, change from locale display name to locale code for front end UI
        // display to append locale code to Tm Name. locale display name is too long.
        this.sourceLocale = tmInfo.getSourceLanguage();
        this.targetLocale = tmInfo.getTargetLanguage();
        this.tmPath = tmInfo.getTmPath();
        this.label = tmName;
        this.value = tmPath;
    }

    public void initImportTmInfo(TmInfo tmInfo)
    {
        this.tmName = tmInfo.getTmName();
        this.tmGroupName = tmInfo.getGroupName();
        this.tmPath = tmInfo.getTmPath();
        this.sourceLocaleCode = tmInfo.getSourceLanguage();
        this.sourceLocale = tmInfo.getSourceLocaleDisplayName();

        this.targetLocaleCode = tmInfo.getTargetLanguage();
        this.targetLocale = tmInfo.getTargetLocaleDisplayName();

        this.status = PENDING;
        this.uploaded = false;
    }

    public void initExportTmInfo(TmInfo tmInfo)
    {
        this.tmName = tmInfo.getTmName();
        this.tmGroupName = tmInfo.getGroupName();
        this.tmPath = tmInfo.getTmPath();
        this.sourceLocaleCode = tmInfo.getSourceLanguage();
        this.sourceLocale = tmInfo.getSourceLocaleDisplayName();

        this.targetLocaleCode = tmInfo.getTargetLanguage();
        this.targetLocale = tmInfo.getTargetLocaleDisplayName();

        this.totalTus = tmInfo.getTuCount();
        this.numberOfPrivateTus = tmInfo.getPrivateTuCount();
        this.numberOfPublicTus = tmInfo.getPublicTuCount();
        this.lastTuModificationDate = convertLongToFormatDate(tmInfo.getLastModificationDate());
        this.status = PENDING;
    }

    public void initRenameTmInfo(Long tmId, String tmName, String tmGroupName, String sourceLocale, String targetLocale, String tmPath)
    {
        this.tmId = tmId;
        this.tmName = tmName;
        this.label = tmName;
        this.value = tmPath;
        this.tmGroupName = tmGroupName;
        this.tmPath = tmPath;
        this.sourceLocaleCode = sourceLocale;
        this.sourceLocale = Locale.makeLocale(this.sourceLocaleCode).getDisplayName();
        this.targetLocaleCode = targetLocale;
        this.targetLocale = Locale.makeLocale(this.targetLocaleCode).getDisplayName();
    }

    private String convertLongToFormatDate(Long date)
    {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(date));
    }

    public String getTmName()
    {
        return tmName;
    }

    public void setTmName(String tmName)
    {
        this.tmName = tmName;
    }

    public String getTmGroupName()
    {
        return tmGroupName;
    }

    public void setTmGroupName(String tmGroupName)
    {
        this.tmGroupName = tmGroupName;
    }

    public String getTmPath()
    {
        return tmPath;
    }

    public void setTmPath(String tmPath)
    {
        this.tmPath = tmPath;
    }

    public String getSourceLocaleCode()
    {
        return sourceLocaleCode;
    }

    public void setSourceLocaleCode(String sourceLocaleCode)
    {
        this.sourceLocaleCode = sourceLocaleCode;
    }

    public String getSourceLocale()
    {
        return sourceLocale;
    }

    public void setSourceLocale(String sourceLocale)
    {
        this.sourceLocale = sourceLocale;
    }

    public String getTargetLocaleCode()
    {
        return targetLocaleCode;
    }

    public void setTargetLocaleCode(String targetLocaleCode)
    {
        this.targetLocaleCode = targetLocaleCode;
    }

    public String getTargetLocale()
    {
        return targetLocale;
    }

    public void setTargetLocale(String targetLocale)
    {
        this.targetLocale = targetLocale;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public boolean isUploaded()
    {
        return uploaded;
    }

    public void setUploaded(boolean uploaded)
    {
        this.uploaded = uploaded;
    }

    public Long getTotalTus()
    {
        return totalTus;
    }

    public void setTotalTus(Long totalTus)
    {
        this.totalTus = totalTus;
    }

    public Long getNumberOfPublicTus()
    {
        return numberOfPublicTus;
    }

    public void setNumberOfPublicTus(Long numberOfPublicTus)
    {
        this.numberOfPublicTus = numberOfPublicTus;
    }

    public Long getNumberOfPrivateTus()
    {
        return numberOfPrivateTus;
    }

    public void setNumberOfPrivateTus(Long numberOfPrivateTus)
    {
        this.numberOfPrivateTus = numberOfPrivateTus;
    }

    public String getLastTuModificationDate()
    {
        return lastTuModificationDate;
    }

    public void setLastTuModificationDate(String lastTuModificationDate)
    {
        this.lastTuModificationDate = lastTuModificationDate;
    }

    public Long getTmId()
    {
        return tmId;
    }

    public void setTmId(Long tmId)
    {
        this.tmId = tmId;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}
