package org.gs4tr.tmserver.tool.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.LoginResponse;
import org.gs4tr.tmserver.tool.api.model.TmInfo;
import org.gs4tr.tmserver.tool.api.model.TmListResponse;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.SessionInfoCache;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.TmInfoListCache;
import org.gs4tr.tmserver.tool.controller.jobqueue.cache.UserDetailsCache;
import org.gs4tr.tmserver.tool.controller.model.DropdownOption;
import org.gs4tr.tmserver.tool.controller.model.ErrorResponse;
import org.gs4tr.tmserver.tool.controller.model.GenericResponse;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.service.user.UserService;
import org.gs4tr.tmserver.tool.session.ExtendedSessionInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@RestController
public abstract class TmServerToolController
{
    protected static final String USER_AGENT_HEADER_NAME = "User-Agent";

    @Value("${jwt.tokenHead}")
    private String tokenHeader;

    @Value("${tmserver.url}")
    protected String _url;

    @Value("${tmserver.tmPowerUsername}")
    protected String _tmPowerUserName;

    @Value("${tmserver.tmPowerUserpassword}")
    protected String _tmPowerUserPassword;

    @Value("${concordance.tmGroupShortCode}")
    private String tmGroupShortCodes;

    @Autowired
    private UserService userService;

    @Autowired
    private TmServerApi tmServerApi;

    @Autowired
    private TmInfoListCache tmInfoListCache;

    @Autowired
    private UserDetailsCache userDetailsCache;

    @Autowired
    private SessionInfoCache sessionInfoCache;

    protected String sessionId;

    protected void addSessionInformation(String sessionId, ExtendedSessionInformation info)
    {
        sessionInfoCache.put(sessionId, info);
    }

    public void storeTmList(String url, TmListResponse response)
    {
        tmInfoListCache.put(url, response);
    }

    protected void addUserInfoCache(String sessionId, UserDetails userDetails)
    {
        userDetailsCache.put(sessionId, userDetails);
    }

    protected void clearServerCache(String sessionId)
    {
        userDetailsCache.remove(sessionId);
    }

    protected GenericResponse createErrorMessage(String message)
    {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setGlobal(message);
        return genericResponse;
    }

    protected ErrorResponse createErrorResponse(String errorMessage)
    {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setGlobal(errorMessage);
        return errorResponse;
    }

    protected ExtendedSessionInformation createNewSessionInfo(String powerUsername)
    {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String sessionId = getSessionId();
        String userAddress = request.getHeader("X-FORWARDED-FOR");
        if (userAddress == null)
        {
            userAddress = request.getRemoteAddr();
        }
        return new ExtendedSessionInformation(powerUsername, sessionId, new Date(), new Date(), userAddress,
            request.getHeader(USER_AGENT_HEADER_NAME));
    }

    protected TmServerInfo createTmServerInfo(Map<String, Object> newTmServerInfo)
    {
        if (newTmServerInfo.containsKey("sid"))
        {
            sessionId = (String) newTmServerInfo.get("sid");
        }
        // User connects to remote Tm Servers
        if (newTmServerInfo.containsKey(("url")))
        {
            String url = (String) newTmServerInfo.get("url");
            String tmPowerUserName = (String) newTmServerInfo.get("sub");
            String tmPowerUserPassword = (String) newTmServerInfo.get("pass");
            return new TmServerInfo(url, tmPowerUserName, tmPowerUserPassword, true);
        }
        // User logins with their own power user credential
        else if (newTmServerInfo.containsKey("sub"))
        {
            String tmPowerUserName = (String) newTmServerInfo.get("sub");
            String tmPowerUserPassword;
            if (!StringUtils.equals("admin", tmPowerUserName))
            {
                tmPowerUserPassword = (String) newTmServerInfo.get("pass");
            }
            else
            {
                tmPowerUserName = _tmPowerUserName;
                tmPowerUserPassword = _tmPowerUserPassword;
            }
            return new TmServerInfo(_url, tmPowerUserName, tmPowerUserPassword);
        }
        else
        {
            return new TmServerInfo(_url, _tmPowerUserName, _tmPowerUserPassword);
        }
    }

    protected TmServerInfo createTmServerInfo(String username, String password)
    {
        return new TmServerInfo(_url, username, password);
    }

    protected Map<String, Object> decodeHeaderToken(String token)
    {
        final String authToken = token.substring(tokenHeader.length());
        return decodeToken(authToken);
    }

    protected Map<String, Object> decodeToken(String token)
    {
        return userService.decodeToken(token);
    }

    protected String getSecurityTicketFromAvailableTm(TmListResponse tmListResponse, TmServerInfo tmServerInfo) throws IOException
    {
        String securityTicket = StringUtils.EMPTY;
        for (TmInfo tmInfo : tmListResponse.getTmList())
        {
            String tmLocation = tmInfo.getTmPath();
            if (StringUtils.isNotEmpty(tmLocation))
            {
                LoginResponse loginResponse = tmServerApi.login(tmServerInfo, tmLocation);
                if (loginResponse != null && StringUtils.isNotEmpty(loginResponse.getSecurityTicket()))
                {
                    securityTicket = loginResponse.getSecurityTicket();
                    break;
                }
            }
        }
        return securityTicket;
    }

    protected String getSessionId()
    {
        if (sessionId == null || sessionId.length() == 0)
        {
            return RequestContextHolder.currentRequestAttributes().getSessionId();
        }
        else
        {
            return sessionId;
        }
    }

    protected SessionInformation getSessionInformation(String sessionId)
    {
        return sessionInfoCache.get(sessionId);
    }

    protected Set<String> getTmGroupShortCodes()
    {
        Set<String> set = new HashSet<String>();
        if (tmGroupShortCodes.length() > 0)
        {
            String[] tmp = tmGroupShortCodes.split(",");
            for (String str : tmp)
            {
                set.add(str.trim());
            }
        }
        return set;
    }

    protected TmListResponse getTmList(Map<String, Object> tmsCredential, TmServerInfo tmServerInfo) throws IOException
    {
        TmListResponse response;
        String url = tmsCredential.containsKey("url") ? (String) tmsCredential.get("url") : _url;
        url = url + "-TmList";

        response = getTmList(url);
        if (isRefreshTmList(tmsCredential) || response == null || response.getTmList().isEmpty())
        {
            response = tmServerApi.fetchTmList(tmServerInfo);
        }
        storeTmList(url, response);
        return response;
    }

    protected TmListResponse getTmList(String key)
    {
        return tmInfoListCache.get(key);
    }

    protected boolean isRefreshTmList(Map<String, Object> tmsCredential)
    {
        return tmsCredential.containsKey("refreshTmList") && (Boolean) tmsCredential.get("refreshTmList");
    }

    protected void removeSessionInformation(String sessionId)
    {
        sessionInfoCache.remove(sessionId);
    }

    protected Set<String> getSelectedTmGroupSet(List<DropdownOption> selectedTmGroup)
    {
        Set<String> selectedTmGroupSet = new LinkedHashSet<String>();
        for (DropdownOption option : selectedTmGroup)
        {
            selectedTmGroupSet.add(option.getShortcode());
        }
        return selectedTmGroupSet;
    }

    protected Set<String> getSelectedTmSet(List<DropdownOption> selectedTms)
    {
        Set<String> selectedTmSet = new LinkedHashSet<String>();
        for (DropdownOption option : selectedTms)
        {
            selectedTmSet.add(option.getValue());
        }
        return selectedTmSet;
    }
}
