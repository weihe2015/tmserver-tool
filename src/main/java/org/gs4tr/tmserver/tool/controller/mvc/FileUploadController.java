package org.gs4tr.tmserver.tool.controller.mvc;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.controller.TmServerToolController;
import org.gs4tr.tmserver.tool.controller.model.ErrorResponse;
import org.gs4tr.tmserver.tool.controller.model.FileUploadOption;
import org.gs4tr.tmserver.tool.controller.model.UploadFileResponse;
import org.gs4tr.tmserver.tool.model.TmLocale;
import org.gs4tr.tmserver.tool.model.swagger.JsonErrorMessage;
import org.gs4tr.tmserver.tool.service.file.Constants;
import org.gs4tr.tmserver.tool.service.file.FolderUtils;
import org.gs4tr.tmserver.tool.service.file.ZipFileUtils;
import org.gs4tr.tmserver.tool.service.tmx.TmxLocale;
import org.gs4tr.tmserver.tool.service.tmx.WfTxtLocale;
import org.gs4tr.tmserver.tool.service.tmx.XmlPreDefinedEntityFixer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "File Upload REST Endpoint")
public class FileUploadController extends TmServerToolController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);
    private static final String SYSTEM_DIR = System.getProperty("user.dir");

    @Autowired
    private ZipFileUtils zipFileUtils;

    @Autowired
    private FolderUtils folderUtils;

    /**
     * This function mainly reads source and target locale from TMX file, and create a language
     * folder as parent folder, which contains this TMX file.
     *
     * @param String original tmx file path or Wf Txt file path
     * @param String uploaded foldername with randomToken
     * @param Map<TmLocale, String> the mapping from Locale to Tm Path
     * @return true if it moved successfully
     * @return false if it has error
     * @throws IOException, SAXException, DocumentException, JSONException
     */
    private boolean moveTmFileToFolderWithLocale(String tmFilePath, String folderName,
        Map<TmLocale, String> localeTmPathMapping) throws IOException, SAXException, DocumentException
    {
        UploadFileResponse uploadFileResponse = new UploadFileResponse();
        File file = new File(tmFilePath);
        String sourceLocaleCode = "";
        String targetLocaleCode = "";

        // accept either ".tmx" or ".TMX" files
        if (tmFilePath.endsWith(Constants.TMXEXTENSION) || tmFilePath.endsWith(Constants.TMX_UPPER_EXTENSION))
        {
            // Remove Invalid XML chars here:
            File tempFile = File.createTempFile("temptmx", ".tmp");
            XmlPreDefinedEntityFixer fixer = new XmlPreDefinedEntityFixer(file, tempFile);
            fixer.clean();
            FileUtils.copyFile(tempFile, file);
            FileUtils.deleteQuietly(tempFile);
            TmxLocale tmxFileLocale = new TmxLocale(tmFilePath);
            // There is no TU in the TMX file, skip it.
            if (tmxFileLocale.getSourceLocale() == null || tmxFileLocale.getTargetLocale() == null)
            {
                uploadFileResponse.setErrorMessage(Messages.getString("UploadTmxController.0"));
                return false;
            }
            sourceLocaleCode = tmxFileLocale.getSourceLocaleInLowerCase();
            targetLocaleCode = tmxFileLocale.getTargetLocaleInLowerCase();
        }
        // accept either ".txt" or ".TXT" files
        else if (tmFilePath.endsWith(Constants.TXTEXTENSION) || tmFilePath.endsWith(Constants.TXT_UPPER_EXTENSION))
        {
            WfTxtLocale wfTxtFileLocale = new WfTxtLocale(tmFilePath);
            if (wfTxtFileLocale.getSourceLocale() == null || wfTxtFileLocale.getTargetLocale() == null)
            {
                uploadFileResponse.setErrorMessage(Messages.getString("UploadTmxController.0"));
                return false;
            }
            sourceLocaleCode = wfTxtFileLocale.getSourceLocaleInLowerCase();
            targetLocaleCode = wfTxtFileLocale.getTargetLocaleInLowerCase();
        }
        else
        {
            // It should not reach here:
            return false;
        }

        String tmPath = "";
        for (TmLocale locale : localeTmPathMapping.keySet())
        {
            if (StringUtils.equalsIgnoreCase(locale.getSourceLocale(), sourceLocaleCode) &&
                StringUtils.equalsIgnoreCase(locale.getTargetLocale(), targetLocaleCode))
            {
                tmPath = localeTmPathMapping.get(locale);
                break;
            }
        }

        if (StringUtils.isEmpty(tmPath))
        {
            String errorMessage = String.format(Messages.getString("ImportTmController.7"),
                FilenameUtils.getName(tmFilePath), sourceLocaleCode, targetLocaleCode);
            LOGGER.warn(errorMessage);

            FileUtils.deleteQuietly(file);
            return false;
        }

        String folderNameWithLocale = folderUtils.getFolderWithLocales(folderName, sourceLocaleCode,
            targetLocaleCode);

        if (!new File(folderNameWithLocale).exists())
        {
            new File(folderNameWithLocale).mkdirs();
        }
        String newTmxFilePath = folderUtils.getNewTmxFilePathWithLocales(folderNameWithLocale, tmPath, file.getName());
        FileUtils.copyFile(file, new File(newTmxFilePath));
        FileUtils.deleteQuietly(file);
        return true;
    }

    @ApiOperation(value = "REST API of uploading files for import TM and manipulate TMX files")
    @ApiResponses({
        @ApiResponse(code = 201, message = "File is uploaded successfully", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @PostMapping("/api/v1/tm/tmx/upload")
    public ResponseEntity<?> uploadFiles(@ModelAttribute final FileUploadOption fileUploadOption)
    {
        String date = new SimpleDateFormat(Constants.DATEFORMATE).format(new Date());
        String type = fileUploadOption.getType();
        String randomToken = fileUploadOption.getRandomToken();
        MultipartFile uploadFile = fileUploadOption.getUploadedFiles()[0];

        if (StringUtils.equalsIgnoreCase(Constants.IMPORT, type))
        {
            String folderName = folderUtils.getUploadFolderPath(SYSTEM_DIR, date, randomToken);

            File tmpUploadDir = new File(folderName);
            if (!tmpUploadDir.exists())
            {
                tmpUploadDir.mkdirs();
            }
            return uploadImportTmxFiles(uploadFile, fileUploadOption.getSelectedTmRows(), folderName);
        }
        else if (StringUtils.equalsIgnoreCase(Constants.MANIPULATE, type))
        {
            String folderName = folderUtils.getSourceManipulateDir(SYSTEM_DIR, date, randomToken);

            File tmpUploadDir = new File(folderName);
            if (!tmpUploadDir.exists())
            {
                tmpUploadDir.mkdirs();
            }

            return uploadManipulateTmxFiles(uploadFile, folderName);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(StringUtils.EMPTY);
    }

    private ResponseEntity<?> uploadImportTmxFiles(MultipartFile uploadFile, JSONArray selectedTmRows, String folderName)
    {
        // Conflict randomToken should be handled in UI side, not in the server side
        // Because there may be case that multiple upload promises are called, with the
        // same token.
        Map<TmLocale, String> localeTmPathMapping = new LinkedHashMap<>();
        for (int i = 0; i < selectedTmRows.length(); ++i)
        {
            JSONObject subObj = selectedTmRows.getJSONObject(i);
            String sourceLocaleCode = subObj.getString("sourceLocaleCode");
            String targetLocaleCode = subObj.getString("targetLocaleCode");
            // TM Path can have underscore, so don't use underscore as temp character.
            String tmPath = subObj.getString("tmPath").replace(Constants.SLASH, Constants.DOLLAR);
            TmLocale tmLocale = new TmLocale(sourceLocaleCode, targetLocaleCode);
            localeTmPathMapping.put(tmLocale, tmPath);
        }

        String filename = "";
        try
        {
            // Avoid spaces in filename:
            String originalFileName = uploadFile.getOriginalFilename().replaceAll(Constants.SPACE, Constants.UNDERSCORE);
            filename = folderName + originalFileName;
            File file = new File(filename);
            uploadFile.transferTo(file);

            // unzip the file if uploaded file is zip
            if (file.getName().endsWith(Constants.ZIPEXTENSION) ||
                file.getName().endsWith(Constants.ZIP_UPPER_EXTENSION))
            {
                String zipOutputFolder = FilenameUtils.removeExtension(filename);
                zipFileUtils.unzipFiles(file, zipOutputFolder);

                List<String> tmxFileList = new ArrayList<>();
                folderUtils.getAllTmFiles(zipOutputFolder, tmxFileList);

                // For each tmx file: get source and target Locale from tmx file.
                List<UploadFileResponse> uploadFileResponses = new ArrayList<>();
                for (String tmFile : tmxFileList)
                {
                    JSONObject subJSONObject = new JSONObject();
                    boolean subStatus = false;
                    try
                    {
                        subStatus = moveTmFileToFolderWithLocale(tmFile, folderName, localeTmPathMapping);
                    }
                    catch (SAXException | DocumentException ex)
                    {
                        subJSONObject.put("errorMessage", ex.getLocalizedMessage());
                        LOGGER.error(ex.getMessage(), ex);
                    }
                    UploadFileResponse uploadFileResponse = new UploadFileResponse();
                    uploadFileResponse.setStatus(subStatus);
                    uploadFileResponse.setFilename(FilenameUtils.getName(tmFile));
                    uploadFileResponse.setOriginalZipFileName(FilenameUtils.getName(filename));
                    if (!subStatus && uploadFileResponse.getErrorMessage() == null)
                    {
                        uploadFileResponse.setErrorMessage(Messages.getString("ImportTmController.8"));
                    }
                    uploadFileResponses.add(uploadFileResponse);
                }
                FileUtils.deleteDirectory(new File(zipOutputFolder));
                return ResponseEntity.ok(uploadFileResponses);
            }
            // get source and target Locale from tmx file.
            else
            {
                UploadFileResponse uploadFileResponse = new UploadFileResponse();
                boolean status = false;
                try
                {
                    status = moveTmFileToFolderWithLocale(filename, folderName, localeTmPathMapping);
                }
                catch (SAXException | DocumentException ex)
                {
                    uploadFileResponse.setErrorMessage(ex.getMessage());
                    LOGGER.error(ex.getMessage(), ex);
                }
                uploadFileResponse.setStatus(status);
                uploadFileResponse.setFilename(FilenameUtils.getName(filename));
                if (!status && uploadFileResponse.getErrorMessage() == null)
                {
                    uploadFileResponse.setErrorMessage(Messages.getString("ImportTmController.8"));
                }
                return ResponseEntity.ok(uploadFileResponse);
            }
        }
        catch (IOException ex)
        {
            UploadFileResponse uploadFileResponse = new UploadFileResponse();
            uploadFileResponse.setStatus(false);
            uploadFileResponse.setFilename(FilenameUtils.getName(filename));
            uploadFileResponse.setGlobal(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(uploadFileResponse);
        }
    }

    public ResponseEntity<?> uploadManipulateTmxFiles(MultipartFile uploadFile, String folderName)
    {
        try
        {
            String newFileName = folderName + uploadFile.getOriginalFilename();
            File file = new File(newFileName);
            uploadFile.transferTo(file);

            // unzip the file if uploaded file is zip
            if (newFileName.endsWith(Constants.ZIPEXTENSION) || newFileName.endsWith(Constants.ZIP_UPPER_EXTENSION))
            {
                String zipOutputFolder = folderName + uploadFile.getOriginalFilename();
                zipOutputFolder = FilenameUtils.removeExtension(zipOutputFolder);
                zipFileUtils.unzipFiles(file, zipOutputFolder);

                List<String> tmxFileList = new ArrayList<String>();
                folderUtils.getAllTmxFiles(zipOutputFolder, tmxFileList);

                List<UploadFileResponse> uploadFileResponses = new ArrayList<>();
                for (String tmxFile : tmxFileList)
                {
                    UploadFileResponse uploadFileResponse = new UploadFileResponse();
                    uploadFileResponse.setStatus(true);
                    uploadFileResponse.setFilename(FilenameUtils.getName(tmxFile));
                    uploadFileResponse.setOriginalZipFileName(FilenameUtils.getName(FilenameUtils.getName(newFileName)));
                    uploadFileResponses.add(uploadFileResponse);
                }
                return ResponseEntity.ok(uploadFileResponses);
            }
            else
            {
                UploadFileResponse uploadFileResponse = new UploadFileResponse();
                uploadFileResponse.setStatus(true);
                uploadFileResponse.setFilename(FilenameUtils.getName(newFileName));
                return ResponseEntity.ok(uploadFileResponse);
            }
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("TmxManipulateController.3"), ex.getMessage());
            LOGGER.error(errorMessage);

            ErrorResponse errorResponse = createErrorResponse(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }
}
