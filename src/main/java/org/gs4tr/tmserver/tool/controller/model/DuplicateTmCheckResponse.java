package org.gs4tr.tmserver.tool.controller.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class DuplicateTmCheckResponse
{
    private boolean success;
    private String errorMessage;
    private List<TmLanguagePair> duplicateTmLanguagePairs;

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public List<TmLanguagePair> getDuplicateTmLanguagePairs()
    {
        return duplicateTmLanguagePairs;
    }

    public void setDuplicateTmLanguagePairs(List<TmLanguagePair> duplicateTmLanguagePairs)
    {
        this.duplicateTmLanguagePairs = duplicateTmLanguagePairs;
    }

    public void addDuplicateTmLanugagePair(TmLanguagePair tmLanguagePair)
    {
        if (duplicateTmLanguagePairs == null)
        {
            duplicateTmLanguagePairs = new ArrayList<>();
        }
        duplicateTmLanguagePairs.add(tmLanguagePair);
    }
}
