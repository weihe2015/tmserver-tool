package org.gs4tr.tmserver.tool.controller.model;

import java.util.List;

public class CreatePdCsvOption
{
    private String cleanupUsername;
    private String cleanupPassword;
    private String paClientName;
    private boolean addFileName;
    private boolean useGenericUsernameAsProfileName;
    private boolean emptyCleanupUserInfo;
    private List<DropdownOption> selectedTmGroup;

    public List<DropdownOption> getSelectedTmGroup()
    {
        return selectedTmGroup;
    }

    public void setSelectedTmGroup(List<DropdownOption> selectedTmGroup)
    {
        this.selectedTmGroup = selectedTmGroup;
    }

    public String getCleanupUsername()
    {
        return cleanupUsername;
    }

    public void setCleanupUsername(String cleanupUsername)
    {
        this.cleanupUsername = cleanupUsername;
    }

    public String getPaClientName()
    {
        return paClientName;
    }

    public void setPaClientName(String paClientName)
    {
        this.paClientName = paClientName;
    }

    public String getCleanupPassword()
    {
        return cleanupPassword;
    }

    public void setCleanupPassword(String cleanupPassword)
    {
        this.cleanupPassword = cleanupPassword;
    }

    public boolean isAddFileName()
    {
        return addFileName;
    }

    public void setAddFileName(boolean addFileName)
    {
        this.addFileName = addFileName;
    }

    public boolean isUseGenericUsernameAsProfileName()
    {
        return useGenericUsernameAsProfileName;
    }

    public void setUseGenericUsernameAsProfileName(boolean useGenericUsernameAsProfileName)
    {
        this.useGenericUsernameAsProfileName = useGenericUsernameAsProfileName;
    }

    public boolean isEmptyCleanupUserInfo()
    {
        return emptyCleanupUserInfo;
    }

    public void setEmptyCleanupUserInfo(boolean emptyCleanupUserInfo)
    {
        this.emptyCleanupUserInfo = emptyCleanupUserInfo;
    }
}
