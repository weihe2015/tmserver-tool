package org.gs4tr.tmserver.tool.controller.mvc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.LoginResponse;
import org.gs4tr.tmserver.tool.api.model.TmInfo;
import org.gs4tr.tmserver.tool.api.model.TmListResponse;
import org.gs4tr.tmserver.tool.controller.TmServerToolController;
import org.gs4tr.tmserver.tool.controller.model.GenericResponse;
import org.gs4tr.tmserver.tool.controller.model.MixedTmInfo;
import org.gs4tr.tmserver.tool.controller.model.TmServerVersionResponse;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.model.swagger.GenericTmInfo;
import org.gs4tr.tmserver.tool.model.swagger.JsonErrorMessage;
import org.gs4tr.tmserver.tool.repository.TmRenameJDBCRepository;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "REST Endpoint of TM List")
public class TmListController extends TmServerToolController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TmListController.class);

    @Value("${concordance.tmGroupShortCode}")
    private String tmGroupShortCodes;

    @Autowired
    private TmServerApi tmServerApi;

    @Autowired
    private TmRenameJDBCRepository tmRenameJDBCRepository;

    @Autowired
    private ObjectMapper objectMapper;

    protected List<MixedTmInfo> constructTmListResult(String type, TmListResponse tmListResponse)
    {
        List<MixedTmInfo> list = new ArrayList<>();
        // import Tm List:
        if (StringUtils.equalsIgnoreCase("import", type))
        {
            for (TmInfo tmInfo : tmListResponse.getTmList())
            {
                MixedTmInfo importTmInfo = new MixedTmInfo();
                importTmInfo.initImportTmInfo(tmInfo);
                list.add(importTmInfo);
            }
        }
        // Export Tm List:
        else if (StringUtils.equalsIgnoreCase("export", type))
        {
            for (TmInfo tmInfo : tmListResponse.getTmList())
            {
                MixedTmInfo exportTmInfo = new MixedTmInfo();
                exportTmInfo.initExportTmInfo(tmInfo);
                list.add(exportTmInfo);
            }
        }
        else if (StringUtils.equalsIgnoreCase("concordance", type))
        {
            for (TmInfo tmInfo : tmListResponse.getTmList())
            {
                MixedTmInfo concordanceSearchTmInfo = new MixedTmInfo();
                concordanceSearchTmInfo.initConcordanceTmInfo(tmInfo);
                list.add(concordanceSearchTmInfo);
            }
        }
        return list;
    }

    public ResponseEntity<?> getImportExportConcordanceTmList(Map<String, Object> tmsCredential, String type)
    {
        try
        {
            TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
            TmListResponse response;
            if (tmsCredential.containsKey("limited"))
            {
                response = tmServerApi.fetchTmListWithTmGroupCodes(tmServerInfo, getTmGroupShortCodes());
            }
            else
            {
                response = getTmList(tmsCredential, tmServerInfo);
            }

            if (response == null || response.getTmList().isEmpty())
            {
                String errorMessage = String.format(Messages.getString("TmListController.0"), type);
                LOGGER.warn(errorMessage);

                GenericResponse genericResponse = createErrorMessage(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(genericResponse);
            }

            List<MixedTmInfo> list = constructTmListResult(type, response);
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(list));
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("TmListController.1"), type, ex.getMessage());
            LOGGER.error(errorMessage);

            GenericResponse genericResponse = createErrorMessage(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(genericResponse);
        }
    }

    private ResponseEntity<String> getRenameTmList() throws IOException
    {
        List<MixedTmInfo> tmInfos = tmRenameJDBCRepository.getAllTmList();
        return ResponseEntity.ok(objectMapper.writeValueAsString(tmInfos));
    }

    @ApiOperation(value = "REST API of getting available TM List from TM Server")
    @ApiResponses({
        @ApiResponse(code = 200, message = "TM Info List is succesfully retrieved.", response = GenericTmInfo.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping(path = "/api/v1/tm/tmlist", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getTmListByType(@RequestHeader(value = "Authorization") String authToken,
        @ApiParam(value = "tmListType", required = true) @RequestParam(value = "type", required = true) String type,
        @ApiParam(value = "isRefreshTmList", required = false) @RequestParam(value = "isRefreshTmList", required = false) boolean refreshTmList)
    {
        try
        {
            Map<String, Object> tmsCredential = decodeHeaderToken(authToken);

            if (refreshTmList)
            {
                tmsCredential.put("refreshTmList", refreshTmList);
            }

            if (StringUtils.equalsIgnoreCase("rename", type))
            {
                return getRenameTmList();
            }
            else
            {
                return getImportExportConcordanceTmList(tmsCredential, type);
            }
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("TmListController.1"), type, ex.getMessage());
            LOGGER.error(errorMessage);
        }
        return ResponseEntity.ok(new JSONObject().toString());
    }

    @ApiOperation(value = "REST API of getting current TM Server version")
    @ApiResponses({
        @ApiResponse(code = 200, message = "TM Server version is succesfully retrieved.", response = String.class),
        @ApiResponse(code = 400, message = "Bad request", response = JsonErrorMessage.class),
        @ApiResponse(code = 401, message = "Unauthorized request", response = JsonErrorMessage.class),
        @ApiResponse(code = 403, message = "Forbidden request", response = JsonErrorMessage.class),
        @ApiResponse(code = 404, message = "Page Not found", response = JsonErrorMessage.class) })
    @GetMapping(path = "/api/v1/tms/version")
    public ResponseEntity<?> getTmServerVersion(@RequestHeader(value = "Authorization") String authToken)
    {
        try
        {
            Map<String, Object> tmsCredential = decodeHeaderToken(authToken);
            TmServerInfo tmServerInfo = createTmServerInfo(tmsCredential);
            TmListResponse tmListResponse;
            if (tmsCredential.containsKey("limited"))
            {
                tmListResponse = tmServerApi.fetchTmListWithTmGroupCodes(tmServerInfo, getTmGroupShortCodes());
            }
            else
            {
                tmListResponse = getTmList(tmsCredential, tmServerInfo);
            }
            if (tmListResponse == null || tmListResponse.getTmList().isEmpty())
            {
                String errorMessage = String.format(Messages.getString("TmListController.4"));
                LOGGER.error(errorMessage);

                GenericResponse genericResponse = createErrorMessage(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(genericResponse);
            }
            else
            {
                String tmPath = tmListResponse.getTmList().get(0).getTmPath();
                LoginResponse loginResponse = tmServerApi.getTmServerVersion(tmServerInfo, tmPath);
                if (!loginResponse.isSuccess())
                {
                    String loginResponseText = objectMapper.writeValueAsString(loginResponse);
                    String errorMessage = String.format(Messages.getString("TmListController.5"), loginResponseText);
                    GenericResponse genericResponse = createErrorMessage(errorMessage);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(genericResponse);
                }
                TmServerVersionResponse tmServerVersionResponse = new TmServerVersionResponse();
                tmServerVersionResponse.setVersion(loginResponse.getVersion());
                return ResponseEntity.ok(tmServerVersionResponse);
            }
        }
        catch (IOException ex)
        {
            String errorMessage = String.format(Messages.getString("TmListController.2"), ex.getMessage());
            LOGGER.error(errorMessage);
            GenericResponse genericResponse = createErrorMessage(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(genericResponse);
        }
    }
}
