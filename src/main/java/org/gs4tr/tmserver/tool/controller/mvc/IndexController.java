package org.gs4tr.tmserver.tool.controller.mvc;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class IndexController implements ErrorController
{
    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public ModelAndView error()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        return modelAndView;
    }

    @Override
    public String getErrorPath()
    {
        return PATH;
    }

    @RequestMapping(value = { "/500" }, method = RequestMethod.GET)
    public ModelAndView InternalServerError()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        return modelAndView;
    }

    @RequestMapping(value = { "/405" }, method = RequestMethod.GET)
    public ModelAndView methodIsNotAllow()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("404");
        return modelAndView;
    }

    @RequestMapping(value = { "/404" }, method = RequestMethod.GET)
    public ModelAndView pageNotFound()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("404");
        return modelAndView;
    }

    @RequestMapping(value = { "/401" }, method = RequestMethod.GET)
    public ModelAndView pageUnauthorized()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("401");
        return modelAndView;
    }

    @RequestMapping(value = { "/concordance", "/createCsv", "/createTm", "/createTmWithCSV", "/importTm",
        "/exportTm", "/renameTm", "/manipulateTmx", "/error"}, method = RequestMethod.GET)
    public ModelAndView getActionViews()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping(value = { "/", "/login", "/SSO"}, method = RequestMethod.GET)
    public ModelAndView index()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping(value = { "/silent-refresh" }, method = RequestMethod.GET)
    public ModelAndView oauthSilentRefresh()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("silent-refresh");
        return modelAndView;
    }
}
