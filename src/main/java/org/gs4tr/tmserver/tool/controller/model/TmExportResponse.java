package org.gs4tr.tmserver.tool.controller.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class TmExportResponse
{
    private boolean success;
    private String message;
    private String jobId;
    private String status;
    private boolean exportToFtp;

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public String getMessage()
    {
        return message;
    }

    public String getJobId()
    {
        return jobId;
    }

    public void setJobId(String jobId)
    {
        this.jobId = jobId;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public boolean isExportToFtp()
    {
        return exportToFtp;
    }

    public void setExportToFtp(boolean exportToFtp)
    {
        this.exportToFtp = exportToFtp;
    }
}
