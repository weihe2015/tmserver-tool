package org.gs4tr.tmserver.tool.model;

import org.gs4tr.foundation.locale.Locale;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TmLocale
{
    @JsonProperty("Source")
    private String _sourceLocale;
    @JsonProperty("Target")
    private String _targetLocale;
    @JsonProperty("Source Language")
    private String _sourceLocaleDisplayName;
    @JsonProperty("Target Language")
    private String _targetLocaleDisplayName;

    public TmLocale()
    {
    }

    public TmLocale(String sourceLocale, String targetLocale)
    {
        this._sourceLocale = sourceLocale;
        this._targetLocale = targetLocale;

        Locale srcLocale = Locale.makeLocale(this._sourceLocale);
        this._sourceLocaleDisplayName = srcLocale.getDisplayName();

        Locale tgtLocale = Locale.makeLocale(this._targetLocale);
        this._targetLocaleDisplayName = tgtLocale.getDisplayName();
    }

    public String getSourceLocale()
    {
        return this._sourceLocale;
    }

    public String getSourceLocaleDisplayName()
    {
        return _sourceLocaleDisplayName;
    }

    public String getSourceLocaleInLowerCase()
    {
        return this._sourceLocale.toLowerCase();
    }

    public String getTargetLocale()
    {
        return this._targetLocale;
    }

    public String getTargetLocaleDisplayName()
    {
        return _targetLocaleDisplayName;
    }

    public String getTargetLocaleInLowerCase()
    {
        return this._targetLocale.toLowerCase();
    }

    public void setSourceLocale(String sourceLocale)
    {
        this._sourceLocale = sourceLocale;
    }

    public void setTargetLocale(String targetLocale)
    {
        this._targetLocale = targetLocale;
    }
}
