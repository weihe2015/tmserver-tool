package org.gs4tr.tmserver.tool.model.swagger;

public class JsonErrorMessage
{
    public String global;

    public JsonErrorMessage(String global)
    {
        this.global = global;
    }

    public String getGlobal()
    {
        return global;
    }

    public void setGlobal(String global)
    {
        this.global = global;
    }
}
