package org.gs4tr.tmserver.tool.model.export;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.gs4tr.tmserver.tool.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TmFtpAdaptor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TmFtpAdaptor.class);

    static private final String CONNECT = "connect";
    static private final String LOGIN = "login";
    static private final String LOGOUT = "logout";

    private final String _ftpUrl;
    private final String _ftpUsername;
    private final String _ftpPassword;
    private final int _ftpPort = 21;

    @Autowired
    public TmFtpAdaptor(@Value("${ftp.url}") String ftpUrl, @Value("${ftp.username}") String ftpUsername,
        @Value("${ftp.password}") String ftpPassword)
    {
        this._ftpUrl = ftpUrl;
        this._ftpUsername = ftpUsername;
        this._ftpPassword = ftpPassword;
    }

    private boolean checkDirecotryExists(FTPClient ftpClient, String dirPath) throws IOException
    {
        ftpClient.changeWorkingDirectory(dirPath);
        int returnCode = ftpClient.getReplyCode();
        if (returnCode == 550)
        {
            return false;
        }
        return true;
    }

    public void exportFileToFtp(InputStream inputStream, String remoteFileName, String folderName)
        throws SocketException, IOException
    {
        FTPClient ftpClient = new FTPClient();
        ftpConnect(ftpClient);
        ftpLogin(ftpClient);

        if (!ftpClient.isConnected())
        {
            LOGGER.error(String.format(Messages.getString("FTPClient.3"), remoteFileName));
        }

        else
        {
            boolean answer = ftpClient.sendNoOp();
            if (!answer)
            {
                LOGGER.error(Messages.getString("FTPClient.4"));
            }
            else
            {
                // Define FTP setting
                ftpClient.enterLocalPassiveMode();
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

                // Make a new directory if there is not an existing one.
                if (!checkDirecotryExists(ftpClient, folderName))
                {
                    ftpClient.makeDirectory(folderName);
                }
                // Change working directory to this folder
                ftpClient.changeWorkingDirectory(folderName);

                // Upload files to FTP:
                boolean done = ftpClient.storeFile(remoteFileName, inputStream);
                inputStream.close();
                if (done)
                {
                    LOGGER.info(String.format(Messages.getString("FTPClient.0"), remoteFileName));
                }
                else
                {
                    LOGGER.info(String.format(Messages.getString("FTPClient.1"), remoteFileName));
                }
            }
        }

        ftpLogout(ftpClient);
        ftpDisconnect(ftpClient);
    }

    private void ftpConnect(FTPClient ftpClient) throws SocketException, IOException
    {
        ftpClient.connect(this._ftpUrl, this._ftpPort);
        logServerReply(ftpClient, CONNECT);
    }

    private void ftpDisconnect(FTPClient ftpClient) throws IOException
    {
        ftpClient.disconnect();
    }

    private void ftpLogin(FTPClient ftpClient) throws IOException
    {
        ftpClient.login(this._ftpUsername, this._ftpPassword);
        logServerReply(ftpClient, LOGIN);
    }

    private void ftpLogout(FTPClient ftpClient) throws IOException
    {
        ftpClient.logout();
        logServerReply(ftpClient, LOGOUT);
    }

    public String getFtpPassword()
    {
        return this._ftpPassword;
    }

    public String getFtpUrl()
    {
        return this._ftpUrl;
    }

    public String getFtpUsername()
    {
        return this._ftpUsername;
    }

    private void logServerReply(FTPClient ftpClient, String step)
    {
        String[] replies = ftpClient.getReplyStrings();
        if (replies != null && replies.length > 0)
        {
            for (String aReply : replies)
            {
                LOGGER.debug(aReply);
            }
        }
    }
}
