package org.gs4tr.tmserver.tool.model.export;

import java.util.concurrent.Semaphore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("ExportTmLimiter")
public class ExportTmLimiter
{
    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportTmLimiter.class);

    @Value("${ws.exportTm.thread.enabled:true}")
    private boolean enabledExportTmLimiter;

    @Value("${ws.exportTm.thread.limit:8}")
    private int exportTmLimit;

    private final Semaphore _exportPermitBucket;

    public ExportTmLimiter()
    {
        if (exportTmLimit <= 0)
        {
            exportTmLimit = 8;
        }
        this._exportPermitBucket = new Semaphore(exportTmLimit);
    }

    public boolean tryAcquire()
    {
        if (!enabledExportTmLimiter)
        {
            return true;
        }
        return getExportPermitBucket().tryAcquire();
    }

    public void restorePermit()
    {
        if (!enabledExportTmLimiter)
        {
            return;
        }
        getExportPermitBucket().release();
    }

    public Semaphore getExportPermitBucket()
    {
        return _exportPermitBucket;
    }
}
