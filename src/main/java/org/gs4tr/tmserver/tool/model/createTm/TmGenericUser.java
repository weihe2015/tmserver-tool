package org.gs4tr.tmserver.tool.model.createTm;

import org.gs4tr.tmserver.tool.controller.model.DropdownOption;

public class TmGenericUser extends TmGenericUsername
{
    private DropdownOption role;
    private String password;

    public String getPassword()
    {
        return this.password;
    }

    public DropdownOption getRole()
    {
        return this.role;
    }

    public void setRole(DropdownOption role)
    {
        this.role = role;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

}
