package org.gs4tr.tmserver.tool.model.thread;

public class TaskStatus
{
    String content;

    public TaskStatus()
    {
    }

    public TaskStatus(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
}
