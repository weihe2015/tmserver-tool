package org.gs4tr.tmserver.tool.model.xml.xpath;

import org.gs4tr.tmserver.tool.model.xml.Document;
import org.gs4tr.tmserver.tool.model.xml.Element;
import org.gs4tr.tmserver.tool.model.xml.Namespace;
import org.gs4tr.tmserver.tool.model.xml.Node;
import org.jaxen.NamespaceContext;

/**
 * <code>DefaultNamespaceContext</code> implements a Jaxen NamespaceContext such that a context node
 * is used to determine the current XPath namespace prefixes and namespace URIs available.
 */
public class DefaultNamespaceContext implements NamespaceContext
{
    static public DefaultNamespaceContext create(Object node)
    {
        Element element = null;

        if (node instanceof Element)
        {
            element = (Element) node;
        }
        else if (node instanceof Document)
        {
            Document doc = (Document) node;
            element = doc.getRootElement();
        }
        else if (node instanceof Node)
        {
            element = ((Node) node).getParent();
        }

        if (element != null)
        {
            return new DefaultNamespaceContext(element);
        }

        return null;
    }

    private final Element element;

    public DefaultNamespaceContext(Element element)
    {
        this.element = element;
    }

    @Override
    public String translateNamespacePrefixToUri(String prefix)
    {
        if (prefix != null && prefix.length() > 0)
        {
            Namespace ns = element.getNamespaceForPrefix(prefix);

            if (ns != null)
            {
                return ns.getURI();
            }
        }

        return null;
    }
}
