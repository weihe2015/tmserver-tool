package org.gs4tr.tmserver.tool.model.xml;

public class CDATA implements Node
{
    private Element parent;
    private String text;

    public CDATA(String text)
    {
        this.text = text;
    }

    @Override
    public String asXML()
    {
        return "<![CDATA[" + text + "]]>";
    }

    @Override
    public short getNodeType()
    {
        return CDATA_NODE;
    }

    @Override
    public Element getParent()
    {
        return parent;
    }

    public String getText()
    {
        return text;
    }

    @Override
    public void setParent(Element node)
    {
        parent = node;
    }
}
