package org.gs4tr.tmserver.tool.model.xml;

public class XmlDeclaration implements Node
{
    // Need only rewrite the encoding on output, do that with regexps
    // to leave original whitespace and quotes intact.
    private String original;
    private String version;
    private String encoding;
    private Boolean standalone;

    public XmlDeclaration(String original)
    {
        this.original = original;
    }

    @Override
    public String asXML()
    {
        return original;
    }

    public String getEncoding()
    {
        return encoding;
    }

    @Override
    public short getNodeType()
    {
        return XML_DECL_NODE;
    }

    public String getOriginal()
    {
        return original;
    }

    @Override
    public Element getParent()
    {
        throw new RuntimeException("parent not supported");
    }

    public Boolean getStandalone()
    {
        return standalone;
    }

    public String getVersion()
    {
        return version;
    }

    public String getXmlDecl()
    {
        return original;
    }

    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    public void setOriginal(String original)
    {
        this.original = original;
    }

    @Override
    public void setParent(Element node)
    {
        throw new RuntimeException("parent not supported");
    }

    public void setStandalone(Boolean standalone)
    {
        this.standalone = standalone;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }
}
