package org.gs4tr.tmserver.tool.model.xml;

public interface Node
{
    short DOCUMENT_NODE = 1;
    short DOCUMENT_TYPE_NODE = 2;
    short XML_DECL_NODE = 3;
    short ELEMENT_NODE = 4;
    short ATTRIBUTE_NODE = 5;
    short ATTRIBUTE_VALUE_NODE = 6;
    short TEXT_NODE = 7;
    short CDATA_NODE = 8;
    short ENTITY_NODE = 9;
    short PROCESSING_INSTRUCTION_NODE = 10;
    short COMMENT_NODE = 11;
    short NAMESPACE_NODE = 12;

    String asXML();

    short getNodeType();

    Element getParent();

    void setParent(Element node);

}
