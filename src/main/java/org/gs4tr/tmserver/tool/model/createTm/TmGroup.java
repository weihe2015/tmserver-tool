package org.gs4tr.tmserver.tool.model.createTm;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmGroup extends TmGroupOrganization
{
    private String shortcode;

    public String getShortcode()
    {
        return shortcode;
    }

    public void setShortcode(String shortcode)
    {
        this.shortcode = shortcode;
    }
}
