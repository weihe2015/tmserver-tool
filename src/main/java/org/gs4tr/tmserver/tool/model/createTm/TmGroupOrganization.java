package org.gs4tr.tmserver.tool.model.createTm;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmGroupOrganization
{
    private String label;
    private String value;
    private String id;
    private String name;

    public String getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public void moveProperties()
    {
        this.label = this.name;
        this.value = this.id;
        this.name = null;
        this.id = null;
    }

}
