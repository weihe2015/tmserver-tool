package org.gs4tr.tmserver.tool.model.bilingual;

public class TencentTu extends BilingualTu
{
    private String project;
    private String product;
    private String type;
    private String metadata;

    public TencentTu()
    {
    }

    public String getProject()
    {
        return project;
    }

    public void setProject(String project)
    {
        this.project = project;
    }

    public String getProduct()
    {
        return product;
    }

    public void setProduct(String product)
    {
        this.product = product;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getMetadata()
    {
        return metadata;
    }

    public void setMetadata(String key)
    {
        this.metadata = key;
    }
}
