package org.gs4tr.tmserver.tool.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TmBasicInfo extends TmLocale
{
    @JsonProperty("TM Name")
    private String _tmName;
    @JsonProperty("TM Group Name")
    private String _tmGroupName;
    @JsonIgnore
    private Long _lastModificationTime;

    public TmBasicInfo()
    {
    }

    public TmBasicInfo(String tmName, String tmGroupName, String sourceLocale, String targetLocale,
        Long lastModificationTime)
    {
        super(sourceLocale, targetLocale);
        this._tmName = tmName;
        this._tmGroupName = tmGroupName;
        this._lastModificationTime = lastModificationTime;
    }

    public String getDateFromLong(long date)
    {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(date));
    }

    public Long getLastTuModificationTimeStamp()
    {
        return this._lastModificationTime;
    }

    public String getModificationDateAsString()
    {
        return String.valueOf(this._lastModificationTime);
    }

    public String getTmGroupName()
    {
        return this._tmGroupName;
    }

    public String getTmName()
    {
        return this._tmName;
    }

    public void setLastTuModificationTimeStamp(Long lastTuModificationTimeStamp)
    {
        this._lastModificationTime = lastTuModificationTimeStamp;
    }

    public void setTmGroupName(String tmGroupName)
    {
        this._tmGroupName = tmGroupName;
    }

    public void setTmName(String tmName)
    {
        this._tmName = tmName;
    }
}
