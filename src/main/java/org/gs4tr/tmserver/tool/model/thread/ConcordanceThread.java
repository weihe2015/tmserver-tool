package org.gs4tr.tmserver.tool.model.thread;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.TmServerApi;
import org.gs4tr.tmserver.tool.api.model.LoginResponse;
import org.gs4tr.tmserver.tool.api.model.TmInfo;
import org.gs4tr.tmserver.tool.controller.model.TmSearchOption;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.model.concordance.TmSearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcordanceThread implements Runnable
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ConcordanceThread.class);

    private final TmInfo tmInfo;
    private final TmServerApi tmServerApi;
    private final TmServerInfo tmServerInfo;
    private List<TmSearchResult> searchResult;
    private final TmSearchOption option;
    private final boolean isTPAuth;

    public ConcordanceThread(TmInfo tmInfo, TmServerApi tmServerApi, TmServerInfo tmServerInfo,
        List<TmSearchResult> searchResult, TmSearchOption option, boolean isTPAuth)
    {
        this.tmInfo = tmInfo;
        this.tmServerApi = tmServerApi;
        this.tmServerInfo = tmServerInfo;
        this.searchResult = searchResult;
        this.option = option;
        this.isTPAuth = isTPAuth;
    }

    @Override
    public void run()
    {
        String tmPath = tmInfo.getTmPath();
        String securityTicket = null;
        try
        {
            if (tmPath != null)
            {
                String accessToken = null;
                if (!isTPAuth)
                {
                    LoginResponse response = tmServerApi.login(tmServerInfo, tmPath);
                    if (response != null)
                    {
                        securityTicket = response.getSecurityTicket();
                    }
                }
                else
                {
                    accessToken = option.getAccessToken();
                }

                if (StringUtils.isNotEmpty(securityTicket) || isTPAuth)
                {
                    List<TmSearchResult> result = tmServerApi.concordance(tmServerInfo, option, tmInfo,
                        securityTicket, accessToken);

                    if (result != null)
                    {
                        searchResult.addAll(result);
                    }
                }
            }
        }
        catch (IOException ex)
        {
            LOGGER.error(String.format(Messages.getString("ConcordanceSearchController.2"), ex.getMessage()));
        }
        finally
        {
            if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
            {
                tmServerApi.logout(tmServerInfo, securityTicket);
            }
        }
    }

}
