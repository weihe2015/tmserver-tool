package org.gs4tr.tmserver.tool.model.createTm;

import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.foundation.locale.LocaleException;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmLanguage
{
    // Locale Display Name. Ex: English (United States)
    private String text;
    private String locale;
    // Locale: en-US:
    private String value;

    public String getLocale()
    {
        return this.locale;
    }

    public String getValue()
    {
        return this.value;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public void moveProperty()
    {
        try
        {
            this.text = Locale.makeLocale(locale).getDisplayName();
            this.value = locale;
        }
        catch (LocaleException ex)
        {
            this.text = locale;
        }
        this.locale = null;
    }
}
