package org.gs4tr.tmserver.tool.model.xml;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Namespace implements Node
{
    /** No Namespace present. */
    static public final Namespace NO_NAMESPACE = new Namespace("", "");
    /** XML Namespace. */
    static public final Namespace XML_NAMESPACE = new Namespace("xml",
        "http://www.w3.org/XML/1998/namespace");
    /** XMLNS Namespace. */
    static public final Namespace XMLNS_NAMESPACE = new Namespace("xmlns", "http://www.w3.org/2000/xmlns/");

    /**
     * Factory list of namespaces. Keys are <i>URI</i> &amp; <i>prefix</i>. Values are Namespace
     * objects.
     */
    static private final ConcurrentMap<String, ConcurrentMap<String, Namespace>> namespacemap = new ConcurrentHashMap<String, ConcurrentMap<String, Namespace>>(
        512, 0.75f, 64);

    static
    {
        // Pre-populate the map with the constant namespaces that would
        // otherwise fail validation.
        final ConcurrentMap<String, Namespace> nmap = new ConcurrentHashMap<String, Namespace>();
        nmap.put(NO_NAMESPACE.getPrefix(), NO_NAMESPACE);
        namespacemap.put(NO_NAMESPACE.getURI(), nmap);

        final ConcurrentMap<String, Namespace> xmap = new ConcurrentHashMap<String, Namespace>();
        xmap.put(XML_NAMESPACE.getPrefix(), XML_NAMESPACE);
        namespacemap.put(XML_NAMESPACE.getURI(), xmap);

        final ConcurrentMap<String, Namespace> xnsmap = new ConcurrentHashMap<String, Namespace>();
        xnsmap.put(XMLNS_NAMESPACE.getPrefix(), XMLNS_NAMESPACE);
        namespacemap.put(XMLNS_NAMESPACE.getURI(), xnsmap);
    }

    /**
     * Retrieves or creates a <code>Namespace</code> for the supplied URI and makes it usable as a
     * default namespace, as no prefix is supplied. This method is thread-safe.
     *
     * @param uri <code>String</code> URI of new <code>Namespace</code>.
     * @return <code>Namespace</code> - ready to use namespace.
     */
    static public Namespace getNamespace(final String uri)
    {
        return getNamespace("", uri);
    }

    /**
     * Retrieves or creates a <code>Namespace</code> for the supplied <i>prefix</i> and <i>uri</i>.
     * This method is thread-safe.
     *
     * @param prefix <code>String</code> prefix to map to <code>Namespace</code>.
     * @param uri <code>String</code> URI of new <code>Namespace</code>.
     * @return <code>Namespace</code> - ready to use namespace.
     */
    static public Namespace getNamespace(final String prefix, final String uri)
    {
        if (uri == null)
        {
            if ("".equals(prefix))
            {
                return NO_NAMESPACE;
            }
            // We have an attempt for some prefix (not "" or it would have
            // found NO_NAMESPACE) on the null URI.
            throw new RuntimeException("Namespace URIs must be non-null and non-empty Strings");
        }

        // Must have checked for 'null' uri else namespacemap throws NPE.
        ConcurrentMap<String, Namespace> urimap = namespacemap.get(uri);
        if (urimap == null)
        {
            // No Namespaces yet with that URI.
            urimap = new ConcurrentHashMap<String, Namespace>();
            final ConcurrentMap<String, Namespace> xmap = namespacemap.putIfAbsent(uri, urimap);

            if (xmap != null)
            {
                // Some other thread registered this URI. Use it.
                urimap = xmap;
            }
        }

        // OK, we have a container for the URI, let's search on the prefix.
        Namespace result = urimap.get(prefix);
        if (result != null)
        {
            // got one.
            return result;
        }

        // OK, no namespace yet for that uri/prefix.
        // Validate the prefix (the uri is already validated).

        if ("".equals(uri))
        {
            // Attempt for some prefix (not "" or it would have found NO_NAMESPACE)
            // on the "" URI. Note, we have already done this check for 'null' uri above.
            throw new RuntimeException("Namespace URIs must be non-null and non-empty Strings");
        }

        // No namespace found.
        result = new Namespace(prefix, uri);
        final Namespace prev = urimap.putIfAbsent(prefix, result);
        if (prev != null)
        {
            // Some other thread registered this Namespace. Use it.
            result = prev;
        }

        return result;
    }

    /** The prefix mapped to this namespace. */
    private String prefix;

    /** The URI for this namespace. */
    private String uri;

    /**
     * Constructor.
     *
     * @param prefix is the prefix for this namespace.
     * @param uri is the URI for this namespace.
     */
    private Namespace(String prefix, String uri)
    {
        this.prefix = (prefix != null) ? prefix : "";
        this.uri = (uri != null) ? uri : "";
    }

    @Override
    public String asXML()
    {
        // TODO: not sure this needs to print.
        return "[NAMESPACE " + (prefix != null ? prefix + ":" : "") + uri + "]";
    }

    /**
     * Two <code>Namespaces</code> are equal if and only if both their prefixes and URIs are
     * byte-for-byte equals.
     */
    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
        {
            return true;
        }

        if (obj instanceof Namespace)
        {
            Namespace ns = (Namespace) obj;
            return prefix.equals(ns.prefix) && uri.equals(ns.uri);
        }

        return false;
    }

    @Override
    public short getNodeType()
    {
        return NAMESPACE_NODE;
    }

    @Override
    public Element getParent()
    {
        throw new RuntimeException("parent not supported");
    }

    /**
     * @return the prefix for this <code>Namespace</code>.
     */
    public String getPrefix()
    {
        return prefix;
    }

    /**
     * @return the URI for this <code>Namespace</code>.
     */
    public String getURI()
    {
        return uri;
    }

    /**
     * This returns the hash code for the <code>Namespace</code> that conforms to the 'equals()'
     * contract. // *
     * <p>
     * // * If two namespaces have the same URI, they are equal and have the same // * hash code,
     * even if they have different prefixes.
     *
     * @return <code>int</code> - hash code for this <code>Namespace</code>.
     */
    @Override
    public int hashCode()
    {
        int rc = prefix.hashCode() ^ uri.hashCode();
        if (rc == 0)
        {
            return 0xBabe;
        }
        return rc;
    }

    @Override
    public void setParent(Element node)
    {
        throw new RuntimeException("parent not supported");
    }

    @Override
    public String toString()
    {
        if (this == NO_NAMESPACE)
        {
            return "[NO_NAMESPACE]";
        }
        else if (this == XML_NAMESPACE)
        {
            return "[XML_NAMESPACE]";
        }
        else if (this == XMLNS_NAMESPACE)
        {
            return "[XMLNS_NAMESPACE]";
        }
        return "[NAMESPACE " + (prefix != null ? prefix + ":" : "") + uri + "]";
    }
}
