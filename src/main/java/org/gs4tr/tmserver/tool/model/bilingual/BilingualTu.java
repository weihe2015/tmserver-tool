package org.gs4tr.tmserver.tool.model.bilingual;

public class BilingualTu
{
    private String source;
    private String target;

    public BilingualTu()
    {

    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getTarget()
    {
        return target;
    }

    public void setTarget(String target)
    {
        this.target = target;
    }
}
