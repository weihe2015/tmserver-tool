package org.gs4tr.tmserver.tool.model.server;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TM_PROJECT")
public class TmProject
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PROJECT_ID", nullable = false)
    private Long id;

    @Column(name = "CLIENT_IDENTIFIER", nullable = true)
    private String _clientIdentifier;

    @Column(name = "ENABLED", nullable = true)
    private boolean _enabled;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "SHORT_CODE", nullable = false)
    private String shortCode;

    @Column(name = "PROJECT_DESCRIPTION", nullable = true)
    private String _projectDescription;

    @Column(name = "ORGANIZATION_ID", nullable = true)
    private long organizationId;

    public TmProject()
    {

    }

    public String getName()
    {
        return this.name;
    }

    public Long getOrganizationId()
    {
        return this.organizationId;
    }

    public String getShortCode()
    {
        return this.shortCode;
    }

}
