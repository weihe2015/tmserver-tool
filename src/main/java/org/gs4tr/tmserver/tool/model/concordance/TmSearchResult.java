package org.gs4tr.tmserver.tool.model.concordance;

import java.util.List;

import org.gs4tr.tmserver.tool.api.model.Attribute;
import org.gs4tr.tmserver.tool.api.model.Segment;
import org.gs4tr.tmserver.tool.model.TmBasicInfo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmSearchResult extends TmBasicInfo
{
    @JsonProperty("Source")
    private String _source;
    @JsonProperty("Target")
    private String _target;
    @JsonIgnore
    private long _creationDate;
    @JsonProperty("Creation Date")
    private String creationDate;
    @JsonIgnore
    private long _modificationDate;
    @JsonProperty("Modification Date")
    private String modificationDate;
    @JsonProperty("Creation User")
    private String _creationUser;
    @JsonProperty("Modification User")
    private String _modificationUser;
    @JsonProperty("attributes")
    private List<Attribute> _attributes;

    public TmSearchResult(String tmName, String tmGroupName, String sourceLocale,
        String targetLocale, Segment segment)
    {
        super(tmName, tmGroupName, sourceLocale, targetLocale, segment.getModificationDate());
        this._source = segment.getSource();
        this._target = segment.getTarget();
        this._creationUser = segment.getCreationUser();
        this._modificationUser = segment.getModificationUser();
        this._creationDate = segment.getCreationDate();
        this.creationDate = getDateFromLong(_creationDate);
        this._modificationDate = segment.getModificationDate();
        this.modificationDate = getDateFromLong(_modificationDate);
        this._attributes = segment.getAttributes();
    }

    public List<Attribute> getAttributes()
    {
        return this._attributes;
    }

    public String getCreationUser()
    {
        return this._creationUser;
    }

    public String getModificationUser()
    {
        return this._modificationUser;
    }

    public String getSource()
    {
        return this._source;
    }

    public String getTarget()
    {
        return this._target;
    }

    public void setCreationDate(long creationDate)
    {
        this._creationDate = creationDate;
    }

    public void setCreationUser(String creationUser)
    {
        this._creationUser = creationUser;
    }

    public void setModificationUser(String modificationUser)
    {
        this._modificationUser = modificationUser;
    }

    public void setAttributes(List<Attribute> _attributes)
    {
        this._attributes = _attributes;
    }

    public void setModificationDate(long _modificationDate)
    {
        this._modificationDate = _modificationDate;
    }

    public String getCreationDate()
    {
        return creationDate;
    }

    public String getModificationDate()
    {
        return modificationDate;
    }
}
