package org.gs4tr.tmserver.tool.model.xml;

public class Comment implements Node
{
    private Element parent;
    private String original;
    private String comment;

    public Comment(String original, String comment)
    {
        this.original = original;
        this.comment = comment;
    }

    @Override
    public String asXML()
    {
        return original;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public short getNodeType()
    {
        return COMMENT_NODE;
    }

    @Override
    public Element getParent()
    {
        return parent;
    }

    @Override
    public void setParent(Element node)
    {
        parent = node;
    }
}
