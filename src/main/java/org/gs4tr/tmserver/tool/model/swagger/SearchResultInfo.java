package org.gs4tr.tmserver.tool.model.swagger;

import java.util.Date;
import java.util.List;

public class SearchResultInfo
{
    public String tmGroupName;
    public String tmName;
    public String creationUser;
    public String modificationUser;
    public String souceLocaleDisplayName;
    public String targetLocaleDisplayName;
    public Date modificationDate;
    public Date creationDate;
    public String source;
    public String target;
    public List<Attribute> attributes;
}
