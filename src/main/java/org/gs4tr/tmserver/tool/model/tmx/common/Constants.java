package org.gs4tr.tmserver.tool.model.tmx.common;

import org.gs4tr.foundation3.core.utils.VersionManager;

public class Constants
{
    static public final String AP_CREATIONTOOL = "TDC Analysis Package";
    static public final String AP_CREATIONTOOL_VERSION = VersionManager.getJarVersion(
        "org.gs4tr.tm3.tmx.Version");
    public static final String KEEP_NEWER = "tm.importer.keepNewer";
    public static final String MODIFICATION_AS_CREATION = "tm.importer.modificationAsCreation";
}
