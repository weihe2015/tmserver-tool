package org.gs4tr.tmserver.tool.model.importTm;

public enum TmImportTuOption
{
	SKIP, APPEND, DONTOVERWRITE, OVERWRITE, OVERWRITE_WHEN_IDENTICAL_ATTRIBUTES
}
