package org.gs4tr.tmserver.tool.model.createTm;

public class TmGenericUsername
{
    private String username;

    public TmGenericUsername()
    {
    }

    public TmGenericUsername(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return this.username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }
}
