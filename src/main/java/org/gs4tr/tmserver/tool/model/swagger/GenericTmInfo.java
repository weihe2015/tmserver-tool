package org.gs4tr.tmserver.tool.model.swagger;

public class GenericTmInfo
{
    public String sourceLocaleCode;
    public String sourceLocale;
    public String tmGroupname;
    public String tmName;
    public String targetLocale;
    public String targetLocaleCode;
    public String status;
    public String tmPath;
    public long lastTuModificationDate;
    public int totalTus;
    public int numberOfPrivateTus;
    public boolean uploaded;

}
