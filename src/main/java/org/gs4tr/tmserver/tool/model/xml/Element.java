package org.gs4tr.tmserver.tool.model.xml;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.model.xml.xpath.MyDomXPath;
import org.jaxen.JaxenException;
import org.jaxen.XPath;

public class Element implements Node
{
    private Document document;
    private Element parent;
    // Name is pre-parsed, list of space/attr.
    private List<Node> startTagContent = new ArrayList<>();
    private List<Node> content = new ArrayList<>();
    private List<Node> endTagContent = new ArrayList<>();

    // The pre-parsed qname and pre-parsed attributes.
    private QName qname;
    private List<Attribute> attributes = new ArrayList<>();

    // The namespace of this element and the list of namespaces declared in its
    // attributes.
    private Namespace namespace;
    private List<Namespace> declaredNamespaces = new ArrayList<>();

    /**
     * Constructor for an initial QName that can later be overwritten with the real QName with
     * namespaces resolved.
     */
    public Element(QName qname)
    {
        this.qname = qname;
        namespace = qname.getNamespace();
    }

    public void add(Node node)
    {
        content.add(node);
        node.setParent(this);
    }

    public void addAttribute(Attribute attr)
    {
        startTagContent.add(attr);
        attributes.add(attr);
        attr.setParent(this);
    }

    public void addEndTag(Node node)
    {
        endTagContent.add(node);
    }

    public void addStartTag(Node node)
    {
        startTagContent.add(node);
    }

    @Override
    public String asXML()
    {
        StringBuffer result = new StringBuffer();

        for (int i = 0, max = startTagContent.size(); i < max; i++)
        {
            result.append(startTagContent.get(i).asXML());
        }

        for (int i = 0, max = content.size(); i < max; i++)
        {
            result.append(content.get(i).asXML());
        }

        for (int i = 0, max = endTagContent.size(); i < max; i++)
        {
            result.append(endTagContent.get(i).asXML());
        }

        return result.toString();
    }

    public Attribute attribute(QName name)
    {
        for (int i = 0, max = attributes.size(); i < max; i++)
        {
            Attribute attr = attributes.get(i);
            if (attr.getName().equals(name))
            {
                return attr;
            }
        }

        return null;
    }

    public Attribute attribute(String name)
    {
        for (int i = 0, max = attributes.size(); i < max; i++)
        {
            Attribute attr = attributes.get(i);
            if (attr.getName().getName().equals(name))
            {
                return attr;
            }
        }

        return null;
    }

    /**
     * Returns the element's attributes like DOM4J's element.attributes();.
     */
    public List<Attribute> attributes()
    {
        return attributes;
    }

    public String attributeValue(String name)
    {
        for (int i = 0, max = attributes.size(); i < max; i++)
        {
            Attribute attr = attributes.get(i);
            if (attr.getName().getName().equals(name))
            {
                return attr.getValue().getText();
            }
        }

        return null;
    }

    /**
     * Returns the element's content like DOM4J's element.content();. This is as list of Text and
     * Entity nodes, Elements, Comments, PIs.
     */
    public List<Node> content()
    {
        return content;
    }

    /**
     * Returns the element's list of declared namespaces.
     */
    public List<Namespace> declaredNamespaces()
    {
        return declaredNamespaces;
    }

    public Element element(QName qname)
    {
        for (int i = 0, max = content.size(); i < max; i++)
        {
            Node node = content.get(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) node;
                if (element.getName().equals(qname))
                {
                    return element;
                }
            }
        }

        return null;
    }

    public Element element(String name)
    {
        for (int i = 0, max = content.size(); i < max; i++)
        {
            Node node = content.get(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) node;
                if (element.getName().getName().equals(name))
                {
                    return element;
                }
            }
        }

        return null;
    }

    public List<Element> elements()
    {
        ArrayList<Element> result = new ArrayList<>();

        for (int i = 0, max = content.size(); i < max; i++)
        {
            Node node = content.get(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                result.add((Element) node);
            }
        }

        return result;
    }

    public List<Element> elements(QName name)
    {
        ArrayList<Element> result = new ArrayList<>();

        for (int i = 0, max = content.size(); i < max; i++)
        {
            Node node = content.get(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) node;
                if (element.getName().equals(name))
                {
                    result.add(element);
                }
            }
        }

        return result;
    }

    public Document getDocument()
    {
        return document;
    }

    /**
     * Returns the content of the element's start tag. This is as list containing a single Text node
     * that contains the end tag's original.
     */
    public List<Node> getEndTagContent()
    {
        return endTagContent;
    }

    /**
     * Returns the text value of the element <b>without</b> recursing into child elements.
     * <p>
     * TODO: Possibly normalize according to XML 1.0 rules? Handle other embedded html entities?
     * 
     * @see #getTextFull()
     */
    public String getHtmlEmbeddedText()
    {
        StringBuffer result = new StringBuffer();
        boolean found = false;

        for (int i = 0, max = content.size(); i < max; i++)
        {
            Node node = content.get(i);
            switch (node.getNodeType())
            {
            case Node.TEXT_NODE:
                String text = ((Text) node).getText();
                if (found && StringUtils.equals("nbsp;", text.trim()))
                {
                    text = "amp;" + text;
                    found = !found;
                }
                result.append(text);
                break;
            case Node.CDATA_NODE:
                result.append(((CDATA) node).getText());
                break;
            case Node.ENTITY_NODE:
                String entity = StringUtils.defaultString(((Entity) node).getValue());
                if (StringUtils.equals("&", entity))
                {
                    found = !found;
                }
                result.append(entity);
                break;
            }
        }

        return result.toString();
    }

    public QName getName()
    {
        return qname;
    }

    /**
     * Returns the element's namespace.
     */
    public Namespace getNamespace()
    {
        return namespace;
    }

    public Namespace getNamespaceForPrefix(String prefix)
    {
        if (prefix == null)
        {
            prefix = "";
        }

        if (prefix.equals(namespace.getPrefix()))
        {
            return namespace;
        }
        else if (prefix.equals("xml"))
        {
            return Namespace.XML_NAMESPACE;
        }
        else if (prefix.equals("xmlns"))
        {
            return Namespace.XMLNS_NAMESPACE;
        }
        else
        {
            for (int i = 0, max = declaredNamespaces.size(); i < max; i++)
            {
                Namespace ns = declaredNamespaces.get(i);
                if (prefix.equals(ns.getPrefix()))
                {
                    return ns;
                }
            }
        }

        Element parent = getParent();

        if (parent != null)
        {
            Namespace result = parent.getNamespaceForPrefix(prefix);
            if (result != null)
            {
                return result;
            }
        }

        if (prefix.length() == 0)
        {
            return Namespace.NO_NAMESPACE;
        }

        return null;
    }

    @Override
    public short getNodeType()
    {
        return ELEMENT_NODE;
    }

    @Override
    public Element getParent()
    {
        return parent;
    }

    public String getPath()
    {
        Element parent = getParent();

        if (parent == null)
        {
            return "/" + qname.getName();
        }

        return parent.getPath() + "/" + qname.getName();
    }

    /**
     * Returns the content of the element's start tag. This is as list of Text and Attribute nodes.
     */
    public List<Node> getStartTagContent()
    {
        return startTagContent;
    }

    /**
     * Returns the text value of the element <b>without</b> recursing into child elements.
     * <p>
     * TODO: Possibly normalize according to XML 1.0 rules?
     * 
     * @see #getTextFull()
     */
    public String getText()
    {
        StringBuffer result = new StringBuffer();

        for (int i = 0, max = content.size(); i < max; i++)
        {
            Node node = content.get(i);
            switch (node.getNodeType())
            {
            case Node.TEXT_NODE:
                result.append(((Text) node).getText());
                break;
            case Node.CDATA_NODE:
                result.append(((CDATA) node).getText());
                break;
            case Node.ENTITY_NODE:
                result.append(StringUtils.defaultString(((Entity) node).getValue()));
                break;
            }
        }

        return result.toString();
    }

    /**
     * Returns the text value of the element <b>with</b> recursing into child elements.
     * <p>
     * TODO: Possibly normalize according to XML 1.0 rules?
     * 
     * @see #getText()
     */
    public String getTextFull()
    {
        StringBuffer result = new StringBuffer();

        for (int i = 0, max = content.size(); i < max; i++)
        {
            Node node = content.get(i);
            switch (node.getNodeType())
            {
            case Node.TEXT_NODE:
                result.append(((Text) node).getText());
                break;
            case Node.CDATA_NODE:
                result.append(((CDATA) node).getText());
                break;
            case Node.ENTITY_NODE:
                result.append(StringUtils.defaultString(((Entity) node).getValue()));
                break;
            case Node.ELEMENT_NODE:
                result.append(((Element) node).getTextFull());
                break;
            }
        }

        return result.toString();
    }

    public boolean hasContent()
    {
        return content.size() > 0;
    }

    @SuppressWarnings("unchecked")
    public List<? extends Node> selectNodes(String xpath)
    {
        try
        {
            XPath myxpath = new MyDomXPath(xpath);
            return myxpath.selectNodes(this);
        }
        catch (JaxenException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public Node selectSingleNode(String xpath)
    {
        try
        {
            XPath myxpath = new MyDomXPath(xpath);
            return (Node) myxpath.selectSingleNode(this);
        }
        catch (JaxenException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public void setContent(List<Node> content)
    {
        this.content = content;
    }

    /**
     * Returns the element's list of declared namespaces.
     */
    public void setDeclaredNamespaces(List<Namespace> list)
    {
        declaredNamespaces.clear();
        declaredNamespaces.addAll(list);
    }

    public void setDocument(Document doc)
    {
        document = doc;
    }

    /**
     * Sets the element's namespace.
     */
    public void setNamespace(Namespace ns)
    {
        namespace = ns;
    }

    @Override
    public void setParent(Element node)
    {
        parent = node;
    }

    public void setQName(QName qname)
    {
        this.qname = qname;
        namespace = qname.getNamespace();
    }

    @Override
    public String toString()
    {
        return "<" + qname.getName() + ">";
    }
}
