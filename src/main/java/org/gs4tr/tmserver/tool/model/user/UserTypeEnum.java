package org.gs4tr.tmserver.tool.model.user;

public enum UserTypeEnum
{
    ORGANIZATION, VENDOR, POWER_USER, CLIENT_USER, ADMIN;
}
