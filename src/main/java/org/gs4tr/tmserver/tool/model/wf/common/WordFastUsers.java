package org.gs4tr.tmserver.tool.model.wf.common;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

public class WordFastUsers
{
    static private final String _sHeaderName = "User ID";

    private String _sHeaderString;
    private String _sDefaultUserID;
    private Hashtable<String, String> _htUsers;

    public WordFastUsers(Set<String> users, boolean isSet)
    {
        if (users != null && users.size() > 0)
        {
            StringBuilder sbsWordFastUsers = new StringBuilder();
            sbsWordFastUsers.append(_sHeaderName).append(",");
            String first = users.iterator().next();
            sbsWordFastUsers.append(first);
            Iterator<String> itKey = users.iterator();
            int count = 1;
            while (itKey.hasNext())
            {
                if (count > 32)
                {
                    break;
                }
                String value = itKey.next();
                sbsWordFastUsers.append(",").append(value).append(" ").append(value);
                count++;
            }
            _sHeaderString = sbsWordFastUsers.toString();
        }
        else
        {
            init(null);
        }
    }

    /**
     * Instantiates an object representing the memory users. Information about users are found in
     * the header of the memory and in a translation unit. The memory header contains all users
     * information, TU contains only the ID of the user.
     * <p>
     * The constructor accept the string coming from the memory header like "User ID,B,B Benoit,A
     * Albert", several users like "B Benoit, D Dan". The first will be the default user, one user
     * like "Milosh" and null value, in that case the machine user will be used.
     */
    public WordFastUsers(String sUsers)
    {
        init(sUsers);
    }

    public String getDefaultUserID()
    {
        return this._sDefaultUserID;
    }

    public String getDefaultUserName()
    {
        return _htUsers.get(_sDefaultUserID);
    }

    public String getUserName(String sID)
    {
        return _htUsers.get(sID);
    }

    public Hashtable<String, String> getUsers()
    {
        return _htUsers;
    }

    public String getWordFastUsers()
    {
        return this._sHeaderString;
    }

    private void init(String sUsers)
    {
        // User ID,B,B Benoit,A Albert B Benoit,A Albert D Dan null and junk input
        _htUsers = new Hashtable<String, String>();
        _sDefaultUserID = "";
        String sPcUser = System.getProperty("user.name");
        String sPCUserId = sPcUser;
        if (sPcUser.length() > 3)
        {
            sPCUserId = sPcUser.substring(0, 2);
        }
        if (sUsers == null)
        {
            setSimpleUser(sPCUserId, sPcUser);
        }
        else
        {
            sUsers = sUsers.trim();
            if (sUsers.length() < 1)
            {
                setSimpleUser(sPCUserId, sPcUser);
                return;
            }
            if (sUsers.length() < 3)
            {
                if (sUsers.contains(","))
                {
                    setSimpleUser(sPCUserId, sPcUser);
                }
                else
                {
                    setSimpleUser(sUsers, sUsers);
                }
                return;
            }

            if (sUsers.length() >= 3)
            {
                if (sUsers.contains(","))
                {
                    String[] sFields = sUsers.split(",");
                    String sFirstField = "";
                    try
                    {
                        sFirstField = sFields[0];
                    }
                    catch (Exception ex)
                    {
                    }
                    boolean bFullFormat = false;
                    if (sFirstField.trim().equals(_sHeaderName))
                    {
                        bFullFormat = true;
                    }

                    int iStartPos = 0;
                    if (bFullFormat)
                    {
                        iStartPos = 2;
                    }
                    String sUser = "";
                    String[] sUserFields = new String[2];
                    for (int i = iStartPos; i < sFields.length; i++)
                    {
                        sUser = sFields[i].trim();
                        if (!sUser.equals(""))
                        {
                            if (sUser.contains(" "))
                            {
                                sUserFields = sUser.split("\\s+");
                            }
                            else
                            {
                                if (sUser.length() > 2)
                                {
                                    sUserFields[0] = sUser.substring(0, 2);
                                }
                                else
                                {
                                    sUserFields[0] = sUser.substring(0, 1);
                                }
                                sUserFields[1] = sUser;
                            }
                            if (sUserFields[0].length() > 3)
                            {
                                sUserFields[0] = sUserFields[0].substring(0, 3);
                            }
                            _htUsers.put(sUserFields[0].trim(), sUserFields[1].trim());
                            // default user
                            if (i == iStartPos)
                            {
                                if (!bFullFormat)
                                {
                                    _sDefaultUserID = sUserFields[0];
                                }
                                else
                                {
                                    try
                                    {
                                        _sDefaultUserID = sFields[1];
                                    }
                                    catch (Exception e)
                                    {
                                    }
                                }
                            }
                        }
                    }

                    // create or recreate the wordfast users string
                    if (_htUsers.isEmpty())
                    {
                        setSimpleUser(sPCUserId, sPcUser);
                    }
                    else
                    {
                        StringBuilder sbsWordFastUsers = new StringBuilder();
                        sbsWordFastUsers.append(_sHeaderName).append(",");
                        sbsWordFastUsers.append(_sDefaultUserID);
                        Iterator<String> itKey = _htUsers.keySet().iterator();
                        while (itKey.hasNext())
                        {
                            String sId = itKey.next();
                            String sName = _htUsers.get(sId);
                            sbsWordFastUsers.append(",").append(sId).append(" ").append(sName);
                        }
                        _sHeaderString = sbsWordFastUsers.toString();
                    }
                }
                else
                {
                    String[] sFld = sUsers.split(" ");
                    if (sFld.length == 1)
                    {
                        if (sUsers.length() > 3)
                        {
                            setSimpleUser(sUsers.substring(0, 2), sUsers);
                        }
                        else
                        {
                            setSimpleUser(sUsers.substring(0, 1), sUsers);
                        }
                    }
                    else
                    {
                        setSimpleUser(sFld[0], sFld[1]);
                    }
                }
            }
        }

    }

    public void setDefaultUserID(String sDefaultUser)
    {
        this._sDefaultUserID = sDefaultUser;
    }

    private void setSimpleUser(String sID, String sName)
    {
        if (StringUtils.isBlank(sID))
        {
            sID = "X";
        }
        if (StringUtils.isBlank(sName))
        {
            sName = "XX";
        }
        if (sID.length() > 3)
        {
            sID = sID.substring(0, 3);
        }
        _sHeaderString = _sHeaderName + "," + sID + "," + sID + " " + sName;
        _sDefaultUserID = sID;
        _htUsers.put(sID, sName);
    }
}
