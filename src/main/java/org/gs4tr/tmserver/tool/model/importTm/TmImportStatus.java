package org.gs4tr.tmserver.tool.model.importTm;

import org.gs4tr.tmserver.tool.controller.jobqueue.importTm.ImportTmPhase;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TmImportStatus
{
    private String filename;
    private String jobId;
    private ImportTmPhase phase;

    public TmImportStatus(String jobId, String filename)
    {
        this.jobId = jobId;
        this.filename = filename;
        this.phase = ImportTmPhase.WAITING;
    }

    public String getFilename()
    {
        return this.filename;
    }

    public String getJobId()
    {
        return this.jobId;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public void setJobId(String jobId)
    {
        this.jobId = jobId;
    }

    public ImportTmPhase getPhase()
    {
        return phase;
    }

    public void setPhase(ImportTmPhase phase)
    {
        this.phase = phase;
    }

}
