package org.gs4tr.tmserver.tool.model.xml.xpath;

import java.util.List;

import org.jaxen.BaseXPath;
import org.jaxen.JaxenException;

/**
 * An XPath implementation for the MyDOM object model.
 * <p>
 * This is the main entry point for matching an XPath against a MyDOM tree. You create a compiled
 * XPath object, then match it against one or more context nodes using the
 * {@link #selectNodes(Object)} method, as in the following example:
 * </p>
 *
 * <pre>
 * Node node = ...;
 * XPath path = new MyDomXPath("a/b/c");
 * List results = path.selectNodes(node);
 * </pre>
 */
public class MyDomXPath extends BaseXPath
{
    static private final long serialVersionUID = 1L;

    /** Constructs an XPath from the given expression string. */
    public MyDomXPath(String xpathExpr) throws JaxenException
    {
        super(xpathExpr, DocumentNavigator.getInstance());
    }

    @Override
    public Object evaluate(Object context) throws JaxenException
    {
        super.setNamespaceContext(DefaultNamespaceContext.create(context));
        return super.selectNodes(context);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List selectNodes(Object context) throws JaxenException
    {
        super.setNamespaceContext(DefaultNamespaceContext.create(context));
        return super.selectNodes(context);
    }
}
