package org.gs4tr.tmserver.tool.model.xml;

public class Entity implements Node
{
    private Element parent;
    private String name;
    private String value;

    /**
     * @param name the name of the entity, for instance &amp;amp; = "amp".
     * @param value the value of the entity, for instance &amp;amp; = "&amp;". If null, the
     *        <code>name</code> could not be decoded.
     */
    public Entity(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    @Override
    public String asXML()
    {
        return "&" + name + ";";
    }

    /** Returns the name of the entity, for instance &amp;amp; = "amp". */
    public String getName()
    {
        return name;
    }

    @Override
    public short getNodeType()
    {
        return ENTITY_NODE;
    }

    @Override
    public Element getParent()
    {
        return parent;
    }

    /**
     * Returns the value of the entity, for instance &amp;amp; = "&amp;". If null, the
     * <code>name</code> could not be decoded.
     */
    public String getValue()
    {
        return value;
    }

    @Override
    public void setParent(Element node)
    {
        parent = node;
    }

    @Override
    public String toString()
    {
        return "&" + name + ";";
    }
}
