package org.gs4tr.tmserver.tool.model.importTm;

public enum TmAttributeUpdateOption
{
	IGNORE, ONLY_EXISTING, PRESERVE
}
