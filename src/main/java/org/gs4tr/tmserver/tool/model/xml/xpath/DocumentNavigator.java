package org.gs4tr.tmserver.tool.model.xml.xpath;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.model.xml.Attribute;
import org.gs4tr.tmserver.tool.model.xml.CDATA;
import org.gs4tr.tmserver.tool.model.xml.Comment;
import org.gs4tr.tmserver.tool.model.xml.Document;
import org.gs4tr.tmserver.tool.model.xml.Element;
import org.gs4tr.tmserver.tool.model.xml.Entity;
import org.gs4tr.tmserver.tool.model.xml.Namespace;
import org.gs4tr.tmserver.tool.model.xml.Node;
import org.gs4tr.tmserver.tool.model.xml.ProcessingInstruction;
import org.gs4tr.tmserver.tool.model.xml.QName;
import org.gs4tr.tmserver.tool.model.xml.Text;
import org.jaxen.DefaultNavigator;
import org.jaxen.JaxenConstants;
import org.jaxen.NamedAccessNavigator;
import org.jaxen.NamespaceContext;
import org.jaxen.Navigator;
import org.jaxen.UnsupportedAxisException;
import org.jaxen.XPath;
import org.jaxen.saxpath.SAXPathException;
import org.jaxen.util.SingleObjectIterator;

/**
 * Interface for navigating around the MyDOM object model.
 * <p>
 * This class is not intended for direct usage, but is used by the Jaxen engine during evaluation.
 * </p>
 */
@SuppressWarnings("rawtypes")
public class DocumentNavigator extends DefaultNavigator implements NamedAccessNavigator
{
    /** Singleton implementation. */
    static private class Singleton
    {
        /** Singleton instance. */
        static private DocumentNavigator instance = new DocumentNavigator();
    }

    static private final long serialVersionUID = 1L;

    /** Retrieve the singleton instance of this <code>DocumentNavigator</code>. */
    static public Navigator getInstance()
    {
        return Singleton.instance;
    }

    // ----------------------------------------------------------------------
    // Axis Iterators
    // ----------------------------------------------------------------------

    /**
     * Retrieves an <code>Iterator</code> matching the <code>attribute</code> XPath axis.
     *
     * @param contextNode the original context node
     * @return an Iterator capable of traversing the axis, not null
     * @throws UnsupportedAxisException if the semantics of the attribute axis are not supported by
     *         this object model
     */
    @Override
    public Iterator getAttributeAxisIterator(Object contextNode)
    {
        if (!(contextNode instanceof Element))
        {
            return JaxenConstants.EMPTY_ITERATOR;
        }

        Element elem = (Element) contextNode;
        return elem.attributes().iterator();
    }

    /**
     * Retrieves an <code>Iterator</code> that returns the <code>attribute</code> XPath axis where
     * the names of the attributes match the supplied name and optional namespace.
     * <p>
     * This method must only return attribute nodes with the correct name.
     * <p>
     * If the namespaceURI is null, no namespace should be used. The prefix will never be null.
     *
     * @param contextNode the origin context node
     * @param localName the local name of the attributes to return, always present
     * @param namespacePrefix the prefix of the namespace of the attributes to return
     * @param namespaceURI the URI of the namespace of the attributes to return
     * @return an Iterator capable of traversing the named attributes, or null if none
     * @throws UnsupportedAxisException if the attribute axis is not supported by this object model
     */
    @Override // NamedAccessNavigator
    public Iterator getAttributeAxisIterator(Object contextNode, String localName, String namespacePrefix,
        String namespaceURI)
    {
        if (contextNode instanceof Element)
        {
            Element element = (Element) contextNode;

            Namespace ns = (namespaceURI == null ? Namespace.NO_NAMESPACE
                : Namespace.getNamespace(namespacePrefix, namespaceURI));
            String qName = StringUtils.isNotEmpty(namespacePrefix) ? namespacePrefix + ":" + localName
                : localName;

            Attribute attr = element.attribute(QName.get(namespacePrefix, localName, qName, ns));

            if (attr != null)
            {
                return new SingleObjectIterator(attr);
            }
        }

        return JaxenConstants.EMPTY_ITERATOR;
    }

    /**
     * Retrieve the local name of the given attribute node.
     *
     * @param attr the context attribute node
     * @return the local name of the attribute node
     */
    @Override
    public String getAttributeName(Object obj)
    {
        Attribute attr = (Attribute) obj;

        return attr.getName().getName();
    }

    @Override
    public String getAttributeNamespaceUri(Object obj)
    {
        Attribute attr = (Attribute) obj;

        return attr.getNamespace().getURI();
    }

    /**
     * Retrieves the qualified name of the given attribute node.
     *
     * @param attr the context attribute node
     * @return the qualified name of the attribute node
     */
    @Override
    public String getAttributeQName(Object obj)
    {
        Attribute attr = (Attribute) obj;

        return attr.getName().getQualifiedName();
    }

    /**
     * Retrieves the string-value of an attribute node. This should be the XML 1.0 normalized
     * attribute value. This may be the empty string but must not be null.
     *
     * @param attr the attribute node
     * @return the string-value of the node
     */
    @Override
    public String getAttributeStringValue(Object obj)
    {
        Attribute attr = (Attribute) obj;

        // TODO: normalize according to XML 1.0 rules.
        return attr.getValue().getText();
    }

    // ----------------------------------------------------------------------
    // Extractors
    // ----------------------------------------------------------------------

    /**
     * Retrieves an <code>Iterator</code> matching the <code>child</code> elements XPath axis.
     *
     * @param contextNode the original context node
     * @return an Iterator capable of traversing the axis, not null
     * @throws UnsupportedAxisException if the semantics of the child axis are not supported by this
     *         object model
     */
    // CvdL: these are Document.content() and Element.content().
    @Override
    public Iterator getChildAxisIterator(Object contextNode)
    {
        Node node = (Node) contextNode;

        switch (node.getNodeType())
        {
        case Node.ELEMENT_NODE:
            return ((Element) node).content().iterator();
        case Node.DOCUMENT_NODE:
            return ((Document) node).content().iterator();
        }

        return JaxenConstants.EMPTY_ITERATOR;
    }

    /**
     * Retrieves an <code>Iterator</code> that returns the <code>child</code> XPath axis where the
     * names of the children match the supplied name and optional namespace.
     * <p>
     * This method must only return element nodes with the correct name.
     * <p>
     * If the namespaceURI is null, no namespace should be used. The prefix will never be null.
     *
     * @param contextNode the origin context node
     * @param localName the local name of the children to return, always present
     * @param namespacePrefix the prefix of the namespace of the children to return
     * @param namespaceURI the namespace URI of the children to return
     * @return an Iterator capable of traversing the named children, or null if none
     * @throws UnsupportedAxisException if the child axis is not supported by this object model
     */
    @Override // NamedAccessNavigator
    public Iterator getChildAxisIterator(Object contextNode, String localName, String namespacePrefix,
        String namespaceURI)
    {
        if (contextNode instanceof Element)
        {
            Element node = (Element) contextNode;

            Namespace ns = (namespaceURI == null ? Namespace.NO_NAMESPACE
                : Namespace.getNamespace(namespacePrefix, namespaceURI));

            String qName = StringUtils.isNotEmpty(namespacePrefix) ? namespacePrefix + ":" + localName
                : localName;

            return node.elements(QName.get(namespacePrefix, localName, qName, ns)).iterator();
        }

        if (contextNode instanceof Document)
        {
            Document node = (Document) contextNode;
            Element el = node.getRootElement();

            if (el == null || !el.getName().getName().equals(localName))
            {
                return JaxenConstants.EMPTY_ITERATOR;
            }
            if (namespaceURI != null)
            {
                if (!namespaceURI.equals(el.getNamespace().getURI()))
                {
                    return JaxenConstants.EMPTY_ITERATOR;
                }
            }

            return new SingleObjectIterator(el);
        }

        return JaxenConstants.EMPTY_ITERATOR;
    }

    /**
     * Retrieves the string-value of a comment node. This may be the empty string if the comment is
     * empty, but must not be null.
     *
     * @param comment the comment node
     * @return the string-value of the node
     */
    @Override
    public String getCommentStringValue(Object obj)
    {
        Comment cmt = (Comment) obj;

        return cmt.getComment();
    }

    /**
     * Returns the document node that contains the given context node.
     *
     * @see #isDocument(Object)
     * @param contextNode the context node
     * @return the document of the context node
     */
    @Override
    public Object getDocumentNode(Object contextNode)
    {
        if (contextNode instanceof Document)
        {
            return contextNode;
        }

        if (contextNode instanceof Element)
        {
            Element element = (Element) contextNode;
            return element.getDocument();
        }

        return null;
    }

    /**
     * Retrieves the local name of the given element node.
     *
     * @param element the context element node
     * @return the local name of the element node
     */
    @Override
    public String getElementName(Object obj)
    {
        Element elem = (Element) obj;

        return elem.getName().getName();
    }

    @Override
    public String getElementNamespaceUri(Object obj)
    {
        Element elem = (Element) obj;

        return elem.getNamespace().getURI();
    }

    /**
     * Retrieves the qualified name of the given element node.
     *
     * @param element the context element node
     * @return the qualified name of the element node
     */
    @Override
    public String getElementQName(Object obj)
    {
        Element elem = (Element) obj;

        return elem.getName().getQualifiedName();
    }

    /**
     * Retrieves the string-value of an element node. This may be the empty string if the element is
     * empty, but must not be null.
     *
     * @param element the element node.
     * @return the string-value of the node.
     */
    @Override
    public String getElementStringValue(Object obj)
    {
        Element elem = (Element) obj;
        return elem.getText();
    }

    /**
     * Retrieves an <code>Iterator</code> matching the <code>namespace</code> XPath axis.
     *
     * @param contextNode the original context node
     * @return an Iterator capable of traversing the axis, not null
     * @throws UnsupportedAxisException if the semantics of the namespace axis are not supported by
     *         this object model
     */
    @Override
    public Iterator getNamespaceAxisIterator(Object contextNode)
    {
        if (!(contextNode instanceof Element))
        {
            return JaxenConstants.EMPTY_ITERATOR;
        }

        Element element = (Element) contextNode;
        ArrayList<NamespaceAdapter> result = new ArrayList<>();
        HashSet<String> prefixes = new HashSet<>();

        for (Element context = element; context != null; context = context.getParent())
        {
            ArrayList<Namespace> declaredNS = new ArrayList<>(context.declaredNamespaces());
            declaredNS.add(context.getNamespace());

            List<Attribute> attrs = context.attributes();
            for (int i = 0, max = attrs.size(); i < max; i++)
            {
                declaredNS.add(attrs.get(i).getNamespace());
            }

            // CvdL: this is DOM4J code based on Jaxen code that prevents using the same
            // prefix twice.
            // Technically it is allowed:
            // https://www.sitepoint.com/xml-namespaces-explained/.
            // "It is also possible (though not recommended) for the same prefix refer to
            // different namespaces, depending on their context[...]"
            // <ns:elem xmlns:ns="uri1"><ns:elem xmlns:ns="uri2"/></ns:elem>
            for (int i = 0, max = declaredNS.size(); i < max; i++)
            {
                Namespace namespace = declaredNS.get(i);

                if (namespace != Namespace.NO_NAMESPACE)
                {
                    String prefix = namespace.getPrefix();
                    if (!prefixes.contains(prefix))
                    {
                        prefixes.add(prefix);
                        // This makes the Namespace attached to *this* parent.
                        result.add(new NamespaceAdapter(element, namespace));
                    }
                }
            }
        }

        // This makes the Namespace attached to *this* parent.
        result.add(new NamespaceAdapter(element, Namespace.XML_NAMESPACE));

        return result.iterator();
    }

    /**
     * Retrieves the namespace prefix of a namespace node.
     *
     * @param ns the namespace node
     * @return the prefix associated with the node
     */
    @Override
    public String getNamespacePrefix(Object obj)
    {
        if (obj instanceof Namespace)
        {
            Namespace ns = (Namespace) obj;
            return ns.getPrefix();
        }
        else
        {
            NamespaceAdapter ns = (NamespaceAdapter) obj;
            return ns.getNamespace().getPrefix();
        }
    }

    /**
     * Retrieves the string-value of a namespace node. This is generally the namespace URI. This may
     * be the empty string but must not be null.
     *
     * @param ns the namespace node
     * @return the string-value of the node
     */
    @Override
    public String getNamespaceStringValue(Object obj)
    {
        if (obj instanceof Namespace)
        {
            Namespace ns = (Namespace) obj;
            return ns.getURI();
        }
        else
        {
            NamespaceAdapter ns = (NamespaceAdapter) obj;
            return ns.getNamespace().getURI();
        }
    }

    /**
     * Retrieves an <code>Iterator</code> matching the <code>parent</code> XPath axis.
     *
     * @param contextNode the original context node
     * @return an Iterator capable of traversing the axis, not null
     * @throws UnsupportedAxisException if the semantics of the parent axis are not supported by
     *         this object model
     */
    @Override
    public Iterator getParentAxisIterator(Object contextNode)
    {
        Object parent = null;

        if (contextNode instanceof Document)
        {
            return JaxenConstants.EMPTY_ITERATOR;
        }

        if (contextNode instanceof Element)
        {
            Element element = (Element) contextNode;

            parent = ((Element) contextNode).getParent();

            if (parent == null)
            {
                parent = element.getDocument();
            }
        }
        else if (contextNode instanceof Attribute)
        {
            parent = ((Attribute) contextNode).getParent();
        }
        else if (contextNode instanceof NamespaceAdapter)
        {
            parent = ((NamespaceAdapter) contextNode).getElement();
        }
        else if (contextNode instanceof ProcessingInstruction)
        {
            parent = ((ProcessingInstruction) contextNode).getParent();
        }
        else if (contextNode instanceof Comment)
        {
            parent = ((Comment) contextNode).getParent();
        }
        else if (contextNode instanceof Text)
        {
            parent = ((Text) contextNode).getParent();
        }

        if (parent != null)
        {
            return new SingleObjectIterator(parent);
        }

        return JaxenConstants.EMPTY_ITERATOR;
    }

    /**
     * Returns the parent of the given context node.
     * <p>
     * The parent of any node must either be a document node or an element node.
     * </p>
     *
     * @see #isDocument
     * @see #isElement
     * @param contextNode the context node
     * @return the parent of the context node, or null if this is a document node.
     * @throws UnsupportedAxisException if the parent axis is not supported by the model
     */
    @Override
    public Object getParentNode(Object contextNode)
    {
        if (contextNode instanceof Node)
        {
            Node node = (Node) contextNode;

            switch (node.getNodeType())
            {
            case Node.DOCUMENT_NODE:
            {
                return null;
            }
            case Node.ELEMENT_NODE:
            {
                Element element = (Element) contextNode;
                Element parent = element.getParent();

                if (parent != null)
                {
                    return parent;
                }

                return element.getDocument();
            }
            case Node.ATTRIBUTE_NODE:
            {
                return ((Attribute) contextNode).getParent();
            }
            case Node.PROCESSING_INSTRUCTION_NODE:
            {
                return ((ProcessingInstruction) contextNode).getParent();
            }
            case Node.COMMENT_NODE:
            {
                return ((Comment) contextNode).getParent();
            }
            case Node.TEXT_NODE:
            {
                return ((Text) contextNode).getParent();
            }
            case Node.CDATA_NODE:
            {
                return ((CDATA) contextNode).getParent();
            }

            default:
                // Add other nodes that support parent() here.
                throw new RuntimeException("Unhandled node type " + node.getNodeType());
                // return null;
            }
        }

        if (contextNode instanceof NamespaceAdapter)
        {
            return ((NamespaceAdapter) contextNode).getElement();
        }

        return null;
    }

    /**
     * Retrieves the data of a processing-instruction.
     *
     * @param pi the context processing-instruction node
     * @return the data of the processing-instruction node
     */
    @Override
    public String getProcessingInstructionData(Object obj)
    {
        ProcessingInstruction pi = (ProcessingInstruction) obj;

        return pi.getData();
    }

    /**
     * Retrieves the target of a processing-instruction.
     *
     * @param pi the context processing-instruction node
     * @return the target of the processing-instruction node
     */
    @Override
    public String getProcessingInstructionTarget(Object obj)
    {
        ProcessingInstruction pi = (ProcessingInstruction) obj;

        return pi.getTarget();
    }

    /**
     * Retrieves the string-value of a text node. This must not be null and should not be the empty
     * string. The XPath data model does not allow empty text nodes.
     *
     * @param text the text node
     * @return the string-value of the node
     */
    @Override
    public String getTextStringValue(Object obj)
    {
        // isText(Object) accepts the content() list of elements
        // if it contains only text.
        if (obj instanceof List)
        {
            StringBuffer result = new StringBuffer();

            List list = (List) obj;
            for (int i = 0, max = list.size(); i < max; i++)
            {
                Object o = list.get(i);
                if (o instanceof Text)
                {
                    result.append(((Text) o).getText());
                }
                else if (o instanceof CDATA)
                {
                    result.append(((CDATA) o).getText());
                }
                else if (o instanceof Entity)
                {
                    result.append(StringUtils.defaultString(((Entity) o).getValue()));
                }
            }

            return result.toString();
        }

        if (obj instanceof Text)
        {
            return ((Text) obj).getText();
        }

        if (obj instanceof CDATA)
        {
            return ((CDATA) obj).getText();
        }

        if (obj instanceof Entity)
        {
            return StringUtils.defaultString(((Entity) obj).getValue());
        }

        throw new RuntimeException("DocumentNavigator.getTextStringValue() called on unknown obj " + obj);
        // return "";
    }

    @Override
    public boolean isAttribute(Object obj)
    {
        return obj instanceof Attribute;
    }

    @Override
    public boolean isComment(Object obj)
    {
        return obj instanceof Comment;
    }

    // ----------------------------------------------------------------------
    // String-Value extractors
    // ----------------------------------------------------------------------

    @Override
    public boolean isDocument(Object obj)
    {
        return obj instanceof Document;
    }

    @Override
    public boolean isElement(Object obj)
    {
        return obj instanceof Element;
    }

    @Override
    public boolean isNamespace(Object obj)
    {
        return obj instanceof Namespace || obj instanceof NamespaceAdapter;
    }

    @Override
    public boolean isProcessingInstruction(Object obj)
    {
        return obj instanceof ProcessingInstruction;
    }

    @Override
    public boolean isText(Object obj)
    {
        // This is called in "/path[text()]" and looks like it is the
        // elem.content() list. If it consists of only text-ish nodes
        // we can return true. See getTextStringValue(Object).
        if (obj instanceof List)
        {
            List list = (List) obj;
            for (int i = 0, max = list.size(); i < max; i++)
            {
                Object o = list.get(i);
                if (!(o instanceof Text || o instanceof Entity || o instanceof CDATA))
                {
                    return false;
                }
            }

            return true;
        }

        // This is called in "/path[text()]" so needs to include ENTITY.
        return (obj instanceof Text || obj instanceof CDATA || obj instanceof Entity);
    }

    // ----------------------------------------------------------------------
    // General utilities
    // ----------------------------------------------------------------------

    /**
     * Returns a parsed form of the given XPath string, which will be suitable for queries on MyDOM
     * documents.
     */
    @Override
    public XPath parseXPath(String xpath) throws SAXPathException
    {
        return new MyDomXPath(xpath);
    }

    /**
     * Translates a namespace prefix to a namespace URI, <strong>possibly</strong> considering a
     * particular element node.
     * <p>
     * Strictly speaking, prefix-to-URI translation should occur irrespective of any element in the
     * document. This method is provided to allow a non-conforming ease-of-use enhancement.
     * </p>
     *
     * @see NamespaceContext
     * @param prefix the prefix to translate
     * @param element the element to consider during translation
     * @return the namespace URI associated with the prefix
     */
    @Override
    public String translateNamespacePrefixToUri(String prefix, Object context)
    {
        Element element = null;
        if (context instanceof Element)
        {
            element = (Element) context;
        }
        else if (context instanceof Node)
        {
            Node node = (Node) context;
            element = node.getParent();
        }

        if (element != null)
        {
            Namespace namespace = element.getNamespaceForPrefix(prefix);

            if (namespace != null)
            {
                return namespace.getURI();
            }
        }

        return null;
    }
}
