package org.gs4tr.tmserver.tool.model.tmx;

public class NewAttribute
{
    private String key;
    private String value;
    private boolean hide;

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public boolean isHide()
    {
        return hide;
    }

    public void setHide(boolean hide)
    {
        this.hide = hide;
    }
}
