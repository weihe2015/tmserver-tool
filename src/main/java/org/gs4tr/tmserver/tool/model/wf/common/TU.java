package org.gs4tr.tmserver.tool.model.wf.common;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class TU
{
    private boolean dirty = false;

    // Persistent members of a Wordfast TM TU line.
    private Date _creationDate;
    private String _creationUser;
    private int _reuseCounter;
    private String _source;
    private String _target;
    private String _sourceLocale;
    private String _targetLocale;
    private String _workgroup;
    private Map<CustomAttributeEnum, String> _attributeMap;

    // Transient members of a Wordfast TM TU line.
    private Date _modificationDate; // not persistent in Wordfast TMs
    private String _modificationUser; // not persistent in Wordfast TMs

    // TODO: pointer in memory - delete
    private long _pointer;

    private String _sTuLine;
    private String _sWordFastDate;

    /**
     * Constructs a new empty translation unit with the current date as creation date.
     */
    public TU()
    {
        // set default values
        _creationDate = new Date();
        _sWordFastDate = WordFastDate.valueOf(getCreationDate());
        _creationUser = "";
        _modificationDate = null;
        _modificationUser = "";
        _source = "";
        _target = "";
        _sourceLocale = "XX-XX";
        _targetLocale = "XX-XX";

        _reuseCounter = 0;
        _pointer = 0;

        initAttributeMap();

        dirty = true;
    }

    /**
     * Extends an existing TuImpl (shallow copy).
     */
    // public TU(TuImpl tu)
    // {
    // _creationDate = tu.getCreationDate();
    // _sWordFastDate = WordFastDate.valueOf(tu.getCreationDate());
    // _creationUser = tu.getCreationUser();
    // _modificationDate = tu.getModificationDate();
    // _modificationUser = tu.getModificationUser();
    // _source = tu.getSource();
    // _target = tu.getTarget();
    // _sourceLocale = tu.getSourceLocale();
    // _targetLocale = tu.getTargetLocale();
    //
    // _reuseCounter = tu.getReuseCounter();
    // _pointer = tu.getPointer();
    //
    // // Shallow copy, modifications to either TU are shared.
    // setAttributeMap(tu.getAttributeMap());
    //
    // dirty = true;
    // }

    /**
     * Constructs a new translation unit object from a memory line from TM.
     */
    public TU(String sTU)
    {
        if (StringUtils.isBlank(sTU))
        {
            throw new RuntimeException("TU line is empty");
        }

        String[] sFields = sTU.split("\t");
        if (sFields.length < 7)
        {
            throw new RuntimeException("TU line has less than 7 fields: " + sTU);
        }

        // also sets creation date
        setWordFastDate(sFields[0]);
        _creationUser = sFields[1];
        _reuseCounter = Integer.parseInt(sFields[2]);
        _sourceLocale = sFields[3];
        _source = sFields[4];
        _targetLocale = sFields[5];
        _target = sFields[6];

        String attributeValue1 = "";
        String attributeValue2 = "";
        String attributeValue3 = "";
        String attributeValue4 = "";

        if (sFields.length >= 8)
        {
            attributeValue1 = sFields[7].trim();

            int workgroupSeparator = attributeValue1.indexOf('!');
            if (workgroupSeparator >= 0)
            {
                // TODO: here is the workgroup but I don't know what to do with it
                // in a Lucene TM. Isn't the "group" defined by the user account?
                _workgroup = attributeValue1.substring(workgroupSeparator + 1);
                attributeValue1 = attributeValue1.substring(0, workgroupSeparator);
            }

            // Also undo the "!" conversion created in createWfTu().
            attributeValue1 = attributeValue1.replace('\u00A1', '!');
        }
        if (sFields.length >= 9)
        {
            attributeValue2 = sFields[8].trim();
        }
        if (sFields.length >= 10)
        {
            attributeValue3 = sFields[9].trim();
        }
        if (sFields.length >= 11)
        {
            attributeValue4 = sFields[10].trim();
        }

        // set attribute map
        HashMap<CustomAttributeEnum, String> attributeMap = new HashMap<CustomAttributeEnum, String>();
        attributeMap.put(CustomAttributeEnum.CUSTOM1, attributeValue1);
        attributeMap.put(CustomAttributeEnum.CUSTOM2, attributeValue2);
        attributeMap.put(CustomAttributeEnum.CUSTOM3, attributeValue3);
        attributeMap.put(CustomAttributeEnum.CUSTOM4, attributeValue4);
        _attributeMap = attributeMap;

        dirty = true;
    }

    private void buildLine()
    {
        if (!dirty)
        {
            return;
        }

        StringBuilder sb = new StringBuilder();

        sb.append(_sWordFastDate);
        sb.append("\t");
        sb.append(getCreationUser());
        sb.append("\t");
        sb.append(getReuseCounter());
        sb.append("\t");
        sb.append(getSourceLocale());
        sb.append("\t");
        sb.append(getSource());
        sb.append("\t");
        sb.append(getTargetLocale());
        sb.append("\t");
        sb.append(getTarget());
        sb.append("\t");
        sb.append(getAttributeValueId1().replace('!', '\u00A1'));
        if (StringUtils.isNotBlank(_workgroup))
        {
            sb.append("!");
            sb.append(_workgroup);
        }
        sb.append("\t");
        sb.append(getAttributeValueId2());
        sb.append("\t");
        sb.append(getAttributeValueId3());
        sb.append("\t");
        sb.append(getAttributeValueId4());
        // no trailing \t

        _sTuLine = sb.toString();

        dirty = false;
    }

    public Map<CustomAttributeEnum, String> getAttributeMap()
    {
        return _attributeMap;
    }

    // getter and setter

    public String getAttributeValueId1()
    {
        return _attributeMap.get(CustomAttributeEnum.CUSTOM1);
    }

    public String getAttributeValueId2()
    {
        return _attributeMap.get(CustomAttributeEnum.CUSTOM2);
    }

    public String getAttributeValueId3()
    {
        return _attributeMap.get(CustomAttributeEnum.CUSTOM3);
    }

    public String getAttributeValueId4()
    {
        return _attributeMap.get(CustomAttributeEnum.CUSTOM4);
    }

    public Date getCreationDate()
    {
        return _creationDate;
    }

    public String getCreationUser()
    {
        return _creationUser;
    }

    public Date getModificationDate()
    {
        return _modificationDate;
    }

    public String getModificationUser()
    {
        return _modificationUser;
    }

    public long getPointer()
    {
        return _pointer;
    }

    public int getReuseCounter()
    {
        return _reuseCounter;
    }

    public String getSource()
    {
        return _source;
    }

    public String getSourceLocale()
    {
        return _sourceLocale;
    }

    public String getTarget()
    {
        return _target;
    }

    public String getTargetLocale()
    {
        return _targetLocale;
    }

    public String getWordFastDate()
    {
        return _sWordFastDate;
    }

    public String getWorkgroup()
    {
        return _workgroup;
    }

    private void initAttributeMap()
    {
        _attributeMap = new HashMap<CustomAttributeEnum, String>();
        _attributeMap.put(CustomAttributeEnum.CUSTOM1, "");
        _attributeMap.put(CustomAttributeEnum.CUSTOM2, "");
        _attributeMap.put(CustomAttributeEnum.CUSTOM3, "");
        _attributeMap.put(CustomAttributeEnum.CUSTOM4, "");

        dirty = true;
    }

    public void setAttributeMap(Map<CustomAttributeEnum, String> attributeMap)
    {
        _attributeMap = attributeMap;
        dirty = true;
    }

    public void setAttributeValueId1(String value)
    {
        _attributeMap.put(CustomAttributeEnum.CUSTOM1, value.trim());
        dirty = true;
    }

    public void setAttributeValueId2(String value)
    {
        _attributeMap.put(CustomAttributeEnum.CUSTOM2, value.trim());
        dirty = true;
    }

    public void setAttributeValueId3(String value)
    {
        _attributeMap.put(CustomAttributeEnum.CUSTOM3, value.trim());
        dirty = true;
    }

    public void setAttributeValueId4(String value)
    {
        _attributeMap.put(CustomAttributeEnum.CUSTOM4, value.trim());
        dirty = true;
    }

    /** Side-effect: sets _sWordFastDate */
    public void setCreationDate(Date creationDate)
    {
        _creationDate = creationDate;
        _sWordFastDate = WordFastDate.valueOf(creationDate);
        dirty = true;
    }

    public void setCreationUser(String creationUser)
    {
        _creationUser = creationUser;
        dirty = true;
    }

    public void setModificationDate(Date modificationDate)
    {
        _modificationDate = modificationDate;
        dirty = true;
    }

    public void setModificationUser(String modificationUser)
    {
        _modificationUser = modificationUser;
        dirty = true;
    }

    public void setPointer(long pointer)
    {
        _pointer = pointer;
    }

    public void setReuseCounter(int reuseCounter)
    {
        _reuseCounter = reuseCounter;
        dirty = true;
    }

    public void setSource(String source)
    {
        _source = source;
        dirty = true;
    }

    public void setSourceLocale(String sourceLocale)
    {
        _sourceLocale = sourceLocale;
        dirty = true;
    }

    public void setTarget(String target)
    {
        _target = target;
        dirty = true;
    }

    public void setTargetLocale(String targetLocale)
    {
        _targetLocale = targetLocale;
        dirty = true;
    }

    /** Side-effect: sets creation date */
    public void setWordFastDate(String sWordFastDate)
    {
        Date date = WordFastDate.parse(sWordFastDate);
        if (date == null)
        {
            throw new RuntimeException("invalid Wordfast date: " + sWordFastDate);
        }

        _sWordFastDate = sWordFastDate;
        _creationDate = date;
        dirty = true;
    }

    public void setWorkgroup(String workgroup)
    {
        _workgroup = workgroup;
        dirty = true;
    }

    @Override
    public String toString()
    {
        buildLine();
        return _sTuLine;
    }
}
