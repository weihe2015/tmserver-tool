package org.gs4tr.tmserver.tool.model.swagger;

import java.util.List;

public class ImportTMProcessResponse
{
    public String phase;
    public int percent;
    public int total;
    public int imported;
    public int skipped;
    public boolean success;
    public int returnCode;
    public List<String> errors;
}
