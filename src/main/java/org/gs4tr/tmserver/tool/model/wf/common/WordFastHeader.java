package org.gs4tr.tmserver.tool.model.wf.common;

import java.util.Date;

public class WordFastHeader
{
    static public final String WORDFAST_MEMORY_VERSION = "v.546";

    private String wordFastDate = "";
    private Date date;
    private String wordFastUsers = "";
    private String unitCount = "";
    private String sourceLocale = "";
    private String targetLocale = "";
    private String version = "";
    private String rule = "";
    private String licence = "";
    private String[] attributes = new String[4];

    public WordFastHeader()
    {
        attributes = new String[] { "", "", "", "" };
        version = "Wordfast TM " + WORDFAST_MEMORY_VERSION;
        rule = "00";
        unitCount = "TU=00000000";
    }

    public WordFastHeader(String line)
    {
        String[] memFields = line.split("\t");
        /*
         * memory header (start by BOM in Unicode) 0 : %20050119~082351\t 1 : %User ID,i
         * internet\t 2 : %TU=00000000\t 3 : %EN-US\t 4 : %Wordfast TM v4.30/00\t 5 :
         * %FR-FR\t 6 : %---89660958\t 7 to 10 : Optional 4 attributes (if not \t\t\t\t)
         * 11 : 60 spaces + dot
         */

        wordFastDate = "00010101~000000";
        try
        {
            wordFastDate = memFields[0].substring(memFields[0].indexOf("%") + 1);
        }
        catch (Exception e)
        {
        }
        date = WordFastDate.parse(wordFastDate);
        // *** Users *** 1 ***
        wordFastUsers = "User ID,A Admin";
        try
        {
            wordFastUsers = memFields[1].replaceAll("%", "");
        }
        catch (Exception e)
        {
        }
        // *** UnitCount *** 2 ***
        unitCount = "TU=00000000";
        try
        {
            unitCount = memFields[2].replaceAll("%", "");
        }
        catch (Exception e)
        {
        }

        // *** source language *** 3 et 5 ***
        sourceLocale = "";
        targetLocale = "";
        try
        {
            sourceLocale = memFields[3].replaceAll("%", "");
        }
        catch (Exception e)
        {
        }
        try
        {
            targetLocale = memFields[5].replaceAll("%", "");
        }
        catch (Exception e)
        {
        }

        // **** Version and rules *** 4 ***
        version = "Wordfast TM " + WORDFAST_MEMORY_VERSION;
        rule = "00";
        String versionAndRules = "";
        try
        {
            versionAndRules = memFields[4].replaceAll("%", "");
        }
        catch (Exception e)
        {
        }
        String[] sFld = versionAndRules.split("/");
        try
        {
            version = sFld[0];
        }
        catch (Exception e)
        {
        }
        try
        {
            rule = sFld[1];
            // first number is the rule define by the 4 constants
            // OVERWRITE,OVERWRITE_WHEN_IDENTICAL_ATTRIBUTES,APPEND,SKIP
            // if (rule.length() == 2)
            // _RULE = new Integer(_sRules.substring(0, 1)).intValue();
            // the second number is not implemented
        }
        catch (Exception e)
        {
        }

        // **** License *** 6 ***
        licence = "";
        try
        {
            licence = memFields[6].replaceAll("%", "");
        }
        catch (Exception e)
        {
        }

        // **** Attributes *** 7 to 10 ***
        attributes = new String[] { "", "", "", "" };
        try
        {
            attributes[0] = memFields[7];
        }
        catch (Exception e)
        {
        }
        try
        {
            attributes[1] = memFields[8];
        }
        catch (Exception e)
        {
        }
        try
        {
            attributes[2] = memFields[9];
        }
        catch (Exception e)
        {
        }
        try
        {
            attributes[3] = memFields[10];
        }
        catch (Exception e)
        {
        }
    }

    public WordFastHeader(String wordFastDate, String wordFastUsers, String unitCount, String sourceLocale,
        String targetLocale, String version, String rule, String licence, String[] attributes)
    {
        this.wordFastDate = wordFastDate;
        this.wordFastUsers = wordFastUsers;
        this.unitCount = unitCount;
        this.sourceLocale = sourceLocale;
        this.targetLocale = targetLocale;
        this.version = version;
        this.rule = rule;
        this.licence = licence;
        this.attributes = attributes;
    }

    public String getAttribute(int index)
    {
        return attributes[index];
    }

    public String[] getAttributes()
    {
        return attributes;
    }

    public Date getDate()
    {
        return date;
    }

    public String getHeader()
    {
        // (start by BOM in Unicode)
        // 0 : %20050119~082351\t
        // 1 : %User ID,i internet\t
        // 2 : %TU=00000000\t
        // 3 : %EN-US\t
        // 4 : %Wordfast TM v4.30/00\t
        // 5 : %FR-FR\t
        // 6 : %---89660958\t
        // 7 : Optional 4 attributes (if not \t\t\t\t)
        // 8 : 60 spaces + dot

        StringBuilder sbHeader = new StringBuilder();
        // sbHeader.append(_sBom);
        sbHeader.append("%").append(wordFastDate).append("\t");
        sbHeader.append("%").append(wordFastUsers).append("\t");
        sbHeader.append("%").append(unitCount).append("\t");
        sbHeader.append("%").append(sourceLocale).append("\t");
        sbHeader.append("%").append(version).append("/").append(rule).append("\t");
        sbHeader.append("%").append(targetLocale).append("\t");
        sbHeader.append("%").append(licence).append("\t");
        sbHeader.append(attributes[0]);
        sbHeader.append("\t");
        sbHeader.append(attributes[1]);
        sbHeader.append("\t");
        sbHeader.append(attributes[2]);
        sbHeader.append("\t");
        sbHeader.append(attributes[3]);
        sbHeader.append("\t");
        // trailing spaces
        int iSpaceCount = 60;
        for (int i = 0; i < iSpaceCount; i++)
        {
            sbHeader.append(" ");
        }
        sbHeader.append(".");
        return sbHeader.toString();
    }

    public String getLicence()
    {
        return licence;
    }

    public String getRule()
    {
        return rule;
    }

    public String getSourceLocale()
    {
        return sourceLocale;
    }

    public String getTargetLocale()
    {
        return targetLocale;
    }

    public String getUnitCount()
    {
        return unitCount;
    }

    public String getVersion()
    {
        return version;
    }

    public String getWordFastDate()
    {
        return wordFastDate;
    }

    public String getWordFastUsers()
    {
        return wordFastUsers;
    }

    public void setAttributes(String[] attributes)
    {
        this.attributes = attributes;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public void setLicence(String licence)
    {
        this.licence = licence;
    }

    public void setRule(String rule)
    {
        this.rule = rule;
    }

    public void setSourceLocale(String sourceLocale)
    {
        this.sourceLocale = sourceLocale;
    }

    public void setTargetLocale(String targetLocale)
    {
        this.targetLocale = targetLocale;
    }

    public void setUnitCount(int unitCount)
    {
        this.unitCount = toStringUnitCount(unitCount);
    }

    public void setUnitCount(String unitCount)
    {
        this.unitCount = unitCount;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public void setWordFastDate(String wordFastDate)
    {
        this.wordFastDate = wordFastDate;
    }

    public void setWordFastUsers(String wordFastUsers)
    {
        this.wordFastUsers = wordFastUsers;
    }

    private String toStringUnitCount(int count)
    {
        String sCount = "TU=00000000";
        if (count < 10)
        {
            sCount = "TU=0000000" + count;
        }
        else if (count < 100)
        {
            sCount = "TU=000000" + count;
        }
        else if (count < 1000)
        {
            sCount = "TU=00000" + count;
        }
        else if (count < 10000)
        {
            sCount = "TU=0000" + count;
        }
        else if (count < 100000)
        {
            sCount = "TU=000" + count;
        }
        else if (count < 1000000)
        {
            sCount = "TU=00" + count;
        }
        else if (count < 10000000)
        {
            sCount = "TU=0" + count;
        }
        else if (count < 100000000)
        {
            sCount = "TU=" + count;
        }
        return sCount;
    }
}
