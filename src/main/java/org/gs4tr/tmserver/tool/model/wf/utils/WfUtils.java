package org.gs4tr.tmserver.tool.model.wf.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.foundation3.io.model.FileStatus;
import org.gs4tr.foundation3.io.utils.FileUtils;
import org.gs4tr.tmserver.tool.model.wf.common.TU;
import org.gs4tr.tmserver.tool.model.wf.common.WordFastDate;

public class WfUtils
{
    // \w includes digits but that's ok in locales like ES-00.
    static private Pattern localeValidationPattern = Pattern.compile("^\\w\\w([-_]\\w\\w)?$");

    static private boolean checkMemoryFormat(String filename) throws IOException
    {
        if (!filename.toLowerCase(Locale.ROOT).endsWith(".txt"))
        {
            return false;
        }

        String encoding = getEncoding(filename);
        String header = readHeader(filename, encoding);
        String[] memFields = header.split("\t");

        // memory header (start by BOM in Unicode)
        // 0 : %20050119~082351\t
        // 1 : %User ID,i internet\t
        // 2 : %TU=00000000\t
        // 3 : %EN-US\t
        // 4 : %Wordfast TM v4.30/00\t
        // 5 : %FR-FR\t
        // 6 : %---89660958\t
        // 7 to 10 : Optional 4 attributes (if not \t\t\t\t)
        // 11 : 60 spaces + dot

        // check number of header fields
        if (memFields.length < 7)
        {
            return false;
        }

        // checking labels
        // bom can start the string
        if (memFields[0].indexOf('%') == -1) // Date
        {
            return false;
        }
        if (!memFields[1].startsWith("%") && !memFields[1].startsWith("\"%")) // User
        {
            return false;
        }
        if (!memFields[2].startsWith("%")) // TU
        {
            return false;
        }
        if (!memFields[3].startsWith("%") || memFields[3].length() < 3 || memFields[3].length() > 6) // src
                                                                                                     // locale
        {
            return false;
        }
        if (!memFields[4].toLowerCase(Locale.ROOT).startsWith("%wordfast")) // Wordfast magic
        {
            return false;
        }
        if (!memFields[5].startsWith("%") || memFields[5].length() < 3 || memFields[5].length() > 6) // trg
                                                                                                     // locale
        {
            return false;
        }
        // if (!memFields[6].startsWith("%"))
        // return false;

        return true;
    }

    static public void fieldCheck(String line) throws Exception
    {
        String[] fields = line.split("\t");
        if (fields.length < 7)
        {
            throw new Exception("too few fields (" + fields.length + "), expected 7-11");
        }

        if (fields.length > 11)
        {
            throw new Exception("too many fields (" + fields.length + "), expected 7-11");
        }

        if (WordFastDate.parse(fields[0]) == null)
        {
            throw new Exception("Invalid date field `" + fields[0] + "'");
        }

        if (StringUtils.isBlank(fields[4]))
        {
            throw new Exception("Empty source segment");
        }
    }

    static public void fieldSanityCheck(TU tu) throws Exception
    {
        if (StringUtils.isEmpty(tu.getCreationUser()) || tu.getCreationUser().length() > 255)
        {
            throw new Exception("invalid creation user");
        }
    }

    static public String getEncoding(String filename) throws IOException
    {
        FileStatus status = FileUtils.getStatus(new File(filename));
        return status.getEncoding().getEncoding();
    }

    static private boolean isValidLocale(String locale)
    {
        return localeValidationPattern.matcher(locale).matches();
    }

    static public void localeCheck(TU tu) throws Exception
    {
        if (StringUtils.isEmpty(tu.getSourceLocale()))
        {
            throw new Exception("empty source locale");
        }

        if (StringUtils.isEmpty(tu.getTargetLocale()))
        {
            throw new Exception("empty target locale");
        }

        if (!isValidLocale(tu.getSourceLocale()))
        {
            throw new Exception("invalid source locale " + tu.getSourceLocale());
        }

        if (!isValidLocale(tu.getTargetLocale()))
        {
            throw new Exception("invalid target locale " + tu.getTargetLocale());
        }
    }

    static public String normalizeHeaderLocale(String locale)
    {
        if (locale.length() > 2 && (locale.endsWith("ZZ") || locale.endsWith("00") || locale.endsWith("01")))
        {
            locale = locale.substring(0, 2);
        }

        return locale;
    }

    private static String readHeader(String sMemoryPath, String sEncoding) throws IOException
    {
        FileInputStream fis = new FileInputStream(sMemoryPath);
        InputStreamReader isr = new InputStreamReader(fis, Charset.forName(sEncoding));
        BufferedReader br = new BufferedReader(isr);

        String result = br.readLine();
        if (result.indexOf("\t\t") > -1)
        {
            result = result.replaceAll("\t\t", "\t \t").replaceAll("\t\t", "\t \t");
        }

        br.close();
        isr.close();
        fis.close();

        return result;
    }

    static public void validateWfHeader(String filename) throws IOException
    {
        if (!checkMemoryFormat(filename))
        {
            throw new IOException("invalid WF header or empty file");
        }
    }
}
