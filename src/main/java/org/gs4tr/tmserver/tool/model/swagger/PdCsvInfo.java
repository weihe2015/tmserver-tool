package org.gs4tr.tmserver.tool.model.swagger;

public class PdCsvInfo
{
    public String tmProfileName;
    public String analysisURL;
    public String analysisUsername;
    public String analysisPassword;
    public String linguistUrl;
    public String linguistUsername;
    public String linguistPassword;
    public String tmName;
    public String sourceLocale;
    public String targetLocale;
    public int updateTm;
    public int addFilename;
    public String paClientName;
    public String organization;
}
