package org.gs4tr.tmserver.tool.model.xml;

public class ProcessingInstruction implements Node
{
    private Element parent;
    private String original;
    private String target;
    private String data;

    public ProcessingInstruction(String original, String target, String data)
    {
        this.original = original;
        this.target = target;
        this.data = data;
    }

    @Override
    public String asXML()
    {
        return original;
    }

    public String getData()
    {
        return data;
    }

    @Override
    public short getNodeType()
    {
        return PROCESSING_INSTRUCTION_NODE;
    }

    @Override
    public Element getParent()
    {
        return parent;
    }

    public String getTarget()
    {
        return target;
    }

    @Override
    public void setParent(Element node)
    {
        parent = node;
    }
}
