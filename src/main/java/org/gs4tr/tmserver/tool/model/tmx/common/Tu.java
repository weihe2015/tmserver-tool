package org.gs4tr.tmserver.tool.model.tmx.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.gs4tr.foundation3.core.utils.UTC;
import org.gs4tr.foundation3.xml.XmlParser;
import org.gs4tr.tmserver.tool.service.tmx.EditUtil;

public class Tu implements TmxConstants
{
    static private transient String EOL = System.getProperty("line.separator");

    private String m_tuid;
    private String m_srclang;
    private String m_datatype;
    private String m_creationdate;
    private String m_creationid;
    private String m_changedate;
    private String m_changeid;
    private String m_usagecount;
    private String m_lastusagedate;
    private String m_segtype;
    private String m_creationtool;
    private String m_creationtoolversion;
    private String m_o_tmf;
    private String m_o_encoding;

    private List<Element> m_props = new ArrayList<Element>();
    private List<Tuv> m_tuvs = new ArrayList<Tuv>();

    public Tu()
    {
    }

    public Tu(Element p_root)
    {
        init(p_root);
    }

    public Tu(String p_arg)
    {
        m_tuid = p_arg;
    }

    public void addProp(Element p_prop)
    {
        m_props.add(p_prop);
    }

    public void addProp(String p_prop)
    {
        Element prop = XmlParser.parseXml(p_prop).getRootElement();
        m_props.add(prop);
    }

    public void addTuv(Tuv p_arg)
    {
        m_tuvs.add(p_arg);
    }

    public void clearProps()
    {
        m_props.clear();
    }

    public void clearTuvs()
    {
        m_tuvs.clear();
    }

    public String getChangeDate()
    {
        return m_changedate;
    }

    public Date getChangeDateAsDate()
    {
        return StringUtils.isNotBlank(m_changedate) ? UTC.parseNoSeparators(m_changedate) : null;
    }

    public String getChangeId()
    {
        return m_changeid;
    }

    public String getCreationDate()
    {
        return m_creationdate;
    }

    public Date getCreationDateAsDate()
    {
        return StringUtils.isNotBlank(m_creationdate) ? UTC.parseNoSeparators(m_creationdate) : null;
    }

    public String getCreationId()
    {
        return m_creationid;
    }

    public String getDataType()
    {
        return m_datatype;
    }

    public String getId()
    {
        return m_tuid;
    }

    public String getLastUsageDate()
    {
        return m_lastusagedate;
    }

    public List<Element> getProps()
    {
        return m_props;
    }

    public String getSrclang()
    {
        return m_srclang;
    }

    public Tuv getTuv(String p_language)
    {
        for (int i = 0, max = m_tuvs.size(); i < max; i++)
        {
            Tuv tuv = m_tuvs.get(i);

            // Unlike the other TMX attributes, the values for srclang are not
            // case-sensitive,
            // but they do follow RFC 3066 syntax: XX-YY.
            // See http://www.lisa.org/fileadmin/standards/tmx1.4/tmx.htm#srclang
            if (tuv.getLanguage().equalsIgnoreCase(p_language))
            {
                return tuv;
            }
        }
        return null;
    }

    public List<Tuv> getTuvs()
    {
        return m_tuvs;
    }

    public String getUsageCount()
    {
        return m_usagecount;
    }

    public String getXml()
    {
        return getXml(true);
    }

    public String getXml(boolean writeXmlLang)
    {
        StringBuffer result = new StringBuffer(128);

        result.append("  <tu");
        if (StringUtils.isNotBlank(m_tuid))
        {
            result.append(" tuid=\"");
            result.append(EditUtil.encodeXmlEntities(m_tuid));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_srclang))
        {
            result.append(" srclang=\"");
            result.append(EditUtil.encodeXmlEntities(m_srclang));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_datatype))
        {
            result.append(" datatype=\"");
            result.append(EditUtil.encodeXmlEntities(m_datatype));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_creationdate))
        {
            result.append(" creationdate=\"");
            result.append(EditUtil.encodeXmlEntities(m_creationdate));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_creationid))
        {
            result.append(" creationid=\"");
            result.append(EditUtil.encodeXmlEntities(m_creationid));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_changedate))
        {
            result.append(" changedate=\"");
            result.append(EditUtil.encodeXmlEntities(m_changedate));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_changeid))
        {
            result.append(" changeid=\"");
            result.append(EditUtil.encodeXmlEntities(m_changeid));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_usagecount))
        {
            result.append(" usagecount=\"");
            result.append(EditUtil.encodeXmlEntities(m_usagecount));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_lastusagedate))
        {
            result.append(" lastusagedate=\"");
            result.append(EditUtil.encodeXmlEntities(m_lastusagedate));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_segtype))
        {
            result.append(" segtype=\"");
            result.append(EditUtil.encodeXmlEntities(m_segtype));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_creationtool))
        {
            result.append(" creationtool=\"");
            result.append(EditUtil.encodeXmlEntities(m_creationtool));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_creationtoolversion))
        {
            result.append(" creationtoolversion=\"");
            result.append(EditUtil.encodeXmlEntities(m_creationtoolversion));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_o_tmf))
        {
            result.append(" o-tmf=\"");
            result.append(EditUtil.encodeXmlEntities(m_o_tmf));
            result.append("\"");
        }
        if (StringUtils.isNotBlank(m_o_encoding))
        {
            result.append(" o-encoding=\"");
            result.append(EditUtil.encodeXmlEntities(m_o_encoding));
            result.append("\"");
        }

        result.append(">").append(EOL);

        for (int i = 0, max = m_props.size(); i < max; ++i)
        {
            Element element = m_props.get(i);
            result.append("    ");
            result.append(element.asXML());
            result.append(EOL);
        }

        for (int i = 0, max = m_tuvs.size(); i < max; ++i)
        {
            result.append(m_tuvs.get(i).getXml(writeXmlLang));
        }

        result.append("  </tu>").append(EOL);

        return result.toString();
    }

    @SuppressWarnings("unchecked")
    private void init(Element p_root)
    {
        clearProps();
        clearTuvs();

        m_tuid = p_root.attributeValue(TUID);
        m_srclang = p_root.attributeValue(SRCLANG);
        m_datatype = p_root.attributeValue(DATATYPE);
        m_creationdate = p_root.attributeValue(CREATIONDATE);
        m_creationid = p_root.attributeValue(CREATIONID);
        m_changedate = p_root.attributeValue(CHANGEDATE);
        m_changeid = p_root.attributeValue(CHANGEID);
        m_usagecount = p_root.attributeValue(USAGECOUNT);
        m_lastusagedate = p_root.attributeValue(LASTUSAGEDATE);
        m_segtype = p_root.attributeValue(SEGTYPE);
        m_creationtool = p_root.attributeValue(CREATIONTOOL);
        m_creationtoolversion = p_root.attributeValue(CREATIONTOOLVERSION);
        m_o_tmf = p_root.attributeValue(O_TMF);
        m_o_encoding = p_root.attributeValue(O_ENCODING);

        for (Iterator<Element> it = p_root.elementIterator(PROP); it.hasNext();)
        {
            Element item = it.next();
            m_props.add(item);
        }

        for (Iterator<Element> it = p_root.elementIterator(TUV); it.hasNext();)
        {
            Element item = it.next();
            Tuv tuv = new Tuv(item);

            addTuv(tuv);
        }
    }

    public void setChangeDate(Date changedate)
    {
        this.m_changedate = UTC.valueOfNoSeparators(changedate);
    }

    public void setChangeDate(String changedate)
    {
        this.m_changedate = changedate;
    }

    public void setChangeId(String changeid)
    {
        this.m_changeid = changeid;
    }

    public void setCreationDate(Date creationdate)
    {
        this.m_creationdate = UTC.valueOfNoSeparators(creationdate);
    }

    public void setCreationDate(String creationdate)
    {
        this.m_creationdate = creationdate;
    }

    public void setCreationId(String creationid)
    {
        this.m_creationid = creationid;
    }

    public void setDataType(String p_dataType)
    {
        m_datatype = p_dataType;
    }

    public void setId(String p_tuid)
    {
        m_tuid = p_tuid;
    }

    public void setLastUsageDate(String lastusagedate)
    {
        this.m_lastusagedate = lastusagedate;
    }

    public void setSrclang(String srclang)
    {
        this.m_srclang = srclang;
    }

    public void setUsageCount(String usagecount)
    {
        this.m_usagecount = usagecount;
    }
}
