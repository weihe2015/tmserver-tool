package org.gs4tr.tmserver.tool.model.api;

public class TmServerInfo
{
    private String _url;
    private String _loginUrl;
    private String _tmUserName;
    private String _tmPassword;
    private String _logoutUrl;
    private String _searchUrl;
    private String _concordanceUrl;
    private String _getTuInfoUrl;
    private String _exportUrl;
    private String _getOrganizationListUrl;
    private String _getGroupListUrl;
    private String _getLanguageListUrl;
    private String _createTmGroupUrl;
    private String _createTmGroupCodeUrl;
    private String _validateTmGroupCodeUrl;
    private String _createTmCleanupPasscodeUrl;
    private String _validateTmCleanupPasscodeUrl;
    private String _createTmShortcodeUrl;
    private String _validateTmShortcodeUrl;
    private String _getGenericRolesUrl;
    private String _generateGenericUsernameUrl;
    private String _generateGenericUserPasswordUrl;
    private String _createTmUrl;
    private String _importTmUrl;
    private String _importTmProgressUrl;
    private String _tmListUrl;
    private String _validateOAuthTokenUrl;
    private String _tmOrganizationCacheKey;
    private String _tmLanguagesCacheKey;
    private boolean _isRemote;

    public TmServerInfo(String url, String tmUserName, String tmPassword)
    {
        this._url = url;
        this._tmUserName = tmUserName;
        this._tmPassword = tmPassword;
        createURLs();
    }

    public TmServerInfo(String url, String tmUserName, String tmPassword, boolean isRemote)
    {
        this._url = url;
        this._tmUserName = tmUserName;
        this._tmPassword = tmPassword;
        this._isRemote = isRemote;
        createURLs();
    }

    public TmServerInfo(String url)
    {
        setUrl(url);
    }

    public TmServerInfo()
    {
    }

    public void setUrl(String url)
    {
        this._url = url;
        createURLs();
    }

    public void createURLs()
    {
        this._loginUrl = this._url + "/service/v2/login";
        this._logoutUrl = this._loginUrl.replace("login", "logout");
        this._searchUrl = this._loginUrl.replace("login", "search");
        this._concordanceUrl = this._loginUrl.replace("login", "concordance");
        this._getTuInfoUrl = this._loginUrl.replace("login", "getTuInfo");
        this._exportUrl = this._loginUrl.replace("login", "export");
        this._getOrganizationListUrl = this._loginUrl.replace("login", "project/getOrganizationList");
        this._getGroupListUrl = this._loginUrl.replace("login", "tm/getProjects");
        this._getLanguageListUrl = this._loginUrl.replace("login", "tm/getLanguages");
        this._createTmGroupUrl = this._loginUrl.replace("login", "project/create");
        this._createTmGroupCodeUrl = this._loginUrl.replace("login",
            "project/createProjectShortCode");
        this._validateTmGroupCodeUrl = this._loginUrl.replace("login", "project/validateProjectShortCode");
        this._createTmCleanupPasscodeUrl = this._loginUrl.replace("login", "tm/createRandomCleanupCode");
        this._validateTmCleanupPasscodeUrl = this._loginUrl.replace("login", "tm/validateCleanupCode");
        this._createTmShortcodeUrl = this._loginUrl.replace("login", "tm/createTmShortCode");
        this._validateTmShortcodeUrl = this._loginUrl.replace("login", "tm/validateTmShortCode");
        this._getGenericRolesUrl = this._loginUrl.replace("login", "tm/getRoles");
        this._generateGenericUsernameUrl = this._loginUrl.replace("login", "tm/createGenericUsername");
        this._generateGenericUserPasswordUrl = this._loginUrl.replace("login", "tm/generateRandomPassword");
        this._createTmUrl = this._loginUrl.replace("login", "tm/create");
        this._importTmUrl = this._loginUrl.replace("login", "import");
        this._importTmProgressUrl = this._loginUrl.replace("login", "getImportProgress");
        this._validateOAuthTokenUrl = this._loginUrl.replace("login", "oauth/validate");
        this._tmListUrl = this._loginUrl.replace("login", "getTmList.tm");
        this._tmOrganizationCacheKey = this._url + "-TmOrganizationList";
        this._tmLanguagesCacheKey = this._url + "-TmLanguagesList";
    }

    public String getConcordanceURL()
    {
        return this._concordanceUrl;
    }

    public String getCreateTmCleanupPasscodeURL()
    {
        return this._createTmCleanupPasscodeUrl;
    }

    public String getCreateTmGroupURL()
    {
        return this._createTmGroupUrl;
    }

    public String getCreatTmGroupCodeURL()
    {
        return this._createTmGroupCodeUrl;
    }

    public String getCreateTmShortcodeURL()
    {
        return this._createTmShortcodeUrl;
    }

    public String getCreateTmURL()
    {
        return this._createTmUrl;
    }

    public String getExportURL()
    {
        return this._exportUrl;
    }

    public String getGenerateGenericUsernameURL()
    {
        return this._generateGenericUsernameUrl;
    }

    public String getGenerateGenericUserPasswordURL()
    {
        return this._generateGenericUserPasswordUrl;
    }

    public String getGenericRolesURL()
    {
        return this._getGenericRolesUrl;
    }

    public String getGroupListURL()
    {
        return this._getGroupListUrl;
    }

    public String getImportTmProgressURL()
    {
        return this._importTmProgressUrl;
    }

    public String getImportTmURL()
    {
        return this._importTmUrl;
    }

    public boolean getIsRemote()
    {
        return this._isRemote;
    }

    public String getLanguageListURL()
    {
        return this._getLanguageListUrl;
    }

    public String getLoginURL()
    {
        return this._loginUrl;
    }

    public String getLogoutURL()
    {
        return this._logoutUrl;
    }

    public String getOrganizationListURL()
    {
        return this._getOrganizationListUrl;
    }

    public String getSearchURL()
    {
        return this._searchUrl;
    }

    public String getTmLanguagesCacheKey()
    {
        return this._tmLanguagesCacheKey;
    }

    public String getTmListUrl()
    {
        return this._tmListUrl;
    }

    public String getTmOrganizationCacheKey()
    {
        return this._tmOrganizationCacheKey;
    }

    public String getTmPassword()
    {
        return this._tmPassword;
    }

    public String getTmUserName()
    {
        return this._tmUserName;
    }

    public String getTuInfoURL()
    {
        return this._getTuInfoUrl;
    }

    public String getURL()
    {
        return this._url;
    }

    public String getValidateTmGroupCodeURL()
    {
        return this._validateTmGroupCodeUrl;
    }

    public String getValidateTmPasscodeURL()
    {
        return this._validateTmCleanupPasscodeUrl;
    }

    public String getValidateTmShortcodeURL()
    {
        return this._validateTmShortcodeUrl;
    }

    public void setIsRemote(boolean isRemote)
    {
        this._isRemote = isRemote;
    }

    public String getValidateOAuthTokenUrl()
    {
        return _validateOAuthTokenUrl;
    }
}
