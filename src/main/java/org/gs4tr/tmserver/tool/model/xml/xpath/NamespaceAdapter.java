package org.gs4tr.tmserver.tool.model.xml.xpath;

import org.gs4tr.tmserver.tool.model.xml.Element;
import org.gs4tr.tmserver.tool.model.xml.Namespace;

/**
 * Wrapper for MyDOM namespace nodes to give them a parent, as required by the XPath data model.
 * <p>
 * In the MyDOM model, namespaces do not have a parent, plus they have been interned so they could
 * never point to different parents.
 */
public class NamespaceAdapter
{
    private Element element;
    private Namespace namespace;

    /**
     * Creates a namespace-node wrapper for a namespace node that is assigned to the given MyDOM
     * element.
     */
    public NamespaceAdapter(Element element, Namespace namespace)
    {
        this.element = element;
        this.namespace = namespace;
    }

    /**
     * Creates a namespace-node wrapper for a namespace node that hasn't been assigned to an element
     * yet.
     */
    public NamespaceAdapter(Namespace namespace)
    {
        this.namespace = namespace;
    }

    /**
     * Returns the MyDOM element from which this namespace node has been retrieved. The result may
     * be null when the namespace node has not yet been assigned to an element.
     */
    public Element getElement()
    {
        return element;
    }

    /**
     * Returns the MyDOM namespace object of this namespace node; the MyDOM namespace object
     * contains the prefix and URI of the namespace.
     */
    public Namespace getNamespace()
    {
        return namespace;
    }

    /**
     * Sets or changes the element to which this namespace node is assigned.
     */
    public void setElement(Element element)
    {
        this.element = element;
    }

    @Override
    public String toString()
    {
        return ("[xmlns:" + namespace.getPrefix() + "=\"" + namespace.getURI() + "\", " + "element=" +
            (element != null ? element.getName() : "(null)") + "]");
    }
}
