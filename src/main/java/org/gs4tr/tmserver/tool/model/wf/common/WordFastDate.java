package org.gs4tr.tmserver.tool.model.wf.common;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Helper class for Wordfast time format "20000216~155600".
 * <p>
 * Note if a private TU is returned from a search within a workspace, the date has the format
 * "20000216!155600" with exclamation as separator.
 */
public class WordFastDate
{
    static public final String FORMAT = "yyyyMMdd'~'HHmmss";

    static public Boolean isPrivateTuDate(String date)
    {
        return isPrivateTuDateInternal(date) ? Boolean.TRUE : Boolean.FALSE;
    }

    static private boolean isPrivateTuDateInternal(String date)
    {
        return date.charAt(8) == '!';
    }

    /**
     * Parses a WordFast date string into a Java Date.
     */
    static public Date parse(String date)
    {
        // Parsing dates is not thread-safe (cannot use single instance)
        SimpleDateFormat formatter = new SimpleDateFormat(FORMAT);

        if (isPrivateTuDateInternal(date))
        {
            date = privateToPublicDate(date);
        }

        ParsePosition pos = new ParsePosition(0);

        return formatter.parse(date, pos);
    }

    static public String privateToPublicDate(String date)
    {
        return date.replace('!', '~');
    }

    /**
     * Converts a Java Date to the Wordfast format.
     */
    static public String valueOf(Date date)
    {
        // Formatting dates may be thread-safe (could use single instance)
        SimpleDateFormat formatter = new SimpleDateFormat(FORMAT);

        return formatter.format(date);
    }

    /** Static class, private constructor. */
    private WordFastDate()
    {
    }
}
