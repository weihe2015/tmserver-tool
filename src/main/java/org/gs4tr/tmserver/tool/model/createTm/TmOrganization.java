package org.gs4tr.tmserver.tool.model.createTm;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmOrganization extends TmGroupOrganization
{

}
