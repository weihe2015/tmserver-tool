package org.gs4tr.tmserver.tool.model.auth;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class NewUserRequest implements Serializable
{
    private static final long serialVersionUID = -2483417814435347101L;

    private String fullname;
    private String username;
    private String password;

    public NewUserRequest()
    {
        super();
    }

    public NewUserRequest(String requestBody) throws JSONException
    {
        JSONObject obj = new JSONObject(requestBody);
        this.fullname = obj.getString("fullname");
        this.username = obj.getString("username");
        this.password = obj.getString("password");
    }

    public String getFullName()
    {
        return this.fullname;
    }

    public void getFullName(String fullname)
    {
        this.fullname = fullname;
    }

    public String getPassword()
    {
        return this.password;
    }

    public String getUsername()
    {
        return this.username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }
}
