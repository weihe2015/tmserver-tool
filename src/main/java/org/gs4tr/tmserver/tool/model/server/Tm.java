package org.gs4tr.tmserver.tool.model.server;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TM_TM")
public class Tm
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TM_ID", nullable = false)
    private Long id;

    @Column(name = "CONTEXT_MODE_ENABLED", nullable = true)
    private boolean _contextModeEnabled;

    @Column(name = "CREATION_DATE", nullable = true)
    private Date _creationDate;

    @Column(name = "CREATION_USER", nullable = true)
    private String _creationUser;

    @Column(name = "DATE_LAST_REMOTE_LOGIN", nullable = true)
    private Date _dateLastRemoteLogin;

    @Column(name = "DELETING", nullable = true)
    private boolean _deleting;

    @Column(name = "DISABLE_DATE", nullable = true)
    private Date _disabledDate;

    @Column(name = "DISABLE_USER", nullable = true)
    private String _disableUser;

    @Column(name = "ENABLED", nullable = true)
    private boolean _enabled;

    @Column(name = "name", nullable = true)
    private String _name;

    @Column(name = "PROJECT_SHORT_CODE", nullable = true)
    private String projectShortCode;

    @Column(name = "SHORT_CODE", nullable = true)
    private String _shortCode;

    @Column(name = "SOURCE_LOCALE", nullable = false)
    private String _sourceLocale;

    @Column(name = "TARGET_LOCALE", nullable = false)
    private String _targetLocale;

    @Column(name = "TM_COLLECTION_ID", nullable = true)
    private int _tmCollectionID;

    public Tm()
    {

    }

    public String getName()
    {
        return this._name;
    }

    public String getSourceLocale()
    {
        return this._sourceLocale;
    }

    public String getTargetLocale()
    {
        return this._targetLocale;
    }

    public void setName(String name)
    {
        this._name = name;
    }

}
