package org.gs4tr.tmserver.tool.model.xml.parser;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.model.xml.Attribute;
import org.gs4tr.tmserver.tool.model.xml.AttributeValue;
import org.gs4tr.tmserver.tool.model.xml.CDATA;
import org.gs4tr.tmserver.tool.model.xml.Comment;
import org.gs4tr.tmserver.tool.model.xml.Document;
import org.gs4tr.tmserver.tool.model.xml.DocumentDeclaration;
import org.gs4tr.tmserver.tool.model.xml.Element;
import org.gs4tr.tmserver.tool.model.xml.Entity;
import org.gs4tr.tmserver.tool.model.xml.Namespace;
import org.gs4tr.tmserver.tool.model.xml.ProcessingInstruction;
import org.gs4tr.tmserver.tool.model.xml.QName;
import org.gs4tr.tmserver.tool.model.xml.Text;
import org.gs4tr.tmserver.tool.model.xml.XmlDeclaration;

public class Parser
{
    static public final String XML_URI = "http://www.w3.org/XML/1998/namespace";
    static public final String XMLNS_URI = "http://www.w3.org/2000/xmlns/";

    static private final Pattern s_piPattern = Pattern.compile("^<\\?\\s*(\\w+)(\\s+(.*?))?\\?>$",
        Pattern.DOTALL | Pattern.UNICODE_CHARACTER_CLASS);

    static private final char EOF = (char) -1;

    // nameStart / name lookup tables based on XML 1.1
    // http://www.w3.org/TR/2001/WD-xml11-20011213/
    static private final int LOOKUP_MAX = 0x400;
    static private final char LOOKUP_MAX_CHAR = (char) LOOKUP_MAX;
    static private boolean lookupNameChar[] = new boolean[LOOKUP_MAX];

    static
    {
        // CvdL: colon is parsed explicitly
        // setName/*Start*/(':');
        for (char ch = 'A'; ch <= 'Z'; ++ch)
        {
            setName/* Start */(ch);
        }
        setName/* Start */('_');
        for (char ch = 'a'; ch <= 'z'; ++ch)
        {
            setName/* Start */(ch);
        }
        for (char ch = '\u00c0'; ch <= '\u02FF'; ++ch)
        {
            setName/* Start */(ch);
        }
        for (char ch = '\u0370'; ch <= '\u037d'; ++ch)
        {
            setName/* Start */(ch);
        }
        for (char ch = '\u037f'; ch < '\u0400'; ++ch)
        {
            setName/* Start */(ch);
        }

        setName('-');
        setName('.');
        for (char ch = '0'; ch <= '9'; ++ch)
        {
            setName(ch);
        }
        setName('\u00b7'); // Middle dot !?!
        for (char ch = '\u0300'; ch <= '\u036f'; ++ch)
        {
            setName(ch);
        }
    }

    static private final void setName(char ch)
    {
        lookupNameChar[ch] = true;
    }

    private Reader reader;
    private boolean readerEOF = false;
    private int lineNumber = 1;
    private int columnNumber = 0;
    private Document document;
    private Stack<Element> elementStack = new Stack<>();

    public Parser()
    {
    }

    /**
     * Reads optional spaces.
     * 
     * @return the next char.
     */
    private char allowSpace(char ch, StringBuffer orig)
    {
        while (isS(ch))
        {
            orig.append(ch);
            ch = more();
        }
        return ch;
    }

    private String decodeEntity(String text)
    {
        if (text.equals("amp"))
        {
            return "&";
        }
        if (text.equals("apos"))
        {
            return "'";
        }
        if (text.equals("gt"))
        {
            return ">";
        }
        if (text.equals("lt"))
        {
            return "<";
        }
        if (text.equals("quot"))
        {
            return "\"";
        }

        if (text.charAt(0) == '#')
        {
            int offset = 1;
            int base = 10;
            if (text.charAt(1) == 'x')
            {
                offset = 2;
                base = 16;
            }

            return String.valueOf((char) Integer.parseInt(text.substring(offset), base));
        }

        return null;
    }

    private void error(String msg)
    {
        throw new RuntimeException("Error @" + lineNumber + ":" + columnNumber + ": " + msg);
    }

    /**
     * Finds a parent element that declares a namespace &lt;foo xmlns="uri"/> and returns it. The
     * namespace can be the default NS (prefix = null) or a named one (prefix != null).
     */
    private Namespace findNamespaceInElementStack(String prefix)
    {
        if ("xmlns".equals(prefix))
        {
            return Namespace.XMLNS_NAMESPACE;
        }
        else if ("xml".equals(prefix))
        {
            return Namespace.XML_NAMESPACE;
        }

        for (int i = elementStack.size() - 1; i >= 0; --i)
        {
            Element element = elementStack.get(i);

            List<Namespace> declaredNS = element.declaredNamespaces();
            for (int j = 0, maxj = declaredNS.size(); j < maxj; j++)
            {
                Namespace ns = declaredNS.get(j);
                if (prefix.equals(ns.getPrefix()))
                {
                    return ns;
                }
            }
        }

        return Namespace.NO_NAMESPACE;
    }

    private Element getCurrentElement()
    {
        return !elementStack.isEmpty() ? elementStack.peek() : null;
    }

    private boolean isS(char ch)
    {
        return (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t') ||
            (/* supportXml11 && */ (ch == '\u0085' || ch == '\u2028'));
    }

    private boolean isXMLNameChar(char ch)
    {
        return (ch < LOOKUP_MAX_CHAR && lookupNameChar[ch]) || (ch >= LOOKUP_MAX_CHAR && ch <= '\u2027') ||
            (ch >= '\u202A' && ch <= '\u218F') || (ch >= '\u2800' && ch <= '\uFFEF');
    }

    private char more()
    {
        if (readerEOF)
        {
            throw new RuntimeException("EOF");
        }

        try
        {
            char ch = (char) reader.read();
            if (ch == EOF)
            {
                readerEOF = true;
            }

            if (ch == '\n')
            {
                ++lineNumber;
                columnNumber = 1;
            }
            else
            {
                ++columnNumber;
            }

            // System.out.print(printable(ch));System.out.flush();
            return ch;
        }
        catch (IOException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public Document parse(Reader reader)
    {
        reset();
        this.reader = reader;
        document = new Document();

        // [1] document ::= prolog element Misc*
        // [2] prolog: ::= XMLDecl? Misc* (doctypedecl Misc*)? and look for [39] element
        char ch = parseProlog();

        if (ch == EOF)
        {
            error("invalid XML");
        }

        ch = parseElement(ch);

        // epilog: Misc*
        parseEpilog(ch);

        return document;
    }

    /**
     * Parses an XML attribute.
     * 
     * @return the next char.
     */
    private char parseAttributes(char ch, StringBuffer tagEnd)
    {
        // ASSUMPTION: seen one char (/ or > or start of attr name)

        while (ch != EOF)
        {
            ch = allowSpace(ch, tagEnd);

            // Assume it was the tag end!
            if (ch == '/' || ch == '>')
            {
                if (ch == '/')
                {
                    tagEnd.append(ch);
                    ch = more();
                    if (ch != '>')
                    {
                        error("invalid XML");
                    }
                }
                tagEnd.append(ch);
                return more();
            }

            // No, it was space between element name and attribute name.
            if (tagEnd.length() > 0)
            {
                getCurrentElement().addStartTag(new Text(tagEnd.toString()));
                tagEnd.setLength(0);
            }

            StringBuffer sbName = new StringBuffer();
            StringBuffer sbLocalName = new StringBuffer();
            String name = null, prefix = "", localName = null;

            while (isXMLNameChar(ch))
            {
                sbName.append(ch);
                ch = more();
            }

            if (ch == ':')
            {
                prefix = sbName.toString();
                sbName.append(ch);
                ch = more();
                while (isXMLNameChar(ch))
                {
                    sbName.append(ch);
                    sbLocalName.append(ch);
                    ch = more();
                }
                localName = sbLocalName.toString();
            }

            name = sbName.toString();
            if (localName == null)
            {
                localName = name;
            }

            if (name.isEmpty() || localName.isEmpty())
            {
                error("invalid XML");
            }

            // Now we have a qname, a prefix and local name.
            QName qname = QName.get(prefix, localName, name, /* Namespace.NO_NAMESPACE */null);
            Attribute attr = new Attribute(qname);

            attr.add(new Text(sbName.toString()));

            ch = allowSpace(ch, tagEnd);
            ch = requireInputTo(ch, "=", tagEnd);
            ch = allowSpace(ch, tagEnd);

            if (ch != '\'' && ch != '"')
            {
                error("invalid XML");
            }

            char quote = ch;
            attr.setQuote(quote);
            tagEnd.append(quote);
            attr.add(new Text(tagEnd.toString()));
            tagEnd.setLength(0);

            // Read the attribute value as list of text and entities.
            AttributeValue attrValue = new AttributeValue();
            StringBuffer value = new StringBuffer();
            ch = more();
            while (ch != EOF && ch != quote)
            {
                if (ch == '&' && value.length() > 0)
                {
                    attrValue.add(new Text(value.toString()));
                    value.setLength(0);
                }

                if (ch == '&')
                {
                    ch = parseEntity(ch, attrValue);
                    continue;
                }
                else
                {
                    value.append(ch);
                }

                ch = more();
            }

            if (value.length() > 0)
            {
                attrValue.add(new Text(value.toString()));
                value.setLength(0);
            }

            attr.add(attrValue);
            attr.add(new Text(String.valueOf(quote)));
            getCurrentElement().addAttribute(attr);

            ch = more();
        }

        return ch;
    }

    /**
     * Parses an CDATA section.
     * 
     * @return the next char.
     */
    private char parseCDATA()
    {
        // ASSUMPTION: seen <![
        StringBuffer orig = new StringBuffer("<![");

        char ch = more();
        ch = requireInputTo(ch, "CDATA[", orig);
        ch = requireInputToCDATAEnd(ch, /* "]]>", */ orig);

        String text = orig.substring("<![CDATA[".length(), orig.length() - "]]>".length());

        CDATA result = new CDATA(text);
        getCurrentElement().add(result);

        return ch;
    }

    /**
     * Parses an XML comment.
     * 
     * @return the next char.
     */
    private char parseComment()
    {
        // ASSUMPTION: seen <!-
        StringBuffer orig = new StringBuffer("<!-");

        char ch = more();
        ch = requireInputToCommentEnd(ch, /* "-->", */ orig);

        String comment = orig.substring("<!--".length(), orig.length() - "-->".length());

        Comment result = new Comment(orig.toString(), comment);

        Element current = getCurrentElement();
        if (current != null)
        {
            current.add(result);
        }
        else
        {
            document.add(result);
        }

        return ch;
    }

    private char parseDocDecl()
    {
        // [28] doctypedecl ::= '<!DOCTYPE' S Name (S ExternalID)? S? ('['
        // (markupdecl | DeclSep)* ']' S?)? '>'

        // ASSUMPTION: seen <!D
        StringBuffer orig = new StringBuffer("<!");
        char ch = requireInput('D', "DOCTYPE", orig);

        orig.append(ch);

        int bracketLevel = 0;
        while (true)
        {
            ch = more();
            orig.append(ch);

            if (ch == '[')
            {
                ++bracketLevel;
            }
            if (ch == ']')
            {
                --bracketLevel;
            }
            if (ch == '>' && bracketLevel == 0)
            {
                break;
            }
        }

        DocumentDeclaration result = new DocumentDeclaration(orig.toString());
        document.add(result);

        return more();
    }

    /**
     * Parses an XML element with content.
     * 
     * @return the next char.
     */
    private char parseElement(char ch)
    {
        // ASSUMPTION: seen < and one character.

        StringBuffer sbName = new StringBuffer();
        StringBuffer sbLocalName = new StringBuffer();
        String name = null, prefix = "", localName = null;

        while (isXMLNameChar(ch))
        {
            sbName.append(ch);
            ch = more();
        }

        if (ch == ':')
        {
            prefix = sbName.toString();
            sbName.append(ch);
            ch = more();
            while (isXMLNameChar(ch))
            {
                sbName.append(ch);
                sbLocalName.append(ch);
                ch = more();
            }
            localName = sbLocalName.toString();
        }

        name = sbName.toString();
        if (localName == null)
        {
            localName = name;
        }

        if (name.isEmpty() || localName.isEmpty())
        {
            error("invalid XML");
        }

        // Now we have a qname, a prefix and local name.
        // Assign temporary qname, will update after reading attributes.
        QName qname = QName.get(prefix, localName, name, /* Namespace.NO_NAMESPACE */null);
        Element result = new Element(qname);
        result.setDocument(document);

        result.addStartTag(new Text("<" + sbName.toString()));

        StringBuffer spaces = new StringBuffer();
        ch = allowSpace(ch, spaces);
        if (spaces.length() > 0)
        {
            result.addStartTag(new Text(spaces.toString()));
        }

        // Attach the element and push it on the stack.
        Element current = getCurrentElement();
        if (current != null)
        {
            current.add(result);
        }
        else
        {
            document.add(result);
        }

        elementStack.push(result);

        // Parse attributes and collect the tag end.
        StringBuffer tagEnd = new StringBuffer();
        ch = parseAttributes(ch, tagEnd);

        // After attributes: resolve namespaces, fix QName.
        resolveNamespaces(result);

        // Self-closing empty tag? Pop the element and return.
        result.addStartTag(new Text(tagEnd.toString()));
        if (tagEnd.toString().endsWith("/>"))
        {
            elementStack.pop();
            return ch;
        }

        // Read element content.
        StringBuffer text = new StringBuffer();
        while (ch != EOF)
        {
            if ((ch == '<' || ch == '&') && text.length() != 0)
            {
                getCurrentElement().add(new Text(text.toString()));
                text.setLength(0);
            }

            if (ch == '<')
            {
                ch = more();

                if (ch == '?')
                {
                    ch = parsePI();
                }
                else if (ch == '!')
                {
                    ch = more();

                    if (ch == '-')
                    {
                        ch = parseComment();
                    }
                    else if (ch == '[')
                    {
                        ch = parseCDATA();
                    }
                    else
                    {
                        error("invalid XML");
                    }
                }
                else if (ch == '/')
                {
                    ch = parseEndElement(ch);

                    elementStack.pop();
                    break;
                }
                else
                {
                    ch = parseElement(ch);
                }
            }
            else if (ch == '&')
            {
                ch = parseEntity(ch, null);
            }
            else
            {
                text.append(ch);
                ch = more();
            }
        }

        return ch;
    }

    /**
     * Parses an XML element end tag.
     * 
     * @return the next char.
     */
    private char parseEndElement(char ch)
    {
        // ASSUMPTION: seen </
        StringBuffer orig = new StringBuffer("</");

        ch = more();
        while (isXMLNameChar(ch))
        {
            orig.append(ch);
            ch = more();
        }

        if (ch == ':')
        {
            orig.append(ch);
            ch = more();
            while (isXMLNameChar(ch))
            {
                orig.append(ch);
                ch = more();
            }
        }

        ch = allowSpace(ch, orig);
        ch = requireInput(ch, ">", orig);

        Element current = getCurrentElement();
        current.addEndTag(new Text(orig.toString()));

        return ch;
    }

    /**
     * Parses an XML entity reference.
     * 
     * @return the next char.
     */
    private char parseEntity(char ch, AttributeValue val)
    {
        // ASSUMPTION: seen &
        StringBuffer orig = new StringBuffer("&");
        StringBuffer token = new StringBuffer();

        ch = requireInputTo(';', orig, token);

        String value = decodeEntity(token.toString());
        Entity result = new Entity(/* orig.toString() */token.toString(), value);

        if (val != null)
        {
            val.add(result);
        }
        else
        {
            getCurrentElement().add(result);
        }

        return ch;
    }

    private void parseEpilog(char ch)
    {
        // epilog: Misc*
        // [27] Misc ::= Comment | PI | S

        // ASSUMPTION: seen next char after </root>

        while (ch != EOF)
        {
            if (ch == '<')
            {
                ch = more();

                if (ch == '?')
                {
                    ch = parsePI();
                }
                else if (ch == '!')
                {
                    ch = more();

                    if (ch == '-')
                    {
                        ch = parseComment();
                    }
                    else
                    {
                        error("invalid XML");
                    }
                }
                else
                {
                    error("invalid XML");
                }
            }
            else if (isS(ch))
            {
                StringBuffer orig = new StringBuffer();
                ch = allowSpace(ch, orig);
                document.add(new Text(orig.toString()));
            }
            else
            {
                error("invalid XML, char=" + printable(ch));
            }
        }
    }

    /**
     * Parses an XML processing instruction.
     * 
     * @return the next char.
     */
    private char parsePI()
    {
        // [16] PI ::= '<?' PITarget (S (Char* - (Char* '?>' Char*)))? '?>'
        // [17] PITarget ::= Name - (('X' | 'x') ('M' | 'm') ('L' | 'l'))

        // ASSUMPTION: seen <?
        StringBuffer orig = new StringBuffer("<?");

        char ch = more();
        ch = requireInputTo(ch, "?>", orig);

        String original = orig.toString();
        String target = "";
        String data = "";
        Matcher matcher = s_piPattern.matcher(original);
        if (matcher.matches())
        {
            target = matcher.group(1);
            data = matcher.group(2);
        }

        ProcessingInstruction result = new ProcessingInstruction(original, target, data);

        Element current = getCurrentElement();
        if (current != null)
        {
            current.add(result);
        }
        else
        {
            document.add(result);
        }

        return ch;
    }

    private char parseProlog()
    {
        // [2] prolog: ::= XMLDecl? Misc* (doctypedecl Misc*)? and look for [39] element
        char ch = more();
        // Read BOM.
        if (ch == '\uFEFF')
        {
            document.setHasBOM(true);
            ch = more();
        }

        // [27] Misc ::= Comment | PI | S
        while (ch != EOF)
        {
            if (ch == '<')
            {
                ch = more();

                if (ch == '?')
                {
                    if (document.getXmlDecl() == null)
                    {
                        ch = parseXmlDecl();
                    }
                    else
                    {
                        ch = parsePI();
                    }
                }
                else if (ch == '!')
                {
                    ch = more();

                    if (ch == '-')
                    {
                        ch = parseComment();
                    }
                    else if (ch == 'D')
                    {
                        ch = parseDocDecl();
                    }
                    else
                    {
                        error("invalid XML");
                    }
                }
                else if (ch == '/')
                {
                    // Haven't read elements yet!
                    error("invalid XML");
                }
                else
                {
                    // The root element, switch to parsing the element tree.
                    return ch;
                }
            }
            else if (isS(ch))
            {
                StringBuffer orig = new StringBuffer();
                ch = allowSpace(ch, orig);
                document.add(new Text(orig.toString()));
            }
            else
            {
                error("invalid XML, char=" + printable(ch));
            }
        }

        return ch;
    }

    private char parseXmlDecl()
    {
        // [16] PI ::= '<?' PITarget (S (Char* - (Char* '?>' Char*)))? '?>'
        // [17] PITarget ::= Name - (('X' | 'x') ('M' | 'm') ('L' | 'l'))

        // Have read "<?".
        StringBuffer orig = new StringBuffer("<");

        char ch;
        ch = requireInput('?', "?xml", orig);
        ch = requireSpace(ch, orig);
        ch = requireInput(ch, "version", orig);
        ch = allowSpace(ch, orig);
        ch = requireInput(ch, "=", orig);
        ch = allowSpace(ch, orig);
        if (ch != '\'' && ch != '"')
        {
            error("XML does not start with XML declaration");
        }

        StringBuffer version = new StringBuffer();
        StringBuffer encoding = new StringBuffer();
        StringBuffer standalone = new StringBuffer();

        // Version
        orig.append(ch);
        ch = requireInputTo(ch, orig, version);
        ch = allowSpace(ch, orig);

        // Encoding
        if (ch == 'e')
        {
            ch = requireInput(ch, "encoding", orig);
            ch = allowSpace(ch, orig);
            ch = requireInput(ch, "=", orig);
            ch = allowSpace(ch, orig);
            if (ch != '\'' && ch != '"')
            {
                error("XML does not start with XML declaration");
            }

            orig.append(ch);
            ch = requireInputTo(ch, orig, encoding);
            ch = allowSpace(ch, orig);
        }

        // Standalone
        if (ch == 's')
        {
            ch = requireInput(ch, "standalone", orig);
            ch = allowSpace(ch, orig);
            ch = requireInput(ch, "=", orig);
            ch = allowSpace(ch, orig);
            if (ch != '\'' && ch != '"')
            {
                error("XML does not start with XML declaration");
            }

            orig.append(ch);
            ch = requireInputTo(ch, orig, encoding);
            ch = allowSpace(ch, orig);
        }

        ch = requireInput(ch, "?>", orig);

        XmlDeclaration result = new XmlDeclaration(orig.toString());
        result.setVersion(version.toString());
        result.setEncoding(encoding != null ? encoding.toString() : null);
        result.setStandalone(standalone != null ? Boolean.valueOf(standalone.toString()) : null);
        document.add(result);

        return ch;
    }

    private String printable(char ch)
    {
        if (ch == '\n')
        {
            return "\\n";
        }
        else if (ch == '\r')
        {
            return "\\r";
        }
        else if (ch == '\t')
        {
            return "\\t";
        }
        if (ch > 127 || ch < 32)
        {
            return "\\u" + Integer.toHexString(ch);
        }
        return String.valueOf(ch);
    }

    /**
     * Reads an expected string and collects input into original.
     * 
     * @return the next char.
     */
    private char requireInput(char ch, String input, StringBuffer orig)
    {
        for (int i = 0; i < input.length(); i++)
        {
            if (ch != input.charAt(i))
            {
                error(
                    "expected " + printable(input.charAt(i)) + " in " + input + " and not " + printable(ch));
            }
            orig.append(ch);
            ch = more();
        }
        return ch;
    }

    /**
     * Reads up to a known String (with non-repeating chars) and collects input into original.
     * 
     * @return the next char.
     */
    private char requireInputTo(char ch, String to, StringBuffer orig)
    {
        int matched = 0;
        final int matchedMax = to.length();

        while (matched < matchedMax)
        {
            if (ch == to.charAt(matched))
            {
                ++matched;
            }
            else
            {
                // TODO: this is wrong. Must lock back for partial matches.
                // Example: to=]]> input=]]]>. See requireInputToCDATAEnd().
                matched = 0;
            }

            orig.append(ch);
            ch = more();
        }

        return ch;
    }

    /**
     * Reads up to a known char (a closing quote, an equal sign) and collects input into original.
     * 
     * @return the next char.
     */
    private char requireInputTo(char to, StringBuffer orig, StringBuffer token)
    {
        char ch = more();
        while (ch != to)
        {
            orig.append(ch);
            token.append(ch);
            ch = more();
        }
        orig.append(ch);
        ch = more();
        return ch;
    }

    /**
     * Reads up to "]]>" and collects input into original.
     * 
     * @return the next char.
     */
    private char requireInputToCDATAEnd(char ch, StringBuffer orig)
    {
        // http://stackoverflow.com/questions/15151628/string-match-in-sliding-window.
        int state = 0;

        while (true)
        {
            if (ch == ']')
            {
                if (state == 0 || state == 1)
                {
                    ++state;
                }
            }
            else if (ch == '>')
            {
                if (state == 2)
                {
                    orig.append(ch);
                    return more();
                }
            }
            else
            {
                state = 0;
            }

            orig.append(ch);
            ch = more();
        }
    }

    /**
     * Reads up to "-->" and collects input into original.
     * 
     * @return the next char.
     */
    private char requireInputToCommentEnd(char ch, StringBuffer orig)
    {
        // http://stackoverflow.com/questions/15151628/string-match-in-sliding-window.
        int state = 0;

        while (true)
        {
            if (ch == '-')
            {
                if (state == 0 || state == 1)
                {
                    ++state;
                }
            }
            else if (ch == '>')
            {
                if (state == 2)
                {
                    orig.append(ch);
                    return more();
                }
            }
            else
            {
                state = 0;
            }

            orig.append(ch);
            ch = more();
        }
    }

    private char requireSpace(char ch, StringBuffer orig)
    {
        if (!isS(ch))
        {
            error("expected space");
        }

        while (isS(ch))
        {
            orig.append(ch);
            ch = more();
        }
        return ch;
    }

    private void reset()
    {
        readerEOF = false;
        lineNumber = 1;
        columnNumber = 0;
        document = null;
        elementStack.clear();
    }

    /**
     * Reads the namespaces declared in the element's attribute values and stores them in the
     * element's declaredNamespaces field.
     */
    private void resolveDeclaredNamespaces(Element element)
    {
        List<Namespace> declaredNS = new ArrayList<>();

        List<Attribute> attrs = element.attributes();
        for (int i = 0, max = attrs.size(); i < max; i++)
        {
            Attribute attr = attrs.get(i);
            QName qname = attr.getName();

            if (StringUtils.isEmpty(qname.getPrefix()) && qname.getName().equals("xmlns"))
            {
                declaredNS.add(Namespace.getNamespace("", attr.getValue().getText()));
            }
            else if (qname.getPrefix().equals("xmlns"))
            {
                declaredNS.add(Namespace.getNamespace(qname.getName(), attr.getValue().getText()));
            }
        }

        // Store the declared namespaces on the element.
        element.setDeclaredNamespaces(declaredNS);
    }

    // private String printable(String s)
    // {
    // if (s == null) return null;
    //
    // StringBuffer sb = new StringBuffer();
    // for (int i = 0, max = s.length(); i < max; ++i)
    // {
    // sb.append(printable(s.charAt(i)));
    // }
    //
    // return sb.toString();
    // }

    private void resolveNamespaces(Element element)
    {
        // First collect what the element declares:
        // <elem xmlns="uri" xmlns:ns="uri"/>
        // <ns:elem xmlns="uri" xmlns:ns="uri"/>
        resolveDeclaredNamespaces(element);

        // Now resolve what the element's QName really is.
        QName qname = element.getName();
        Namespace ns = findNamespaceInElementStack(qname.getPrefix());
        element.setQName(QName.get(qname.getPrefix(), qname.getName(), qname.getQualifiedName(), ns));

        // Resolve what the element's attribute's QName really is.
        List<Attribute> attrs = element.attributes();
        for (int i = 0, max = attrs.size(); i < max; i++)
        {
            Attribute attr = attrs.get(i);
            qname = attr.getName();
            // Attributes without prefix are not in default namespace but in no_namespace.
            // Sigh.
            ns = Namespace.NO_NAMESPACE;
            if (!qname.getPrefix().isEmpty())
            {
                ns = findNamespaceInElementStack(qname.getPrefix());
            }
            attr.setQName(QName.get(qname.getPrefix(), qname.getName(), qname.getQualifiedName(), ns));
        }
    }
}
