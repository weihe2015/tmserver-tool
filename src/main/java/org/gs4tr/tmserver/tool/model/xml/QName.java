package org.gs4tr.tmserver.tool.model.xml;

public class QName
{
    static private final QNameCache cache = new QNameCache();

    static public QName get(String prefix, String name, String qualifiedName, Namespace ns)
    {
        if (ns == null)
        {
            return new QName(prefix, name, qualifiedName, ns);
        }
        return cache.get(prefix, name, qualifiedName, ns);
    }

    /** The prefix of the element or attribute QName. */
    private String prefix;

    /** The local name of the element or attribute. */
    private String name;

    /** The qualified name of the element or attribute (prefix:name). */
    private String qualifiedName;

    /** The Namespace associated with this element or attribute. */
    private Namespace namespace;

    /** A cached version of the hashcode for efficiency. */
    private int hashCode;

    /** Private constructor. */
    /* package private */ QName(String prefix, String name, String qualifiedName, Namespace ns)
    {
        this.prefix = prefix;
        this.name = name;
        this.qualifiedName = qualifiedName;
        this.namespace = (ns == null) ? Namespace.NO_NAMESPACE : ns;
    }

    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if (object instanceof QName)
        {
            QName that = (QName) object;

            // We cache hash codes so this should be quick.
            if (hashCode() == that.hashCode())
            {
                return getName().equals(that.getName()) && getNamespace().equals(that.getNamespace());
            }
        }

        return false;
    }

    public String getName()
    {
        return name;
    }

    public Namespace getNamespace()
    {
        return namespace;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public String getQualifiedName()
    {
        return qualifiedName;
    }

    @Override
    public int hashCode()
    {
        if (hashCode == 0)
        {
            hashCode = getName().hashCode() ^ getNamespace().hashCode();

            if (hashCode == 0)
            {
                hashCode = 0xbabe;
            }
        }

        return hashCode;
    }
}
