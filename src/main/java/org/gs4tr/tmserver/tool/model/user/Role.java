package org.gs4tr.tmserver.tool.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

@Entity
@Table(name = "TM_ROLE")
public class Role
{
    @Id
    @Column(name = "ROLE_ID", length = 50)
    @OrderColumn(name = "IDX_USER_ROLE_ROLE")
    private String roleId;

    @Column(name = "ROLE_TYPE", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private RoleTypeEnum roleType;

    @Column(name = "GENERIC")
    private Boolean generic;

    public Role()
    {
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (!(obj instanceof Role))
        {
            return false;
        }
        final Role other = (Role) obj;
        if (!getRoleId().equals(other.getRoleId()))
        {
            return false;
        }

        return true;
    }

    public String getRoleId()
    {
        return roleId;
    }

    public RoleTypeEnum getRoleType()
    {
        return roleType;
    }

    public Boolean isGeneric()
    {
        if (generic == null)
        {
            generic = Boolean.FALSE;
        }

        return generic;
    }

    public void setGeneric(Boolean generic)
    {
        this.generic = generic;
    }

    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }

    public void setRoleType(RoleTypeEnum roleType)
    {
        this.roleType = roleType;
    }
}
