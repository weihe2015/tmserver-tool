package org.gs4tr.tmserver.tool.model.xml;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * <p>
 * <code>QNameCache</code> caches instances of <code>QName</code> for reuse both across documents
 * and within documents.
 */
public class QNameCache
{
    /** Cache of {@link QName} instances with no namespace. */
    private Map<String, QName> noNamespaceCache = Collections.synchronizedMap(
        new WeakHashMap<String, QName>());

    /**
     * Cache of {@link Map} instances indexed by namespace which contain caches of {@link QName} for
     * each name.
     */
    private Map<Namespace, Map<String, QName>> namespaceCache = Collections.synchronizedMap(
        new WeakHashMap<Namespace, Map<String, QName>>());

    /* package private */ QNameCache()
    {
    }

    /**
     * An internal factory method for namespace maps.
     *
     * @return a newly created {@link Map} instance.
     */
    protected Map<String, QName> createMap()
    {
        return Collections.synchronizedMap(new HashMap<String, QName>());
    }

    @SuppressWarnings("unused")
    private void debug()
    {
        System.err.println("vvvvvvvvvvvvvv");
        for (String key : noNamespaceCache.keySet())
        {
            System.err.println(key + " -> " + noNamespaceCache.get(key).getQualifiedName() + " (" +
                noNamespaceCache.get(key).getPrefix() + ":" + noNamespaceCache.get(key).getName() + ")");
        }
        System.err.println("--------------");
        for (Namespace key : namespaceCache.keySet())
        {
            System.err.println("Namespace " + key.getPrefix() + ":" + key.getURI() + " -> ");
            Map<String, QName> map = namespaceCache.get(key);
            for (String key2 : map.keySet())
            {
                System.err.println("  " + key2 + " -> " + map.get(key2).getQualifiedName() + " (" +
                    map.get(key2).getPrefix() + ":" + map.get(key2).getName() + ")");
            }
        }
        System.err.println("^^^^^^^^^^^^^^");
    }

    /**
     * !
     *
     * @return the QName for the given local name, qualified name and namepsace
     */
    public QName get(String prefix, String localName, String qName, Namespace namespace)
    {
        Map<String, QName> cache = getNamespaceCache(namespace);

        QName result = cache.get(localName);

        if (result == null)
        {
            result = new QName(prefix, localName, qName, namespace);
            cache.put(localName, result);
        }

        // debug();

        return result;
    }

    /**
     * @return the cache for the given namespace. If one does not currently exist it is created.
     */
    private Map<String, QName> getNamespaceCache(Namespace namespace)
    {
        if (namespace == Namespace.NO_NAMESPACE)
        {
            return noNamespaceCache;
        }

        Map<String, QName> result = namespaceCache.get(namespace);

        if (result == null)
        {
            result = createMap();
            namespaceCache.put(namespace, result);
        }

        return result;
    }

    /**
     * @return the cached QName instance if there is one, or adds the given qname to the cache and
     *         returns that.
     */
    public QName intern(QName qname)
    {
        return get(qname.getPrefix(), qname.getName(), qname.getQualifiedName(), qname.getNamespace());
    }
}
