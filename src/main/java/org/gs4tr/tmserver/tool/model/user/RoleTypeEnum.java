package org.gs4tr.tmserver.tool.model.user;

public enum RoleTypeEnum
{
	CONTEXT, SYSTEM, WORKFLOW
}
