package org.gs4tr.tmserver.tool.model.auth;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class ResetUserPasswordRequest implements Serializable
{
    private static final long serialVersionUID = -3433752794954488637L;

    private String username;
    private String oldPassword;
    private String newPassword1;
    private String newPassword2;

    public ResetUserPasswordRequest()
    {
        super();
    }

    public ResetUserPasswordRequest(String requestBody) throws JSONException
    {
        JSONObject obj = new JSONObject(requestBody);
        this.username = obj.getString("username");
        this.oldPassword = obj.getString("oldPassword");
        this.newPassword1 = obj.getString("newPassword1");
        this.newPassword2 = obj.getString("newPassword2");
    }

    public String getNewPassword1()
    {
        return this.newPassword1;
    }

    public String getNewPassword2()
    {
        return this.newPassword2;
    }

    public String getOldPassword()
    {
        return this.oldPassword;
    }

    public String getUsername()
    {
        return this.username;
    }

    public void setOldPassword(String password)
    {
        this.oldPassword = password;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }
}
