package org.gs4tr.tmserver.tool.model.xml;

public class DocumentDeclaration implements Node
{
    private String original;

    public DocumentDeclaration(String original)
    {
        this.original = original;
    }

    @Override
    public String asXML()
    {
        return original;
    }

    public String getDocDecl()
    {
        return original;
    }

    @Override
    public short getNodeType()
    {
        return DOCUMENT_TYPE_NODE;
    }

    public String getOriginal()
    {
        return original;
    }

    @Override
    public Element getParent()
    {
        throw new RuntimeException("parent not supported");
    }

    @Override
    public void setParent(Element node)
    {
        throw new RuntimeException("parent not supported");
    }
}
