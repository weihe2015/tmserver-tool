package org.gs4tr.tmserver.tool.model.thread;

import java.util.concurrent.ConcurrentMap;

import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.controller.model.TmxManipulateOption;
import org.gs4tr.tmserver.tool.service.tmx.ManipulateTmxFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManipulateTmxThread implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(ManipulateTmxThread.class);

    private final String tmxFile;
    private final String targetFileName;
    private final TmxManipulateOption tmxManipulateOption;
    private ConcurrentMap<String, String> errorMap;

    public ManipulateTmxThread(String tmxFile, String targetFileName, TmxManipulateOption tmxManipulateOption,
        ConcurrentMap<String, String> errorMap)
    {
        this.tmxFile = tmxFile;
        this.targetFileName = targetFileName;
        this.tmxManipulateOption = tmxManipulateOption;
        this.errorMap = errorMap;
    }

    @Override
    public void run()
    {
        try
        {
            ManipulateTmxFile manipulate = new ManipulateTmxFile(tmxFile, targetFileName, tmxManipulateOption);
            manipulate.process();
        }
        catch (Exception e)
        {
            String errorMessage = String.format(Messages.getString("TmxManipulateThread.0"), e.getMessage(),
                tmxFile);
            errorMap.put(tmxFile, e.getMessage());
            logger.error(errorMessage);
        }
    }
}
