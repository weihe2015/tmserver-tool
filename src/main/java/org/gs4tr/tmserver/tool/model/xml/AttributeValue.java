package org.gs4tr.tmserver.tool.model.xml;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class AttributeValue implements Node
{
    /** List of Text and Entity. */
    private List<Node> content = new ArrayList<>();

    public AttributeValue()
    {
    }

    public void add(Node node)
    {
        content.add(node);
    }

    @Override
    public String asXML()
    {
        StringBuffer result = new StringBuffer();

        for (int i = 0, max = content.size(); i < max; i++)
        {
            result.append(content.get(i).asXML());
        }

        return result.toString();
    }

    public List<Node> content()
    {
        return content;
    }

    @Override
    public short getNodeType()
    {
        return ATTRIBUTE_VALUE_NODE;
    }

    @Override
    public Element getParent()
    {
        throw new RuntimeException("parent not supported");
    }

    /** Returns the textual value of this node after evaluating all entities. */
    public String getText()
    {
        if (content.size() == 0)
        {
            return "";
        }
        else if (content.size() == 1)
        {
            return getText(content.get(0));
        }
        else
        {
            StringBuffer result = new StringBuffer();
            for (int i = 0, max = content.size(); i < max; i++)
            {
                result.append(getText(content.get(i)));
            }
            return result.toString();
        }
    }

    private String getText(Node node)
    {
        switch (node.getNodeType())
        {
        case Node.TEXT_NODE:
            return ((Text) node).getText();
        case Node.ENTITY_NODE:
            return StringUtils.defaultString(((Entity) node).getValue());
        default:
            throw new RuntimeException(
                "AttributeValue::getText(): unhandled node type " + node.getNodeType());
        }
    }

    @Override
    public void setParent(Element node)
    {
        throw new RuntimeException("parent not supported");
    }
}
