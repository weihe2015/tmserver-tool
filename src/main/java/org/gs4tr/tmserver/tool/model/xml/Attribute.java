package org.gs4tr.tmserver.tool.model.xml;

import java.util.ArrayList;
import java.util.List;

public class Attribute implements Node
{
    private Element parent;
    // Name is pre-parsed, list of space/=/space/quote/value/quote.
    private List<Node> content = new ArrayList<>();
    private QName qname;
    private char quote = '"';
    private AttributeValue value;

    // The namespace of this attribute.
    private Namespace namespace;

    public Attribute(QName qname)
    {
        this.qname = qname;
        namespace = qname.getNamespace();
    }

    public void add(Node node)
    {
        content.add(node);
        if (node.getNodeType() == Node.ATTRIBUTE_VALUE_NODE)
        {
            value = (AttributeValue) node;
        }
    }

    @Override
    public String asXML()
    {
        StringBuffer result = new StringBuffer();

        for (int i = 0, max = content.size(); i < max; i++)
        {
            result.append(content.get(i).asXML());
        }

        return result.toString();
    }

    /**
     * Name is pre-parsed, this returns the list of spaces, =, quote, attribute value, quote
     * contained within the attribute.
     */
    public List<Node> content()
    {
        return content;
    }

    public List<Node> getContent()
    {
        return content;
    }

    public QName getName()
    {
        return qname;
    }

    /**
     * Returns the attribute's namespace.
     */
    public Namespace getNamespace()
    {
        return namespace;
    }

    @Override
    public short getNodeType()
    {
        return ATTRIBUTE_NODE;
    }

    @Override
    public Element getParent()
    {
        return parent;
    }

    public char getQuote()
    {
        return quote;
    }

    public AttributeValue getValue()
    {
        return value;
    }

    public void setName(QName qname)
    {
        this.qname = qname;
    }

    @Override
    public void setParent(Element node)
    {
        parent = node;
    }

    public void setQName(QName qname)
    {
        this.qname = qname;
        namespace = qname.getNamespace();
    }

    public void setQuote(char quote)
    {
        this.quote = quote;
    }
}
