package org.gs4tr.tmserver.tool.model.xml;

public class Text implements Node
{
    private Element parent;
    private String text;

    public Text(String text)
    {
        this.text = text;
    }

    @Override
    public String asXML()
    {
        return text;
    }

    @Override
    public short getNodeType()
    {
        return TEXT_NODE;
    }

    @Override
    public Element getParent()
    {
        return parent;
    }

    public String getText()
    {
        return text;
    }

    @Override
    public void setParent(Element node)
    {
        parent = node;
    }

    public void setText(String newtext)
    {
        text = newtext;
    }

    @Override
    public String toString()
    {
        return text;
    }
}
