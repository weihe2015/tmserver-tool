package org.gs4tr.tmserver.tool.model.user;

import java.io.File;

public class StringConstants
{
    public static final String AMPERSAND = "&";

    public static final String AT = "@";

    public static final String BACK_SLASH = "\\";

    public static final String CLOSE_BRACKET = ")";

    public static final String COLON = ":";

    public static final String COMMA = ",";

    public static final String DASH = "-";

    public static final String DOT = ".";

    public static final String DOUBLE_QUOTE = "\"";

    public static final String EQUAL = "=";

    public static final String FOLDER_SEPARATOR = File.separator;

    public static final String GRAVE_ACCENT = "`";

    public static final String GREATER_THAN = ">";

    public static final String LINE_SEPARATOR_PROPERTY = "line.separator";

    public static final String LINE_SEPARATOR = System.getProperty(LINE_SEPARATOR_PROPERTY);

    public static final String LESS_THAN = "<";

    public static final String NEW_LINE = "\n";

    public static final String OPEN_BRACKET = "(";

    public static final String PERCENT = "%";

    public static final String PROJECT_DIRECTOR = "Project Director";

    public static final String QUESTION_MARK = "?";

    public static final String QUOTE = "'";

    public static final String SEMICOLON = ";";

    public static final String SLASH = "/";

    public static final String SPACE = " ";

    public static final String SYSTEM_TEMP_DIR_PROPERTY = "java.io.tmpdir";

    public static final String TAB = "\t";

    public static final String TEMP_DIR = System.getProperty(SYSTEM_TEMP_DIR_PROPERTY);

    public static final String UNDERSCORE = "_";

    public static final String EMPTY = "";

    public static final String OPEN_SQUARE_BRACKET = "[";

    public static final String CLOSE_SQUARE_BRACKET = "]";

    public static final String HASHTAG = "#";

}
