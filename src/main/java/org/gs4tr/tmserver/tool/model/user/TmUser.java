package org.gs4tr.tmserver.tool.model.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;

@Entity
@Table(name = "TM_USER_PROFILE")
public class TmUser implements Serializable
{
    private static final long serialVersionUID = -2468113659988540963L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_PROFILE_ID")
    private Long _id;

    @Column(name = "ACCOUNT_NON_EXPIRED")
    private Boolean _accountNonExpired = Boolean.TRUE;

    @Column(name = "ACCOUNT_NON_LOCKED")
    private Boolean _accountNonLocked = Boolean.TRUE;

    @Column(name = "CREDENTIALS_NON_EXPIRED")
    private Boolean _credentialsNonExpired = Boolean.TRUE;

    @Column(name = "ENABLED")
    private Boolean _enabled = Boolean.TRUE;

    @Column(name = "EMAIL_ADDRESS", length = 255)
    private String _emailAddress;

    @Column(name = "USERNAME", nullable = false, length = 128, unique = true)
    @NotEmpty(message = "*Please provide your username")
    private String username;

    @Column(name = "PASSWORD", length = 128)
    @NotEmpty(message = "*Please provide your password")
    @Transient
    private String _password;

    @Column(name = "USER_TYPE", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private UserTypeEnum _userType;

    // on 3-14-19 PREVIOUS_PASSWORDS field has been removed on TM Server 5.2

    @Column(name = "FIRSTNAME", length = 50)
    @NotEmpty(message = "Please provide your first name")
    private String _firstName;

    @Column(name = "LASTNAME", length = 50)
    @NotEmpty(message = "Please provide your last name")
    private String _lastName;

    @Column(name = "SSO_USER")
    private boolean _ssoUser;

    @Column(name = "TIME_ZONE", length = 30)
    private String _timeZone;

    @Column(name = "DATE_LAST_FAILED_LOGIN")
    @Type(type = "timestamp")
    private Date _dateLastFailedLogin;

    @Column(name = "DATE_LAST_LOGIN")
    @Type(type = "timestamp")
    private Date _dateLastLogin;

    @Column(name = "DATE_PASSWORD_CHANGED")
    @Type(type = "timestamp")
    private Date _datePasswordChanged;

    @Column(name = "DATE_PREVIOUS_LOGIN")
    @Type(type = "timestamp")
    private Date _datePreviousLogin;

    @Column(name = "USER_PHONE1", length = 25)
    @Type(type = "java.lang.String")
    private String _phone1;

    @Column(name = "USER_PHONE2", length = 25)
    @Type(type = "java.lang.String")
    private String _phone2;

    public TmUser()
    {
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (!(obj instanceof TmUser))
        {
            return false;
        }
        TmUser other = (TmUser) obj;
        if (!username.equals(other.username))
        {
            return false;
        }
        return true;
    }

    public Date getDateLastFailedLogin()
    {
        return _dateLastFailedLogin;
    }

    public Date getDatePasswordChanged()
    {
        return _datePasswordChanged;
    }

    public Date getDatePreviousLogin()
    {
        return _datePreviousLogin;
    }

    public String getFirstName()
    {
        return _firstName;
    }

    @Transient
    public String getFullName()
    {
        StringBuilder nameBuilder = new StringBuilder();

        nameBuilder.append(getFirstName());
        nameBuilder.append(StringConstants.SPACE);
        nameBuilder.append(getLastName());

        return nameBuilder.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public String getLastName()
    {
        return _lastName;
    }

    public String getPassword()
    {
        return _password;
    }

    public String getPhone1()
    {
        return _phone1;
    }

    public String getPhone2()
    {
        return _phone2;
    }

    public String getTimeZone()
    {
        return _timeZone;
    }

    public String getUsername()
    {
        return username;
    }

    public UserTypeEnum getUserType()
    {
        return _userType;
    }

    public boolean isAccountNonExpired()
    {
        return _accountNonExpired;
    }

    public boolean isAccountNonLocked()
    {
        return _accountNonLocked;
    }

    public boolean isCredentialsNonExpired()
    {
        return _credentialsNonExpired;
    }

    public boolean isEnabled()
    {
        return _enabled;
    }

    @Transient
    public boolean isPowerUser()
    {
        return UserTypeEnum.POWER_USER == getUserType();
    }

    public void setAccountNonExpired(boolean accountNonExpired)
    {
        this._accountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked)
    {
        _accountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired)
    {
        this._credentialsNonExpired = credentialsNonExpired;
    }

    public void setDateLastFailedLogin(Date dateLastFailedLogin)
    {
        _dateLastFailedLogin = dateLastFailedLogin;
    }

    public void setDateLastLogin(Date dateLastLogin)
    {
        this._dateLastLogin = dateLastLogin;
    }

    public void setDatePasswordChanged(Date datePasswordChanged)
    {
        _datePasswordChanged = datePasswordChanged;
    }

    public void setDatePreviousLogin(Date datePreviousLogin)
    {
        this._datePreviousLogin = datePreviousLogin;
    }

    public void setEmailAddress(String emailAddress)
    {
        _emailAddress = emailAddress;
    }

    public void setFirstName(String firstName)
    {
        _firstName = firstName;
    }

    public void setId(Long id)
    {
        this._id = id;
    }

    public void setLastName(String lastName)
    {
        _lastName = lastName;
    }

    public void setPassword(String password)
    {
        this._password = password;
    }

    public void setPhone1(String phone1)
    {
        _phone1 = phone1;
    }

    public void setPhone2(String phone2)
    {
        _phone2 = phone2;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public boolean isSSOUser()
    {
        return _ssoUser;
    }

    public void setSSOUser(boolean _ssoUser)
    {
        this._ssoUser = _ssoUser;
    }
}
