package org.gs4tr.tmserver.tool.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TM_USER_ROLE")
public class TmUserRole
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_PROFILE_ID")
    private Long userProfileId;

    @Column(name = "ROLE_ID")
    private String roleId;

    public TmUserRole()
    {
    }

    public String getRoleId()
    {
        return this.roleId;
    }

    public Long getUserProfileId()
    {
        return this.userProfileId;
    }

}
