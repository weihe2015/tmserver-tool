package org.gs4tr.tmserver.tool.model.xml;

import java.util.ArrayList;
import java.util.List;

import org.gs4tr.tmserver.tool.model.xml.xpath.MyDomXPath;
import org.jaxen.JaxenException;
import org.jaxen.XPath;

public class Document implements Node
{
    // List of XmlDeclaration, spaces, DocumentDeclation, PIs, Comments,
    // root element, more spaces, PIs and comments.
    private List<Node> content = new ArrayList<>();
    private boolean hasBOM;
    private XmlDeclaration xmlDecl;
    private Element rootElement;

    public Document()
    {
    }

    public void add(Node node)
    {
        content.add(node);

        if (node.getNodeType() == Node.ELEMENT_NODE && rootElement == null)
        {
            // Parser sets elem.setDocument() on all documents.
            rootElement = (Element) node;
        }
        else if (node.getNodeType() == Node.XML_DECL_NODE)
        {
            xmlDecl = (XmlDeclaration) node;
        }
    }

    @Override
    public String asXML()
    {
        StringBuffer result = new StringBuffer();

        if (hasBOM)
        {
            result.append('\uFEFF');
        }

        for (int i = 0, max = content.size(); i < max; i++)
        {
            result.append(content.get(i).asXML());
        }

        return result.toString();
    }

    public List<Node> content()
    {
        return content;
    }

    @Override
    public short getNodeType()
    {
        return DOCUMENT_NODE;
    }

    @Override
    public Element getParent()
    {
        throw new RuntimeException("parent not supported");
    }

    public Element getRootElement()
    {
        return rootElement;
    }

    public XmlDeclaration getXmlDecl()
    {
        return xmlDecl;
    }

    public boolean isHasBOM()
    {
        return hasBOM;
    }

    @SuppressWarnings("unchecked")
    public List<Node> selectNodes(String xpath)
    {
        try
        {
            XPath myxpath = new MyDomXPath(xpath);
            return myxpath.selectNodes(this);
        }
        catch (JaxenException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public Node selectSingleNode(String xpath)
    {
        try
        {
            XPath myxpath = new MyDomXPath(xpath);
            return (Node) myxpath.selectSingleNode(this);
        }
        catch (JaxenException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public void setHasBOM(boolean hasBOM)
    {
        this.hasBOM = hasBOM;
    }

    @Override
    public void setParent(Element node)
    {
        throw new RuntimeException("parent not supported");
    }
}
