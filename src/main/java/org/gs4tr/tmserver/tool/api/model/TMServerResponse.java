package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public abstract class TMServerResponse
{
    private int returnCode;
    private boolean success;
    private int time_ms;
    private String errorMessage;
    private String message;
    private String version;

    public int getReturnCode()
    {
        return returnCode;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public int getTime_ms()
    {
        return time_ms;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public String getVersion()
    {
        return version;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }
}
