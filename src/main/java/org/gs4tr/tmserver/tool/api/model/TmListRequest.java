package org.gs4tr.tmserver.tool.api.model;

import org.gs4tr.tmserver.tool.model.api.TmServerInfo;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@NoArgsConstructor
public class TmListRequest
{
    private String username;
    private String password;
    private String tmGroupShortCode;

    public void initTmListRequest(TmServerInfo tmServerInfo)
    {
        this.username = tmServerInfo.getTmUserName();
        this.password = tmServerInfo.getTmPassword();
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getTmGroupShortCode()
    {
        return tmGroupShortCode;
    }

    public void setTmGroupShortCode(String tmGroupShortCode)
    {
        this.tmGroupShortCode = tmGroupShortCode;
    }
}
