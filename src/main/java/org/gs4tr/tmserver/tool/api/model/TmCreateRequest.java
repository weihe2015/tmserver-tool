package org.gs4tr.tmserver.tool.api.model;

import java.util.ArrayList;
import java.util.List;

import org.gs4tr.tmserver.tool.controller.model.TmCreateOption;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@NoArgsConstructor
public class TmCreateRequest extends TmServerRequest
{
    private String groupId;
    private TmCreateInfo tm;
    private List<TmGenericUser> users;

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public TmCreateInfo getTm()
    {
        return tm;
    }

    public void setTm(TmCreateInfo tm)
    {
        this.tm = tm;
    }

    public List<TmGenericUser> getUsers()
    {
        return users;
    }

    public void setUsers(List<TmGenericUser> users)
    {
        this.users = users;
    }

    public void setTmCreateOption(TmCreateOption tmCreateOption)
    {
        this.groupId = tmCreateOption.getSelectedTmGroup().getValue();
        this.tm = new TmCreateInfo();
        setTmInfo(tmCreateOption);

        this.users = new ArrayList<>();
        TmGenericUser tmGenericUser = new TmGenericUser();
        tmGenericUser.setUsername(tmCreateOption.getUser().getUsername());
        tmGenericUser.setPassword(tmCreateOption.getUser().getPassword());
        tmGenericUser.setRole(tmCreateOption.getUser().getRole().getValue());
        users.add(tmGenericUser);
    }

    private void setTmInfo(TmCreateOption tmCreateOption)
    {
        this.tm.setName(tmCreateOption.getTmName());
        this.tm.setShortCode(tmCreateOption.getTmShortCode());
        this.tm.setEnabled(tmCreateOption.isEnabled());
        this.tm.setContextModeEnabled(tmCreateOption.isContextModeEnabled());
        this.tm.setSourceLanguage(tmCreateOption.getSourceLanguage());
        this.tm.setTargetLanguage(tmCreateOption.getTargetLanguage());
        this.tm.setCleanupCode(tmCreateOption.getCleanupCode());
    }

}
