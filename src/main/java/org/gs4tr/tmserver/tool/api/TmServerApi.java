package org.gs4tr.tmserver.tool.api;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Set;

import org.gs4tr.tmserver.tool.api.model.CreateShortcodeResponse;
import org.gs4tr.tmserver.tool.api.model.CreateTmResponse;
import org.gs4tr.tmserver.tool.api.model.FetchOrganizationsResponse;
import org.gs4tr.tmserver.tool.api.model.FetchTmGroupsResponse;
import org.gs4tr.tmserver.tool.api.model.FetchTmLanguageResponse;
import org.gs4tr.tmserver.tool.api.model.GenerateUserPasswordResponse;
import org.gs4tr.tmserver.tool.api.model.GenerateUsernameResponse;
import org.gs4tr.tmserver.tool.api.model.GenericRolesResponse;
import org.gs4tr.tmserver.tool.api.model.ImportProcessResponse;
import org.gs4tr.tmserver.tool.api.model.ImportTmResponse;
import org.gs4tr.tmserver.tool.api.model.LoginResponse;
import org.gs4tr.tmserver.tool.api.model.TmCleanupPasscodeResponse;
import org.gs4tr.tmserver.tool.api.model.TmGroupCreateResponse;
import org.gs4tr.tmserver.tool.api.model.TmInfo;
import org.gs4tr.tmserver.tool.api.model.TmListResponse;
import org.gs4tr.tmserver.tool.api.model.TmTuInfoResponse;
import org.gs4tr.tmserver.tool.api.model.ValidateCodeResponse;
import org.gs4tr.tmserver.tool.api.model.ValidateOAuthTokenResponse;
import org.gs4tr.tmserver.tool.controller.jobqueue.exportTm.ExportTmInfo;
import org.gs4tr.tmserver.tool.controller.model.TmCreateOption;
import org.gs4tr.tmserver.tool.controller.model.TmExportOption;
import org.gs4tr.tmserver.tool.controller.model.TmGenericUsernameOption;
import org.gs4tr.tmserver.tool.controller.model.TmGroupOption;
import org.gs4tr.tmserver.tool.controller.model.TmImportOption;
import org.gs4tr.tmserver.tool.controller.model.TmSearchOption;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.model.concordance.TmSearchResult;
import org.springframework.http.ResponseEntity;

public interface TmServerApi
{
    public List<TmSearchResult> concordance(TmServerInfo tmServerInfo, TmSearchOption searchOption,
        TmInfo tmInfo, String securityTicket, String oauthToken) throws IOException;

    public HttpURLConnection connect(String url, boolean isExport, String oauthToken) throws IOException;

    public CreateTmResponse createTm(TmServerInfo tmServerInfo, TmCreateOption tmCreateOption,
        String securityTicket, String oauthToken) throws IOException;

    public TmCleanupPasscodeResponse createTmCleanupPasscode(TmServerInfo tmServerInfo, String securityTicket, String oauthToken)
        throws IOException;

    public TmGroupCreateResponse createTmGroup(TmServerInfo tmServerInfo, TmGroupOption tmGroupOption, String securityTicket, String oauthToken) throws IOException;

    public CreateShortcodeResponse createTmGroupShortCode(TmServerInfo tmServerInfo, String tmGroupName, String securityTicket, String oauthToken)
        throws IOException;

    public CreateShortcodeResponse createTmShortCode(TmServerInfo tmServerInfo, String tmName, String securityTicket, String oauthToken)
        throws IOException;

    public ResponseEntity<?> exportTm(TmServerInfo tmServerInfo, TmExportOption tmExportOption, String securityTicket, String oauthToken,
        ExportTmInfo info) throws IOException;

    public GenerateUsernameResponse generateGenericUsername(TmServerInfo tmServerInfo, TmGenericUsernameOption tmGenericUsernameOption,
        String securityTicket, String oauthToken) throws IOException;

    public GenerateUserPasswordResponse generateGenericUserpassword(TmServerInfo tmServerInfo, String securityTicket,
        String oauthToken) throws IOException;

    public GenericRolesResponse getGenericRoles(TmServerInfo tmServerInfo, String securityTicket, String oauthToken)
        throws IOException;

    public FetchTmGroupsResponse getGroupList(TmServerInfo tmServerInfo, String securityTicket, String oauthToken)
        throws IOException;

    public ImportProcessResponse getImportProgress(TmServerInfo tmServerInfo, String jobId, String securityTicket, String oauthToken)
        throws IOException;

    public FetchTmLanguageResponse getLanguageList(TmServerInfo tmServerInfo, String securityTicket, String oauthToken)
        throws IOException;

    public FetchOrganizationsResponse getOrganizationList(TmServerInfo tmServerInfo, String securityTicket, String oauthToken)
        throws IOException;

    public TmListResponse fetchTmList(TmServerInfo tmServerInfo) throws IOException;

    public TmListResponse fetchTmListWithTmGroupCodes(TmServerInfo tmServerInfo, Set<String> tmGroupShortCodes) throws IOException;

    public LoginResponse getTmServerVersion(TmServerInfo tmServerInfo, String tmPath) throws IOException;

    public TmTuInfoResponse getTuInfo(TmServerInfo tmServerInfo, String securityTicket, String oauthToken) throws IOException;

    public ImportTmResponse importTm(TmServerInfo tmServerInfo, TmImportOption tmImportOption, File uploadFile,
        String securityTicket, String oauthToken, boolean isTPAuth) throws IOException;

    public LoginResponse login(TmServerInfo tmServerInfo, String tmlocation) throws IOException;

    public void logout(TmServerInfo tmServerInfo, String securityTicket);

    public List<TmSearchResult> search(TmServerInfo tmServerInfo, TmSearchOption searchOption,
        TmInfo tmInfo, String securityTicket, String oauthToken) throws IOException;

    public ValidateCodeResponse validateTmCleanupPasscode(TmServerInfo tmServerInfo, String tmCleanupPasscode,
        String securityTicket, String oauthToken) throws IOException;

    public ValidateCodeResponse validateTmGroupCode(TmServerInfo tmServerInfo, String tmGroupCode,
        String securityTicket, String oauthToken) throws IOException;

    public ValidateCodeResponse validateTmShortCode(TmServerInfo tmServerInfo, String tmShortCode, String securityTicket, String oauthToken) throws IOException;

    public ValidateOAuthTokenResponse validateOAuthToken(TmServerInfo tmServerInfo, String oauthToken) throws IOException;
}
