package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class Attribute
{
    private String name;
    private String value;

    public String getName()
    {
        return name;
    }

    public String getValue()
    {
        return value;
    }

}
