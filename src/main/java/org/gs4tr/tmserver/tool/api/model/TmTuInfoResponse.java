package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class TmTuInfoResponse extends TMServerResponse
{
    private TmTuInfo tuInfo;

    public TmTuInfo getTuInfo()
    {
        return tuInfo;
    }

    public void setTuInfo(TmTuInfo tuInfo)
    {
        this.tuInfo = tuInfo;
    }
}
