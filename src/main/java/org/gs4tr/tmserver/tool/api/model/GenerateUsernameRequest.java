package org.gs4tr.tmserver.tool.api.model;

import org.gs4tr.tmserver.tool.controller.model.TmGenericUsernameOption;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class GenerateUsernameRequest extends TmServerRequest
{
    private String tmName;
    private String sourceLanguage;
    private String targetLanguage;

    public void initGenerateUsernameRequest(TmGenericUsernameOption tmGenericUsernameOption)
    {
        setTmName(tmGenericUsernameOption.getTmName());
        setSourceLanguage(tmGenericUsernameOption.getSourceLanguageCode());
        setTargetLanguage(tmGenericUsernameOption.getTargetLanguageCode());
    }

    public void setTmName(String tmName)
    {
        this.tmName = tmName;
    }

    public void setSourceLanguage(String sourceLanguage)
    {
        this.sourceLanguage = sourceLanguage;
    }

    public void setTargetLanguage(String targetLanguage)
    {
        this.targetLanguage = targetLanguage;
    }

    public String getTmName()
    {
        return tmName;
    }

    public String getSourceLanguage()
    {
        return sourceLanguage;
    }

    public String getTargetLanguage()
    {
        return targetLanguage;
    }

}
