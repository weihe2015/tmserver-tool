package org.gs4tr.tmserver.tool.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
public class Segment
{
    private String id;
    private String source;
    private String target;
    private String creationUser;
    private String modificationUser;
    private Long creationDate;
    private Long modificationDate;

    private List<Attribute> attributes;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getTarget()
    {
        return target;
    }

    public void setTarget(String target)
    {
        this.target = target;
    }

    public String getCreationUser()
    {
        return creationUser;
    }

    public void setCreationUser(String creationUser)
    {
        this.creationUser = creationUser;
    }

    public String getModificationUser()
    {
        return modificationUser;
    }

    public void setModificationUser(String modificationUser)
    {
        this.modificationUser = modificationUser;
    }

    public Long getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Long creationDate)
    {
        this.creationDate = creationDate;
    }

    public Long getModificationDate()
    {
        return modificationDate;
    }

    public void setModificationDate(Long modificationDate)
    {
        this.modificationDate = modificationDate;
    }

    public List<Attribute> getAttributes()
    {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes)
    {
        this.attributes = attributes;
    }
}