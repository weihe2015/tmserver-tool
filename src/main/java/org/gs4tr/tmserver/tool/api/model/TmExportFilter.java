package org.gs4tr.tmserver.tool.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Setter
public class TmExportFilter
{
    private Long creationDateStart;
    private Long creationDateEnd;
    private Long modificationDateStart;
    private Long modificationDateEnd;

    private String creationUser;
    private String modificationUser;
    private String workgroup;

    private List<Attribute> attributes;

    public Long getCreationDateStart()
    {
        return creationDateStart;
    }

    public Long getCreationDateEnd()
    {
        return creationDateEnd;
    }

    public Long getModificationDateStart()
    {
        return modificationDateStart;
    }

    public Long getModificationDateEnd()
    {
        return modificationDateEnd;
    }

    public String getCreationUser()
    {
        return creationUser;
    }

    public String getModificationUser()
    {
        return modificationUser;
    }

    public String getWorkgroup()
    {
        return workgroup;
    }

    public List<Attribute> getAttributes()
    {
        return attributes;
    }

    public void setCreationDateStart(Long creationDateStart)
    {
        this.creationDateStart = creationDateStart;
    }

    public void setCreationDateEnd(Long creationDateEnd)
    {
        this.creationDateEnd = creationDateEnd;
    }

    public void setModificationDateStart(Long modificationDateStart)
    {
        this.modificationDateStart = modificationDateStart;
    }

    public void setModificationDateEnd(Long modificationDateEnd)
    {
        this.modificationDateEnd = modificationDateEnd;
    }

    public void setCreationUser(String creationUser)
    {
        this.creationUser = creationUser;
    }

    public void setModificationUser(String modificationUser)
    {
        this.modificationUser = modificationUser;
    }

    public void setWorkgroup(String workgroup)
    {
        this.workgroup = workgroup;
    }

    public void setAttributes(List<Attribute> attributes)
    {
        this.attributes = attributes;
    }
}
