package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class CreateShortcodeRequest extends TmServerRequest
{
    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
