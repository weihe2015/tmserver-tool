package org.gs4tr.tmserver.tool.api.model;

import org.gs4tr.tmserver.tool.controller.model.TmSearchOption;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class ConcordanceSearchRequest extends TmServerRequest
{
    private String query;
    private boolean caseSensitive;
    private boolean matchWholeWords;
    private boolean exactMatch;
    private boolean publicTuOnly;
    private int maxResults;
    private String location;
    private SearchFilter searchFilter;

    public void initConcordanceSearchRequest(TmSearchOption searchOption)
    {
        this.query = searchOption.getQuery();
        this.caseSensitive = searchOption.isCaseSensitive();
        this.matchWholeWords = searchOption.isMatchWholeWord();
        this.exactMatch = searchOption.isExactMatch();
        this.publicTuOnly = searchOption.isPublicTuOnly();
        this.maxResults = searchOption.getResultNum();
        this.location = searchOption.getSearchLocation();
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public boolean isCaseSensitive()
    {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }

    public boolean isMatchWholeWords()
    {
        return matchWholeWords;
    }

    public void setMatchWholeWords(boolean matchWholeWords)
    {
        this.matchWholeWords = matchWholeWords;
    }

    public boolean isExactMatch()
    {
        return exactMatch;
    }

    public void setExactMatch(boolean exactMatch)
    {
        this.exactMatch = exactMatch;
    }

    public boolean isPublicTuOnly()
    {
        return publicTuOnly;
    }

    public void setPublicTuOnly(boolean publicTuOnly)
    {
        this.publicTuOnly = publicTuOnly;
    }

    public int getMaxResults()
    {
        return maxResults;
    }

    public void setMaxResults(int maxResults)
    {
        this.maxResults = maxResults;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public SearchFilter getSearchFilter()
    {
        return searchFilter;
    }

    public void setSearchFilter(SearchFilter searchFilter)
    {
        this.searchFilter = searchFilter;
    }
}
