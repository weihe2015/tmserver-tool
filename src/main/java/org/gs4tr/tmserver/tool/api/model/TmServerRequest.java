package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmServerRequest
{
    private String securityTicket;

    public void setSecurityTicket(String securityTicket)
    {
        this.securityTicket = securityTicket;
    }

    public String getSecurityTicket()
    {
        return securityTicket;
    }

}
