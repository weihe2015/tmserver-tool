package org.gs4tr.tmserver.tool.api.model;

import java.util.List;

import org.gs4tr.tmserver.tool.model.createTm.TmGroup;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class FetchTmGroupsResponse extends TMServerResponse
{
    private List<TmGroup> projects;

    public List<TmGroup> getProjects()
    {
        return projects;
    }

    public void setProjects(List<TmGroup> projects)
    {
        this.projects = projects;
    }
}
