package org.gs4tr.tmserver.tool.api.model;

import org.gs4tr.tmserver.tool.controller.model.TmCreateOption;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@NoArgsConstructor
public class CreateTmResponse extends TMServerResponse
{
    private TmInfo tmInfo;
    private String sourceLanguage;
    private String targetLangauge;
    private String tmName;
    private String tmShortCode;
    private int listVersion;

    public TmInfo getTmInfo()
    {
        return tmInfo;
    }

    public void setTmInfo(TmInfo tmInfo)
    {
        this.tmInfo = tmInfo;
    }

    public int getListVersion()
    {
        return listVersion;
    }

    public void setListVersion(int listVersion)
    {
        this.listVersion = listVersion;
    }

    public String getTmName()
    {
        return tmName;
    }

    public void setTmName(String tmName)
    {
        this.tmName = tmName;
    }

    public String getTmShortCode()
    {
        return tmShortCode;
    }

    public void setTmShortCode(String tmShortCode)
    {
        this.tmShortCode = tmShortCode;
    }

    public String getSourceLanguage()
    {
        return sourceLanguage;
    }

    public void setSourceLanguage(String sourceLanguage)
    {
        this.sourceLanguage = sourceLanguage;
    }

    public String getTargetLangauge()
    {
        return targetLangauge;
    }

    public void setTargetLangauge(String targetLangauge)
    {
        this.targetLangauge = targetLangauge;
    }

    public void setFailureCreateTmInfo(TmCreateOption tmCreateOption)
    {
        this.sourceLanguage = tmCreateOption.getSourceLanguage();
        this.targetLangauge = tmCreateOption.getTargetLanguage();
        this.tmName = tmCreateOption.getTmName();
        this.tmShortCode = tmCreateOption.getTmShortCode();
    }
}
