package org.gs4tr.tmserver.tool.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@NoArgsConstructor
public class TmListResponse extends TMServerResponse implements Serializable
{
    private static final long serialVersionUID = 4565144336114068293L;

    private List<TmInfo> tmList;
    private int listVersion;

    public List<TmInfo> getTmList()
    {
        return tmList;
    }

    public void setTmList(List<TmInfo> tmList)
    {
        this.tmList = tmList;
    }

    public void addTmList(List<TmInfo> tmList)
    {
        if (this.tmList == null)
        {
            this.tmList = new ArrayList<>();
        }
        this.tmList.addAll(tmList);
    }

    public int getListVersion()
    {
        return listVersion;
    }

    public void setListVersion(int listVersion)
    {
        this.listVersion = listVersion;
    }

    /**
     * Filter Tm List by Tm Group ShortCode set in application.properties Limited user to search only Tm
     * Group listed in the application.properties It is a temporarily solution for issue of get Tm List
     * returning all Tm in the server
     */
    public void filterTmInfoListByTmGroupShortCode(Set<String> tmGroupShortCodes)
    {
        List<TmInfo> tmInfos = new ArrayList<>();
        for (TmInfo tmInfo : this.tmList)
        {
            String groupShortCode = tmInfo.getGroupShortCode();
            // Save TMs that matches the TmGroupCode listed in application.properties
            if (tmGroupShortCodes.isEmpty() || tmGroupShortCodes.contains(groupShortCode))
            {
                tmInfos.add(tmInfo);
            }
        }
        this.setTmList(tmInfos);
    }
}
