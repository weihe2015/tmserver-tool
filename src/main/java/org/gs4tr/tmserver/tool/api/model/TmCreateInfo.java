package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@NoArgsConstructor
public class TmCreateInfo
{
    private String name;
    private String shortCode;
    private boolean enabled;
    private boolean contextModeEnabled;
    private String sourceLanguage;
    private String targetLanguage;
    private String cleanupCode;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getShortCode()
    {
        return shortCode;
    }

    public void setShortCode(String shortCode)
    {
        this.shortCode = shortCode;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isContextModeEnabled()
    {
        return contextModeEnabled;
    }

    public void setContextModeEnabled(boolean contextModeEnabled)
    {
        this.contextModeEnabled = contextModeEnabled;
    }

    public String getSourceLanguage()
    {
        return sourceLanguage;
    }

    public void setSourceLanguage(String sourceLanguage)
    {
        this.sourceLanguage = sourceLanguage;
    }

    public String getTargetLanguage()
    {
        return targetLanguage;
    }

    public void setTargetLanguage(String targetLanguage)
    {
        this.targetLanguage = targetLanguage;
    }

    public String getCleanupCode()
    {
        return cleanupCode;
    }

    public void setCleanupCode(String cleanupCode)
    {
        this.cleanupCode = cleanupCode;
    }
}
