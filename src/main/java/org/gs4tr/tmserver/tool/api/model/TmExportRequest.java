package org.gs4tr.tmserver.tool.api.model;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.controller.model.TmExportOption;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class TmExportRequest extends TmServerRequest
{
    private String exportType;
    private String tmLocation;
    private boolean includePrivateTus;
    private boolean includeAttributes;
    private TmExportFilter exportFilter;

    public String getExportType()
    {
        return exportType;
    }

    public void setExportType(String exportType)
    {
        this.exportType = exportType;
    }

    public boolean isIncludePrivateTus()
    {
        return includePrivateTus;
    }

    public void setIncludePrivateTus(boolean includePrivateTus)
    {
        this.includePrivateTus = includePrivateTus;
    }

    public boolean isIncludeAttributes()
    {
        return includeAttributes;
    }

    public void setIncludeAttributes(boolean includeAttributes)
    {
        this.includeAttributes = includeAttributes;
    }

    public TmExportFilter getExportFilter()
    {
        return exportFilter;
    }

    public void setExportFilter(TmExportFilter p_exportFilter)
    {
        if (p_exportFilter == null)
        {
            this.exportFilter = p_exportFilter;
            return;
        }

        this.exportFilter = p_exportFilter;
        boolean containsFilter = false;
        // remove zero or empty string fields by setting them to null;
        if (exportFilter.getCreationDateStart() == 0)
        {
            exportFilter.setCreationDateStart(null);
        }
        else
        {
            containsFilter = true;
        }

        if (exportFilter.getCreationDateEnd() == 0)
        {
            exportFilter.setCreationDateEnd(null);
            containsFilter = false;
        }
        else
        {
            containsFilter = true;
        }

        if (exportFilter.getModificationDateStart() == 0)
        {
            exportFilter.setModificationDateStart(null);
            containsFilter = false;
        }
        else
        {
            containsFilter = true;
        }

        if (exportFilter.getModificationDateEnd() == 0)
        {
            exportFilter.setModificationDateEnd(null);
        }
        else
        {
            containsFilter = true;
        }

        if (StringUtils.isEmpty(exportFilter.getCreationUser()))
        {
            exportFilter.setCreationUser(null);
        }
        else
        {
            containsFilter = true;
        }

        if (StringUtils.isEmpty(exportFilter.getModificationUser()))
        {
            exportFilter.setModificationUser(null);
        }
        else
        {
            containsFilter = true;
        }

        if (StringUtils.isEmpty(exportFilter.getWorkgroup()))
        {
            exportFilter.setWorkgroup(null);
        }
        else
        {
            containsFilter = true;
        }

        if (exportFilter.getAttributes().size() == 0)
        {
            exportFilter.setAttributes(null);
        }
        else
        {
            containsFilter = true;
        }

        if (!containsFilter)
        {
            this.exportFilter = null;
        }
    }

    public void setExportOption(TmExportOption tmExportOption)
    {
        this.exportType = tmExportOption.getTmExportType();
        this.includeAttributes = tmExportOption.isIncludeAttributes();
        this.includePrivateTus = tmExportOption.isIncludePrivateTus();
        this.exportFilter = tmExportOption.getExportFilter();
    }

    public String getTmLocation()
    {
        return tmLocation;
    }

    public void setTmLocation(String tmLocation)
    {
        this.tmLocation = tmLocation;
    }
}
