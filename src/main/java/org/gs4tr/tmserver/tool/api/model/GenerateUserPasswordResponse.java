package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class GenerateUserPasswordResponse extends TMServerResponse
{
    private String password;

    public String getPassword()
    {
        return this.password;
    }
}
