package org.gs4tr.tmserver.tool.api.model;

import java.util.List;

import org.gs4tr.tmserver.tool.model.createTm.TmLanguage;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class FetchTmLanguageResponse extends TMServerResponse
{
    private List<TmLanguage> languages;

    public List<TmLanguage> getLanguages()
    {
        return languages;
    }

    public void setLanguages(List<TmLanguage> languages)
    {
        this.languages = languages;
    }

}
