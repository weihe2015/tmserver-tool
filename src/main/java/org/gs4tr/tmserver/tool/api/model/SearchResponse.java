package org.gs4tr.tmserver.tool.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class SearchResponse extends TMServerResponse
{
    private int resultNum;
    private List<Segment> segments;

    public int getResultNum()
    {
        return resultNum;
    }

    public void setResultNum(int resultNum)
    {
        this.resultNum = resultNum;
    }

    public List<Segment> getSegments()
    {
        return segments;
    }

    public void setSegments(List<Segment> segments)
    {
        this.segments = segments;
    }
}
