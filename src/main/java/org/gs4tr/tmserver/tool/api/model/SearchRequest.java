package org.gs4tr.tmserver.tool.api.model;

import org.gs4tr.tmserver.tool.controller.model.TmSearchOption;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class SearchRequest extends TmServerRequest
{
    private boolean includePrivateTus;
    private String query;
    private int maxResults;
    private boolean showRawScore;

    public SearchRequest createNewSearchRequest()
    {
        return new SearchRequest();
    }

    public boolean isIncludePrivateTus()
    {
        return includePrivateTus;
    }

    public void setIncludePrivateTus(boolean includePrivateTus)
    {
        this.includePrivateTus = includePrivateTus;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public int getMaxResults()
    {
        return maxResults;
    }

    public void setMaxResults(int maxResults)
    {
        this.maxResults = maxResults;
    }

    public boolean isShowRawScore()
    {
        return showRawScore;
    }

    public void setShowRawScore(boolean showRawScore)
    {
        this.showRawScore = showRawScore;
    }

    public void setSearchOption(TmSearchOption searchOption)
    {
        this.query = searchOption.getQuery();
        this.includePrivateTus = !searchOption.isPublicTuOnly();
        this.maxResults = searchOption.getResultNum();
    }
}
