package org.gs4tr.tmserver.tool.api.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class GenericRolesResponse extends TMServerResponse implements Serializable
{
    private static final long serialVersionUID = -4744088936717009742L;

    private List<String> roles;

    public List<String> getRoles()
    {
        return roles;
    }

    public void setRoles(List<String> roles)
    {
        this.roles = roles;
    }
}
