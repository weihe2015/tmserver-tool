package org.gs4tr.tmserver.tool.api.model;

import org.gs4tr.tmserver.tool.model.api.TmServerInfo;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class LoginRequest
{
    private String username;
    private String password;
    private String tmLocation;
    private String workgroupId;

    public void setInfo(TmServerInfo tmServerInfo, String tmLocation)
    {
        this.username = tmServerInfo.getTmUserName();
        this.password = tmServerInfo.getTmPassword();
        this.tmLocation = tmLocation;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getTmLocation()
    {
        return tmLocation;
    }

    public void setTmLocation(String tmLocation)
    {
        this.tmLocation = tmLocation;
    }

    public String getWorkgroupId()
    {
        return workgroupId;
    }

    public void setWorkgroupId(String workgroupId)
    {
        this.workgroupId = workgroupId;
    }
}
