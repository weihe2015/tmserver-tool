package org.gs4tr.tmserver.tool.api.model;

import java.util.List;

import org.gs4tr.tmserver.tool.model.createTm.TmOrganization;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class FetchOrganizationsResponse extends TMServerResponse
{
    private List<TmOrganization> organizations;
    private String message;

    public List<TmOrganization> getOrganizations()
    {
        return organizations;
    }

    public void setOrganizations(List<TmOrganization> organizations)
    {
        this.organizations = organizations;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
