package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class SearchFilter extends TmExportFilter
{
    private boolean includeCDateStart;
    private boolean includeCDateEnd;
    private boolean includeMDateStart;
    private boolean includeMDateEnd;


    public boolean isIncludeCDateStart()
    {
        return includeCDateStart;
    }

    public boolean isIncludeCDateEnd()
    {
        return includeCDateEnd;
    }

    public boolean isIncludeMDateStart()
    {
        return includeMDateStart;
    }

    public boolean isIncludeMDateEnd()
    {
        return includeMDateEnd;
    }

}
