package org.gs4tr.tmserver.tool.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.Messages;
import org.gs4tr.tmserver.tool.api.model.ConcordanceSearchRequest;
import org.gs4tr.tmserver.tool.api.model.CreateShortcodeRequest;
import org.gs4tr.tmserver.tool.api.model.CreateShortcodeResponse;
import org.gs4tr.tmserver.tool.api.model.CreateTmGroupRequest;
import org.gs4tr.tmserver.tool.api.model.CreateTmResponse;
import org.gs4tr.tmserver.tool.api.model.FetchOrganizationsResponse;
import org.gs4tr.tmserver.tool.api.model.FetchTmGroupsResponse;
import org.gs4tr.tmserver.tool.api.model.FetchTmLanguageResponse;
import org.gs4tr.tmserver.tool.api.model.GenerateUserPasswordResponse;
import org.gs4tr.tmserver.tool.api.model.GenerateUsernameRequest;
import org.gs4tr.tmserver.tool.api.model.GenerateUsernameResponse;
import org.gs4tr.tmserver.tool.api.model.GenericRolesResponse;
import org.gs4tr.tmserver.tool.api.model.ImportProcessRequest;
import org.gs4tr.tmserver.tool.api.model.ImportProcessResponse;
import org.gs4tr.tmserver.tool.api.model.ImportTmResponse;
import org.gs4tr.tmserver.tool.api.model.LoginRequest;
import org.gs4tr.tmserver.tool.api.model.LoginResponse;
import org.gs4tr.tmserver.tool.api.model.SearchRequest;
import org.gs4tr.tmserver.tool.api.model.SearchResponse;
import org.gs4tr.tmserver.tool.api.model.Segment;
import org.gs4tr.tmserver.tool.api.model.TmServerRequest;
import org.gs4tr.tmserver.tool.api.model.TmCleanupPasscodeResponse;
import org.gs4tr.tmserver.tool.api.model.TmCreateRequest;
import org.gs4tr.tmserver.tool.api.model.TmExportRequest;
import org.gs4tr.tmserver.tool.api.model.TmGroupCreateResponse;
import org.gs4tr.tmserver.tool.api.model.TmInfo;
import org.gs4tr.tmserver.tool.api.model.TmListRequest;
import org.gs4tr.tmserver.tool.api.model.TmListResponse;
import org.gs4tr.tmserver.tool.api.model.TmTuInfoResponse;
import org.gs4tr.tmserver.tool.api.model.ValidateCodeRequest;
import org.gs4tr.tmserver.tool.api.model.ValidateCodeResponse;
import org.gs4tr.tmserver.tool.api.model.ValidateOAuthTokenRequest;
import org.gs4tr.tmserver.tool.api.model.ValidateOAuthTokenResponse;
import org.gs4tr.tmserver.tool.controller.jobqueue.exportTm.ExportTmInfo;
import org.gs4tr.tmserver.tool.controller.model.TmCreateOption;
import org.gs4tr.tmserver.tool.controller.model.TmExportOption;
import org.gs4tr.tmserver.tool.controller.model.TmExportResponse;
import org.gs4tr.tmserver.tool.controller.model.TmGenericUsernameOption;
import org.gs4tr.tmserver.tool.controller.model.TmGroupOption;
import org.gs4tr.tmserver.tool.controller.model.TmImportOption;
import org.gs4tr.tmserver.tool.controller.model.TmSearchOption;
import org.gs4tr.tmserver.tool.model.api.TmServerInfo;
import org.gs4tr.tmserver.tool.model.concordance.TmSearchResult;
import org.gs4tr.tmserver.tool.model.createTm.TmGroup;
import org.gs4tr.tmserver.tool.model.createTm.TmLanguage;
import org.gs4tr.tmserver.tool.model.createTm.TmOrganization;
import org.gs4tr.tmserver.tool.model.export.TmFtpAdaptor;
import org.gs4tr.tmserver.tool.service.file.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * All the GL Tm Server Web Services description are available in
 * https://collaborate.translations.com/display/techprodmng/GLTMS+Web+Services+v2
 */
@Service("TmServerApiService")
public class TmServerApiImpl implements TmServerApi
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TmServerApiImpl.class);
    private static final String BOUNDARY = Long.toHexString(System.currentTimeMillis());
    private static final String UTF8_CHAR_SET = StandardCharsets.UTF_8.name();

    private static final String JSSE_ENABLE_SNI_EXTENSION = "jsse.enableSNIExtension";

    @Value("${http.connection.timeout:43200}")
    private int httpConnectionTimeout;

    @Value("${http.read.timeout:43200}")
    private int httpConnectionReadTimeout;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TmFtpAdaptor tmFtpAdaptor;

    /**
     * <p>
     * The concordance search function to utilize Concordance Search RESTful API to perform
     * searching on given text in TmSearchOption and return a list of TmSearchResult.
     * </p>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info
     * @param TmSearchOption searchOption, which contains all options required in POST content
     *        request in Concordance search RESTful API
     * @param MixedTmInfo tmInfo, which contains Tm name, Tm Group Name, source/target Locale , it will
     *        be filled by source, target and modification date obtained from calling concordance
     *        search RESTful API.
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return A list of TmSearchResult
     */
    @Override
    public List<TmSearchResult> concordance(TmServerInfo tmServerInfo, TmSearchOption searchOption, TmInfo tmInfo,
        String securityTicket, String oauthToken) throws IOException
    {
        ConcordanceSearchRequest concordanceSearchRequest = new ConcordanceSearchRequest();
        List<TmSearchResult> result = new LinkedList<TmSearchResult>();
        HttpURLConnection conn = connect(tmServerInfo.getConcordanceURL(), false, oauthToken);
        concordanceSearchRequest.setSecurityTicket(securityTicket);
        concordanceSearchRequest.initConcordanceSearchRequest(searchOption);


        String requestText = objectMapper.writeValueAsString(concordanceSearchRequest);
        sendRequestContent(conn, requestText);
        String responseText = getHttpConnectionResponseAsText(conn);
        SearchResponse searchResponse = objectMapper.readValue(responseText, SearchResponse.class);

        int resultNum = searchResponse.getResultNum();
        if (resultNum == 0)
        {
            // No result is found.
            return null;
        }

        List<Segment> segments = searchResponse.getSegments();
        for (Segment segment : segments)
        {
            TmSearchResult sr = new TmSearchResult(tmInfo.getTmName(), tmInfo.getGroupName(), tmInfo.getSourceLanguage(), tmInfo.getTargetLanguage(), segment);
            result.add(sr);
        }
        conn.disconnect();
        return result;
    }

    /**
     * <p>
     * Helper function to open a new connection and set HttpURLConnection property as
     * application/json and configuration.
     * </p>
     *
     * @param String url, the URL that user wants to connect to
     * @param Boolean isExport, the boolean flag that determines whether the connection is needed to
     *        set output accept type as application/json. It is for Tm Export webservice only.
     * @throws IOException
     * @return HttpURLConnection con, which has an open and properly set connection.
     */
    @Override
    public HttpURLConnection connect(String url, boolean isExport, String oauthToken) throws IOException
    {
        URL object = new URL(url);

        HttpURLConnection conn = (HttpURLConnection) object.openConnection();
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestProperty(Constants.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        if (!isExport)
        {
            conn.setRequestProperty(Constants.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        }
        if (StringUtils.isNotEmpty(oauthToken))
        {
            conn.setRequestProperty(Constants.AUTHORIZATION, Constants.BEARER + oauthToken);
        }
        conn.setRequestMethod(Constants.POST);
        conn.setConnectTimeout(httpConnectionTimeout * 1000);
        conn.setReadTimeout(httpConnectionReadTimeout * 1000);
        return conn;
    }

    @Override
    public CreateTmResponse createTm(TmServerInfo tmServerInfo, TmCreateOption tmCreateOption, String securityTicket,
        String oauthToken) throws IOException
    {
        TmCreateRequest tmCreateRequest = new TmCreateRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            tmCreateRequest.setSecurityTicket(securityTicket);
        }
        HttpURLConnection conn = connect(tmServerInfo.getCreateTmURL(), false, oauthToken);

        if (!tmCreateOption.isAllTmCleanupPasscodeSame())
        {
            TmCleanupPasscodeResponse response = createTmCleanupPasscode(tmServerInfo, securityTicket, oauthToken);
            String newTmCleanupPassword = response.getCode();
            tmCreateOption.setCleanupCode(newTmCleanupPassword);
        }
        if (!tmCreateOption.isAllGenericUserpasswordSame())
        {
            GenerateUserPasswordResponse generateUserPasswordResponse = generateGenericUserpassword(tmServerInfo,
                securityTicket, oauthToken);
            String newGenericUserpassword = generateUserPasswordResponse.getPassword();
            tmCreateOption.updateGenericUserPassword(newGenericUserpassword);
        }

        if (StringUtils.isEmpty(tmCreateOption.getTmShortCode()))
        {
            CreateShortcodeResponse response = createTmShortCode(tmServerInfo, tmCreateOption.getTmName(), securityTicket, oauthToken);
            String tmShortCode = response.getShortCode();
            tmCreateOption.setTmShortCode(tmShortCode);
        }
        String tmName = tmCreateOption.getTmName();
        if (tmCreateOption.isAppendLanguagesToTmName())
        {
            tmCreateOption.setTmNameWithLanguagesCode();
        }

        tmCreateRequest.setTmCreateOption(tmCreateOption);

        String requestText = objectMapper.writeValueAsString(tmCreateRequest);
        sendRequestContent(conn, requestText);
        String responseText = getHttpConnectionResponseAsText(conn);
        CreateTmResponse createTmResponse = objectMapper.readValue(responseText, CreateTmResponse.class);

        if (!createTmResponse.isSuccess())
        {
            LOGGER.error(String.format(Messages.getString("CreateTmController.27"), requestText));
        }

        // reset Tm Name
        tmCreateOption.setTmName(tmName);
        return createTmResponse;
    }

    /**
     * <p>
     * A function to utilize create Tm Cleanup passcode RESTful API to create a Tm cleanup passcode
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "code":"fasdl#!131"
     * }</code> <br/>
     * <br/>
     * <o>Sample failed response data:
     * </p>
     * <code>{
     * "returnCode": 1,
     * "errorMessage": "Error message description"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return <code>TmCleanupPasscodeResponse</code> received from Tm Server
     */
    @Override
    public TmCleanupPasscodeResponse createTmCleanupPasscode(TmServerInfo tmServerInfo, String securityTicket,
        String oauthToken) throws IOException
    {
        TmServerRequest tmServerRequest = new TmServerRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            tmServerRequest.setSecurityTicket(securityTicket);
        }
        HttpURLConnection conn = connect(tmServerInfo.getCreateTmCleanupPasscodeURL(), false, oauthToken);

        sendRequestContent(conn, objectMapper.writeValueAsString(tmServerRequest));

        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, TmCleanupPasscodeResponse.class);
    }

    /**
     * <p>
     * A function to utilize with REST API to create Tm Group from TM Server.
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "36b6225d-f995-47b2-9c17-4d999b075a20",
     * "organizationId":"orgId1",
     * "projectName":"Test project",
     * "shortcode":"TES000001"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true
     * }</code> <br/>
     * <br/>
     * <o>Sample failed response data:
     * </p>
     * <code>{
     * "returnCode": 11,
     * "errorMessage": "Error message description"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param TmGroupOption tmGroupOption, which contains all necessary request options for createTm
     *        RESTFul API.
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return <code>TMServerResponse</code> received from Tm Server
     */
    @Override
    public TmGroupCreateResponse createTmGroup(TmServerInfo tmServerInfo, TmGroupOption tmGroupOption,
        String securityTicket, String oauthToken) throws IOException
    {
        CreateTmGroupRequest createTmGroupRequest = new CreateTmGroupRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            createTmGroupRequest.setSecurityTicket(securityTicket);
        }
        createTmGroupRequest.initCreateTmGroupRequest(tmGroupOption);
        HttpURLConnection conn = connect(tmServerInfo.getCreateTmGroupURL(), false, oauthToken);
        String requestText = objectMapper.writeValueAsString(createTmGroupRequest);
        sendRequestContent(conn, requestText);
        String responseText = getHttpConnectionResponseAsText(conn);

        TmGroupCreateResponse response = objectMapper.readValue(responseText, TmGroupCreateResponse.class);

        if (!response.isSuccess())
        {
            String errorMessage = String.format(Messages.getString("TmServerAPIImpl.0"), response.getErrorMessage());
            LOGGER.error(errorMessage);
        }
        conn.disconnect();
        return response;
    }

    /**
     * A function utilize create TmGroupCode RESTful API to create a TM Groupcode based on provided TM
     * Group name
     *
     * @param TmServerInfo tmServerInfo
     * @param String tmGroup
     * @param String securityTicket
     * @throws IOException
     */
    @Override
    public CreateShortcodeResponse createTmGroupShortCode(TmServerInfo tmServerInfo,
        String tmGroupName, String securityTicket, String oauthToken) throws IOException
    {
        CreateShortcodeRequest createShortCodeRequest = new CreateShortcodeRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            createShortCodeRequest.setSecurityTicket(securityTicket);
        }
        HttpURLConnection conn = connect(tmServerInfo.getCreatTmGroupCodeURL(), false, oauthToken);
        createShortCodeRequest.setName(tmGroupName);
        sendRequestContent(conn, objectMapper.writeValueAsString(createShortCodeRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, CreateShortcodeResponse.class);
    }

    /**
     * <p>
     * A function to utilize create tmShortCode RESTful API to create a Tm ShortCode based on
     * provided Tm Name
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013",
     * "name": "Test TM"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "shortCode":"TES000001"
     * }</code> <br/>
     * <br/>
     * <o>Sample failed response data:
     * </p>
     * <code>{
     * "returnCode": 1,
     * "errorMessage": "Error message description"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param String tmName
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return <code>CreateShortcodeResponse</code> received from Tm Server
     */
    @Override
    public CreateShortcodeResponse createTmShortCode(TmServerInfo tmServerInfo, String tmName, String securityTicket, String oauthToken)
        throws IOException
    {
        CreateShortcodeRequest createShortCodeRequest = new CreateShortcodeRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            createShortCodeRequest.setSecurityTicket(securityTicket);
        }
        HttpURLConnection conn = connect(tmServerInfo.getCreateTmShortcodeURL(), false, oauthToken);
        createShortCodeRequest.setName(tmName);
        sendRequestContent(conn, objectMapper.writeValueAsString(createShortCodeRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, CreateShortcodeResponse.class);
    }

    /**
     * <p>
     * A function to utilize ExportTM REST API by providing corresponding Tm request data to Tm
     * Server and export the file from outputStream to FTP
     * <p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "exportType": "TMX",
     * "includePrivateTus": "true",
     * "includeAttributes":"true",
     * "securityTicket": "496973ad-e6a7-4b2b-9eef-6934b80b08c2"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param TmExportOption, which contains all necessary request options for ExportTm RESTFul API.
     * @param TmFtpAdaptor tmFtpConfiguration, the FTP info where the exported Tm will sent
     *        to.
     * @param String securityTicket, a String obtained from successfully login.
     * @param boolean whether user wants to export TMX files to FTP or directly to browser.
     * @throws IOException
     * @return ResponseEntity<?>
     */
    @Override
    public ResponseEntity<?> exportTm(TmServerInfo tmServerInfo, TmExportOption tmExportOption, String securityTicket,
        String oauthToken, ExportTmInfo info) throws IOException
    {
        TmExportRequest tmExportRequest = new TmExportRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            tmExportRequest.setSecurityTicket(securityTicket);
        }
        else
        {
            tmExportRequest.setTmLocation(tmExportOption.getTmPath());
        }
        tmExportRequest.setExportOption(tmExportOption);
        tmExportRequest.setExportFilter(tmExportOption.getExportFilter());

        System.setProperty("jsse.enableSNIExtension", "false");
        HttpURLConnection conn = connect(tmServerInfo.getExportURL(), true, oauthToken);

        String tmExportRequestText = objectMapper.writeValueAsString(tmExportRequest);
        sendRequestContent(conn, tmExportRequestText);

        TmExportResponse tmExportResponse = new TmExportResponse();
        if (conn.getResponseCode() == HttpStatus.INTERNAL_SERVER_ERROR.value())
        {
            conn.disconnect();
            String message = Messages.getString("ExportTmController.4");
            tmExportResponse.setMessage(message);
            tmExportResponse.setSuccess(false);
            if (info != null)
            {
                info.setExportTmPhaseFailed();
                info.setMessage(message);
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(tmExportResponse);
        }

        // opens input stream from the HTTP connection
        InputStream inputStream = conn.getInputStream();
        // TODO: cannot remain inputstream after get filename
        String tmPath = tmExportOption.getTmPath().replace(Constants.SLASH, Constants.UNDERSCORE);
        String tmxFileName = createTmxFileName(tmExportOption, tmPath);
        String date = new SimpleDateFormat(Constants.DATEFORMATE).format(new Date());

        String message = String.format(Messages.getString("ExportTmController.5"), tmExportOption.getTmName(), tmPath);
        if (tmExportOption.isExportedToFtp())
        {
            // Save the file to FTP:
            tmFtpAdaptor.exportFileToFtp(inputStream, tmxFileName, date);

            tmExportResponse.setMessage(message);
            tmExportResponse.setSuccess(false);
            LOGGER.info(message);

            if (info != null)
            {
                info.setMessage(message);
                info.setExportToFtp(true);
            }
            return ResponseEntity.status(HttpStatus.OK).body(tmExportResponse);
        }
        else
        {
            // redirect file to front end:
            String fileType = URLConnection.guessContentTypeFromName(tmxFileName);
            byte[] out = IOUtils.toByteArray(inputStream);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("content-disposition", "attachment; filename=" + tmxFileName);
            responseHeaders.add(Constants.CONTENT_TYPE, fileType);
            responseHeaders.add("filename", tmxFileName);

            ResponseEntity<byte[]> respEntity = new ResponseEntity<byte[]>(out, responseHeaders, HttpStatus.OK);
            LOGGER.info(message);

            if (info != null)
            {
                info.setExportToFtp(false);
                info.setFileType(fileType);
                info.setOut(out);
                info.setTmxFileName(tmxFileName);
            }
            return respEntity;
        }
    }

    private String createTmxFileName(TmExportOption tmExportOption, String tmPath)
    {
        // String tmxFileName = getFileName(inputStream) + ".zip";
        // Construct ExportTMX Name with TMName + TMPath
        StringBuilder sb = new StringBuilder();
        sb.append(tmExportOption.getTmGroupName()).append(Constants.UNDERSCORE);
        sb.append(tmExportOption.getTmName()).append(Constants.UNDERSCORE);
        sb.append(tmExportOption.getSourceLocaleCode()).append(Constants.UNDERSCORE);
        sb.append(tmExportOption.getTargetLocaleCode()).append(Constants.UNDERSCORE);
        sb.append(tmPath).append(Constants.ZIPEXTENSION);
        return sb.toString();
    }

    /**
     * <p>
     * A function to utilize create tmGenericUsername RESTful API to create a Tm generic username
     * based on provided TmGenericUsernameOption
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013",
     * "tmName":"test tm",
     * "sourceLanguage":"en-US", // optional
     * "targetLanguage":"de-DE"  // optional
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "username": "testtm_en-US_de-DE1"
     * }</code> <br/>
     * <br/>
     * <o>Sample failed response data:
     * </p>
     * <code>{
     * "returnCode": 1,
     * "errorMessage": "Error message description"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param TmGenericUsernameOption tmGenericUsernameOption
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return <code>GenerateUsernameResponse</code> received from Tm Server
     */
    @Override
    public GenerateUsernameResponse generateGenericUsername(TmServerInfo tmServerInfo,
        TmGenericUsernameOption tmGenericUsernameOption, String securityTicket, String oauthToken)
        throws IOException
    {
        GenerateUsernameRequest generateUsernameRequest = new GenerateUsernameRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            generateUsernameRequest.setSecurityTicket(securityTicket);
        }
        HttpURLConnection conn = connect(tmServerInfo.getGenerateGenericUsernameURL(), false, oauthToken);
        generateUsernameRequest.initGenerateUsernameRequest(tmGenericUsernameOption);
        sendRequestContent(conn, objectMapper.writeValueAsString(generateUsernameRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, GenerateUsernameResponse.class);
    }

    /**
     * <p>
     * A function to utilize create tmGenericPassword RESTful API to create a Tm generic password based
     * on provided TmGenericUsernameOption
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "password":"f1asd@l131"
     * }</code> <br/>
     * <br/>
     * <o>Sample failed response data:
     * </p>
     * <code>{
     * "returnCode": 1,
     * "errorMessage": "Error message description"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param TmGenericUsernameOption tmGenericUsernameOption
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return <code>GenerateUserPasswordResponse</code> received from Tm Server
     */
    @Override
    public GenerateUserPasswordResponse generateGenericUserpassword(TmServerInfo tmServerInfo, String securityTicket, String oauthToken) throws IOException
    {
        TmServerRequest tmServerRequest = new TmServerRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            tmServerRequest.setSecurityTicket(securityTicket);
        }

        HttpURLConnection conn = connect(tmServerInfo.getGenerateGenericUserPasswordURL(), false, oauthToken);

        sendRequestContent(conn, objectMapper.writeValueAsString(tmServerRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, GenerateUserPasswordResponse.class);
    }

    /**
     * <p>
     * A function to utilize getGenericUserRole RESTful API to create a Tm generic password based on
     * provided TmGenericUsernameOption
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "roles": [read, read/write]
     * }</code> <br/>
     * <br/>
     * <o>Sample failed response data:
     * </p>
     * <code>{
     * "returnCode": 1,
     * "errorMessage": "Error message description"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param TmGenericUsernameOption tmGenericUsernameOption
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return A GenericRolesResponse of roles description.
     */
    @Override
    public GenericRolesResponse getGenericRoles(TmServerInfo tmServerInfo, String securityTicket, String oauthToken)
        throws IOException
    {
        TmServerRequest tmServerRequest = new TmServerRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            tmServerRequest.setSecurityTicket(securityTicket);
        }
        HttpURLConnection conn = connect(tmServerInfo.getGenericRolesURL(), false, oauthToken);
        sendRequestContent(conn, objectMapper.writeValueAsString(tmServerRequest));

        String responseText = getHttpConnectionResponseAsText(conn);
        GenericRolesResponse response = objectMapper.readValue(responseText, GenericRolesResponse.class);
        if (!response.isSuccess())
        {
            String errorMessage = String.format(Messages.getString("TmServerAPIImpl.1"), objectMapper.writeValueAsString(response));
            LOGGER.warn(errorMessage);
            return null;
        }
        conn.disconnect();
        return response;
    }

    /**
     * <p>
     * A function to call getTmGroupList REST API to get all Tm Group from TM Server.
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>
     * {
     * "securityTicket": "36b6225d-f995-47b2-9c17-4d999b075a20",
     * "organizationId":"orgId1",
     * "projectName":"Test project",
     * "shortcode":"TES000001"
     * }
     * </code>
     * <p>
     * Sample response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "projects": [
     * {
     * 	"id": " id#1",
     * "name": "Project  #1"
     * },
     * {
     * "id": "id#2",
     * "name": "Project  #2"
     * }
     * ]
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return List of TmGroup
     */
    @Override
    public FetchTmGroupsResponse getGroupList(TmServerInfo tmServerInfo, String securityTicket, String oauthToken)
        throws IOException
    {
        TmServerRequest tmServerRequest = new TmServerRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            tmServerRequest.setSecurityTicket(securityTicket);
        }

        HttpURLConnection conn = connect(tmServerInfo.getGroupListURL(), false, oauthToken);
        sendRequestContent(conn, objectMapper.writeValueAsString(tmServerRequest));
        String responseText = getHttpConnectionResponseAsText(conn);

        FetchTmGroupsResponse response = objectMapper.readValue(responseText, FetchTmGroupsResponse.class);

        if (!response.isSuccess())
        {
            String message = Messages.getString("TmServerAPIImpl.2");
            LOGGER.warn(message);
        }
        else
        {
            List<TmGroup> tmGroups = response.getProjects();
            // Sort the Tm Group by name
            tmGroups.sort((TmGroup o1, TmGroup o2) -> o1.getName().compareTo(o2.getName()));
            tmGroups.forEach(tmGroup -> {
                tmGroup.moveProperties();
            });
            response.setProjects(tmGroups);
        }
        conn.disconnect();
        return response;
    }

    @Override
    public ImportProcessResponse getImportProgress(TmServerInfo tmServerInfo, String jobId,
        String securityTicket, String oauthToken) throws IOException
    {
        ImportProcessRequest importProcessRequest = new ImportProcessRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            importProcessRequest.setSecurityTicket(securityTicket);
        }
        importProcessRequest.setJobId(jobId);
        HttpURLConnection conn = connect(tmServerInfo.getImportTmProgressURL(), false, oauthToken);

        sendRequestContent(conn, objectMapper.writeValueAsString(importProcessRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, ImportProcessResponse.class);
    }

    /**
     * A function to utilize getLanguageList REST API to get all Tm Group from TM Server.
     * <p>
     * Sample POST request content:
     * </p>
     * <code>
     * {
     * "securityTicket": "36b6225d-f995-47b2-9c17-4d999b075a20",
     * "organizationId":"orgId1",
     * "projectName":"Test project",
     * "shortcode":"TES000001"
     * }
     * </code>
     * <p>
     * Sample response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "languages": [
     * {
     * "locale": "en_US",
     * "value": "English (United States)"
     * },
     * {
     * "locale": "fr_FR",
     * "value": "French (France)"
     * }
     * ]
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return List of TmLanguage
     */
    @Override
    public FetchTmLanguageResponse getLanguageList(TmServerInfo tmServerInfo, String securityTicket, String oauthToken)
        throws IOException
    {
        TmServerRequest tmServerRequest = new TmServerRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            tmServerRequest.setSecurityTicket(securityTicket);
        }
        HttpURLConnection conn = connect(tmServerInfo.getLanguageListURL(), false, oauthToken);

        sendRequestContent(conn, objectMapper.writeValueAsString(tmServerRequest));
        String responseText = getHttpConnectionResponseAsText(conn);

        FetchTmLanguageResponse response = objectMapper.readValue(responseText, FetchTmLanguageResponse.class);

        if (!response.isSuccess())
        {
            String message = Messages.getString("TmServerAPIImpl.3");
            LOGGER.warn(message);
        }
        else
        {
            List<TmLanguage> tmLanguages = response.getLanguages();
            tmLanguages.forEach(language -> {
                language.moveProperty();
            });
        }
        conn.disconnect();
        return response;
    }

    /**
     * <p>
     * A function to call getOrganizationList REST API to get all Organization from TM Server.
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013"
     * }</code>
     * <p>
     * Sample response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "organizations": [
     * {
     * "id": "Org_id#1",
     * "name": "Organization #1"
     * },
     * {
     * "id": "Org_id#2",
     * "name": "Organization #1"
     * }
     * ]
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return List of TmOrganization
     */
    @Override
    public FetchOrganizationsResponse getOrganizationList(TmServerInfo tmServerInfo, String securityTicket, String oauthToken)
        throws IOException
    {
        TmServerRequest tmServerRequest = new TmServerRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            tmServerRequest.setSecurityTicket(securityTicket);
        }

        HttpURLConnection conn = connect(tmServerInfo.getOrganizationListURL(), false, oauthToken);

        sendRequestContent(conn, objectMapper.writeValueAsString(tmServerRequest));
        String responseText = getHttpConnectionResponseAsText(conn);

        FetchOrganizationsResponse response = objectMapper.readValue(responseText, FetchOrganizationsResponse.class);
        if (!response.isSuccess())
        {
            String message = String.format(Messages.getString("TmServerAPIImpl.4"), responseText);
            LOGGER.warn(message);
        }
        else
        {
            List<TmOrganization> organizations = response.getOrganizations();
            // Sort the Organization by name
            Collections.sort(organizations, (o1, o2) -> (o1.getName().compareTo(o2.getName())));
            organizations.forEach(organization -> {
                organization.moveProperties();
            });
            response.setOrganizations(organizations);
        }
        conn.disconnect();
        return response;
    }

    private String getHttpConnectionResponseAsText(HttpURLConnection conn) throws IOException
    {
        StringBuffer sb = new StringBuffer();
        InputStreamReader reader = null;
        BufferedReader br = null;
        try
        {
            reader = new InputStreamReader((conn.getInputStream()));
            br = new BufferedReader(reader);
            String output;
            while ((output = br.readLine()) != null)
            {
                sb.append(output);
            }
        }
        finally
        {
            if (reader != null)
            {
                reader.close();
            }
            if (br != null)
            {
                br.close();
            }
        }
        return sb.toString();
    }

    @Override
    public TmListResponse fetchTmList(TmServerInfo tmServerInfo) throws IOException
    {
        TmListRequest tmListRequest = new TmListRequest();
        tmListRequest.initTmListRequest(tmServerInfo);
        return fetchTmList(tmServerInfo, tmListRequest);
    }

    @Override
    public TmListResponse fetchTmListWithTmGroupCodes(TmServerInfo tmServerInfo, Set<String> tmGroupShortCodes) throws IOException
    {
        TmListResponse response = null;
        for (String tmGroupShortCode : tmGroupShortCodes)
        {
            TmListRequest tmListRequest = new TmListRequest();
            tmListRequest.initTmListRequest(tmServerInfo);
            tmListRequest.setTmGroupShortCode(tmGroupShortCode);
            try
            {
                TmListResponse subResponse = fetchTmList(tmServerInfo, tmListRequest);
                if (response == null)
                {
                    response = subResponse;
                }
                else
                {
                    response.addTmList(subResponse.getTmList());
                }
            }
            catch (IOException ex)
            {
                String message = String.format(Messages.getString("TmServerAPIImpl.7"), tmServerInfo.getTmListUrl(),
                    tmServerInfo.getTmUserName(), tmGroupShortCode, ex.getMessage());
                LOGGER.error(message);
            }
        }

        return response;
    }

    private TmListResponse fetchTmList(TmServerInfo tmServerInfo, TmListRequest tmListRequest) throws IOException
    {
        HttpURLConnection conn = connect(tmServerInfo.getTmListUrl(), false, null);

        sendRequestContent(conn, objectMapper.writeValueAsString(tmListRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        TmListResponse response = objectMapper.readValue(responseText, TmListResponse.class);

        if (response.getReturnCode() != 0)
        {
            String message = String.format(Messages.getString("TmServerAPIImpl.7"), tmServerInfo.getTmListUrl(),
                tmServerInfo.getTmUserName());
            LOGGER.warn(message);
        }
        else
        {
            // TM Server does not have success value
            response.setSuccess(true);
            // filter out TM with only enabled:
            List<TmInfo> enabledTmList = new ArrayList<>();
            for (TmInfo tmInfo : response.getTmList())
            {
                if (tmInfo.isEnabled())
                {
                    enabledTmList.add(tmInfo);
                }
            }
            response.setTmList(enabledTmList);
        }
        conn.disconnect();
        return response;
    }

    @Override
    public LoginResponse getTmServerVersion(TmServerInfo tmServerInfo, String tmLocation) throws IOException
    {
        LoginRequest loginRequest = new LoginRequest();
        System.setProperty(JSSE_ENABLE_SNI_EXTENSION, String.valueOf(false));
        HttpURLConnection conn = connect(tmServerInfo.getLoginURL(), false, null);
        loginRequest.setInfo(tmServerInfo, tmLocation);
        sendRequestContent(conn, objectMapper.writeValueAsString(loginRequest));

        String responseText = getHttpConnectionResponseAsText(conn);
        LoginResponse loginResponse = objectMapper.readValue(responseText, LoginResponse.class);

        conn.disconnect();
        if (loginResponse.isSuccess())
        {
            String securityTicket = loginResponse.getSecurityTicket();
            logout(tmServerInfo, securityTicket);
        }
        return loginResponse;
    }

    /**
     * <p>
     * The getTuInfo function to utilize getTuInfo RESTful API to get various TU info from the TM
     * Server.
     * </p>
     * <p>
     * This function will set
     * <code>numberOfPrivateTus, LastTuModificationTimestamp, numberOfPublicTus</code>, which are
     * obtained from RESTful API response data, in TmInfo
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "tuInfo": {
     * "numberOfPrivateTus": 0,
     * "totalNumberOfTus": 1,
     * "lastTuModificationTimestamp": 1476972107943,
     * "numberOfPublicTus": 1
     * }
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param MixedTmInfo tmInfo, which contains various TM Info.
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return TmTuInfoResponse
     */
    @Override
    public TmTuInfoResponse getTuInfo(TmServerInfo tmServerInfo, String securityTicket, String oauthToken) throws IOException
    {
        TmServerRequest tmServerRequest = new TmServerRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            tmServerRequest.setSecurityTicket(securityTicket);
        }
        HttpURLConnection conn = connect(tmServerInfo.getTuInfoURL(), false, oauthToken);

        sendRequestContent(conn, objectMapper.writeValueAsString(tmServerRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        // Get TU info of each TM and store them into TmListInfo object.
        TmTuInfoResponse response = objectMapper.readValue(responseText, TmTuInfoResponse.class);
        conn.disconnect();
        return response;
    }

    @Override
    public ImportTmResponse importTm(TmServerInfo tmServerInfo, TmImportOption tmImportOption, File uploadFile,
        String securityTicket, String oauthToken, boolean isTPAuth) throws IOException
    {
        HttpURLConnection conn = tmImportConnect(tmServerInfo.getImportTmURL(), oauthToken, isTPAuth);

        OutputStream out = conn.getOutputStream();
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out, UTF8_CHAR_SET), true);

        // Append security Ticket
        if (!isTPAuth && StringUtils.isNotEmpty(securityTicket))
        {
            writer.append("--" + BOUNDARY).append(Constants.CRLF);
            writer.append("Content-Disposition: form-data; name=\"securityTicket\"").append(Constants.CRLF);
            writer.append("Content-Type: text/plain; charset=" + UTF8_CHAR_SET).append(Constants.CRLF);
            writer.append(Constants.CRLF).append(securityTicket).append(Constants.CRLF).flush();
        }

        // Append Tm Attribute Update Option
        writer.append("--" + BOUNDARY).append(Constants.CRLF);
        writer.append("Content-Disposition: form-data; name=\"attributeUpdateOption\"").append(Constants.CRLF);
        writer.append("Content-Type: text/plain; charset=" + UTF8_CHAR_SET).append(Constants.CRLF);
        writer.append(Constants.CRLF).append(tmImportOption.getTmAttributeImportOption()).append(Constants.CRLF).flush();

        // Append Tm Import Option:
        writer.append("--" + BOUNDARY).append(Constants.CRLF);
        writer.append("Content-Disposition: form-data; name=\"importOption\"").append(Constants.CRLF);
        writer.append("Content-Type: text/plain; charset=" + UTF8_CHAR_SET).append(Constants.CRLF);
        writer.append(Constants.CRLF).append(tmImportOption.getTmImportOption()).append(Constants.CRLF).flush();

        // Append Ignore Invalid TUs option: available on TM Server 5.4
        if (tmImportOption.isIgnoreInvalidTus())
        {
            writer.append("--" + BOUNDARY).append(Constants.CRLF);
            writer.append("Content-Disposition: form-data; name=\"ignoreInvalidTus\"").append(Constants.CRLF);
            writer.append("Content-Type: text/plain; charset=" + UTF8_CHAR_SET).append(Constants.CRLF);
            writer.append(Constants.CRLF).append(String.valueOf(tmImportOption.isIgnoreInvalidTus())).append(Constants.CRLF).flush();
        }

        // Append Keep Newer Tus option: available on TM Server 5.4
        if (tmImportOption.isKeepNewerTu())
        {
            writer.append("--" + BOUNDARY).append(Constants.CRLF);
            writer.append("Content-Disposition: form-data; name=\"keepNewer\"").append(Constants.CRLF);
            writer.append("Content-Type: text/plain; charset=" + UTF8_CHAR_SET).append(Constants.CRLF);
            writer.append(Constants.CRLF).append(String.valueOf(tmImportOption.isKeepNewerTu())).append(Constants.CRLF).flush();
        }

        // Append Tm Path:
        writer.append("--" + BOUNDARY).append(Constants.CRLF);
        writer.append("Content-Disposition: form-data; name=\"tm\"").append(Constants.CRLF);
        writer.append("Content-Type: text/plain; charset=" + UTF8_CHAR_SET).append(Constants.CRLF);
        writer.append(Constants.CRLF).append(tmImportOption.getTmPath()).append(Constants.CRLF).flush();

        // Append Tmx File as Binary Data:
        // Send binary file.
        writer.append("--" + BOUNDARY).append(Constants.CRLF);
        writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" +
                uploadFile.getName() + "\"").append(Constants.CRLF);
        writer.append("Content-Type: application/octet-stream").append(Constants.CRLF);
        writer.append("Content-Transfer-Encoding: binary").append(Constants.CRLF);
        writer.append(Constants.CRLF).flush();
        Files.copy(uploadFile.toPath(), out);
        out.flush(); // Important before continuing with writer!
        writer.append(Constants.CRLF).flush(); // CRLF is important! It indicates end of boundary.

        // End of multipart/form-data.
        writer.append("--" + BOUNDARY + "--").append(Constants.CRLF).flush();

        // Close the streams
        out.close();
        writer.close();

        String fileName = uploadFile.getName();
        // Check connection is 500 internal server error:
        if (conn.getResponseCode() == HttpStatus.INTERNAL_SERVER_ERROR.value())
        {
            conn.disconnect();
            String errorMessage = String.format(Messages.getString("TmServerAPIImpl.12"), fileName);
            LOGGER.error(errorMessage);
            ImportTmResponse importTmResponse = new ImportTmResponse();
            importTmResponse.setSuccess(false);
            importTmResponse.setFileName(fileName);
            importTmResponse.setErrorMessage(errorMessage);
            return importTmResponse;
        }

        // get HTTP Response from Server:
        String responseText = getHttpConnectionResponseAsText(conn);
        LOGGER.debug(String.format(Messages.getString("TmServerAPIImpl.11"), fileName, tmImportOption.getTmName(), responseText));

        conn.disconnect();
        ImportTmResponse importTmResponse = objectMapper.readValue(responseText, ImportTmResponse.class);
        importTmResponse.setFileName(uploadFile.getName());

        if (!importTmResponse.isSuccess())
        {
            String message = String.format(Messages.getString("TmServerAPIImpl.8"), responseText);
            LOGGER.warn(message);
        }
        return importTmResponse;
    }

    /**
     * <p>
     * A function that takes TmServerInfo object and tmLocation String as inputs to utilize login
     * RESTful API to connect to Tm Server.
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "username": "testadmin",
     *	"password": "password1!",
     *	"tmLocation": "PRO000001/MAR000001"
     * } </code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>
     * {
     * "version": "4.5.1",
     * "sourceLanguage": "en-US",
     * "targetLanguage": "de-DE",
     * "readOnly": true,
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013",
     * "success": true
     * "returnCode": 0,
     * "time_ms": 15
     * // “attributes”: []
     * }
     * </code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password info
     * @param String tmLocation, the Tm Path that user wants to connect to.
     * @throws IOException
     * @return LoginResponse contains securityTicket, which will be used in following web services.
     */
    @Override
    public LoginResponse login(TmServerInfo tmServerInfo, String tmLocation) throws IOException
    {
        LoginRequest loginRequest = new LoginRequest();
        System.setProperty(JSSE_ENABLE_SNI_EXTENSION, String.valueOf(false));
        HttpURLConnection conn = connect(tmServerInfo.getLoginURL(), false, null);
        loginRequest.setInfo(tmServerInfo, tmLocation);

        sendRequestContent(conn, objectMapper.writeValueAsString(loginRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        LoginResponse response = objectMapper.readValue(responseText, LoginResponse.class);

        if (!response.isSuccess())
        {
            String message = String.format(Messages.getString("TmServerAPIImpl.9"),
                objectMapper.writeValueAsString(loginRequest), responseText);
            LOGGER.error(message);
        }
        conn.disconnect();
        return response;
    }

    /**
     * <p>
     * The logout function to utilize logout RESTful API to logout from Tm Server.
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "success": true,
     * "returnCode": 0,
     * "time_ms": 5
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password info
     * @param String securityTicket, a String obtained from successfully login.
     * @return none
     */
    @Override
    public void logout(TmServerInfo tmServerInfo, String securityTicket)
    {
        TmServerRequest tmServerRequest = new TmServerRequest();
        HttpURLConnection conn = null;
        try
        {
            conn = connect(tmServerInfo.getLogoutURL(), false, null);
            tmServerRequest.setSecurityTicket(securityTicket);
            sendRequestContent(conn, objectMapper.writeValueAsString(tmServerRequest));
        }
        catch (IOException ex)
        {
            String message = String.format(Messages.getString("TmServerAPIImpl.10"), ex.getMessage());
            LOGGER.error(message);
        }
        finally
        {
            if (conn != null)
            {
                conn.disconnect();
            }
        }
    }

    /**
     * <p>
     * The search function to utilize Search RESTful API to perform searching on given text in
     * TmSearchOption and return a list of TmSearchResult.
     * </p>
     *
     * @throws IOException
     * @return A list of TmSearchResult
     */
    @Override
    public List<TmSearchResult> search(TmServerInfo tmServerInfo, TmSearchOption searchOption,
        TmInfo tmInfo, String securityTicket, String oauthToken) throws IOException
    {
        SearchRequest searchRequest = new SearchRequest();
        List<TmSearchResult> result = new LinkedList<TmSearchResult>();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            searchRequest.setSecurityTicket(securityTicket);
        }
        searchRequest.setSearchOption(searchOption);

        HttpURLConnection conn = connect(tmServerInfo.getSearchURL(), false, oauthToken);
        sendRequestContent(conn, objectMapper.writeValueAsString(searchRequest));

        String responseText = getHttpConnectionResponseAsText(conn);
        SearchResponse searchResponse = objectMapper.readValue(responseText, SearchResponse.class);

        int resultNum = searchResponse.getResultNum();
        if (resultNum == 0)
        {
            // No result is found.
            return result;
        }
        List<Segment> segments = searchResponse.getSegments();
        for (Segment segment : segments)
        {
            TmSearchResult sr = new TmSearchResult(tmInfo.getTmName(),
                tmInfo.getGroupName(), tmInfo.getSourceLanguage(), tmInfo.getTargetLanguage(), segment);

            result.add(sr);
        }
        conn.disconnect();
        return result;
    }

    /**
     * <p>
     * Helper function that uses outputStream to write JSONObject request content.
     * </p>
     *
     * @param HttpURLConnection con, the httpURLconnection obtained from connect function.
     * @param String content, the JSONObject content converted as String
     * @throws IOException
     * @return boolean, <code>true</code> for connect successfully, <code>false</code> for fail to
     *         connect.
     */
    private boolean sendRequestContent(HttpURLConnection con, String content) throws IOException
    {
        OutputStream os = null;
        try
        {
            os = con.getOutputStream();
            os.write(content.getBytes());
            os.flush();
            if (con.getResponseCode() != HttpURLConnection.HTTP_OK)
            {
                LOGGER.error(
                    "Tm Server Tool: connection error: " + con.getResponseMessage() +
                        " with sent content: " + content);
                return false;
            }
        }
        finally
        {
            if (os != null)
            {
                os.close();
            }
        }
        return true;
    }

    /**
     * <p>
     * Helper function to open a new connection for Tm Import web service and set configurations
     * properly.
     * </p>
     *
     * @param String url, the Tm Import web service URL
     * @throws IOException
     * @return HttpURLConnection con, which has an open and properly set connection.
     */
    private HttpURLConnection tmImportConnect(String url, String oauthToken, boolean isTPAuth) throws IOException
    {
        URL object = new URL(url);
        HttpURLConnection con = (HttpURLConnection) object.openConnection();
        // Indicate that we want to write to the HTTP request body
        con.setDoOutput(true);
        if (StringUtils.isNotEmpty(oauthToken) && isTPAuth)
        {
            con.setRequestProperty(Constants.AUTHORIZATION, Constants.BEARER + oauthToken);
        }
        con.setRequestMethod(Constants.POST);
        con.setRequestProperty(Constants.CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA_VALUE + "; boundary=" + BOUNDARY);
        // Indicate that we want to write some data as the HTTP request body
        con.setDoOutput(true);
        return con;
    }

    /**
     * <p>
     * A function to utilize validate Tm Cleanup passcode RESTful API to validate a Tm cleanup passcode
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013",
     * "value":"alskdjlk#@13"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "valid":true
     * }</code> <br/>
     * <br/>
     * <o>Sample failed response data:
     * </p>
     * <code>{
     * "returnCode": 1,
     * "errorMessage": "Error message description"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param String tmCleanupPasscode, the passcode that is needed to be validated.
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return <code>ValidateCodeResponse</code> received from Tm Server
     */
    @Override
    public ValidateCodeResponse validateTmCleanupPasscode(TmServerInfo tmServerInfo, String tmCleanupPasscode, String securityTicket,
        String oauthToken) throws IOException
    {
        ValidateCodeRequest validateCodeRequest = new ValidateCodeRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            validateCodeRequest.setSecurityTicket(securityTicket);
        }
        validateCodeRequest.setValue(tmCleanupPasscode);
        HttpURLConnection conn = connect(tmServerInfo.getValidateTmPasscodeURL(), false, oauthToken);

        sendRequestContent(conn, objectMapper.writeValueAsString(validateCodeRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, ValidateCodeResponse.class);
    }

    /**
     * <p>
     * A function to validate Tm Group Code entered by user.
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013",
     * "value":"TES000001"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "valid":true
     * }</code> <br/>
     * <br/>
     * <o>Sample failed response data:
     * </p>
     * <code>{
     * "returnCode": 1,
     * "errorMessage": "Error message description"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param String tmGroupCode
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return <code>ValidateCodeResponse</code> received from Tm Server
     */
    @Override
    public ValidateCodeResponse validateTmGroupCode(TmServerInfo tmServerInfo, String tmGroupCode, String securityTicket,
        String oauthToken) throws IOException
    {
        ValidateCodeRequest validateCodeRequest = new ValidateCodeRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            validateCodeRequest.setSecurityTicket(securityTicket);
        }
        validateCodeRequest.setValue(tmGroupCode);

        HttpURLConnection conn = connect(tmServerInfo.getValidateTmGroupCodeURL(), false, oauthToken);
        sendRequestContent(conn, objectMapper.writeValueAsString(validateCodeRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, ValidateCodeResponse.class);
    }

    /**
     * <p>
     * A function to utilize validate tmShortCode RESTful API to validate a Tm ShortCode with provided
     * Tm ShortCode
     * </p>
     * <p>
     * Sample POST request content:
     * </p>
     * <code>{
     * "securityTicket": "de305d54-75b4-431b-adb2-eb6b9e546013",
     * "value":"TES000001"
     * }</code>
     * <p>
     * Sample successful response data:
     * </p>
     * <code>{
     * "returnCode": 0,
     * "success": true,
     * "valid":true
     * }</code> <br/>
     * <br/>
     * <o>Sample failed response data:
     * </p>
     * <code>{
     * "returnCode": 1,
     * "errorMessage": "Error message description"
     * }</code> <br/>
     * <br/>
     *
     * @param TmServerInfo tmServerInfo, which contains all web services URL, username and password
     *        info.
     * @param String tmCleanupPasscode, the passcode that is needed to be validated.
     * @param Strign tmShortCode, obtained from user input in front end.
     * @param String securityTicket, a String obtained from successfully login.
     * @throws IOException
     * @return <code>ValidateCodeResponse</code> received from Tm Server
     */
    @Override
    public ValidateCodeResponse validateTmShortCode(TmServerInfo tmServerInfo, String tmShortCode, String securityTicket, String oauthToken) throws IOException
    {
        ValidateCodeRequest validateCodeRequest = new ValidateCodeRequest();
        boolean isTPAuth = StringUtils.isNotEmpty(oauthToken);
        if (!isTPAuth)
        {
            validateCodeRequest.setSecurityTicket(securityTicket);
        }
        validateCodeRequest.setValue(tmShortCode);
        HttpURLConnection conn = connect(tmServerInfo.getValidateTmShortcodeURL(), false, oauthToken);
        sendRequestContent(conn, objectMapper.writeValueAsString(validateCodeRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, ValidateCodeResponse.class);
    }

    @Override
    public ValidateOAuthTokenResponse validateOAuthToken(TmServerInfo tmServerInfo, String oauthToken) throws IOException
    {
        ValidateOAuthTokenRequest validateOAuthTokenRequest = new ValidateOAuthTokenRequest();
        HttpURLConnection conn = connect(tmServerInfo.getValidateOAuthTokenUrl(), false, null);
        validateOAuthTokenRequest.setToken(oauthToken);
        sendRequestContent(conn, objectMapper.writeValueAsString(validateOAuthTokenRequest));
        String responseText = getHttpConnectionResponseAsText(conn);
        conn.disconnect();
        return objectMapper.readValue(responseText, ValidateOAuthTokenResponse.class);
    }

}
