package org.gs4tr.tmserver.tool.api.model;

import java.io.Serializable;
import java.util.List;

import org.gs4tr.foundation.locale.Locale;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@NoArgsConstructor
public class TmInfo implements Serializable
{
    private static final long serialVersionUID = -3405106939779590659L;

    private String tmName;
    private String tmShortCode;
    private String groupName;
    private String groupShortCode;
    private Long tuCount;
    private Long privateTuCount;
    private Long publicTuCount;
    private Long lastModificationDate;
    private List<String> genericUsers;
    private List<String> connectionStrings;
    private String sourceLanguage;
    private String targetLanguage;
    private String organization;
    private boolean enabled;
    private String updateCode;

    public String getTmName()
    {
        return tmName;
    }

    public void setTmName(String tmName)
    {
        this.tmName = tmName;
    }

    public String getTmShortCode()
    {
        return tmShortCode;
    }

    public void setTmShortCode(String tmShortCode)
    {
        this.tmShortCode = tmShortCode;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public Long getTuCount()
    {
        return tuCount;
    }

    public void setTuCount(Long tuCount)
    {
        this.tuCount = tuCount;
    }

    public Long getPrivateTuCount()
    {
        return privateTuCount;
    }

    public void setPrivateTuCount(Long privateTuCount)
    {
        this.privateTuCount = privateTuCount;
    }

    public Long getPublicTuCount()
    {
        return publicTuCount;
    }

    public void setPublicTuCount(Long publicTuCount)
    {
        this.publicTuCount = publicTuCount;
    }

    public Long getLastModificationDate()
    {
        return lastModificationDate;
    }

    public void setLastModificationDate(Long lastModificationDate)
    {
        this.lastModificationDate = lastModificationDate;
    }

    public List<String> getGenericUsers()
    {
        return genericUsers;
    }

    public void setGenericUsers(List<String> genericUsers)
    {
        this.genericUsers = genericUsers;
    }

    public List<String> getConnectionStrings()
    {
        return connectionStrings;
    }

    public void setConnectionStrings(List<String> connectionStrings)
    {
        this.connectionStrings = connectionStrings;
    }

    public String getSourceLanguage()
    {
        return sourceLanguage;
    }

    public String getSourceLocaleDisplayName()
    {
        Locale srcLocale = Locale.makeLocale(this.sourceLanguage);
        return srcLocale.getDisplayName();
    }

    public void setSourceLanguage(String sourceLanguage)
    {
        this.sourceLanguage = sourceLanguage;
    }

    public String getTargetLanguage()
    {
        return targetLanguage;
    }

    public String getTargetLocaleDisplayName()
    {
        Locale tgtLocale = Locale.makeLocale(this.targetLanguage);
        return tgtLocale.getDisplayName();
    }

    public void setTargetLanguage(String targetLanguage)
    {
        this.targetLanguage = targetLanguage;
    }

    public String getOrganization()
    {
        return organization;
    }

    public void setOrganization(String organization)
    {
        this.organization = organization;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public String getUpdateCode()
    {
        return updateCode;
    }

    public void setUpdateCode(String updateCode)
    {
        this.updateCode = updateCode;
    }

    public String getGroupShortCode()
    {
        return groupShortCode;
    }

    public void setGroupShortCode(String groupShortCode)
    {
        this.groupShortCode = groupShortCode;
    }

    public String getTmPath()
    {
        StringBuffer sb = new StringBuffer(this.groupShortCode);
        sb.append("/");
        sb.append(this.tmShortCode);
        return sb.toString();
    }

    public void setTuInfo(TmTuInfoResponse response)
    {
        this.tuCount = response.getTuInfo().getTotalNumberOfTus();
        this.publicTuCount = response.getTuInfo().getNumberOfPublicTus();
        this.privateTuCount = response.getTuInfo().getNumberOfPrivateTus();
    }
}
