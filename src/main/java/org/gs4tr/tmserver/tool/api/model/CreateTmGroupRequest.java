package org.gs4tr.tmserver.tool.api.model;

import org.gs4tr.tmserver.tool.controller.model.TmGroupOption;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@NoArgsConstructor
public class CreateTmGroupRequest extends TmServerRequest
{
    private String organizationId;
    private String projectName;
    private String shortcode;

    public void initCreateTmGroupRequest(TmGroupOption tmGroupOption)
    {
        this.organizationId = tmGroupOption.getSelectedOrganization().getValue();
        this.projectName = tmGroupOption.getTmGroupName();
        this.shortcode = tmGroupOption.getTmGroupShortcode();
    }

    public String getOrganizationId()
    {
        return organizationId;
    }

    public void setOrganizationId(String organizationId)
    {
        this.organizationId = organizationId;
    }

    public String getProjectName()
    {
        return projectName;
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public String getShortcode()
    {
        return shortcode;
    }

    public void setShortcode(String shortcode)
    {
        this.shortcode = shortcode;
    }
}
