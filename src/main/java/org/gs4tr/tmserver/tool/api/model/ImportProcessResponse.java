package org.gs4tr.tmserver.tool.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class ImportProcessResponse extends TMServerResponse
{
    private String phase;
    private int percent;
    private long total;
    private long imported;
    private long skipped;
    private List<String> errors;
    private String errorMessage;
    private boolean hasInvalidSegments;

    public String getPhase()
    {
        return phase;
    }

    public void setPhase(String phase)
    {
        this.phase = phase;
    }

    public int getPercent()
    {
        return percent;
    }

    public void setPercent(int percent)
    {
        this.percent = percent;
    }

    public long getTotal()
    {
        return total;
    }

    public void setTotal(long total)
    {
        this.total = total;
    }

    public long getImported()
    {
        return imported;
    }

    public void setImported(long imported)
    {
        this.imported = imported;
    }

    public long getSkipped()
    {
        return skipped;
    }

    public void setSkipped(long skipped)
    {
        this.skipped = skipped;
    }

    public List<String> getErrors()
    {
        return errors;
    }

    public void setErrors(List<String> errors)
    {
        this.errors = errors;
    }

    @Override
    public String getErrorMessage()
    {
        return errorMessage;
    }

    @Override
    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public boolean isHasInvalidSegments()
    {
        return hasInvalidSegments;
    }

    public void setHasInvalidSegments(boolean hasInvalidSegments)
    {
        this.hasInvalidSegments = hasInvalidSegments;
    }
}
