package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class ValidateOAuthTokenResponse extends TMServerResponse
{
    private String version;

    public String getVersion()
    {
        return version;
    }

}
