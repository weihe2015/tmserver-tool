package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class TmTuInfo
{
    private Long totalNumberOfTus;
    private Long numberOfPrivateTus;
    private Long numberOfPublicTus;
    private Long lastTuModificationTimestamp;

    public Long getTotalNumberOfTus()
    {
        return totalNumberOfTus;
    }

    public void setTotalNumberOfTus(Long totalNumberOfTus)
    {
        this.totalNumberOfTus = totalNumberOfTus;
    }

    public Long getNumberOfPrivateTus()
    {
        return numberOfPrivateTus;
    }

    public void setNumberOfPrivateTus(Long numberOfPrivateTus)
    {
        this.numberOfPrivateTus = numberOfPrivateTus;
    }

    public Long getNumberOfPublicTus()
    {
        return numberOfPublicTus;
    }

    public void setNumberOfPublicTus(Long numberOfPublicTus)
    {
        this.numberOfPublicTus = numberOfPublicTus;
    }

    public Long getLastTuModificationTimestamp()
    {
        return lastTuModificationTimestamp;
    }

    public void setLastTuModificationTimestamp(Long lastTuModificationTimestamp)
    {
        this.lastTuModificationTimestamp = lastTuModificationTimestamp;
    }

}
