package org.gs4tr.tmserver.tool.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@NoArgsConstructor
public class GenerateUsernameResponse extends TMServerResponse
{
    private String username;

    public String getUsername()
    {
        return username;
    }

}
