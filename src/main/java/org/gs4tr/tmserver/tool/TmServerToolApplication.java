package org.gs4tr.tmserver.tool;

import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@SpringBootApplication
@ComponentScan(basePackages = { "org.gs4tr.tmserver.tool" })
@EnableAutoConfiguration(exclude =
{ ErrorMvcAutoConfiguration.class })
public class TmServerToolApplication extends SpringBootServletInitializer
{
    /**
     * A component that handles error page direction to 404 page.
     */
    @Component
    public class CustomizationBean implements EmbeddedServletContainerCustomizer
    {
        @Override
        public void customize(ConfigurableEmbeddedServletContainer container)
        {
            container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/404"));
            container.addErrorPages(new ErrorPage(HttpStatus.UNAUTHORIZED, "/401"));
        }
    }

    /**
     * Main Spring Boot Class
     */
    public static void main(String[] args)
    {
        SpringApplication.run(TmServerToolApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        return application.sources(TmServerToolApplication.class);
    }

    /**
     * A Bean that helps to set upload file size to unlimited
     */
    @Bean
    public TomcatEmbeddedServletContainerFactory tomcatEmbedded()
    {
        TomcatEmbeddedServletContainerFactory tomcatFactory = new TomcatEmbeddedServletContainerFactory();
        tomcatFactory.addConnectorCustomizers((TomcatConnectorCustomizer) connector -> {
            if ((connector.getProtocolHandler() instanceof AbstractHttp11Protocol<?>))
            {
                // -1 means unlimited
                ((AbstractHttp11Protocol<?>) connector.getProtocolHandler()).setMaxSwallowSize(-1);
            }
        });
        return tomcatFactory;
    }

}
