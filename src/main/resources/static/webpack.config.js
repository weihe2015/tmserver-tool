const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const Visualizer = require('webpack-visualizer-plugin');
const WatchTimePlugin = require('webpack-watch-time-plugin');
const merge = require("webpack-merge");
require("@babel/polyfill");

module.exports = env => {
  const { PLATFORM, TOOLURL } = env;
  let config = merge([{
    context: __dirname,
    devtool: false, // change to 'source-map' for yarn analyze
    entry: ["@babel/polyfill", "./js/react/index.js"],
    node: {
      fs: 'empty',
      net: 'empty'
    },
    optimization: {
      minimizer: [
        new UglifyJsPlugin()
      ]
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel-loader',
          query: {
            presets: [
              "@babel/preset-env",
              "@babel/preset-react",
            ],
            plugins: [
              "@babel/plugin-syntax-dynamic-import",
              "@babel/plugin-proposal-class-properties",
              "@babel/plugin-proposal-export-namespace-from",
              "@babel/plugin-proposal-throw-expressions",
              "@babel/plugin-transform-regenerator",
              "transform-semantic-ui-react-imports"
            ]
          }
        },
        {
          test: /(\.css$)/,
          loader: ['style-loader', 'css-loader', 'sass-loader']
        },
        {
          test: /\.(png|woff|woff2|eot|ttf|svg)$/,
          loader: 'url-loader?limit=100000'
        }
      ]
    },
    output: {
      path: __dirname + "/js/build",
      // service worker cannot use regex, so pending on: '[chunkhash].bundle.js', or use [name].bundle.js to have main/0,1,2 on filename
      filename: PLATFORM === 'developing' ? 'bundle.js' : '[name].[contenthash:8].js',
      publicPath: `/${TOOLURL}/js/build/`,
    },
    plugins: [
      new MiniCssExtractPlugin(),
      new OptimizeCssAssetsPlugin(),
      new webpack.DefinePlugin({
        'process.env.PLATFORM': JSON.stringify(env.PLATFORM),
        'process.env.TOOLURL': JSON.stringify(env.TOOLURL),
        'process.env.INSTANCE': JSON.stringify(env.INSTANCE)
      }),
      new HtmlWebpackPlugin({
        template: './index.html',
        filename: '../../../templates/index.html'
      })
    ]
  }]);
  if (PLATFORM === 'staging' || PLATFORM === 'developing') {
    config.plugins.push(new WatchTimePlugin());
    config.plugins.push(new Visualizer({ filename: './statistics.html' }));
  }
  return config;
};
