const cacheName = 'v1';
self.addEventListener('install', e => {
  console.log("[ServiceWorker] Installed");
  let cacheFiles = [
    '../images/favicon.ico',
    'https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin',
    'https://fonts.googleapis.com/css?family=Open+Sans',
    'https://cdnjs.cloudflare.com/ajax/libs/react-table/6.8.6/react-table.css',
    'https://cdn.jsdelivr.net/npm/semantic-ui@2.4.0/dist/semantic.min.css'
  ]
  // for (let i = 0; i <= 26; i++) {
  //   if (i !== 11) {
  //     cacheFiles.push(`./build/bundle-${i}.js`);
  //   }
  // }
  e.waitUntil(
    caches.open(cacheName).then(function (cache) {
      console.log("[ServiceWorker] Caching cacheFiles");
      return cache.addAll(cacheFiles);
    })
  );
})

self.addEventListener('activate', e => {
  console.log("[ServiceWorker] Activated");
  e.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(cacheNames.map(function (thisCacheName) {
        if (thisCacheName !== cacheName) {
          console.log("[ServiceWorker] Remove Cached cacheFiles", thisCacheName);
          return caches.delete(thisCacheName);
        }
      }))
    })
  );
})

self.addEventListener('fetch', e => {
  console.log("[ServiceWorker] Fetching", e.request.url);

  e.responseWith(
    caches.match(e.request).then(response => {
      if (response) {
        console.log("[ServiceWorker] Found in cache", e.request.url);
        return response;
      }
      else {
        const requestClone = e.request.clone();
        fetch(requestClone).then(response => {
          if (!response) {
            console.log("[ServiceWorker] No response from fetch");
            return response;
          }
          else {
            const responseClone = response.clone();
            caches.open(cacheName).then(cache => {
              cache.put(e.request, responseClone);
              return response;
            })
          }
        }).catch(err => {
          console.log("[ServiceWorker] Error Fetching & catching data", err.response);
        });
      }
    })
  );

})