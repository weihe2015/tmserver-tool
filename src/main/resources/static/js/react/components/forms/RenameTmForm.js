import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Checkbox, Dimmer, Form, Grid,
  Loader, Message
} from '../../utils/reactSemanticUI';
import CryptoJS from "crypto-js";
import ReactTable from "react-table";

import { fetchRenameTmList, renameTm } from '../../actions/tm';
import InlineError from '../messages/InlineError';
import {
  sendBrowserNotification,
  includeElement, removeElement
} from '../../utils/main';

class RenameTmForm extends Component {
  state = {
    tmList: [],
    selectedRows: [],
    currentPageRows: [],
    isSelectAllRowsInCurrentPage: false,
    isSelectAllRowsOptionVisible: false,
    options: {
      selectedTmList: [],
      newTmName: ''
    },
    initLoading: false,
    loading: false,
    success: false,
    unauthorized: false,
    errors: {}
  }

  componentDidMount = async () => {
    this.setState({ initLoading: true });

    const { renameTmListData } = this.props;
    const currentDate = new Date().getTime();
    let tmList = [];
    if (!renameTmListData || renameTmListData.exp <= currentDate) {
      try {
        const promise = await this.props.fetchRenameTmList();
        tmList = promise.data;
      }
      catch (err) {
        const { response } = err;
        // request is unauthorized: most likely the token is expired, direct user to login page:
        if (!!response && response.hasOwnProperty("status") && response.status === 401) {
          localStorage.clear();
          this.setState({
            ...this.state,
            unauthorized: true,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : null
            }
          });
        } else {
          this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : null
            }
          });
        }
      }
    }
    else {
      tmList = renameTmListData.renameTmList;
    }
    this.setState({
      ...this.state,
      tmList,
      success: false,
      initLoading: false
    });
  }

  onChange = (event) => {
    let { errors } = this.state;
    delete errors['newTmName'];
    this.setState({
      ...this.state,
      success: false,
      errors,
      options: { ...this.state.options, [event.target.name]: event.target.value }
    })
  }

  checkTmName = (name) => {
    const re = /^[\(\)0-9a-zA-Z\-_ ]+$/im;
    return re.test(name);
  }

  onTableViewChange = () => {
    const table = this.reactTable;
    if (table) {
      const page = table.state.page;
      const pageSize = table.state.pageSize;
      const allData = table.getResolvedState().sortedData;
      const startIdx = page * pageSize;
      const currentPageRows = allData.slice(startIdx, startIdx + pageSize).map((item) => {
        let obj = { index: item._index, info: item._original };
        return obj;
      });

      this.setState({
        ...this.state,
        isSelectAllRowsOptionVisible: true,
        currentPageRows
      });
    }
  }

  onSelectAllRowsInCurrentPageChange = () => {
    const { isSelectAllRowsInCurrentPage, currentPageRows } = this.state;
    let { selectedRows } = this.state;
    // If select all rows in current page option turns from false to true
    // Add all currentPageRows into selectedRows
    if (!isSelectAllRowsInCurrentPage) {
      currentPageRows.forEach(currentPageRow => {
        if (!includeElement(selectedRows, currentPageRow.index)) {
          selectedRows.push(currentPageRow);
        }
      });
      this.setState({
        ...this.state, selectedRows, success: false,
        isSelectAllRowsInCurrentPage: true
      });
    }
    // If select all rows in current page option turns from true to false,
    // we need to remove current page Rows from selectedRows:
    else {
      currentPageRows.forEach(currentPageRow => {
        if (includeElement(selectedRows, currentPageRow.index)) {
          selectedRows = removeElement(selectedRows, currentPageRow.index);
        }
      });
      this.setState({
        ...this.state,
        selectedRows,
        success: false,
        isSelectAllRowsInCurrentPage: false
      });
    }
  }

  validate = (options) => {
    const errors = {};
    if (options.selectedTmList.length === 0) {
      errors.selectedTmList = "Please select one TM";
    }
    if (options.newTmName === '') {
      errors.newTmName = "New Tm name cannot be empty";
    }
    else if (!this.checkTmName(options.newTmName)) {
      errors.newTmName = `The new TM name is not valid.
        Only 0-9, A-Z, a-z, -, _, (, ) is allowed.`;
    }
    return errors;
  }

  onSubmit = async (event) => {
    event.preventDefault();
    const { selectedRows } = this.state;
    let { options } = this.state;
    // move selectedRows to options.selectedTmList
    selectedRows.forEach((selectedRow) => {
      options.selectedTmList.push(selectedRow.info);
    });

    const errors = this.validate(options);
    this.setState({ errors });
    if (Object.keys(errors).length !== 0) {
      return;
    }
    const renameTmError = `Failed to rename TM, please check the log...`;
    this.setState({ ...this.state, loading: true });
    await this.props.renameTm(options)
      .then(async (_) => {
        await this.setState({
          ...this.state,
          loading: false,
          success: true,
          errors: {},
          selectedRows: [],
          currentPageRows: [],
          isSelectAllRowsInCurrentPage: false,
          isSelectAllRowsOptionVisible: false,
          options: {
            selectedTmList: [],
            newTmName: ''
          }
        })
        await this.props.fetchRenameTmList().then(async (res) => {
          const notificationType = "Tm Rename Update";
          const notificationContent = "Tm Rename is successful! Please check in Tm Server.";
          sendBrowserNotification(notificationType, notificationContent);
          await this.setState({
            ...this.state,
            tmList: res.data,
          });
        }).catch(err => {
          this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : null
            }
          });
        });
      })
      .catch(err => {
        const { response } = err;
        if (!!response && response.hasOwnProperty("status") && (response.status === 400 || response.status === 500)) {
          this.setState({
            ...this.state,
            loading: false,
            errors: {
              ...this.state.errors,
              global: renameTmError
            }
          });
        }
        else {
          this.setState({
            ...this.state,
            loading: false,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : renameTmError
            }
          });
        }
      });
  }

  render() {
    let { selectedRows, errors } = this.state;
    const { tmList, initLoading,
      loading, options, success } = this.state;

    if (Object.keys(errors).length > 0) {
      window.scrollTo(0, 0);
    }

    return (
      <div style={{ "marginLeft": "50px", "marginRight": "50px" }} >
        <h4>Rename TM</h4>
        <Form onSubmit={this.onSubmit}>
          <Dimmer inverted active={initLoading}>
            <Loader indeterminate>Initial loading...</Loader>
          </Dimmer>
          <Dimmer inverted active={loading}>
            <Loader indeterminate>
              Renaming Tm, please wait...
            </Loader>
          </Dimmer>
          <Grid columns={1} celled>
            <Grid.Row>
              <Grid.Column>
                {(!success && errors && errors.global) &&
                  <Message negative>
                    <Message.Header>Something went wrong</Message.Header>
                    <p>{errors.global}</p>
                  </Message>}
                {(!success && errors && errors.selectedTmList) &&
                  <Message negative>
                    <Message.Header>Something went wrong</Message.Header>
                    <p>{errors.selectedTmList}</p>
                  </Message>}
                {success && (
                  <Message positive>
                    <Message.Header>Tm Rename Update</Message.Header>
                    <p>Tm Rename is successful! Please check in Tm Server.</p>
                  </Message>
                )}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Tm Name to rename to:</label>
                </Form.Field>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                <Form.Field>
                  <Form.Input
                    error={!!errors && !!errors.newTmName}
                    type="text"
                    id="newTmName"
                    name="newTmName"
                    placeholder="Enter Translation Memory Name to rename"
                    value={options.newTmName}
                    onChange={this.onChange.bind(this)}
                  />
                  {(errors && errors.newTmName) &&
                    <InlineError text={errors.newTmName} />}
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                {!!tmList && tmList.length > 0 && (
                  <div>
                    <div>
                      <p>Total {selectedRows.length} of row{selectedRows.length <= 1 ?
                        ' is' : 's are'} selected.&nbsp;&nbsp;</p>
                      {this.state.isSelectAllRowsOptionVisible && (
                        <div>
                          <Checkbox onChange={this.onSelectAllRowsInCurrentPageChange}
                            checked={this.state.isSelectAllRowsInCurrentPage}
                            label={this.state.isSelectAllRowsInCurrentPage ?
                              "Deselect all rows in current page" :
                              "Select all rows in current page"} />
                          <br />
                        </div>
                      )}
                      <br />
                    </div>
                    <ReactTable
                      ref={(r) => {
                        this.reactTable = r;
                      }}
                      onPageChange={this.onTableViewChange}
                      onPageSizeChange={this.onTableViewChange}
                      onSortedChange={this.onTableViewChange}
                      onExpandedChange={this.onTableViewChange}
                      onFilteredChange={this.onTableViewChange}
                      onResizedChange={this.onTableViewChange}
                      data={tmList}
                      noDataText="No Tm is selected"
                      columns={[
                        {
                          Header: "Tm Name",
                          accessor: "tmName",
                          sortable: false,
                          filterable: true,
                          Cell: ({ value }) => (
                            <div style={{
                              "textAlign": "left",
                              "marginLeft": "15px"
                            }}>
                              {value}
                            </div>
                          )
                        },
                        {
                          Header: "Tm Group Name",
                          accessor: "tmGroupName",
                          sortable: false,
                          filterable: true
                        },
                        {
                          Header: "Tm Short Code",
                          accessor: "tmPath",
                          sortable: false,
                          filterable: false
                        },
                        {
                          Header: "Source Language",
                          accessor: "sourceLocale",
                          sortable: false,
                          filterable: false
                        },
                        {
                          Header: "Target Language",
                          accessor: "targetLocale",
                          sortable: false,
                          filterable: false
                        }
                      ]}
                      getTrProps={(state, rowInfo) => {
                        return {
                          onClick: (event) => {
                            if (includeElement(selectedRows, rowInfo.index)) {
                              const newSelectedRows =
                                removeElement(selectedRows, rowInfo.index);
                              delete errors['selectedTmList'];
                              this.setState({
                                ...this.state,
                                success: false,
                                errors,
                                selectedRows: newSelectedRows
                              })
                            }
                            else {
                              const obj = { index: rowInfo.index, info: rowInfo.original };
                              selectedRows.push(obj);
                              delete errors['selectedTmList'];
                              this.setState({
                                ...this.state,
                                success: false,
                                errors,
                                selectedRows
                              });
                            }
                          },
                          style: {
                            background: (rowInfo &&
                              includeElement(selectedRows, rowInfo.index)) ?
                              '#00afec' : 'white',
                            color: (rowInfo &&
                              includeElement(selectedRows, rowInfo.index)) ?
                              'white' : 'black'
                          }
                        }
                      }}
                      defaultPageSize={10}
                      className="-striped -highlight"
                      style={{ "textAlign": "center" }}
                    />
                  </div>)}
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <br /><br />
          <Button floated="right" primary size="large"
            disabled={loading || initLoading}>
            Rename
          </Button>
          <br /><br />
        </Form>
      </div>
    )
  }
}

RenameTmForm.propTypes = {
  fetchRenameTmList: PropTypes.func.isRequired,
  renameTm: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  renameTmListData: state.tm.renameTmListData
})

export default connect(mapStateToProps,
  { fetchRenameTmList, renameTm })(RenameTmForm)
