import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import {
  Button,
  Checkbox,
  Dimmer,
  Dropdown,
  Form,
  Grid,
  Icon,
  Loader,
  Message,
  Segment
} from "../../utils/reactSemanticUI";
import crypto from "crypto";
import ReactTable from "react-table";
import Dropzone from "react-dropzone";
import map from "lodash/map";
import Cookies from "universal-cookie";
import {
  fetchImportTmList,
  uploadImportFile,
  validateRandomToken,
  importTm,
  fetchImportProgress,
  fetchExportTmList
} from "../../actions/tm";
import TmImportStatus from "../messages/TmImportStatus";
import { importTmOptions, importTmAttributesOptions } from "../common";
import {
  includeElement,
  removeElement,
  filterMethod,
  sendBrowserNotification,
  sortLocales
} from "../../utils/main";
import userManager from "../../utils/userManager";
import { oauthLogin } from '../../actions/auth';
import decode from 'jwt-decode';

const cookies = new Cookies();
// cookies expires in one year
const cookieExpiredTime = new Date(
  new Date().getTime() + 60 * 60 * 1000 * 24 * 365
);
class ImportTmForm extends Component {
  state = {
    tmList: [], // tm List which is used to construct React Table.
    sourceLocales: [], // sorted source locales list
    targetLocales: [], // sorted target locales list
    files: [], // selected file to upload.
    unzipFiles: {}, // used to display tmx files unzip from zip file
    selectedRows: [],
    currentPageRows: [],
    isSelectAllRowsInCurrentPage: false,
    isSelectAllRowsOptionVisible: false,
    options: {
      tmImportOption: "APPEND",
      tmAttributeImportOption: "IGNORE",
      ignoreInvalidTus: true,
      keepNewerTu: true
    },
    importingList: {
      uncompleted: [],
      completed: []
    },
    randomToken: "",
    dropzoneActive: false,
    initLoading: false,
    loading: false,
    importing: false,
    refetchImportTmListLoading: false,
    refetchExportTmListLoading: false,
    removedUploadedFileLoading: false,
    getImportProgressLoading: false,
    tokenExpiredLoading: false,
    uploadLoading: false,
    uploadSuccess: false,
    success: false,
    unauthorized: false,
    errors: {}
  };

  onDragEnter = () => {
    this.setState({ ...this.state, dropzoneActive: true });
  };

  onDragLeave = () => {
    this.setState({ ...this.state, dropzoneActive: false });
  };

  onDrop = (acceptedFiles, rejectedFiles, e) => {
    const { selectedRows } = this.state;
    if (selectedRows.length === 0) {
      this.setState({
        ...this.state,
        success: false,
        uploadSuccess: false,
        dropzoneActive: false,
        unzipFiles: {},
        errors: {
          uploadFile: `No Tm is selected for uploaded files.
          Please select the Tm for import before dropping the tmx/Wf txt files.`
        }
      });
      return;
    } else {
      let { files, errors } = this.state;
      let previousUploadedFilenameList = [];
      // concat previous uploaded files.
      files.forEach(file => {
        previousUploadedFilenameList.push(file.name);
      });
      acceptedFiles = acceptedFiles.map(acceptedFile => {
        const fileName = acceptedFile.name;
        if (
          fileName.endsWith(".tmx") ||
          fileName.endsWith(".zip") ||
          fileName.endsWith(".txt")
        ) {
          if (previousUploadedFilenameList.includes(fileName)) {
            errors.uploadError += `<br/>There is already have a file named: ${fileName},
              please rename and upload it again.`;
          } else {
            acceptedFile.isUploaded = false;
            files.push(acceptedFile);
          }
        } else {
          errors.uploadError += `<br/>Skipping file ${fileName}.
            We only accept txt, tmx and zip file for uploading.`;
        }
        return acceptedFile;
      });
      this.setState({
        ...this.state,
        files,
        errors,
        unzipFiles: {},
        success: false,
        uploadSuccess: false,
        dropzoneActive: false
      });
    }
  };

  handleUploadClick = async (event) => {
    event.preventDefault();
    event.stopPropagation();
    let { files, tmList, errors, selectedRows, unzipFiles } = this.state;
    // error checking:
    if (files.length === 0) {
      errors.uploadError = "No file is selected.";
      this.setState({
        ...this.state,
        errors,
        files,
        uploadLoading: false,
        uploadSuccess: false
      });
      return;
    }
    if (selectedRows.length === 0) {
      errors.uploadError = "No Tm Row is selected.";
      this.setState({
        ...this.state,
        errors,
        files,
        uploadLoading: false,
        uploadSuccess: false
      });
      return;
    }

    // remove previous n.
    delete errors.uploadFile;
    this.setState({ ...this.state, uploadLoading: true });
    let selectedTmRows = [];
    selectedRows.forEach(selectedRow => {
      // remove previous import status when user import in second attempt
      tmList[selectedRow.index].status = "Pending";
      // TODO: maybe add index to update row status info:
      // selectedRow.info.index = selectedRows[i].index;
      selectedTmRows.push(selectedRow.info);
    });
    // generate a random token to resolve concurrency issue
    let { randomToken } = this.state;
    if (randomToken.length === 0) {
      randomToken = crypto
        .randomBytes(Math.ceil(5 / 2))
        .toString("hex")
        .slice(0, 5);
    }

    // check if randomToken is existed in the server
    let randomTokenValidateObject = {};
    randomTokenValidateObject.token = randomToken;
    await this.props.validateRandomToken(randomTokenValidateObject).then(res => {
      if (!res.data.status) {
        randomToken = crypto
          .randomBytes(Math.ceil(5 / 2))
          .toString("hex")
          .slice(0, 5);
      }
    }).catch(err => {
      randomToken = crypto.randomBytes(Math.ceil(5 / 2))
        .toString("hex")
        .slice(0, 5);
    });

    let promises = [];
    files.forEach(file => {
      if (!file.isUploaded || !file.uploadSuccess) {
        const promise = this.props.uploadImportFile(
          selectedTmRows,
          randomToken,
          file
        );
        promises.push(promise);
      }
    });

    Promise.all(promises).then(results => {
      results.forEach(res => {
        const notificationType = "Tm Import, Upload File Status:";
        const notificationContent = "Import File is successful!";
        sendBrowserNotification(notificationType, notificationContent);

        const responseJSON = JSON.parse(res.data.text);
        // If source file is zip file, it will return a JSONArray
        if (Array.isArray(responseJSON)) {
          // ZipFilename is the same for all responseJSON[j] subFile.
          let zipFilename = "";
          let subList = [];
          responseJSON.forEach(responseSubJSON => {
            zipFilename = responseSubJSON.originalZipFileName;
            subList.push(responseSubJSON);
          });
          unzipFiles[zipFilename] = subList;
          // mark zip file in files as zipFile and marked it as upload success.
          files = files.map(file => {
            if (zipFilename === file.name) {
              file.uploadSuccess = true;
              file.isUploaded = true;
              file.isZipFile = true;
            }
            return file;
          });
        }
        // Handle separated files.
        else {
          const { filename, status } = responseJSON;
          files = files.map(file => {
            if (filename === file.name) {
              file.uploadSuccess = status;
              file.isUploaded = true;
              // upload failed:
              if (!status) {
                file.errorMessage = responseJSON.errorMessage;
              }
            }
            return file;
          });
        }
      });
      this.setState({
        ...this.state,
        errors,
        files,
        randomToken,
        tmList,
        success: false,
        uploadLoading: false,
        uploadSuccess: true
      });
    }).catch(err => {
      const errMessage = JSON.parse(err.res.text);
      errors.uploadFile = `Filename: ${errMessage.filename} Error: ${
        errMessage.global
        }`;
      this.setState({
        ...this.state,
        errors,
        randomToken,
        tmList,
        success: false,
        files: [],
        uploadLoading: false,
        uploadSuccess: false
      });
    });
  };

  handleSelectionOptionChange = (_, { name, value }) => {
    this.setState({
      ...this.state,
      uploadSuccess: false,
      success: false,
      options: {
        ...this.state.options,
        [name]: value
      }
    });
    if (name === "tmImportOption") {
      cookies.set("_tmImportOption", value, {
        path: `/${process.env.TOOLURL}`,
        expires: cookieExpiredTime
      });
    } else if (name === "tmAttributeImportOption") {
      cookies.set("_tmAttributeImportOption", value, {
        path: `/${process.env.TOOLURL}`,
        expires: cookieExpiredTime,
        samesite: "strict"
      });
    }
  };

  onIgnoreInvalidTusChange = event => {
    event.preventDefault();
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        ignoreInvalidTus: !this.state.options.ignoreInvalidTus
      }
    });
  };

  onKeepNewerTuChange = event => {
    event.preventDefault();
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        keepNewerTu: !this.state.options.keepNewerTu
      }
    });
  };

  componentDidMount = async () => {
    const { importTmListData, oauthUser } = this.props;
    let options = this.loadImportTmOptionsFromCookies(this.state.options);

    this.oauthInterval = setInterval(this.oauthSilentRefresh, 35 * 1000);
    this.refreshTmImportStatusInterval = setInterval(this.getTmImportProgress, 30 * 1000);
    this.refreshTmImportListInterval = setInterval(this.tmImportListPeriodRefresh, 3 * 56 * 1000);

    let tmList = [];
    const currentDate = new Date().getTime();
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      await this.oauthSilentRefresh();
    }

    this.setState({ initLoading: true });
    const info = {
      type: "import",
      refresh: false
    };
    if (!importTmListData || importTmListData.exp <= currentDate) {
      try {
        const promise = await this.props.fetchImportTmList(info);
        tmList = promise.data;
      }
      catch (err) {
        const { response } = err;
        // request is unauthorized: most likely the token is expired, direct user to login page:
        if (!!response && response.hasOwnProperty("status") && response.status === 401) {
          localStorage.clear();
          this.setState({
            ...this.state,
            unauthorized: true,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : null
            }
          });
        } else {
          this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : null
            }
          });
        }
      }
      finally {
        this.setState({
          ...this.state,
          initLoading: false
        });
      }
    }
    else {
      tmList = importTmListData.importTmList;
    }
    let sourceLocales = [];
    let targetLocales = [];

    if (tmList !== undefined && tmList.length > 0) {
      const sortedLocales = sortLocales(tmList);
      sourceLocales = sortedLocales.sourceLocales;
      targetLocales = sortedLocales.targetLocales;
    }
    this.setState({
      uploadSuccess: false,
      success: false,
      tmList,
      sourceLocales,
      targetLocales,
      options,
      initLoading: false,
    });
  };

  loadImportTmOptionsFromCookies = (options) => {
    // Preload cookies values:
    if (!cookies.get("_tmImportOption")) {
      options.tmImportOption = "APPEND";
    }
    else {
      options.tmImportOption = cookies.get("_tmImportOption");
    }

    if (!cookies.get("_tmAttributeImportOption")) {
      options.tmAttributeImportOption = "IGNORE";
    }
    else {
      options.tmAttributeImportOption = cookies.get("_tmAttributeImportOption");
    }
    return options;
  }

  componentWillUnmount = () => {
    clearInterval(this.oauthInterval);
    clearInterval(this.refreshTmImportStatusInterval);
    clearInterval(this.refreshTmImportListInterval);
  }

  oauthSilentRefresh = async () => {
    const currentDate = new Date().getTime() / 1000;
    const { oauthUser } = this.props;
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      this.setState({ ...this.state, tokenExpiredLoading: true });
      try {
        await userManager.signinSilent();
        const { access_token } = oauthUser;
        const payload = decode(access_token);
        let credentials = {};
        credentials.email = payload.sub;
        credentials.creation = payload.nbf;
        credentials.expiration = payload.exp;
        credentials.accessToken = access_token;
        credentials.idToken = oauthUser.id_token;
        await this.props.oauthLogin(credentials).catch(err => {
          console.log(err);
        })
      }
      catch (err) {
        localStorage.oauthLastLoc = window.location.pathname;
        await userManager.signinRedirect();
        console.log(err);
      }
      finally {
        this.setState({ ...this.state, tokenExpiredLoading: false });
      }
    }
  }

  tmImportListPeriodRefresh = () => {
    const { importTmListData } = this.props;
    const currentDate = new Date().getTime() / 1000;
    if (!!importTmListData && importTmListData.exp > currentDate) {
      return;
    }
    this.refetchImportTmList();
  }

  refetchImportTmList = async () => {
    // event.preventDefault();
    // event.stopPropagation();

    this.setState({
      ...this.state,
      uploadSuccess: false,
      success: false,
      refetchImportTmListLoading: true
    });
    const info = {
      type: "import",
      refresh: true
    };
    let tmList = [];
    try {
      const promise = await this.props.fetchImportTmList(info);
      tmList = promise.data;
    }
    catch (err) {
      const { response } = err;
      // request is unauthorized: most likely the token is expired, direct user to login page:
      if (!!response && response.hasOwnProperty("status") && response.status === 401) {
        localStorage.clear();
        await this.setState({
          ...this.state,
          unauthorized: true,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : null
          }
        });
      } else {
        await this.setState({
          ...this.state,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : null
          }
        });
      }
    }
    finally {
      let sourceLocales = [];
      let targetLocales = [];

      if (tmList !== undefined && tmList.length > 0) {
        const sortedLocales = sortLocales(tmList);
        sourceLocales = sortedLocales.sourceLocales;
        targetLocales = sortedLocales.targetLocales;
      }
      this.setState({
        ...this.state,
        tmList,
        sourceLocales,
        targetLocales,
        refetchImportTmListLoading: false
      });
    }
  };

  getTmImportProgress = async () => {
    let { importingList, tmList } = this.state;
    const { oauthUser } = this.props;
    if (importingList.uncompleted.length === 0) {
      return;
    }

    let uncompletedImportList = [];
    let completedImportList = [];
    let promises = [];
    importingList.uncompleted.forEach(subList => {
      const { phase } = subList;
      if (phase.length > 0 && phase !== "FINISHED" && phase !== "FAILED") {
        if (oauthUser && !oauthUser.expired) {
          subList.access_token = oauthUser.access_token;
        }
        const promise = this.props.fetchImportProgress(subList);
        promises.push(promise);
        uncompletedImportList.push(subList);
      } else {
        completedImportList.push(subList);
      }
    });

    if (uncompletedImportList.length === 0) {
      await this.setState({
        ...this.state,
        importing: false,
        success: true,
        importingList: {
          uncompleted: [],
          completed: []
        }
      });
      return;
    }

    await this.setState({
      ...this.state,
      uploadSuccess: false,
      success: false,
      getImportProgressLoading: true
    });

    Promise.all(promises).then(async (results) => {
      let newImportingList = [];
      // remove previous status info:
      results.forEach((_, idx) => {
        const rowIdx = uncompletedImportList[idx].rowIdx;
        tmList[rowIdx].status = "";
      });
      completedImportList.forEach((_, idx) => {
        const rowIdx = uncompletedImportList[idx].rowIdx;
        tmList[rowIdx].status = "";
      });

      // Iterate the response and append the status
      results.forEach((res, idx) => {
        const message = res.data;
        let filename = uncompletedImportList[idx].filename;
        const rowIdx = uncompletedImportList[idx].rowIdx;
        let status = "";
        const { phase, success } = message;
        if (success === false && phase === 'FAILED') {
          status += `Import is Failed:&Filename: ${filename.substring(
            filename.indexOf("@") + 1
          )}&`;
          // to avoid errorMessage undefined
          if (message.hasOwnProperty('errorMessage')) {
            status += `Phase Name: ${phase}&`;
            status += `Error: ${message.errorMessage}#`;
          }
          else {
            status += `Phase Name: ${phase}#`;
          }
        }
        else if (phase === "FINISHED") {
          if (message.success) {
            status += `Import Success:&Filename: ${filename.substring(
              filename.indexOf("@") + 1
            )}&`;
            status += `Phase Name: ${phase}&`;
            status += `Total TU: ${message.total}&Imported TU: ${
              message.imported
              }&`;
            status += `Skipped TU: ${message.skipped}#`;
          } else if (!message.success && message.hasOwnProperty("errors")) {
            status += `Import Failure:&Filename: ${filename.substring(
              filename.indexOf("@") + 1
            )}&`;
            status += `Phase Name: ${phase}&`;
            if (message.errors !== undefined && Array.isArray(message.errors)) {
              message.errors.forEach(error => {
                status += `${error}#`;
              });
            } else if (message.errors !== undefined) {
              status += `${message.errors}#`;
            }
          }
          // handle import status other than success or failure
          else {
            status += `Import In Progress:&Filename: ${filename.substring(
              filename.indexOf("@") + 1
            )}&`;
            status += `Phase Name: ${phase}#`;
          }
        }
        else if (phase === 'WAITING') {
          status += `Import In Progress:&Filename: ${filename.substring(
            filename.indexOf("@") + 1
          )}&`;
          status += `Phase Name: ${phase}#`;
        }
        else if (phase === 'FAILED') {
          status += `Import is Failed:&Filename: ${filename.substring(
            filename.indexOf("@") + 1
          )}&`;
          status += `Phase Name: ${phase}&`;
          status += `Error: ${message.errorMessage}#`;
        }
        else if (phase === "IMPORT") {
          status += `Import In Progress:&Filename: ${filename.substring(
            filename.indexOf("@") + 1
          )}&`;
          status += `Phase Name: ${phase}&`;
          status += `Progress: ${message.percent}% #`;
        } else {
          status += `Import In Progress:&Filename: ${filename.substring(
            filename.indexOf("@") + 1
          )}&`;
          if (phase !== undefined) {
            status += `Phase Name: ${phase}#`;
          }
        }
        tmList[rowIdx].status += status;
        if (phase.length > 0 && phase !== "FINISHED" && phase !== "FAILED") {
          newImportingList.push(uncompletedImportList[idx]);
        }
      });

      completedImportList.forEach(subList => {
        let filename = subList.filename;
        const { rowIdx, message } = subList;
        let status = "";

        status += `Import Success:&Filename: ${filename.substring(
          filename.indexOf("@") + 1
        )}&`;
        status += `Phase Name: ${message.phase}&`;
        status += `Total TU: ${message.total}&Imported TU: ${
          message.imported
          }&`;
        status += `Skipped TU: ${message.skipped}#`;
        tmList[rowIdx].status = status;
      });

      await this.setState({
        ...this.state,
        importingList: {
          uncompleted: newImportingList,
          completed: completedImportList
        },
        success: newImportingList.length === 0,
        tmList
      });
    }).finally(() => {
      this.setState({
        ...this.state,
        uploadSuccess: false,
        getImportProgressLoading: false,
      });
    });
  };

  removeUploadedFiles = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const divElement = event.target.querySelectorAll("div")[0];
    const filename = divElement.getAttribute("filename");
    this.setState({
      ...this.state,
      uploadSuccess: false,
      success: false,
      removedUploadedFileLoading: true
    });
    let { files } = this.state;

    // removed file from file List:
    files = files.filter(item => item.name !== filename);
    this.setState({
      ...this.state,
      files,
      uploadSuccess: false,
      success: false,
      removedUploadedFileLoading: false,
      errors: {}
    });
  };

  onTableViewChange = () => {
    const table = this.reactTable;
    if (table) {
      const page = table.state.page;
      const pageSize = table.state.pageSize;
      const allData = table.getResolvedState().sortedData;
      const startIdx = page * pageSize;
      const currentPageRows = allData
        .slice(startIdx, startIdx + pageSize)
        .map(item => {
          let obj = { index: item._index, info: item._original };
          return obj;
        });

      this.setState({
        ...this.state,
        isSelectAllRowsOptionVisible: true,
        currentPageRows
      });
    }
  };

  onSelectAllRowsInCurrentPageChange = () => {
    const { isSelectAllRowsInCurrentPage, currentPageRows } = this.state;
    let { selectedRows } = this.state;
    // If select all rows in current page option turns from false to true
    // Add all currentPageRows into selectedRows
    if (!isSelectAllRowsInCurrentPage) {
      currentPageRows.forEach(currentPageRow => {
        if (!includeElement(selectedRows, currentPageRow.index)) {
          selectedRows.push(currentPageRow);
        }
      });
      this.setState({
        ...this.state,
        selectedRows,
        isSelectAllRowsInCurrentPage: true
      });
    }
    // If select all rows in current page option turns from true to false,
    // we need to remove current page Rows from selectedRows:
    else {
      currentPageRows.forEach(currentPageRow => {
        if (includeElement(selectedRows, currentPageRow.index)) {
          selectedRows = removeElement(selectedRows, currentPageRow.index);
        }
      });
      this.setState({
        ...this.state,
        selectedRows,
        isSelectAllRowsInCurrentPage: false
      });
    }
  };

  handleRemoveAllUploadedFiles = () => {
    this.setState({
      ...this.state,
      files: []
    });
  };

  validate = () => {
    const errors = {};
    const { files, selectedRows } = this.state;
    // No file is selected for upload
    if (Object.keys(files).length === 0) {
      errors.uploadFile = "No file is selected for upload.";
    }
    if (selectedRows.length === 0) {
      errors.selectedRows = "No row is selected for upload.";
    }
    return errors;
  };

  onSubmit = async (event) => {
    event.preventDefault();
    let errors = this.validate();
    this.setState({ ...this.state, errors, uploadSuccess: false });
    const {
      tmList,
      selectedRows,
      options,
      randomToken
    } = this.state;
    let importingList = {
      uncompleted: [],
      completed: []
    };
    if (Object.keys(errors).length !== 0) {
      return;
    }
    this.setState({
      ...this.state,
      loading: true,
      refetchExportTmListLoading: false
    });

    const { oauthUser } = this.props;
    let promises = [];
    let indexes = [];
    selectedRows.forEach(selectedRow => {
      const info = selectedRow.info;
      indexes.push(selectedRow.index);
      // construct the upload folder path:
      const folderLocales = `${info.sourceLocaleCode}_${
        info.targetLocaleCode
        }`;
      const data = {
        randomToken,
        folderLocales,
        tmName: info.tmName,
        tmPath: info.tmPath,
        tmImportOption: options.tmImportOption,
        tmAttributeImportOption: options.tmAttributeImportOption,
        ignoreInvalidTus: options.ignoreInvalidTus,
        keepNewerTu: options.keepNewerTu
      };
      if (oauthUser && !oauthUser.expired) {
        data.accessToken = oauthUser.access_token;
      }
      const promise = this.props.importTm(data);
      promises.push(promise);
    });

    Promise.all(promises)
      .then(async results => {
        const notificationType = "Tm Import Progress Update";
        const notificationContent = `Tm Import is successfully started! Please check in the Import Status column and TM Server.`;
        sendBrowserNotification(notificationType, notificationContent);

        await this.setState({
          ...this.state,
          loading: false,
          uploadSuccess: false,
          success: false,
          importing: true
        })

        let newTmList = tmList;
        // iterate response of each row:
        results.forEach((res, i) => {
          let status = "";
          const subJSON = res.data;
          if (!Array.isArray(subJSON)) {
            newTmList[indexes[i]].status = subJSON.errorMessage;
          } else {
            res.data.forEach(subData => {
              let { filename, jobId, phase } = subData;
              status += `Import is in scheduled:&Filename: ${filename.substring(
                filename.indexOf("@") + 1
              )}&`;
              status += `Phase Name: ${phase}#`;

              // construct a new JSON object and save it to importing List
              // So that we can use it later to update the status via ajax call.
              let tmPath = filename.substring(0, filename.indexOf("@"));
              let subObj = {};
              subObj.phase = phase;
              subObj.tmPath = tmPath;
              subObj.filename = filename;
              subObj.jobId = jobId;
              subObj.rowIdx = indexes[i];
              importingList.uncompleted.push(subObj);
            });
            newTmList[indexes[i]].status = status;
          }
        });
      }).catch(err => {
        console.log(err);
        const errMessage = `${
          err.response.data
          }, please remember to click upload button in each row after dropping files.`;
        this.setState({
          ...this.state,
          errors: { global: errMessage }
        });
      }).finally(() => {
        this.setState({
          ...this.state,
          randomToken: "",
          files: [],
          importingList,
          importing: true,
          loading: false,
          uploadSuccess: false,
          success: false
        });
      });
  };

  render() {
    let { selectedRows } = this.state;
    const {
      tmList,
      sourceLocales,
      targetLocales,
      importingList,
      options,
      initLoading,
      files,
      unzipFiles,
      dropzoneActive,
      refetchImportTmListLoading,
      refetchExportTmListLoading,
      loading,
      uploadLoading,
      removedUploadedFileLoading,
      importing,
      isSelectAllRowsInCurrentPage,
      isSelectAllRowsOptionVisible,
      getImportProgressLoading,
      tokenExpiredLoading,
      uploadSuccess,
      success,
      unauthorized,
      errors
    } = this.state;

    if (unauthorized) {
      return <Redirect to={`/${process.env.TOOLURL}/login`} />;
    }

    const dropzoneStyle = {
      margin: "auto",
      width: "100%",
      height: "200px",
      borderWidth: "2px",
      borderColor: "rgb(102,102,102)",
      borderStyle: "dashed",
      borderRadius: "5px"
    };
    const dropStyle = {
      margin: "auto",
      width: "100%",
      height: "200px",
      borderWidth: "2px",
      borderColor: "rgb(102,204,102)",
      borderStyle: "solid",
      borderRadius: "5px",
      backgroundColor: "rgb(238,238,238)"
    };

    return (
      <div style={{ marginLeft: "20px", marginRight: "20px" }}>
        <Form>
          <Dimmer inverted active={initLoading}>
            <Loader indeterminate>Initial loading...</Loader>
          </Dimmer>
          <Dimmer inverted active={refetchImportTmListLoading}>
            <Loader indeterminate>
              Import Tm List is refetching, please wait...
            </Loader>
          </Dimmer>
          <Dimmer inverted active={uploadLoading}>
            <Loader indeterminate>Uploading files, please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={loading}>
            <Loader indeterminate>Importing Tm, please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={getImportProgressLoading}>
            <Loader indeterminate>
              Fetching Tm Import Progress, please wait...
            </Loader>
          </Dimmer>
          <Dimmer inverted active={tokenExpiredLoading}>
            <Loader indeterminate>Token expired, refreshing it...</Loader>
          </Dimmer>

          {Object.keys(errors).length > 0 && (
            <Message negative>
              <Message.Header>Tm Import Error:</Message.Header>
              {errors.global && <p>{errors.global}</p>}
              {errors.uploadFile && <p>{errors.uploadFile}</p>}
              {errors.uploadError && <p>{errors.uploadError}</p>}
              {errors.selectedRows && <p>{errors.selectedRows}</p>}
            </Message>
          )}
          {uploadSuccess && (
            <Message positive>
              <Message.Header>Tm Import Update</Message.Header>
              <p>Upload File is successful!</p>
            </Message>
          )}
          {(importing && importingList.uncompleted.length > 0) && (
            <Message positive icon>
              <Icon name='circle notched' loading />
              <Message.Content>
                <Message.Header>TM Import Status</Message.Header>
                <p>{importingList.uncompleted.length} / {importingList.uncompleted.length + importingList.completed.length} TMs are importing, please wait...</p>
              </Message.Content>
            </Message>
          )}
          {success && (
            <Message positive>
              <Message.Header>Tm Import Update</Message.Header>
              <p>
                Tm Import has completed! Please check in the Import
                Status column and TM Server.
              </p>
            </Message>
          )}
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column width={4}>
                <h4>Import TM</h4>
              </Grid.Column>
              <Grid.Column floated="right" width={5}>
                {refetchExportTmListLoading && (
                  <div>
                    <Icon
                      name="refresh"
                      color="orange"
                      loading={refetchExportTmListLoading}
                    />
                    <p>
                      Tm Import Process has started, now it is refreshing export
                      Tm list
                    </p>
                  </div>
                )}
              </Grid.Column>
            </Grid.Row>
          </Grid>

          <h4>1. Select Tm to import:</h4>
          {tmList.length >= 0 && (
            <Segment>
              <Grid columns={1}>
                <Grid.Row>
                  <Grid.Column floated="left" width={5}>
                    <p>
                      Total {selectedRows.length} of row
                      {selectedRows.length <= 1 ? " is" : "s are"}{" "}
                      selected.&nbsp;&nbsp;
                    </p>
                    {isSelectAllRowsOptionVisible && (
                      <Checkbox
                        onChange={this.onSelectAllRowsInCurrentPageChange}
                        checked={isSelectAllRowsInCurrentPage}
                        label={
                          isSelectAllRowsInCurrentPage
                            ? "Deselect all rows in current page"
                            : "Select all rows in current page"
                        }
                      />
                    )}
                  </Grid.Column>
                  <Grid.Column floated="right" width={3}>
                    <Button
                      basic
                      color="teal"
                      icon
                      labelPosition="left"
                      onClick={this.refetchImportTmList.bind(this)}
                    >
                      <Icon name="refresh" />
                      Refresh Import Tm List
                    </Button>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <br />
              <Grid columns={1}>
                <Grid.Row>
                  <Grid.Column>
                    <ReactTable
                      ref={r => {
                        this.reactTable = r;
                      }}
                      onPageChange={this.onTableViewChange}
                      onPageSizeChange={this.onTableViewChange}
                      onSortedChange={this.onTableViewChange}
                      onExpandedChange={this.onTableViewChange}
                      onFilteredChange={this.onTableViewChange}
                      onResizedChange={this.onTableViewChange}
                      data={tmList}
                      noDataText="No Tm is available for import"
                      defaultFilterMethod={filterMethod}
                      columns={[
                        {
                          Header: "Tm Name",
                          accessor: "tmName",
                          sortable: true,
                          filterable: true,
                          width: 250,
                          filterMethod: (filter, row) => row[filter.id].
                            toLowerCase().startsWith(filter.value.toLowerCase()),
                          filterAll: false
                        },
                        {
                          Header: "Tm Group Name",
                          accessor: "tmGroupName",
                          sortable: true,
                          filterable: true,
                          width: 150,
                          filterMethod: (filter, row) => row[filter.id].
                            toLowerCase().startsWith(filter.value.toLowerCase()),
                          filterAll: false
                        },
                        {
                          Header: "Source Language",
                          accessor: "sourceLocale",
                          width: 250,
                          filterable: true,
                          filterMethod: (filter, row) => {
                            if (filter.value == "all") {
                              return true;
                            }
                            return row.sourceLocale === filter.value;
                          },
                          Filter: ({ filter, onChange }) => (
                            <select
                              onChange={e => onChange(e.target.value)}
                              style={{ width: "100%" }}
                              value={filter ? filter.value : "all"}
                            >
                              <option value="all">Show All</option>
                              {map(sourceLocales, sourceLocale => (
                                <option key={sourceLocale} value={sourceLocale}>
                                  {sourceLocale}
                                </option>
                              ))}
                            </select>
                          )
                        },
                        {
                          Header: "Target Language",
                          accessor: "targetLocale",
                          width: 250,
                          filterable: true,
                          filterMethod: (filter, row) => {
                            if (filter.value == "all") {
                              return true;
                            }
                            return row.targetLocale === filter.value;
                          },
                          Filter: ({ filter, onChange }) => (
                            <select
                              onChange={e => onChange(e.target.value)}
                              style={{ width: "100%" }}
                              value={filter ? filter.value : "all"}
                            >
                              <option value="all">Show All</option>
                              {map(targetLocales, targetLocale => (
                                <option key={targetLocale} value={targetLocale}>
                                  {targetLocale}
                                </option>
                              ))}
                            </select>
                          )
                        },
                        {
                          Header: "Tm Short Code",
                          accessor: "tmPath",
                          sortable: true,
                          filterable: false,
                          width: 250
                        },
                        {
                          Header: "Import Status",
                          accessor: "status",
                          sortable: true,
                          filterable: false,
                          width: 400,
                          Cell: props => (
                            <TmImportStatus
                              str={props.value}
                              removeUploadedFiles={this.removeUploadedFiles.bind(this)}
                              removedUploadedFileLoading={
                                removedUploadedFileLoading
                              }
                              getTmImportProgress={this.getTmImportProgress.bind(this)}
                            />
                          )
                        }
                      ]}
                      getTrProps={(_, rowInfo) => {
                        return {
                          onClick: (e) => {
                            if (includeElement(selectedRows, rowInfo.index)) {
                              const newSelectedRows = removeElement(
                                selectedRows,
                                rowInfo.index
                              );
                              this.setState({
                                ...this.state,
                                uploadSuccess: false,
                                success: false,
                                selectedRows: newSelectedRows
                              });
                            } else {
                              const obj = {
                                index: rowInfo.index,
                                info: rowInfo.original
                              };
                              selectedRows.push(obj);
                              this.setState({
                                ...this.state,
                                uploadSuccess: false,
                                success: false,
                                selectedRows
                              });
                            }
                          },
                          style: {
                            background:
                              rowInfo &&
                                includeElement(selectedRows, rowInfo.index)
                                ? "#00afec"
                                : "white",
                            color:
                              rowInfo &&
                                includeElement(selectedRows, rowInfo.index)
                                ? "white"
                                : "black"
                          }
                        };
                      }}
                      pageSizeOptions={[10, 20, 50, 100, 200, 300]}
                      defaultPageSize={10}
                      className="-striped -highlight"
                      style={{ textAlign: "center" }}
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          )}
          <br />
          <h4>2. Select/Drop Tmx files and click upload button:</h4>
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column>
                <Dropzone
                  style={dropzoneActive ? dropStyle : dropzoneStyle}
                  onDragLeave={this.onDragLeave.bind(this)}
                  onDragEnter={this.onDragEnter.bind(this)}
                  onDrop={this.onDrop.bind(this)}
                  multiple={true}
                >
                  <div style={{ margin: "40px auto 0", width: "30%" }}>
                    <p>Drop/Select files to upload.</p>
                  </div>
                </Dropzone>
                <br />
                <Button
                  fluid
                  icon
                  labelPosition="left"
                  color="teal"
                  onClick={this.handleUploadClick.bind(this)}
                >
                  <Icon name="cloud upload" />
                  Upload
                </Button>
              </Grid.Column>
              <Grid.Column>
                <p>Pending: File selected for upload:</p>
                <ul>
                  {map(files, file => (
                    <li key={file.name}>
                      {!file.isUploaded && (
                        <div>
                          {file.name}&nbsp;&nbsp;&nbsp;
                          <Button
                            icon
                            size="mini"
                            onClick={this.removeUploadedFiles}
                          >
                            <Icon
                              name="window close"
                              loading={removedUploadedFileLoading}
                            >
                              <div filename={file.name} />
                            </Icon>
                          </Button>
                        </div>
                      )}
                      {file.uploadSuccess &&
                        file.isUploaded &&
                        !file.isZipFile && (
                          <div>
                            {file.name}&nbsp;&nbsp;&nbsp;is uploaded
                            successfully!&nbsp;&nbsp;
                            <Icon name="checkmark" color="green" />
                          </div>
                        )}
                      {!file.uploadSuccess &&
                        file.isUploaded &&
                        !file.isZipFile &&
                        !file.hasOwnProperty("errorMessage") && (
                          <div>
                            {file.name}&nbsp;&nbsp;&nbsp;is failed to upload,
                            because of no matching Tm of its locale.
                            <br />
                            &nbsp;&nbsp; Please choose the Tm with corresponding
                            locale and click upload button again.&nbsp;&nbsp;
                            <Icon name="remove" color="red" />
                          </div>
                        )}
                      {!file.uploadSuccess &&
                        file.isUploaded &&
                        !file.isZipFile &&
                        file.hasOwnProperty("errorMessage") && (
                          <div>
                            {file.name}&nbsp;&nbsp;&nbsp;is failed to upload,
                            because {file.errorMessage}.<br />
                            &nbsp;&nbsp; It will be skipped for
                            importing.&nbsp;&nbsp;
                            <Icon name="remove" color="red" />
                          </div>
                        )}
                      {file.uploadSuccess &&
                        file.isUploaded &&
                        file.isZipFile && <div>{file.name}:</div>}
                      {file.uploadSuccess &&
                        file.isUploaded &&
                        file.isZipFile &&
                        map(unzipFiles[file.name], subFile => (
                          <div key={subFile.filename}>
                            {subFile.status && (
                              <div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                {subFile.filename}&nbsp;&nbsp;&nbsp;is uploaded
                                successfully!&nbsp;&nbsp;
                                <Icon name="checkmark" color="green" />
                              </div>
                            )}
                            {!subFile.status &&
                              subFile.hasOwnProperty("errorMessage") && (
                                <div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  {subFile.filename}&nbsp;&nbsp;&nbsp;is failed
                                  to upload, because {subFile.errorMessage}
                                  &nbsp;&nbsp; It will be skipped for
                                  importing.&nbsp;&nbsp;
                                  <Icon name="remove" color="red" />
                                </div>
                              )}
                            {!subFile.status &&
                              !subFile.hasOwnProperty("errorMessage") && (
                                <div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  {subFile.filename}&nbsp;&nbsp;&nbsp;is failed
                                  to upload, because of no matching Tm of its
                                  locale.
                                  <br />
                                  &nbsp;&nbsp; Please choose the Tm with
                                  corresponding locale and click upload button
                                  again.&nbsp;&nbsp;
                                  <Icon name="remove" color="red" />
                                </div>
                              )}
                          </div>
                        ))}
                    </li>
                  ))}
                  {!this.state.uploadSuccess && (
                    <Button
                      basic
                      onClick={this.handleRemoveAllUploadedFiles.bind(this)}
                    >
                      Remove All Files
                    </Button>
                  )}
                </ul>
              </Grid.Column>
            </Grid.Row>
            <h4>3. Choose import options:</h4>
            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Tm Import Options:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Dropdown
                  placeholder="Select Tm Import Options"
                  name="tmImportOption"
                  value={options.tmImportOption}
                  fluid
                  search
                  selection
                  options={importTmOptions}
                  onChange={this.handleSelectionOptionChange.bind(this)}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Tm Attribute Import Options:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Dropdown
                  placeholder="Select Tm Attribute Import Options"
                  name="tmAttributeImportOption"
                  value={options.tmAttributeImportOption}
                  fluid
                  search
                  selection
                  options={importTmAttributesOptions}
                  onChange={this.handleSelectionOptionChange.bind(this)}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Checkbox
                  toggle
                  label="Optional: Ignore Invalid Tus during Import"
                  onChange={this.onIgnoreInvalidTusChange.bind(this)}
                  checked={options.ignoreInvalidTus}
                />
              </Grid.Column>
              <Grid.Column>
                <Checkbox
                  toggle
                  label="Optional: Keep Newer TU during Import"
                  onChange={this.onKeepNewerTuChange.bind(this)}
                  checked={options.keepNewerTu}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <br />
          <br />
          <Button
            floated="right"
            primary
            size="large"
            onClick={this.onSubmit}
            disabled={loading || initLoading}
          >
            Import
          </Button>
          <br />
          <br />
        </Form>
      </div>
    );
  }
}

ImportTmForm.propTypes = {
  fetchImportTmList: PropTypes.func.isRequired,
  uploadImportFile: PropTypes.func.isRequired,
  importTm: PropTypes.func.isRequired,
  fetchImportProgress: PropTypes.func.isRequired,
  validateRandomToken: PropTypes.func.isRequired,
  fetchExportTmList: PropTypes.func.isRequired,
  oauthLogin: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  tmServerVersion: state.tm.tmServerVersion,
  oauthUser: state.oidc.user,
  importTmListData: state.tm.importTmListData
});

export default connect(
  mapStateToProps,
  {
    fetchImportTmList,
    uploadImportFile,
    importTm,
    fetchImportProgress,
    validateRandomToken,
    fetchExportTmList,
    oauthLogin
  }
)(ImportTmForm);
