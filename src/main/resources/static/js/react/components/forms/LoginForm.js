import React, { Component } from 'react';
import { Button, Dimmer, Form, Loader, Message } from '../../utils/reactSemanticUI';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import InlineError from '../messages/InlineError';
import userManager from "../../utils/userManager";

class LoginForm extends Component {
  state = {
    credential: {
      username: '',
      password: ''
    },
    errors: {},
    oauthLogin: false,
    loading: false,
    success: false
  }

  onChange = (event) => this.setState({
    credential: {
      ...this.state.credential,
      [event.target.name]: event.target.value
    }
  })

  handleLogin = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const { credential } = this.state;
    const errors = this.validate(credential);
    this.setState({ ...this.state, errors });

    if (Object.keys(errors).length === 0) {
      this.setState({ ...this.state, loading: true });
      this.props
        .submit(this.state.credential)
        .catch(err => {
          let errors = {};
          errors.global = err.response.data.global;
          this.setState({ errors, loading: false });
        });
    }
  }

  handleTPAuthLogin = (event) => {
    event.preventDefault();
    event.stopPropagation();
    userManager.signinRedirect();
  }

  validate = (credential) => {
    let errors = {};
    if (!credential.username) {
      errors.username = "Username cannot be empty.";
    }
    if (!credential.password) {
      errors.password = "Password cannot be empty.";
    }
    return errors;
  }

  render() {
    const { credential, errors, loading, success, oauthLogin } = this.state;
    const { oauthUrl } = this.props;
    if (Object.keys(errors).length > 0) {
      window.scrollTo(0, 0);
    }
    if (oauthLogin && oauthUrl) {
      window.location.href = oauthUrl;
    }
    else if (success) {
      return <Redirect to={from} />;
    }
    return (
      <Form onSubmit={this.handleLogin.bind(this)}>
        {(errors && errors.global) && <Message negative>
          <Message.Header>Something went wrong</Message.Header>
          <p>{errors.global}</p>
        </Message>}
        <Dimmer inverted active={loading}>
          <Loader indeterminate>Login, please wait...</Loader>
        </Dimmer>
        <Form.Field required>
          <label htmlFor="email">Username:</label>
          <Form.Input
            error={!!errors && !!errors.username}
            type="text"
            id="username"
            name="username"
            placeholder="Username"
            value={credential.username}
            onChange={this.onChange}
          />
        </Form.Field>
        {(errors && errors.username) &&
          <InlineError text={errors.username} />}

        <Form.Field required>
          <label htmlFor="password">Password</label>
          <Form.Input
            error={!!errors && !!errors.password}
            type="password"
            id="password"
            name="password"
            placeholder="Password"
            value={credential.password}
            onChange={this.onChange}
          />
        </Form.Field>
        {(errors && errors.password) && <InlineError text={errors.password} />}
        <br />
        <Button primary size="medium" floated="right" onClick={this.handleLogin.bind(this)}
          disabled={loading}>Login</Button>
        <Button primary size="medium" floated="left" onClick={this.handleTPAuthLogin.bind(this)}
          disabled={loading}>Login with TPAuth</Button>
        <br /><br />
      </Form>
    )
  }
}

const mapStateToProps = state => ({
  oauthUrl: state.user.oauthUrl
});

export default connect(mapStateToProps, {})(LoginForm);
