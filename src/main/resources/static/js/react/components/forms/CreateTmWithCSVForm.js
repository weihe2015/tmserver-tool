import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import {
  Button,
  Checkbox,
  Dimmer,
  Form,
  Grid,
  Icon,
  Loader,
  Message,
  Modal,
  Popup
} from "../../utils/reactSemanticUI";

import Cookies from "universal-cookie";
import Dropzone from "react-dropzone";
import {
  fetchOrganizationList,
  fetchTmGroupList,
  fetchImportTmList,
  fetchTmGroupCode,
  validateTmGroupShortCode,
  createTmGroup,
  createTmWithCSV
} from "../../actions/tm";
import { sendBrowserNotification } from "../../utils/main";
import TmGroup from "./subComponents/TmGroup";
import CreateTmGroupForm from "./CreateTmGroupForm";
import CreateTmPdCsvOptions from './subComponents/CreateTmPdCsvOptions';
import CreateTmSampleCSV from "./subComponents/CreateTmSampleCSV";
import userManager from "../../utils/userManager";
import { oauthLogin } from '../../actions/auth';
import decode from 'jwt-decode';

const cookies = new Cookies();
// cookies expires in one year
const cookieExpiredTime = new Date(
  new Date().getTime() + 60 * 60 * 1000 * 24 * 365
);

const fetchTmInfoGeneralError = `Failed to fetch Tm Groups from TM Server`;
const fetchGroupsError = `Failed to fetch Tm Groups from TM Server,
  Please check the TM Server role setting and enable WS - Get TM List in System policy TM tab`;
class CreateTmWithCSVForm extends Component {
  state = {
    initLoading: false,
    refreshLoading: false,
    refreshImportTmListLoading: false,
    loading: false,
    createTmLoading: false,
    tokenExpiredLoading: false,
    organizationList: [],
    tmGroupList: [],
    options: {
      selectedOrganization: {},
      selectedTmGroup: {},
      selectedTmGroups: [],
      enabled: true,
      contextModeEnabled: true,
      createTmInMultipleTmGroup: false,
      pdCsvCreationEnabled: true,
      cleanupUsername: "",
      paClientName: "",
      addFileName: false,
      useGenericUsernameAsProfileName: false,
      emptyCleanupUserInfo: false
    },
    groupOptions: {
      tmGroupName: "",
      tmGroupShortcode: "",
      selectedOrganization: null
    },
    errors: {},
    groupErrors: {},
    success: false,
    dropzoneActive: false,
    files: [],
    uploadSuccess: false,
    isCreateTmGroupModalVisible: false,
    tmGroupNameCreated: "",
    tmGroupShortCodeCreateLoading: false,
    tmGroupLoading: false,
    tmGroupCreationSuccess: false,
    unauthorized: false,
    sampleCSVModalVisible: false
  };

  componentDidMount = async () => {
    // Check OAuth user credential and signin siliently
    const { organizationListData, tmGroupListData, oauthUser } = this.props;

    this.oauthInterval = setInterval(this.oauthSilentRefresh, 30 * 1000);
    let options = this.loadCreateTmOptionsFromCookies(this.state.options);
    const currentDate = new Date().getTime();
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      await this.oauthSilentRefresh();
    }

    if (!tmGroupListData || tmGroupListData.exp <= currentDate) {
      const info = {
        refreshTmList: true,
        refreshOrganizationList: true
      };
      if (oauthUser && !oauthUser.expired) {
        info.access_token = oauthUser.access_token;
      }
      try {
        this.setState({
          ...this.state,
          initLoading: true
        });
        let promises = [];
        const organizationListPromise = this.props.fetchOrganizationList(info);
        promises.push(organizationListPromise);
        const tmGroupListPromise = this.props.fetchTmGroupList(info);
        promises.push(tmGroupListPromise);
        await Promise.all(promises).then(res => {
          this.setState({
            options,
            organizationList: res[0].data,
            tmGroupList: res[1].data,
          });
        })
      }
      catch (err) {
        const { response } = err;
        // request is unauthorized: most likely the token is expired, direct user to login page:
        if (!!response && response.hasOwnProperty("status") && response.status === 401) {
          localStorage.clear();
          await this.setState({
            ...this.state,
            unauthorized: true,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
            }
          });
        }
        else if (!!response && response.hasOwnProperty("status") &&
          (response.status === 400 || response.status === 500)) {
          await this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: fetchGroupsError
            }
          });
        }
        else {
          await this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
            }
          });
        }
      }
    }
    else {
      await this.setState({
        ...this.state,
        options,
        organizationList: organizationListData.organizationList,
        tmGroupList: tmGroupListData.tmGroupList,
      });
    }
    this.setState({
      ...this.state,
      initLoading: false,
      success: false
    });
  }

  loadCreateTmOptionsFromCookies = (options) => {
    // Preload cookies values:
    if (!cookies.get("_enabledTmAfterCreation")) {
      options.enabled = true;
    }
    else {
      options.enabled = cookies.get("_enabledTmAfterCreation") === "true";
    }
    if (!cookies.get("_contextModeEnabled")) {
      options.contextModeEnabled = true;
    }
    else {
      options.contextModeEnabled = cookies.get("_contextModeEnabled") === "true";
    }
    if (!cookies.get("_addFileName")) {
      options.addFileName = true;
    }
    else {
      options.addFileName = cookies.get("_addFileName") === "true";
    }
    if (!cookies.get("_useGenericUsernameAsProfileName")) {
      options.useGenericUsernameAsProfileName = true;
    }
    else {
      options.useGenericUsernameAsProfileName = cookies.get("_useGenericUsernameAsProfileName") === "true";
    }
    if (!cookies.get("_emptyCleanupUserInfo")) {
      options.emptyCleanupUserInfo = true;
    }
    else {
      options.emptyCleanupUserInfo = cookies.get("_emptyCleanupUserInfo") === "true";
    }
    return options;
  }

  componentWillUnmount = () => {
    clearInterval(this.oauthInterval);
  }

  oauthSilentRefresh = async () => {
    const currentDate = new Date().getTime() / 1000;
    const { oauthUser } = this.props;
    // Refresh token at
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      this.setState({ ...this.state, tokenExpiredLoading: true });
      try {
        await userManager.signinSilent();
        const { access_token } = oauthUser;
        const payload = decode(access_token);
        let credentials = {};
        credentials.email = payload.sub;
        credentials.creation = payload.nbf;
        credentials.expiration = payload.exp;
        credentials.accessToken = access_token;
        credentials.idToken = oauthUser.id_token;
        await this.props.oauthLogin(credentials).catch(err => {
          console.log(err);
        })
      }
      catch (err) {
        console.log(err);
        localStorage.oauthLastLoc = window.location.pathname;
        await userManager.signinRedirect();
      }
      finally {
        this.setState({ ...this.state, tokenExpiredLoading: false });
      }
    }
  }

  handleSelectionOptionChange = (_, { name, value }) => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        [name]: value
      }
    });
  };

  handleOrganizationOptionChange = (selectedOption) => {
    let { groupErrors } = this.state;
    if (groupErrors.hasOwnProperty("organization")) {
      delete groupErrors.organization;
    }
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        selectedOrganization: selectedOption
      },
      groupOptions: {
        ...this.state.groupOptions,
        selectedOrganization: selectedOption
      }
    });
  };

  handleTmGroupOptionChange = (selectedOption) => {
    let { options } = this.state;
    const { createTmInMultipleTmGroup } = options;
    if (createTmInMultipleTmGroup) {
      options.selectedTmGroup = {};
      options.selectedTmGroups = selectedOption
    }
    else {
      // Use TM Group Name as PA Client Name by default
      // https://techqa1.translations.com/jira/browse/TMSTOOL-103
      if (options.pdCsvCreationEnabled) {
        options.paClientName = selectedOption.label;
      }
      options.selectedTmGroup = selectedOption;
      options.selectedTmGroups = [];
    }
    this.setState({
      ...this.state,
      success: false,
      options
    });
  };

  handlePDCSVCreationEnabledOption = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        pdCsvCreationEnabled: !this.state.options.pdCsvCreationEnabled
      }
    });
  };

  handlePdCsvFieldsChange = (event) => {
    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        [event.target.name]: event.target.value
      }
    });
  };

  onAddFileNameChange = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        addFileName: !this.state.options.addFileName
      }
    });
    cookies.set("_addFileName", !this.state.options.addFileName, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime,
      sameSite: "strict"
    });
  };

  onProfileNameChange = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        useGenericUsernameAsProfileName: !this.state.options
          .useGenericUsernameAsProfileName
      }
    });
    cookies.set(
      "_useGenericUsernameAsProfileName",
      !this.state.options.useGenericUsernameAsProfileName,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };


  onEmptyCleanupUserInfo = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        emptyCleanupUserInfo: !this.state.options.emptyCleanupUserInfo
      }
    });
    cookies.set(
      "_emptyCleanupUserInfo",
      !this.state.options.emptyCleanupUserInfo,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  refreshTmOrganizationAndGroupList = async () => {
    this.setState({ ...this.state, refreshLoading: true });
    const { oauthUser } = this.props;
    const info = {
      refreshTmList: true,
      refreshOrganizationList: true
    };
    if (oauthUser && !oauthUser.expired) {
      info.access_token = oauthUser.access_token;
    }
    try {
      let promises = [];
      const organizationListPromise = this.props.fetchOrganizationList(info);
      promises.push(organizationListPromise);

      const tmGroupListPromise = this.props.fetchTmGroupList(info);
      promises.push(tmGroupListPromise);

      await Promise.all(promises)
        .then(async (res) => {
          this.setState({
            ...this.state,
            organizationList: res[0].data,
            tmGroupList: res[1].data
          });
        })
        .catch(err => {
          this.setState({
            ...this.state,
            refreshLoading: false,
            success: false,
            errors: { ...this.state.errors, tmGroup: err.response.data.global }
          });
        });
    }
    catch (err) {
      const { response } = err;
      // request is unauthorized: most likely the token is expired, direct user to login page:
      if (!!response && response.hasOwnProperty("status") && response.status === 401) {
        localStorage.clear();
        this.setState({
          ...this.state,
          unauthorized: true,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : null
          }
        });
      }
      else {
        this.setState({
          ...this.state,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : null
          }
        });
      }
    }
    finally {
      this.setState({
        ...this.state,
        refreshLoading: false,
        success: false
      });
    }
  };

  handleTmEnabledOption = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        enabled: !this.state.options.enabled
      }
    });
    cookies.set("_enabledTmAfterCreation", !this.state.options.enabled, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime,
      samesite: "strict"
    });
  };

  handleTmContextModeEnabledOption = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        contextModeEnabled: !this.state.options.contextModeEnabled
      }
    });
    cookies.set("_contextModeEnabled", !this.state.options.contextModeEnabled, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime,
      samesite: "strict"
    });
  };

  handleCreateTmGroupModalVisible = () => {
    this.setState({
      ...this.state,
      success: false,
      isCreateTmGroupModalVisible: !this.state.isCreateTmGroupModalVisible
    });
  }

  handleTmGroupFields = (event) => {
    const { tmGroupList } = this.state;
    let { groupErrors, groupOptions } = this.state;
    let value = event.target.value;
    if (groupOptions.selectedOrganization === "") {
      groupErrors.organization = "Please select an organization";
    } else {
      delete groupErrors.organization;
    }
    if (event.target.name === 'tmGroupShortcode') {
      const tmGroupShortCode = value;
      if (tmGroupShortCode.length > 10) {
        groupErrors.tmGroupShortcode =
          "Tm Group Shortcode cannot exceed 10 characters";
        value = value.substring(0, 10);
      } else {
        delete groupErrors.tmGroupShortcode;
      }
    }
    else if (event.target.name === 'tmGroupName') {
      delete groupErrors.tmGroupName;
    }
    this.setState({
      ...this.state,
      success: false,
      tmGroupNameCreated: "",
      tmGroupCreationSuccess: false,
      groupErrors,
      groupOptions: {
        ...this.state.groupOptions,
        [event.target.name]: value
      }
    });
  };

  onTmGroupSubmit = async (event) => {
    event.preventDefault();

    let { groupOptions, tmGroupList } = this.state;
    const groupErrors = this.tmGroupValidate(groupOptions, tmGroupList);

    if (Object.keys(groupErrors).length !== 0) {
      this.setState({
        ...this.state,
        groupOptions: {
          ...this.state.groupOptions,
          tmGroupShortcode: ""
        },
        groupErrors
      });
      return;
    }
    this.setState({
      ...this.state,
      groupErrors: {},
      tmGroupLoading: true,
      tmGroupCreationSuccess: false
    });

    const info = {
      refreshTmList: true,
      refreshOrganizationList: true
    };
    const { oauthUser } = this.props;
    let data = {};
    if (oauthUser && !oauthUser.expired) {
      data.accessToken = oauthUser.access_token;
      groupOptions.accessToken = oauthUser.access_token;
      info.access_token = oauthUser.access_token;
    }
    try {
      // validate tm Group Short Code via API
      data.groupCode = groupOptions.tmGroupShortcode
      const res = await this.props.validateTmGroupShortCode(data);
      // Remove leading and trailing spaces:
      groupOptions.tmGroupName = groupOptions.tmGroupName.trim();
      await this.props.createTmGroup(groupOptions);

      let promises = [];
      const organizationListPromise = this.props.fetchOrganizationList(info);
      promises.push(organizationListPromise);

      const tmGroupListPromise = this.props.fetchTmGroupList(info);
      promises.push(tmGroupListPromise);

      await Promise.all(promises).then(async (res) => {
        this.setState({
          ...this.state,
          groupErrors: {},
          tmGroupNameCreated: groupOptions.tmGroupName,
          groupOptions: {
            tmGroupName: "",
            tmGroupShortcode: "",
            selectedOrganization: this.state.groupOptions.selectedOrganization
          },
          organizationList: res[0].data,
          tmGroupList: res[1].data
        });
      });
    } catch (err) {
      this.setState({
        ...this.state,
        groupErrors: {
          ...this.state.groupErrors,
          global: err.response.data.global
        }
      });
    }
    finally {
      this.setState({
        ...this.state,
        tmGroupLoading: false,
        success: false,
        tmGroupCreationSuccess: true
      });
    }
  }

  handleFetchTmGroupCode = async () => {
    let { groupOptions, groupErrors } = this.state;
    let { tmGroupName } = groupOptions;
    if (tmGroupName.length === 0 || groupErrors.hasOwnProperty("tmGroupName")) {
      return;
    }
    this.setState({ ...this.state, tmGroupShortCodeCreateLoading: true });
    const { oauthUser } = this.props;
    let data = {};
    if (oauthUser && !oauthUser.expired) {
      data.access_token = oauthUser.access_token;
    }
    data.name = tmGroupName;
    this.props.fetchTmGroupCode(data).then(res => {
      groupOptions.tmGroupShortcode = res.data.toString();
      this.setState({
        ...this.state,
        groupOptions
      });
    }).catch(err => {
      this.setState({
        ...this.state,
        groupErrors: {
          ...this.state.groupErrors,
          global: err.response.data.global
        }
      });
    }).finally(() => {
      this.setState({ ...this.state, tmGroupShortCodeCreateLoading: false });
    });
  };

  tmGroupValidate = (groupOptions, tmGroupList) => {
    let groupErrors = {};
    const {
      tmGroupName,
      tmGroupShortcode,
      selectedOrganization
    } = groupOptions;
    if (selectedOrganization === "") {
      groupErrors.organization = "Please select an organization";
    }
    if (tmGroupName === "") {
      groupErrors.tmGroupName = "Tm Group Name cannot be empty";
    }
    else {
      const duplicateTmGroupName = tmGroupList.filter(
        tmGroup => tmGroup.label === tmGroupName
      );
      if (duplicateTmGroupName.length > 0) {
        groupErrors.tmGroupName = "Tm Group Name has already exists";
      }
    }

    if (tmGroupShortcode === "") {
      groupErrors.tmGroupShortcode = "Tm Group Short Code cannot be empty";
    }
    else if (tmGroupShortcode.length > 10) {
      groupErrors.tmGroupShortcode = "Tm Group Shortcode cannot exceed 10 characters";
    }
    return groupErrors;
  };

  onSubmit = async (event) => {
    event.preventDefault();
    let { options, files } = this.state;
    const errors = this.validate(options, files);
    this.setState({ errors });
    if (Object.keys(errors).length !== 0) {
      return;
    }
    this.setState({
      ...this.state,
      createTmLoading: true,
      refreshImportTmListLoading: false
    });
    try {
      const { oauthUser } = this.props;
      if (oauthUser && !oauthUser.expired) {
        options.accessToken = oauthUser.access_token;
      }
      const res = await this.props.createTmWithCSV(options, files[0]);
      const notificationType = "Tm Creation Update";
      const notificationContent =
        "Tm Creation is successful! Please check in Tm Server.";
      sendBrowserNotification(notificationType, notificationContent);

      if (options.pdCsvCreationEnabled && !!res.data.body.pdCsvContent) {
        const time = new Date().toLocaleDateString("en-US").replace(/\//g, '-');
        const alasql = await import('alasql');
        alasql.default('SELECT * INTO CSV("' + time + '-PD_strings.csv",{headers:false,separator:","}) FROM ? ', [res.data.body.pdCsvContent]);
      }

      await this.setState({
        ...this.state,
        errors: {},
        createTmLoading: false,
        success: true,
        refreshImportTmListLoading: true
      });
      // refresh import Tm list after users creates TM
      const info = {
        type: "import",
        refresh: true
      };

      await this.props.fetchImportTmList(info);
    }
    catch (err) {
      const response = JSON.parse(err.res.text);
      const subResponse = JSON.parse(response.global);
      await this.setState({
        ...this.state,
        errors: {
          ...errors,
          global: subResponse[0].global,
          success: false
        }
      });
    }
    finally {
      this.setState({
        ...this.state,
        createTmLoading: false,
        refreshImportTmListLoading: false
      });
    }
  };

  onDragEnter = () => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      dropzoneActive: true
    });
  };

  onDragLeave = () => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      dropzoneActive: false
    });
  };

  onDrop = (acceptedFiles, rejectedFiles, e) => {
    let { files } = this.state;
    acceptedFiles.forEach(acceptedFile => {
      if (acceptedFile.name.endsWith(".csv")) {
        files.push(acceptedFile);
      }
    });
    this.setState({
      ...this.state,
      files,
      success: false,
      uploadSuccess: false,
      errors: {},
      dropzoneActive: false
    });
  };

  removeUploadedFiles = async e => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      files: []
    });
  };

  handleSampleCSVFileModal = e => {
    this.setState({
      ...this.state,
      sampleCSVModalVisible: !this.state.sampleCSVModalVisible
    });
  };

  handleCreateTmInMultipleTmGroupsOption = () => {
    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        createTmInMultipleTmGroup: !this.state.options.createTmInMultipleTmGroup
      }
    });
  };

  handleSampleCSVFileModalClose = () =>
    this.setState({ ...this.state, sampleCSVModalVisible: false });

  validate = (options, files) => {
    let errors = {};
    if (options.selectedOrganization === "") {
      errors.organization = "Please select organization";
    }
    if (options.selectedTmGroup === "") {
      errors.tmGroup = "Please select one Tm Group";
    }
    if (files.length == 0) {
      errors.file = "Please upload a CSV file";
    }
    return errors;
  };

  render() {
    const {
      initLoading,
      refreshLoading,
      refreshImportTmListLoading,
      loading,
      tokenExpiredLoading,
      organizationList,
      tmGroupList,
      options,
      errors,
      success,
      isCreateTmGroupModalVisible,
      tmGroupNameCreated,
      dropzoneActive,
      groupOptions,
      tmGroupLoading,
      createTmLoading,
      files,
      tmGroupCreationSuccess,
      sampleCSVModalVisible,
      groupErrors,
      unauthorized
    } = this.state;

    if (unauthorized) {
      return <Redirect to={`/${process.env.TOOLURL}/login`} />;
    }

    if (Object.keys(errors).length > 0) {
      window.scrollTo(0, 0);
    }

    const dropzoneStyle = {
      margin: "auto",
      width: "100%",
      height: "200px",
      borderWidth: "2px",
      borderColor: "rgb(102,102,102)",
      borderStyle: "dashed",
      borderRadius: "5px"
    };
    const dropStyle = {
      margin: "auto",
      width: "100%",
      height: "200px",
      borderWidth: "2px",
      borderColor: "rgb(102,204,102)",
      borderStyle: "solid",
      borderRadius: "5px",
      backgroundColor: "rgb(238,238,238)"
    };

    return (
      <div style={{ marginLeft: "50px", marginRight: "50px" }}>
        <Form>
          <Dimmer inverted active={initLoading}>
            <Loader indeterminate>Initial Loading, Please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={loading}>
            <Loader indeterminate>Loading, Please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={tokenExpiredLoading}>
            <Loader indeterminate>Token expired, refreshing it...</Loader>
          </Dimmer>
          <Dimmer inverted active={refreshLoading}>
            <Loader indeterminate>
              Refreshing Tm Group List and Organization List, please wait...
            </Loader>
          </Dimmer>
          <Dimmer inverted active={createTmLoading}>
            <Loader indeterminate>
              Tm Creation is in process, please wait...
            </Loader>
          </Dimmer>
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column width={4}>
                <h4>Create Tm</h4>
              </Grid.Column>
              <Grid.Column floated="right" width={5}>
                {refreshImportTmListLoading && (
                  <div>
                    <Icon
                      name="refresh"
                      color="orange"
                      loading={refreshImportTmListLoading}
                    />
                    <p>
                      Tm Creation is done, now it is refreshing import Tm list
                    </p>
                  </div>
                )}
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Grid columns={3} celled>
            {!success && errors && errors.global && (
              <Message negative>
                <Message.Header>Something went wrong</Message.Header>
                {errors.global}
              </Message>
            )}
            {success && (
              <Message positive>
                <Message.Header>Tm Creation Update</Message.Header>
                <p>Tm Creation is successful! Please check in Tm Server.</p>
              </Message>
            )}
            <Grid.Row>
              <Grid.Column>
                <Form.Field>
                  <label>Create TM in multiple TM Groups:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Checkbox
                  toggle
                  onClick={this.handleCreateTmInMultipleTmGroupsOption.bind(
                    this
                  )}
                />
              </Grid.Column>
              <Grid.Column>
                <Form.Field required>
                  <label>Tm Group:</label>
                </Form.Field>
              </Grid.Column>
              <TmGroup
                selectedTmGroup={options.createTmInMultipleTmGroup ? options.selectedTmGroups : options.selectedTmGroup}
                tmGroupList={tmGroupList}
                errors={errors}
                handleTmGroupOptionChange={this.handleTmGroupOptionChange.bind(this)}
                isMulti={options.createTmInMultipleTmGroup}
              />
              <Grid.Column>
                {process.env.INSTANCE === 'GL-TPT1' ? (
                  <Popup
                    trigger={
                      <Button
                        icon
                        color="purple"
                      >
                        <Icon name="plus" />
                          &nbsp;&nbsp;Add New TM Group
                        </Button>
                    }
                    content="No new clients to be created on this server, create them on tptprod3 instead."
                  />
                ) : (<Button
                  icon
                  color="purple"
                  onClick={this.handleCreateTmGroupModalVisible.bind(this)}
                >
                  <Icon name="plus" />
                    &nbsp;&nbsp;Add New TM Group
                </Button>)}
              </Grid.Column>
              <Grid.Column width={8}>
                <Popup
                  trigger={
                    <Button
                      float="right"
                      color="green"
                      onClick={this.refreshTmOrganizationAndGroupList.bind(this)}>
                      <Icon name="refresh" />
                      Refresh Tm Organization and Group List
                    </Button>
                  }
                  content="Refresh Tm Group List and Organization List"
                />
              </Grid.Column>
            </Grid.Row>
            {isCreateTmGroupModalVisible && (
              <CreateTmGroupForm
                selectedOrganization={options.selectedOrganization}
                handleOrganizationOptionChange={this.handleOrganizationOptionChange.bind(this)}
                organizationList={organizationList}
                errors={errors}
                groupErrors={groupErrors}
                tmGroupCreationSuccess={tmGroupCreationSuccess}
                tmGroupNameCreated={tmGroupNameCreated}
                tmGroupLoading={tmGroupLoading}
                tmGroupName={groupOptions.tmGroupName}
                tmGroupShortcode={groupOptions.tmGroupShortcode}
                handleTmGroupFields={this.handleTmGroupFields.bind(this)}
                onTmGroupSubmit={this.onTmGroupSubmit.bind(this)}
                handleCreateTmGroupModalVisible={this.handleCreateTmGroupModalVisible.bind(this)}
                tmGroupShortCodeCreateLoading={this.state.tmGroupShortCodeCreateLoading}
                fetchTmGroupCode={this.handleFetchTmGroupCode.bind(this)}
              />
            )}
            <Grid.Row>
              <Grid.Column>
                <Button
                  color="brown"
                  onClick={this.handleSampleCSVFileModal.bind(this)}
                >
                  Sample CSV file
                </Button>
                <Modal
                  size="large"
                  open={sampleCSVModalVisible}
                  closeOnEscape={false}
                  closeOnDimmerClick={true}
                  onClose={this.handleSampleCSVFileModalClose.bind(this)}
                >
                  <Modal.Header>Sample CSV file as table</Modal.Header>
                  <Modal.Content>
                    <p>
                      Please upload the CSV file in the format as following
                      table.
                    </p>
                    <CreateTmSampleCSV
                      createTmInMultiTmGroup={options.createTmInMultipleTmGroup}
                    />
                  </Modal.Content>
                  <Modal.Actions>
                    <Button
                      color="green"
                      onClick={this.handleSampleCSVFileModal.bind(this)}
                    >
                      Close
                    </Button>
                  </Modal.Actions>
                </Modal>
              </Grid.Column>
              <Grid.Column width={10}>
                <p>
                  Sample CSV content table that requires to correctly create TM
                </p>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Dropzone
                  style={dropzoneActive ? dropStyle : dropzoneStyle}
                  onDragLeave={this.onDragLeave.bind(this)}
                  onDragEnter={this.onDragEnter.bind(this)}
                  onDrop={this.onDrop.bind(this)}
                  multiple={false}
                >
                  <div style={{ textAlign: "center", marginTop: "50px" }}>
                    <h4>Drop/Select one CSV file to create TM</h4>
                  </div>
                </Dropzone>
              </Grid.Column>
              <Grid.Column>
                <p>Selected file:</p>
                {files.length > 0 && (
                  <div>
                    <p>
                      Filename: {files[0].name} &nbsp;&nbsp;&nbsp;
                      <Button
                        icon
                        size="mini"
                        name={files[0].name}
                        onClick={this.removeUploadedFiles.bind(this)}
                      >
                        <Icon name="window close" />
                      </Button>
                    </p>
                  </div>
                )}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Form.Field>
                  <Checkbox
                    label="Enable Tm(s) after creation:"
                    checked={options.enabled}
                    onChange={this.handleTmEnabledOption.bind(this)}
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field>
                  <Checkbox
                    label="Enable Context Mode"
                    checked={options.contextModeEnabled}
                    onChange={this.handleTmContextModeEnabledOption.bind(this)}
                  />
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
            <CreateTmPdCsvOptions
              options={options}
              errors={errors}
              handlePDCSVCreationEnabledOption={this.handlePDCSVCreationEnabledOption.bind(this)}
              handlePdCsvFieldsChange={this.handlePdCsvFieldsChange.bind(this)}
              onAddFileNameChange={this.onAddFileNameChange.bind(this)}
              onProfileNameChange={this.onProfileNameChange.bind(this)}
              onEmptyCleanupUserInfo={this.onEmptyCleanupUserInfo.bind(this)}
            />
          </Grid>
          <Button
            floated="right"
            primary
            size="large"
            disabled={loading || initLoading}
            onClick={this.onSubmit}
          >
            Create
          </Button>
          <br />
          <br />
        </Form>
      </div>
    );
  }
}

CreateTmWithCSVForm.propTypes = {
  fetchOrganizationList: PropTypes.func.isRequired,
  fetchTmGroupList: PropTypes.func.isRequired,
  fetchImportTmList: PropTypes.func.isRequired,
  createTmWithCSV: PropTypes.func.isRequired,
  fetchTmGroupCode: PropTypes.func.isRequired,
  oauthLogin: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  oauthUser: state.oidc.user,
  organizationListData: state.tm.organizationListData,
  tmGroupListData: state.tm.tmGroupListData
});

export default connect(
  mapStateToProps,
  {
    fetchOrganizationList,
    fetchTmGroupList,
    fetchImportTmList,
    fetchTmGroupCode,
    validateTmGroupShortCode,
    createTmGroup,
    createTmWithCSV,
    oauthLogin
  }
)(CreateTmWithCSVForm);
