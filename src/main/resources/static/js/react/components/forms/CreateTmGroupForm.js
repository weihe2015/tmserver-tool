import React from 'react';
import PropTypes from 'prop-types';
import InlineError from '../messages/InlineError';
import { Button, Dimmer, Form, Icon, Grid, Loader, Message } from '../../utils/reactSemanticUI';
import TmOrganization from './subComponents/TmOrganization';

const CreateTmGroupForm = (props) => (
  <Grid.Row>
    <Grid.Column width={12} stretched={true} >
      <div style={{ "marginLeft": "200px" }}>
        {(props.groupErrors && props.groupErrors.organization) &&
          <Message negative>
            <Message.Header>Tm Group Creation Error:</Message.Header>
            <p>{props.groupErrors.organization}</p>
          </Message>}
        {(props.groupErrors && props.groupErrors.global) &&
          <Message negative>
            <Message.Header>Tm Group Creation Error:</Message.Header>
            <p>{props.groupErrors.global}</p>
          </Message>}
        {(props.tmGroupCreationSuccess) && (
          <Message positive>
            <Message.Header>Tm Group Creation Update:</Message.Header>
            <p>Tm Group named
            &nbsp;{props.tmGroupNameCreated} is created successful!</p>
          </Message>
        )}
        <Dimmer inverted active={props.tmGroupLoading}>
          <Loader indeterminate>Creating Tm Group, Please wait...</Loader>
        </Dimmer>
        <Dimmer inverted active={props.tmGroupShortCodeCreateLoading}>
          <Loader indeterminate>Generating TM Group Shortcode, Please wait...</Loader>
        </Dimmer>
        <Grid columns={2}>
          <Grid.Column width={5}>
            <Form.Field required>
              <label>Organization:</label>
            </Form.Field>
          </Grid.Column>
          <Grid.Column width={11} floated="right">
            <TmOrganization
              selectedOrganization={props.selectedOrganization}
              handleOrganizationOptionChange={props.handleOrganizationOptionChange}
              organizationList={props.organizationList}
              errors={props.errors}
            />
          </Grid.Column>
        </Grid>
        <h4> Create New Tm Group:</h4>
        <div style={{ "marginLeft": "250px" }}>
          <Form.Field required>
            <label htmlFor="tmGroupName">TM Group Name:</label>
            <Form.Input
              error={!!props.groupErrors && !!props.groupErrors.tmGroupName}
              type="text"
              id="tmGroupName"
              name="tmGroupName"
              value={props.tmGroupName}
              onChange={props.handleTmGroupFields}
            />
            {(props.groupErrors && props.groupErrors.tmGroupName) &&
              <InlineError text={props.groupErrors.tmGroupName} />}
          </Form.Field>
          <Grid columns={2}>
            <Grid.Column width={9}>
              <Form.Field required>
                <label htmlFor="tmGroupShortcode">
                  TM Group Short Code:
                </label>
                <Form.Input
                  error={!!props.groupErrors && !!props.groupErrors.tmGroupShortcode}
                  type="text"
                  id="tmGroupShortcode"
                  name="tmGroupShortcode"
                  value={props.tmGroupShortcode}
                  onChange={props.handleTmGroupFields}
                />
                {(props.groupErrors && props.groupErrors.tmGroupShortcode) &&
                  <InlineError text={props.groupErrors.tmGroupShortcode} />}
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={7}>
              <div style={{ "marginTop": "20px" }}>
                <Button color="teal" onClick={props.fetchTmGroupCode}
                  disabled={props.tmGroupName.length === 0 || !!props.groupErrors.tmGroupName}>
                  <Icon name='pencil' />Generate randomly</Button>
              </div>
            </Grid.Column>
          </Grid>

          <br />
          <Button floated="right" color='green'
            onClick={props.onTmGroupSubmit}>
            <Icon name='checkmark' />
            Apply
          </Button>
          <Button basic floated="right" color='blue'
            onClick={props.handleCreateTmGroupModalVisible}>
            <Icon name='remove' />Close
          </Button>
        </div>
      </div>
    </Grid.Column>
  </Grid.Row>
);

CreateTmGroupForm.propTypes = {
  groupErrors: PropTypes.shape(),
  tmGroupCreationSuccess: PropTypes.bool.isRequired,
  tmGroupNameCreated: PropTypes.string.isRequired,
  tmGroupLoading: PropTypes.bool.isRequired,
  tmGroupName: PropTypes.string.isRequired,
  tmGroupShortcode: PropTypes.string.isRequired,
  handleTmGroupFields: PropTypes.func.isRequired,
  onTmGroupSubmit: PropTypes.func.isRequired,
  handleCreateTmGroupModalVisible: PropTypes.func.isRequired
}

export default CreateTmGroupForm;


