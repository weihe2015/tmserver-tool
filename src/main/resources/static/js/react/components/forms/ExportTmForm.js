import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import {
  Button,
  Checkbox,
  Dimmer,
  Form,
  Grid,
  Icon,
  Loader,
  Message,
  Segment,
  Popup
} from "../../utils/reactSemanticUI";
import ReactTable from "react-table";
import Cookies from "universal-cookie";
import { Helmet } from "react-helmet";
import DayPicker, { DateUtils } from "react-day-picker";
import map from "lodash/map";
import {
  fetchExportTmList,
  exportTm,
  fetchExportProgress,
  deleteExportTmInfo
} from "../../actions/tm";
import { saveByteToFile } from "../../utils/downloadFile";
import {
  includeElement,
  removeElement,
  filterMethod,
  sendBrowserNotification,
  sortLocales
} from "../../utils/main";
import userManager from "../../utils/userManager";
import { oauthLogin } from '../../actions/auth';
import decode from 'jwt-decode';

const cookies = new Cookies();
// cookies expires in one year
const cookieExpiredTime = new Date(
  new Date().getTime() + 60 * 60 * 1000 * 24 * 365
);
class ExportTmForm extends Component {
  state = {
    tmList: [],
    sourceLocales: [],
    targetLocales: [],
    options: {
      isIncludePrivateTus: false,
      isIncludeAttributes: false
    },
    exportFilter: {
      creationUser: "",
      modificationUser: "",
      workgroup: "",
      attributes: []
    },
    creationDateRange: {
      from: undefined,
      to: undefined
    },
    modificationDateRange: {
      from: undefined,
      to: undefined
    },
    isExportedToFtp: false,
    exportDatesCheckbox: false,
    selectedRows: [],
    currentPageRows: [],
    indexes: [],
    exportedIndexes: [],
    isSelectAllRowsInCurrentPage: false,
    isSelectAllRowsOptionVisible: false,
    initLoading: false,
    refetchExportTmListLoading: false,
    loading: false,
    tokenExpiredLoading: false,
    fetchExportProgressLoading: false,
    success: false,
    unauthorized: false,
    exporting: false,
    exportTmMessage: "",
    errors: {}
  };

  componentDidMount = async () => {
    this.oauthInterval = setInterval(this.oauthSilentRefresh, 30 * 1000);
    this.exportTmProgressInterval = setInterval(this.getTmExportProgress, 15 * 1000);
    this.exportTmListInterval = setInterval(this.tmExportListPeriodRefresh, 3 * 56 * 1000)

    const { exportTmListData, oauthUser } = this.props;
    this.setState({ initLoading: true });
    const info = {
      type: "export",
      refresh: false
    };

    let options = this.loadExportTmOptionsFromCookies(this.state.options);
    let tmList = [];
    const currentDate = new Date().getTime();
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      await this.oauthSilentRefresh();
    }
    if (!exportTmListData || exportTmListData.exp <= currentDate) {
      try {
        const promise = await this.props.fetchExportTmList(info);
        tmList = promise.data;
      }
      catch (err) {
        const { response } = err;
        // request is unauthorized: most likely the token is expired, direct user to login page:
        if (!!response && response.hasOwnProperty("status") && response.status === 401) {
          localStorage.clear();
          await this.setState({
            ...this.state,
            unauthorized: true,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : null
            }
          });
        } else {
          await this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : null
            }
          });
        }
      }
      finally {
        await this.setState({
          ...this.state,
          initLoading: false,
          success: false,
        });
      }
    }
    else {
      tmList = exportTmListData.exportTmList;
    }
    let sourceLocales = [];
    let targetLocales = [];
    if (tmList !== undefined && tmList.length > 0) {
      const sortedLocales = sortLocales(tmList);
      sourceLocales = sortedLocales.sourceLocales;
      targetLocales = sortedLocales.targetLocales;
    }
    this.setState({
      ...this.state,
      uploadSuccess: false,
      success: false,
      initLoading: false,
      tmList,
      sourceLocales,
      targetLocales,
      options
    });
  };

  componentWillUnmount = () => {
    clearInterval(this.oauthInterval);
    clearInterval(this.exportTmProgressInterval);
    clearInterval(this.exportTmListInterval);
  }

  oauthSilentRefresh = async () => {
    const currentDate = new Date().getTime() / 1000;
    const { oauthUser } = this.props;
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      this.setState({ ...this.state, tokenExpiredLoading: true });
      try {
        await userManager.signinSilent();
        const { access_token } = oauthUser;
        const payload = decode(access_token);
        let credentials = {};
        credentials.email = payload.sub;
        credentials.creation = payload.nbf;
        credentials.expiration = payload.exp;
        credentials.accessToken = access_token;
        credentials.idToken = oauthUser.id_token;
        await this.props.oauthLogin(credentials).catch(err => {
          console.log(err);
        })
      }
      catch (err) {
        localStorage.oauthLastLoc = window.location.pathname;
        await userManager.signinRedirect();
        console.log(err);
      }
      finally {
        this.setState({ ...this.state, tokenExpiredLoading: false });
      }
    }
  }

  tmExportListPeriodRefresh = () => {
    const { exportTmListData } = this.props;
    const currentDate = new Date().getTime();
    if (!!exportTmListData && exportTmListData.exp > currentDate) {
      return;
    }
    this.refetchExportTmList();
  }

  loadExportTmOptionsFromCookies = (options) => {
    // Preload cookies values:
    if (!cookies.get("_isIncludePrivateTus")) {
      options.isIncludePrivateTus = false;
    }
    else {
      options.isIncludePrivateTus = cookies.get("_isIncludePrivateTus") === "true";
    }
    if (!cookies.get("_isIncludeAttributes")) {
      options.isIncludeAttributes = false;
    }
    else {
      options.isIncludeAttributes = cookies.get("_isIncludeAttributes") === "true";
    }
    return options;
  }

  refetchExportTmList = async () => {
    // event.preventDefault();
    // event.stopPropagation();

    this.setState({
      ...this.state,
      success: false,
      refetchExportTmListLoading: true
    });
    const info = {
      type: "export",
      refresh: true
    };

    let tmList = [];
    try {
      const promise = await this.props.fetchExportTmList(info);
      tmList = promise.data;
    }
    catch (err) {
      const { response } = err;
      // request is unauthorized: most likely the token is expired, direct user to login page:
      if (!!response && response.hasOwnProperty("status") && response.status === 401) {
        localStorage.clear();
        await this.setState({
          ...this.state,
          unauthorized: true,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : null
          }
        });
      }
      else {
        await this.setState({
          ...this.state,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : null
          }
        });
      }
    }
    finally {
      let sourceLocales = [];
      let targetLocales = [];
      if (tmList !== undefined && tmList.length > 0) {
        const sortedLocales = sortLocales(tmList);
        sourceLocales = sortedLocales.sourceLocales;
        targetLocales = sortedLocales.targetLocales;
      }
      this.setState({
        ...this.state,
        tmList,
        sourceLocales,
        targetLocales,
        refetchExportTmListLoading: false,
        success: false,
      });
    }
  };

  handlePrivateTusCheckBox = (event, data) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        isIncludePrivateTus: data.checked
      }
    });
    cookies.set(
      "_isIncludePrivateTus",
      !this.state.options.isIncludePrivateTus,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  handleAttributesCheckBox = (event, data) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        isIncludeAttributes: data.checked
      }
    });
    cookies.set(
      "_isIncludeAttributes",
      !this.state.options.isIncludeAttributes,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  handleIsExportedToFtp = () => {
    this.setState({
      ...this.state,
      success: false,
      isExportedToFtp: !this.state.isExportedToFtp
    });
  };

  handleExportDateOptionCheckBox = () => {
    this.setState({
      ...this.state,
      exportDatesCheckbox: !this.state.exportDatesCheckbox
    });
  };

  handleCreationDayClick = (day) => {
    const creationDateRange = DateUtils.addDayToRange(day, this.state.creationDateRange);
    this.setState({
      ...this.state,
      creationDateRange
    });
  };

  handleModificationDayClick = (day) => {
    const modificationDateRange = DateUtils.addDayToRange(day, this.state.modificationDateRange);
    this.setState({
      ...this.state,
      modificationDateRange
    });
  };

  resetExportTmCreationDate = () => {
    this.setState({
      ...this.state,
      creationDateRange: {
        from: undefined,
        to: undefined
      }
    });
  };

  resetExportTmModificationDate = () => {
    this.setState({
      ...this.state,
      modificationDateRange: {
        from: undefined,
        to: undefined
      }
    });
  };

  onExportFilterChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      ...this.state,
      exportFilter: {
        ...this.state.exportFilter,
        [name]: value,
      }
    });
  }

  addNewAttributeRow = () => {
    const attribute = {
      name: "",
      value: "",
      hide: false
    };
    let { attributes } = this.state.exportFilter;
    attributes.push(attribute);
    this.setState({
      ...this.state,
      exportFilter: {
        ...this.state.exportFilter,
        attributes
      }
    });
  }

  hideNewAttribute = async (event) => {
    const idx = event.target.id;
    let { attributes } = this.state.exportFilter;
    attributes = attributes.map((attribute, i) => {
      if (i == idx) {
        attribute['hide'] = true;
      }
      return attribute;
    });
    const filter = await import("lodash/filter");
    attributes = filter.default(attributes, { hide: false });
    this.setState({
      ...this.state,
      exportFilter: {
        ...this.state.exportFilter,
        attributes
      }
    });
  }

  onAttributeFieldsChange = (event) => {
    const { name, value } = event.target;
    const targetName = name.split("_")[0];
    const idx = parseInt(name.split("_")[1], 10);
    let { attributes } = this.state.exportFilter;
    attributes = attributes.map((attribute, i) => {
      if (i == idx) {
        attribute[targetName] = value;
      }
      return attribute;
    });
    this.setState({
      ...this.state,
      exportFilter: {
        ...this.state.exportFilter,
        attributes
      }
    });
  }

  validate = () => {
    const errors = {};
    const { selectedRows } = this.state;
    if (selectedRows.length === 0) {
      errors.selectedRows = "Please select at leat one TM for Export";
    }
    return errors;
  };

  onSubmit = async (event) => {
    event.preventDefault();

    const errors = this.validate();
    this.setState({ ...this.state, errors });
    const {
      selectedRows,
      options,
      isExportedToFtp,
      creationDateRange,
      modificationDateRange,
      exportFilter
    } = this.state;
    let { tmList } = this.state;
    if (Object.keys(errors).length !== 0) {
      return;
    }
    this.setState({
      ...this.state,
      loading: true,
      success: false
    });

    let promises = [];
    const { oauthUser } = this.props;
    let { indexes } = this.state;

    selectedRows.forEach(async (selectedRow, _) => {
      const info = selectedRow.info;
      indexes.push(selectedRow.index);
      let data = {
        sourceLocaleCode: info.sourceLocaleCode,
        targetLocaleCode: info.targetLocaleCode,
        tmGroupName: info.tmGroupName,
        tmName: info.tmName,
        tmPath: info.tmPath,
        tmExportType: "TMX",
        includeAttributes: options.isIncludeAttributes,
        includePrivateTus: options.isIncludePrivateTus,
        exportedToFtp: isExportedToFtp,
        exportFilter: {}
      };
      data = this.moveExportFilterInfo(data, creationDateRange, modificationDateRange, exportFilter);
      if (oauthUser && !oauthUser.expired) {
        data.accessToken = oauthUser.access_token;
      }
      const promise = this.props.exportTm(data);
      promises.push(promise);
    });

    Promise.all(promises).then(results => {
      let newTmList = tmList;
      results.forEach((res, i) => {
        newTmList[indexes[i]].status = res.data.status;
        newTmList[indexes[i]].jobId = res.data.jobId;
        newTmList[indexes[i]].exportToFtp = res.data.exportToFtp;
      });
      this.setState({
        ...this.state,
        errors: {},
        tmList: newTmList,
        indexes,
        exportedIndexes: indexes,
        loading: false,
        exporting: true
      });
    });
  };

  moveExportFilterInfo = (data, creationDateRange, modificationDateRange, exportFilter) => {
    if (creationDateRange.from != undefined && creationDateRange.to != undefined) {
      data.exportFilter.creationDateStart = new Date(creationDateRange.from).getTime();
      data.exportFilter.creationDateEnd = new Date(creationDateRange.to).getTime();
    }
    else if (creationDateRange.from !== undefined) {
      data.exportFilter.creationDateStart = new Date(creationDateRange.from).getTime();
      data.exportFilter.creationDateEnd = 0;
    }
    else {
      data.exportFilter.creationDateStart = 0;
      data.exportFilter.creationDateEnd = 0;
    }

    if (modificationDateRange.from != undefined && modificationDateRange.to != undefined) {
      data.exportFilter.modificationDateStart = new Date(modificationDateRange.from).getTime();
      data.exportFilter.modificationDateEnd = new Date(modificationDateRange.to).getTime();
    }
    else if (creationDateRange.from !== undefined) {
      data.exportFilter.modificationDateStart = new Date(modificationDateRange.from).getTime();
      data.exportFilter.modificationDateEnd = 0;
    }
    else {
      data.exportFilter.modificationDateStart = 0;
      data.exportFilter.modificationDateEnd = 0;
    }
    data.exportFilter.creationUser = exportFilter.creationUser;
    data.exportFilter.modificationUser = exportFilter.modificationUser;
    data.exportFilter.workgroup = exportFilter.workgroup
    data.exportFilter.attributes = exportFilter.attributes.map((attribute, _) => {
      delete attribute.hide;
      return attribute;
    });
    return data;
  }

  getTmExportProgress = async () => {
    let { indexes, tmList, exportedIndexes } = this.state;
    if (indexes.length == 0) {
      return;
    }
    const { oauthUser } = this.props;
    let promises = [];
    this.setState({
      ...this.state,
      fetchExportProgressLoading: true
    });
    indexes.forEach((index, _) => {
      const { jobId, tmPath, exportToFtp } = tmList[index];
      let data = {
        jobId,
        tmPath: tmPath.replace('/', '_'),
        exportToFtp
      }
      if (oauthUser && !oauthUser.expired) {
        data.access_token = oauthUser.access_token;
      }
      const promise = this.props.fetchExportProgress(data);
      promises.push(promise);

    });

    let newIndexes = [];
    Promise.all(promises).then(async (results) => {
      results.forEach((res, i) => {
        const { exportToFtp } = tmList[indexes[i]];
        if (exportToFtp === false) {
          if (!!res.data.headers.filename) {
            const filename = res.data.headers.filename;
            const data = res.data.data;
            saveByteToFile(filename, data);
            tmList[indexes[i]].status = "FINISHED";
          }
          else {
            tmList[indexes[i]].status = "START";
            newIndexes.push(indexes[i]);
          }
        }
        else if (exportToFtp === true) {
          const status = res.data.data.status;
          tmList[indexes[i]].status = status;
          if (status !== 'FINISHED' && status !== 'PENDING' && status !== 'FAILED') {
            newIndexes.push(indexes[i]);
          }
        }
      });

      if (newIndexes.length == 0) {
        // call APIs to remove all cached Export TM info
        let deletePromises = [];
        exportedIndexes.forEach((idx, _) => {
          const { jobId, tmPath } = tmList[idx];
          const data = {
            jobId,
            tmPath: tmPath.replace('/', '_'),
          };
          const promise = this.props.deleteExportTmInfo(data);
          deletePromises.push(promise);
        });
        Promise.all(deletePromises).then(_ => {
        }).catch(err => {
          console.log(err);
        });
        await this.setState({
          ...this.state,
          tmList,
          indexes: newIndexes,
          success: true,
          exporting: false,
          exportedIndexes: []
        });
      }
      else {
        await this.setState({
          ...this.state,
          tmList,
          indexes: newIndexes
        });
      }
    }).catch(err => {
      console.log(err);
    }).finally(() => {
      this.setState({
        ...this.state,
        fetchExportProgressLoading: false
      });
    })
  }

  onTableViewChange = () => {
    const table = this.reactTable;
    if (table) {
      const page = table.state.page;
      const pageSize = table.state.pageSize;
      const allData = table.getResolvedState().sortedData;
      const startIdx = page * pageSize;
      const currentPageRows = allData
        .slice(startIdx, startIdx + pageSize)
        .map(item => {
          let obj = { index: item._index, info: item._original };
          return obj;
        });

      this.setState({
        ...this.state,
        isSelectAllRowsOptionVisible: true,
        currentPageRows
      });
    }
  };

  onSelectAllRowsInCurrentPageChange = () => {
    const { isSelectAllRowsInCurrentPage, currentPageRows } = this.state;
    let { selectedRows } = this.state;
    // If select all rows in current page option turns from false to true
    // Add all currentPageRows into selectedRows
    if (!isSelectAllRowsInCurrentPage) {
      currentPageRows.forEach(currentPageRow => {
        if (!includeElement(selectedRows, currentPageRow.index)) {
          selectedRows.push(currentPageRow);
        }
      });
      this.setState({
        ...this.state,
        selectedRows,
        isSelectAllRowsInCurrentPage: true
      });
    }
    // If select all rows in current page option turns from true to false,
    // we need to remove current page Rows from selectedRows:
    else {
      currentPageRows.forEach(currentPageRow => {
        if (includeElement(selectedRows, currentPageRow.index)) {
          selectedRows = removeElement(selectedRows, currentPageRow.index);
        }
      });
      this.setState({
        ...this.state,
        selectedRows,
        isSelectAllRowsInCurrentPage: false
      });
    }
  };

  render() {
    let { selectedRows } = this.state;
    const {
      tmList,
      sourceLocales,
      targetLocales,
      indexes,
      options,
      initLoading,
      refetchExportTmListLoading,
      isExportedToFtp,
      creationDateRange,
      modificationDateRange,
      exportFilter,
      loading,
      tokenExpiredLoading,
      fetchExportProgressLoading,
      exportDatesCheckbox,
      isSelectAllRowsInCurrentPage,
      isSelectAllRowsOptionVisible,
      success,
      exporting,
      unauthorized,
      errors
    } = this.state;

    const c_modifiers = { start: creationDateRange.from, end: creationDateRange.to };
    const m_modifiers = { start: modificationDateRange.from, end: modificationDateRange.to };

    if (unauthorized) {
      return <Redirect to={`/${process.env.TOOLURL}/login`} />;
    }

    return (
      <div style={{ marginLeft: "20px", marginRight: "20px" }}>
        <h4>Export TM</h4>
        <Form>
          <Dimmer inverted active={initLoading}>
            <Loader indeterminate>Initial loading...</Loader>
          </Dimmer>
          <Dimmer inverted active={refetchExportTmListLoading}>
            <Loader indeterminate>
              Export Tm List is refetching, please wait...
            </Loader>
          </Dimmer>
          <Dimmer inverted active={tokenExpiredLoading}>
            <Loader indeterminate>Token expired, refreshing it...</Loader>
          </Dimmer>
          <Dimmer inverted active={loading}>
            <Loader indeterminate>Exporting Tm, please wait..</Loader>
          </Dimmer>
          <Dimmer inverted active={fetchExportProgressLoading}>
            <Loader indeterminate>Fetching Export TM progress, please wait..</Loader>
          </Dimmer>
          {Object.keys(errors).length > 0 && (
            <Message negative>
              <Message.Header>Something went wrong</Message.Header>
              {errors.global && <p>{errors.global}</p>}
              {errors.selectedRows && <p>{errors.selectedRows}</p>}
              {errors.exportDateFilter && <p>{errors.exportDateFilter}</p>}
            </Message>
          )}
          {(exporting && indexes.length > 0) && (
            <Message positive icon>
              <Icon name='circle notched' loading />
              <Message.Content>
                <Message.Header>TM Export Status</Message.Header>
                <p>{indexes.length} TMs are exporting, please wait...</p>
              </Message.Content>
            </Message>
          )}
          {success && (
            <Message positive icon>
              <Message.Content>
                <Message.Header>TM Export Status</Message.Header>
                <p>All TMs have been exported</p>
              </Message.Content>
            </Message>
          )}
          <h4>1. Select Tm to export:</h4>
          {tmList !== undefined && tmList.length > 0 && (
            <Segment>
              <Grid columns={1}>
                <Grid.Row>
                  <Grid.Column floated="left" width={5}>
                    <p>
                      {selectedRows.length} of TM
                      {selectedRows.length <= 1 ? " is" : "s are"} selected.
                    </p>
                    {isSelectAllRowsOptionVisible && (
                      <Checkbox
                        onChange={this.onSelectAllRowsInCurrentPageChange}
                        checked={isSelectAllRowsInCurrentPage}
                        label={
                          isSelectAllRowsInCurrentPage
                            ? "Deselect all rows in current page"
                            : "Select all rows in current page"
                        }
                      />
                    )}
                  </Grid.Column>
                  <Grid.Column floated="right" width={3}>
                    <Button
                      basic
                      color="teal"
                      icon
                      labelPosition="left"
                      onClick={this.refetchExportTmList.bind(this)}
                    >
                      <Icon name="refresh" />
                      Refresh Export Tm List
                    </Button>
                  </Grid.Column>
                  {/* <Grid.Column>
                    <Button
                      basic
                      onClick={this.getTmExportProgress.bind(this)}
                    ></Button>
                  </Grid.Column> */}
                </Grid.Row>
              </Grid>
              <br />
              <ReactTable
                ref={r => {
                  this.reactTable = r;
                }}
                onPageChange={this.onTableViewChange}
                onPageSizeChange={this.onTableViewChange}
                onSortedChange={this.onTableViewChange}
                onExpandedChange={this.onTableViewChange}
                onFilteredChange={this.onTableViewChange}
                onResizedChange={this.onTableViewChange}
                data={tmList}
                noDataText="No Tm is available for export"
                filterable
                defaultFilterMethod={filterMethod}
                defaultFiltered={[
                  {
                    id: "sourceLocale",
                    value: "all"
                  }
                ]}
                columns={[
                  {
                    Header: "Tm Name",
                    accessor: "tmName",
                    sortable: true,
                    filterable: true,
                    width: 250,
                    filterMethod: (filter, row) => row[filter.id].
                      toLowerCase().startsWith(filter.value.toLowerCase()),
                    filterAll: false
                  },
                  {
                    Header: "Tm Group Name",
                    accessor: "tmGroupName",
                    sortable: true,
                    filterable: true,
                    width: 150,
                    filterMethod: (filter, row) => row[filter.id].
                      toLowerCase().startsWith(filter.value.toLowerCase()),
                    filterAll: false
                  },
                  {
                    Header: "Source Language",
                    accessor: "sourceLocale",
                    width: 250,
                    filterable: true,
                    filterMethod: (filter, row) => {
                      if (filter.value == "all") {
                        return true;
                      }
                      return row.sourceLocale === filter.value;
                    },
                    Filter: ({ filter, onChange }) => (
                      <select
                        onChange={e => onChange(e.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}
                      >
                        <option value="all">Show All</option>
                        {map(sourceLocales, sourceLocale => (
                          <option key={sourceLocale} value={sourceLocale}>
                            {sourceLocale}
                          </option>
                        ))}
                      </select>
                    )
                  },
                  {
                    Header: "Target Language",
                    accessor: "targetLocale",
                    width: 250,
                    filterable: true,
                    filterMethod: (filter, row) => {
                      if (filter.value == "all") {
                        return true;
                      }
                      return row.targetLocale === filter.value;
                    },
                    Filter: ({ filter, onChange }) => (
                      <select
                        onChange={e => onChange(e.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}
                      >
                        <option value="all">Show All</option>
                        {map(targetLocales, targetLocale => (
                          <option key={targetLocale} value={targetLocale}>
                            {targetLocale}
                          </option>
                        ))}
                      </select>
                    )
                  },
                  {
                    Header: "Tm Short Code",
                    accessor: "tmPath",
                    sortable: true,
                    filterable: false,
                    width: 250
                  },
                  {
                    Header: "Total Tus",
                    accessor: "totalTus",
                    sortable: true,
                    filterable: false,
                    width: 100
                  },
                  {
                    Header: "Total Public Tus",
                    accessor: "numberOfPublicTus",
                    sortable: true,
                    filterable: false,
                    width: 150
                  },
                  {
                    Header: "Total Private Tus",
                    accessor: "numberOfPrivateTus",
                    sortable: true,
                    filterable: false,
                    width: 150
                  },
                  {
                    Header: "Last Tu Modification Date",
                    accessor: "lastTuModificationDate",
                    sortable: true,
                    filterable: false,
                    width: 250
                  },
                  {
                    Header: "Export Status",
                    accessor: "status",
                    sortable: true,
                    filterable: false,
                    width: 250
                  }
                ]}
                getTrProps={(_, rowInfo) => {
                  return {
                    onClick: (_) => {
                      if (includeElement(selectedRows, rowInfo.index)) {
                        const newSelectedRows = removeElement(
                          selectedRows,
                          rowInfo.index
                        );
                        this.setState({
                          ...this.state,
                          success: false,
                          selectedRows: newSelectedRows
                        });
                      } else {
                        const obj = {
                          index: rowInfo.index,
                          info: rowInfo.original
                        };
                        selectedRows.push(obj);
                        this.setState({
                          ...this.state,
                          success: false,
                          selectedRows
                        });
                      }
                    },
                    style: {
                      background:
                        rowInfo && includeElement(selectedRows, rowInfo.index)
                          ? "#00afec"
                          : "white",
                      color:
                        rowInfo && includeElement(selectedRows, rowInfo.index)
                          ? "white"
                          : "black"
                    }
                  };
                }}
                pageSizeOptions={[10, 20, 50, 100, 200, 300]}
                defaultPageSize={10}
                className="-striped -highlight"
                style={{ textAlign: "center" }}
              />
            </Segment>
          )}
          <h4>2. Choose export options:</h4>
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column>
                <Checkbox
                  toggle
                  label="Include Private Tus"
                  checked={options.isIncludePrivateTus}
                  onChange={this.handlePrivateTusCheckBox.bind(this)}
                />
              </Grid.Column>
              <Grid.Column>
                <Checkbox
                  toggle
                  label="Include Attributes"
                  checked={options.isIncludeAttributes}
                  onChange={this.handleAttributesCheckBox.bind(this)}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Popup
                  trigger={
                    <Checkbox
                      toggle
                      label="Export TMX to FTP"
                      checked={isExportedToFtp}
                      onChange={this.handleIsExportedToFtp.bind(this)}
                    />
                  }
                  content="Note: If this option is checked, exported TMX will be sent to FTP. If not, it will be download directly."
                />
              </Grid.Column>
              <Grid.Column>
                <Checkbox
                  toggle
                  label="Optional, Export TM based on additional filter"
                  onClick={this.handleExportDateOptionCheckBox.bind(this)}
                />
              </Grid.Column>
            </Grid.Row>
            {exportDatesCheckbox && (
              <Grid.Row>
                <Grid.Column width="6">
                  <h4>Creation Date:</h4>
                  <DayPicker
                    className="Selectable"
                    numberOfMonths={2}
                    onDayClick={this.handleCreationDayClick.bind(this)}
                    modifiers={c_modifiers}
                    selectedDays={[creationDateRange.from, creationDateRange]}
                    disabledDays={day => day > (new Date())}
                  />
                  <Button
                    floated="right"
                    color="green"
                    onClick={this.resetExportTmCreationDate.bind(this)}
                  >
                    Reset Creation Date
                  </Button>
                </Grid.Column>
                <Grid.Column width="6" floated="right">
                  <h4>Modification Date:</h4>
                  <DayPicker
                    className="Selectable"
                    numberOfMonths={2}
                    onDayClick={this.handleModificationDayClick.bind(this)}
                    modifiers={m_modifiers}
                    selectedDays={[modificationDateRange.from, modificationDateRange]}
                    disabledDays={day => day > (new Date())}
                  />
                  <Helmet>
                    <style>{`
                  .Selectable .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
                    background-color: #f0f8ff !important;
                    color: #4a90e2;
                  }
                  .Selectable .DayPicker-Day {
                    border-radius: 0 !important;
                  }
                  .Selectable .DayPicker-Day--start {
                    border-top-left-radius: 50% !important;
                    border-bottom-left-radius: 50% !important;
                  }
                  .Selectable .DayPicker-Day--end {
                    border-top-right-radius: 50% !important;
                    border-bottom-right-radius: 50% !important;
                  }
                `}</style>
                  </Helmet>
                  <Button
                    floated="right"
                    color="green"
                    onClick={this.resetExportTmModificationDate.bind(this)}
                  >
                    Reset Modification Date
                  </Button>
                </Grid.Column>
              </Grid.Row>
            )}
            {/* creationUser, modificationUser or workgroup */}
            {exportDatesCheckbox && (
              <Grid.Row>
                <Grid.Column width={10}>
                  <Form.Group widths='equal'>
                    <Form.Input
                      fluid label='Creation User:'
                      type="text"
                      id="creationUser"
                      name="creationUser"
                      value={exportFilter.creationUser}
                      onChange={this.onExportFilterChange.bind(this)}
                    />
                    <Form.Input
                      fluid label='Modification User:'
                      type="text"
                      id="modificationUser"
                      name="modificationUser"
                      value={exportFilter.modificationUser}
                      onChange={this.onExportFilterChange.bind(this)}
                    />
                    <Form.Input
                      fluid label='WorkGroup:'
                      type="text"
                      id="workgroup"
                      name="workgroup"
                      value={exportFilter.workgroup}
                      onChange={this.onExportFilterChange.bind(this)}
                    />
                  </Form.Group>
                </Grid.Column>
                <Grid.Column width={6}>
                  <h4>TM Attributes:</h4>
                  <Grid columns={2} celled>
                    {map(exportFilter.attributes, ({ name, value, _ }, idx) => (
                      <Grid.Row key={idx}>
                        <Grid.Column>
                          <Form.Field required>
                            <label>Attribute Name:</label>
                            <Form.Input
                              type="text"
                              name={`name_${idx}`}
                              onChange={this.onAttributeFieldsChange.bind(this)}
                              value={name}
                            />
                          </Form.Field>
                        </Grid.Column>
                        <Grid.Column>
                          <Form.Field required>
                            <label>Attribute Value:</label>
                            <Form.Input
                              type="text"
                              name={`value_${idx}`}
                              onChange={this.onAttributeFieldsChange.bind(this)}
                              value={value}
                            />
                          </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={6} floated='right'>
                          <Button color="red" id={idx} onClick={this.hideNewAttribute.bind(this)}>
                            <Icon name="delete" />
                            Delete
                            </Button>
                        </Grid.Column>
                      </Grid.Row>
                    ))}
                    <Grid.Row>
                      <Grid.Column width={8} floated='right'>
                        <Button color="green" onClick={this.addNewAttributeRow.bind(this)}>
                          <Icon name="add" />
                          Add new attribute
                        </Button>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </Grid.Column>
              </Grid.Row>
            )}
          </Grid>
          <br />
          <br />
          <Button
            floated="right"
            primary
            size="large"
            disabled={loading || initLoading || exporting}
            onClick={this.onSubmit}
          >
            Export
          </Button>
          <br />
          <br />
        </Form>
      </div>
    );
  }
}

ExportTmForm.propTypes = {
  fetchExportTmList: PropTypes.func.isRequired,
  exportTm: PropTypes.func.isRequired,
  oauthLogin: PropTypes.func.isRequired,
  fetchExportProgress: PropTypes.func.isRequired,
  deleteExportTmInfo: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  oauthUser: state.oidc.user,
  exportTmListData: state.tm.exportTmListData
});

export default connect(
  mapStateToProps,
  {
    fetchExportTmList,
    exportTm,
    oauthLogin,
    fetchExportProgress,
    deleteExportTmInfo
  }
)(ExportTmForm);
