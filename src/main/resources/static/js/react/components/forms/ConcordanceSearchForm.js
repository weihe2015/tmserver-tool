import React, { Component } from "react";
import {
  Button,
  Checkbox,
  Dimmer,
  Dropdown,
  Form,
  Grid,
  Loader,
  Message
} from "../../utils/reactSemanticUI";

import { connect } from "react-redux";
import { Redirect } from "react-router";
import PropTypes from "prop-types";
import Select from "react-select";
import map from "lodash/map";
import Cookies from "universal-cookie";

import InlineError from "../messages/InlineError";
import { fetchSearchTmList } from "../../actions/tm";
import { searchLocationOptions } from "../common";
import {
  onLocaleChangeWithTmName,
  sendBrowserNotification
} from "../../utils/main";
import userManager from "../../utils/userManager";
import { oauthLogin } from '../../actions/auth';
import decode from 'jwt-decode';

const cookies = new Cookies();
// cookies expires in one year
const cookieExpiredTime = new Date(
  new Date().getTime() + 60 * 60 * 1000 * 24 * 365
);
class ConcordanceSearchForm extends Component {
  state = {
    options: {
      query: "",
      searchLocation: {
        value: "SOURCE_AND_TARGET",
        label: "SOURCE_AND_TARGET"
      },
      resultNum: 200,
      caseSensitive: false,
      matchWholeWord: false,
      exactMatch: false,
      publicTuOnly: false,
      selectedTmList: ""
    },
    selectedTmList: [],
    isDisplayAdvancedSearchOptions: true,
    displayLocale: true,
    initLoading: false,
    loading: false,
    tokenExpiredLoading: false,
    allTmList: [],
    success: false,
    unauthorized: false,
    searchTmListQuery: "",
    errors: {}
  };

  componentDidMount = async () => {
    this.oauthInterval = setInterval(this.oauthSilentRefresh, 30 * 1000);

    const { searchTmListData, oauthUser } = this.props;
    const { displayLocale } = this.state;
    let [options, isDisplayAdvancedSearchOptions] = this.loadSearchOptionsFromCookies(this.state.options);

    const currentDate = new Date().getTime();
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      await this.oauthSilentRefresh();
    }
    this.setState({ initLoading: true });
    const info = {
      type: "concordance"
    };
    let allTmList = [];
    if (!searchTmListData || searchTmListData.exp <= currentDate) {
      try {
        const promise = await this.props.fetchSearchTmList(info);
        allTmList = promise.data;
      }
      catch (err) {
        const { response } = err;
        // request is unauthorized: most likely the token is expired, direct user to login page:
        if (!!response && response.hasOwnProperty("status") && response.status === 401) {
          localStorage.clear();
          this.setState({
            initLoading: false,
            unauthorized: true
          });
        } else {
          this.setState({ initLoading: false });
        }
      }
      finally {
        this.setState({ initLoading: false });
      }
    }
    else {
      allTmList = searchTmListData.searchTmList;
    }
    allTmList = onLocaleChangeWithTmName(displayLocale, allTmList);
    this.setState({
      success: false,
      allTmList,
      options,
      isDisplayAdvancedSearchOptions,
      initLoading: false
    });
  };

  componentWillUnmount = () => {
    clearInterval(this.oauthInterval);
  }

  oauthSilentRefresh = async () => {
    const currentDate = new Date().getTime() / 1000;
    const { oauthUser } = this.props;
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      this.setState({ ...this.state, tokenExpiredLoading: true });
      try {
        await userManager.signinSilent();
        const { access_token } = oauthUser;
        const payload = decode(access_token);
        let credentials = {};
        credentials.email = payload.sub;
        credentials.creation = payload.nbf;
        credentials.expiration = payload.exp;
        credentials.accessToken = access_token;
        credentials.idToken = oauthUser.id_token;
        await this.props.oauthLogin(credentials).catch(err => {
          console.log(err);
        })
      }
      catch (err) {
        localStorage.oauthLastLoc = window.location.pathname;
        await userManager.signinRedirect();
        console.log(err);
      }
      finally {
        this.setState({ ...this.state, tokenExpiredLoading: false });
      }
    }
  }

  loadSearchOptionsFromCookies = (options) => {
    // Preload cookies values:
    let isDisplayAdvancedSearchOptions = true;
    if (!cookies.get("_searchLocation")) {
      options.searchLocation = { value: "SOURCE_AND_TARGET", label: "SOURCE_AND_TARGET" };
    }
    else {
      options.searchLocation = cookies.get("_searchLocation");
    }
    if (!cookies.get("_isCaseSensitive")) {
      options.caseSensitive = false;
    }
    else {
      options.caseSensitive = cookies.get("_isCaseSensitive") === "true";
    }
    if (!cookies.get("_isMatchWholeWord")) {
      options.matchWholeWord = false;
    }
    else {
      options.matchWholeWord = cookies.get("_isMatchWholeWord") === "true";
    }
    if (!cookies.get("_isExactMatch")) {
      options.exactMatch = true;
    }
    else {
      options.exactMatch = cookies.get("_isExactMatch") === "true";
    }
    if (!cookies.get("_isPublicTuOnly")) {
      options.publicTuOnly = true;
    }
    else {
      options.publicTuOnly = cookies.get("_isPublicTuOnly") === "true";
    }
    if (!cookies.get("_displayAdvancedSearchOptions")) {
      isDisplayAdvancedSearchOptions = true;
    }
    else {
      isDisplayAdvancedSearchOptions = cookies.get("_displayAdvancedSearchOptions") === "true";
    }
    return [options, isDisplayAdvancedSearchOptions];
  }

  onHandleSearchQueryChange = (event, data) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      options: { ...this.state.options, query: data.searchQuery }
    });
  };

  handleSelectionOptionChange = (e, { value }) => {
    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        query: value
      }
    });
  };

  onOptionChange = (event) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      errors: {},
      success: false,
      isDisplayAdvancedSearchOptions: !this.state.isDisplayAdvancedSearchOptions
    });
    cookies.set(
      "_displayAdvancedSearchOptions",
      !this.state.isDisplayAdvancedSearchOptions,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  onCaseSensitiveChange = event => {
    this.setState({
      ...this.state,
      errors: {},
      success: false,
      options: {
        ...this.state.options,
        caseSensitive: !this.state.options.caseSensitive
      }
    });
    cookies.set("_isCaseSensitive", !this.state.options.caseSensitive, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime
    });
  };

  onMatchWholeWordChange = event => {
    this.setState({
      ...this.state,
      success: false,
      errors: {},
      options: {
        ...this.state.options,
        matchWholeWord: !this.state.options.matchWholeWord
      }
    });
    cookies.set("_isMatchWholeWord", !this.state.options.matchWholeWord, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime
    });
  };

  onExactMatchChange = event => {
    this.setState({
      ...this.state,
      success: false,
      errors: {},
      options: {
        ...this.state.options,
        exactMatch: !this.state.options.exactMatch
      }
    });
    cookies.set("_isExactMatch", !this.state.options.exactMatch, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime
    });
  };

  onPublicTuOnlyChange = (event) => {
    this.setState({
      ...this.state,
      success: false,
      errors: {},
      options: {
        ...this.state.options,
        publicTuOnly: !this.state.options.publicTuOnly
      }
    });
    cookies.set("_isPublicTuOnly", !this.state.options.publicTuOnly, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime
    });
  };

  handlesearchLocationOption = (selectedOption) => {
    this.setState({
      ...this.state,
      success: false,
      errors: {},
      options: { ...this.state.options, searchLocation: selectedOption }
    });
    cookies.set("_searchLocation", selectedOption.value, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime
    });
  };

  onDisplayLocaleChange = (event) => {
    const { displayLocale } = this.state;
    let { allTmList } = this.state;
    allTmList = onLocaleChangeWithTmName(!displayLocale, allTmList);
    this.setState({
      ...this.state,
      allTmList,
      displayLocale: !this.state.displayLocale
    });
  };

  handleSelectChange = selectedOption => {
    this.setState({
      ...this.state,
      success: false,
      errors: {},
      selectedTmList: selectedOption
    });
  };

  handSearchTmQueryChange = query => {
    this.setState({
      ...this.state,
      searchTmListQuery: query
    });
  };

  onChange = (event) =>
    this.setState({
      ...this.state,
      success: false,
      errors: {},
      options: { ...this.state.options, [event.target.name]: event.target.value }
    });

  onSubmit = async (event) => {
    event.preventDefault();

    let { options, selectedTmList, allTmList } = this.state;
    if (selectedTmList.length > 0) {
      let tempList = [];
      selectedTmList.forEach(selectedTm => {
        tempList.push({ label: selectedTm.label, value: selectedTm.value });
      });
      options.selectedTmList = tempList;
    }
    // User must select one Tm to search
    const errors = this.validate(options, allTmList);
    this.setState({ ...this.state, options, errors });
    if (Object.keys(errors).length !== 0) {
      return;
    }
    // save this search string:
    let prevSearchHistory = localStorage.searchInputHistory;
    if (prevSearchHistory === undefined) {
      prevSearchHistory = [];
    } else {
      prevSearchHistory = JSON.parse(prevSearchHistory);
    }
    const newSearchInput = {
      value: options.query,
      key: new Date().getTime(),
      text: options.query
    };
    // limit the search history size to 8:
    if (prevSearchHistory.length === 8) {
      prevSearchHistory.pop();
    }
    // Check if this search query is the same of last one:
    if (prevSearchHistory.length === 0) {
      // add the most recent search input on the top:
      prevSearchHistory.unshift(newSearchInput);
    } else {
      const lastSearchItem = prevSearchHistory[0];
      if (lastSearchItem.value !== newSearchInput.value) {
        // add the most recent search input on the top:
        prevSearchHistory.unshift(newSearchInput);
      }
    }

    localStorage.searchInputHistory = JSON.stringify(prevSearchHistory);
    // Change because of React-Select v2
    const prevSearchLocation = options.searchLocation;
    options.searchLocation = options.searchLocation.value;

    this.setState({ ...this.state, loading: true });

    const { oauthUser } = this.props;
    if (oauthUser && !oauthUser.expired) {
      options.accessToken = oauthUser.access_token;
    }
    this.props.submit(options).then(() => {
      const notificationType = "Tm Search Update";
      const notificationContent = `Tm Concordance Search is successful! Please check back and scoll down the page.`;
      sendBrowserNotification(notificationType, notificationContent);

      this.setState({
        ...this.state,
        success: true,
        options: {
          ...this.state.options,
          searchLocation: prevSearchLocation
        },
        errors: {},
        loading: false
      });
    }).catch(err => {
      this.setState({
        ...this.state,
        options: {
          ...this.state.options,
          searchLocation: prevSearchLocation
        },
        success: false,
        loading: false,
        error: err.response.data.global
      });
    }).finally(() => {
      this.setState({ ...this.state, loading: false });
    });
  };

  validate = (options, allTmList) => {
    const errors = {};
    if (options.query === "") {
      errors.query = "Search terms cannot be empty";
    }
    if (options.resultNum < 0) {
      errors.resultNum = "Search Result Number cannot be nagative";
    }
    if (options.selectedTmList.length < 1) {
      errors.selectedTmList = "Please select at least one Tm";
    }
    if (allTmList.length == 0) {
      errors.global = "No Tm is available for searching.";
    }
    return errors;
  };

  render() {
    const {
      options,
      isDisplayAdvancedSearchOptions,
      initLoading,
      tokenExpiredLoading,
      displayLocale,
      selectedTmList,
      loading,
      allTmList,
      unauthorized,
      errors,
      searchTmListQuery
    } = this.state;
    // Get previous search input from localStorage.
    let prevSearchHistory = localStorage.searchInputHistory;
    if (prevSearchHistory !== undefined) {
      prevSearchHistory = JSON.parse(prevSearchHistory);
    } else {
      prevSearchHistory = [];
    }

    if (unauthorized) {
      return <Redirect to={`/${process.env.TOOLURL}/login`} />;
    }

    return (
      <div style={{ marginLeft: "20px", marginRight: "20px" }}>
        <h4>Concordance Search</h4>
        <Form onSubmit={this.onSubmit}>
          {errors && errors.global && (
            <Message negative>
              <Message.Header>Something went wrong</Message.Header>
              <p>{errors.global}</p>
            </Message>
          )}
          {!isDisplayAdvancedSearchOptions && Object.keys(errors).length > 0 && (
            <Message negative>
              <Message.Header>Concordance Search Input Error:</Message.Header>
              {map(errors, error => (
                <p key={error}>{error}</p>
              ))}
              <p>{errors.global}</p>
            </Message>
          )}
          <Dimmer inverted active={initLoading}>
            <Loader indeterminate>Initial loading, please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={loading}>
            <Loader indeterminate>Searching, please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={tokenExpiredLoading}>
            <Loader indeterminate>Token expired, refreshing it...</Loader>
          </Dimmer>
          <div style={{ margin: "10px 200px 0px 50px" }}>
            <Dropdown
              icon="search"
              placeholder="Enter search terms or content"
              noResultsMessage="No previous search query"
              minCharacters={3}
              searchQuery={options.query}
              value={options.query}
              onSearchChange={this.onHandleSearchQueryChange.bind(this)}
              fluid
              search
              selection
              onChange={this.handleSelectionOptionChange.bind(this)}
              header={
                <Dropdown.Header icon="search" content="Recent searches" />
              }
              options={prevSearchHistory}
            />
          </div>
          {isDisplayAdvancedSearchOptions && !loading && (
            <div style={{ margin: "50px" }}>
              <legend>Advanced Search Options:</legend>
              <br />
              <br />
              <Grid columns={6}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <Form.Field>
                      <label>Search Location</label>
                      <Select
                        name="role"
                        isClearable={false}
                        isSearchable={false}
                        defaultValue={searchLocationOptions[2]}
                        value={options.searchLocation}
                        onChange={this.handlesearchLocationOption}
                        options={searchLocationOptions}
                        style={{ width: "60%" }}
                      />
                    </Form.Field>
                  </Grid.Column>

                  <Grid.Column width={2}>
                    <Form.Field>
                      <label>Maximum Return Results</label>
                      <Form.Input
                        type="number"
                        id="resultNum"
                        name="resultNum"
                        min="1"
                        value={options.resultNum}
                        onChange={this.onChange}
                        style={{ width: "50%" }}
                      />
                      {errors && errors.returnNum && (
                        <InlineError text={errors.returnNum} />
                      )}
                    </Form.Field>
                  </Grid.Column>

                  <Grid.Column width={2}>
                    <Form.Field>
                      <Checkbox
                        name="caseSensitive"
                        label="Make Case Sensitive"
                        onChange={this.onCaseSensitiveChange}
                        checked={options.caseSensitive}
                      />
                    </Form.Field>
                  </Grid.Column>

                  <Grid.Column width={2}>
                    <Form.Field>
                      <Checkbox
                        name="matchWholeWord"
                        label="Match Whole Word"
                        onChange={this.onMatchWholeWordChange}
                        checked={options.matchWholeWord}
                      />
                    </Form.Field>
                  </Grid.Column>

                  <Grid.Column width={2}>
                    <Form.Field>
                      <Checkbox
                        name="exactMatch"
                        label="Exact Match"
                        onChange={this.onExactMatchChange}
                        checked={options.exactMatch}
                      />
                    </Form.Field>
                  </Grid.Column>
                  <Grid.Column width={2}>
                    <Form.Field>
                      <Checkbox
                        name="publicTuOnly"
                        label="Public TU Only"
                        onChange={this.onPublicTuOnlyChange}
                        checked={options.publicTuOnly}
                      />
                    </Form.Field>
                  </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                  <Grid.Column width={6}>
                    <Form.Field>
                      <label>Search in TM</label>
                      <Select
                        name="searchTmList"
                        placeholder="Select Tm for search"
                        isClearable={true}
                        isSearchable={true}
                        closeMenuOnSelect={false}
                        onSelectResetsInput={false}
                        onInputChange={this.handSearchTmQueryChange}
                        inputValue={searchTmListQuery}
                        isMulti
                        onChange={this.handleSelectChange}
                        options={allTmList}
                        value={selectedTmList}
                      />
                      {errors && errors.selectedTmList && (
                        <InlineError text={errors.selectedTmList} />
                      )}
                    </Form.Field>
                  </Grid.Column>
                  <Grid.Column>
                    <Form.Field>
                      <div style={{ marginTop: "20px" }}>
                        <Checkbox
                          label="Display Locale with Tm Name"
                          onChange={this.onDisplayLocaleChange}
                          checked={displayLocale}
                        />
                      </div>
                    </Form.Field>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          )}
          <br />
          <br />
          <Checkbox
            float="right"
            label="Show Advanced Search Options"
            onChange={this.onOptionChange}
            checked={isDisplayAdvancedSearchOptions}
          />
          <Button
            floated="right"
            primary
            size="large"
            disabled={loading || initLoading}
          >
            Search
          </Button>
        </Form>
      </div>
    );
  }
}

ConcordanceSearchForm.propTypes = {
  fetchSearchTmList: PropTypes.func.isRequired,
  oauthLogin: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  oauthUser: state.oidc.user,
  searchTmListData: state.tm.searchTmListData
});

export default connect(
  mapStateToProps,
  {
    fetchSearchTmList,
    oauthLogin
  }
)(ConcordanceSearchForm);
