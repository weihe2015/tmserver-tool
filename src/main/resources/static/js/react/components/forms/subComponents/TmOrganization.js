import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Select from 'react-select';
import InlineError from '../../messages/InlineError';
import { Form, Grid } from '../../../utils/reactSemanticUI';

const TmOrganization = (props) => (
  <Grid.Column width={8}>
  <Form.Field>
    <Select
      name="organization"
      placeholder="Select Organization"
      isClearable={true}
      isSearchable={true}
      value={Object.keys(props.selectedOrganization).length === 0 ? '' : 
        props.selectedOrganization}
      onChange={props.handleOrganizationOptionChange}
      options={props.organizationList}
    />
    {(props.errors && props.errors.organization) &&
      <InlineError text={props.errors.organization} />}
  </Form.Field>
</Grid.Column>
);

TmOrganization.propTypes = {
  selectedOrganization: PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string
  }).isRequired,
  organizationList: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string
    })
  ).isRequired,
  handleOrganizationOptionChange: PropTypes.func.isRequired
}
export default connect(null, {})(TmOrganization);