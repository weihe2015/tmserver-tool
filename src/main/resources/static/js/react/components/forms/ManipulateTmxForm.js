import React, { Component } from "react";
import {
  Button,
  Checkbox,
  Dimmer,
  Form,
  Grid,
  Icon,
  Loader,
  Message
} from "../../utils/reactSemanticUI";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Cookies from "universal-cookie";

import map from "lodash/map";
import Dropzone from "react-dropzone";
import { saveByteToFile } from "../../utils/downloadFile";
import {
  uploadTmxFileForManipulation,
  manipulateTmxFiles
} from "../../actions/tm";
import { sendBrowserNotification } from "../../utils/main";

const cookies = new Cookies();
// cookies expires in one year
const cookieExpiredTime = new Date(
  new Date().getTime() + 60 * 60 * 1000 * 24 * 365
);
class ManipulateTmxForm extends Component {
  state = {
    options: {
      newAttributes: [
        {
          key: "",
          value: "",
          hide: false
        }
      ],
      addFilenameAsAttribute: false,
      removePreviousAttribute: false,
      removeTrailingSpaces: false,
      insertChecksum: false,
      moveAttributesToTu: false,
      randomToken: ""
    },
    dropzoneActive: false,
    files: [],
    unzipFiles: {}, // used to display tmx files unzip from zip file
    uploadLoading: false,
    uploadSuccess: false,
    loading: false,
    success: false,
    outputFilename: "",
    unauthorized: false,
    errors: {}
  };

  componentDidMount = () => {
    // Preload cookies values:
    const addFilenameAsAttribute =
      cookies.get("_addFilenameAsAttribute") === undefined
        ? false
        : cookies.get("_addFilenameAsAttribute") === "true";
    const removePreviousAttribute =
      cookies.get("_removePreviousAttribute") === undefined
        ? false
        : cookies.get("_removePreviousAttribute") === "true";
    const removeTrailingSpaces =
      cookies.get("_removeTrailingSpaces") === undefined
        ? false
        : cookies.get("_removeTrailingSpaces") === "true";

    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        addFilenameAsAttribute,
        removePreviousAttribute,
        removeTrailingSpaces
      }
    });
  };
  onRemovePreviousAttributeChange = e => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      options: {
        ...this.state.options,
        removePreviousAttribute: !this.state.options.removePreviousAttribute
      }
    });
    cookies.set(
      "_removePreviousAttribute",
      !this.state.options.removePreviousAttribute,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  onAddFilenameAttributeChange = e => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      options: {
        ...this.state.options,
        addFilenameAsAttribute: !this.state.options.addFilenameAsAttribute
      }
    });
    cookies.set(
      "_addFilenameAsAttribute",
      !this.state.options.addFilenameAsAttribute,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  onRemoveTrailingSpacesChange = e => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      options: {
        ...this.state.options,
        removeTrailingSpaces: !this.state.options.removeTrailingSpaces
      }
    });
    cookies.set(
      "_removeTrailingSpaces",
      !this.state.options.removeTrailingSpaces,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  onInsertChecksumChange = e => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      options: {
        ...this.state.options,
        insertChecksum: !this.state.options.insertChecksum
      }
    });
  }

  onMoveAttributesToTuChange = e => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      options: {
        ...this.state.options,
        moveAttributesToTu: !this.state.options.moveAttributesToTu
      }
    });
  }

  onOutputFileToFTPOptionChange = e => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      options: {
        ...this.state.options,
        outputFileToFTP: !this.state.options.outputFileToFTP
      }
    });
  };

  addNewAttributeRow = () => {
    const newAttribute = {
      key: "",
      value: "",
      hide: false
    };
    let { newAttributes } = this.state.options;
    newAttributes.push(newAttribute);
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      options: {
        ...this.state.options,
        newAttributes
      }
    });
  };

  hideNewAttribute = async e => {
    const idx = e.target.id;
    let { newAttributes } = this.state.options;
    newAttributes = newAttributes.map((newAttribute, i) => {
      if (i == idx) {
        newAttribute["hide"] = true;
      }
      return newAttribute;
    });
    const filter = await import("lodash/filter");
    newAttributes = filter.default(newAttributes, { hide: false });
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      options: {
        ...this.state.options,
        newAttributes
      }
    });
  };

  onAttributeFieldsChange = e => {
    const { name, value } = e.target;
    const targetName = name.split("_")[0];
    const idx = parseInt(name.split("_")[1], 10);
    let { newAttributes } = this.state.options;
    newAttributes = newAttributes.map((newAttribute, i) => {
      if (i == idx) {
        newAttribute[targetName] = value;
      }
      return newAttribute;
    });
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      options: {
        ...this.state.options,
        newAttributes
      }
    });
  };

  onDragEnter = () => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      dropzoneActive: true
    });
  };

  onDragLeave = () => {
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      dropzoneActive: false
    });
  };

  onDrop = (acceptedFiles, rejectedFiles, e) => {
    let { files } = this.state;
    acceptedFiles.forEach(acceptedFile => {
      const fileName = acceptedFile.name;
      if (
        fileName.endsWith(".tmx") ||
        fileName.endsWith(".TMX") ||
        fileName.endsWith(".zip") ||
        fileName.endsWith(".ZIP")
      ) {
        files.push(acceptedFile);
      }
    });
    this.setState({
      ...this.state,
      files,
      success: false,
      uploadSuccess: false,
      errors: {},
      unzipFiles: {},
      dropzoneActive: false
    });
  };

  removeUploadedFiles = async e => {
    let { files } = this.state;
    const filter = await import("lodash/filter");
    files = filter.default(files, ({ name }) => name !== e.target.name);
    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      files
    });
  };

  uploadFiles = async e => {
    e.preventDefault();
    const forEach = await import("lodash/forEach");

    this.setState({
      ...this.state,
      success: false,
      uploadSuccess: false,
      uploadLoading: true
    });
    let { files, unzipFiles } = this.state;
    // generate a random token to resolve concurrency issue
    let { options } = this.state;
    let { randomToken } = this.state.options;
    if (randomToken.length === 0) {
      const crypto = await import("crypto");
      randomToken = crypto
        .randomBytes(Math.ceil(5 / 2))
        .toString("hex")
        .slice(0, 5);
    }

    let promises = [];
    forEach.default(files, file => {
      if (!file.isUploaded || !file.uploadSuccess) {
        const promise = this.props.uploadTmxFileForManipulation(
          randomToken,
          file
        );
        promises.push(promise);
      }
    });
    Promise.all(promises)
      .then(results => {
        const notificationType = "Manipulation Upload Status:";
        const notificationContent =
          "Tmx Manipulation Files are uploaded successful!";
        sendBrowserNotification(notificationType, notificationContent);

        results.forEach(res => {
          const responseJSON = JSON.parse(res.data.text);
          if (Array.isArray(responseJSON)) {
            let zipFilename = "";
            let subList = [];
            responseJSON.forEach(responseSubJSON => {
              zipFilename = responseSubJSON.originalZipFileName;
              subList.push(responseSubJSON);
            });
            unzipFiles[zipFilename] = subList;
            // mark zip file in files as zipFile and marked it as upload success.
            files = files.map(file => {
              if (zipFilename === file.name) {
                file.uploadSuccess = true;
                file.isUploaded = true;
                file.isZipFile = true;
              }
              return file;
            });
          } else {
            const { filename, status } = responseJSON;
            files = files.map(file => {
              if (filename === file.name) {
                file.uploadSuccess = status;
                file.isUploaded = true;
              }
              return file;
            });
          }
        });
        this.setState({
          ...this.state,
          options,
          files,
          success: false,
          errors: {},
          uploadLoading: false,
          uploadSuccess: true
        });
      })
      .catch(err => {
        this.setState({
          ...this.state,
          options,
          success: false,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : null
          },
          uploadLoading: false,
          uploadSuccess: false
        });
      });
  };

  handleResetButton = e => {
    e.preventDefault();
    this.setState({
      options: {
        newAttributes: [
          {
            key: "",
            value: "",
            hide: false
          }
        ],
        addFilenameAsAttribute: false,
        removePreviousAttribute: false,
        removeTrailingSpaces: false,
        insertChecksum: false,
        moveAttributesToTu: false,
        randomToken: ""
      },
      dropzoneActive: false,
      files: [],
      unzipFiles: {}, // used to display tmx files unzip from zip file
      uploadLoading: false,
      uploadSuccess: false,
      loading: false,
      success: false,
      outputFilename: "",
      errors: {}
    });
    cookies.set("_addFilenameAsAttribute", false, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime,
      samesite: "strict"
    });
    cookies.set("_addFilenameAsAttribute", false, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime,
      samesite: "strict"
    });
    cookies.set("_removeTrailingSpaces", false, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime,
      samesite: "strict"
    });
  };

  validate = files => {
    let errors = {};
    // No file is selected for upload
    if (Object.keys(files).length === 0) {
      errors.files = "No file is selected for upload.";
    }
    files.forEach(file => {
      if (
        !file.hasOwnProperty("uploadSuccess") &&
        !file.hasOwnProperty("isUploaded")
      ) {
        errors.files = "Please upload files before clicking Manipulate Button";
      }
    });
    return errors;
  };

  onSubmit = e => {
    e.preventDefault();
    const { files, options } = this.state;
    let errors = this.validate(files);
    this.setState({ ...this.state, errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ ...this.state, loading: true });
      this.props
        .manipulateTmxFiles(options)
        .then(res => {
          const notificationType = "Tmx Manipulation Update";
          const notificationContent = `Tmx Manipulation is success. Please check the downloaded zip file.`;
          sendBrowserNotification(notificationType, notificationContent);

          const filename = res.data.headers.filename;
          const data = res.data.data;
          saveByteToFile(filename, data);
          // something went wrong with tmx files, display error message to user:
          if (res.data.headers.hasOwnProperty("errormessages")) {
            errors.manipulate = [];
            const errorMessages = JSON.parse(res.data.headers.errormessages);
            errorMessages.forEach(errorMessage => {
              errors.manipulate.push(errorMessage);
            });
            this.setState({
              ...this.state,
              loading: false,
              uploadSuccess: false,
              errors,
              files: [],
              success: true,
              outputFilename: filename
            });
          } else {
            this.setState({
              ...this.state,
              loading: false,
              uploadSuccess: false,
              errors: {},
              files: [],
              success: true,
              outputFilename: filename
            });
          }
        })
        .catch(err => {
          const reader = new FileReader();
          var myBlob = new Blob([err.response.data], { type: "text/plain" });
          reader.addEventListener("loadend", e => {
            const text = e.srcElement.result;
            const message = JSON.parse(text);
            this.setState({
              ...this.state,
              loading: false,
              uploadSuccess: false,
              success: false,
              errors: { global: message.global }
            });
          });
          reader.readAsText(myBlob);
        });
    }
  };

  render() {
    const {
      dropzoneActive,
      options,
      files,
      unzipFiles,
      success,
      uploadLoading,
      loading,
      uploadSuccess,
      outputFilename,
      errors
    } = this.state;

    if (Object.keys(errors).length > 0) {
      window.scrollTo(0, 0);
    }

    const dropzoneStyle = {
      margin: "auto",
      width: "100%",
      height: "200px",
      borderWidth: "2px",
      borderColor: "rgb(102,102,102)",
      borderStyle: "dashed",
      borderRadius: "5px"
    };
    const dropStyle = {
      margin: "auto",
      width: "100%",
      height: "200px",
      borderWidth: "2px",
      borderColor: "rgb(102,204,102)",
      borderStyle: "solid",
      borderRadius: "5px",
      backgroundColor: "rgb(238,238,238)"
    };

    return (
      <div style={{ marginLeft: "20px", marginRight: "20px" }}>
        <h4>Manipulate Tmx Files</h4>
        <Dimmer inverted active={uploadLoading}>
          <Loader indeterminate>Uploading files, please wait...</Loader>
        </Dimmer>
        <Dimmer inverted active={loading}>
          <Loader indeterminate>Manipulating Tmx files, please wait...</Loader>
        </Dimmer>
        {uploadSuccess && (
          <Message positive>
            <Message.Header>Manipulate Tmx Files Update:</Message.Header>
            <p>Upload is successful!</p>
          </Message>
        )}
        {success && (
          <Message positive>
            <Message.Header>Manipulate Tmx Files Update:</Message.Header>
            <p>
              Manipulation is successful! Please check the downloaded file{" "}
              {outputFilename} in the browser.
            </p>
          </Message>
        )}
        {errors && errors.files && (
          <Message negative>
            <Message.Header>Manipulate Tmx Input Error:</Message.Header>
            <p>{errors.files}</p>
          </Message>
        )}
        {errors && errors.global && (
          <Message negative>
            <Message.Header>Manipulate Tmx Error:</Message.Header>
            <p>{errors.global}</p>
          </Message>
        )}
        {!!errors.manipulate && (
          <Message warning>
            <Message.Header>Manipulate Tmx file error:</Message.Header>
            <ul>
              {map(errors.manipulate, subObj => (
                <li key={subObj.fileName}>
                  <p>
                    File Name: {subObj.fileName}, Error Message:{" "}
                    {subObj.errorMessage}
                  </p>
                </li>
              ))}
            </ul>
          </Message>
        )}
        <Grid columns={2}>
          <Grid.Row>
            <Grid.Column>
              <Dropzone
                style={dropzoneActive ? dropStyle : dropzoneStyle}
                onDragLeave={this.onDragLeave.bind(this)}
                onDragEnter={this.onDragEnter.bind(this)}
                onDrop={this.onDrop.bind(this)}
                multiple={true}
              >
                <div style={{ textAlign: "center", marginTop: "50px" }}>
                  <h4>Drop/Select tmx files to upload</h4>
                </div>
              </Dropzone>
              <br />
              {files.length > 0 && (
                <Button
                  fluid
                  icon
                  labelPosition="left"
                  color="teal"
                  onClick={this.uploadFiles.bind(this)}
                >
                  <Icon name="cloud upload" />
                  Upload
                </Button>
              )}
            </Grid.Column>
            <Grid.Column>
              <h4>Selected tmx/zip files:</h4>
              <ul>
                {map(files, file => (
                  <li key={file.lastModified}>
                    {!file.isUploaded && (
                      <div>
                        {file.name}&nbsp;&nbsp;&nbsp;
                        <Button
                          icon
                          size="mini"
                          name={file.name}
                          onClick={this.removeUploadedFiles.bind(this)}
                        >
                          <Icon name="window close" />
                        </Button>
                      </div>
                    )}
                    {file.uploadSuccess && file.isUploaded && !file.isZipFile && (
                      <div>
                        {file.name}&nbsp;&nbsp;&nbsp;is uploaded
                        successfully!&nbsp;&nbsp;
                        <Icon name="checkmark" color="green" />
                      </div>
                    )}
                    {file.uploadSuccess &&
                      file.isUploaded &&
                      file.isZipFile && <div>{file.name}:</div>}
                    {file.uploadSuccess &&
                      file.isUploaded &&
                      file.isZipFile &&
                      map(unzipFiles[file.name], subFile => (
                        <div key={subFile.filename}>
                          {subFile.status && (
                            <div>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              {subFile.filename}&nbsp;&nbsp;&nbsp;is uploaded
                              successfully!&nbsp;&nbsp;
                              <Icon name="checkmark" color="green" />
                            </div>
                          )}
                        </div>
                      ))}
                  </li>
                ))}
              </ul>
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <h4>New Attributes:</h4>
        <Grid columns={2} celled>
          {map(options.newAttributes, ({ key, value, hide }, idx) => (
            <Grid.Row key={idx}>
              <Grid.Column>
                <Form.Field required>
                  <label>Key:</label>
                  <Form.Input
                    type="text"
                    name={`key_${idx}`}
                    onChange={this.onAttributeFieldsChange}
                    value={key}
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field required>
                  <label>Value:</label>
                  <Form.Input
                    type="text"
                    name={`value_${idx}`}
                    onChange={this.onAttributeFieldsChange}
                    value={value}
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Button color="red" id={idx} onClick={this.hideNewAttribute}>
                  <Icon name="delete" />
                  Delete
                </Button>
              </Grid.Column>
            </Grid.Row>
          ))}
          <Grid.Row>
            <Grid.Column>
              <Button color="green" onClick={this.addNewAttributeRow}>
                <Icon name="add" />
                Add additional attribute pair
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <h4>Manipulate Tmx Options:</h4>
        <br />
        <Grid columns={5}>
          <Grid.Row>
            <Grid.Column>
              <Checkbox
                label="Remove previous attributes"
                onChange={this.onRemovePreviousAttributeChange}
                checked={options.removePreviousAttribute}
              />
            </Grid.Column>
            <Grid.Column>
              <Checkbox
                label="Add tmx Filename as attribute"
                onChange={this.onAddFilenameAttributeChange}
                checked={options.addFilenameAsAttribute}
              />
            </Grid.Column>
            <Grid.Column>
              <Checkbox
                label="Remove Trailing Spaces from segment"
                onChange={this.onRemoveTrailingSpacesChange}
                checked={options.removeTrailingSpaces}
              />
            </Grid.Column>
            <Grid.Column>
              <Checkbox
                label="Insert Checksum"
                onChange={this.onInsertChecksumChange}
                checked={options.insertChecksum}
              />
            </Grid.Column>
            <Grid.Column>
              <Checkbox
                label="Move Props from Tuv to Tu"
                onChange={this.onMoveAttributesToTuChange}
                checked={options.moveAttributesToTu}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <br />
        <br />
        <Button
          floated="right"
          primary
          size="large"
          disabled={loading}
          onClick={this.onSubmit.bind(this)}
        >
          Manipulate
        </Button>
        <Button
          floated="left"
          color="olive"
          size="large"
          onClick={this.handleResetButton.bind(this)}
        >
          Reset
        </Button>
        <br />
        <br />
      </div>
    );
  }
}

ManipulateTmxForm.propTypes = {
  uploadTmxFileForManipulation: PropTypes.func.isRequired,
  manipulateTmxFiles: PropTypes.func.isRequired
};

export default connect(
  null,
  {
    uploadTmxFileForManipulation,
    manipulateTmxFiles
  }
)(ManipulateTmxForm);
