import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import {
  Button,
  Checkbox,
  Confirm,
  Dimmer,
  Dropdown,
  Form,
  Grid,
  Icon,
  Loader,
  Message,
  Popup
} from "../../utils/reactSemanticUI";
import Select from "react-select";
import escapeRegExp from "lodash/escapeRegExp";
import Cookies from "universal-cookie";

import InlineError from "../messages/InlineError";
import {
  fetchOrganizationList,
  fetchTmGroupList,
  fetchLanguageList,
  fetchGenericUsername,
  fetchGenericUserpassword,
  fetchCleanupPasscode,
  fetchGenericUserRole,
  validateCleanupPasscode,
  fetchTmGroupCode,
  validateTmGroupShortCode,
  createTmGroup,
  createTm,
  fetchImportTmList,
  checkDuplicateTm
} from "../../actions/tm";
import { sendBrowserNotification } from "../../utils/main";
import TmGroup from "./subComponents/TmGroup";
import CreateTmGroupForm from "./CreateTmGroupForm";
import CreateTmPdCsvOptions from './subComponents/CreateTmPdCsvOptions';
import userManager from "../../utils/userManager";
import { oauthLogin } from '../../actions/auth';
import decode from 'jwt-decode';

const cookies = new Cookies();
// cookies expires in one year
const cookieExpiredTime = new Date(
  new Date().getTime() + 60 * 60 * 1000 * 24 * 365
);

const fetchTmInfoGeneralError = `Failed to fetch Tm Groups/Language from TM Server`;
const fetchGroupsError = `Failed to fetch Tm Groups/Langauge from TM Server,
  Please check the TM Server role setting and enable WS - Get TM List in System policy TM tab`;
class CreateTmForm extends Component {
  state = {
    initLoading: false,
    refreshLoading: false,
    refreshImportTmListLoading: false,
    tokenExpiredLoading: false,
    loading: false,
    createTmLoading: false,
    autoFillTmInfoLoading: false,
    organizationList: [],
    tmGroupList: [],
    languageList: [],
    limitedLanguageList: [],
    userRoleList: [],
    searchTmGroupQuery: "",
    options: {
      selectedOrganization: {},
      selectedTmGroup: {},
      invertedLanguage: false,
      sourceLanguage: "en-US",
      targetLanguages: [],
      tmName: "",
      appendLanguagesToTmName: false,
      genericUsername: "",
      user: {
        role: { label: "read/write", value: "read/write" },
        password: ""
      },
      cleanupCode: "",
      allGenericUserpasswordSame: true,
      allTmCleanupPasscodeSame: true,
      enabled: true,
      contextModeEnabled: true,
      pdCsvCreationEnabled: true,
      cleanupUsername: "",
      paClientName: "",
      addFileName: false,
      useGenericUsernameAsProfileName: false,
      emptyCleanupUserInfo: false
    },
    groupOptions: {
      tmGroupName: "",
      tmGroupShortcode: "",
      selectedOrganization: ""
    },
    errors: {},
    groupErrors: {},
    success: false,
    numOfCreatedTm: 0,
    failureTmCreationResponses: [],
    isCreateTmGroupModalVisible: false,
    tmGroupNameCreated: "",
    tmGroupShortCodeCreateLoading: false,
    tmGroupLoading: false,
    tmGroupCreationSuccess: false,
    unauthorized: false,
    twoLetterCodeConfirmation: false,
    openTwoLetterCodeConfirmation: false,
    duplicateTmInSameGroupConfirm: false,
    openCheckDuplicateTmInSameGroupConfirmation: false,
    duplicateTmLocalesPairText: "",
    duplicateTmCheckLoading: false
  };

  componentDidMount = async () => {
    // Check OAuth user credential and signin siliently
    const { organizationListData, tmGroupListData,
      languageListData, genericUserRoleData, oauthUser } = this.props;

    let options = this.loadCreateTmOptionsFromCookies(this.state.options);
    this.oauthInterval = setInterval(this.oauthSilentRefresh, 30 * 1000);
    this.orgTmGroupInterval = setInterval(this.tmOrganizationGroupListPeriodReresh, 9 * 58 * 1000)

    const currentDate = new Date().getTime() / 1000;
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      await this.oauthSilentRefresh();
    }

    let serverError = false;
    if (!tmGroupListData || tmGroupListData.exp <= currentDate) {
      let info = {
        refreshTmList: true,
        refreshOrganizationList: true
      };
      if (oauthUser && !oauthUser.expired) {
        info.access_token = oauthUser.access_token;
      }
      try {
        this.setState({ ...this.state, initLoading: true });
        let promises = [];
        const organizationListPromise = this.props.fetchOrganizationList(info);
        promises.push(organizationListPromise);
        const tmGroupListPromise = this.props.fetchTmGroupList(info);
        promises.push(tmGroupListPromise);
        await Promise.all(promises).then(res => {
          this.setState({
            options,
            organizationList: res[0].data,
            tmGroupList: res[1].data,
          });
        })
      }
      catch (err) {
        serverError = true;
        const { response } = err;
        // request is unauthorized: most likely the token is expired, direct user to login page:
        if (!!response && response.hasOwnProperty("status") && response.status === 401) {
          localStorage.clear();
          await this.setState({
            ...this.state,
            unauthorized: true,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
            }
          });
        }
        else if (!!response && response.hasOwnProperty("status") &&
          (response.status === 400 || response.status === 500)) {
          await this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: fetchGroupsError
            }
          });
        }
        else {
          await this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
            }
          });
        }
      }
    }
    else {
      // We need to add await keyword in order to have organizationList and tmGroupList
      // loaded as state data.
      await this.setState({
        ...this.state,
        options,
        organizationList: organizationListData.organizationList,
        tmGroupList: tmGroupListData.tmGroupList
      });
    }

    if (serverError) {
      this.setState({
        ...this.state,
        initLoading: false,
        success: false
      });
      return;
    }

    if (!languageListData) {
      try {
        this.setState({ ...this.state, initLoading: true });
        let promises = [];
        let data = {};
        if (oauthUser && !oauthUser.expired) {
          data.access_token = oauthUser.access_token;
        }
        const languageListPromise = this.props.fetchLanguageList(data);
        promises.push(languageListPromise);

        const fetchGenericUserRole = this.props.fetchGenericUserRole(data);
        promises.push(fetchGenericUserRole);
        await Promise.all(promises).then(res => {
          this.setState({
            ...this.state,
            options,
            languageList: res[0].data,
            userRoleList: res[1].data
          });
        });
      }
      catch (err) {
        const { response } = err;
        // request is unauthorized: most likely the token is expired, direct user to login page:
        if (!!response && response.hasOwnProperty("status") && response.status === 401) {
          localStorage.clear();
          this.setState({
            ...this.state,
            unauthorized: true,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
            }
          });
        }
        else if (!!response && response.hasOwnProperty("status")
          && (response.status === 400 || response.status === 500)) {
          this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: fetchGroupsError
            }
          });
        }
        else {
          this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
            }
          });
        }
      }
    }
    else {
      // We need to add await keyword in order to have languageList and userRoleList
      // loaded as state data.
      await this.setState({
        ...this.state,
        languageList: languageListData.languageList,
        userRoleList: genericUserRoleData.genericUserRole
      });
    }
    this.setState({
      ...this.state,
      initLoading: false,
      success: false
    });
  }

  loadCreateTmOptionsFromCookies = (options) => {
    // Preload cookies values:
    if (!cookies.get("_appendLanguagesToTmName")) {
      options.appendLanguagesToTmName = false;
    }
    else {
      options.appendLanguagesToTmName = cookies.get("_appendLanguagesToTmName") === "true";
    }
    if (!cookies.get("_allGenericUserpasswordSame")) {
      options.allGenericUserpasswordSame = true;
    }
    else {
      options.allGenericUserpasswordSame = cookies.get("_allGenericUserpasswordSame") === "true";
    }
    if (!cookies.get("_allTmCleanupPasscodeSame")) {
      options.allTmCleanupPasscodeSame = true;
    }
    else {
      options.allTmCleanupPasscodeSame = cookies.get("_allTmCleanupPasscodeSame") === "true";
    }
    if (!cookies.get("_enabledTmAfterCreation")) {
      options.enabled = true;
    }
    else {
      options.enabled = cookies.get("_enabledTmAfterCreation") === "true";
    }
    if (!cookies.get("_contextModeEnabled")) {
      options.contextModeEnabled = true;
    }
    else {
      options.contextModeEnabled = cookies.get("_contextModeEnabled") === "true";
    }
    if (!cookies.get("_addFileName")) {
      options.addFileName = true;
    }
    else {
      options.addFileName = cookies.get("_addFileName") === "true";
    }
    if (!cookies.get("_useGenericUsernameAsProfileName")) {
      options.useGenericUsernameAsProfileName = true;
    }
    else {
      options.useGenericUsernameAsProfileName = cookies.get("_useGenericUsernameAsProfileName") === "true";
    }
    if (!cookies.get("_emptyCleanupUserInfo")) {
      options.emptyCleanupUserInfo = true;
    }
    else {
      options.emptyCleanupUserInfo = cookies.get("_emptyCleanupUserInfo") === "true";
    }
    return options;
  }

  componentWillUnmount = () => {
    clearInterval(this.oauthInterval);
    clearInterval(this.orgTmGroupInterval);
  }

  tmOrganizationGroupListPeriodReresh = () => {
    const { organizationListData, tmGroupListData } = this.props;
    const currentDate = new Date().getTime() / 1000;
    if (!!tmGroupListData && tmGroupListData.exp > currentDate &&
      !!organizationListData && organizationListData.exp > currentDate) {
      return;
    }
    this.refreshTmOrganizationAndGroupList();
  }

  oauthSilentRefresh = async () => {
    const currentDate = new Date().getTime() / 1000;
    const { oauthUser } = this.props;
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      this.setState({ ...this.state, tokenExpiredLoading: true });
      try {
        await userManager.signinSilent();
        const { access_token } = oauthUser;
        const payload = decode(access_token);
        let credentials = {};
        credentials.email = payload.sub;
        credentials.creation = payload.nbf;
        credentials.expiration = payload.exp;
        credentials.accessToken = access_token;
        credentials.idToken = oauthUser.id_token;
        await this.props.oauthLogin(credentials).catch(err => {
          console.log(err);
        })
      }
      catch (err) {
        localStorage.oauthLastLoc = window.location.pathname;
        await userManager.signinRedirect();
        console.log(err);
      }
      finally {
        this.setState({ ...this.state, tokenExpiredLoading: false });
      }
    }
  }

  handleSelectionOptionChange = (_, { name, value }) => {
    let { errors } = this.state;
    delete errors["targetLanguage"];

    this.setState({
      ...this.state,
      success: false,
      errors,
      options: {
        ...this.state.options,
        [name]: value
      }
    });
  };

  clearAllSelectedTargetLanguages = (event) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        targetLanguages: []
      }
    });
  };

  handleOrganizationOptionChange = (selectedOption) => {
    let { groupErrors } = this.state;
    if (groupErrors.hasOwnProperty("organization")) {
      delete groupErrors.organization;
    }
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        selectedOrganization: selectedOption
      },
      groupOptions: {
        ...this.state.groupOptions,
        selectedOrganization: selectedOption
      }
    });
  };

  handleTmGroupOptionChange = async (selectedOption) => {
    // auto populate some fields
    let { options, errors } = this.state;
    // remove empty TM Group and TM name error:
    delete errors["tmGroup"];
    delete errors["tmName"];
    // remove generic username/password and cleanup passcode errors
    delete errors["genericUsername"];
    delete errors["genericUserpassword"];
    delete errors["cleanupCode"];

    options.selectedTmGroup = selectedOption;
    // ignore options updates if user delete the group selection.
    if (Object.keys(selectedOption).length == 0) {
      this.setState({
        ...this.state,
        success: false,
        options,
        errors
      });
      return;
    }
    await this.setState({
      ...this.state,
      autoFillTmInfoLoading: true
    });
    // create tmName basedon TM group name
    options.tmName = selectedOption.label;
    // On 3-12-19, Petr decided to use full TM Group name as generic username:
    // ticket: https://techqa1.translations.com/jira/projects/TMSTOOL/issues/TMSTOOL-47
    // TMSTOOL-49: remove all non-alphanumeric characters
    // Only allow A-Z a-z and 0-9 in generic username
    options.genericUsername = selectedOption.label.replace(/[^0-9A-Za-z]/gi, "");
    // // create generic username based on TM group.
    // const { sourceLanguage, targetLanguages } = options;

    // const newLable = selectedOption.label.replace(/[^0-9A-Za-z]/gi, '');
    // const genericUsernameParams = { tmName: newLable,
    //   sourceLanguageCode: sourceLanguage,
    //   targetLanguageCode: targetLanguages.length > 0 ? targetLanguages[0] : ''
    // }
    // const promise = await this.props.fetchGenericUsername(genericUsernameParams);
    // options.genericUsername = promise.data;

    // if (options.user.password === "" || options.cleanupCode === "") {
    //   // Check OAuth user credential and refresh silently if it is expired.
    //   const { oauthUser } = this.props;
    //   if (oauthUser && oauthUser.expired) {
    //     await userManager.signinSilent();
    //   }
    // }
    let data = {};
    const { oauthUser } = this.props;
    if (oauthUser && !oauthUser.expired) {
      data.access_token = oauthUser.access_token;
    }
    if (options.user.password === "") {
      const promise = await this.props.fetchGenericUserpassword(data);
      options.user.password = promise.data.password;
    }
    if (options.cleanupCode === "") {
      const promise = await this.props.fetchCleanupPasscode(data);
      options.cleanupCode = promise.data.code;
    }
    // Use TM Group Name as PA Client Name by default
    // https://techqa1.translations.com/jira/browse/TMSTOOL-103
    if (options.pdCsvCreationEnabled) {
      options.paClientName = selectedOption.label;
    }
    this.setState({
      ...this.state,
      success: false,
      numOfCreatedTm: 0,
      failureTmCreationResponses: [],
      autoFillTmInfoLoading: false,
      options,
      errors
    });
  };

  handleUserRoleOptionChange = (selectedOption) => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        user: {
          ...this.state.options.user,
          role: selectedOption
        }
      }
    });
  };

  refreshTmOrganizationAndGroupList = async () => {
    this.setState({ ...this.state, refreshLoading: true });
    const { oauthUser } = this.props;
    const info = {
      refreshTmList: true,
      refreshOrganizationList: true
    };
    if (oauthUser && !oauthUser.expired) {
      info.access_token = oauthUser.access_token;
    }
    try {
      let promises = [];
      const organizationListPromise = this.props.fetchOrganizationList(info);
      promises.push(organizationListPromise);

      const tmGroupListPromise = this.props.fetchTmGroupList(info);
      promises.push(tmGroupListPromise);

      await Promise.all(promises).then(async (res) => {
        await this.setState({
          ...this.state,
          organizationList: res[0].data,
          tmGroupList: res[1].data
        });
      }).catch(err => {
        this.setState({
          ...this.state,
          refreshLoading: false,
          success: false,
          errors: {
            ...this.state.errors,
            tmGroup: err.response.data.global
          }
        });
      });
    }
    catch (err) {
      const { response } = err;
      // request is unauthorized: most likely the token is expired, direct user to login page:
      if (!!response && response.hasOwnProperty("status") && response.status === 401) {
        localStorage.clear();
        await this.setState({
          ...this.state,
          unauthorized: true,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
          }
        });
      }
      else if (!!response && response.hasOwnProperty("status") &&
        (response.status === 400 || response.status === 500)) {
        this.setState({
          ...this.state,
          errors: {
            ...this.state.errors,
            global: fetchGroupsError
          }
        });
      }
      else {
        await this.setState({
          ...this.state,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
          }
        });
      }
    }
    finally {
      this.setState({
        ...this.state,
        refreshLoading: false,
        success: false
      });
    }

  }

  handleInvertedLanguage = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        invertedLanguage: !this.state.options.invertedLanguage
      }
    });
  };

  handleAppendLanguageToTmName = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        appendLanguagesToTmName: !this.state.options.appendLanguagesToTmName
      }
    });
    cookies.set(
      "_appendLanguagesToTmName",
      !this.state.options.appendLanguagesToTmName,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  handleAllUserpasswordsTheSameOption = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        allGenericUserpasswordSame: !this.state.options
          .allGenericUserpasswordSame
      }
    });
    cookies.set(
      "_allGenericUserpasswordSame",
      !this.state.options.allGenericUserpasswordSame,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  handleAllTmCleanupPasscodesTheSameOption = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        allTmCleanupPasscodeSame: !this.state.options.allTmCleanupPasscodeSame
      }
    });
    cookies.set(
      "_allTmCleanupPasscodeSame",
      !this.state.options.allTmCleanupPasscodeSame,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  onChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    if (name === "tmName" && value.length > 0) {
      // change to use Tm name as parameter to be the generic username
      // 9-20-18: remove any whitespace from generate username
      // TMSTOOL-49: remove all non-alphanumeric characters
      // Only allow A-Z a-z and 0-9 in generic username
      let genericUserNameWithoutSpaces = value.replace(/\s/g, "").replace(/[^0-9a-z]/gi, "");
      this.setState({
        ...this.state,
        success: false,
        numOfCreatedTm: 0,
        failureTmCreationResponses: [],
        options: {
          ...this.state.options,
          [event.target.name]: value,
          genericUsername: genericUserNameWithoutSpaces
        }
      });
    } else if (name === "genericUsername" && value.length > 0) {
      // TMSTOOL-49: remove all non-alphanumeric characters
      // Only allow A-Z a-z and 0-9 in generic username
      let genericUserNameWithoutSpaces = value.replace(/\s/g, "").replace(/[^0-9a-z]/gi, "");
      this.setState({
        ...this.state,
        success: false,
        numOfCreatedTm: 0,
        failureTmCreationResponses: [],
        options: {
          ...this.state.options,
          [event.target.name]: genericUserNameWithoutSpaces
        }
      });
    } else {
      // TMSTOOL-32 remove all leading and trailing spaces:
      this.setState({
        ...this.state,
        success: false,
        numOfCreatedTm: 0,
        failureTmCreationResponses: [],
        options: {
          ...this.state.options,
          [event.target.name]: value
        }
      });
    }
  };

  handlePdCsvFieldsChange = (event) => {
    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        [event.target.name]: event.target.value
      }
    });
  };

  onAddFileNameChange = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        addFileName: !this.state.options.addFileName
      }
    });
    cookies.set("_addFileName", !this.state.options.addFileName, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime,
      sameSite: "strict"
    });
  };

  onProfileNameChange = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        useGenericUsernameAsProfileName: !this.state.options
          .useGenericUsernameAsProfileName
      }
    });
    cookies.set(
      "_useGenericUsernameAsProfileName",
      !this.state.options.useGenericUsernameAsProfileName,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  onEmptyCleanupUserInfo = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        emptyCleanupUserInfo: !this.state.options.emptyCleanupUserInfo
      }
    });
    cookies.set(
      "_emptyCleanupUserInfo",
      !this.state.options.emptyCleanupUserInfo,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  fetchGenericUserpassword = async (event) => {
    event.preventDefault();
    this.setState({ ...this.state, loading: true });
    const { oauthUser } = this.props;
    let data = {};
    if (oauthUser && !oauthUser.expired) {
      data.access_token = oauthUser.access_token;
    }
    this.props.fetchGenericUserpassword(data).then(res =>
      this.setState({
        ...this.state,
        loading: false,
        success: false,
        options: {
          ...this.state.options,
          user: { ...this.state.options.user, password: res.data }
        }
      })
    ).catch(err => {
      this.setState({
        ...this.state,
        loading: false,
        success: false,
        errors: {
          ...this.state.errors,
          genericUserpassword: err.response.data.global
        }
      });
    });
  };

  fetchCleanupPasscode = async () => {
    this.setState({ ...this.state, loading: true });
    let data = {};
    const { oauthUser } = this.props;
    if (oauthUser && !oauthUser.expired) {
      data.access_token = oauthUser.access_token;
    }
    this.props.fetchCleanupPasscode(data).then(res =>
      this.setState({
        ...this.state,
        loading: false,
        success: false,
        options: { ...this.state.options, cleanupCode: res.data }
      })
    ).catch(err => {
      this.setState({
        ...this.state,
        loading: false,
        success: false,
        errors: {
          ...this.state.errors,
          cleanupCode: err.response.data.global
        }
      });
    });
  };

  onGenericUserPasswordChange = (event) => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        user: {
          ...this.state.options.user,
          password: event.target.value
        }
      }
    });
  };

  handleTmEnabledOption = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        enabled: !this.state.options.enabled
      }
    });
    cookies.set("_enabledTmAfterCreation", !this.state.options.enabled, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime,
      sameSite: "strict"
    });
  };

  handleTmContextModeEnabledOption = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        contextModeEnabled: !this.state.options.contextModeEnabled
      }
    });
    cookies.set("_contextModeEnabled", !this.state.options.contextModeEnabled, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime,
      sameSite: "strict"
    });
  };

  handlePDCSVCreationEnabledOption = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        pdCsvCreationEnabled: !this.state.options.pdCsvCreationEnabled
      }
    });
  };

  handleCreateTmGroupModalVisible = () => {
    this.setState({
      ...this.state,
      success: false,
      isCreateTmGroupModalVisible: !this.state.isCreateTmGroupModalVisible
    });
  }

  handleTmGroupFields = (event) => {
    let { groupErrors, groupOptions } = this.state;
    let value = event.target.value;
    if (groupOptions.selectedOrganization === "") {
      groupErrors.organization = "Please select an organization";
    } else {
      delete groupErrors.organization;
    }

    if (event.target.name === 'tmGroupShortcode') {
      const tmGroupShortCode = value;
      if (tmGroupShortCode.length > 10) {
        groupErrors.tmGroupShortcode =
          "Tm Group Shortcode cannot exceed 10 characters";
        value = value.substring(0, 10);
      } else {
        delete groupErrors.tmGroupShortcode;
      }
    }
    else if (event.target.name === 'tmGroupName') {
      delete groupErrors.tmGroupName;
    }
    // We don't perform duplicate checking at this time.
    this.setState({
      ...this.state,
      success: false,
      tmGroupNameCreated: "",
      tmGroupCreationSuccess: false,
      groupErrors,
      groupOptions: {
        ...this.state.groupOptions,
        [event.target.name]: value
      }
    });
  };

  onTmGroupSubmit = async (event) => {
    event.preventDefault();
    let { groupOptions, tmGroupList } = this.state;
    const groupErrors = this.tmGroupValidate(groupOptions, tmGroupList);

    if (Object.keys(groupErrors).length !== 0) {
      this.setState({
        ...this.state,
        groupOptions: {
          ...this.state.groupOptions,
          tmGroupShortcode: ""
        },
        groupErrors
      });
      return;
    }
    this.setState({
      ...this.state,
      groupErrors: {},
      tmGroupLoading: true,
      tmGroupCreationSuccess: false
    });

    const { oauthUser } = this.props;
    let data = {};
    let info = {
      refreshTmList: true,
      refreshOrganizationList: true
    };
    if (oauthUser && !oauthUser.expired) {
      data.accessToken = oauthUser.access_token;
      groupOptions.accessToken = oauthUser.access_token;
      info.access_token = oauthUser.access_token;
    }
    try {
      // validate tm Group Short Code via API
      data.groupCode = groupOptions.tmGroupShortcode
      const res = await this.props.validateTmGroupShortCode(data);
      // Remove leading and trailing spaces:
      groupOptions.tmGroupName = groupOptions.tmGroupName.trim();
      await this.props.createTmGroup(groupOptions);

      let promises = [];
      const organizationListPromise = this.props.fetchOrganizationList(info);
      promises.push(organizationListPromise);

      const tmGroupListPromise = this.props.fetchTmGroupList(info);
      promises.push(tmGroupListPromise);

      await Promise.all(promises).then(async (res) => {
        await this.setState({
          ...this.state,
          groupErrors: {},
          tmGroupNameCreated: groupOptions.tmGroupName,
          groupOptions: {
            tmGroupName: "",
            tmGroupShortcode: "",
            selectedOrganization: this.state.groupOptions.selectedOrganization
          },
          tmGroupCreationSuccess: true,
          organizationList: res[0].data,
          tmGroupList: res[1].data
        });
      });
    } catch (err) {
      await this.setState({
        ...this.state,
        groupErrors: {
          ...this.state.groupErrors,
          global: err.response.data.global
        }
      });
    }
    finally {
      this.setState({
        ...this.state,
        tmGroupLoading: false
      });
    }
  };

  handleFetchTmGroupCode = async () => {
    let { groupOptions, groupErrors } = this.state;
    let { tmGroupName } = groupOptions;
    if (tmGroupName.length === 0 || groupErrors.hasOwnProperty("tmGroupName")) {
      return;
    }
    this.setState({ ...this.state, tmGroupShortCodeCreateLoading: true });
    const { oauthUser } = this.props;
    let data = {};
    if (oauthUser && !oauthUser.expired) {
      data.access_token = oauthUser.access_token;
    }
    data.name = tmGroupName;
    this.props.fetchTmGroupCode(data).then(res => {
      groupOptions.tmGroupShortcode = res.data.toString();
      this.setState({
        ...this.state,
        groupOptions
      });
    }).catch(err => {
      this.setState({
        ...this.state,
        groupErrors: {
          ...this.state.groupErrors,
          global: err.response.data.global
        }
      });
    }).finally(() => {
      this.setState({
        ...this.state,
        tmGroupShortCodeCreateLoading: false
      });
    });
  };

  tmGroupValidate = (groupOptions, tmGroupList) => {
    let groupErrors = {};
    const {
      tmGroupName,
      tmGroupShortcode,
      selectedOrganization
    } = groupOptions;
    if (selectedOrganization === "") {
      groupErrors.organization = "Please select an organization";
    }
    if (tmGroupName === "") {
      groupErrors.tmGroupName = "Tm Group Name cannot be empty";
    }
    else {
      const duplicateTmGroupName = tmGroupList.filter(
        tmGroup => tmGroup.label === tmGroupName
      );
      if (duplicateTmGroupName.length > 0) {
        groupErrors.tmGroupName = "Tm Group Name has already exists";
      }
    }

    if (tmGroupShortcode === "") {
      groupErrors.tmGroupShortcode = "Tm Group Short Code cannot be empty";
    }
    else if (tmGroupShortcode.length > 10) {
      groupErrors.tmGroupShortcode = "Tm Group Shortcode cannot exceed 10 characters";
    }
    return groupErrors;
  };

  handleCheckDuplicateTMs = async (options) => {
    const res = await this.props.checkDuplicateTm(options);
    const { success } = res;
    if (success === true) {
      return [false, new Array()];
    }
    else {
      return [true, res.data.duplicateTmLanguagePairs];
    }
  };

  matchBeginningTextSearch = (languageList, query) => {
    query = "^" + escapeRegExp(query);
    const regex = new RegExp(query, "i");
    return languageList.filter(opt => regex.test(opt.text));
  };

  handleTwoLetterLanguageCodeConfirmationCancel = (event) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      twoLetterCodeConfirmation: false,
      openTwoLetterCodeConfirmation: false
    });
  };

  handleTwoLetterLanguageCodeConfirmationConfirm = (event) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      twoLetterCodeConfirmation: true,
      openTwoLetterCodeConfirmation: false
    });
    this.onCreateTmSubmit(event);
  };

  handleCheckDuplicateTmConfirmationCancel = (event) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      duplicateTmInSameGroupConfirm: false,
      openCheckDuplicateTmInSameGroupConfirmation: false,
      duplicateTmLocalesPairText: ''
    })
  }

  handleCheckDuplicateTmConfirmationConfirm = (event) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      duplicateTmInSameGroupConfirm: true,
      openCheckDuplicateTmInSameGroupConfirmation: false,
      duplicateTmLocalesPairText: ''
    });
    this.onCreateTmSubmit(event);
  }

  checkTwoLetterLanguageCodeExitedInOptions = (options) => {
    // check if source locale is two letter code:
    if (options.sourceLanguage.length === 2) {
      return true;
    }
    // check if target locale has two letter code:
    for (let i = 0, max = options.targetLanguages.length; i < max; i++) {
      const targetLanguage = options.targetLanguages[i];
      if (targetLanguage.length === 2) {
        return true;
      }
    }
    return false;
  }

  onCreateTmSubmit = async (event) => {
    event.preventDefault();

    let { options, openTwoLetterCodeConfirmation,
      twoLetterCodeConfirmation,
      duplicateTmInSameGroupConfirm,
      openCheckDuplicateTmInSameGroupConfirmation } = this.state;
    const errors = this.validate(options);
    this.setState({
      ...this.state,
      errors
    });
    if (Object.keys(errors).length !== 0) {
      return;
    }

    // Check user locale selection to see if there is two letter language code:
    if (!openTwoLetterCodeConfirmation && !twoLetterCodeConfirmation) {
      const twoLetterLanguageCodeExisted = this.checkTwoLetterLanguageCodeExitedInOptions(options);
      if (twoLetterLanguageCodeExisted === true) {
        this.setState({
          ...this.state,
          openTwoLetterCodeConfirmation: true
        });
        return;
      }
    }

    // check duplicate TMs in the same TM Group:
    if (!openCheckDuplicateTmInSameGroupConfirmation && !duplicateTmInSameGroupConfirm) {
      await this.setState({
        ...this.state,
        duplicateTmCheckLoading: true
      })
      const [duplicateTmExist, duplicateLanguagePairs] = await this.handleCheckDuplicateTMs(options);
      await this.setState({
        ...this.state,
        duplicateTmCheckLoading: false
      })
      if (duplicateTmExist === true && !!duplicateLanguagePairs && duplicateLanguagePairs.length > 0) {
        let duplicateTmLocalesPairText = '';
        duplicateLanguagePairs.forEach(pair => {
          duplicateTmLocalesPairText += pair.sourceLanguage + " -> " + pair.targetLanguage + ", ";
        });
        duplicateTmLocalesPairText = duplicateTmLocalesPairText.substring(0, duplicateTmLocalesPairText.length - 2);
        this.setState({
          ...this.state,
          duplicateTmLocalesPairText,
          openCheckDuplicateTmInSameGroupConfirmation: true
        });
        return;
      }
    }

    await this.setState({
      ...this.state,
      numOfCreatedTm: 0,
      failureTmCreationResponses: [],
      createTmLoading: true,
      refreshImportTmListLoading: false,
      openTwoLetterCodeConfirmation: false,
      twoLetterCodeConfirmation: false,
      openCheckDuplicateTmInSameGroupConfirmation: false,
      duplicateTmInSameGroupConfirm: false
    });

    // TMSTool-32 No space is allowed in Generate username
    options.genericUsername = options.genericUsername.replace(/\s/g, "");
    // TMSTool-33. Remove Trailing and leading spaces in all value fields:
    options.tmName = options.tmName.trim();
    options.user.password = options.user.password.replace(/\s/g, "");
    options.cleanupCode = options.cleanupCode.replace(/\s/g, "");
    try {
      const { oauthUser } = this.props;
      let data = {};
      if (oauthUser && !oauthUser.expired) {
        data.accessToken = oauthUser.access_token;
        options.accessToken = oauthUser.access_token;
      }
      data.cleanupCode = options.cleanupCode;
      await this.props.validateCleanupPasscode(data);
      const res = await this.props.createTm(options);
      if (options.pdCsvCreationEnabled && !!res.data.pdCsvContent) {
        const time = new Date().toLocaleDateString("en-US").replace(/\//g, '-');
        const alasql = await import('alasql');
        alasql.default('SELECT * INTO CSV("' + time + '-PD_strings.csv",{headers:false,separator:","}) FROM ? ', [res.data.pdCsvContent]);
      }

      // TMSTool-78 Display the number of successfully created TM.
      let numOfCreatedTm = 0;
      let failureTmCreationResponses = [];

      if (!!res.data.createTmResponses) {
        const { createTmResponses } = res.data;
        for (let i = 0; i < createTmResponses.length; i++) {
          let createTmResponse = createTmResponses[i];
          if (createTmResponse.success === true) {
            numOfCreatedTm = numOfCreatedTm + 1;
          }
          else if (createTmResponse.success === false) {
            failureTmCreationResponses.push(createTmResponse);
          }
        }
      }

      const notificationType = "Tm Creation Update";
      const notificationContent = "Tm Creation is successful! Please check in Tm Server.";
      sendBrowserNotification(notificationType, notificationContent);
      await this.setState({
        ...this.state,
        errors: {},
        createTmLoading: false,
        success: true,
        numOfCreatedTm,
        failureTmCreationResponses,
        refreshImportTmListLoading: true
      });
      // refresh import Tm list after users creates TM
      const info = {
        type: "import",
        refresh: true
      };
      await this.props.fetchImportTmList(info);
    }
    catch (err) {
      await this.setState({
        ...this.state,
        errors: {
          ...errors,
          global: (!!err.response && !!err.response.data) ? err.response.data.global : null,
          success: false
        }
      });
    }
    finally {
      this.setState({
        ...this.state,
        createTmLoading: false,
        refreshImportTmListLoading: false
      });
    }
  };

  validate = options => {
    let errors = {};
    if (options.selectedOrganization === "") {
      errors.organization = "Please select organization";
    }
    if (options.selectedTmGroup === "") {
      errors.tmGroup = "Please select one Tm Group";
    }
    if (options.sourceLanguage === "") {
      errors.sourceLanguage = "Please select source language";
    }
    if (options.targetLanguages.length === 0) {
      errors.targetLanguage = "Please select at least one target language";
    }
    if (options.tmName === "") {
      errors.tmName = "Tm Name cannot be blank";
    }
    if (options.genericUsername === "") {
      errors.genericUsername = "Generic Username cannot be blank";
    }
    if (options.user.password === "") {
      errors.genericUserpassword = "Generic Userpassword cannot be blank";
    }
    if (options.cleanupCode === "") {
      errors.cleanupCode = "Cleanup code cannot be empty";
    }
    return errors;
  };

  render() {
    const {
      initLoading,
      refreshLoading,
      refreshImportTmListLoading,
      loading,
      tokenExpiredLoading,
      autoFillTmInfoLoading,
      organizationList,
      tmGroupList,
      languageList,
      userRoleList,
      options,
      errors,
      numOfCreatedTm,
      failureTmCreationResponses,
      success,
      isCreateTmGroupModalVisible,
      tmGroupNameCreated,
      groupOptions,
      tmGroupLoading,
      createTmLoading,
      tmGroupCreationSuccess,
      groupErrors,
      unauthorized,
      openTwoLetterCodeConfirmation,
      openCheckDuplicateTmInSameGroupConfirmation,
      duplicateTmLocalesPairText,
      duplicateTmCheckLoading
    } = this.state;

    if (unauthorized) {
      return <Redirect to={`/${process.env.TOOLURL}/login`} />;
    }

    if (Object.keys(errors).length > 0) {
      window.scrollTo(0, 0);
    }

    const totalTmCreationRequests = numOfCreatedTm + failureTmCreationResponses.length;

    return (
      <div style={{ marginLeft: "50px", marginRight: "50px" }}>
        <Form>
          <Dimmer inverted active={initLoading}>
            <Loader indeterminate>Initial Loading, Please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={loading}>
            <Loader indeterminate>Loading, Please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={tokenExpiredLoading}>
            <Loader indeterminate>Token expired, refreshing it...</Loader>
          </Dimmer>
          <Dimmer inverted active={refreshLoading}>
            <Loader indeterminate>
              Refreshing Tm Group List and Organization List, please wait...
            </Loader>
          </Dimmer>
          <Dimmer inverted active={autoFillTmInfoLoading}>
            <Loader indeterminate>
              Auto filling Tm creation info, please wait...
            </Loader>
          </Dimmer>
          <Dimmer inverted active={duplicateTmCheckLoading}>
            <Loader indeterminate>
              Checking if any duplicate Tm exists, please wait...
            </Loader>
          </Dimmer>
          <Dimmer inverted active={createTmLoading}>
            <Loader indeterminate>
              Tm Creation is in process, please wait...
            </Loader>
          </Dimmer>
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column width={4}>
                <h4>Create Tm</h4>
              </Grid.Column>
              <Grid.Column floated="right" width={5}>
                {refreshImportTmListLoading && (
                  <div>
                    <Icon
                      name="refresh"
                      color="orange"
                      loading={refreshImportTmListLoading}
                    />
                    <p>
                      Tm Creation is done, now it is refreshing import Tm list
                    </p>
                  </div>
                )}
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Grid columns={3} celled>
            {!success && errors && errors.global && (
              <Message negative>
                <Message.Header>Something went wrong</Message.Header>
                <p>{errors.global}</p>
              </Message>
            )}
            {success && (
              <Message positive>
                <Message.Header>Tm Creation Update</Message.Header>
                <p>Total {numOfCreatedTm} of {totalTmCreationRequests} TMs has been created.
                  Please check in Tm Server.</p>
                {totalTmCreationRequests.length > 0 &&
                  <>
                    <p>Tm failed to create:</p>
                    <Message.List>
                      {totalTmCreationRequests.forEach((_, res) => (
                        <Message.Item>TmName: {res.tmName},
                          source locale: {res.sourceLanguage},
                          target locale: {res.targetLangauge},
                          tmShortCode: {res.tmShortCode},
                          errorMessage: {res.errorMessage}
                        </Message.Item>
                      ))}
                    </Message.List>
                  </>}
              </Message>
            )}
            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Tm Group:</label>
                </Form.Field>
              </Grid.Column>
              <TmGroup
                selectedTmGroup={options.selectedTmGroup}
                tmGroupList={tmGroupList}
                errors={errors}
                handleTmGroupOptionChange={this.handleTmGroupOptionChange.bind(this)}
                isMulti={false}
              />
              <Grid.Column>
                {process.env.INSTANCE === 'GL-TPT1' ? (
                  <Popup
                    trigger={
                      <Button
                        icon
                        color="purple"
                      >
                        <Icon name="plus" />
                          &nbsp;&nbsp;Add New TM Group
                        </Button>
                    }
                    content="No new clients to be created on this server, create them on tptprod3 instead."
                  />
                ) : (<Button
                  icon
                  color="purple"
                  onClick={this.handleCreateTmGroupModalVisible.bind(this)}
                >
                  <Icon name="plus" />
                &nbsp;&nbsp;Add New TM Group
                </Button>)}
              </Grid.Column>
              <Grid.Column width={8}>
                <Popup
                  trigger={
                    <Button
                      float="right"
                      color="green"
                      onClick={this.refreshTmOrganizationAndGroupList.bind(
                        this
                      )}
                    >
                      <Icon name="refresh" />
                      Refresh Tm Organization and Group List
                    </Button>
                  }
                  content="Refresh Tm Group List and Organization List"
                />
              </Grid.Column>
            </Grid.Row>

            {isCreateTmGroupModalVisible && (
              <CreateTmGroupForm
                selectedOrganization={options.selectedOrganization}
                handleOrganizationOptionChange={this.handleOrganizationOptionChange.bind(this)}
                organizationList={organizationList}
                errors={errors}
                groupErrors={groupErrors}
                tmGroupCreationSuccess={tmGroupCreationSuccess}
                tmGroupNameCreated={tmGroupNameCreated}
                tmGroupLoading={tmGroupLoading}
                tmGroupName={groupOptions.tmGroupName}
                tmGroupShortcode={groupOptions.tmGroupShortcode}
                handleTmGroupFields={this.handleTmGroupFields.bind(this)}
                onTmGroupSubmit={this.onTmGroupSubmit.bind(this)}
                handleCreateTmGroupModalVisible={this.handleCreateTmGroupModalVisible.bind(this)}
                tmGroupShortCodeCreateLoading={this.state.tmGroupShortCodeCreateLoading}
                fetchTmGroupCode={this.handleFetchTmGroupCode.bind(this)}
              />
            )}
            <Grid.Row>
              <Grid.Column width={12}>
                <Form.Field required>
                  <label>
                    TPT Production: Please use 4 letter language code for TM
                    Creation.
                  </label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field required>
                  <label>
                    {!options.invertedLanguage
                      ? "Source language"
                      : "Target Language"}
                    :
                  </label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Dropdown
                    placeholder={
                      !options.invertedLanguage
                        ? "Select Source Language"
                        : "Select Target Language"
                    }
                    error={
                      !!this.state.errors && !!this.state.errors.sourceLanguage
                    }
                    name="sourceLanguage"
                    defaultValue="en-US"
                    fluid
                    search
                    selection
                    loading={initLoading}
                    options={languageList}
                    onChange={this.handleSelectionOptionChange.bind(this)}
                  />
                  {errors && errors.sourceLanguage && (
                    <InlineError text={errors.sourceLanguage} />
                  )}
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field required>
                  <label>
                    {!options.invertedLanguage
                      ? "Target language(s)"
                      : "Source language(s)"}
                    :
                  </label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={5}>
                <Form.Field>
                  <Dropdown
                    placeholder={
                      !options.invertedLanguage
                        ? "Select single or multiple Target Languages"
                        : "Select single or multiple Source Languages"
                    }
                    error={
                      !!this.state.errors && !!this.state.errors.targetLanguage
                    }
                    name="targetLanguages"
                    fluid
                    selection
                    multiple
                    search={this.matchBeginningTextSearch.bind(this)}
                    value={options.targetLanguages}
                    lazyLoad={true}
                    loading={initLoading}
                    options={languageList}
                    onChange={this.handleSelectionOptionChange.bind(this)}
                  />
                  {errors && errors.targetLanguage && (
                    <InlineError text={errors.targetLanguage} />
                  )}
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={2}>
                <Form.Field>
                  <Button
                    color="red"
                    onClick={this.clearAllSelectedTargetLanguages.bind(this)}
                  >
                    Clear All
                  </Button>
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Form.Field>
                  <Popup
                    trigger={<label>Invert language:</label>}
                    content="Note: The source and target are now flipped for
                      creating into English and backtranslation TM's."
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Checkbox toggle onClick={this.handleInvertedLanguage} />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Tm Name/Client Name:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={5}>
                <Form.Field>
                  <Form.Input
                    error={!!errors && !!errors.tmName}
                    type="text"
                    id="tmName"
                    name="tmName"
                    value={options.tmName}
                    onChange={this.onChange}
                  />
                  {errors && errors.tmName && (
                    <InlineError text={errors.tmName} />
                  )}
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Popup
                  trigger={
                    <Checkbox
                      label="Append Languages To Tm Name/Client Name"
                      onChange={this.handleAppendLanguageToTmName}
                      checked={options.appendLanguagesToTmName}
                    />
                  }
                  content="Note: This option will enable appending source and
                    target language code like zh-cn to the Tm Name"
                />
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                <Form.Field>
                  <label>Tm ShortCode:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                Tm ShortCode will be generated automatically.
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Generic Username:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Form.Input
                    error={!!errors && !!errors.genericUsername}
                    type="text"
                    id="genericUsername"
                    name="genericUsername"
                    value={options.genericUsername}
                    onChange={this.onChange}
                  />
                  {errors && errors.genericUsername && (
                    <InlineError text={errors.genericUsername} />
                  )}
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Generic User Password:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={5}>
                <Form.Field>
                  <Form.Input
                    error={!!errors && !!errors.genericUserpassword}
                    type="text"
                    id="genericUserpassword"
                    name="genericUserpassword"
                    value={options.user.password}
                    onChange={this.onGenericUserPasswordChange}
                  />
                  {errors && errors.genericUserpassword && (
                    <InlineError text={errors.genericUserpassword} />
                  )}
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Button color="teal" onClick={this.fetchGenericUserpassword}>
                  <Icon name="pencil" />
                  Generate random
                </Button>
              </Grid.Column>
              <Grid.Column>
                <Checkbox
                  label="Make all generic user passwords the same"
                  checked={options.allGenericUserpasswordSame}
                  onChange={this.handleAllUserpasswordsTheSameOption}
                />
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Tm Cleanup Passcode:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={5}>
                <Form.Field>
                  <Form.Input
                    error={!!errors && !!errors.cleanupCode}
                    type="text"
                    id="cleanupCode"
                    name="cleanupCode"
                    value={options.cleanupCode}
                    onChange={this.onChange}
                  />
                  {errors && errors.cleanupCode && (
                    <InlineError text={errors.cleanupCode} />
                  )}
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Button color="teal" onClick={this.fetchCleanupPasscode}>
                  <Icon name="pencil" />
                  Generate random
                </Button>
              </Grid.Column>
              <Grid.Column>
                <Checkbox
                  label="Make all TM cleanup passcodes the same"
                  checked={options.allTmCleanupPasscodeSame}
                  onChange={this.handleAllTmCleanupPasscodesTheSameOption}
                />
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Generic User Role:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field>
                  <Select
                    name="role"
                    isClearable={false}
                    isSearchable={true}
                    closeMenuOnSelect={true}
                    value={options.user.role}
                    onChange={this.handleUserRoleOptionChange.bind(this)}
                    options={userRoleList}
                  />
                </Form.Field>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                <Form.Field>
                  <Checkbox
                    label="Enable Tm(s) after creation:"
                    checked={options.enabled}
                    onChange={this.handleTmEnabledOption}
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field>
                  <Checkbox
                    label="Enable Context Mode"
                    checked={options.contextModeEnabled}
                    onChange={this.handleTmContextModeEnabledOption}
                  />
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
            <CreateTmPdCsvOptions
              options={options}
              errors={errors}
              handlePDCSVCreationEnabledOption={this.handlePDCSVCreationEnabledOption.bind(this)}
              handlePdCsvFieldsChange={this.handlePdCsvFieldsChange.bind(this)}
              onAddFileNameChange={this.onAddFileNameChange.bind(this)}
              onProfileNameChange={this.onProfileNameChange.bind(this)}
              onEmptyCleanupUserInfo={this.onEmptyCleanupUserInfo.bind(this)}
            />
          </Grid>
          <Button
            floated="right"
            primary
            size="large"
            disabled={loading || initLoading}
            onClick={this.onCreateTmSubmit.bind(this)}
          >
            Create
          </Button>
          <Confirm open={openTwoLetterCodeConfirmation}
            content='The locale you selected contains two letter language code, are you sure to proceed?'
            onCancel={this.handleTwoLetterLanguageCodeConfirmationCancel.bind(this)}
            onConfirm={this.handleTwoLetterLanguageCodeConfirmationConfirm.bind(this)} />

          <Confirm open={openCheckDuplicateTmInSameGroupConfirmation}
            content={`TM already exists for ${duplicateTmLocalesPairText}, are you sure to proceed?`}
            onCancel={this.handleCheckDuplicateTmConfirmationCancel.bind(this)}
            onConfirm={this.handleCheckDuplicateTmConfirmationConfirm.bind(this)} />
          <br />
          <br />
        </Form>
      </div>
    );
  }
}

CreateTmForm.propTypes = {
  fetchOrganizationList: PropTypes.func.isRequired,
  fetchTmGroupList: PropTypes.func.isRequired,
  fetchLanguageList: PropTypes.func.isRequired,
  fetchGenericUsername: PropTypes.func.isRequired,
  fetchGenericUserpassword: PropTypes.func.isRequired,
  fetchCleanupPasscode: PropTypes.func.isRequired,
  fetchGenericUserRole: PropTypes.func.isRequired,
  validateCleanupPasscode: PropTypes.func.isRequired,
  fetchTmGroupCode: PropTypes.func.isRequired,
  validateTmGroupShortCode: PropTypes.func.isRequired,
  createTmGroup: PropTypes.func.isRequired,
  createTm: PropTypes.func.isRequired,
  fetchImportTmList: PropTypes.func.isRequired,
  checkDuplicateTm: PropTypes.func.isRequired,
  oauthLogin: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  oauthUser: state.oidc.user,
  organizationListData: state.tm.organizationListData,
  tmGroupListData: state.tm.tmGroupListData,
  languageListData: state.tm.languageListData,
  genericUserRoleData: state.tm.genericUserRoleData
});

export default connect(
  mapStateToProps,
  {
    fetchOrganizationList,
    fetchTmGroupList,
    fetchLanguageList,
    fetchGenericUsername,
    fetchGenericUserpassword,
    fetchCleanupPasscode,
    fetchGenericUserRole,
    validateCleanupPasscode,
    fetchTmGroupCode,
    validateTmGroupShortCode,
    createTmGroup,
    createTm,
    fetchImportTmList,
    checkDuplicateTm,
    oauthLogin
  }
)(CreateTmForm);
