import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Button,
  Checkbox,
  Dimmer,
  Form,
  Grid,
  Icon,
  Loader,
  Message,
  Popup
} from "../../utils/reactSemanticUI";
import { Redirect } from "react-router";
import Cookies from "universal-cookie";
import { sendBrowserNotification } from "../../utils/main";
import TmOrganization from "./subComponents/TmOrganization";
import TmGroup from "./subComponents/TmGroup";

const cookies = new Cookies();
// cookies expires in one year
const cookieExpiredTime = new Date(
  new Date().getTime() + 60 * 60 * 1000 * 24 * 365
);
class CreateTmStringCsvForm extends Component {
  state = {
    loading: false,
    options: {
      selectedOrganization: {},
      selectedTmGroup: [],
      includeOnlyEnabledTm: true
    },
    success: false,
    unauthorized: false,
    errors: {}
  };

  componentDidMount = () => {
    let options = this.loadCreateTmStringCsvOptions(this.state.options);
    this.setState({
      ...this.state,
      options
    });
  };

  loadCreateTmStringCsvOptions = (options) => {
    if (!cookies.get("_includeOnlyEnabledTm")) {
      options.includeOnlyEnabledTm = true;
    }
    else {
      options.includeOnlyEnabledTm = cookies.get("_includeOnlyEnabledTm") === "true";
    }
    return options;
  }

  refreshTmOrganizationAndGroupList = (event) => {
    event.preventDefault();
    this.props.refreshTmOrganizationAndGroupList();
  }

  handleOrganizationOptionChange = (selectedOption) => {
    let selectedOrganization = {};
    // If selectedOption is not empty
    if (!!selectedOption) {
      selectedOrganization = selectedOption;
    }
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        selectedOrganization
      }
    });
  };

  handleTmGroupOptionChange = selectedOption => {
    let selectedTmGroup = [];
    if (!!selectedOption) {
      selectedTmGroup = selectedOption;
    }
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        selectedTmGroup
      }
    });
  };

  onIncludeOnlyEnabledTmChange = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        includeOnlyEnabledTm: !this.state.options.includeOnlyEnabledTm
      }
    });
    cookies.set(
      "_includeOnlyEnabledTm",
      !this.state.options.includeOnlyEnabledTm,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime, sameSite: "strict" }
    );
  };

  validate = (options) => {
    const errors = {};
    if (Object.keys(options.selectedOrganization).length === 0) {
      errors.organization = "Please select one organization";
    }
    if (options.selectedTmGroup.length === 0) {
      errors.tmGroup = "Please select at least one Tm group";
    }
    return errors;
  };

  onSubmit = (event) => {
    event.preventDefault();
    const { options } = this.state;
    const errors = this.validate(options);

    this.setState({ ...this.state, errors });
    if (Object.keys(errors).length !== 0) {
      return;
    }
    // options.selectedOrganization = options.selectedOrganization.value;
    this.setState({ ...this.state, loading: true });
    this.props.submit(options).then(res => {
      const notificationType = "CSV Creation Update";
      const notificationContent =
        "TM String CSV file is created successful! Please check the downloaded file.";
      sendBrowserNotification(notificationType, notificationContent);
      this.setState({
        errors: {},
        success: true,
        loading: false
      });
    }).catch(err => {
      this.setState({
        errors: {
          ...this.state.errors,
          global: !!err.response.data ? err.response.data.global : null
        },
        success: false,
        loading: false
      });
    });
  };

  render() {
    const {
      loading,
      options,
      success,
      unauthorized,
      errors
    } = this.state;

    const { initLoading, refreshLoading, organizationList, tmGroupList } = this.props;

    if (unauthorized) {
      return <Redirect to={`/${process.env.TOOLURL}/login`} />;
    }

    return (
      <div style={{ marginLeft: "50px", marginRight: "50px" }}>
        {success && (
          <Message positive>
            <Message.Header>TM String CSV Creation Update</Message.Header>
            <p>
              TM String CSV file is created successful! Please check the
              downloaded file.
            </p>
          </Message>
        )}
        {!success && errors.global && (
          <Message negative>
            <Message.Header>TM String CSV Creation Update</Message.Header>
            <p>
              TM String CSV file creation is failed because: {errors.global}
            </p>
          </Message>
        )}
        <Form onSubmit={this.onSubmit}>
          <Dimmer inverted active={initLoading}>
            <Loader indeterminate>Initial Loading, please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={refreshLoading}>
            <Loader indeterminate>Refreshing Tm Group, please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={loading}>
            <Loader indeterminate>
              Creating Tm String CSV, please wait...
            </Loader>
          </Dimmer>
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column width={4}>
                <h4>Create TM String CSV</h4>
              </Grid.Column>
              <Grid.Column floated="right" width={5}>
                <Button
                  basic
                  color="teal"
                  icon
                  labelPosition="left"
                  onClick={this.refreshTmOrganizationAndGroupList.bind(this)}
                >
                  <Icon name="refresh" />
                  Refresh Tm Organization and Group List
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Grid columns={2} celled>
            <Grid.Row>
              <Grid.Column>
                <Form.Field required>
                  <label>Organization:</label>
                </Form.Field>
              </Grid.Column>
              <TmOrganization
                selectedOrganization={options.selectedOrganization}
                handleOrganizationOptionChange={
                  this.handleOrganizationOptionChange.bind(this)
                }
                organizationList={organizationList}
                errors={errors}
              />
              <Grid.Column>
                <Form.Field required>
                  <label>Tm Group:</label>
                </Form.Field>
              </Grid.Column>
              <TmGroup
                selectedTmGroup={options.selectedTmGroup}
                tmGroupList={tmGroupList}
                errors={errors}
                handleTmGroupOptionChange={this.handleTmGroupOptionChange}
                isMulti={true}
              />
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={5}>
                <Form.Field required>
                  <Popup
                    trigger={<label>Include Only Enabled TM</label>}
                    content="Include only enabled Tm or both enabled and disabled Tm in csv file"
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Checkbox
                    toggle
                    onChange={this.onIncludeOnlyEnabledTmChange}
                    checked={this.state.options.includeOnlyEnabledTm}
                  />
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={10}>
                <Button
                  floated="right"
                  primary
                  size="large"
                  disabled={loading || initLoading}
                >
                  Create
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Form>
      </div>
    );
  }
}



export default connect(null, {})(CreateTmStringCsvForm);
