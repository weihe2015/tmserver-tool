import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Button,
  Checkbox,
  Dimmer,
  Form,
  Grid,
  Icon,
  Loader,
  Message,
  Popup
} from "../../utils/reactSemanticUI";
import Cookies from "universal-cookie";
import InlineError from "../messages/InlineError";
import { sendBrowserNotification } from "../../utils/main";
import { fetchOrganizationList, fetchTmGroupList } from "../../actions/tm";
import TmGroup from "./subComponents/TmGroup";

const cookies = new Cookies();
// cookies expires in one year
const cookieExpiredTime = new Date(
  new Date().getTime() + 60 * 60 * 1000 * 24 * 365
);

const initOptions = {
  cleanupUsername: "",
  cleanupPassword: "",
  paClientName: "",
  selectedTmGroup: [],
  addFileName: true,
  useGenericUsernameAsProfileName: true,
  emptyCleanupUserInfo: false
};

class CreatePDStringForm extends Component {
  state = {
    options: initOptions,
    loading: false,
    success: false,
    errors: {}
  };

  componentDidMount = () => {
    let options = initOptions;
    options = this.loadCreatePDStringOptionsFromCookies(options);
    this.setState({
      ...this.state,
      options
    });
  }

  loadCreatePDStringOptionsFromCookies = (options) => {
    if (!cookies.get("_addFileName")) {
      options.addFileName = true;
    }
    else {
      options.addFileName = cookies.get("_addFileName") === "true";
    }
    if (!cookies.get("_useGenericUsernameAsProfileName")) {
      options.useGenericUsernameAsProfileName = true;
    }
    else {
      options.useGenericUsernameAsProfileName = cookies.get("_useGenericUsernameAsProfileName") === "true";
    }
    if (!cookies.get("_emptyCleanupUserInfo")) {
      options.emptyCleanupUserInfo = true;
    }
    else {
      options.emptyCleanupUserInfo = cookies.get("_emptyCleanupUserInfo") === "true";
    }
    return options;
  }

  refreshTmOrganizationAndGroupList = async (event) => {
    event.preventDefault();
    this.props.refreshTmOrganizationAndGroupList();
  }

  onAddFileNameChange = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        addFileName: !this.state.options.addFileName
      }
    });
    cookies.set("_addFileName", !this.state.options.addFileName, {
      path: `/${process.env.TOOLURL}`,
      expires: cookieExpiredTime
    });
  };

  onProfileNameChange = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        useGenericUsernameAsProfileName: !this.state.options
          .useGenericUsernameAsProfileName
      }
    });
    cookies.set(
      "_useGenericUsernameAsProfileName",
      !this.state.options.useGenericUsernameAsProfileName,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime }
    );
  };

  onEmptyCleanupUserInfo = () => {
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        emptyCleanupUserInfo: !this.state.options.emptyCleanupUserInfo
      }
    });
    cookies.set(
      "_emptyCleanupUserInfo",
      !this.state.options.emptyCleanupUserInfo,
      { path: `/${process.env.TOOLURL}`, expires: cookieExpiredTime }
    );
  };

  onTextFieldsInputChange = (event) => {
    let { errors } = this.state;
    delete errors[event.target.name];

    this.setState({
      ...this.state,
      errors,
      success: false,
      options: {
        ...this.state.options,
        [event.target.name]: event.target.value
      }
    });
  };

  handleTmGroupOptionChange = (selectedOption) => {
    let selectedTmGroup = [];
    if (!!selectedOption) {
      selectedTmGroup = selectedOption;
    }
    this.setState({
      ...this.state,
      success: false,
      options: {
        ...this.state.options,
        selectedTmGroup: selectedTmGroup
      }
    });
  };

  onSubmit = (event) => {
    event.preventDefault();
    const { options } = this.state;
    const errors = this.validate(options);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props.submit(options).then(res => {
        const notificationType = "CSV Creation Update";
        const notificationContent =
          "PD String CSV file is created successful! Please check the downloaded file.";
        sendBrowserNotification(notificationType, notificationContent);
        this.setState({
          ...this.state,
          errors: {},
          success: true,
          loading: false
        });
      })
        .catch(err => {
          const errMessage = err.response.data.global;
          this.setState({
            success: false,
            loading: false,
            errors: { global: errMessage }
          });
        });
    }
  };

  validate = options => {
    const errors = {};
    if (!options.emptyCleanupUserInfo && !options.cleanupUsername) {
      errors.cleanupUsername = "Cleanup username cannot be empty.";
    }
    if (!options.emptyCleanupUserInfo && !options.cleanupPassword) {
      errors.cleanupPassword = "Cleanup password cannot be empty.";
    }
    if (options.selectedTmGroup.length === 0) {
      errors.tmGroup = "Please select at least one Tm group";
    }
    return errors;
  };

  render() {
    const {
      options,
      loading,
      success,
      errors
    } = this.state;

    const { initLoading, refreshLoading, tmGroupList } = this.props;

    return (
      <div style={{ marginLeft: "50px", marginRight: "50px" }}>
        {success && (
          <Message positive>
            <Message.Header>PD String CSV Creation Update</Message.Header>
            <p>
              PD String CSV file is created successful! Please check the
              downloaded file.
            </p>
          </Message>
        )}
        {!success && errors.global && (
          <Message negative>
            <Message.Header>PD String CSV Creation Update</Message.Header>
            <p>PD String CSV Creation is failed because: {errors.global}</p>
          </Message>
        )}
        <Form onSubmit={this.onSubmit}>
          <Dimmer inverted active={initLoading}>
            <Loader indeterminate>Initial Loading, please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={refreshLoading}>
            <Loader indeterminate>Refresh Tm Group, please wait...</Loader>
          </Dimmer>
          <Dimmer inverted active={loading}>
            <Loader indeterminate>
              Creating PD String CSV, please wait...
            </Loader>
          </Dimmer>
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column width={4}>
                <h4>Create PD String CSV</h4>
              </Grid.Column>
              <Grid.Column floated="right" width={5}>
                <Button
                  basic
                  color="blue"
                  icon
                  labelPosition="left"
                  onClick={this.refreshTmOrganizationAndGroupList.bind(this)}
                >
                  <Icon name="refresh" />
                  Refresh Tm Organization and Group List
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Grid columns={2} celled>
            <Grid.Row>
              <Grid.Column>
                <Form.Field required={!!options && !options.emptyCleanupUserInfo}>
                  <label>Analysis/Cleanup Username:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Form.Input
                    error={!!errors && !!errors.cleanupUsername}
                    type="text"
                    id="username"
                    name="cleanupUsername"
                    placeholder="Cleanup Username"
                    value={!!options ? options.cleanupUsername : ""}
                    onChange={this.onTextFieldsInputChange.bind(this)}
                  />
                  {errors && errors.cleanupUsername && (
                    <InlineError text={errors.cleanupUsername} />
                  )}
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field required={options && !options.emptyCleanupUserInfo}>
                  <label>Analysis/Cleanup password:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Form.Input
                    error={!!errors && !!errors.cleanupPassword}
                    type="text"
                    id="password"
                    name="cleanupPassword"
                    placeholder="Cleanup password"
                    value={!!options ? options.cleanupPassword : ""}
                    onChange={this.onTextFieldsInputChange.bind(this)}
                  />
                  {errors && errors.cleanupPassword && (
                    <InlineError text={errors.cleanupPassword} />
                  )}
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field>
                  <label>PA Client Name:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Form.Input
                    type="text"
                    id="paClientName"
                    name="paClientName"
                    placeholder="PA Client Name:"
                    value={!!options ? options.paClientName : ""}
                    onChange={this.onTextFieldsInputChange.bind(this)}
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field required>
                  <label>Add File Name:</label>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Checkbox
                    toggle
                    onChange={this.onAddFileNameChange.bind(this)}
                    checked={!!options ? options.addFileName : false}
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field required>
                  <Popup
                    trigger={
                      <label>Use Generic username as Profile Name:</label>
                    }
                    content="If checked, it will use generic username as Tm profile name. If not checked, it will use Tm name as Tm profile name."
                    basic
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Checkbox
                    toggle
                    onChange={this.onProfileNameChange.bind(this)}
                    checked={!!options ? options.useGenericUsernameAsProfileName : true}
                  />
                </Form.Field>
              </Grid.Column>

              <Grid.Column>
                <Form.Field required>
                  {/* TMSTool-75 rename label name */}
                  <Popup
                    trigger={<label>GLTMS 5.4.0 and above import format:</label>}
                    content="If checked, columns of cleanup URL, cleanup username and cleanup password will be blank."
                    basic
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Checkbox
                    toggle
                    onChange={this.onEmptyCleanupUserInfo.bind(this)}
                    checked={!!options ? options.emptyCleanupUserInfo : false}
                  />
                </Form.Field>
              </Grid.Column>

              <Grid.Column>
                <Form.Field required>
                  <label>Tm Group:</label>
                </Form.Field>
              </Grid.Column>
              <TmGroup
                selectedTmGroup={options.selectedTmGroup}
                tmGroupList={tmGroupList}
                errors={errors}
                handleTmGroupOptionChange={this.handleTmGroupOptionChange}
                isMulti={true}
              />
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={10}>
                <Button
                  floated="right"
                  primary
                  size="large"
                  disabled={loading || initLoading}
                >
                  Create
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Form>
      </div>
    );
  }
}

CreatePDStringForm.propTypes = {
  fetchOrganizationList: PropTypes.func.isRequired,
  fetchTmGroupList: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  oauthUser: state.oidc.user,
  tmGroupListData: state.tm.tmGroupListData
});

export default connect(
  mapStateToProps,
  {
    fetchOrganizationList,
    fetchTmGroupList
  }
)(CreatePDStringForm);
