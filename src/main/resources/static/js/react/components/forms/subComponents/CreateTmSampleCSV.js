import React from 'react';
import { Table } from '../../../utils/reactSemanticUI';

const CreateTmSampleCSV = ({ createTmInMultiTmGroup }) => (
  <Table celled>
    <Table.Header>
      <Table.Row>
        { createTmInMultiTmGroup && ( <Table.HeaderCell>Tm Group Name</Table.HeaderCell> )} 
        <Table.HeaderCell>Tm Name</Table.HeaderCell>
        <Table.HeaderCell>Source Locale</Table.HeaderCell>
        <Table.HeaderCell>Target Locale</Table.HeaderCell>
        <Table.HeaderCell>Tm Short Code</Table.HeaderCell>
        <Table.HeaderCell>TM Cleanup Passcode</Table.HeaderCell>
        <Table.HeaderCell>Generic Username</Table.HeaderCell>
        <Table.HeaderCell>Generic User Password</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      <Table.Row>
        { createTmInMultiTmGroup && ( <Table.Cell>Honey</Table.Cell> )} 
        <Table.Cell>honeyacs72_enus_frfr</Table.Cell>
        <Table.Cell>en-US</Table.Cell>
        <Table.Cell>fr-FR</Table.Cell>
        <Table.Cell>HON000003</Table.Cell>
        <Table.Cell>password</Table.Cell>
        <Table.Cell>honeyacs72_enus_frfr</Table.Cell>
        <Table.Cell>generic user password</Table.Cell>
      </Table.Row>
      <Table.Row>
        { createTmInMultiTmGroup && ( <Table.Cell>Honey</Table.Cell> )} 
        <Table.Cell>honeyacs72_enus_eses</Table.Cell>
        <Table.Cell>en-US</Table.Cell>
        <Table.Cell>es-ES</Table.Cell>
        <Table.Cell>HON000004</Table.Cell>
        <Table.Cell>password</Table.Cell>
        <Table.Cell>honeyacs72_enus_eses</Table.Cell>
        <Table.Cell>generic user password</Table.Cell>
      </Table.Row>
    </Table.Body>
  </Table>
);

export default CreateTmSampleCSV;