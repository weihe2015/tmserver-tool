import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox, Form, Grid, Popup } from '../../../utils/reactSemanticUI';

const CreateTmPdCsvOptions = (props) => (
    <Grid.Row>
        <Grid.Column>
            <Form.Field>
                <Checkbox label='Enable PD CSV file creation for newly created TMs'
                    checked={props.options.pdCsvCreationEnabled}
                    onChange={props.handlePDCSVCreationEnabledOption} />
            </Form.Field>
        </Grid.Column>
        {props.options.pdCsvCreationEnabled && (
            <Grid.Column width={10}>
                <Grid columns={2}>
                    <Grid.Row>
                        <Grid.Column>
                            <Form.Field required={!!props.options && !props.options.emptyCleanupUserInfo}>
                                <label>Analysis/Cleanup Username:</label>
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Form.Field>
                                <Form.Input
                                    error={!!props.errors && !!props.errors.cleanupUsername}
                                    type="text"
                                    id="username"
                                    name="cleanupUsername"
                                    placeholder="Cleanup Username"
                                    value={props.options.cleanupUsername}
                                    onChange={props.handlePdCsvFieldsChange}
                                />
                            </Form.Field>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Form.Field>
                                <label>PA Client Name:</label>
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Form.Field>
                                <Form.Input
                                    type="text"
                                    id="paClientName"
                                    name="paClientName"
                                    placeholder="PA Client Name:"
                                    value={props.options.paClientName}
                                    onChange={props.handlePdCsvFieldsChange}
                                />
                            </Form.Field>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column>
                            <Form.Field required>
                                <label>Add File Name:</label>
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Form.Field>
                                <Checkbox
                                    toggle
                                    onChange={props.onAddFileNameChange}
                                    checked={props.options.addFileName}
                                />
                            </Form.Field>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column>
                            <Form.Field required>
                                <Popup
                                    trigger={
                                        <label>
                                            Use Generic username as Profile Name:
                                        </label>
                                    }
                                    content="If checked, it will use generic username as Tm profile name. If not checked, it will use Tm name as Tm profile name."
                                    basic
                                />
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Form.Field>
                                <Checkbox
                                    toggle
                                    onChange={props.onProfileNameChange}
                                    checked={
                                        props.options.useGenericUsernameAsProfileName
                                    }
                                />
                            </Form.Field>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column>
                            <Form.Field required>
                                {/* TMSTool-75 rename label name */}
                                <Popup
                                    trigger={<label>GLTMS 5.4.0 and above import format:</label>}
                                    content="If checked, columns of cleanup URL, cleanup username and cleanup password will be blank."
                                    basic
                                />
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Form.Field>
                                <Checkbox
                                    toggle
                                    onChange={props.onEmptyCleanupUserInfo}
                                    checked={!!props.options ? props.options.emptyCleanupUserInfo : false}
                                />
                            </Form.Field>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Grid.Column>
        )}
    </Grid.Row>
)

CreateTmPdCsvOptions.propTypes = {
    errors: PropTypes.shape(),
    options: PropTypes.shape().isRequired,
    handlePDCSVCreationEnabledOption: PropTypes.func.isRequired,
    handlePdCsvFieldsChange: PropTypes.func.isRequired,
    onAddFileNameChange: PropTypes.func.isRequired,
    onProfileNameChange: PropTypes.func.isRequired,
    onEmptyCleanupUserInfo: PropTypes.func.isRequired
}

export default CreateTmPdCsvOptions;