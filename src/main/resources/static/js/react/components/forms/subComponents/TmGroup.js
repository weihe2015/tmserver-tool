import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import InlineError from '../../messages/InlineError';
import { Form, Grid } from '../../../utils/reactSemanticUI';

class TmGroup extends Component {
  state = {
    searchTmGroupQuery: ''
  }

  handTmGroupQueryChange = (query) => {
    this.setState({
      ...this.state,
      searchTmGroupQuery: query
    });
  };

  render() {
    const { errors, isMulti, tmGroupList, selectedTmGroup } = this.props;
    const { searchTmGroupQuery } = this.state;
    return (
      <Grid.Column width={8}>
        <Form.Field>
          <Select
            name="tmGroup"
            placeholder="Select TM Group"
            isClearable={isMulti}
            isSearchable={true}
            closeMenuOnSelect={!isMulti}
            onInputChange={this.handTmGroupQueryChange.bind(this)}
            inputValue={searchTmGroupQuery}
            isMulti={isMulti}
            value={Object.keys(selectedTmGroup).length === 0 ? [] : selectedTmGroup}
            onChange={this.props.handleTmGroupOptionChange}
            options={tmGroupList}
          />
          {(errors && errors.tmGroup) &&
            <InlineError text={errors.tmGroup} />}
        </Form.Field>
      </Grid.Column>
    )
  }
}

TmGroup.propTypes = {
  selectedTmGroup: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.string
      })
    ),
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string
    })
  ]),
  tmGroupList: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string
    })
  ).isRequired,
  handleTmGroupOptionChange: PropTypes.func.isRequired
}

export default TmGroup;