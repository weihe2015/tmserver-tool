export const searchLocationOptions = [
  {
    value: "SOURCE",
    label: "Source"
  },
  {
    value: "TARGET",
    label: "Target"
  },
  {
    value: "SOURCE_AND_TARGET",
    label: "Source and Target"
  }
]

export const importTmOptions = [
  {
    value: "APPEND",
    text: "APPEND"
  },
  {
    value: "DONTOVERWRITE",
    text: "KEEP EXISTING TU"
  },
  {
    value: "OVERWRITE",
    text: "OVERWRITE"
  },
  {
    value: "OVERWRITE_WHEN_IDENTICAL_ATTRIBUTES",
    text: "OVERWRITE WHEN IDENTICAL ATTRIBUTES"
  }
]

export const importTmAttributesOptions = [
  {
    value: "IGNORE",
    text: "IGNORE"
  },
  {
    value: "ONLY_EXISTING",
    text: "ONLY EXISTING"
  },
  {
    value: "PRESERVE",
    text: "PRESERVE"
  }
]
