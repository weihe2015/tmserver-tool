import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Icon } from '../../utils/reactSemanticUI';
import map from 'lodash/map';

class TmImportStatus extends Component {

  state = {
    str: this.props.str,
    removedUploadedFileLoading: this.props.removedUploadedFileLoading
  }

  componentWillReceiveProps = props => {
    this.setState({
      str: props.str,
      removedUploadedFileLoading: props.removedUploadedFileLoading
    });
  }

  render() {
    const { str, removedUploadedFileLoading } = this.state;
    // initial rendering:
    if (!str) {
      return (
        <span>
        </span>
      )
    }
    // Listing selected files:
    else if (str.startsWith('%')) {
      return (
        <span>
          <ul>
            {map(str.slice(1).split('#'), (val) => (
              <div key={val}>
                {!val.startsWith('Pending:') ?
                  (<li key={val}>{val.substring(0, val.indexOf('%'))}&nbsp;&nbsp;
                    <Button icon size="mini"
                      onClick={this.props.removeUploadedFiles}>
                      <Icon name='window close'
                        loading={removedUploadedFileLoading}>
                        <div pathname={val.substring(val.indexOf('%') + 1)}
                          filename={val.substring(0, val.indexOf('%'))}>
                        </div>
                      </Icon>
                    </Button></li>)
                  :
                  (
                    <li key={val}>{val}&nbsp;&nbsp;</li>
                  )
                }
              </div>
            ))}
          </ul>
        </span>
      )
    }
    // upload status:
    else if (str.startsWith('@')) {
      return (
        <span>
          <ul>
            {map(str.slice(1).split('#'), (val) => (
              <div key={val}>
                <li key={val}>{val}&nbsp;&nbsp;</li>
              </div>
            ))}
          </ul>
        </span>
      )
    }
    // import status:
    else {
      return (
        <span>
          <ul>
            {map(str.split('#'), (val) => (
              val.indexOf('&') > 0 ? (
                <li key={val}>
                  <ul key={val}>{map(val.split('&'), subVal => (
                    <li key={subVal}>{subVal}</li>
                  ))}
                  </ul>
                </li>
              ) : (
                  (val.length > 0) && <li key={val}>{val}</li>
                )
            ))}
            {/* {str.indexOf('&') > 0 && (
              <Button color='green' icon
                labelPosition='left' onClick={this.props.getTmImportProgress}>
                <Icon name="refresh" />
                Get Tm Import Progress
              </Button>
            )} */}
          </ul>
        </span>
      )
    }
  }
}

TmImportStatus.propTypes = {
  removeUploadedFiles: PropTypes.func.isRequired,
  getTmImportProgress: PropTypes.func.isRequired
}

export default connect(null, {})(TmImportStatus);