import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const UserRoute = ({ isConfirmed, isNotLimited, isAuthenticated, component: Component, ...rest}) => (
	<Route {...rest} render={props => (isAuthenticated) ? (isNotLimited ? <Component {...props} /> :
		<Redirect to={{
			pathname: `/${process.env.TOOLURL}/concordance`,
			state:  { fromPath: props.location }
		}} />) :
		<Redirect to={{
			pathname: `/${process.env.TOOLURL}/login`,
			state: { fromPath: props.location }
		}} />
	} />
);

UserRoute.propTypes = {
	component: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool.isRequired,
	isNotLimited: PropTypes.bool
};

const mapStateToProps = (state) => ({
	isAuthenticated: !!state.user.user && !!localStorage.tmServerToolJWT,
	isNotLimited: state.user.user && state.user.user.limited === undefined
})

export default connect(mapStateToProps)(UserRoute);
