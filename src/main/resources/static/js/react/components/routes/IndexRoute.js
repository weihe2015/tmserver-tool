import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const IndexRoute = ({ isConfirmed, isAuthenticated, component: Component, ...rest}) => (
	<Route {...rest} render={props => (isAuthenticated) ? <Component {...props} /> : 
		<Redirect to={{
			pathname: `/${process.env.TOOLURL}/login`,
			state: { fromPath: props.location }
		}} />
	} />
);

IndexRoute.propTypes = {
	component: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
	isAuthenticated: !!state.user.user
})

export default connect(mapStateToProps)(IndexRoute);
