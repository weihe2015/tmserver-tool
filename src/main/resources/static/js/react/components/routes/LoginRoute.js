import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const LoginRoute = ({ isConfirmed, isAuthenticated, component: Component, ...rest}) => (
	/** if not authenticated, render login page, else redirect user to index page */
	<Route {...rest} render={props => (!isAuthenticated) ? <Component {...props} /> : <Redirect to={{
		pathname: `/${process.env.TOOLURL}`,
		state: { fromPath: props.location }
	}} />} />
);

LoginRoute.propTypes = {
	component: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool.isRequired,
	isNotLimited: PropTypes.bool
};

const mapStateToProps = (state) => ({
	isAuthenticated: !!state.user.user && !!localStorage.tmServerToolJWT,
	isNotLimited: state.user.user && state.user.user.limited === undefined
})

export default connect(mapStateToProps)(LoginRoute);
