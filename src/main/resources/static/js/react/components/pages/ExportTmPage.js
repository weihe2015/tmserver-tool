import React from 'react';
import { connect } from 'react-redux';
import { Segment } from '../../utils/reactSemanticUI';

import TopNavigation from '../navigation/TopNavigation';
import DashboardFooter from '../navigation/DashboardFooter';
import ExportTmForm from '../forms/ExportTmForm';

const ExportTmPage = () => (
  <div>
    <Segment>
      <TopNavigation page="exportTm" />
      <ExportTmForm />
    </Segment>
    <DashboardFooter />
  </div>
)

export default connect(null, {})(ExportTmPage);
