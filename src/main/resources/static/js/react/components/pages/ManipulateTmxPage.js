import React from 'react';
import { connect } from 'react-redux';
import { Segment } from '../../utils/reactSemanticUI';
import TopNavigation from '../navigation/TopNavigation';
import DashboardFooter from '../navigation/DashboardFooter';
import ManipulateTmxForm from '../forms/ManipulateTmxForm';

const ManipulateTmxPage = () => (
  <div>
    <Segment>
      <TopNavigation page="manipulateTmx" />
      <ManipulateTmxForm />
    </Segment>
    <DashboardFooter />
  </div>
)

export default connect(null, {})(ManipulateTmxPage);