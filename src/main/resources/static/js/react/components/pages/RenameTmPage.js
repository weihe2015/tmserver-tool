import React from 'react';
import { connect } from 'react-redux';
import { Segment } from '../../utils/reactSemanticUI';

import TopNavigation from '../navigation/TopNavigation';
import DashboardFooter from '../navigation/DashboardFooter';
import RenameTmForm from '../forms/RenameTmForm';

const RenameTmPage = () => (
  <div>
    <Segment>
      <TopNavigation page="renameTm"/>
      <RenameTmForm />
    </Segment>
    <DashboardFooter />
  </div>
)

export default connect(null, {})(RenameTmPage);
