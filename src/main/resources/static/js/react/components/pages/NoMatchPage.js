import React from 'react';
import { connect } from 'react-redux';
import Header from '../navigation/Header';

const NoMatchPage = () => (
    <Header statusCode={404} message={"Page Not Found."}/>   
)

export default connect(null, {})(NoMatchPage);