import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Message, Segment } from '../../utils/reactSemanticUI';
import ReactTable from "react-table";
import map from 'lodash/map';
import Highlight from 'react-highlighter';

import ConcordanceSearchForm from '../forms/ConcordanceSearchForm';
import DashboardFooter from '../navigation/DashboardFooter';
import TopNavigation from '../navigation/TopNavigation';
import { fetchConcordanceSearchResult } from '../../actions/tm';
import {
  includeElement, removeElement, sortLanguages
} from '../../utils/main';

export class ConcordancePage extends Component {

  state = {
    searchQuery: '',
    searchResults: [],
    selectedRows: [],
    sourceLanguages: [],
    targetLanguages: [],
    errors: {}
  }

  submit = (data) => this.props.fetchConcordanceSearchResult(data).then((res) => {
    let sourceLanguages = [];
    let targetLanguages = [];
    let searchResults = [];
    if (res.data.length > 0) {
      // Sort the locales for each search, since the locales can be different
      const sortedLanguages = sortLanguages(res.data);
      sourceLanguages = sortedLanguages.sourceLanguages;
      targetLanguages = sortedLanguages.targetLanguages;

      // TMSTool-64, output the CSV file with specified JSON key order.
      res.data.forEach(searchResult => {
        let sortedSearchResult = {};
        sortedSearchResult["TM Group Name"] = searchResult["TM Group Name"];
        sortedSearchResult["TM Name"] = searchResult["TM Name"];
        sortedSearchResult["Source Language"] = searchResult["Source Language"];
        sortedSearchResult["Source"] = searchResult["Source"];
        sortedSearchResult["Target Language"] = searchResult["Target Language"];
        sortedSearchResult["Target"] = searchResult["Target"];
        sortedSearchResult["Creation User"] = searchResult["Creation User"];
        sortedSearchResult["Creation Date"] = searchResult["Creation Date"];
        sortedSearchResult["Modification User"] = searchResult["Modification User"];
        sortedSearchResult["Modification Date"] = searchResult["Modification Date"];
        searchResults.push(sortedSearchResult);
      });
    }
    this.setState({
      searchQuery:data.query, searchResults, errors: {}, sourceLanguages, targetLanguages
    });
  }).catch(err => {
    this.setState({
      searchResults:[], 
      selectedRows: [], 
      searchQuery: err.response.data.searchString,
      errors: {
        ...this.state.errors,
        global: !!err.response.data ? err.response.data.global : null
      },
    });
  })

  exportDataToExcel = async () => {
    const { searchResults } = this.state;

    if (searchResults.length > 0) {
      const time = new Date().toLocaleDateString("en-US").replace(/\//g, '-');
      const alasql = await import('alasql');
      // TMSTool-64, Petr requests to have header on exported csv file.
      alasql.default('SELECT * INTO CSV("' + time + '-search_result.csv",{headers:true,separator:","}) FROM ? ', [searchResults]);
    }
  }

  render() {
    const { searchQuery, searchResults, errors, sourceLanguages, targetLanguages } = this.state;
    let { selectedRows } = this.state;

    return (
      <div>
        <Segment>
          <TopNavigation page="concordance"/>
          { Object.keys(errors).length > 0 && (
            <Message warning>
              <Message.Header>TM Search Result:</Message.Header>
              <p>The search term "{searchQuery}" was not found in the selected TMs. 
                Try searching for a different term or search in other TMs if available.</p>
            </Message>
          )}
          {searchResults.length > 0 && (
            <Message success>
              <Message.Header>TM Search Result:</Message.Header>
              <p>The search term "{searchQuery}" was found in {searchResults.length} TUs.</p>
            </Message>
          )}
          <ConcordanceSearchForm submit={this.submit} />
          <br /><br />
        </Segment>
        {searchResults.length > 0 && (
          <Segment>
             <Button color='blue'
               onClick={this.exportDataToExcel}>
               Export data into Excel
             </Button>
             <br/><br/>
             <ReactTable
                data={searchResults}
                noDataText="No Result"
                filterable
                defaultFiltered={
                  [{
                    id: 'Source Language',
                    value: 'all'
                  }]}
                columns={[
                  {
                    Header: "Tm Group Name",
                    accessor: "TM Group Name",
                    width: 150,
                    style: { 'whiteSpace': 'unset' }
                  },
                  {
                    Header: "Tm Name",
                    accessor: "TM Name",
                    width: 200,
                    sortable: true,
                    filterable: false,
                    style: { 'whiteSpace': 'unset' }
                  },
                  {
                    Header: "Source Language",
                    accessor: "Source Language",
                    width: 250,
                    filterable: true,
                    filterMethod: (filter, row) => {
                      if (filter.value == "all") {
                        return true;
                      }
                      return row['Source Language'] === filter.value;
                    },
                    Filter: ({ filter, onChange }) => (
                      <select
                        onChange={e => onChange(e.target.value)}
                        style={{"width": "100%"}}
                        value={filter ? filter.value : "all"} >
                        <option value="all">Show All</option>
                        {map(sourceLanguages, (sourceLanguage) => (
                          <option key={sourceLanguage} value={sourceLanguage}>
                            {sourceLanguage}
                          </option>
                        ))}
                      </select>
                    )
                  },
                  {
                    Header: "Source Text",
                    accessor: "Source",
                    filterable: true,
                    width: 300,
                    style: { 'whiteSpace': 'unset' },
                    Cell: row => (
                      <Highlight search={searchQuery}
                        matchStyle={{"backgroundColor": "#FFFF00"}}>
                        {row.value}
                      </Highlight>
                    )
                  },
                  {
                    Header: "Target Language",
                    accessor: "Target Language",
                    width: 250,
                    filterable: true,
                    filterMethod: (filter, row) => {
                      if (filter.value == "all") {
                        return true;
                      }
                      return row['Target Language'] === filter.value;
                    },
                    Filter: ({ filter, onChange }) => (
                      <select
                        onChange={e => onChange(e.target.value)}
                        style={{"width": "100%"}}
                        value={filter ? filter.value : "all"} >
                        <option value="all">Show All</option>
                        {map(targetLanguages, (targetLanguage) => (
                          <option key={targetLanguage} value={targetLanguage}>
                            {targetLanguage}
                          </option>
                        ))}
                      </select>
                    )
                  },
                  {
                    Header: "Target Text",
                    accessor: "Target",
                    filterable: true,
                    width: 300,
                    style: { 'whiteSpace': 'unset' },
                    Cell: row => (
                      <Highlight search={searchQuery}
                        matchStyle={{"backgroundColor": "#FFFF00"}}>
                        {row.value}
                      </Highlight>
                    )
                  },
                  {
                    Header: "Creation User",
                    accessor: "Creation User",
                    width: 150,
                    sortable: true,
                    filterable: false,
                    style: { 'whiteSpace': 'unset' }
                  },
                  {
                    Header: "Creation Date",
                    accessor: "Creation Date",
                    width: 150,
                    sortable: true,
                    filterable: false,
                    style: { 'whiteSpace': 'unset' }
                  },
                  {
                    Header: "Modification User",
                    accessor: "Modification User",
                    width: 150,
                    sortable: true,
                    filterable: false,
                    style: { 'whiteSpace': 'unset' }
                  },
                  {
                    Header: "Modification Date",
                    accessor: "Modification Date",
                    width: 150,
                    sortable: true,
                    filterable: false,
                    style: { 'whiteSpace': 'unset' }
                  }
                ]}
                getTrProps={(state, rowInfo) => {
                  return {
                    onClick: (e) => {
                      if (includeElement(selectedRows, rowInfo.index)) {
                        const newSelectedRows =
                          removeElement(selectedRows, rowInfo.index);
                        this.setState({
                          ...this.state, success: false,
                          selectedRows: newSelectedRows
                        });
                      }
                      else {
                        const obj = { index: rowInfo.index, info: rowInfo.original };
                        selectedRows = [];
                        selectedRows.push(obj);
                        this.setState({
                          ...this.state, success: false,
                          selectedRows
                        });
                      }
                    },
                    style: {
                      background: (rowInfo &&
                        includeElement(selectedRows, rowInfo.index)) ?
                        '#00afec' : 'white',
                      color: (rowInfo &&
                        includeElement(selectedRows, rowInfo.index)) ?
                        'white' : 'black'
                    }
                  }
                }}
                defaultPageSize={10}
                className="-striped -highlight"
                style={{"textAlign": "center", "height": "600px" }}
              />
          </Segment>
        )}
        { (selectedRows.length > 0) && (
          <ReactTable
            data={selectedRows[0].info.attributes}
            noDataText="No Result"
            filterable={false}
            columns={[
              {
                Header: "Attribute Name",
                accessor: "name",
                style: { 'whiteSpace': 'unset' }
              },
              {
                Header: "Attribute Value",
                accessor: "value",
                style: { 'whiteSpace': 'unset' }
              }
            ]}
            defaultPageSize={5}
            className="-striped -highlight"
            style={{"textAlign": "center" }}
          />
        )}
        <br/><br/>
        <DashboardFooter />
      </div>
    )
  }
}

ConcordancePage.propTypes = {
  fetchConcordanceSearchResult: PropTypes.func.isRequired
}

export default connect(null,
  {fetchConcordanceSearchResult})(ConcordancePage);
