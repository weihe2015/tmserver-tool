import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Dimmer, Grid, Loader, Message, Segment } from '../../utils/reactSemanticUI';

import TopNavigation from '../navigation/TopNavigation';
import DashboardFooter from '../navigation/DashboardFooter';
import CreateTmStringCsvForm from '../forms/CreateTmStringCsvForm';
import CreatePDStringForm from '../forms/CreatePDStringForm';
import { createTmStringCsv, createPdStringCsv } from '../../actions/tm';
import { fetchOrganizationList, fetchTmGroupList } from "../../actions/tm";
import userManager from "../../utils/userManager";
import { oauthLogin } from '../../actions/auth';
import decode from 'jwt-decode';

const fetchTmInfoGeneralError = `Failed to fetch Tm Organization/Groups from TM Server`;
const fetchGroupsError = `Failed to fetch Tm Organization/Groups from TM Server,
  Please check the TM Server role setting and enable WS - Get TM List in System policy TM tab`;

class CreateCsvPage extends Component {

  state = {
    initLoading: false,
    refreshLoading: false,
    organizationList: [],
    tmGroupList: [],
    errors: {},
    unauthorized: false,
    tokenExpiredLoading: false
  }

  componentDidMount = async () => {
    this.oauthInterval = setInterval(this.oauthSilentRefresh, 30 * 1000);
    this.tmOrgGroupInterval = setInterval(this.tmOrgGroupSilentRefresh, 5 * 30 * 1000);

    const { organizationListData, tmGroupListData } = this.props;

    this.setState({ ...this.state, initLoading: true });
    const currentDate = new Date().getTime();
    if (!tmGroupListData || tmGroupListData.exp <= currentDate) {
      let info = {
        refreshTmList: false,
        refreshOrganizationList: true
      };
      const { oauthUser } = this.props;
      if (oauthUser && !oauthUser.expired) {
        info.access_token = oauthUser.access_token;
      }
      try {
        let promises = [];
        const organizationListPromise = this.props.fetchOrganizationList(info);
        promises.push(organizationListPromise);
        const tmGroupListPromise = this.props.fetchTmGroupList(info);
        promises.push(tmGroupListPromise);
        await Promise.all(promises).then(res => {
          this.setState({
            ...this.state,
            organizationList: res[0].data,
            tmGroupList: res[1].data
          });
        })
      }
      catch (err) {
        const { response } = err;
        // request is unauthorized: most likely the token is expired, direct user to login page:
        if (!!response && response.hasOwnProperty("status") && response.status === 401) {
          localStorage.clear();
          await this.setState({
            ...this.state,
            unauthorized: true,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
            }
          });
        }
        else if (!!response && response.hasOwnProperty("status") &&
          (response.status === 400 || response.status === 500)) {
          await this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: fetchGroupsError
            }
          });
        }
        else {
          await this.setState({
            ...this.state,
            errors: {
              ...this.state.errors,
              global: !!err.response.data ? err.response.data.global : fetchTmInfoGeneralError
            }
          });
        }
      }
      finally {
        this.setState({
          ...this.state,
          initLoading: false,
          success: false,
        });
      }
    }
    else {
      this.setState({
        ...this.state,
        organizationList: organizationListData.organizationList,
        tmGroupList: tmGroupListData.tmGroupList,
        initLoading: false,
        success: false,
      });
    }
  };

  componentWillUnmount = () => {
    clearInterval(this.oauthInterval);
    clearInterval(this.tmOrgGroupInterval);
  }

  oauthSilentRefresh = async () => {
    const currentDate = new Date().getTime() / 1000;
    const { oauthUser } = this.props;
    if (oauthUser && (oauthUser.expired || (oauthUser.expires_at - 120) <= currentDate)) {
      this.setState({ ...this.state, tokenExpiredLoading: true });
      try {
        await userManager.signinSilent();
        const { access_token } = oauthUser;
        const payload = decode(access_token);
        let credentials = {};
        credentials.email = payload.sub;
        credentials.creation = payload.nbf;
        credentials.expiration = payload.exp;
        credentials.accessToken = access_token;
        credentials.idToken = oauthUser.id_token;
        await this.props.oauthLogin(credentials).catch(err => {
          console.log(err);
        })
      }
      catch (err) {
        localStorage.oauthLastLoc = window.location.pathname;
        await userManager.signinRedirect();
        console.log(err);
      }
      finally {
        this.setState({ ...this.state, tokenExpiredLoading: false });
      }
    }
  }

  tmOrgGroupSilentRefresh = () => {
    const { organizationListData, tmGroupListData } = this.props;
    const currentDate = new Date().getTime() / 1000;
    if (!!tmGroupListData && tmGroupListData.exp > currentDate &&
      !!organizationListData && organizationListData.exp > currentDate) {
      return;
    }
    this.refreshTmOrganizationAndGroupList();
  }

  refreshTmOrganizationAndGroupList = async () => {
    this.setState({ ...this.state, refreshLoading: true });
    const { oauthUser } = this.props;
    const info = {
      refreshTmList: true,
      refreshOrganizationList: true
    };
    if (oauthUser && !oauthUser.expired) {
      info.access_token = oauthUser.access_token;
    }
    try {
      let promises = [];
      const organizationListPromise = this.props.fetchOrganizationList(info);
      promises.push(organizationListPromise);
      const tmGroupListPromise = this.props.fetchTmGroupList(info);
      promises.push(tmGroupListPromise);

      await Promise.all(promises).then(res => {
        this.setState({
          ...this.state,
          organizationList: res[0].data,
          tmGroupList: res[1].data
        });
      });
    }
    catch (err) {
      const { response } = err;
      // request is unauthorized: most likely the token is expired, direct user to login page:
      if (!!response && response.hasOwnProperty("status") && response.status === 401) {
        localStorage.clear();
        this.setState({
          ...this.state,
          unauthorized: true,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : null
          }
        });
      }
      else {
        this.setState({
          ...this.state,
          errors: {
            ...this.state.errors,
            global: !!err.response.data ? err.response.data.global : null
          }
        });
      }
    }
    finally {
      this.setState({
        ...this.state,
        refreshLoading: false,
        success: false,
      });
    }
  }

  tmStringCsvFormSubmit = (data) => this.props.createTmStringCsv(data).then(async (res) => {
    const time = new Date().toLocaleDateString("en-US").replace(/\//g, '-');
    const alasql = await import('alasql');
    alasql.default('SELECT * INTO CSV("' + time + '-Tm_strings.csv",{headers:false, separator:","}) FROM ? ',
      [res.data]);
  })

  pdStringCsvFormSubmit = (data) => this.props.createPdStringCsv(data).then(async (res) => {
    const time = new Date().toLocaleDateString("en-US").replace(/\//g, '-');
    const alasql = await import('alasql');
    alasql.default('SELECT * INTO CSV("' + time + '-PD_strings.csv",{headers:false,separator:","}) FROM ? ',
      [res.data]);
  })

  render() {
    const {
      initLoading,
      refreshLoading,
      tokenExpiredLoading,
      organizationList,
      tmGroupList,
      errors
    } = this.state;
    return (
      <>
        <Segment>
          <TopNavigation page="createCsv" />
          <Dimmer inverted active={tokenExpiredLoading}>
            <Loader indeterminate>Token expired, refreshing it...</Loader>
          </Dimmer>
          <Grid columns={2} celled>
            <Grid.Row>
              {errors && errors.global && (
                <Message negative>
                  <Message.Header>Something went wrong</Message.Header>
                  <p>{errors.global}</p>
                </Message>
              )}
              <Grid.Column>
                <CreateTmStringCsvForm
                  initLoading={initLoading}
                  refreshLoading={refreshLoading}
                  organizationList={organizationList}
                  tmGroupList={tmGroupList}
                  refreshTmOrganizationAndGroupList={this.refreshTmOrganizationAndGroupList}
                  submit={this.tmStringCsvFormSubmit}
                />
              </Grid.Column>
              <Grid.Column>
                <CreatePDStringForm
                  initLoading={initLoading}
                  refreshLoading={refreshLoading}
                  tmGroupList={tmGroupList}
                  refreshTmOrganizationAndGroupList={this.refreshTmOrganizationAndGroupList}
                  submit={this.pdStringCsvFormSubmit} />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <DashboardFooter />
      </>
    )
  }
}

CreateCsvPage.propTypes = {
  createTmStringCsv: PropTypes.func.isRequired,
  createPdStringCsv: PropTypes.func.isRequired,
  fetchOrganizationList: PropTypes.func.isRequired,
  fetchTmGroupList: PropTypes.func.isRequired,
  oauthLogin: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  oauthUser: state.oidc.user,
  organizationListData: state.tm.organizationListData,
  tmGroupListData: state.tm.tmGroupListData
});

export default connect(mapStateToProps, {
  createTmStringCsv,
  createPdStringCsv,
  fetchOrganizationList,
  fetchTmGroupList,
  oauthLogin
})(CreateCsvPage);
