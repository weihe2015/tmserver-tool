import React from 'react';
import { connect } from 'react-redux';
import { Segment } from '../../utils/reactSemanticUI';

import TopNavigation from '../navigation/TopNavigation';
import DashboardFooter from '../navigation/DashboardFooter';
import CreateTmWithCSVForm from '../forms/CreateTmWithCSVForm';

const CreateTmWithCSVPage = () => (
  <div>
    <Segment>
      <TopNavigation page="createTmWithCSV"/>
      <CreateTmWithCSVForm />
    </Segment>
    <DashboardFooter />
  </div>
)

export default connect(null, { })(CreateTmWithCSVPage);