import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import queryString from 'query-string';
import { Message, Segment } from '../../utils/reactSemanticUI';
import DashboardFooter from '../navigation/DashboardFooter';
import LoginForm from '../forms/LoginForm';
import { login } from '../../actions/auth';

class LoginPage extends Component {
  constructor(props) {
    super(props);
    const responseObj = queryString.parse(props.location.search);
    if (responseObj && responseObj['loginFailed'] == 'TokenExpired') {
      this.state = {
        authorized: false,
        tokenExpired: true,
        userNotFound: false,
        nonSSOUser: false,
        isLogout: false
      };
    }
    else if (responseObj && responseObj['loginFailed'] == 'userNotFound') {
      this.state = {
        authorized: false,
        tokenExpired: false,
        userNotFound: true,
        nonSSOUser: false,
        isLogout: false
      };
    }
    else if (responseObj && responseObj['loginFailed'] == 'NonSSOUser') {
      this.state = {
        authorized: false,
        tokenExpired: false,
        userNotFound: false,
        nonSSOUser: true,
        isLogout: false
      };
    }
    else {
      this.state = {
        authorized: false,
        tokenExpired: false,
        userNotFound: false,
        nonSSOUser: false,
        isLogout: false
      };
    }
  }

  state = {
    authorized: false,
    tokenExpired: false,
    userNotFound: false,
    nonSSOUser: false,
    isLogout: false
  }

  submit = (data) => this.props.login(data).then(() => this.setState({ ...this.state, authorized: true }));

  // set localStorage tmServerToolJWT token expired in 1 week
  componentWillUnmount = () => {
    setInterval(() => {
      localStorage.clear();
    }, 1000 * 604800);
  }

  render() {
    let { fromPath } = this.props.location.state || { fromPath: { pathname: `/${process.env.TOOLURL}` } };
    const { authorized, isLogout, tokenExpired, userNotFound, nonSSOUser } = this.state;
    // Either user login from login page or access this login page after login, redirect user to fromPath.
    if (authorized || localStorage.tmServerToolJWT) {
      return <Redirect to={fromPath} />
    }
    return (
      <div className="ui container" style={{ "marginTop": "15%" }}>
        <Segment style={{
          "marginLeft": "25%", "marginRight": "25%",
          "marginButtom": "20%"
        }}>
          {isLogout && (
            <Message positive>
              <Message.Header>Logout Update</Message.Header>
              <p>You have logout successfully.</p>
            </Message>
          )}
          {tokenExpired && <Message warning>
            <Message.Header>Login Failed</Message.Header>
            <p>Token has expired, please try to login again.</p>
          </Message>}
          {userNotFound && <Message warning>
            <Message.Header>Login Failed</Message.Header>
            <p>No user email has found in this TM Server.</p>
          </Message>}
          {nonSSOUser && <Message warning>
            <Message.Header>Login Failed</Message.Header>
            <p>Non-SSO user with the same email address is existed</p>
          </Message>}
          <h4 style={{ "textAlign": "center" }}>
            Welcome to TM Server Tool</h4>
          <LoginForm submit={this.submit} />
          <br />
        </Segment>
        <DashboardFooter />
      </div>
    )
  }
}

LoginPage.propTypes = {
  login: PropTypes.func.isRequired
}

export default connect(null, { login })(LoginPage);
