import React from 'react';
import { connect } from 'react-redux';
import { Segment } from '../../utils/reactSemanticUI';

import TopNavigation from '../navigation/TopNavigation';
import DashboardFooter from '../navigation/DashboardFooter';
import ImportTmForm from '../forms/ImportTmForm';

const ImportTmPage = () => (
  <div>
    <Segment>
      <TopNavigation page="importTm"/>
      <ImportTmForm />
    </Segment>
    <DashboardFooter />
  </div>
)

export default connect(null, {})(ImportTmPage);
