import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Dimmer, Form, Message,
  Loader, Segment, Popup
} from '../../utils/reactSemanticUI';
import InlineError from '../messages/InlineError';
import { connectOtherTmServer } from '../../actions/auth';

class TMSCredential extends Component {
  state = {
    credential: {
      url: '',
      sub: '',
      pass: ''
    },
    errors: {},
    loading: false,
    success: false
  }

  onChange = (event) => {
    const name = event.target.name;
    let val = event.target.value;
    if (name === 'url') {
      if (val.endsWith('/TMS/')) {
        val = val.slice(0, -1);
      }
      val = val.replace(/([^#]+\/TMS)(\/#[^#]+)/, '$1');
    }
    this.setState({
      credential: {
        ...this.state.credential,
        [name]: val
      }
    })
  }

  onChangeTmServer = (event) => {
    event.preventDefault();
    let errors = this.validate();
    this.setState({
      ...this.state,
      errors,
      success: false,
      loading: true
    });
    const { sid } = this.props;
    let { credential } = this.state;
    // Try to fix the Tm Server URL by adding https:// and remove unnecessary tailing info.
    if (!credential.url.startsWith("https://")) {
      credential.url = "https://" + credential.url;
    }
    credential.url = credential.url.replace(/([^#]+\/TMS)(\/#[^#]+)/, '$1');
    // remove / at the end
    if (credential.url.endsWith("/TMS/")) {
      credential.url = credential.url.slice(0, -1);
    }
    credential.sid = sid;

    this.setState({
      ...this.state,
      credential,
      loading: true
    });
    this.props.connectOtherTmServer(credential).then(() => {
      this.setState({
        ...this.state,
        success: true,
        errors: {}
      });
    }).catch(err => {
      this.setState({
        ...this.state,
        success: false,
        errors: {
          ...this.state.errors,
          global: !!err.response.data ? err.response.data.global : null
        },
      });
    }).finally(() => {
      this.setState({
        ...this.state,
        loading: false
      })
    });
  }

  validate = () => {
    const errors = {};
    let { credential } = this.state;
    // No file is selected for upload
    if (!credential.url) {
      errors.url = "Tm Server URL cannot be empty.";
    }
    if (!credential.sub) {
      errors.username = "Power username cannot be empty.";
    }
    if (!credential.pass) {
      errors.password = "Power userpassword cannot be empty.";
    }
    return errors;
  }

  render() {
    const { credential, errors, loading, success } = this.state;
    return (
      <Segment>
        <div style={{ "margin": "10px 30px 20px 30px" }}>
          <Popup trigger={<h4>Optional: Connect to other Tm Server:</h4>}
            content="Note: The new Tm Server is needed for version 5.2 and above." />
          <Form>
            <Dimmer inverted active={loading}>
              <Loader indeterminate>Connecting to other Tm Server, please wait...</Loader>
            </Dimmer>
            {success && (
              <Message positive>
                <Message.Header>Connect new Tm Server successful!</Message.Header>
                <p>New Tm Server Info has saved.</p>
              </Message>
            )}
            {(errors && errors.global) ?
              <Message negative>
                <Message.Header>Connect new Tm Server Failure</Message.Header>
                <p>{errors.global}</p>
              </Message> : <div></div>
            }
            <Form.Field required>
              <label htmlFor="url">TM Server URL:</label>
              <Form.Input
                type="text"
                id="url"
                name="url"
                placeholder="Tm Server URL, Ex: https://qa-tms2.translations.com/TMS"
                value={credential.url}
                onChange={this.onChange.bind(this)}
              />
            </Form.Field>
            {(errors && errors.url) &&
              <InlineError text={errors.url} />}
            <Form.Field required>
              <label htmlFor="username">Poweruser username:</label>
              <Form.Input
                type="text"
                id="username"
                name="sub"
                placeholder="The username for poweruser in new TMS"
                value={credential.sub}
                onChange={this.onChange.bind(this)}
              />
            </Form.Field>
            {(errors && errors.username) &&
              <InlineError text={errors.username} />}
            <Form.Field required>
              <label htmlFor="url">Poweruser password:</label>
              <Form.Input
                type="text"
                id="password"
                name="pass"
                placeholder="The password for poweruser in new TMS"
                value={credential.pass}
                onChange={this.onChange.bind(this)}
              />
            </Form.Field>
            {(errors && errors.password) &&
              <InlineError text={errors.password} />}
            <br />
            <Button primary size="big" floated="right" onClick={this.onChangeTmServer}
              disabled={loading}>Connect &amp; Save</Button>
            <br /><br />
          </Form>
        </div>
      </Segment>
    )
  }
}

TMSCredential.propTypes = {
  connectOtherTmServer: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  sid: state.user.user.sid
})

export default connect(mapStateToProps, { connectOtherTmServer })(TMSCredential);