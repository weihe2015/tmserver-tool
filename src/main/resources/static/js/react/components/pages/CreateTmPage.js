import React from 'react';
import { connect } from 'react-redux';
import { Segment } from '../../utils/reactSemanticUI';

import TopNavigation from '../navigation/TopNavigation';
import DashboardFooter from '../navigation/DashboardFooter';
import CreateTmForm from '../forms/CreateTmForm';

const CreateTmPage = () => (
  <div>
    <Segment>
      <TopNavigation page="createTm"/>
      <CreateTmForm />
    </Segment>
    <DashboardFooter />
  </div>
)

export default connect(null, { })(CreateTmPage);
