import React from 'react';
import { connect } from 'react-redux';
import Header from '../navigation/Header';

const ErrorPage = () => (
    <Header statusCode={500} message={"Internal Server Error."}/>   
)
export default connect(null, {})(ErrorPage);