import React, { Component } from 'react';
import { connect } from 'react-redux';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs'
import { Button } from '../../utils/reactSemanticUI';

class ServerStatusPage extends Component {
  state = {
    text: '',
    openSocket: false
  }

  onOpenSocket = (e) => {
    const socket = new SockJS(`/${process.env.TOOLURL}/websocket`);
    const stompClient = Stomp.over(socket);
    this.stompClientRef = stompClient;
    stompClient.connect({}, (frame) => {
        this.setState({ openSocket: true });
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/thread', (message) => {
            console.log(JSON.parse(message.body).content);
        });
    });
  }

  onCloseSocket = (e) => {
    if (this.stompClientRef != null) {
      this.stompClientRef.disconnect();
      this.setState({ openSocket: false });
    }
  }

  render() {
    const { openSocket } = this.state;
    return (
      <div>
        <Button size="big" floated="right" color="green"
          onClick={this.onOpenSocket} disabled={openSocket}>Connect</Button>
        <Button size="big" floated="right" color="red"
          onClick={this.onCloseSocket} disabled={!openSocket}>Disconnect</Button>
      </div>
    )
  }
}

export default connect(null, {})(ServerStatusPage);