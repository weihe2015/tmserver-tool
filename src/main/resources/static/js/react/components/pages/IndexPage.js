import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Button, Card, Grid, Segment } from "semantic-ui-react";
import TopNavigation from "../navigation/TopNavigation";
import DashboardFooter from "../navigation/DashboardFooter";
import TMSCredentialPage from "./TMSCredentialPage";

import "../../css/IndexPage.css";

const IndexPage = ({ isNotLimited, oauthLoginLimit }) => (
  <div>
    <Segment>
      <TopNavigation page="index" />
      <div style={{ margin: "20px 60px 30px 60px" }}>
        <h4>
          Welcome to TM Server Tool! Please choose the functionality you want to
          use:
        </h4>
        <br />
        <Card.Group centered itemsPerRow={isNotLimited ? 4 : 1}>
          <div className="container">
            <div>
              <Card>
                <Card.Content>
                  <Card.Header textAlign="center">
                    Concordance Search
                  </Card.Header>
                  <Card.Description textAlign="center">
                    Perform Concordance Search on entered query on different TMs
                  </Card.Description>
                </Card.Content>
                <Card.Content extra>
                  <div className="ui two buttons">
                    <Button
                      content="Search TMs"
                      icon="fork"
                      basic
                      color="teal"
                      as={Link}
                      to={`/${process.env.TOOLURL}/concordance`}
                    />
                  </div>
                </Card.Content>
              </Card>
            </div>

            {isNotLimited && (
              <div>
                <Card>
                  <Card.Content>
                    <Card.Header textAlign="center">Create TMs</Card.Header>
                    <Card.Description textAlign="center">
                      Create multiple TMs at once by providing necessary TM
                      metadata.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div className="ui two buttons">
                      <Button
                        content="Create TMs"
                        icon="fork"
                        basic
                        color="orange"
                        as={Link}
                        to={`/${process.env.TOOLURL}/createTm`}
                      />
                    </div>
                  </Card.Content>
                </Card>
              </div>
            )}
            {isNotLimited && (
              <div>
                <Card>
                  <Card.Content>
                    <Card.Header textAlign="center">
                      Create TMs with CSV
                    </Card.Header>
                    <Card.Description textAlign="center">
                      Create multiple TMs at once by providing CSV file.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div className="ui two buttons">
                      <Button
                        content="Create TMs"
                        icon="fork"
                        basic
                        color="violet"
                        as={Link}
                        to={`/${process.env.TOOLURL}/createTmWithCSV`}
                      />
                    </div>
                  </Card.Content>
                </Card>
              </div>
            )}
            {isNotLimited && (
              <div>
                <Card>
                  <Card.Content>
                    <Card.Header textAlign="center">Import TMs</Card.Header>
                    <Card.Description textAlign="center">
                      Select TMs, upload TMX or Wf Txt files and import multiple
                      TMs at once.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div className="ui two buttons">
                      <Button
                        content="Import TMs"
                        icon="fork"
                        basic
                        color="purple"
                        as={Link}
                        to={`/${process.env.TOOLURL}/importTm`}
                      />
                    </div>
                  </Card.Content>
                </Card>
              </div>
            )}
            {isNotLimited && (
              <div>
                <Card>
                  <Card.Content>
                    <Card.Header textAlign="center">Manipulate Tmx</Card.Header>
                    <Card.Description textAlign="center">
                      manipulate TMX by removing
                      attributes, add attributes and value and remove
                      trailing spaces.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div className="ui two buttons">
                      <Button
                        content="Manipulate Tmx"
                        icon="fork"
                        basic
                        color="brown"
                        as={Link}
                        to={`/${process.env.TOOLURL}/manipulateTmx`}
                      />
                    </div>
                  </Card.Content>
                </Card>
              </div>
            )}
            {isNotLimited && (
              <div>
                <Card>
                  <Card.Content>
                    <Card.Header textAlign="center">Create CSV</Card.Header>
                    <Card.Description textAlign="center">
                      Create CSV files for PD Tm Profile creations and TM
                      Strings in TM Server.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div className="ui two buttons">
                      <Button
                        content="Create CSV"
                        icon="fork"
                        color="blue"
                        basic
                        as={Link}
                        to={`/${process.env.TOOLURL}/createCsv`}
                      />
                    </div>
                  </Card.Content>
                </Card>
              </div>
            )}
            {isNotLimited && (
              <div>
                <Card>
                  <Card.Content>
                    <Card.Header textAlign="center">Rename TMs</Card.Header>
                    <Card.Description textAlign="center">
                      Select TMs and rename their TM names
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div className="ui two buttons">
                      <Button
                        content="Rename TMs"
                        icon="fork"
                        basic
                        color="violet"
                        as={Link}
                        to={`/${process.env.TOOLURL}/renameTm`}
                      />
                    </div>
                  </Card.Content>
                </Card>
              </div>
            )}
            {isNotLimited && (
              <div>
                <Card>
                  <Card.Content>
                    <Card.Header textAlign="center">Export TMs</Card.Header>
                    <Card.Description textAlign="center">
                      Select TMs, and export them as TMX files to browsers or
                      FTP.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div className="ui two buttons">
                      <Button
                        content="Export TMs"
                        icon="fork"
                        basic
                        color="purple"
                        as={Link}
                        to={`/${process.env.TOOLURL}/exportTm`}
                      />
                    </div>
                  </Card.Content>
                </Card>
              </div>
            )}
          </div>
        </Card.Group>

        <br />
        <Grid>
          {isNotLimited && !oauthLoginLimit && (
            <Grid.Row columns={4}>
              <Grid.Column />
              <Grid.Column width={8}>
                <TMSCredentialPage />
              </Grid.Column>
              <Grid.Column />
            </Grid.Row>
          )}
        </Grid>
      </div>
    </Segment>
    <DashboardFooter />
  </div>
);

IndexPage.propTypes = {
  isNotLimited: PropTypes.bool.isRequired,
  oauthLoginLimit: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isNotLimited: state.user.user && state.user.user.limited === undefined,
  oauthLoginLimit: state.user.user && state.user.user.oauth === true
});

export default connect(
  mapStateToProps,
  {}
)(IndexPage);
