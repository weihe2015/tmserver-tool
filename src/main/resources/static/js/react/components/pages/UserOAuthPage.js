import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CallbackComponent } from "redux-oidc";
import queryString from 'query-string';
import decode from 'jwt-decode';
import { oauthLogin } from '../../actions/auth';
import userManager from '../../utils/userManager';
import { Dimmer, Loader } from "../../utils/reactSemanticUI";

class UserOAuthPage extends Component {
    render() {
        return (
            <CallbackComponent
                userManager={userManager}
                successCallback={() => {
                    const responseObj = queryString.parse(this.props.location.hash);
                    const { access_token } = responseObj;
                    const payload = decode(access_token);
                    let credentials = {};
                    credentials.email = payload.sub;
                    credentials.creation = payload.nbf;
                    credentials.expiration = payload.exp;
                    credentials.accessToken = access_token;
                    credentials.idToken = responseObj.id_token;

                    this.props.oauthLogin(credentials).then(() => {
                        if (localStorage.oauthLastLoc) {
                            this.props.history.push(`${localStorage.oauthLastLoc}`);
                        }
                        else {
                            this.props.history.push(`/${process.env.TOOLURL}`);
                        }
                    }).catch(err => {
                        if (err.response && err.response.status == 401) {
                            this.props.history.push(`/${process.env.TOOLURL}/login?loginFailed=NonSSOUser`);
                        }
                        else {
                            this.props.history.push(`/${process.env.TOOLURL}/login?loginFailed=userNotFound`);
                        }
                    })
                }}
                errorCallback={err => {
                    console.log(err);
                    this.props.history.push(`/${process.env.TOOLURL}/login?loginFailed=userNotFound`);
                }}
            >
                <Dimmer inverted active={true}>
                    <Loader indeterminate>Logining and validating token, please wait...</Loader>
                </Dimmer>
            </CallbackComponent>
        )
    }
}

UserOAuthPage.propTypes = {
    oauthLogin: PropTypes.func.isRequired
}

export default connect(null, { oauthLogin })(UserOAuthPage);