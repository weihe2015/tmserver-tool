import React from 'react';

const Header = ({ statusCode, message }) => (
    <div className="pageWrapper animsition">
        <div className="top-bar main-bg">
            <div className="container">
                <div className="center-tbl">
                    <ul className="top-info">
                        <li>
                            <a href="mailto:support@translations.com?subject=Info request&body=Dear%20Team,%0A%0Aplease%20provide%20assisstance%20for%20the%20following%20matter:%0A%0A%0AThank%20you,%0A%0A"><i className="fa fa-envelope"></i>support@translations.com</a></li>
                    </ul>
                    <ul>
                        <li><a href="#" data-toggle="modal" data-target="#disclaimermodal"><i className="fa fa-bookmark"></i>Disclaimer</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#toumodal"><i className="fa fa-legal"></i>Terms of Use</a></li>
                    </ul>

                </div>
            </div>
        </div>
        <header className="top-head" data-sticky="true">
            <div className="container">
                <div className="logo">
                    <a href="index.html"><img alt="" src="assets/images/logo.png" /></a>
                </div>
                <div className="f-right responsive-nav">
                    <nav className="top-nav nav-animate to-bottom">
                        <ul>
                            <li className="selected"><a href="#">Home</a>&nbsp;</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div id="contentWrapper">
            <div className="section">
                <div className="container">
                    <div className="row">
                        <div className="padding-vertical-30 not-found clearfix">
                            <div className="lg-not-found f-left">{statusCode}<i></i></div>
                            <div className="ops">
                                <span className="main-color bold font-50">Sorry!</span>
                                <span className="pg-nt-fnd">{message}</span>
                            </div>
                            <div>
                                <p className="t-center">Contact <a href="mailto:whe@translations.com?subject=Info request&body=Dear%20Team,%0A%0Aplease%20provide%20assisstance%20for%20the%20following%20matter:%0A%0A%0AThank%20you,%0A%0A"><i className="fa fa-envelope"></i> whe@translations.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal fade" id="disclaimermodal" tabIndex="-1" role="dialog" aria-labelledby="disclaimermodal">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id="gridSystemModalLabel">Disclaimer</h4>
                        </div>
                        <div className="modal-body">
                            <div className="padding-horizontal-20 padding-vertical-20">
                                <p>The material in this website could include technical inaccuracies or other errors. The User's use and browsing of the website is at the User's risk. Neither Translations.com nor any other party involved in creating, producing or delivering the website shall be liable for any direct, incidental, consequential, indirect or punitive damages arising out of the User's access to, or use of, the website. Translations.com does not warrant that the functional aspects of the website will be uninterrupted or error free, or that this website or the server that makes it available are free of viruses or other harmful components. Please note that some jurisdictions may not allow the exclusion of implied warranties, so some of the above exclusions may not apply to particular Users. Translations.com reserves the right to make changes to this website at any time without notice.</p>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal fade" id="toumodal" tabIndex="-1" role="dialog" aria-labelledby="toumodal">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id="gridSystemModalLabel">Terms of Use</h4>
                        </div>
                        <div className="modal-body">
                            <div className="padding-horizontal-20 padding-vertical-20">
                                <p>Confidential The material on this page is intended solely for [client], and their employees that utilize Translations.com services. Contents shall not be unlawfully distributed.</p>
                                <p>&copy; 2017 Translations.com</p>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <footer id="footWrapper">
                <div className="footer-top main-bg">
                    <div className="container">
                        <div className="row">
                            <div className="f-left"><span className="font-20 uppercase">
                                <span className="light-text bolder">
                                    <i className="fa fa-cog fa-spin"></i> GlobalLink</span> &ndash; The world's friendliest multilingual CMS</span></div>
                            <div className="f-right">
                                <a href="http://www.translations.com/globallink/products/globallink_enterprise.html" className="alter-bg block-link">More details</a></div>
                        </div>
                    </div>
                </div>
                <div className="footer-middle">
                    <div className="container">
                        <div className="row">

                            <div className="col-md-3 first">
                                <h3>Main Menu</h3>
                                <ul className="menu-widget">
                                    <li><a href="#top">Home</a></li>
                                </ul>
                            </div>
                            <div className="col-md-3 contact-widget" style={{"background": "transparent"}}>
                                <h3>Support</h3>
                                <div className="details">
                                    <ul>
                                        <li><a href="mailto:support@translations.com?subject=Info request&body=Dear%20Team,%0A%0Aplease%20provide%20assisstance%20for%20the%20following%20matter:%0A%0A%0AThank%20you,%0A%0A" data-toggle="modal" data-target="#supportmodal">
                                        <i className="fa fa-support shape sm"></i><span>Support Request</span></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-md-3 contact-widget" style={{"background": "transparent"}}>
                                <h3>Legal</h3>
                                <ul className="details">
                                    <li><a href="#" data-toggle="modal" data-target="#disclaimermodal">
                                        <i className="fa fa-bookmark shape sm"></i><span>Disclaimer</span></a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#toumodal">
                                        <i className="fa fa-legal shape sm"></i><span>Terms of Use</span></a></li>
                                </ul>
                            </div>

                            <div className="col-md-3 last contact-widget">
                                <h3>Contact Us</h3>
                                <ul className="details">
                                    <li><i className="fa fa-map-marker shape sm"></i>
                                        <span>Three Park Avenue, 40th Floor<br />New York, NY 10016</span></li>
                                    <li><i className="fa fa-envelope shape sm"></i><span>info@translations.com</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer-bottom">
                    <div className="container">
                        <div className="row">
                            <div className="copyrights col-md-5 first">© Copyright <b className="main-color">Translations.com</b> 2018. All rights reserved.</div>

                            <div className="col-md-7 last">
                                <ul className="footer-menu f-right">
                                    <li><a href="http://www.translations.com/about-us" >About Us</a></li>
                                    <li>|</li>
                                    <li><a href="http://www.transperfect.com/about/privacy.html" >Privacy</a></li>
                                    <li>|</li>
                                    <li><a href="http://www.translations.com/about/global-group" >Global Group</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
)

export default Header;