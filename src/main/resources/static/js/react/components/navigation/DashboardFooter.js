import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const DashboardFooter = ({ tmsUrl, version }) => (
  <footer>
    {version.length > 0 && (
      <div className="footer-bottom" style={{ "textAlign": "center" }}>
        Currently connecting to <a href={tmsUrl}>{tmsUrl}</a>, Version: {version}
      </div>
    )}
    <br />
    <div className="footer-top" style={{ "textAlign": "center", "whiteSpace": "nowrap" }}>
      © Copyright <b className="main-color">TransPerfect</b>&nbsp;
      {new Date().getFullYear()}. All rights reserved.
    </div>
  </footer>
)

DashboardFooter.propTypes = {
  tmsUrl: PropTypes.string.isRequired,
  version: PropTypes.string.isRequired
}

const mapStateToProps = (state) => ({
  tmsUrl: (!!state.user.user && !!state.user.user.url) ? state.user.user.url :
    (window.location.hostname === 'localhost' ? "https://stg-tpt10.translations.com/TMS/" : window.location.origin + "/TMS"),
  version: (!!state.user.user && !!state.user.user.version) ? state.user.user.version : ""
})

export default connect(mapStateToProps, {})(DashboardFooter);
