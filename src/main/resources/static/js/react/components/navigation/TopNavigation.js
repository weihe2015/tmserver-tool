import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Menu, Image, Icon } from '../../utils/reactSemanticUI';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import GlobalLink from '../../img/gl_techservices_logo.png';
import { logout } from '../../actions/auth';
import userManager from '../../utils/userManager';

const TopNavigation = ({ user, oauthUser, isNotLimited, logout, page, oauth }) => (
  <Menu secondary pointing>
    <Menu.Item>
      <Image src={GlobalLink} size="small" />
    </Menu.Item>
    <Menu.Item as={Link} to={`/${process.env.TOOLURL}`} active={page === 'index'}>
      Home
    </Menu.Item>
    {isNotLimited &&
      <Dropdown item text="Additional Functions">
        <Dropdown.Menu>
          <Dropdown.Item as={Link} to={`/${process.env.TOOLURL}/concordance`}>
            Concordance Search
        </Dropdown.Item>
          <Dropdown.Item as={Link} to={`/${process.env.TOOLURL}/createCsv`}>
            Create CSV
        </Dropdown.Item>
          <Dropdown.Item as={Link} to={`/${process.env.TOOLURL}/createTm`}>
            Create TMs
        </Dropdown.Item>
          <Dropdown.Item as={Link} to={`/${process.env.TOOLURL}/createTmWithCSV`}>
            Create TMs with CSV
        </Dropdown.Item>
          <Dropdown.Item as={Link} to={`/${process.env.TOOLURL}/importTm`}>
            Import TMs
        </Dropdown.Item>
          <Dropdown.Item as={Link} to={`/${process.env.TOOLURL}/exportTm`}>
            Export TMs
        </Dropdown.Item>
          <Dropdown.Item as={Link} to={`/${process.env.TOOLURL}/renameTm`}>
            Rename TMs
        </Dropdown.Item>
          <Dropdown.Item as={Link} to={`/${process.env.TOOLURL}/manipulateTmx`}>
            Manipulate Tmx
        </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    }
    {isNotLimited && (
      <Menu.Item as={Link} to="https://collaborate.translations.com/display/techprodmng/GLTMS+Tool" target="_blank">
        Documentation
      </Menu.Item>
    )}
    <div style={{ "marginLeft": "1rem", "marginTop": "3.5rem" }}>
      <Menu.Item as={Link} to={`/${process.env.TOOLURL}/login?logout=true`} onClick={async () => {
        let data = {};
        data.sid = user.sid;
        if (oauth && !!oauthUser) {
          const { id_token } = oauthUser;
          await userManager.removeUser();
          // User does not want to redirect to TPAuth page for logout.
          const host = window.location.protocol + "//" + window.location.hostname;
          const endSessionURL = `https://sso-stg.transperfect.com/connect/endsession?id_token_hint=${id_token}&post_logout_redirect_uri=${host}`;
          data.endSessionURL = endSessionURL;
        }
        if (user.limited !== undefined) {
          data.limited = user.limited
        }
        await logout(data);
      }}>
        <Icon name='sign out' />
        Log out
      </Menu.Item>
    </div>
  </Menu>
)

TopNavigation.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  oauth: PropTypes.bool,
  isNotLimited: PropTypes.bool.isRequired
}

function mapStateToProps(state) {
  return {
    user: state.user.user,
    oauthUser: state.oidc.user,
    oauth: state.user.user && state.user.user.oauth,
    isNotLimited: state.user.user && state.user.user.limited === undefined
  }
}

// const mapStateToProps = (state) => ({
//   user: state.user
// })

export default connect(mapStateToProps, { logout })(TopNavigation);
