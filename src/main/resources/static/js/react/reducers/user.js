import * as types from '../types';

export default function user(state={}, action={}) {
	switch(action.type) {
		case types.USER_LOGGED_IN:
		case types.USER_OAUTH_LOGIN:
		case types.TM_SERVER_CONNECT:
			return { ...state, user: action.data };
		case types.USER_LOGGED_OUT:
			return {};
		default:
			return state;
	}
}
