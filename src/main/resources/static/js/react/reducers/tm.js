import * as types from '../types';

export default function tm(state = {}, action = {}) {
  const expireTime = 12 * 60 * 60 * 1000;
  switch (action.type) {
    case types.SEARCH_TM_LIST_FETCH: {
      const cachedData = {
        exp: new Date().getTime() + expireTime,
        searchTmList: action.data
      };
      return { ...state, searchTmListData: cachedData };
    }
    case types.CONCORDANCE_SEARCH_FETCH:
      return { ...state, concordanceSearchResult: action.data };
    case types.ORGANIZATION_LIST_FETCH: {
      const cachedData = {
        exp: new Date().getTime() + expireTime,
        organizationList: action.data
      };
      return { ...state, organizationListData: cachedData };
    }
    case types.TM_GROUP_LIST_FETCH: {
      const cachedData = {
        exp: new Date().getTime() + expireTime,
        tmGroupList: action.data
      };
      return { ...state, tmGroupListData: cachedData };
    }
    case types.TM_STRING_CSV_CREATE:
      return { ...state, tmStringCsv: action.data };
    case types.PD_STRING_CSV_CREATE:
      return { ...state, pdStringCsv: action.data };
    case types.LANGUAGE_LIST_FETCH: {
      const data = {
        languageList: action.data
      }
      return { ...state, languageListData: data };
    }
    case types.GENERIC_USERPASSWORD_FETCH:
      return { ...state, genericUserpassword: action.data };
    case types.CLEANUP_PASSCODE_FETCH:
      return { ...state, cleanupPasscode: action.data };
    case types.GENERIC_USERNAME_FETCH:
      return { ...state, genericUsername: action.data };
    case types.GENERIC_USER_ROLE_FETCH: {
      const data = {
        genericUserRole: action.data
      }
      return { ...state, genericUserRoleData: data };
    }
    case types.CLEANUP_PASSCODE_VALIDATE:
      return { ...state, validateCleanupPasscode: action.data };
    case types.TM_GROUP_SHORT_CODE_FETCH:
      return { ...state, tmGroupCodeFetched: action.data };
    case types.TM_GROUP_SHORT_CODE_VALIDATE:
      return { ...state, validateTmGroupShortCode: action.data };
    case types.TM_GROUP_CREATION:
      return { ...state, createTmGroup: action.data };
    case types.TM_DUPLICATE_CHECK:
      return { ...state, duplicateTmCheck: action.data };
    case types.TM_CREATION:
    case types.TM_CREATION_WITH_CSV:
      return { ...state, createTm: action.data };
    case types.IMPORT_TM_LIST_FETCH: {
      const cachedData = {
        exp: new Date().getTime() + expireTime,
        importTmList: action.data
      };
      return { ...state, importTmListData: cachedData };
    }
    case types.UPLOAD_IMPORT_FILE:
      return { ...state, uploadImportFile: action.data };
    case types.RANDOM_TOKEN_VALIDATE:
      return { ...state, validateRandomToken: action.data };
    case types.IMPORT_TM:
      return { ...state, importTm: action.data };
    case types.IMPORT_PROGRESS_FETCH:
      return { ...state, importProgress: action.data };
    case types.EXPORT_TM_LIST_FETCH: {
      const cachedData = {
        exp: new Date().getTime() + expireTime,
        exportTmList: action.data
      };
      return { ...state, exportTmListData: cachedData };
    }
    case types.EXPORT_TM: {
      if (!state.exportTm) {
        state.exportTm = [];
      }
      state.exportTm.push(action.data);
      return state;
    }

    case types.EXPORT_PROGRESS_FETCH: {
      if (!state.exportTmProgress) {
        state.exportTmProgress = [];
      }
      state.exportTmProgress.push(action.data);
      return state;
    }
    case types.RENAME_TM_LIST_FETCH: {
      const cachedData = {
        exp: new Date().getTime() + expireTime,
        renameTmList: action.data
      };
      return { ...state, renameTmListData: cachedData };
    }
    case types.TM_RENAME:
      return { ...state, renameTm: action.data };
    case types.TMX_FILES_FOR_MANIPULATION_UPLOAD:
      return { ...state, tmxForManipulationUpload: action.data };
    case types.TMX_FILES_MANIPULATE:
      return { ...state, tmxManipulation: action.data };
    case types.TM_SERVER_VERSION_FETCH:
      return { ...state, tmServerVersion: action.data };
    case types.TM_SERVER_TOOL_TM_INFO_CLEAR:
      return {};
    default:
      return state;
  }
}
