import React from 'react';
import ReactDOM from 'react-dom';
import decode from 'jwt-decode';
import thunk from 'redux-thunk';
import { BrowserRouter, Route } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';
import ReactGA from 'react-ga';
import { loadUser, OidcProvider } from "redux-oidc";

import App from './App';
import { userLoggedIn, logout } from './actions/auth';
import rootReducer from './rootReducer';
import registerServiceWorker from './registerServiceWorker';
import setAuthorizationHeader from './utils/setAuthorizationHeader';

import userManager from './utils/userManager';

import 'react-notifications/lib/notifications.css';
import 'react-day-picker/lib/style.css';
import './css/main.css';

const isLocalhost = Boolean(
  window.location.hostname === 'localhost' ||
    // [::1] is the IPv6 localhost address.
    window.location.hostname === '[::1]' ||
    // 127.0.0.1/8 is considered localhost for IPv4.
    window.location.hostname.match(
      /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
    )
);

const store = (isLocalhost) ? createStore(rootReducer,
	composeWithDevTools(applyMiddleware(thunk, logger))
) : (createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk))));

loadUser(store, userManager);

if (localStorage.tmServerToolJWT) {
	const payload = decode(localStorage.tmServerToolJWT);
	const currentDate = new Date().getTime();
	const expiredDate = payload.exp * 1000;
	if (expiredDate <= currentDate) {
		logout();
	}
	else {
    setAuthorizationHeader(localStorage.tmServerToolJWT);
    store.dispatch(userLoggedIn(payload));
	}
}

ReactGA.initialize('UA-77888987-2');
ReactGA.pageview(window.location.pathname + window.location.search);

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store} >
      <OidcProvider store={store} userManager={userManager}>
        <Route component={App} />
      </OidcProvider>
    </Provider>
  </BrowserRouter>,
  document.getElementById('root')
);

registerServiceWorker();
