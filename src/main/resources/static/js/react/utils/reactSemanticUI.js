import { Button, Checkbox, Confirm, Dimmer, Dropdown, Form, Grid, Icon, Image, Loader, Modal, Menu, Message, Popup, Segment, Table } from 'semantic-ui-react';

// import Button from 'semantic-ui-react/dist/es/elements/Button';
// import Checkbox from 'semantic-ui-react/dist/es/modules/Checkbox';
// import Dimmer from 'semantic-ui-react/dist/es/modules/Dimmer';
// import Dropdown from 'semantic-ui-react/dist/es/modules/Dropdown';
// import Form from 'semantic-ui-react/dist/es/collections/Form';
// import Grid from 'semantic-ui-react/dist/es/collections/Grid';
// import Icon from 'semantic-ui-react/dist/es/elements/Icon';
// import Image from 'semantic-ui-react/dist/es/elements/Image';
// import Loader from 'semantic-ui-react/dist/es/elements/Loader';
// import Modal from 'semantic-ui-react/dist/es/modules/Modal';
// import Menu from 'semantic-ui-react/dist/es/collections/Menu';
// import Message from 'semantic-ui-react/dist/es/collections/Message';
// import Popup from 'semantic-ui-react/dist/es/modules/Popup';
// import Segment from 'semantic-ui-react/dist/es/elements/Segment';
// import Table from 'semantic-ui-react/dist/es/collections/Table';

export { Button, Checkbox, Confirm, Dimmer, Dropdown, Form, 
    Grid, Icon, Image, Loader, Menu, Message, Modal, Popup, Segment, Table};