import GlobalLink from '../img/gl_techservices_logo.png';
import sortBy from 'lodash/sortBy';
import includes from 'lodash/includes';

// Handle Tm Name in search Tm List by adding/removing source/target locales from TM name:
export function onLocaleChangeWithTmName(displayLocale, dataList) {
  if (displayLocale) {
    dataList = dataList.map(data => {
      data.label = `${data.label} [${data.sourceLocale} -> ${data.targetLocale}]`;
      return data;
    });
  }
  else {
    dataList = dataList.map(data => {
      data.label = data.label.replace(/ \[.*\]/gi, "");
      return data;
    });
  }
  // TMSTool-28, Sort Tm List by label each time when language codes are added or removed.
  dataList = sortBy(dataList, data => {
    return data.label.trim();
  });
  return dataList;
}

export function includeElement(array, idx) {
  return array.filter(e => e.index === idx).length > 0;
}

export function removeElement(array, idx) {
  return array.filter(e => e.index !== idx);
}

export function filterMethod(filter, row, column) {
  const id = filter.pivotId || filter.id;
  return row[id] !== undefined ?
    String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
    : true;
}

export function isBrowserTabVisible() {
  var stateKey, eventKey, keys = {
    hidden: "visibilitychange",
    webkitHidden: "webkitvisibilitychange",
    mozHidden: "mozvisibilitychange",
    msHidden: "msvisibilitychange"
  };
  for (stateKey in keys) {
    if (stateKey in document) {
      eventKey = keys[stateKey];
      break;
    }
  }
  return !document[stateKey];
}

export function sendBrowserNotification(type, content) {
  // Enable notification even though the browser tab is active.
  // if (isBrowserTabVisible()) {
  //   console.log("Browser tab is in active mode, no need to use notification");
  //   return;
  // }
  // check if the browser supports notification:
  if (!window.Notification) {
    console.log("This browser does not support notification!");
    return;
  }
  else if (Notification.permission === 'granted') {
    // create a notification:
    let notification = new Notification(type, {
      body: content,
      icon: GlobalLink
    });
  }
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(permission => {
      // If user accepts enabling notification, let's create a notification:
      let notification = new Notification(type, {
        body: content,
        icon: GlobalLink
      });
    });
  }
  // Finally, if the user has denied notifications and you 
  // want to be respectful there is no need to bother them any more.
}

// Return 1 if a > b
// Return -1 if a < b
// Return 0 if a == b
export function compareVersion(a, b) {
  if (a === b) {
    return 0;
  }
  const a_components = a.split(".");
  const b_components = b.split(".");
  const len = Math.min(a_components.length, b_components.length);

  // loop while the components are equal
  for (let i = 0; i < len; i++) {
    // A bigger than B
    if (parseInt(a_components[i]) > parseInt(b_components[i])) {
      return 1;
    }
    // B bigger than A
    if (parseInt(a_components[i]) < parseInt(b_components[i])) {
      return -1;
    }
  }
  // If one's a prefix of the other, the longer one is greater.
  if (a_components.length > b_components.length) {
    return 1;
  }
  if (a_components.length < b_components.length) {
    return -1;
  }
  // Otherwise they are the same.
  return 0;
}

// Sort source and target locales from tmList:
export function sortLocales(tmList) {
  let sourceLocales = [];
  let targetLocales = [];
  tmList.forEach(tm => {
    const sourceLocale = tm.sourceLocale;
    const targetLocale = tm.targetLocale;
    if (!includes(sourceLocales, sourceLocale)) {
      sourceLocales.push(sourceLocale);
    }
    if (!includes(targetLocales, targetLocale)) {
      targetLocales.push(targetLocale);
    }
  });
  sourceLocales.sort();
  targetLocales.sort();
  return { sourceLocales, targetLocales };
}

// Sort source and target languages from tmList:
export function sortLanguages(tmList) {
  let sourceLanguages = [];
  let targetLanguages = [];
  tmList.forEach(tm => {
    const sourceLanguage = tm['Source Language'];
    const targetLanguage = tm['Target Language'];
    if (!includes(sourceLanguages, sourceLanguage)) {
      sourceLanguages.push(sourceLanguage);
    }
    if (!includes(targetLanguages, targetLanguage)) {
      targetLanguages.push(targetLanguage);
    }
  });
  sourceLanguages.sort();
  targetLanguages.sort();
  return { sourceLanguages, targetLanguages };
}
