import { createUserManager } from 'redux-oidc';

const userManagerConfig = {
  response_type: 'token id_token',
  scope: 'openid email profile',
  automaticSilentRenew: true,
  filterProtocolClaims: true,
  loadUserInfo: true,
  includeIdTokenInSilentRenew: true
}
// local dev:
if (process.env.PLATFORM === 'developing') {
  userManagerConfig.client_id = "VB7hnG4sPhTerRmBVwf96HzYxjNBFrnb";
  userManagerConfig.redirect_uri = `http://localhost:7000/Toolqa/SSO`;
  userManagerConfig.authority = 'https://sso-stg.transperfect.com';
  userManagerConfig.silent_redirect_uri = `http://localhost:7000/Toolqa/silent-refresh`;
}
// STG-TPT10
else if (process.env.PLATFORM === 'staging') {
  userManagerConfig.client_id = "cchahuViDui9G38HjPHq03QLYrbCAYU5";
  userManagerConfig.redirect_uri = `https://stg-tpt10.translations.com/Toolqa/SSO`;
  userManagerConfig.authority = 'https://sso-stg.transperfect.com';
  userManagerConfig.silent_redirect_uri = `https://stg-tpt10.translations.com/Toolqa/silent-refresh`;
}
else if (process.env.PLATFORM === 'production') {
  userManagerConfig.authority = 'https://sso.transperfect.com';
  // We have two production instances:
  if (process.env.INSTANCE === 'GL-TPT1') {
    userManagerConfig.client_id = "z3Qjr5BHg7n2AXU9fq1iCwLJm8WN0EkD";
    userManagerConfig.redirect_uri = `https://gl-tptprod1.transperfect.com/Tool/SSO`;
    userManagerConfig.silent_redirect_uri = `https://gl-tptprod1.transperfect.com/Tool/silent-refresh`;
  }
  else if (process.env.INSTANCE === 'GL-TPT2') {
    userManagerConfig.client_id = "sWwc7tZ5hasBknG9f0LoeFz4K5sDiFYR";
    userManagerConfig.redirect_uri = `https://gl-tptprod2.transperfect.com/Tool/SSO`;
    userManagerConfig.silent_redirect_uri = `https://gl-tptprod2.transperfect.com/Tool/silent-refresh`;
  }
  else if (process.env.INSTANCE === 'GL-TPT3') {
    userManagerConfig.client_id = "d6Iz0psQebR5uHDxVEfZNFLBUGqPrTmh";
    userManagerConfig.redirect_uri = `https://gl-tptprod3.transperfect.com/Tool/SSO`;
    userManagerConfig.silent_redirect_uri = `https://gl-tptprod3.transperfect.com/Tool/silent-refresh`;
  }
  else if (process.env.INSTANCE === 'GL-TPT4') {
    userManagerConfig.client_id = "XEGoeHcuXRdsBaBefrwX22piZljMXy9O";
    userManagerConfig.redirect_uri = `https://gl-tptprod4.transperfect.com/Tool/SSO`;
    userManagerConfig.silent_redirect_uri = `https://gl-tptprod4.transperfect.com/Tool/silent-refresh`;
  }
  else if (process.env.INSTANCE === 'GL-TMS1') {
    userManagerConfig.client_id = "qKJ27CcN5r8t3k4mAUQEZgxe6nRTuhwP";
    userManagerConfig.redirect_uri = `https://gl-tmserver1.translations.com/Tool/SSO`;
    userManagerConfig.silent_redirect_uri = `https://gl-tmserver1.translations.com/Tool/silent-refresh`;
  }
  else if (process.env.INSTANCE === 'GL-TMS2') {
    userManagerConfig.client_id = "";
    userManagerConfig.redirect_uri = `https://gl-tmserver2.translations.com/Tool/SSO`;
    userManagerConfig.silent_redirect_uri = `https://gl-tmserver2.translations.com/Tool/silent-refresh`;
  }
  else if (process.env.INSTANCE === 'GL-TM-EU1') {
    userManagerConfig.client_id = "nT8Chz9rKZHB4jRELq5GVaktspePfDWb";
    userManagerConfig.redirect_uri = `https://tm-eu1.translations.com/Tool/SSO`;
    userManagerConfig.silent_redirect_uri = `https://tm-eu1.translations.com/Tool/silent-refresh`;
  }
}

const userManager = createUserManager(userManagerConfig);

export default userManager;