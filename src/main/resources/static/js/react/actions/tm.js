import * as types from '../types';
import api from '../api';

export const searchTmListFetched = data => ({
  type: types.SEARCH_TM_LIST_FETCH,
  data
});

export const concordanceSearchFetched = data => ({
  type: types.CONCORDANCE_SEARCH_FETCH,
  data
})

export const organizationListFetched = data => ({
  type: types.ORGANIZATION_LIST_FETCH,
  data
})

export const tmGroupListFetched = data => ({
  type: types.TM_GROUP_LIST_FETCH,
  data
})

export const tmStringCsvCreated = data => ({
  type: types.TM_STRING_CSV_CREATE,
  data
})

export const pdStringCsvCreated = data => ({
  type: types.PD_STRING_CSV_CREATE,
  data
})

export const languageListFetched = data => ({
  type: types.LANGUAGE_LIST_FETCH,
  data
})

export const genericUsernameFetch = data => ({
  type: types.GENERIC_USERNAME_FETCH,
  data
})

export const genericUserpasswordFetch = data => ({
  type: types.GENERIC_USERPASSWORD_FETCH,
  data
})

export const cleanupPasscodeFetch = data => ({
  type: types.CLEANUP_PASSCODE_FETCH,
  data
})

export const genericUserRoleFetch = data => ({
  type: types.GENERIC_USER_ROLE_FETCH,
  data
})

export const cleanupPasscodeValidate = data => ({
  type: types.CLEANUP_PASSCODE_VALIDATE,
  data
})

export const tmGroupShortCodeValidate = data => ({
  type: types.TM_GROUP_SHORT_CODE_VALIDATE,
  data
})

export const tmGroupCreation = data => ({
  type: types.TM_GROUP_CREATION,
  data
})

export const tmGroupCodeFetched = data => ({
  type: types.TM_GROUP_SHORT_CODE_FETCH,
  data
})

export const tmDuplicateChecked = data => ({
  type: types.TM_DUPLICATE_CHECK,
  data
})

export const tmCreation = data => ({
  type: types.TM_CREATION,
  data
})

export const tmCreationWithCSV = data => ({
  type: types.TM_CREATION_WITH_CSV,
  data
})

export const importTmListFetch = data => ({
  type: types.IMPORT_TM_LIST_FETCH,
  data
})

export const importFileUpload = data => ({
  type: types.UPLOAD_IMPORT_FILE,
  data
})

export const randomTokenValidate = data => ({
  type: types.RANDOM_TOKEN_VALIDATE,
  data
})

export const tmImport = data => ({
  type: types.IMPORT_TM,
  data
})

export const importProgressFetch = data => ({
  type: types.IMPORT_PROGRESS_FETCH,
  data
})

export const exportTmListFetch = data => ({
  type: types.EXPORT_TM_LIST_FETCH,
  data
})

export const tmExport = data => ({
  type: types.EXPORT_TM,
  data
})

export const exportProgressFetch = data => ({
  type: types.EXPORT_PROGRESS_FETCH,
  data
})

export const exportTmInfoDelete = data => ({
  type: types.EXPORT_TM_INFO_CACHE_DELETE,
  data
})

export const renameTmListFetch = data => ({
  type: types.RENAME_TM_LIST_FETCH,
  data
})

export const tmRename = data => ({
  type: types.TM_RENAME,
  data
})

export const tmxFileForManipulationUpload = data => ({
  type: types.TMX_FILES_FOR_MANIPULATION_UPLOAD,
  data
})

export const tmxFilesManipulate = data => ({
  type: types.TMX_FILES_MANIPULATE,
  data
})

export const tmServerVersionFetch = data => ({
  type: types.TM_SERVER_VERSION_FETCH,
  data
})

export const clearTmServerToolTmInfo = () => ({
  type: types.TM_SERVER_TOOL_TM_INFO_CLEAR,
})

export const fetchSearchTmList = (data) => dispatch =>
  api.tm.fetchSearchTmList(data)
    .then(tmList => dispatch(searchTmListFetched(tmList)))

export const fetchConcordanceSearchResult = data => dispatch =>
  api.tm.fetchConcordanceSearchResult(data)
    .then(res => dispatch(concordanceSearchFetched(res)))

export const fetchOrganizationList = (data) => dispatch =>
  api.tm.fetchOrganizationList(data)
    .then(organizationList => dispatch(organizationListFetched(organizationList)))

export const fetchTmGroupList = (data) => dispatch =>
  api.tm.fetchTmGroupList(data)
    .then(tmGroupList => dispatch(tmGroupListFetched(tmGroupList)))

export const createTmStringCsv = (data) => dispatch =>
  api.tm.createTmStringCsv(data)
    .then(res => dispatch(tmStringCsvCreated(res)))

export const createPdStringCsv = (data) => dispatch =>
  api.tm.createPdStringCsv(data)
    .then(res => dispatch(pdStringCsvCreated(res)))

export const fetchLanguageList = (data) => dispatch =>
  api.tm.fetchLanguageList(data)
    .then(res => dispatch(languageListFetched(res)))

export const fetchGenericUsername = (data) => dispatch =>
  api.tm.fetchGenericUsername(data)
    .then(res => dispatch(genericUsernameFetch(res)))

export const fetchGenericUserpassword = (data) => dispatch =>
  api.tm.fetchGenericUserpassword(data)
    .then(res => dispatch(genericUserpasswordFetch(res)))

export const fetchCleanupPasscode = (data) => dispatch =>
  api.tm.fetchCleanupPasscode(data)
    .then(res => dispatch(cleanupPasscodeFetch(res)))

export const fetchGenericUserRole = (data) => dispatch =>
  api.tm.fetchGenericUserRoles(data)
    .then(res => dispatch(genericUserRoleFetch(res)))

export const validateCleanupPasscode = (data) => dispatch =>
  api.tm.validateCleanupPasscode(data)
    .then(res => dispatch(cleanupPasscodeValidate(res)))

export const validateTmGroupShortCode = (data) => dispatch =>
  api.tm.validateTmGroupCode(data)
    .then(res => dispatch(tmGroupShortCodeValidate(res)))

export const fetchTmGroupCode = (data) => dispatch =>
  api.tm.createTmGroupCode(data)
    .then(res => dispatch(tmGroupCodeFetched(res)))

export const createTmGroup = (data) => dispatch =>
  api.tm.createTmGroup(data)
    .then(res => dispatch(tmGroupCreation(res)))

export const checkDuplicateTm = (data) => dispatch =>
  api.tm.checkDuplicateTm(data)
    .then(res => dispatch(tmDuplicateChecked(res)))

export const createTm = (data) => dispatch =>
  api.tm.createTm(data)
    .then(res => dispatch(tmCreation(res)))

export const createTmWithCSV = (data, file) => dispatch =>
  api.tm.createTmWithCSV(data, file)
    .then(res => dispatch(tmCreationWithCSV(res)))

export const fetchImportTmList = (data) => dispatch =>
  api.tm.fetchImportTmList(data)
    .then(res => dispatch(importTmListFetch(res)))

export const validateRandomToken = (randomToken) => dispatch =>
  api.tm.validateRandomToken(randomToken)
    .then(res => dispatch(randomTokenValidate(res)))

export const uploadImportFile = (data, randomToken, file) => dispatch =>
  api.tm.uploadImportFile(data, randomToken, file)
    .then(res => dispatch(importFileUpload(res)))

export const importTm = (data) => dispatch =>
  api.tm.importTm(data)
    .then(res => dispatch(tmImport(res)))

export const fetchImportProgress = (data) => dispatch =>
  api.tm.fetchImportProgress(data)
    .then(res => dispatch(importProgressFetch(res)))

export const fetchExportTmList = (data) => dispatch =>
  api.tm.fetchExportTmList(data)
    .then(res => dispatch(exportTmListFetch(res)))

export const exportTm = (data) => dispatch =>
  api.tm.exportTm(data)
    .then(res => dispatch(tmExport(res)))

export const fetchExportProgress = (data) => dispatch =>
  api.tm.fetchExportProgress(data)
    .then(res => dispatch(exportProgressFetch(res)))

export const deleteExportTmInfo = (data) => dispatch =>
  api.tm.deleteExportTmInfoCache(data)
    .then(res => dispatch(exportTmInfoDelete(data)))

export const fetchRenameTmList = () => dispatch =>
  api.tm.fetchRenameTmList()
    .then(res => dispatch(renameTmListFetch(res)))

export const renameTm = (data) => dispatch =>
  api.tm.renameTm(data)
    .then(res => dispatch(tmRename(res)))

export const uploadTmxFileForManipulation = (randomToken, file) => dispatch =>
  api.tm.uploadTmxFileForManipulation(randomToken, file)
    .then(res => dispatch(tmxFileForManipulationUpload(res)))

export const manipulateTmxFiles = (data) => dispatch =>
  api.tm.tmxFilesManipulate(data)
    .then(res => dispatch(tmxFilesManipulate(res)))

export const fetchTmServerVersion = () => dispatch =>
  api.tm.fetchTmServerVersion()
    .then(res => dispatch(tmServerVersionFetch(res)))

