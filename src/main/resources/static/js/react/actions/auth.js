
import * as types from '../types';
import api from '../api';
import setAuthorizationHeader from '../utils/setAuthorizationHeader';
import userManager from '../utils/userManager';
import { clearTmServerToolTmInfo } from './tm';

export const userLoggedIn = data => ({
  type: types.USER_LOGGED_IN,
  data
})

export const userLoggedOut = () => ({
  type: types.USER_LOGGED_OUT
})

export const otherTmServerConnect = data => ({
  type: types.TM_SERVER_CONNECT,
  data
})

export const userOAuthLogin = (data) => ({
  type: types.USER_OAUTH_LOGIN,
  data
})

export const login = credential => dispatch =>
  api.user.login(credential).then(async (token) => {
    const jwt = await import('jsonwebtoken');
    if (token) {
      jwt.verify(token, 'TmServerToolSecret', { algorithms: ['HS512'] },
        (err, decoded) => {
          if (err) {
            // TODO: handle invalid token.
            console.log(err);
          }
          else {
            localStorage.tmServerToolJWT = token;
            const info = {
              sub: decoded.sub,
              sid: decoded.sid,
              limited: decoded.limited,
              version: decoded.version,
              token: token
            }
            dispatch(userLoggedIn(info));
            setAuthorizationHeader(token);
          }
        })
    }
  })

export const oauthLogin = (credential) => dispatch =>
  api.user.oauthLogin(credential).then(async (token) => {
    const jwt = await import('jsonwebtoken');
    if (token) {
      jwt.verify(token, 'TmServerToolSecret', { algorithms: ['HS512'] }, (err, decoded) => {
        if (err) {
          // TODO: handle invalid token.
          console.log(err);
        }
        else {
          localStorage.tmServerToolJWT = token;
          const info = {
            email: decoded.email,
            created: decoded.created,
            exp: decoded.exp,
            access_token: decoded.access_token,
            id_token: decoded.id_token,
            sid: decoded.sid,
            limited: decoded.limited,
            version: decoded.version,
            oauth: true
          }
          dispatch(userOAuthLogin(info));
          setAuthorizationHeader(token);
        }
      })
    }
  })

export const logout = (data) => dispatch => {
  api.user.logout(data).then(_ => {
    // console.log(res);
  }).catch(err => {
    console.log(err.response);
  }).finally(() => {
    localStorage.clear();
    setAuthorizationHeader();
    dispatch(userLoggedOut());
    dispatch(clearTmServerToolTmInfo());
    // if (!!data.endSessionURL) {
    //   userManager.signoutRedirect();
    // }
  });
}

export const connectOtherTmServer = (data) => dispatch =>
  api.user.remoteConnect(data)
    .then(async (token) => {
      const jwt = await import('jsonwebtoken');
      if (token) {
        jwt.verify(token, 'TmServerToolSecret', { algorithms: ['HS512'] }, (err, decoded) => {
          if (err) {
            // TODO: handle invalid token.
            console.log(err);
          }
          else {
            localStorage.removeItem("tmServerToolJWT");
            localStorage.tmServerToolJWT = token;
            const info = {
              limited: undefined,
              url: data.url,
              sub: decoded.sub,
              sid: decoded.sid,
              token: token
            }
            dispatch(otherTmServerConnect(info));
            setAuthorizationHeader(token);
          }
        })
      }
    })
