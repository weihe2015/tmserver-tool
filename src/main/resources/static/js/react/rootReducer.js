import { combineReducers } from 'redux';
import { reducer as oidcReducer } from 'redux-oidc';

import user from './reducers/user';
import tm from './reducers/tm';
import subscriptionsReducer from './reducers/subscription';

export default combineReducers ({
  user,
  oidc: oidcReducer,
  tm,
  subscription: subscriptionsReducer
});
