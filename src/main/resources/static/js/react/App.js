import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import { Dimmer, Loader } from './utils/reactSemanticUI';
import UserRoute from './components/routes/UserRoute';
import IndexRoute from './components/routes/IndexRoute';

const Loading = (props) => {
  if (props.error) {
    return (<div>Error!</div>)
  }
  else {
    return (<Dimmer active inverted>
              <Loader inverted>Loading Module...</Loader>
            </Dimmer>)
    }
};

const LoginPage = Loadable({
  loader: () => import('./components/pages/LoginPage'),
  loading: Loading
});

const UserOAuthPage = Loadable({
  loader: () => import('./components/pages/UserOAuthPage'),
  loading: Loading
})

const IndexPage = Loadable({
  loader: () => import('./components/pages/IndexPage'),
  loading: Loading
});

const ConcordancePage = Loadable({
  loader: () => import('./components/pages/ConcordancePage'),
  loading: Loading,
});

const CreateCsvPage = Loadable({
  loader: () => import('./components/pages/CreateCsvPage'),
  loading: Loading,
});

const CreateTmPage = Loadable({
  loader: () => import('./components/pages/CreateTmPage'),
  loading: Loading,
});

const CreateTmWithCSVPage = Loadable({
  loader: () => import('./components/pages/CreateTmWithCSVPage'),
  loading: Loading,
});

const ImportTmPage = Loadable({
  loader: () => import('./components/pages/ImportTmPage'),
  loading: Loading,
});

const ExportTmPage = Loadable({
  loader: () => import('./components/pages/ExportTmPage'),
  loading: Loading,
});

const ManipulateTmxPage = Loadable({
  loader: () => import('./components/pages/ManipulateTmxPage'),
  loading: Loading,
});

const RenameTmPage = Loadable({
  loader: () => import('./components/pages/RenameTmPage'),
  loading: Loading,
});

const ErrorPage = Loadable({
  loader: () => import('./components/pages/ErrorPage'),
  loading: Loading,
});

const NoMatchPage = Loadable({
  loader: () => import('./components/pages/NoMatchPage'),
  loading: Loading,
});

const App = ({location}) => (
  <Switch>
    <Route
      location={location}
      path={`/${process.env.TOOLURL}/login`}
      exact
      component={LoginPage}
    />
    <UserRoute
      location={location}
      path={`/${process.env.TOOLURL}`}
      exact
      component={IndexPage}
    />
   {/** OAuth page */}
    <Route
      location={location}
      path={`/${process.env.TOOLURL}/SSO`}
      exact
      component={UserOAuthPage}
    />
    <IndexRoute
      location={location}
      path={`/${process.env.TOOLURL}/concordance`}
      exact
      component={ConcordancePage}
    />
    <UserRoute
      location={location}
      path={`/${process.env.TOOLURL}/createCsv`}
      exact
      component={CreateCsvPage}
    />
    <UserRoute
      location={location}
      path={`/${process.env.TOOLURL}/createTm`}
      exact
      component={CreateTmPage}
    />
    <UserRoute
      location={location}
      path={`/${process.env.TOOLURL}/createTmWithCSV`}
      exact
      component={CreateTmWithCSVPage}
    />
    <UserRoute
      location={location}
      path={`/${process.env.TOOLURL}/importTm`}
      exact
      component={ImportTmPage}
    />
    <UserRoute
      location={location}
      path={`/${process.env.TOOLURL}/exportTm`}
      exact
      component={ExportTmPage}
    />
    <UserRoute
      location={location}
      path={`/${process.env.TOOLURL}/renameTm`}
      exact
      component={RenameTmPage}
    />
    <UserRoute
      location={location}
      path={`/${process.env.TOOLURL}/manipulateTmx`}
      exact
      component={ManipulateTmxPage}
    />
    <Route
      location={location}
      path={`/${process.env.TOOLURL}/error`}
      exact
      component={ErrorPage}
    />
    <Route component={NoMatchPage} />
  </Switch>
)

App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
}

export default connect(null)(App);
