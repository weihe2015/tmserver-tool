import axios from "axios";
import request from 'superagent-bluebird-promise';
// API Document: http://localhost:7000/Tool/swagger-ui.html#/
const plainTextConfig = {
  headers: {
    'Content-Type': 'text/plain'
  },
  responseType: 'text'
};
const APIURL = `/${process.env.TOOLURL}/api/v1`;
const APIURLV2 = `/${process.env.TOOLURL}/api/v2`;

export default {
  user: {
    login: data => axios.post(`${APIURL}/user/login`, (data))
      .then(res => res.data.token),
    oauthLogin: data => axios.post(`${APIURL}/user/oauthlogin`, (data))
      .then(res => res.data.token),
    logout: data => axios.post(`${APIURL}/user/logout`, (data))
      .then(res => res),
    preLoadTmList: data => axios.get(`${APIURL}/list?key=${data}`)
      .then(res => res.data),
    remoteConnect: data =>
      axios.post(`${APIURL}/user/remote`, (data)).then(res => res.data)
  },
  tm: {
    fetchTmServerVersion: () =>
      axios.get(`${APIURL}/tms/version`).then(res => res.data),
    fetchSearchTmList: ({ type }) =>
      axios.get(`${APIURL}/tm/tmlist?type=${type}`)
        .then(res => res.data),
    fetchConcordanceSearchResult: (data) =>
      axios.post(`${APIURL}/tm/tus`, (data))
        .then(res => res.data),
    fetchOrganizationList: ({ refreshTmList, refreshOrganizationList, access_token }) =>
      axios.get(`${APIURL}/tm/organizations?r=${refreshTmList}&refreshOrgList=${refreshOrganizationList}&access_token=${!!access_token ? access_token : ""}`)
        .then(res => res.data),
    fetchTmGroupList: ({ refreshTmList, access_token }) =>
      axios.get(`${APIURL}/tm/groups?r=${refreshTmList}&access_token=${!!access_token ? access_token : ""}`)
        .then(res => res.data),
    createTmStringCsv: data =>
      axios.post(`${APIURL}/tm/csv/tms`, (data))
        .then(res => res.data),
    createPdStringCsv: data =>
      axios.post(`${APIURL}/tm/csv/pd`, (data))
        .then(res => res.data),
    fetchLanguageList: ({ access_token }) =>
      axios.get(`${APIURL}/tm/languages?access_token=${!!access_token ? access_token : ""}`)
        .then(res => res.data),
    fetchGenericUsername: (data) =>
      axios.post(`${APIURL}/tm/genericUsername`, (data))
        .then(res => res.data),
    fetchGenericUserpassword: ({ access_token }) =>
      axios.get(`${APIURL}/tm/genericPassword?access_token=${!!access_token ? access_token : ""}`)
        .then(res => res.data),
    fetchCleanupPasscode: ({ access_token }) =>
      axios.get(`${APIURL}/tm/cleanupPasscode?access_token=${!!access_token ? access_token : ""}`)
        .then(res => res.data),
    fetchGenericUserRoles: ({ access_token }) =>
      axios.get(`${APIURL}/tm/roles?access_token=${!!access_token ? access_token : ""}`)
        .then(res => res.data),
    validateCleanupPasscode: (data) =>
      axios.post(`${APIURL}/tm/cleanupPasscode`, data)
        .then(res => res.data),
    validateTmGroupCode: (data) =>
      axios.post(`${APIURL}/tm/groupCode`, data)
        .then(res => res.data),
    validateTmShortCode: (data) =>
      axios.post(`${APIURL}/tm/shortCode`, data)
        .then(res => res.data),
    createTmGroup: data =>
      axios.post(`${APIURL}/tm/groups`, (data))
        .then(res => res.data),
    createTmGroupCode: ({ name, access_token }) =>
      axios.get(`${APIURL}/tm/groupCode?name=${name}&access_token=${!!access_token ? access_token : ""}`)
        .then(res => res.data),
    checkDuplicateTm: (data) =>
      axios.post(`${APIURL}/tm/duplicate`, (data))
        .then(res => res.data),
    createTm: (data) =>
      axios.post(`${APIURL}/tm/tms`, (data))
        .then(res => res.data),
    createTmWithCSV: (data, file) => {
      const token = localStorage.tmServerToolJWT;
      const req = request.post(`${APIURL}/tm/tmscsv`)
        .set({ Authorization: `Bearer ${token}` })
        .attach('uploadedFiles', file)
        .field('requestInfo', JSON.stringify(data))
        .promise()
        .then(res => res)
      return req;
    },
    fetchImportTmList: ({ type, refresh }) =>
      axios.get(`${APIURL}/tm/tmlist?type=${type}&isRefreshTmList=${refresh}`)
        .then(res => res.data),
    validateRandomToken: ({ token }) =>
      axios.get(`${APIURL}/tm/token?t=${token}`).then(res => res.data),
    importTm: (data) =>
      axios.put(`${APIURLV2}/tm/tms`, (data))
        .then(res => res.data),
    fetchImportProgress: ({ jobId, tmPath, access_token, filename }) =>
      axios.get(`${APIURLV2}/tm/job/${jobId}?tmpath=${tmPath}&filename=${filename}&access_token=${!!access_token ? access_token : ""}`)
        .then(res => res.data),
    uploadImportFile: (data, randomToken, file) => {
      const token = localStorage.tmServerToolJWT;
      const req = request.post(`${APIURL}/tm/tmx/upload`)
        .set({ Authorization: `Bearer ${token}` })
        .field('randomToken', randomToken)
        .field('type', 'import')
        .field('selectedTmRows', JSON.stringify(data))
        .attach('uploadedFiles', file)
        .promise()
        .then(res => res);
      return req;
    },
    fetchExportTmList: ({ type, refresh }) =>
      axios.get(`${APIURL}/tm/tmlist?type=${type}&isRefreshTmList=${refresh}`).then(res => res.data),
    exportTm: (data) =>
      axios.post(`${APIURLV2}/tms`, data).then(res => res.data),
    fetchExportProgress: ({ jobId, tmPath, exportToFtp, access_token }) =>
      axios.get(`${APIURLV2}/tms/job/${jobId}?tmpath=${tmPath}&access_token=${!!access_token ? access_token : ""}`,
        { responseType: !exportToFtp ? "blob" : "json" })
        .then(res => res),
    deleteExportTmInfoCache: ({ jobId, tmPath }) =>
      axios.delete(`${APIURLV2}/tms/job/${jobId}?tmpath=${tmPath}`)
        .then(res => res.data),
    fetchRenameTmList: () =>
      axios.get(`${APIURL}/tm/tmlist?type=rename`)
        .then(res => res.data),
    renameTm: (data) =>
      axios.put(`${APIURL}/tm/name`, (data))
        .then(res => res.data),
    uploadTmxFileForManipulation: (randomToken, file) => {
      const token = localStorage.tmServerToolJWT;
      const req = request.post(`${APIURL}/tm/tmx/upload`)
        .set({ Authorization: `Bearer ${token}` })
        .field('type', 'manipulate')
        .field('randomToken', randomToken)
        .attach('uploadedFiles', file)
        .promise()
        .then(res => res);
      return req;
    },
    tmxFilesManipulate: data =>
      axios.put(`${APIURL}/tm/tmx`, (data), { responseType: "blob" })
        .then(res => res)
  }
};
