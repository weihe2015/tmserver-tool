import * as types from '../../react/types';
import * as actions from '../../react/actions/auth';

describe('User actions', () => {
  test('should create an action to login user', () => {
    const data = {};
    const expectedAction = {
      type: types.USER_LOGGED_IN,
      data
    };
    expect(actions.userLoggedIn(data)).toEqual(expectedAction);
  });

  test('should create an action to logout user', () => {
    const expectedAction = {
      type: types.USER_LOGGED_OUT
    };
    expect(actions.userLoggedOut()).toEqual(expectedAction);
  });

  test('should create an action to connect remote TM Server', () => {
    const data = {};
    const expectedAction = {
      type: types.TM_SERVER_CONNECT,
      data
    };
    expect(actions.otherTmServerConnect(data)).toEqual(expectedAction);
  })
})