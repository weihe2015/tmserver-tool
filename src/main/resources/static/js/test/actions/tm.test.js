import * as types from '../../react/types';
import * as actions from '../../react/actions/tm';

describe('TM actions', () => {
  test('should create an action to fetch search TM list', () => {
    const data = {};
    const expectedAction = {
      type: types.SEARCH_TM_LIST_FETCH,
      data
    };
    expect(actions.searchTmListFetched(data)).toEqual(expectedAction);
  });

  test('should create an action to fetch concordance search result', () => {
    const data = {};
    const expectedAction = {
      type: types.CONCORDANCE_SEARCH_FETCH,
      data
    };
    expect(actions.concordanceSearchFetched(data)).toEqual(expectedAction);
  });

  test('should create an action to fetch organization list', () => {
    const data = {};
    const expectedAction = {
      type: types.ORGANIZATION_LIST_FETCH,
      data
    };
    expect(actions.organizationListFetched(data)).toEqual(expectedAction);
  });

  test('should create an action to fetch TM group list', () => {
    const data = {};
    const expectedAction = {
      type: types.TM_GROUP_LIST_FETCH,
      data
    };
    expect(actions.tmGroupListFetched(data)).toEqual(expectedAction);
  });

  test('should create an action to create TM String CSV', () => {
    const data = {};
    const expectedAction = {
      type: types.TM_STRING_CSV_CREATE,
      data
    };
    expect(actions.tmStringCsvCreated(data)).toEqual(expectedAction);
  });

  test('should create an action to create PD String CSV', () => {
    const data = {};
    const expectedAction = {
      type: types.PD_STRING_CSV_CREATE,
      data
    };
    expect(actions.pdStringCsvCreated(data)).toEqual(expectedAction);
  });

  test('should create an action to fetch language list', () => {
    const data = {};
    const expectedAction = {
      type: types.LANGUAGE_LIST_FETCH,
      data
    };
    expect(actions.languageListFetched(data)).toEqual(expectedAction);
  });

  test('should create an action to fetch generic username', () => {
    const data = {};
    const expectedAction = {
      type: types.GENERIC_USERNAME_FETCH,
      data
    };
    expect(actions.genericUsernameFetch(data)).toEqual(expectedAction);
  });

  test('should create an action to fetch generic user password', () => {
    const data = {};
    const expectedAction = {
      type: types.GENERIC_USERPASSWORD_FETCH,
      data
    };
    expect(actions.genericUserpasswordFetch(data)).toEqual(expectedAction);
  });

  test('should create an action to fetch cleanup passcode', () => {
    const data = {};
    const expectedAction = {
      type: types.CLEANUP_PASSCODE_FETCH,
      data
    };
    expect(actions.cleanupPasscodeFetch(data)).toEqual(expectedAction);
  });

  test('should create an action to fetch generic user role', () => {
    const data = {};
    const expectedAction = {
      type: types.GENERIC_USER_ROLE_FETCH,
      data
    };
    expect(actions.genericUserRoleFetch(data)).toEqual(expectedAction);
  });

  test('should create an action to validate cleanup passcode', () => {
    const data = {};
    const expectedAction = {
      type: types.CLEANUP_PASSCODE_VALIDATE,
      data
    };
    expect(actions.cleanupPasscodeValidate(data)).toEqual(expectedAction);
  });

  test('should create an action to validate TM group shortcode', () => {
    const data = {};
    const expectedAction = {
      type: types.TM_GROUP_SHORT_CODE_VALIDATE,
      data
    };
    expect(actions.tmGroupShortCodeValidate(data)).toEqual(expectedAction);
  });

  test('should create an action to create TM group', () => {
    const data = {};
    const expectedAction = {
      type: types.TM_GROUP_CREATION,
      data
    };
    expect(actions.tmGroupCreation(data)).toEqual(expectedAction);
  });

  test('should create an action to fetch TM Group code', () => {
    const data = {};
    const expectedAction = {
      type: types.TM_GROUP_SHORT_CODE_FETCH,
      data
    };
    expect(actions.tmGroupCodeFetched(data)).toEqual(expectedAction);
  });

  test('should create an action to create TM', () => {
    const data = {};
    const expectedAction = {
      type: types.TM_CREATION,
      data
    };
    expect(actions.tmCreation(data)).toEqual(expectedAction);
  });

  test('should create an action to create TM with CSV', () => {
    const data = {};
    const expectAction = {
      type: types.TM_CREATION_WITH_CSV,
      data
    };
    expect(actions.tmCreationWithCSV(data)).toEqual(expectAction);
  });

  test('should create an action to fetch import TM list', () => {
    const data = {};
    const expectAction = {
      type: types.IMPORT_TM_LIST_FETCH,
      data
    };
    expect(actions.importTmListFetch(data)).toEqual(expectAction);
  });

  test('should create an action to upload import TM files', () => {
    const data = {};
    const expectAction = {
      type: types.UPLOAD_IMPORT_FILE,
      data
    };
    expect(actions.importFileUpload(data)).toEqual(expectAction);
  });

  test('should create an action to validate random token', () => {
    const data = {};
    const expectAction = {
      type: types.RANDOM_TOKEN_VALIDATE,
      data
    };
    expect(actions.randomTokenValidate(data)).toEqual(expectAction);
  });

  test('should create an action to import TM', () => {
    const data = {};
    const expectAction = {
      type: types.IMPORT_TM,
      data
    };
    expect(actions.tmImport(data)).toEqual(expectAction);
  });

  test('should create an action to fetch import TM progress', () => {
    const data = {};
    const expectAction = {
      type: types.IMPORT_PROGRESS_FETCH,
      data
    };
    expect(actions.importProgressFetch(data)).toEqual(expectAction);
  });

  test('should create an action to fetch export TM list', () => {
    const data = {};
    const expectAction = {
      type: types.EXPORT_TM_LIST_FETCH,
      data
    };
    expect(actions.exportTmListFetch(data)).toEqual(expectAction);
  });

  test('should create an action to export TM', () => {
    const data = {};
    const expectAction = {
      type: types.EXPORT_TM,
      data
    };
    expect(actions.tmExport(data)).toEqual(expectAction);
  });

  test('should create an action to fetch rename TM list', () => {
    const data = {};
    const expectAction = {
      type: types.RENAME_TM_LIST_FETCH,
      data
    };
    expect(actions.renameTmListFetch(data)).toEqual(expectAction);
  });

  test('should create an action to rename TM', () => {
    const data = {};
    const expectAction = {
      type: types.TM_RENAME,
      data
    };
    expect(actions.tmRename(data)).toEqual(expectAction);
  });

  test('should create an action to upload TMX file for manipulation', () => {
    const data = {};
    const expectAction = {
      type: types.TMX_FILES_FOR_MANIPULATION_UPLOAD,
      data
    };
    expect(actions.tmxFileForManipulationUpload(data)).toEqual(expectAction);
  });

  test('should create an action to manipulate TMX file', () => {
    const data = {};
    const expectAction = {
      type: types.TMX_FILES_MANIPULATE,
      data
    };
    expect(actions.tmxFilesManipulate(data)).toEqual(expectAction);
  });

  test('should create an action to fetch TM Server Version', () => {
    const data = {};
    const expectAction = {
      type: types.TM_SERVER_VERSION_FETCH,
      data
    };
    expect(actions.tmServerVersionFetch(data)).toEqual(expectAction);
  });

  test('should create an action to clear TM Server Tool TM Info', () => {
    const expectAction = {
      type: types.TM_SERVER_TOOL_TM_INFO_CLEAR
    };
    expect(actions.clearTmServerToolTmInfo()).toEqual(expectAction);
  });

})