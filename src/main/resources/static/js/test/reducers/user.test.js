import * as types from '../../react/types';
import userReducer from '../../react/reducers/user';

describe('User reducer', () => {
  test('should return the initial state', () => {
    expect(userReducer(undefined, {})).toEqual({});
  });

  const data = {}
  const emptyData = {};

  test('should handle USER_LOGGED_IN', () => {
    expect(userReducer(undefined, { emptyData, type: types.USER_LOGGED_IN })).toEqual(data);
  });

  test('should handle TM_SERVER_CONNECT', () => {
    expect(userReducer(undefined, { emptyData, type: types.TM_SERVER_CONNECT })).toEqual(data);
  });

  test('should handle USER_LOGGED_OUT', () => {
    expect(userReducer(undefined, { emptyData, type: types.TM_SERVER_CONNECT })).toEqual(data);
  });

});
