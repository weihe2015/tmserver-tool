import * as types from '../../react/types';
import tmReducer from '../../react/reducers/tm';

describe('TM reducer', () => {
  test('should return the initial state', () => {
    expect(tmReducer(undefined, {})).toEqual({});
  });

  const data = {};
  const emptyData = {};

  test('should handle SEARCH_TM_LIST_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.SEARCH_TM_LIST_FETCH })).toEqual(data);
  });

  test('should handle CONCORDANCE_SEARCH_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.CONCORDANCE_SEARCH_FETCH })).toEqual(data);
  });

  test('should handle ORGANIZATION_LIST_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.ORGANIZATION_LIST_FETCH })).toEqual(data);
  });

  test('should handle TM_GROUP_LIST_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_GROUP_LIST_FETCH })).toEqual(data);
  });

  test('should handle TM_STRING_CSV_CREATE', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_STRING_CSV_CREATE })).toEqual(data);
  });

  test('should handle PD_STRING_CSV_CREATE', () => {
    expect(tmReducer(undefined, { emptyData, type: types.PD_STRING_CSV_CREATE })).toEqual(data);
  });

  test('should handle LANGUAGE_LIST_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.LANGUAGE_LIST_FETCH })).toEqual(data);
  });

  test('should handle GENERIC_USERPASSWORD_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.GENERIC_USERPASSWORD_FETCH })).toEqual(data);
  });

  test('should handle CLEANUP_PASSCODE_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.CLEANUP_PASSCODE_FETCH })).toEqual(data);
  });

  test('should handle GENERIC_USERNAME_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.GENERIC_USERNAME_FETCH })).toEqual(data);
  });

  test('should handle GENERIC_USER_ROLE_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.GENERIC_USER_ROLE_FETCH })).toEqual(data);
  });

  test('should handle CLEANUP_PASSCODE_VALIDATE', () => {
    expect(tmReducer(undefined, { emptyData, type: types.CLEANUP_PASSCODE_VALIDATE })).toEqual(data);
  });

  test('should handle TM_GROUP_SHORT_CODE_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_GROUP_SHORT_CODE_FETCH })).toEqual(data);
  });

  test('should handle TM_GROUP_SHORT_CODE_VALIDATE', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_GROUP_SHORT_CODE_VALIDATE })).toEqual(data);
  });

  test('should handle TM_GROUP_CREATION', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_GROUP_CREATION })).toEqual(data);
  });

  test('should handle TM_CREATION', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_CREATION })).toEqual(data);
  });

  test('should handle TM_CREATION_WITH_CSV', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_CREATION_WITH_CSV })).toEqual(data);
  });

  test('should handle IMPORT_TM_LIST_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.IMPORT_TM_LIST_FETCH })).toEqual(data);
  });

  test('should handle UPLOAD_IMPORT_FILE', () => {
    expect(tmReducer(undefined, { emptyData, type: types.UPLOAD_IMPORT_FILE })).toEqual(data);
  });

  test('should handle RANDOM_TOKEN_VALIDATE', () => {
    expect(tmReducer(undefined, { emptyData, type: types.RANDOM_TOKEN_VALIDATE })).toEqual(data);
  });

  test('should handle IMPORT_TM', () => {
    expect(tmReducer(undefined, { emptyData, type: types.IMPORT_TM })).toEqual(data);
  });

  test('should handle IMPORT_PROGRESS_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.IMPORT_PROGRESS_FETCH })).toEqual(data);
  });

  test('should handle EXPORT_TM_LIST_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.EXPORT_TM_LIST_FETCH })).toEqual(data);
  });

  test('should handle EXPORT_TM', () => {
    expect(tmReducer(undefined, { emptyData, type: types.EXPORT_TM })).toEqual(data);
  });

  test('should handle RENAME_TM_LIST_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.RENAME_TM_LIST_FETCH })).toEqual(data);
  });

  test('should handle TM_RENAME', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_RENAME })).toEqual(data);
  });

  test('should handle TM_RENAME', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_RENAME })).toEqual(data);
  });

  test('should handle TMX_FILES_FOR_MANIPULATION_UPLOAD', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TMX_FILES_FOR_MANIPULATION_UPLOAD })).toEqual(data);
  });

  test('should handle TMX_FILES_MANIPULATE', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TMX_FILES_MANIPULATE })).toEqual(data);
  });

  test('should handle TM_SERVER_VERSION_FETCH', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_SERVER_VERSION_FETCH })).toEqual(data);
  });

  test('should handle TM_SERVER_TOOL_TM_INFO_CLEAR', () => {
    expect(tmReducer(undefined, { emptyData, type: types.TM_SERVER_TOOL_TM_INFO_CLEAR })).toEqual(data);
  });
});