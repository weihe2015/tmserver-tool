import { compareVersion, sortLocales, sortLanguages } from '../../react/utils/main';

describe('compare version function', () => {
  test('should compare version of 2.1 and 2.0', () => {
    const version1 = "2.1";
    const version2 = "2.2";
    expect(compareVersion(version1, version2)).toBe(-1);
  });

  test('should compare version of 2.4 and 2.0', () => {
    const version1 = "2.4";
    const version2 = "2.0";
    expect(compareVersion(version1, version2)).toBe(1);
  });

  test('should compare version of 2.5 and 2.5', () => {
    const version1 = "2.5";
    const version2 = "2.5";
    expect(compareVersion(version1, version2)).toBe(0);
  });

  test('should compare version of 2.1 and 2.2-RC1', () => {
    const version1 = "2.1";
    const version2 = "2.2-RC1";
    expect(compareVersion(version1, version2)).toBe(-1);
  });

  test('should compare version of 2.1 and 2.2-SNAPSHOT', () => {
    const version1 = "2.1";
    const version2 = "2.2-SNAPSHOT";
    expect(compareVersion(version1, version2)).toBe(-1);
  });

  test('should compare version of 2.2 and 2.2-SNAPSHOT', () => {
    const version1 = "2.2";
    const version2 = "2.2-SNAPSHOT";
    expect(compareVersion(version1, version2)).toBe(0);
  });
});

describe('Sort locales function', () => {
  let tmList = [
    {
      'sourceLocale': 'en-US',
      'targetLocale': 'de-DE'
    }, 
    {
      'sourceLocale': 'en-US',
      'targetLocale': 'zh-CN'
    },
    {
      'sourceLocale': 'en-US',
      'targetLocale': 'ja-JP'
    },
    {
      'sourceLocale': 'de-DE',
      'targetLocale': 'ja-JP'
    },
    {
      'sourceLocale': 'en-US',
      'targetLocale': 'fr-FR'
    }];
  
    let sortedTmList = {
      "sourceLocales": ["de-DE", "en-US"],
      "targetLocales": ["de-DE", "fr-FR", "ja-JP", "zh-CN"]
    }
  
    let incorrectSortedTmList = {
      "sourceLocales": ["en-US", "de-DE"],
      "targetLocales": ["de-DE", "ja-JP", "zh-CN", "fr-FR"]
    }
  
    test('should sort tmList', () => {
      expect(sortLocales(tmList)).toEqual(sortedTmList);
    });
  
    test('should sort tmList but not equal', () => {
      expect(sortLocales(tmList)).not.toEqual(incorrectSortedTmList);
    });
})

describe('Sort languages function', () => {
  let tmList = [
  {
    'Source Language': 'English (United States)',
    'Target Language': 'German (Gemany)'
  }, 
  {
    'Source Language': 'English (United States)',
    'Target Language': 'Chinese (China)'
  },
  {
    'Source Language': 'English (United States)',
    'Target Language': 'Japanese (Japan)'
  },
  {
    'Source Language': 'German (Gemany)',
    'Target Language': 'Japanese (Japan)'
  },
  {
    'Source Language': 'English (United States)',
    'Target Language': 'French (France)'
  }];

  let sortedTmList = {
    "sourceLanguages": ["English (United States)", "German (Gemany)"],
    "targetLanguages": ["Chinese (China)", "French (France)", "German (Gemany)", "Japanese (Japan)", ]
  }

  let incorrectSortedTmList = {
    "sourceLanguages": ["German (Gemany)", "English (United States)"],
    "targetLanguages": ["Chinese (China)", "German (Gemany)", "Japanese (Japan)", "French (France)"]
  }

  test('should sort tmList', () => {
    expect(sortLanguages(tmList)).toEqual(sortedTmList);
  });

  test('should sort tmList but not equal', () => {
    expect(sortLanguages(tmList)).not.toEqual(incorrectSortedTmList);
  });
})