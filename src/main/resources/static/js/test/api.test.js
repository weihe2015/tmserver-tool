import api from '../react/api';
import mockAxios from "axios";

describe('Testing axios APIs', () => {
  test('login POST', async () => {
    const data = {
      username: 'test',
      password: 'test'
    }
    const res = await api.user.login(data);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  })
})