import React from 'react';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';
import { ConcordancePage } from '../../react/components/pages/ConcordancePage';
import configureMockStore from "redux-mock-store";

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares);
const initialState = {};
const store = mockStore(initialState);

describe('Concordance Search Page', () => {
  let fetchConcordanceSearchResult = jest.fn();
  const props = {
    fetchConcordanceSearchResult
  }
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(
      <ConcordancePage store={store} {...props}/>
    );
  });

  test('should render Concordance Search Page', () => {
    expect(wrapper.exists()).toBe(true);
  });

  test('should contains <Segment />', () => {
    expect(wrapper.find('Segment')).toHaveLength(1);
  });

  test('should not contains <ReactTable />', () => {
    expect(wrapper.find('ReactTable')).toHaveLength(0);
  })

})