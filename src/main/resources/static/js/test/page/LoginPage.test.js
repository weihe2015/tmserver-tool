import React from 'react';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { shallow, mount } from 'enzyme';
import LoginPage from '../../react/components/pages/LoginPage';
import configureMockStore from "redux-mock-store";

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares);
const store = mockStore({});

describe("Login Component Page", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(
      <Provider store={store} >
        <LoginPage />
      </Provider>
    );
  });

  test('should render Login Page', () => {
    expect(wrapper.exists()).toBe(true);
  });

});