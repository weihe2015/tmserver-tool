package org.gs4tr.tmserver.tool;

import static org.assertj.core.api.Assertions.assertThat;

import org.gs4tr.tmserver.tool.controller.mvc.TmConcordanceController;
import org.gs4tr.tmserver.tool.controller.mvc.TmCreationController;
import org.gs4tr.tmserver.tool.controller.mvc.TmExportController;
import org.gs4tr.tmserver.tool.controller.mvc.TmImportController;
import org.gs4tr.tmserver.tool.controller.mvc.TmRenameController;
import org.gs4tr.tmserver.tool.controller.mvc.UserController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TmserverToolApplicationTests
{
	@Autowired
	private TmConcordanceController tmConcordanceController;

	@Autowired
	private TmCreationController tmCreationController;

	@Autowired
	private TmExportController tmExportController;

	@Autowired
	private TmImportController tmImportController;

	@Autowired
	private TmRenameController tmRenameController;

	@Autowired
	private UserController userController;

	@Test
	public void contextLoads()
	{
		assertThat(tmConcordanceController).isNotNull();
		assertThat(tmCreationController).isNotNull();
		assertThat(tmExportController).isNotNull();
		assertThat(tmImportController).isNotNull();
		assertThat(tmRenameController).isNotNull();
		assertThat(userController).isNotNull();
	}

}
