package org.gs4tr.tmserver.tool.wf;

import static org.junit.Assert.assertEquals;

import org.gs4tr.tmserver.tool.service.tmx.WfTxtLocale;
import org.junit.Test;

public class WfTxtLocaleTest
{
	static public final String TESTFILE1 = "target/test-classes/451_Research_ENUS-FRFR.txt";
	static public final String TESTFILE2 = "target/test-classes/451_Research_ENUS-ITIT.txt";
	static public final String TESTFILE3 = "target/test-classes/a2elite_EN-ZHCN.txt";

	@Test
	public void WfTxtLocaleTest1() throws Exception
	{
		WfTxtLocale wfTxtLocale = new WfTxtLocale(TESTFILE1);
		assertEquals("en-us", wfTxtLocale.getSourceLocaleInLowerCase());
		assertEquals("fr-fr", wfTxtLocale.getTargetLocaleInLowerCase());
	}

	@Test
	public void WfTxtLocaleTest2() throws Exception
	{
		WfTxtLocale wfTxtLocale = new WfTxtLocale(TESTFILE2);
		assertEquals("en-us", wfTxtLocale.getSourceLocaleInLowerCase());
		assertEquals("it-it", wfTxtLocale.getTargetLocaleInLowerCase());
	}

	@Test
	public void WfTxtLocaleTest3() throws Exception
	{
		WfTxtLocale wfTxtLocale = new WfTxtLocale(TESTFILE3);
		assertEquals("en", wfTxtLocale.getSourceLocaleInLowerCase());
		assertEquals("zh-cn", wfTxtLocale.getTargetLocaleInLowerCase());
	}
}
