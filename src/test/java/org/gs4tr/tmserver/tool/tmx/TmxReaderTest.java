package org.gs4tr.tmserver.tool.tmx;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.service.tmx.TmxLocale;
import org.gs4tr.tmserver.tool.service.tmx.XmlPreDefinedEntityFixer;
import org.junit.Assert;
import org.junit.Test;

public class TmxReaderTest
{
    @Test
    public void TmxReaderTest1() throws Exception
    {
        String tmxFilePath1 = "target/test-classes/UTF-8/ab.com_CS-CZ.tmx";
        File tmxFile1 = new File(tmxFilePath1);
        File tempFile = File.createTempFile("temptmx", ".tmp");
        XmlPreDefinedEntityFixer fixer = new XmlPreDefinedEntityFixer(tmxFile1, tempFile);
        fixer.clean();
        FileUtils.copyFile(tempFile, tmxFile1);
        FileUtils.deleteQuietly(tempFile);

        String sourceLocaleCode = "";
        try
        {
            TmxLocale tmxFileLocale = new TmxLocale(tmxFilePath1);
            sourceLocaleCode = tmxFileLocale.getSourceLocale();
        }
        catch (Exception ex)
        {

        }
        Assert.assertTrue(StringUtils.isNotEmpty(sourceLocaleCode));
        Assert.assertEquals("EN-US", sourceLocaleCode);

        String tmxFilePath2 = "target/test-classes/UTF-8/test8.tmx";
        File tmxFile2 = new File(tmxFilePath2);
        tempFile = File.createTempFile("temptmx", ".tmp");
        fixer = new XmlPreDefinedEntityFixer(tmxFile2, tempFile);
        fixer.clean();
        FileUtils.copyFile(tempFile, tmxFile2);
        FileUtils.deleteQuietly(tempFile);

        sourceLocaleCode = "";
        try
        {
            TmxLocale tmxFileLocale = new TmxLocale(tmxFilePath2);
            sourceLocaleCode = tmxFileLocale.getSourceLocale();
        }
        catch (Exception ex)
        {

        }
        Assert.assertEquals("en-us", sourceLocaleCode);
    }
}
