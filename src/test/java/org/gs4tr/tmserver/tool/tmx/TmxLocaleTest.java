package org.gs4tr.tmserver.tool.tmx;

import static org.junit.Assert.assertEquals;

import org.gs4tr.tmserver.tool.service.tmx.TmxLocale;
import org.junit.Test;

public class TmxLocaleTest
{
	static public final String TESTFILE1 = "target/test-classes/Sterling_IRB_EN-AR.tmx";
	static public final String TESTFILE2 = "target/test-classes/Test_TM_import_en-us_fr-FR.tmx";
	static public final String LARGETESTFILE = "target/test-classes/Merck_Non-Clinical_ENUS-BGBG.tmx";

	@Test
	public void LargeTmxLocaleTest() throws Exception
	{
		TmxLocale tmxFileLocale = new TmxLocale(LARGETESTFILE);
		assertEquals("en-us", tmxFileLocale.getSourceLocaleInLowerCase());
		assertEquals("bg-bg", tmxFileLocale.getTargetLocaleInLowerCase());
	}

	@Test
	public void TmxLocaleTest1() throws Exception
	{
		TmxLocale tmxFileLocale = new TmxLocale(TESTFILE1);
		assertEquals("en", tmxFileLocale.getSourceLocaleInLowerCase());
		assertEquals("ar", tmxFileLocale.getTargetLocaleInLowerCase());
	}

	@Test
	public void TmxLocaleTest2() throws Exception
	{
		TmxLocale tmxFileLocale = new TmxLocale(TESTFILE2);
		assertEquals("en-us", tmxFileLocale.getSourceLocaleInLowerCase());
		assertEquals("fr-fr", tmxFileLocale.getTargetLocaleInLowerCase());
	}
}
