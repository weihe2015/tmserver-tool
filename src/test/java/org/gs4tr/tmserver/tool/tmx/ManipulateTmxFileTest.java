package org.gs4tr.tmserver.tool.tmx;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.gs4tr.tmserver.tool.controller.model.TmxManipulateOption;
import org.gs4tr.tmserver.tool.model.tmx.NewAttribute;
import org.gs4tr.tmserver.tool.model.xml.Attribute;
import org.gs4tr.tmserver.tool.model.xml.Document;
import org.gs4tr.tmserver.tool.model.xml.Element;
import org.gs4tr.tmserver.tool.model.xml.Node;
import org.gs4tr.tmserver.tool.model.xml.parser.Parser;
import org.gs4tr.tmserver.tool.service.tmx.ManipulateTmxFile;
import org.junit.Test;

public class ManipulateTmxFileTest
{
    static public final String TESTFILE = "target/test-classes/Eaton_ASiM_en-US_fr-FR_16-04-2018-102422.tmx";
    static public final String TARGETTESTFILE = "target/test-classes/test_modified.tmx";

    static public final String TESTFILE1 = "target/test-classes/MJK-Danish-TM.tmx";
    static public final String TARGETTESTFILE1 = "target/test-classes/MJK-Danish-TM_output.tmx";

    static public final String TESTFILE2 = "target/test-classes/WP2_0006_zh_CN_TM.tmx";
    static public final String TARGETTESTFILE2 = "target/test-classes/WP2_0006_zh_CN_TM_output.tmx";

    static public final String TESTFILE3 = "target/test-classes/whiteSpaces.tmx";
    static public final String TARGETTESTFILE3 = "target/test-classes/whiteSpaces_output.tmx";

    static public final String TESTFILE4 = "target/test-classes/Corsair_Master.tmx";
    static public final String TARGETTESTFILE4 = "target/test-classes/Corsair_Master_output.tmx";

    static public final String TESTFILE5 = "target/test-classes/awsDocDEDE1.tmx";
    static public final String TARGETTESTFILE5 = "target/test-classes/awsDocDEDE1_output.tmx";

    @Test
    public void addAttributesWithKeepingPreviousAttributesTest() throws Exception
    {
        if (new File(TARGETTESTFILE).exists())
        {
            new File(TARGETTESTFILE).delete();
        }
        assertTrue(new File(TESTFILE).exists());

        List<NewAttribute> newAttributes = new ArrayList<NewAttribute>();
        NewAttribute newAttribute = new NewAttribute();
        newAttribute.setKey("bu");
        newAttribute.setValue("electrical");
        newAttribute.setHide(false);
        newAttributes.add(newAttribute);

        TmxManipulateOption option = new TmxManipulateOption();
        option.setNewAttributes(newAttributes);
        option.setRemovePreviousAttribute(false);
        option.setAddFilenameAsAttribute(false);
        option.setRemoveTrailingSpaces(false);
        option.setRandomToken("ahd21");

        ManipulateTmxFile manipulate = new ManipulateTmxFile(TESTFILE, TARGETTESTFILE, option);
        manipulate.process();

        assertTrue(new File(TARGETTESTFILE).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(TARGETTESTFILE);
            Document document = new Parser().parse(inputReader);
            List<Node> nodeList = document.selectNodes("//tu/prop");
            assertTrue(nodeList.size() > 0);

            Element ele = (Element) nodeList.get(0);
            assertEquals("prop", ele.getName().getName());

            List<Attribute> attributeList = ele.attributes();
            assertTrue(attributeList.size() > 0);
            Attribute attribute = attributeList.get(0);
            assertEquals("type", attribute.getName().getName());
            assertEquals("filename", attribute.getValue().getText());

            ele = (Element) nodeList.get(1);
            assertEquals("prop", ele.getName().getName());

            attributeList = ele.attributes();
            assertTrue(attributeList.size() > 0);
            attribute = attributeList.get(0);
            assertEquals("type", attribute.getName().getName());
            assertEquals("bu", attribute.getValue().getText());
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
            if (new File(TARGETTESTFILE).exists())
            {
                new File(TARGETTESTFILE).delete();
            }
        }
    }

    /**
     * Test new manipulate Tmx option: remove trailing spaces from both source and target
     */
    @Test
    public void noNewAttributeAddedTest() throws Exception
    {
        if (new File(TARGETTESTFILE1).exists())
        {
            new File(TARGETTESTFILE1).delete();
        }
        assertTrue(new File(TESTFILE1).exists());

        List<NewAttribute> newAttributes = new ArrayList<NewAttribute>();
        NewAttribute newAttribute = new NewAttribute();
        newAttribute.setKey("");
        newAttribute.setValue("");
        newAttribute.setHide(false);
        newAttributes.add(newAttribute);

        TmxManipulateOption option = new TmxManipulateOption();
        option.setNewAttributes(newAttributes);
        option.setRemovePreviousAttribute(true);
        option.setAddFilenameAsAttribute(false);
        option.setRemoveTrailingSpaces(true);
        option.setRandomToken("ah8121");

        ManipulateTmxFile manipulate = new ManipulateTmxFile(TESTFILE1, TARGETTESTFILE1, option);
        manipulate.process();

        assertTrue(new File(TARGETTESTFILE1).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(TARGETTESTFILE1);
            Document document = new Parser().parse(inputReader);
            List<Node> nodeList = document.selectNodes("//tu/tuv/prop");
            assertTrue(nodeList.size() == 0);
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
            if (new File(TARGETTESTFILE1).exists())
            {
                new File(TARGETTESTFILE1).delete();
            }
        }
    }

    /**
     * Test to remove white spaces and other zero-space whitespace: Unit Test for TMS-TOOL-23
     */
    @Test
    public void removeNewWhiteSpacesTest() throws Exception
    {
        if (new File(TARGETTESTFILE3).exists())
        {
            new File(TARGETTESTFILE3).delete();
        }
        assertTrue(new File(TESTFILE3).exists());

        List<NewAttribute> newAttributes = new ArrayList<NewAttribute>();
        NewAttribute newAttribute = new NewAttribute();
        newAttribute.setKey("product");
        newAttribute.setValue("MJK");
        newAttribute.setHide(false);
        newAttributes.add(newAttribute);

        TmxManipulateOption option = new TmxManipulateOption();
        option.setNewAttributes(newAttributes);
        option.setRemovePreviousAttribute(true);
        option.setAddFilenameAsAttribute(false);
        option.setRemoveTrailingSpaces(true);
        option.setRandomToken("rw15x");

        ManipulateTmxFile manipulate = new ManipulateTmxFile(TESTFILE3, TARGETTESTFILE3, option);
        manipulate.process();

        assertTrue(new File(TARGETTESTFILE3).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(TARGETTESTFILE3);
            Document document = new Parser().parse(inputReader);
            List<Node> nodeList = document.selectNodes("//tu/tuv/seg");
            assertTrue(nodeList.size() > 0);

            Element ele = (Element) nodeList.get(0);
            assertEquals("seg", ele.getName().getName());

            List<Node> content = ele.content();
            assertEquals(
                "The package consists of 30 ft/50 ft (9 m/15 m) power cord with " +
                    "piggy-back power plug, variable-length float tether and sealed float.",
                content.get(0).asXML());

            ele = (Element) nodeList.get(1);
            assertEquals("seg", ele.getName().getName());
            content = ele.content();
            assertEquals("该包装包括30英尺/50英尺（9米/15米）的电源线、背负式电源插头、可变长度的浮动系绳以及密封浮子。", content.get(0).asXML());

            ele = (Element) nodeList.get(2);
            assertEquals("seg", ele.getName().getName());
            content = ele.content();
            assertEquals(
                "Impeller and cutter plate are made of Hard-Iron™ and Active Seal™ for long service intervals.",
                content.get(0).asXML());

            ele = (Element) nodeList.get(3);
            assertEquals("seg", ele.getName().getName());
            content = ele.content();
            assertEquals("叶轮和刀盘采用Hard-Iron™和Active Seal™制成，维修周期长。", content.get(0).asXML());
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
            if (new File(TARGETTESTFILE3).exists())
            {
                new File(TARGETTESTFILE3).delete();
            }
        }
    }

    /**
     * Test remove trailing spaces from both source and target and remove previous attribute.
     */
    @Test
    public void removeTrailingSpacesAndPreviousAttributeTest() throws Exception
    {
        if (new File(TARGETTESTFILE2).exists())
        {
            new File(TARGETTESTFILE2).delete();
        }
        assertTrue(new File(TESTFILE2).exists());

        List<NewAttribute> newAttributes = new ArrayList<NewAttribute>();
        NewAttribute newAttribute = new NewAttribute();
        newAttribute.setKey("product");
        newAttribute.setValue("MJK");
        newAttribute.setHide(true);
        newAttributes.add(newAttribute);

        TmxManipulateOption option = new TmxManipulateOption();
        option.setNewAttributes(newAttributes);
        option.setRemovePreviousAttribute(true);
        option.setAddFilenameAsAttribute(false);
        option.setRemoveTrailingSpaces(true);
        option.setRandomToken("ata95");

        ManipulateTmxFile manipulate = new ManipulateTmxFile(TESTFILE2, TARGETTESTFILE2, option);
        manipulate.process();

        assertTrue(new File(TARGETTESTFILE2).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(TARGETTESTFILE2);
            Document document = new Parser().parse(inputReader);
            List<Node> nodeList = document.selectNodes("//tu/tuv/seg");
            assertTrue(nodeList.size() > 0);

            Element ele = (Element) nodeList.get(0);
            assertEquals("seg", ele.getName().getName());

            List<Node> content = ele.content();

            assertEquals("Triplex Hi-Pressure motor driven diaphragm pump", content.get(0).asXML());

            ele = (Element) nodeList.get(4);
            assertEquals("seg", ele.getName().getName());

            content = ele.content();
            assertEquals("Model No.:", content.get(0).asXML());
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
            if (new File(TARGETTESTFILE2).exists())
            {
                new File(TARGETTESTFILE2).delete();
            }
        }
    }

    /**
     * Test new manipulate Tmx option: remove trailing spaces from both source and target
     */
    @Test
    public void removeTrailingSpacesOptionTest() throws Exception
    {
        if (new File(TARGETTESTFILE1).exists())
        {
            new File(TARGETTESTFILE1).delete();
        }
        assertTrue(new File(TESTFILE1).exists());

        List<NewAttribute> newAttributes = new ArrayList<NewAttribute>();
        NewAttribute newAttribute = new NewAttribute();
        newAttribute.setKey("product");
        newAttribute.setValue("MJK");
        newAttribute.setHide(false);
        newAttributes.add(newAttribute);

        TmxManipulateOption option = new TmxManipulateOption();
        option.setNewAttributes(newAttributes);
        option.setRemovePreviousAttribute(true);
        option.setAddFilenameAsAttribute(false);
        option.setRemoveTrailingSpaces(true);
        option.setRandomToken("ahw21");

        ManipulateTmxFile manipulate = new ManipulateTmxFile(TESTFILE1, TARGETTESTFILE1, option);
        manipulate.process();

        assertTrue(new File(TARGETTESTFILE1).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(TARGETTESTFILE1);
            Document document = new Parser().parse(inputReader);
            List<Node> nodeList = document.selectNodes("//tu/tuv/seg");
            assertTrue(nodeList.size() > 0);

            Element ele = (Element) nodeList.get(1);
            assertEquals("seg", ele.getName().getName());

            List<Node> content = ele.content();

            assertEquals("tekst/microsoft-resx", content.get(0).asXML());

            ele = (Element) nodeList.get(5);
            assertEquals("seg", ele.getName().getName());

            content = ele.content();
            assertEquals(
                "System.Resources.ResXResourceReader, " +
                    "System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089",
                content.get(0).asXML());
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
            if (new File(TARGETTESTFILE1).exists())
            {
                new File(TARGETTESTFILE1).delete();
            }
        }
    }

    /**
     * Test new manipulate Tmx option: add prev and next Checksum to TU tag.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void addPrevNextChecksumOptionTest() throws Exception
    {
        if (new File(TARGETTESTFILE4).exists())
        {
            new File(TARGETTESTFILE4).delete();
        }
        assertTrue(new File(TESTFILE4).exists());

        TmxManipulateOption option = new TmxManipulateOption();
        option.setRemovePreviousAttribute(true);
        option.setAddFilenameAsAttribute(false);
        option.setInsertChecksum(true);
        option.setRemoveTrailingSpaces(false);
        option.setInsertChecksumInAllTus(true);
        option.setRandomToken("wh121");

        ManipulateTmxFile manipulate = new ManipulateTmxFile(TESTFILE4, TARGETTESTFILE4, option);
        manipulate.process();

        assertTrue(new File(TARGETTESTFILE4).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(TARGETTESTFILE4);
            Document document = new Parser().parse(inputReader);
            List<Node> tuNodes = document.selectNodes("//tu");
            assertEquals(tuNodes.size(), 3);

            // First TU:
            Element tu = (Element) tuNodes.get(0);
            List<Node> props = (List<Node>) tu.selectNodes(".//prop");
            assertEquals(3, props.size());
            Element prop = (Element) props.get(0);
            assertEquals("nextMd5Segment", prop.attribute("type").getValue().getText());

            prop = (Element) props.get(1);
            assertEquals("previousMd5Checksum", prop.attribute("type").getValue().getText());
            assertEquals("0", prop.getText());

            prop = (Element) props.get(2);
            assertEquals("nextMd5Checksum", prop.attribute("type").getValue().getText());
            assertEquals("b15cca8897da7909ee3793a136acff9b", prop.getText());

            // Second TU:
            tu = (Element) tuNodes.get(1);
            props = (List<Node>) tu.selectNodes(".//prop");
            prop = (Element) props.get(0);
            assertEquals("previousMd5Checksum", prop.attribute("type").getValue().getText());
            assertEquals("6cfbbc63fb987671fdd0d7970c06ba02", prop.getText());

            prop = (Element) props.get(1);
            assertEquals("previousMd5Segment", prop.attribute("type").getValue().getText());

            prop = (Element) props.get(2);
            assertEquals("nextMd5Checksum", prop.attribute("type").getValue().getText());
            assertEquals("7ab27a4af5d835698a09c83ab010337c", prop.getText());

            prop = (Element) props.get(3);
            assertEquals("nextMd5Segment", prop.attribute("type").getValue().getText());

            // Last TU:
            tu = (Element) tuNodes.get(2);
            props = (List<Node>) tu.selectNodes(".//prop");
            prop = (Element) props.get(0);
            assertEquals("previousMd5Segment", prop.attribute("type").getValue().getText());

            prop = (Element) props.get(1);
            assertEquals("previousMd5Checksum", prop.attribute("type").getValue().getText());
            assertEquals("b15cca8897da7909ee3793a136acff9b", prop.getText());

            prop = (Element) props.get(2);
            assertEquals("nextMd5Checksum", prop.attribute("type").getValue().getText());
            assertEquals("0", prop.getText());
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
        }
    }

    /**
     * Unit Test of TMSTool-85 move props from TUV to Tu
     */
    @SuppressWarnings("unchecked")
    @Test
    public void movePropsToTuOptionTest() throws Exception
    {
        if (new File(TARGETTESTFILE5).exists())
        {
            new File(TARGETTESTFILE5).delete();
        }
        assertTrue(new File(TESTFILE5).exists());

        TmxManipulateOption option = new TmxManipulateOption();
        option.setRemovePreviousAttribute(true);
        option.setMoveAttributesToTu(true);
        option.setAddFilenameAsAttribute(false);
        option.setInsertChecksum(false);
        option.setRemoveTrailingSpaces(false);
        option.setInsertChecksumInAllTus(false);
        option.setRandomToken("wh521");

        ManipulateTmxFile manipulate = new ManipulateTmxFile(TESTFILE5, TARGETTESTFILE5, option);
        manipulate.process();

        assertTrue(new File(TARGETTESTFILE5).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(TARGETTESTFILE5);
            Document document = new Parser().parse(inputReader);
            List<Node> tuNodes = document.selectNodes("//tu");
            assertEquals(tuNodes.size(), 4);

            // First TU:
            Element tu = (Element) tuNodes.get(0);
            List<Node> props = (List<Node>) tu.selectNodes(".//prop");
            assertEquals(1, props.size());
            Element prop = (Element) props.get(0);
            assertEquals("aligned", prop.attribute("type").getValue().getText());
            assertEquals("false", prop.getText());

            // Second TU:
            tu = (Element) tuNodes.get(1);
            props = (List<Node>) tu.selectNodes(".//prop");
            assertEquals(4, props.size());
            prop = (Element) props.get(0);
            assertEquals("context_prev", prop.attribute("type").getValue().getText());
            assertEquals("For more information about how {3} queues " +
                "work and how to get started using them, see {4}.", prop.getText());

            prop = (Element) props.get(1);
            assertEquals("context_next", prop.attribute("type").getValue().getText());
            assertEquals("{1} queues are available {2} For more information about " +
                "how {3} queues work and how to get started using them, see {4}.", prop.getText());

            prop = (Element) props.get(2);
            assertEquals("aligned", prop.attribute("type").getValue().getText());
            assertEquals("false", prop.getText());

            prop = (Element) props.get(3);
            assertEquals("filename", prop.attribute("type").getValue().getText());
            assertEquals("AWSQueueDocs_-_latest_-_DeveloperGuide_-_chapter-document-history.xml", prop.getText());

            // Last TU:
            tu = (Element) tuNodes.get(2);
            props = (List<Node>) tu.selectNodes(".//prop");
            assertEquals(1, props.size());
            prop = (Element) props.get(0);
            assertEquals("aligned", prop.attribute("type").getValue().getText());
            assertEquals("false", prop.getText());
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
        }
    }

    @Test
    public void detectAndOmitInvalidTuTest() throws Exception
    {
        String testFile = "target/test-classes/invalidTu2.tmx";
        String targetFile = "target/test-classes/invalidTu2_output.tmx";
        if (new File(targetFile).exists())
        {
            new File(targetFile).delete();
        }
        assertTrue(new File(testFile).exists());

        TmxManipulateOption option = new TmxManipulateOption();
        option.setRemovePreviousAttribute(true);
        option.setMoveAttributesToTu(true);
        option.setAddFilenameAsAttribute(false);
        option.setInsertChecksum(true);
        option.setRemoveTrailingSpaces(false);
        option.setInsertChecksumInAllTus(false);
        option.setRandomToken("wh521");

        ManipulateTmxFile manipulate = new ManipulateTmxFile(testFile, targetFile, option);
        manipulate.process();
        assertTrue(new File(targetFile).exists());

        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(targetFile);
            Document document = new Parser().parse(inputReader);
            List<Node> tuNodes = document.selectNodes("//tu");
            assertEquals(tuNodes.size(), 2);

        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
        }
    }

    /**
     * Unit Test of TMSTool-85 move props from TUV to Tu
     */
    @SuppressWarnings("unchecked")
    @Test
    public void movePropsToTuAndInsertChecksumOptionsTest() throws Exception
    {
        if (new File(TARGETTESTFILE5).exists())
        {
            new File(TARGETTESTFILE5).delete();
        }
        assertTrue(new File(TESTFILE5).exists());

        TmxManipulateOption option = new TmxManipulateOption();
        option.setRemovePreviousAttribute(true);
        option.setMoveAttributesToTu(true);
        option.setAddFilenameAsAttribute(false);
        option.setInsertChecksum(true);
        option.setRemoveTrailingSpaces(false);
        option.setInsertChecksumInAllTus(false);
        option.setRandomToken("wh521");

        ManipulateTmxFile manipulate = new ManipulateTmxFile(TESTFILE5, TARGETTESTFILE5, option);
        manipulate.process();

        assertTrue(new File(TARGETTESTFILE5).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(TARGETTESTFILE5);
            Document document = new Parser().parse(inputReader);
            List<Node> tuNodes = document.selectNodes("//tu");
            assertEquals(tuNodes.size(), 4);

            // First TU:
            Element tu = (Element) tuNodes.get(0);
            List<Node> props = (List<Node>) tu.selectNodes(".//prop");
            assertEquals(1, props.size());
            Element prop = (Element) props.get(0);
            assertEquals("aligned", prop.attribute("type").getValue().getText());
            assertEquals("false", prop.getText());

            // Second TU:
            tu = (Element) tuNodes.get(1);
            props = (List<Node>) tu.selectNodes(".//prop");
            assertEquals(6, props.size());

            prop = (Element) props.get(0);
            assertEquals("aligned", prop.attribute("type").getValue().getText());
            assertEquals("false", prop.getText());

            prop = (Element) props.get(1);
            assertEquals("filename", prop.attribute("type").getValue().getText());
            assertEquals("AWSQueueDocs_-_latest_-_DeveloperGuide_-_chapter-document-history.xml", prop.getText());

            prop = (Element) props.get(2);
            assertEquals("previousMd5Checksum", prop.attribute("type").getValue().getText());
            assertEquals("0b47d54010a32776175cb4e74b90e815", prop.getText());

            prop = (Element) props.get(3);
            assertEquals("previousMd5Segment", prop.attribute("type").getValue().getText());
            assertEquals("For more information about how {3} queues " +
                "work and how to get started using them, see {4}.", prop.getText());

            prop = (Element) props.get(4);
            assertEquals("nextMd5Checksum", prop.attribute("type").getValue().getText());
            assertEquals("33e8371749c7b5413ecb2e95fe70e2eb", prop.getText());

            prop = (Element) props.get(5);
            assertEquals("nextMd5Segment", prop.attribute("type").getValue().getText());
            assertEquals("{1} queues are available {2} For more information about " +
                "how {3} queues work and how to get started using them, see {4}.", prop.getText());

            // Third TU:
            tu = (Element) tuNodes.get(2);
            props = (List<Node>) tu.selectNodes(".//prop");
            assertEquals(1, props.size());
            prop = (Element) props.get(0);
            assertEquals("aligned", prop.attribute("type").getValue().getText());
            assertEquals("false", prop.getText());

            // Last TU:
            tu = (Element) tuNodes.get(3);
            props = (List<Node>) tu.selectNodes(".//prop");
            assertEquals(3, props.size());
            prop = (Element) props.get(0);
            assertEquals("mdata", prop.attribute("type").getValue().getText());

            prop = (Element) props.get(1);
            assertEquals("aligned", prop.attribute("type").getValue().getText());
            assertEquals("false", prop.getText());

            prop = (Element) props.get(2);
            assertEquals("filename", prop.attribute("type").getValue().getText());
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
        }
    }
}
