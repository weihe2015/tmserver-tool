package org.gs4tr.tmserver.tool.jwt;

import org.junit.Test;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JwtUtilsTest
{
    private String secret = "nSZfSHtmTrBTz7ak66Eb8N2TgNwf7rPa";

    public Claims getClaimsFromToken(String token)
    {
        Claims claims;
        try
        {
            claims = Jwts.parser().setSigningKey(secret.getBytes("UTF-8")).parseClaimsJws(token).getBody();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            claims = null;
        }
        return claims;
    }

    @Test
    public void jwtUtilsTest1() throws Exception
    {
        String token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjVFNzhBMEE1OEM3MzJENzM2OUNEM0Y0NDIyODgwQzU4MUJCQjc2QjYiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJYbmlncFl4ekxYTnB6VDlFSW9nTVdCdTdkclkifQ.eyJuYmYiOjE1ODg3MDgzMjgsImV4cCI6MTU4ODcwOTIyOCwiaXNzIjoiaHR0cHM6Ly9zc28tc3RnLnRyYW5zcGVyZmVjdC5jb20iLCJhdWQiOiJodHRwczovL3Nzby1zdGcudHJhbnNwZXJmZWN0LmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJWQjdobkc0c1BoVGVyUm1CVndmOTZIell4ak5CRnJuYiIsInN1YiI6IndoZUB0cmFuc2xhdGlvbnMuY29tIiwiYXV0aF90aW1lIjoxNTg4NzA4MzI3LCJpZHAiOiJUcmFuc1BlcmZlY3RBdXRoIiwic2NvcGUiOlsib3BlbmlkIiwiZW1haWwiLCJwcm9maWxlIl0sImFtciI6WyJleHRlcm5hbCJdfQ.oWbF8aVT8gvSsy5tw-HY_6wokmTHnQDC72dO-u5rvqZrhc2JYyMYimyjfX6HNBfRs6jDg_ewREgIuze4KbKCuz6j2wZWNDDa9dbPzghB_KbRFR7hNffawmla8GJKBffLnXXYJKFEya6SRJETEMmLrhwIGkMvYIaDfq2-lD92DPF1cbpu1yo7X0HlkbDrA4M54Zrv2y-cgmEUApcF66I3Si_OKgp2yeMnH9RuUSM9SX9Ga7X5LM_CDStnydxd5c8ErD7DGNAVyoud5GKa3hCaig7XUwQghs7T02Qe54aswE4cBGCn9FE7wdvS7SlAo64PNmkulhIJbwIsb7VQqWZilKFjxa3VJ-kP_NeNe59-pEYp_OeGyowLJRFiCvhM_xWPTP9681DGINBzHR2lpV3R0vqIXRif2EXb-EgpsQY9kB7W8i9pTxQR1J4JKxDC-axfH1iWyqeHbsJ5XX4XRHXO0AyI6U0dpB3ZxcRr0Rt4HLWbQXT8gkSr864YxlhD3IaA1efipg4-YfQ3Ys56VxOaNUKo7c1wtyeIL8K2VVYrr29rqpzPk4jCM8gKf9Kj2JjgiD5JxjMQNRVW6X_4n0TiHEossV3QjR8uQGSaOT8GuD_KVRhVOVM44pgut1CsPpireJ8SoOw7QhtxFhMUsKGfNsRhScvYBG1cd8bpjRcpd1E";
        Claims claims = getClaimsFromToken(token);
        for (String key : claims.keySet())
        {
            System.out.println("key : " + key);
            System.out.println("value : " + claims.get(key));
        }
    }
}
