package org.gs4tr.tmserver.tool.mvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.tmserver.tool.configuration.JwtTokenService;
import org.gs4tr.tmserver.tool.model.xml.Attribute;
import org.gs4tr.tmserver.tool.model.xml.Document;
import org.gs4tr.tmserver.tool.model.xml.Element;
import org.gs4tr.tmserver.tool.model.xml.Node;
import org.gs4tr.tmserver.tool.model.xml.parser.Parser;
import org.gs4tr.tmserver.tool.service.file.FolderUtils;
import org.gs4tr.tmserver.tool.service.file.ZipFileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import io.jsonwebtoken.Claims;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTest
{
    private String token;
    private final String systemDir = System.getProperty("user.dir");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ZipFileUtils zipFileUtils;

    @Autowired
    private FolderUtils folderUtils;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Test
    public void concordanceSearchTest() throws Exception
    {
        JSONObject requestBody = new JSONObject();
        requestBody.put("query", "Test");
        requestBody.put("resultNum", 50);
        requestBody.put("searchLocation", "SOURCE");
        requestBody.put("caseSensitive", false);
        requestBody.put("matchWholeWord", false);
        JSONArray arr = new JSONArray();
        JSONObject obj = new JSONObject();
        obj.put("label", "MERCK/MPENUSFRFR");
        obj.put("value", "MERCK/MPENUSFRFR");
        arr.put(obj);
        obj = new JSONObject();
        obj.put("label", "MERCK/MPENUSITIT");
        obj.put("value", "MERCK/MPENUSITIT");
        arr.put(obj);
        requestBody.put("selectedTmList", arr);
        assertNotNull(token);
        assertTrue(token.length() > 0);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/tus").accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token).content(requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());

        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("TM Group Name"));
        assertTrue(subJSONObject.has("Modification Date"));
        assertTrue(subJSONObject.has("TM Name"));
        assertTrue(subJSONObject.has("Target Language"));
        assertTrue(subJSONObject.has("Source Language"));
        assertTrue(subJSONObject.has("Source"));
        assertTrue(subJSONObject.has("Target"));
    }

    @Test
    public void concordanceSearchTestWithNewTmServerInfo() throws Exception
    {
        JSONObject newTmServerInfo = new JSONObject();
        JSONObject requestBody = new JSONObject();

        newTmServerInfo.put("url", "https://stg-tpt1.translations.com/TMS");
        newTmServerInfo.put("sub", "whe");
        newTmServerInfo.put("pass", "password1!");

        requestBody.put("query", "Test");
        requestBody.put("resultNum", 50);
        requestBody.put("searchLocation", "SOURCE");
        requestBody.put("isCaseSensitive", false);
        requestBody.put("isMatchWholeWord", false);
        JSONArray arr = new JSONArray();
        JSONObject obj = new JSONObject();
        obj.put("label", "MERCK/MPENUSFRFR");
        obj.put("value", "MERCK/MPENUSFRFR");
        arr.put(obj);
        obj = new JSONObject();
        obj.put("label", "MERCK/MPENUSITIT");
        obj.put("value", "MERCK/MPENUSITIT");
        arr.put(obj);
        requestBody.put("selectedTmList", arr);
        assertNotNull(token);
        assertTrue(token.length() > 0);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/tus").accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token).content(requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());

        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("TM Group Name"));
        assertTrue(subJSONObject.has("Modification Date"));
        assertTrue(subJSONObject.has("TM Name"));
        assertTrue(subJSONObject.has("Target Language"));
        assertTrue(subJSONObject.has("Source Language"));
        assertTrue(subJSONObject.has("Source"));
        assertTrue(subJSONObject.has("Target"));
    }

    @Test
    public void concordanceSearchWithEmptyResultTest() throws Exception
    {
        JSONObject requestBody = new JSONObject();
        requestBody.put("query", "asd123asd");
        requestBody.put("resultNum", 50);
        requestBody.put("searchLocation", "SOURCE");
        requestBody.put("isCaseSensitive", false);
        requestBody.put("isMatchWholeWord", false);
        JSONArray arr = new JSONArray();
        JSONObject obj = new JSONObject();
        obj.put("label", "02IDEAS162/02I000568");
        obj.put("value", "02IDEAS162/02I000568");
        arr.put(obj);
        obj = new JSONObject();
        obj.put("label", "02IDEAS162/02I000581");
        obj.put("value", "02IDEAS162/02I000581");
        arr.put(obj);
        requestBody.put("selectedTmList", arr);
        assertNotNull(token);
        assertTrue(token.length() > 0);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/tus").accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token).content(requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        // Change to Concordance Search so there are not exact search.
        resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());
        MvcResult mvcResult = resultActions.andReturn();
        JSONObject responseJSON = new JSONObject(
            mvcResult.getResponse().getContentAsString());
        assertTrue(responseJSON.has("searchString"));
        assertTrue(responseJSON.has("global"));
        assertEquals("asd123asd", responseJSON.getString("searchString"));
        assertEquals("No seach result is found for string: \"asd123asd\"",
            responseJSON.getString("global"));
    }

    @Test
    public void concordanceSearchWithResultTest() throws Exception
    {
        JSONObject requestBody = new JSONObject();
        requestBody.put("query", "Test");
        requestBody.put("resultNum", 50);
        requestBody.put("searchLocation", "SOURCE");
        requestBody.put("isCaseSensitive", false);
        requestBody.put("isMatchWholeWord", false);
        JSONArray arr = new JSONArray();
        JSONObject subJSON = new JSONObject();
        subJSON.put("label", "LAPRAIRIE/LPENGBESMX");
        subJSON.put("value", "LAPRAIRIE/LPENGBESMX");
        arr.put(subJSON);
        subJSON = new JSONObject();
        subJSON.put("label", "INSIGHT/ISENGBNLNL");
        subJSON.put("value", "INSIGHT/ISENGBNLNL");
        arr.put(subJSON);
        requestBody.put("selectedTmList", arr);
        assertNotNull(token);
        assertTrue(token.length() > 0);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/tus").accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token).content(requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        // Change to Concordance Search so there are not exact search.
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        JSONArray responseJSON = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        for (int i = 0, n = responseJSON.length(); i < n; i++)
        {
            JSONObject obj = responseJSON.getJSONObject(i);
            assertTrue(obj.has("TM Group Name"));
            assertTrue(obj.has("TM Name"));
        }
    }

    @Test
    public void createGenericUsernameTest() throws Exception
    {
        String tmName = "Insight";
        JSONObject genericUsernameParams = new JSONObject();
        genericUsernameParams.put("tmName", tmName);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/genericUsername").accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token).content(genericUsernameParams.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String genericUsername = mvcResult.getResponse().getContentAsString();
        assertNotNull(genericUsername);
        assertTrue(genericUsername.length() > 0);
    }

    @Test
    public void createGenericUsernameWithLanguageCodeTest() throws Exception
    {
        String tmName = "Insight";
        JSONObject genericUsernameParams = new JSONObject();
        genericUsernameParams.put("tmName", tmName);
        genericUsernameParams.put("sourceLanguageCode", "en-US");
        genericUsernameParams.put("targetLangaugeCode", "zh-CN");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/genericUsername").accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token).content(genericUsernameParams.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String genericUsername = mvcResult.getResponse().getContentAsString();
        assertNotNull(genericUsername);
        assertTrue(genericUsername.length() > 0);
    }

    @Test
    public void createGenericUsernameWithNewTmServerInfoAndLanguageCodeTest()
        throws Exception
    {
        token = remoteLoginAndGetToken();
        String tmName = "Insight";
        JSONObject genericUsernameParams = new JSONObject();
        genericUsernameParams.put("tmName", tmName);
        genericUsernameParams.put("sourceLanguageCode", "en-US");
        genericUsernameParams.put("targetLangaugeCode", "zh-CN");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/genericUsername").accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token).content(genericUsernameParams.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String genericUsername = mvcResult.getResponse().getContentAsString();
        assertNotNull(genericUsername);
        assertTrue(genericUsername.length() > 0);
    }

    @Test
    public void createGenericUserpasswordTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/genericPassword").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String genericUserpassword = mvcResult.getResponse().getContentAsString();
        assertNotNull(genericUserpassword);
        assertTrue(genericUserpassword.length() > 0);
    }

    @Test
    public void createGenericUserpasswordWithNewTmServerInfo() throws Exception
    {
        token = remoteLoginAndGetToken();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/genericPassword").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String genericUserpassword = mvcResult.getResponse().getContentAsString();
        assertNotNull(genericUserpassword);
        assertTrue(genericUserpassword.length() > 0);
    }

    @Test
    public void createTmCleanupPasscodeTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/cleanupPasscode").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String tmCleanupPasscode = mvcResult.getResponse().getContentAsString();
        assertNotNull(tmCleanupPasscode);
        assertTrue(tmCleanupPasscode.length() > 0);
    }

    @Test
    public void createTmCleanupPasscodeWithNewTmServerInfoTest() throws Exception
    {
        token = remoteLoginAndGetToken();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/cleanupPasscode").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String tmCleanupPasscode = mvcResult.getResponse().getContentAsString();
        assertNotNull(tmCleanupPasscode);
        assertTrue(tmCleanupPasscode.length() > 0);
    }

    @Test
    public void createTmGroupCodeTest() throws Exception
    {
        String tmGroupName = "test";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/groupCode?name=" + tmGroupName).accept(
                MediaType.APPLICATION_JSON).header("Authorization", "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String tmGroupShortCode = mvcResult.getResponse().getContentAsString();
        assertNotNull(tmGroupShortCode);
        assertTrue(tmGroupShortCode.length() > 0);
    }

    /**
     * Have not updated to take new Tm Server Info, I will change it once the RestAPI is changed in
     * TmCreationController.java
     */
    @Test
    public void createTmShortCodeTest() throws Exception
    {
        String tmName = "Insight";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/shortCode?name=" + tmName).accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String tmShortCode = mvcResult.getResponse().getContentAsString();
        assertNotNull(tmShortCode);
        assertTrue(tmShortCode.length() > 0);
    }

    @Ignore("This is a junit test for developement")
    @Test
    public void createTmWithCSVTest() throws Exception
    {
        String CSVFile = "target/test-classes/createTm.csv";
        InputStream inputStream = new FileInputStream(new File(CSVFile));

        byte[] out = IOUtils.toByteArray(inputStream);
        MockMultipartFile multipartFile = new MockMultipartFile("uploadedFiles",
            FilenameUtils.getName(CSVFile), MediaType.TEXT_PLAIN_VALUE, out);

        JSONObject jsonObject = new JSONObject();
        JSONObject selectedTmGroup = new JSONObject();
        selectedTmGroup.put("label", "Test group");
        selectedTmGroup.put("value", "4YESyxwCtA0MEqW82UWMoMnpTWF1uB/q");
        jsonObject.put("selectedTmGroup", selectedTmGroup);

        JSONObject selectedOrganization = new JSONObject();
        selectedOrganization.put("label", "TransPerfect");
        selectedOrganization.put("value", "4YESyxwCtA0MEqW82UWMoMnpTWF1uB/q");
        jsonObject.put("selectedOrganization", selectedOrganization);

        jsonObject.put("enabled", true);
        jsonObject.put("contextModeEnabled", true);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(
            "/api/v1/tm/tmscsv").file(
                multipartFile).contentType(MediaType.APPLICATION_JSON).header(
                    "Authorization",
                    "Bearer " + token).param("requestInfo", jsonObject.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        String responseMessage = mvcResult.getResponse().getContentAsString();
        assertNotNull(responseMessage);
        assertTrue(responseMessage.length() > 0);
        assertEquals("Tm is create successfully.", responseMessage);
    }

    @Test
    public void generateUserRoleTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/roles").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        JSONArray jsonArray = new JSONArray(response);
        assertTrue(jsonArray.length() > 0);

        JSONObject jsonObject = jsonArray.getJSONObject(0);
        assertTrue(jsonObject.has("label"));
        assertTrue(jsonObject.getString("label").length() > 0);
        assertTrue(jsonObject.has("value"));
        assertTrue(jsonObject.getString("value").length() > 0);
    }

    @Test
    public void generateUserRoleWithNewTmServerInfo() throws Exception
    {
        token = remoteLoginAndGetToken();

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/roles").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        JSONArray jsonArray = new JSONArray(response);
        assertTrue(jsonArray.length() > 0);

        JSONObject jsonObject = jsonArray.getJSONObject(0);
        assertTrue(jsonObject.has("label"));
        assertTrue(jsonObject.getString("label").length() > 0);
        assertTrue(jsonObject.has("value"));
        assertTrue(jsonObject.getString("value").length() > 0);
    }

    @Test
    public void getAllTmListTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/tmlist?type=rename&isRefreshTmList=false").accept(
                MediaType.APPLICATION_JSON).header(
                    "Authorization", "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("label"));
        assertTrue(subJSONObject.has("value"));
        assertTrue(subJSONObject.has("tmName"));
        assertTrue(subJSONObject.has("tmGroupName"));
        assertTrue(subJSONObject.has("tmID"));
        assertTrue(subJSONObject.has("sourceLocale"));
        assertTrue(subJSONObject.has("targetLocale"));
        assertTrue(subJSONObject.has("tmPath"));
    }

    @Test
    public void getExportTmListTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/tmlist?type=export&isRefreshTmList=false").accept(
                MediaType.APPLICATION_JSON).header(
                    "Authorization", "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("tmName"));
        assertTrue(subJSONObject.has("sourceLocale"));
        assertTrue(subJSONObject.has("targetLocale"));
        assertTrue(subJSONObject.has("tmGroupName"));
        assertTrue(subJSONObject.has("tmPath"));
        assertTrue(subJSONObject.has("totalTus"));
        assertTrue(subJSONObject.has("numberOfPrivateTus"));
        assertTrue(subJSONObject.has("lastTuModificationDate"));
    }

    @Test
    public void getImportProgressTest() throws Exception
    {
        String tmPath = "NAV000017/NAV000100";
        String jobId = "acb8a438-ead2-4ed5-b66c-495a9159dfea";
        JSONObject requestBody = new JSONObject();
        requestBody.put("tmPath", tmPath);
        requestBody.put("jobId", jobId);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/job/" + jobId + "?tmpath=" + tmPath).accept(
                MediaType.APPLICATION_JSON).header(
                    "Authorization", "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        JSONObject responseJSON = new JSONObject(response);
        assertFalse(responseJSON.getBoolean("success"));
        assertEquals("Couldn't find job with given id",
            responseJSON.getString("errorMessage"));
    }

    @Test
    public void getImportProgressWithNewTmServerInfoTest() throws Exception
    {
        token = remoteLoginAndGetToken();

        String tmPath = "MERCK/MPENUSPTBR";
        String jobId = "acb8a438-ead2-4ed5-b66c-495a9159dfea";
        JSONObject requestBody = new JSONObject();
        requestBody.put("tmPath", tmPath);
        requestBody.put("jobId", jobId);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/job/" + jobId + "?tmpath=" + tmPath).accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        JSONObject responseJSON = new JSONObject(response);
        assertFalse(responseJSON.getBoolean("success"));
        assertEquals("Couldn't find job with given id",
            responseJSON.getString("errorMessage"));
    }

    @Test
    public void getImportTmListTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/tmlist?type=import&isRefreshTmList=false").accept(
                MediaType.APPLICATION_JSON).header(
                    "Authorization", "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("tmName"));
        assertTrue(subJSONObject.has("sourceLocale"));
        assertTrue(subJSONObject.has("targetLocale"));
        assertTrue(subJSONObject.has("tmGroupName"));
        assertTrue(subJSONObject.has("tmPath"));
    }

    @Test
    public void getImportTmListWithNewTmServerInfoTest() throws Exception
    {
        token = remoteLoginAndGetToken();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/tmlist?type=import&isRefreshTmList=false").accept(
                MediaType.APPLICATION_JSON).header(
                    "Authorization", "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("tmName"));
        assertTrue(subJSONObject.has("sourceLocale"));
        assertTrue(subJSONObject.has("targetLocale"));
        assertTrue(subJSONObject.has("tmGroupName"));
        assertTrue(subJSONObject.has("tmPath"));
    }

    @Test
    public void getLanguageListTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/languages").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("text"));
        assertTrue(subJSONObject.getString("text").length() > 0);

        assertTrue(subJSONObject.has("value"));
        assertTrue(subJSONObject.getString("value").length() > 0);
    }

    @Test
    public void getLanguageListWithNewTmServerInfoTest() throws Exception
    {
        token = remoteLoginAndGetToken();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/languages").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("text"));
        assertTrue(subJSONObject.getString("text").length() > 0);

        assertTrue(subJSONObject.has("value"));
        assertTrue(subJSONObject.getString("value").length() > 0);
    }

    @Test
    public void getOrganizationListTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/organizations").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("label"));
        assertTrue(subJSONObject.getString("label").length() > 0);

        assertTrue(subJSONObject.has("value"));
        assertTrue(subJSONObject.getString("value").length() > 0);
    }

    @Test
    public void getOrganizationListWithNewTmServerInfoTest() throws Exception
    {
        token = remoteLoginAndGetToken();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/organizations").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("label"));
        assertTrue(subJSONObject.getString("label").length() > 0);

        assertTrue(subJSONObject.has("value"));
        assertTrue(subJSONObject.getString("value").length() > 0);
    }

    @Test
    public void getRemoteTmServerVersionTest() throws Exception
    {
        token = remoteLoginAndGetToken();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tms/version").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);
        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONObject response = new JSONObject(
            mvcResult.getResponse().getContentAsString());
        assertTrue(response.has("version"));
        assertEquals("5.3.0_RC2", response.get("version"));
    }

    @Test
    public void getSearchTmListTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/tmlist?type=concordance&isRefreshTmList=false").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("tmGroupName"));
        assertTrue(subJSONObject.getString("tmGroupName").length() > 0);

        assertTrue(subJSONObject.has("tmPath"));
        assertTrue(subJSONObject.getString("tmPath").length() > 0);

        assertTrue(subJSONObject.has("tmName"));
        assertTrue(subJSONObject.getString("tmName").length() > 0);

        assertTrue(subJSONObject.has("sourceLocale"));
        assertTrue(subJSONObject.getString("sourceLocale").length() > 0);

        assertTrue(subJSONObject.has("targetLocale"));
        assertTrue(subJSONObject.getString("targetLocale").length() > 0);

        assertTrue(subJSONObject.has("label"));
        assertTrue(subJSONObject.getString("label").length() > 0);

        assertTrue(subJSONObject.has("value"));
        assertTrue(subJSONObject.getString("value").length() > 0);
    }

    @Test
    public void getTmGroupTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/groups").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("label"));
        assertTrue(subJSONObject.getString("label").length() > 0);

        assertTrue(subJSONObject.has("value"));
        assertTrue(subJSONObject.getString("value").length() > 0);
    }

    @Test
    public void getTmGroupWithNewTmServerInfoTest() throws Exception
    {
        token = remoteLoginAndGetToken();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/groups").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONArray responseJSONArray = new JSONArray(
            mvcResult.getResponse().getContentAsString());
        assertNotNull(responseJSONArray);
        assertTrue(responseJSONArray.length() > 0);

        JSONObject subJSONObject = responseJSONArray.getJSONObject(0);
        assertTrue(subJSONObject.has("label"));
        assertTrue(subJSONObject.getString("label").length() > 0);

        assertTrue(subJSONObject.has("value"));
        assertTrue(subJSONObject.getString("value").length() > 0);
    }

    @Test
    public void getTmServerVersionTest() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tms/version").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);
        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        JSONObject response = new JSONObject(
            mvcResult.getResponse().getContentAsString());
        assertTrue(response.has("version"));
        assertEquals("5.3.0_RC2", response.get("version"));
    }

    @Test
    public void httpErrorTests() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/401");
        this.mockMvc.perform(requestBuilder).andExpect(
            MockMvcResultMatchers.status().isOk());

        requestBuilder = MockMvcRequestBuilders.get("/404");
        this.mockMvc.perform(requestBuilder).andExpect(
            MockMvcResultMatchers.status().isOk());

        requestBuilder = MockMvcRequestBuilders.get("/error");
        this.mockMvc.perform(requestBuilder).andExpect(
            MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void httpMethodNotSupportTests() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/genericUsername").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);
        this.mockMvc.perform(requestBuilder).andExpect(
            MockMvcResultMatchers.status().isMethodNotAllowed());
    }

    @Test
    public void httpStatusTests() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/");
        this.mockMvc.perform(requestBuilder).andExpect(
            MockMvcResultMatchers.status().isOk());

        requestBuilder = MockMvcRequestBuilders.get("/login");
        this.mockMvc.perform(requestBuilder).andExpect(
            MockMvcResultMatchers.status().isOk());

        requestBuilder = MockMvcRequestBuilders.get("/signup");
        this.mockMvc.perform(requestBuilder).andExpect(
            MockMvcResultMatchers.status().isNotFound());

        requestBuilder = MockMvcRequestBuilders.get("/reset");
        this.mockMvc.perform(requestBuilder).andExpect(
            MockMvcResultMatchers.status().isNotFound());

        requestBuilder = MockMvcRequestBuilders.get("/random");
        this.mockMvc.perform(requestBuilder).andExpect(
            MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void importTmxFileTestWithUploadTmxFile() throws Exception
    {
        String randomToken = "fa40d";
        String tmPath = "MERCK/MPENUSZHCN";
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getUploadFolderPath(systemDir, date,
            randomToken);

        if (new File(folderName).exists())
        {
            FileUtils.deleteDirectory(new File(folderName));
        }

        String testFile = "target/test-classes/Test_TM_import_en-us_zh-cn.tmx";
        InputStream inputStream = new FileInputStream(new File(testFile));

        byte[] out = IOUtils.toByteArray(inputStream);
        MockMultipartFile multipartFile = new MockMultipartFile("uploadedFiles",
            FilenameUtils.getName(testFile), MediaType.TEXT_XML_VALUE, out);

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sourceLocaleCode", "en-US");
        jsonObject.put("sourceLocale", "English (United States)");
        jsonObject.put("targetLocaleCode", "zh-CN");
        jsonObject.put("targetLocale", "Chinese (China)");
        jsonObject.put("tmGroupName", "Merck");
        jsonObject.put("tmPath", tmPath);
        jsonObject.put("status", "Pending");
        jsonObject.put("uploaded", false);
        jsonArray.put(jsonObject);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(
            "/api/v1/tm/tmx/upload").file(
                multipartFile).param("randomToken", randomToken).param("type",
                    "import").param("selectedTmRows",
                        jsonArray.toString()).header("Authorization",
                            "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();

        JSONObject responseJSON = new JSONObject(response);
        assertTrue(responseJSON.has("status"));
        assertTrue(responseJSON.getBoolean("status"));
        assertTrue(responseJSON.has("filename"));
        assertEquals(FilenameUtils.getName(testFile),
            responseJSON.getString("filename"));
        assertFalse(new File(folderName + FilenameUtils.getName(testFile)).exists());

        tmPath = tmPath.replaceAll("/", "_");
        String folderWithLocaleCode = folderName + "en-us_zh-cn/";
        assertTrue(new File(folderWithLocaleCode + tmPath + "#" +
            FilenameUtils.getName(testFile)).exists());

        // Import TMX file:
        jsonObject = new JSONObject();
        jsonObject.put("randomToken", randomToken);
        jsonObject.put("folderLocales", "en-US_zh-CN");
        jsonObject.put("tmName", "Merck Manual Specialist Facing");
        jsonObject.put("tmPath", "MERCK/MPENUSZHCN");
        jsonObject.put("tmImportOption", "SKIP");
        jsonObject.put("tmAttributeImportOption", "IGNORE");
        requestBuilder = MockMvcRequestBuilders.put("/api/v1/tm/tms").accept(
            MediaType.APPLICATION_JSON).contentType(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token).content(jsonObject.toString());
        resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        mvcResult = resultActions.andReturn();
        response = mvcResult.getResponse().getContentAsString();

        jsonArray = new JSONArray(response);
        for (int i = 0, n = jsonArray.length(); i < n; i++)
        {
            jsonObject = jsonArray.getJSONObject(i);
            assertTrue(jsonObject.has("jobId"));
            assertTrue(jsonObject.has("fileName"));
            assertEquals("MERCK_MPENUSZHCN#Test_TM_import_en-us_zh-cn.tmx",
                jsonObject.get("fileName"));
            assertTrue(jsonObject.has("message"));
        }
        // remove the test folder from uploadDir:
        FileUtils.deleteDirectory(new File(folderName));
    }

    @Test
    public void importTmxFileTestWithUploadZipFile() throws Exception
    {
        String randomToken = "4e71a";
        String tmPath = "MERCK/MSENUSDEDE";
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getUploadFolderPath(systemDir, date,
            randomToken);

        if (new File(folderName).exists())
        {
            FileUtils.deleteDirectory(new File(folderName));
        }

        String testFile = "target/test-classes/Test_TM_import_en-us_de-de.tmx.zip";
        String tmxFilename = "Test_TM_import_en-us_de-de.tmx";
        InputStream inputStream = new FileInputStream(new File(testFile));

        byte[] out = IOUtils.toByteArray(inputStream);
        MockMultipartFile multipartFile = new MockMultipartFile("uploadedFiles",
            FilenameUtils.getName(testFile), MediaType.TEXT_XML_VALUE, out);

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sourceLocaleCode", "en-US");
        jsonObject.put("sourceLocale", "English (United States)");
        jsonObject.put("targetLocaleCode", "de-DE");
        jsonObject.put("targetLocale", "German (Germany)");
        jsonObject.put("tmGroupName", "Merck");
        jsonObject.put("tmPath", tmPath);
        jsonObject.put("status", "Pending");
        jsonObject.put("uploaded", false);
        jsonArray.put(jsonObject);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(
            "/api/v1/tm/tmx/upload").file(
                multipartFile).param("randomToken", randomToken).param("type",
                    "import").param("selectedTmRows",
                        jsonArray.toString()).header("Authorization",
                            "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();

        jsonArray = new JSONArray(response);
        for (int i = 0, n = jsonArray.length(); i < n; i++)
        {
            JSONObject responseJSON = jsonArray.getJSONObject(i);
            assertTrue(responseJSON.has("status"));
            assertTrue(responseJSON.getBoolean("status"));
            assertTrue(responseJSON.has("filename"));
            assertEquals(FilenameUtils.getName(testFile),
                responseJSON.getString("originalZipFileName"));
            assertEquals(tmxFilename, responseJSON.getString("filename"));
            assertTrue(
                new File(folderName + FilenameUtils.getName(testFile)).exists());
        }

        tmPath = tmPath.replaceAll("/", "_");
        String folderWithLocaleCode = folderName + "en-us_de-de/";
        assertTrue(
            new File(folderWithLocaleCode + tmPath + "#" + tmxFilename).exists());

        // Import TMX file:
        jsonObject = new JSONObject();
        jsonObject.put("randomToken", randomToken);
        jsonObject.put("folderLocales", "en-US_de-DE");
        jsonObject.put("tmName", "Merck Manual Specialist Facing");
        jsonObject.put("tmPath", "MERCK/MSENUSDEDE");
        jsonObject.put("tmImportOption", "SKIP");
        jsonObject.put("tmAttributeImportOption", "IGNORE");
        requestBuilder = MockMvcRequestBuilders.put("/api/v1/tm/tms").accept(
            MediaType.APPLICATION_JSON).contentType(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token).content(jsonObject.toString());
        resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        mvcResult = resultActions.andReturn();
        response = mvcResult.getResponse().getContentAsString();

        jsonArray = new JSONArray(response);
        for (int i = 0, n = jsonArray.length(); i < n; i++)
        {
            jsonObject = jsonArray.getJSONObject(i);
            assertTrue(jsonObject.has("jobId"));
            assertTrue(jsonObject.has("fileName"));
            assertEquals("MERCK_MSENUSDEDE#Test_TM_import_en-us_de-de.tmx",
                jsonObject.get("fileName"));
            assertTrue(jsonObject.has("message"));
        }
        // remove the test folder from uploadDir:
        FileUtils.deleteDirectory(new File(folderName));
    }

    @Before
    public void loginAndGetToken() throws Exception
    {
        JSONObject requestBody = new JSONObject();
        requestBody.put("username", "whe");
        requestBody.put("password", "password1!");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/user/login").contentType(
                MediaType.APPLICATION_JSON).content(requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        JSONObject responseJSON = new JSONObject(
            mvcResult.getResponse().getContentAsString());
        assertTrue(responseJSON.has("token"));
        assertTrue(responseJSON.getString("token").length() > 0);
        token = responseJSON.getString("token");
    }

    @After
    public void logoutAndRemoveCache() throws Exception
    {
        final Claims claims = jwtTokenService.getClaimsFromToken(token);
        String sessionId = String.valueOf(claims.get("sid"));
        JSONObject requestBody = new JSONObject();
        requestBody.put("sid", sessionId);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/user/logout").contentType(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token).content(
                        requestBody.toString());
        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        assertEquals("", mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void manipulateTmxFileTest() throws Exception
    {
        String testFile = "target/test-classes/test.tmx";
        String testOutputFoldername = "target/test-classes/output";
        File testOutputFolder = new File(testOutputFoldername);

        if (testOutputFolder.exists())
        {
            testOutputFolder.mkdirs();
        }

        String randomToken = "05119";
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getSourceManipulateDir(systemDir, date,
            randomToken);
        String targetFolderName = folderName.replaceAll("sourceManipulateDir",
            "targetManipulateDir");

        if (new File(folderName).exists())
        {
            FileUtils.deleteDirectory(new File(folderName));
        }

        File sourceFile = new File(testFile);

        String newFileName = folderName + sourceFile.getName();
        File file = new File(newFileName);
        FileUtils.copyFile(sourceFile, file);

        JSONArray arr = new JSONArray();
        JSONObject subJSON = new JSONObject();
        subJSON.put("key", "bu");
        subJSON.put("value", "aem_web");
        subJSON.put("hide", false);
        arr.put(subJSON);

        JSONObject requestBody = new JSONObject();
        requestBody.put("newAttributes", arr);
        requestBody.put("removePreviousAttribute", true);
        requestBody.put("addFilenameAsAttribute", true);
        requestBody.put("removeTrailingSpaces", false);
        requestBody.put("randomToken", randomToken);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put(
            "/api/v1/tm/tmx").accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token).content(requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        // unzip file:
        String zipFilename = targetFolderName +
            mvcResult.getResponse().getHeader("filename");

        zipFileUtils.unzipFiles(new File(zipFilename), testOutputFoldername);
        List<String> tmxFileList = new ArrayList<String>();
        folderUtils.getAllTmxFiles(targetFolderName, tmxFileList);

        for (String tmxFile : tmxFileList)
        {
            Reader inputReader = null;
            try
            {
                inputReader = new FileReader(tmxFile);
                Document document = new Parser().parse(inputReader);
                List<Node> nodeList = document.selectNodes("//tu/prop");
                assertTrue(nodeList.size() > 0);

                Element ele = (Element) nodeList.get(0);
                assertEquals("prop", ele.getName().getName());

                List<Attribute> attributeList = ele.attributes();
                assertTrue(attributeList.size() > 0);
                Attribute attribute = attributeList.get(0);
                assertEquals("type", attribute.getName().getName());
                assertEquals("filename", attribute.getValue().getText());

                ele = (Element) nodeList.get(1);
                assertEquals("prop", ele.getName().getName());

                attributeList = ele.attributes();
                assertTrue(attributeList.size() > 0);
                attribute = attributeList.get(0);
                assertEquals("type", attribute.getName().getName());
                assertEquals("bu", attribute.getValue().getText());
            }
            finally
            {
                IOUtils.closeQuietly(inputReader);
            }
        }

        // remove file from source folder.
        FileUtils.deleteDirectory(testOutputFolder);
        FileUtils.deleteDirectory(new File(folderName));
    }

    @Test
    public void manipulateTmxFileWithRemoveAllPreviousAttributeTest()
        throws Exception
    {
        String testFile = "target/test-classes/test.tmx";
        String testOutputFoldername = "target/test-classes/output";
        File testOutputFolder = new File(testOutputFoldername);

        if (testOutputFolder.exists())
        {
            testOutputFolder.mkdirs();
        }

        String randomToken = "05129";
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getSourceManipulateDir(systemDir, date,
            randomToken);
        String targetFolderName = folderName.replaceAll("sourceManipulateDir",
            "targetManipulateDir");

        File sourceFile = new File(testFile);

        String newFileName = folderName + sourceFile.getName();
        File file = new File(newFileName);
        FileUtils.copyFile(sourceFile, file);

        JSONArray arr = new JSONArray();
        JSONObject subJSON = new JSONObject();
        arr.put(subJSON);

        JSONObject requestBody = new JSONObject();
        requestBody.put("newAttributes", arr);
        requestBody.put("removePreviousAttribute", true);
        requestBody.put("addFilenameAsAttribute", false);
        requestBody.put("removeTrailingSpaces", false);
        requestBody.put("randomToken", randomToken);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put(
            "/api/v1/tm/tmx").accept(
                MediaType.APPLICATION_JSON).contentType(
                    MediaType.APPLICATION_JSON).header("Authorization",
                        "Bearer " + token).content(requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        // unzip file:
        String zipFilename = targetFolderName +
            mvcResult.getResponse().getHeader("filename");

        zipFileUtils.unzipFiles(new File(zipFilename), testOutputFoldername);
        List<String> tmxFileList = new ArrayList<String>();
        folderUtils.getAllTmxFiles(targetFolderName, tmxFileList);

        for (String tmxFile : tmxFileList)
        {
            Reader inputReader = null;
            try
            {
                inputReader = new FileReader(tmxFile);
                Document document = new Parser().parse(inputReader);
                List<Node> nodeList = document.selectNodes("//tu/prop");
                assertTrue(nodeList.size() == 0);
            }
            finally
            {
                IOUtils.closeQuietly(inputReader);
            }
        }

        // remove file from source folder.
        FileUtils.deleteDirectory(testOutputFolder);
        FileUtils.deleteDirectory(new File(folderName));
    }

    private String remoteLoginAndGetToken() throws Exception
    {
        Claims claims = jwtTokenService.getClaimsFromToken(token);
        String sid = (String) claims.get("sid");
        JSONObject requestBody = new JSONObject();
        requestBody.put("url", "https://stg-tpt1.translations.com/TMS");
        requestBody.put("sub", "whe");
        requestBody.put("pass", "password1!");
        requestBody.put("sid", sid);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/user/remote").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token).contentType(
                        MediaType.APPLICATION_JSON).content(requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        String newToken = mvcResult.getResponse().getContentAsString();
        assertTrue(newToken.length() > 0);
        return newToken;
    }

    @Test
    public void uploadAndManipulateTmxFileTest() throws Exception
    {
        // upload file:
        String randomToken = "05109";
        String testFile = "target/test-classes/test.tmx";
        InputStream inputStream = new FileInputStream(new File(testFile));

        byte[] out = IOUtils.toByteArray(inputStream);
        MockMultipartFile multipartFile = new MockMultipartFile("uploadedFiles",
            FilenameUtils.getName(testFile), MediaType.TEXT_XML_VALUE, out);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(
            "/api/v1/tm/tmx/upload").file(
                multipartFile).contentType(MediaType.APPLICATION_JSON).param("type",
                    "manipulate").param(
                        "randomToken", randomToken).header("Authorization",
                            "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getSourceManipulateDir(systemDir, date,
            randomToken);

        assertTrue(new File(folderName + FilenameUtils.getName(testFile)).exists());
        String response = mvcResult.getResponse().getContentAsString();

        JSONObject responseJSON = new JSONObject(response);
        assertTrue(responseJSON.has("status"));
        assertTrue(responseJSON.getBoolean("status"));
        assertTrue(responseJSON.has("filename"));
        assertEquals(FilenameUtils.getName(testFile),
            responseJSON.getString("filename"));

        // manipulate Tmx file:
        JSONArray arr = new JSONArray();
        JSONObject subJSON = new JSONObject();
        subJSON.put("key", "bu");
        subJSON.put("value", "aem_web");
        subJSON.put("hide", false);
        arr.put(subJSON);

        JSONObject requestBody = new JSONObject();
        requestBody.put("newAttributes", arr);
        requestBody.put("removePreviousAttribute", true);
        requestBody.put("addFilenameAsAttribute", true);
        requestBody.put("removeTrailingSpaces", false);
        requestBody.put("randomToken", randomToken);

        requestBuilder = MockMvcRequestBuilders.put("/api/v1/tm/tmx").accept(
            MediaType.APPLICATION_JSON).contentType(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token).content(requestBody.toString());

        resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        mvcResult = resultActions.andReturn();

        // unzip file:
        String targetFolderName = folderName.replaceAll("sourceManipulateDir",
            "targetManipulateDir");
        String testOutputFoldername = "target/test-classes/output";
        String zipFilename = targetFolderName +
            mvcResult.getResponse().getHeader("filename");
        File testOutputFolder = new File(testOutputFoldername);

        zipFileUtils.unzipFiles(new File(zipFilename), testOutputFoldername);
        List<String> tmxFileList = new ArrayList<String>();
        folderUtils.getAllTmxFiles(targetFolderName, tmxFileList);

        for (String tmxFile : tmxFileList)
        {
            Reader inputReader = null;
            try
            {
                inputReader = new FileReader(tmxFile);
                Document document = new Parser().parse(inputReader);
                List<Node> nodeList = document.selectNodes("//tu/prop");
                assertTrue(nodeList.size() > 0);

                Element ele = (Element) nodeList.get(0);
                assertEquals("prop", ele.getName().getName());

                List<Attribute> attributeList = ele.attributes();
                assertTrue(attributeList.size() > 0);
                Attribute attribute = attributeList.get(0);
                assertEquals("type", attribute.getName().getName());
                assertEquals("filename", attribute.getValue().getText());

                ele = (Element) nodeList.get(1);
                assertEquals("prop", ele.getName().getName());

                attributeList = ele.attributes();
                assertTrue(attributeList.size() > 0);
                attribute = attributeList.get(0);
                assertEquals("type", attribute.getName().getName());
                assertEquals("bu", attribute.getValue().getText());
            }
            finally
            {
                IOUtils.closeQuietly(inputReader);
            }
        }

        // remove file from source folder.
        FileUtils.deleteDirectory(testOutputFolder);
        FileUtils.deleteDirectory(new File(folderName));
    }

    @Test
    public void uploadFileForImportTmWithOnlyLanguageMatching() throws Exception
    {
        String randomToken = "0e70b";
        String tmPath = "NAV000017/NAV000100";
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getUploadFolderPath(systemDir, date,
            randomToken);

        if (new File(folderName).exists())
        {
            FileUtils.deleteDirectory(new File(folderName));
        }

        String testFile = "target/test-classes/Sterling_IRB_EN-AR.tmx";
        InputStream inputStream = new FileInputStream(new File(testFile));

        byte[] out = IOUtils.toByteArray(inputStream);
        MockMultipartFile multipartFile = new MockMultipartFile("uploadedFiles",
            FilenameUtils.getName(testFile), MediaType.TEXT_XML_VALUE, out);

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sourceLocaleCode", "en-US");
        jsonObject.put("sourceLocale", "English (United States)");
        jsonObject.put("targetLocaleCode", "ar");
        jsonObject.put("targetLocale", "Arabic");
        jsonObject.put("tmGroupName", "Navigant");
        jsonObject.put("tmPath", tmPath);
        jsonObject.put("status", "Pending");
        jsonObject.put("uploaded", false);
        jsonArray.put(jsonObject);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(
            "/api/v1/tm/tmx/upload").file(
                multipartFile).contentType(MediaType.APPLICATION_JSON).param(
                    "randomToken", randomToken).param(
                        "type", "manipulate").param("selectedTmRows",
                            jsonArray.toString()).header("Authorization",
                                "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();

        JSONObject responseJSON = new JSONObject(response);
        assertTrue(responseJSON.has("status"));
        assertTrue(responseJSON.getBoolean("status"));
        assertTrue(responseJSON.has("filename"));
        assertEquals(FilenameUtils.getName(testFile),
            responseJSON.getString("filename"));
        assertFalse(new File(folderName + FilenameUtils.getName(testFile)).exists());

        tmPath = tmPath.replaceAll("/", "_");
        String folderWithLocaleCode = folderName + "en-us_ar/";
        assertFalse(new File(folderWithLocaleCode + tmPath + "#" +
            FilenameUtils.getName(testFile)).exists());
        // remove the test folder from uploadDir:
        FileUtils.deleteDirectory(new File(folderName));
    }

    @Test
    public void uploadFilesForImportTmTest() throws Exception
    {
        String randomToken = "0e69b";
        String tmPath = "NAV000017/NAV000100";
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getUploadFolderPath(systemDir, date,
            randomToken);

        if (new File(folderName).exists())
        {
            FileUtils.deleteDirectory(new File(folderName));
        }

        String testFile = "target/test-classes/Test_TM_import_en-us_fr-FR.tmx";
        InputStream inputStream = new FileInputStream(new File(testFile));

        byte[] out = IOUtils.toByteArray(inputStream);
        MockMultipartFile multipartFile = new MockMultipartFile("uploadedFiles",
            FilenameUtils.getName(testFile), MediaType.TEXT_XML_VALUE, out);

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sourceLocaleCode", "en-US");
        jsonObject.put("sourceLocale", "English (United States)");
        jsonObject.put("targetLocaleCode", "fr-FR");
        jsonObject.put("targetLocale", "French (France)");
        jsonObject.put("tmGroupName", "Navigant");
        jsonObject.put("tmPath", tmPath);
        jsonObject.put("status", "Pending");
        jsonObject.put("uploaded", false);
        jsonArray.put(jsonObject);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(
            "/api/v1/tm/tmx/upload").file(
                multipartFile).contentType(MediaType.APPLICATION_JSON).param(
                    "randomToken", randomToken).param(
                        "type", "import").param("selectedTmRows",
                            jsonArray.toString()).header("Authorization",
                                "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();

        JSONObject responseJSON = new JSONObject(response);
        assertTrue(responseJSON.has("status"));
        assertTrue(responseJSON.getBoolean("status"));
        assertTrue(responseJSON.has("filename"));
        assertEquals(FilenameUtils.getName(testFile),
            responseJSON.getString("filename"));
        assertFalse(new File(folderName + FilenameUtils.getName(testFile)).exists());

        tmPath = tmPath.replaceAll("/", "_");
        String folderWithLocaleCode = folderName + "en-us_fr-fr/";
        assertTrue(new File(folderWithLocaleCode + tmPath + "#" +
            FilenameUtils.getName(testFile)).exists());
        // remove the test folder from uploadDir:
        FileUtils.deleteDirectory(new File(folderName));
    }

    @Test
    public void uploadFilesWithoutCorrectLocaleTest() throws Exception
    {
        String randomToken = "0e79b";
        String tmPath = "NAV000017/NAV000100";
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getUploadFolderPath(systemDir, date,
            randomToken);
        if (new File(folderName).exists())
        {
            FileUtils.deleteDirectory(new File(folderName));
        }

        String testFile = "target/test-classes/Test_TM_import_en-us_fr-FR.tmx";
        InputStream inputStream = new FileInputStream(new File(testFile));

        byte[] out = IOUtils.toByteArray(inputStream);
        MockMultipartFile multipartFile = new MockMultipartFile("uploadedFiles",
            FilenameUtils.getName(testFile), MediaType.TEXT_XML_VALUE, out);

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sourceLocaleCode", "en-US");
        jsonObject.put("sourceLocale", "English (United States)");
        jsonObject.put("targetLocaleCode", "zh-CN");
        jsonObject.put("targetLocale", "Chinese (China)");
        jsonObject.put("tmGroupName", "Navigant");
        jsonObject.put("tmPath", tmPath);
        jsonObject.put("status", "Pending");
        jsonObject.put("uploaded", false);
        jsonArray.put(jsonObject);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(
            "/api/v1/tm/tmx/upload").file(
                multipartFile).contentType(MediaType.APPLICATION_JSON).param(
                    "randomToken", randomToken).param(
                        "type", "import").param("selectedTmRows",
                            jsonArray.toString()).header("Authorization",
                                "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();

        JSONObject responseJSON = new JSONObject(response);
        assertTrue(responseJSON.has("status"));
        assertFalse(responseJSON.getBoolean("status"));
        assertTrue(responseJSON.has("filename"));
        assertEquals(FilenameUtils.getName(testFile),
            responseJSON.getString("filename"));
        assertFalse(new File(folderName + FilenameUtils.getName(testFile)).exists());

        tmPath = tmPath.replaceAll("/", "_");
        String folderWithLocaleCode = folderName + "en-us_zh-cn/";
        assertFalse(new File(folderWithLocaleCode + tmPath + "#" +
            FilenameUtils.getName(testFile)).exists());
        assertFalse(new File(folderWithLocaleCode).exists());
        // remove the test folder from uploadDir:
        FileUtils.deleteDirectory(new File(folderName));
    }

    @Test
    public void uploadWfTxtFileForImportTmWithOnlyLanguageMatching() throws Exception
    {
        String randomToken = "01agb";
        String tmPath = "NAV000017/NAV000100";
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getUploadFolderPath(systemDir, date,
            randomToken);

        if (new File(folderName).exists())
        {
            FileUtils.deleteDirectory(new File(folderName));
        }

        String testFile = "target/test-classes/451_Research_ENUS-FRFR.txt";
        InputStream inputStream = new FileInputStream(new File(testFile));

        byte[] out = IOUtils.toByteArray(inputStream);
        MockMultipartFile multipartFile = new MockMultipartFile("uploadedFiles",
            FilenameUtils.getName(testFile), MediaType.TEXT_XML_VALUE, out);

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sourceLocaleCode", "en-US");
        jsonObject.put("sourceLocale", "English (United States)");
        jsonObject.put("targetLocaleCode", "fr-fr");
        jsonObject.put("targetLocale", "French (France)");
        jsonObject.put("tmGroupName", "Navigant");
        jsonObject.put("tmPath", tmPath);
        jsonObject.put("status", "Pending");
        jsonObject.put("uploaded", false);
        jsonArray.put(jsonObject);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(
            "/api/v1/tm/tmx/upload").file(
                multipartFile).contentType(MediaType.APPLICATION_JSON).param(
                    "randomToken", randomToken).param(
                        "type", "manipulate").param("selectedTmRows",
                            jsonArray.toString()).header("Authorization",
                                "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();

        JSONObject responseJSON = new JSONObject(response);
        assertTrue(responseJSON.has("status"));
        assertTrue(responseJSON.getBoolean("status"));
        assertTrue(responseJSON.has("filename"));
        assertEquals(FilenameUtils.getName(testFile),
            responseJSON.getString("filename"));
        assertFalse(new File(folderName + FilenameUtils.getName(testFile)).exists());

        tmPath = tmPath.replaceAll("/", "_");
        String folderWithLocaleCode = folderName + "en-us_fr-fr/";
        assertFalse(new File(folderWithLocaleCode + tmPath + "#" +
            FilenameUtils.getName(testFile)).exists());
        // remove the test folder from uploadDir:
        FileUtils.deleteDirectory(new File(folderName));
    }

    @Test
    public void uploadWfTxtFilesWithoutCorrectLocaleTest() throws Exception
    {
        String randomToken = "01a2b";
        String tmPath = "NAV000017/NAV000100";
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderName = folderUtils.getUploadFolderPath(systemDir, date,
            randomToken);
        if (new File(folderName).exists())
        {
            FileUtils.deleteDirectory(new File(folderName));
        }

        String testFile = "target/test-classes/451_Research_ENUS-FRFR.txt";
        InputStream inputStream = new FileInputStream(new File(testFile));

        byte[] out = IOUtils.toByteArray(inputStream);
        MockMultipartFile multipartFile = new MockMultipartFile("uploadedFiles",
            FilenameUtils.getName(testFile), MediaType.TEXT_XML_VALUE, out);

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sourceLocaleCode", "en-US");
        jsonObject.put("sourceLocale", "English (United States)");
        jsonObject.put("targetLocaleCode", "zh-CN");
        jsonObject.put("targetLocale", "Chinese (China)");
        jsonObject.put("tmGroupName", "Navigant");
        jsonObject.put("tmPath", tmPath);
        jsonObject.put("status", "Pending");
        jsonObject.put("uploaded", false);
        jsonArray.put(jsonObject);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(
            "/api/v1/tm/tmx/upload").file(
                multipartFile).contentType(MediaType.APPLICATION_JSON).param(
                    "randomToken", randomToken).param(
                        "type", "import").param("selectedTmRows",
                            jsonArray.toString()).header("Authorization",
                                "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String response = mvcResult.getResponse().getContentAsString();

        JSONObject responseJSON = new JSONObject(response);
        assertTrue(responseJSON.has("status"));
        assertFalse(responseJSON.getBoolean("status"));
        assertTrue(responseJSON.has("filename"));
        assertEquals(FilenameUtils.getName(testFile),
            responseJSON.getString("filename"));
        assertFalse(new File(folderName + FilenameUtils.getName(testFile)).exists());

        tmPath = tmPath.replaceAll("/", "_");
        String folderWithLocaleCode = folderName + "en-us_zh-cn/";
        assertFalse(new File(folderWithLocaleCode + tmPath + "#" +
            FilenameUtils.getName(testFile)).exists());
        assertFalse(new File(folderWithLocaleCode).exists());
        // remove the test folder from uploadDir:
        FileUtils.deleteDirectory(new File(folderName));
    }

    /**
     * Validate random token for uploading TMX files in Tm import
     */
    @Test
    public void validateRandomTokenTest() throws Exception
    {
        // Invalid Token:
        String randomToken = "";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/token?t=" + randomToken).accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);
        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());

        // Conflict Token:
        randomToken = "dwe5a";
        JSONObject requestBody = new JSONObject();
        requestBody.put("token", randomToken);

        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String folderPath = folderUtils.getUploadFolderPath(systemDir, date,
            randomToken);
        File folderPathFile = new File(folderPath);
        if (!folderPathFile.exists())
        {
            folderPathFile.mkdirs();
        }
        requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/token?t=" + randomToken).accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);
        resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isConflict());

        // remove this folder:
        FileUtils.deleteDirectory(folderPathFile);

        // Valid token:
        requestBuilder = MockMvcRequestBuilders.get(
            "/api/v1/tm/token?t=" + randomToken).accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);
        resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void validateTmCleanupPasscodeTest() throws Exception
    {
        String tmCleanupPasscode = "p@ssw0rd1!";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/cleanupPasscode").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token).content(tmCleanupPasscode);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isCreated());
        MvcResult mvcResult = resultActions.andReturn();

        String isValidTmCleanupPasscode = mvcResult.getResponse().getContentAsString();
        assertNotNull(isValidTmCleanupPasscode);
        assertTrue(StringUtils.equalsIgnoreCase("valid", isValidTmCleanupPasscode));
    }

    @Test
    public void validateTmCleanupPasscodeWithNewTmServerInfoTest() throws Exception
    {
        token = remoteLoginAndGetToken();
        String tmCleanupPasscode = "p@ssw0rd1!";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/cleanupPasscode").accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token).content(tmCleanupPasscode);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isCreated());
        MvcResult mvcResult = resultActions.andReturn();

        String isValidTmCleanupPasscode = mvcResult.getResponse().getContentAsString();
        assertNotNull(isValidTmCleanupPasscode);
        assertTrue(StringUtils.equalsIgnoreCase("valid", isValidTmCleanupPasscode));
    }

    /**
     * Have not updated to take new Tm Server Info, I will change it once the RestAPI is changed in
     * TmCreationController.java
     */
    @Test
    public void validateTmShortCodeTest() throws Exception
    {
        String tmShortCode = "INS000339";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/shortCode?code=" + tmShortCode).accept(
                MediaType.APPLICATION_JSON).header(
                    "Authorization", "Bearer " + token);

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();

        String isValidTmShortCode = mvcResult.getResponse().getContentAsString();
        assertNotNull(isValidTmShortCode);
        assertTrue(StringUtils.equalsIgnoreCase("true", isValidTmShortCode));

        tmShortCode = "INS000339INSINS";
        requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/tm/shortCode?code=" + tmShortCode).accept(
                MediaType.APPLICATION_JSON).header("Authorization",
                    "Bearer " + token);

        resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        mvcResult = resultActions.andReturn();

        isValidTmShortCode = mvcResult.getResponse().getContentAsString();
        assertNotNull(isValidTmShortCode);
        assertTrue(StringUtils.equalsIgnoreCase("false", isValidTmShortCode));
    }
}
