package org.gs4tr.tmserver.tool.mvc;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application.properties")
public class ApplicationVersionTest
{
    @Value("${tmserver.version}")
    private String applicationVersion;

    @Test
    public void applicationVersionTest1() throws Exception
    {
        assertThat(applicationVersion).isNotBlank();
        assertThat(applicationVersion).contains("2.4");
    }
}
