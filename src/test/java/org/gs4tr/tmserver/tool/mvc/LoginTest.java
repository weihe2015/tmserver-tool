package org.gs4tr.tmserver.tool.mvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.gs4tr.tmserver.tool.configuration.JwtTokenService;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import io.jsonwebtoken.Claims;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LoginTest
{
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JwtTokenService jwtTokenService;

    private String token;

    @Test
    public void loginWithAdmin() throws Exception
    {
        JSONObject requestBody = new JSONObject();
        requestBody.put("username", "whe");
        requestBody.put("password", "password1!");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/user/login").contentType(MediaType.APPLICATION_JSON).content(
                requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());

        MvcResult mvcResult = resultActions.andReturn();
        JSONObject responseJSON = new JSONObject(
            mvcResult.getResponse().getContentAsString());
        assertTrue(responseJSON.has("token"));
        token = responseJSON.getString("token");
    }

    @Test
    public void loginWithPowerUser() throws Exception
    {
        JSONObject requestBody = new JSONObject();
        requestBody.put("username", "whe");
        requestBody.put("password", "password1!");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/user/login").contentType(MediaType.APPLICATION_JSON).content(
                requestBody.toString());

        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());

        MvcResult mvcResult = resultActions.andReturn();
        JSONObject responseJSON = new JSONObject(
            mvcResult.getResponse().getContentAsString());
        assertTrue(responseJSON.has("token"));
        token = responseJSON.getString("token");
    }

    @After
    public void logoutAndRemoveCache() throws Exception
    {
        final Claims claims = jwtTokenService.getClaimsFromToken(token);
        String sessionId = String.valueOf(claims.get("sid"));
        JSONObject requestBody = new JSONObject();
        requestBody.put("sid", sessionId);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
            "/api/v1/user/logout").contentType(MediaType.APPLICATION_JSON).header(
                "Authorization", "Bearer " + token).content(requestBody.toString());
        ResultActions resultActions = this.mockMvc.perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        assertEquals("", mvcResult.getResponse().getContentAsString());
    }
}
