package org.gs4tr.tmserver.tool.excel;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class MetadataTest
{
    @Test
    public void metadataTest1() throws Exception
    {
        String filePath = "Github控制台\\账号中心\\i18n\\source\\zh.json.k_00043on";
        String path = FilenameUtils.getPath(filePath);
        String fileName;
        if (StringUtils.isNotEmpty(path))
        {
            fileName = FilenameUtils.getExtension(filePath);
            Assert.assertEquals("k_00043on", fileName);
        }


        filePath = "k_0rmi15d_column_title";
        path = FilenameUtils.getPath(filePath);
        if (StringUtils.isEmpty(path))
        {
            fileName = FilenameUtils.getName(filePath);
            Assert.assertEquals("k_0rmi15d_column_title", fileName);
        }
    }
}
