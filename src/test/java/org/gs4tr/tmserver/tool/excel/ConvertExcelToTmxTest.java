package org.gs4tr.tmserver.tool.excel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.gs4tr.tmserver.tool.model.xml.Attribute;
import org.gs4tr.tmserver.tool.model.xml.Document;
import org.gs4tr.tmserver.tool.model.xml.Element;
import org.gs4tr.tmserver.tool.model.xml.Node;
import org.gs4tr.tmserver.tool.model.xml.parser.Parser;
import org.gs4tr.tmserver.tool.service.excel.ConvertExcelToTmx;
import org.junit.Test;

public class ConvertExcelToTmxTest
{
    @Test
    public void convertExcelToTmxTest1() throws Exception
    {
        String testFile = "target/test-classes/tencent/Billing and Management-计费中心-控制台-json.xls";
        String targetFile = "target/test-classes/tencent/Billing and Management-计费中心-控制台-json.tmx";
        String sourceLocale = "zh-CN";
        String targetLocale = "en-US";

        if (new File(targetFile).exists())
        {
            new File(targetFile).delete();
        }
        assertTrue(new File(testFile).exists());

        ConvertExcelToTmx converter = new ConvertExcelToTmx(testFile, targetFile, sourceLocale, targetLocale);
        converter.processTencentFile();

        assertTrue(new File(targetFile).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(targetFile);
            Document document = new Parser().parse(inputReader);
            List<Node> nodeList = document.selectNodes("//tu/prop");
            assertTrue(nodeList.size() > 0);

            // First attribute:
            Element ele = (Element) nodeList.get(0);
            assertEquals("prop", ele.getName().getName());
            assertEquals("Billing and Management", ele.getText());

            List<Attribute> attributeList = ele.attributes();
            assertTrue(attributeList.size() > 0);
            Attribute attribute = attributeList.get(0);
            assertEquals("type", attribute.getName().getName());
            assertEquals("Project", attribute.getValue().getText());

            // Second attribute:
            ele = (Element) nodeList.get(1);
            assertEquals("prop", ele.getName().getName());
            assertEquals("计费中心", ele.getText());

            attributeList = ele.attributes();
            assertTrue(attributeList.size() > 0);
            attribute = attributeList.get(0);
            assertEquals("type", attribute.getName().getName());
            assertEquals("Product", attribute.getValue().getText());

            // Third attribute:
            ele = (Element) nodeList.get(2);
            assertEquals("prop", ele.getName().getName());
            assertEquals("控制台", ele.getText());

            attributeList = ele.attributes();
            assertTrue(attributeList.size() > 0);
            attribute = attributeList.get(0);
            assertEquals("type", attribute.getName().getName());
            assertEquals("Type", attribute.getValue().getText());

            // Fourth attribute:
            ele = (Element) nodeList.get(3);
            assertEquals("prop", ele.getName().getName());
            assertEquals("k_000326o", ele.getText());

            attributeList = ele.attributes();
            assertTrue(attributeList.size() > 0);
            attribute = attributeList.get(0);
            assertEquals("type", attribute.getName().getName());
            assertEquals("json-context", attribute.getValue().getText());
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
        }
    }

    @Test
    public void convertExcelToTmxTest2() throws Exception
    {
        String testFile = "target/test-classes/tencent/Billing and Management-计费中心-控制台(1).xls";
        String targetFile = "target/test-classes/tencent/Billing and Management-计费中心-控制台(1).tmx";
        String sourceLocale = "zh-CN";
        String targetLocale = "en-US";

        if (new File(targetFile).exists())
        {
            new File(targetFile).delete();
        }
        assertTrue(new File(testFile).exists());

        ConvertExcelToTmx converter = new ConvertExcelToTmx(testFile, targetFile, sourceLocale, targetLocale);
        converter.processTencentFile();

        assertTrue(new File(targetFile).exists());
        Reader inputReader = null;
        try
        {
            inputReader = new FileReader(targetFile);
            Document document = new Parser().parse(inputReader);
            List<Node> nodeList = document.selectNodes("//tu/prop");
            assertTrue(nodeList.size() > 0);

            // First attribute:
            Element ele = (Element) nodeList.get(0);
            assertEquals("prop", ele.getName().getName());
            assertEquals("Billing and Management", ele.getText());

            List<Attribute> attributeList = ele.attributes();
            assertTrue(attributeList.size() > 0);
            Attribute attribute = attributeList.get(0);
            assertEquals("type", attribute.getName().getName());
            assertEquals("Project", attribute.getValue().getText());

            // Second attribute:
            ele = (Element) nodeList.get(1);
            assertEquals("prop", ele.getName().getName());
            assertEquals("计费中心", ele.getText());

            attributeList = ele.attributes();
            assertTrue(attributeList.size() > 0);
            attribute = attributeList.get(0);
            assertEquals("type", attribute.getName().getName());
            assertEquals("Product", attribute.getValue().getText());

            // Third attribute:
            ele = (Element) nodeList.get(2);
            assertEquals("prop", ele.getName().getName());
            assertEquals("控制台", ele.getText());

            attributeList = ele.attributes();
            assertTrue(attributeList.size() > 0);
            attribute = attributeList.get(0);
            assertEquals("type", attribute.getName().getName());
            assertEquals("Type", attribute.getValue().getText());
        }
        finally
        {
            IOUtils.closeQuietly(inputReader);
        }
    }
}
