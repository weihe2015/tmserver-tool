package org.gs4tr.tmserver.tool.locale;

import static org.junit.Assert.assertEquals;

import org.gs4tr.foundation.locale.Locale;
import org.junit.Test;

public class TmLocaleTest
{
	@Test
	public void TmxLocaleTest1() throws Exception
	{
		String targetLocale = "ke-MM-SGAW";
		Locale tgtLocale = Locale.makeLocale(targetLocale);
		assertEquals("Karen (Myanmar,Sgaw)", tgtLocale.getDisplayName());
	}

	@Test
	public void TmxLocaleTest2() throws Exception
	{
		String targetLocale = "sr-CS";
		Locale tgtLocale = Locale.makeLocale(targetLocale);
		assertEquals("Serbian (Serbia and Montenegro)", tgtLocale.getDisplayName());
	}
}
