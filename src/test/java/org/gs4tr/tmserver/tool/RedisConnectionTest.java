package org.gs4tr.tmserver.tool;

import org.junit.Assert;
import org.junit.Test;

import redis.clients.jedis.Jedis;

public class RedisConnectionTest
{
	@Test
	public void RedisConnectionTest1() throws Exception
	{
		Jedis jedis = new Jedis("10.12.21.49", 6379, 3000);
		jedis.set("foo", "bar");
		String value = jedis.get("foo");
		Assert.assertEquals("bar", value);
		jedis.del("foo");
		jedis.close();
	}
}
