#!/bin/sh

TC=/opt/apache-tomcat-9.0.19-Tool
TOOL_INSTALLER_FOLDER=/opt/tmp/builds/Tool
TOOL_INSTALLER=tmserver-tool-2.4.6-prod3.war

echo "======================="
echo "Stop Tomcat..."
echo "======================="
cd $TC
bin/shutdown.sh

echo "======================="
echo "Deleting resources..."
echo "======================="
rm -rf $TC/webapps/Toolqa/*
rm -rf $TC/logs/*
rm -rf $TC/work/Catalina/*
rm -rf $TC/conf/Catalina/*

echo "======================="
echo "Downloading installer..."
echo "======================="
if [ ! -d $TOOL_INSTALLER_FOLDER ]
then
   mkdir $TOOL_INSTALLER_FOLDER
fi
cd $TOOL_INSTALLER

if [ -f $TOOL_INSTALLER ]; then
  rm -rf $TOOL_INSTALLER
fi

wget --user=gl_release --password='Tx!cv9Iu0h' "ftp://securegl14.translations.com/QABuilds/GLTMS/TMSTool/$TOOL_INSTALLER"

echo "======================="
echo "Deploying build..."
echo "======================="

echo "##### Copy war/resources #####"
unzip -qq $TOOL_INSTALLER_FOLDER"/"$TOOL_INSTALLER-d $TC/webapps/Toolqa

echo "======================="
echo "Configuring application.properties..."
echo "======================="
cd $TC/webapps/Toolqa/WEB-INF/classes

TMSERVER_URL_OLD="tmserver.url=https:\/\/qa-tms2.translations.com\/TMS"
TMSERVER_URL_NEW="tmserver.url=https:\/\/stg-tpt10.translations.com\/TMS"

TMSERVER_POWER_USERNAME_OLD="tmserver.tmPowerUsername=whe"
TMSERVER_POWER_USERNAME_NEW="tmserver.tmPowerUsername=tms_tool"

TMSERVER_POWER_USERPASSWORD_OLD="tmserver.tmPowerUserpassword=password1!"
TMSERVER_POWER_USERPASSWORD_NEW="tmserver.tmPowerUserpassword=password1!"

MYSQLDB_USERNAME_OLD="mariaDB.datasource.username=user"
MYSQLDB_USERNAME_NEW="mariaDB.datasource.username=pd4"

MYSQLDB_PASSWORD_OLD="mysqlDB.datasource.password=user"
MYSQLDB_PASSWORD_NEW="mysqlDB.datasource.password=n4il1t"

sed -i "s/$TMSERVER_URL_OLD/$TMSERVER_URL_NEW/g" application.properties
sed -i "s/$TMSERVER_POWER_USERNAME_OLD/$TMSERVER_POWER_USERNAME_NEW/g" application.properties
sed -i "s/$TMSERVER_POWER_USERPASSWORD_OLD/$TMSERVER_POWER_USERPASSWORD_OLD/g" application.properties
sed -i "s/$MYSQLDB_USERNAME_OLD/$MYSQLDB_USERNAME_NEW/g" application.properties
sed -i "s/$MYSQLDB_PASSWORD_OLD/$MYSQLDB_PASSWORD_NEW/g" application.properties

echo "======================="
echo "Starting TMS..."
echo "======================="
$TC/bin/catalina.sh start

tail -f $TC/logs/catalina.out