#!/bin/sh

TC=/opt/apache-tomcat-9.0.13-GL
TOOL_INSTALLER_FOLDER=/opt/tmp/builds/Tool
TOOL_INSTALLER=tmserver-tool-2.4.6-prod4.war

echo "======================="
echo "Stop Tomcat..."
echo "======================="
cd $TC/bin
./shutdown.sh

#echo "Wait for 10 seconds for fully shutdown tomcat"
#sleep 10

echo "======================="
echo "Deleting resources..."
echo "======================="
rm -rf $TC/webapps/Tool/*
rm -rf $TC/logs/*
rm -rf $TC/Tool/*
rm -rf $TC/work/Catalina/*
rm -rf $TC/conf/Catalina/*

echo "======================="
echo "Downloading installer..."
echo "======================="
if [ ! -d $TOOL_INSTALLER_FOLDER ]
then
   mkdir $TOOL_INSTALLER_FOLDER
fi

cd $TOOL_INSTALLER_FOLDER

if [ -f $TOOL_INSTALLER ]; then
  rm -rf $TOOL_INSTALLER
fi

wget --user=gl_release --password='Tx!cv9Iu0h' "ftp://securegl14.translations.com/QABuilds/TMSTool/$TOOL_INSTALLER"

echo "======================="
echo "Deploying build..."
echo "======================="

echo "##### Copy war/resources #####"
unzip -qq $TOOL_INSTALLER_FOLDER"/"$TOOL_INSTALLER -d $TC/webapps/Tool

echo "======================="
echo "Configuring application.properties..."
echo "======================="
cd $TC/webapps/Tool/WEB-INF/classes

TMSERVER_URL_OLD="tmserver.url=https:\/\/qa-tms2.translations.com\/TMS"
TMSERVER_URL_NEW="tmserver.url=https:\/\/gl-tptprod4.transperfect.com\/TMS"

TMSERVER_POWER_USERNAME_OLD="tmserver.tmPowerUsername=whe"
TMSERVER_POWER_USERNAME_NEW="tmserver.tmPowerUsername=api_sync"

TMSERVER_POWER_USERPASSWORD_OLD="tmserver.tmPowerUserpassword=password1!"
TMSERVER_POWER_USERPASSWORD_NEW="tmserver.tmPowerUserpassword=rhhKy2ZuvXCfGp3a\*"

MYSQLDB_URL_OLD="jdbc:mariadb:\/\/localhost\/tm_server"
MYSQLDB_URL_NEW="jdbc:mariadb:\/\/10.10.47.52\/tm_server4"

MYSQLDB_USERNAME_OLD="mariaDB.datasource.username=user"
MYSQLDB_USERNAME_NEW="mariaDB.datasource.username=tms_tool"

MYSQLDB_PASSWORD_OLD="mariaDB.datasource.password=user"
MYSQLDB_PASSWORD_NEW="mariaDB.datasource.password=tmt001!"

FTP_URL_OLD="ftp.url=securegl14.translations.com"
FTP_URL_NEW="ftp.url=glsftp1-nj.translations.com"

FTP_USERNAME_OLD="ftp.username=ftp_tms4test"
FTP_USERNAME_NEW="ftp.username=tms_tool"

FTP_PASSWORD_OLD="ftp.password=&h^fEW\$W4^"
FTP_PASSWORD_NEW="ftp.password=ZSD2GwfQNja1"

sed -i "s/$TMSERVER_URL_OLD/$TMSERVER_URL_NEW/g" application.properties
sed -i "s/$TMSERVER_POWER_USERNAME_OLD/$TMSERVER_POWER_USERNAME_NEW/g" application.properties
sed -i "s/$TMSERVER_POWER_USERPASSWORD_OLD/$TMSERVER_POWER_USERPASSWORD_NEW/g" application.properties
sed -i "s/$MYSQLDB_URL_OLD/$MYSQLDB_URL_NEW/g" application.properties
sed -i "s/$MYSQLDB_USERNAME_OLD/$MYSQLDB_USERNAME_NEW/g" application.properties
sed -i "s/$MYSQLDB_PASSWORD_OLD/$MYSQLDB_PASSWORD_NEW/g" application.properties

sed -i "s/$FTP_URL_OLD/$FTP_URL_NEW/g" application.properties
sed -i "s/$FTP_USERNAME_OLD/$FTP_USERNAME_NEW/g" application.properties
sed -i "s/$FTP_PASSWORD_OLD/$FTP_PASSWORD_NEW/g" application.properties

echo "======================="
echo "Starting Tool..."
echo "======================="

cd "$TC/bin"
./startup.sh

tail -f "$TC/logs/catalina.out"