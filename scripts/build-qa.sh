#!/bin/sh
TOOL_HOME="/Users/whe/workspace/tmserver-tool"
WAR_FILE='tmserver-tool-2.4.6.war'

# build front-end files
cd src/main/resources/static
yarn build-qa

cd $TOOL_HOME

mvn -DskipTests clean package

# upload file to FTP:
#This is the FTP servers host or IP address.
HOST='securegl14.translations.com'
#This is the FTP user that has access to the server.
USER='gl_release'
#This is the password for the FTP user.
PASS='Tx!cv9Iu0h'

# TARGET FOLDER:
TARGET_FOLDER='QABuilds/TMSTool/'
FILE="target/$WAR_FILE"

# Upload file
curl --upload-file $FILE "ftp://$USER:$PASS@$HOST/$TARGET_FOLDER"