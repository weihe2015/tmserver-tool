FROM tomcat:9.0.17-jre8-alpine
VOLUME /tmp
# remove previous builds
RUN rm -rf /usr/local/tomcat/webapps/Tool/*
COPY ./target/tmserver-tool-2.4.1-RC2.war /usr/local/tomcat/webapps/Tool/tmserver-tool-2.4.1-RC2.war
RUN sh -c 'touch /usr/local/tomcat/webapps/Tool/tmserver-tool-2.4.1-RC2.war'
ENTRYPOINT ["catalina.sh", "run"]
